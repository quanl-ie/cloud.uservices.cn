<?php
/**
 * Created by PhpStorm.
 * User: xingq
 * Date: 2018/7/23
 * Time: 15:01
 */

namespace webapp\controllers;

use Yii;
use webapp\logic\IntentionProductLogic;


class IntentionProductController extends BaseController
{
    /**
     * 添加客户意向产品
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/24
     * Time: 15:17
     * @return mixed|string|void
     */
    public function actionAdd()
    {
        if (Yii::$app->request->isPost)
        {
            $res = IntentionProductLogic::add();
            return json_encode($res);
        }
    }
    
    /**
     * 修改客户意向产品
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/24
     * Time: 14:40
     * @return mixed|string|void|\yii\web\Response
     */
    public function actionEdit()
    {
        if (Yii::$app->request->isPost)
        {
            $res = IntentionProductLogic::edit();
            return json_encode($res);
        }
        
        $res = IntentionProductLogic::beforeEdit();
        
        if ($res['code'] != 200)
        {
            Yii::$app->session->setFlash('intention_product_message',$res['message']);
            return $this->redirect('account/intention-product');
        }
        
        return $this->renderPartial('edit',['data'=>$res['data']]);
    }
    
    /**
     * 删除意向产品
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/24
     * Time: 14:51
     * @return \yii\web\Response
     */
    public function actionDel()
    {
        $res = IntentionProductLogic::del();
        return json_encode($res);
    }
}
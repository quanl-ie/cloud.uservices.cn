<?php

namespace webapp\controllers;

use webapp\logic\BaseLogic;
use webapp\logic\ContractTypeLogic;
use webapp\logic\ServicesClassLogic;
use webapp\models\AccountAddress;
use webapp\models\BrandQualification;
use Yii;

use webapp\logic\ContractLogic;
use yii\web\Controller;
use common\helpers\Paging;
use webapp\models\Cooporation;
use common\models\ServiceClass;
use webapp\models\SaleOrder;
use webapp\models\Department;
use webapp\models\UploadForm;
use webapp\models\ContractForm;
use yii\web\UploadedFile;
use webapp\models\User;
/**
 *@desc:合同管理
 * @author: chengjuanjuan <chengjuanjuan@c-ntek.com>
 * createdAt: 2018/03/21 10:47
 *
 */


class ContractController extends BaseController
{

    /**
     * @desc 待审批列表
     **/
    public function actionWaitCheck(){

        $result = ContractLogic::getList();
        if($result['totalPage'] >=1){
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();

        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           =  $params;
        $this->_view['department_id']    =  BaseLogic::getDepartmentId();
        $this->_view['department']       =  $info;
        $this->_view['company_id']       =  $this->directCompanyId;
        $this->_view['auth']             =  $this->dataAuthId;
        $this->_view['data']             =  $result['list'];
        $this->_view['pageHtml']         =  isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  =  $result['department_name'];

        return $this->render('wait-check');
    }

    /**
     * @desc 执行中列表
     **/
    public function actionExecuting(){
        //系统是否开启发货功能状态
        $shipSys = '1'; //1 已开启 0 未开启
        $result = ContractLogic::getList('',$shipSys);

        $queryStr = $_SERVER['QUERY_STRING'];
        if($result['totalPage'] >=1){
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();
        $params['department_id']         = trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           = $params;
        $this->_view['department_id']    = BaseLogic::getDepartmentId();
        $this->_view['department']       = $info;
        $this->_view['company_id']       = $this->directCompanyId;
        $this->_view['auth']             = $this->dataAuthId;
        $this->_view['data']             = $result['list'];
        $this->_view['pageHtml']         = isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  = $result['department_name'];
        $this->_view['ship_sys']         = $shipSys;
        return $this->render('executing');
    }


    /**
     * @desc 执行完毕列表
     **/
    public function actionExecuteFinished(){
        //系统是否开启发货功能状态
        $shipSys = '1'; //1 已开启 0 未开启
        $result = ContractLogic::getList('',$shipSys);
        if($result['totalPage'] >=1){
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();

        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           =  $params;
        $this->_view['department_id']    = BaseLogic::getDepartmentId();
        $this->_view['department']       = $info;
        $this->_view['company_id']       = $this->directCompanyId;
        $this->_view['auth']             =  $this->dataAuthId;
        $this->_view['data']             = $result['list'];
        $this->_view['pageHtml']         = isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  =  $result['department_name'];
        return $this->render('execute-finished');
    }


    //审批未通过列表
    public function actionUnchecked(){
        $result = ContractLogic::getList();
        if($result['totalPage'] >=1){
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();

        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           =  $params;
        $this->_view['department_id']    = BaseLogic::getDepartmentId();
        $this->_view['department']       = $info;
        $this->_view['company_id']       = $this->directCompanyId;
        $this->_view['auth']             =  $this->dataAuthId;
        $this->_view['data']             = $result['list'];
        $this->_view['pageHtml']         = isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  =  $result['department_name'];
        return $this->render('unchecked');
    }

    //终止列表
    public function actionTerminated(){
        $result = ContractLogic::getList();
        if($result['totalPage'] >=1){
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();

        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           =  $params;
        $this->_view['department_id']    = BaseLogic::getDepartmentId();
        $this->_view['department']       = $info;
        $this->_view['company_id']       = $this->directCompanyId;
        $this->_view['auth']             =  $this->dataAuthId;
        $this->_view['data']             = $result['list'];
        $this->_view['pageHtml']         = isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  =  $result['department_name'];
        return $this->render('terminated');
    }


    // 添加
    public function actionAdd()
    {
        $accountId =  yii::$app->request->get('accountId',0);
        if($accountId){
            $address = AccountAddress::getAddress($accountId);
        }
        $dataAuthId          = $this->dataAuthId;
        if($dataAuthId == 3){  //本公司及以下
            $top_id = $this->directCompanyId;
            $info = department::getDepartmentInfo($top_id,1,0,1);
            //查询是否还有下级
            $nextDepartment = Department::getDepartmentChildren($top_id,0,1);
            if(count($nextDepartment) > 1){
                $info[0]['exist'] = 1; //有下级
            }else{
                $info[0]['exist'] = 2; //无
            }
        }else if($dataAuthId == 2){  //本公司
            $top_id = $this->directCompanyId;
            $info = department::getDepartmentInfo($top_id,1,0,1);
            $nextDepartment = Department::getDepartmentChildren($top_id,1,1);
            if(count($nextDepartment) > 1){
                $info[0]['exist'] = 1; //有下级
            }else{
                $info[0]['exist'] = 2; //无
            }
        }else if($dataAuthId == 5){   //部门以下
            if($this->departmentObj->type == 3){
                $info = department::getDepartmentInfo($this->departmentId,1,0,1);
                $nextDepartment = Department::getDepartmentChildren($this->departmentId,2);
                if(count($nextDepartment) > 1){
                    $info[0]['exist'] = 1; //有下级
                }else{
                    $info[0]['exist'] = 2; //无
                }
            }else{
                $info = department::getDepartmentInfo($this->departmentId,1,1,1);
                $nextDepartment = Department::getDepartmentChildren($this->departmentId,2);
                if(count($nextDepartment) > 1){
                    $info[0]['exist'] = 1; //有下级
                }else{
                    $info[0]['exist'] = 2; //无
                }
            }

        }else if($dataAuthId == 4){   //本部门
            if($this->departmentObj->type == 3){
                $info = department::getDepartmentInfo($this->departmentId,1,0,1);
                $info[0]['exist'] = 2; //无
            }else{
                $info = department::getDepartmentInfo($this->departmentId,1,1,1);
                $info[0]['exist'] = 2;
            }
        }else{  //本人
            $info = department::getDepartmentInfo($this->departmentId,1,1,1);
            $info[0]['exist'] = 2;
        }
        //添加 部门默认值为本部门
        $define_department = department::getDepartmentInfo($this->departmentId,1,1,1);
        $define_department = $define_department[0];
        //查询当前登陆默认销售人员信息
        $define_user_info['info'] = $this->user->username.'/'.$this->user->mobile;
        $define_user_info['id']   = $this->user->id;
        //品牌
        $brand = BaseLogic::getBrand([],1);
        //合同分类列表
        $contractClassifyList = ContractTypeLogic::getTypeList();
        $model = new ContractForm();
        if (yii::$app->request->isPost) {
            $postData = yii::$app->request->post();
            //print_r($postData);die;
            //判断是否存在id
            if (yii::$app->request->get('id',0) == 0) {
                $res = ContractLogic::add();
                if($res['code'] == 200) {
                    return $this->redirect('/contract/wait-check');
                }
                else {
                    Yii::$app->session->setFlash('message',$res['message']);
                }
            }
        }
        return $this->render('add',[
            'define_department'     => $define_department,
            'define_user_info'      => $define_user_info,
            'department'            => $info, //权限内的所有机构
            'auth'                  => $dataAuthId,
            'model'                 => $model,
            'brand'                 => $brand,
            'contractClassifyList'  =>$contractClassifyList
        ]);
    }

   public function actionEdit(){
       $dataAuthId          = $this->dataAuthId;
       if($dataAuthId == 3){  //本公司及以下
           $top_id = $this->directCompanyId;
           $info = department::getDepartmentInfo($top_id,1,0,1);
           //查询是否还有下级
           $nextDepartment = Department::getDepartmentChildren($top_id,0,1);
           if(count($nextDepartment) > 1){
               $info[0]['exist'] = 1; //有下级
           }else{
               $info[0]['exist'] = 2; //无
           }
       }else if($dataAuthId == 2){  //本公司
           $top_id = $this->directCompanyId;
           $info = department::getDepartmentInfo($top_id,1,0,1);
           $nextDepartment = Department::getDepartmentChildren($top_id,1,1);
           if(count($nextDepartment) > 1){
               $info[0]['exist'] = 1; //有下级
           }else{
               $info[0]['exist'] = 2; //无
           }
       }else if($dataAuthId == 5){   //部门以下
           if($this->departmentObj->type == 3){
               $info = department::getDepartmentInfo($this->departmentId,1,0,1);
               $nextDepartment = Department::getDepartmentChildren($this->departmentId,2);
               if(count($nextDepartment) > 1){
                   $info[0]['exist'] = 1; //有下级
               }else{
                   $info[0]['exist'] = 2; //无
               }
           }else{
               $info = department::getDepartmentInfo($this->departmentId,1,1,1);
               $nextDepartment = Department::getDepartmentChildren($this->departmentId,2);
               if(count($nextDepartment) > 1){
                   $info[0]['exist'] = 1; //有下级
               }else{
                   $info[0]['exist'] = 2; //无
               }
           }

       }else if($dataAuthId == 4){   //本部门
           if($this->departmentObj->type == 3){
               $info = department::getDepartmentInfo($this->departmentId,1,0,1);
               $info[0]['exist'] = 2; //无
           }else{
               $info = department::getDepartmentInfo($this->departmentId,1,1,1);
               $info[0]['exist'] = 2;
           }
       }else{  //本人
           $info = department::getDepartmentInfo($this->departmentId,1,1,1);
           $info[0]['exist'] = 2;
       }
       //添加 部门默认值为本部门
       $define_department = department::getDepartmentInfo($this->departmentId,1,1,1);
       $define_department = $define_department[0];
       //品牌
       $brand = BaseLogic::getBrand([],1);
       //合同分类列表
       $contractClassifyList = ContractTypeLogic::getTypeList();
       $model = new ContractForm();
        if (yii::$app->request->isPost) {
            $postData = yii::$app->request->post();
            //print_r($postData);die;
            $res = ContractLogic::edit();
            if($res['code'] != 200) {
                Yii::$app->session->setFlash('message',$res['message']);
            }else{
                return $this->redirect('/contract/wait-check');
            }
        }
        $res = ContractLogic::getDetail();
        $list = $res['data'];
       if($res['code'] != 200) {
           Yii::$app->session->setFlash('message',$res['message']);
       }
       $address = ContractLogic::getAddress($list['info']['account_id'],$list['info']['delivery_address']) ;

       //查询销售人员信息
       $account = User::getOne(['id'=>$list['info']['sale_user_id']]);
       $list['info']['sale_user_info'] = $account['username'].'/'.$account['mobile'];
       //获取机构信息
       $department_edit = array();
       if(!empty($account)){
           $department_name = Department::getOne(['id'=>$list['info']['sale_shop']]);
           $department_edit = isset($department_name->attributes)?$department_name->attributes:[];
       }
       return $this->render('/contract/edit',[
           'info'                   => $list['info'],
           'detail'                 => $list['detail'],
           'address'                => $address,
           'define_department'      => $define_department,
           'department'             => $info, //权限内的所有机构
           'auth'                   => $dataAuthId,
           'model'                  => $model,
           'brand'                  => $brand,
           'contractClassifyList'   => $contractClassifyList,
           'department_edit'        => $department_edit,

       ]);

    }


    //详情
    function actionDetail(){
        //系统是否开启发货功能状态
        $shipSys = '1'; //1 已开启 0 未开启
        $id     = intval(Yii::$app->request->get('id'));
        $res    = ContractLogic::getDetail($id,'','all');
        $statusDesc = ['1'=>'待指派','2'=>'待服务','3'=>'服务中','4'=>'已完成'];
        if($res){
            return $this->render('detail',[
                'info'          => isset($res['data']['info']) ? $res['data']['info'] : '',
                'detail'        => isset($res['data']['detail']) ? $res['data']['detail'] :'',
                'stock_info'    => isset($res['data']['stock_info']) ? $res['data']['stock_info'] :'',
                'send_list_info'=> isset($res['data']['send_list_info']) ? $res['data']['send_list_info'] :'',
                'return_info'   => isset($res['data']['return_info']) ? $res['data']['return_info'] : '',
                'exchange_info' => isset($res['data']['exchange_info']) ? $res['data']['exchange_info'] :'',
                'order_data_info' => isset($res['data']['order_data_info'])?$res['data']['order_data_info']:[],
                'shipSys'         =>$shipSys,
                'statusDesc'      =>$statusDesc
            ]);
        }

    }


    //审核
    function actionCheckOption(){
        //系统是否开启发货功能状态
        $shipSys = '1'; //1 已开启 0 未开启
        $id = intval(Yii::$app->request->get('id'));
        if(Yii::$app->request->post()){
           $res = ContractLogic::checkOption();
           return json_encode($res);
        }
        $res = ContractLogic::getDetail($id,'','all');
        return $this->render('detail',[
            'info'          =>$res['data']['info'],
            'detail'        =>$res['data']['detail'],
            'shipSys'       =>$shipSys
        ]);
    }


    //更新状态
    function actionUpdateStatus(){
        $id = intval(Yii::$app->request->post('id'));
        $type = trim(Yii::$app->request->post('type'));
        $info = ContractLogic::getDetail($id);
        if($info['success']){
            $res = ContractLogic::updateStatus($type);
            return json_encode($res);
        }
    }
    
    /**
     * 获取合同弹窗列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/8/1
     * Time: 14:43
     * @return string
     */
    public function actionGetContract(){
        
        $result = ContractLogic::getList();
        if($result['totalPage'] >=1)
        {
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();
        
        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        
        return $this->renderPartial('get-contract',[
            'params'          =>$params,
            'department_id'   =>BaseLogic::getDepartmentId(),
            'department'      =>$info,
            'company_id'      =>$this->directCompanyId,
            'auth'            =>$this->dataAuthId,
            'data'            =>$result['list'],
            'pageHtml'        =>isset($pageHtml)?$pageHtml:' ',
            'department_name' =>$result['department_name'],
        
        ]);
    }
    
    /**
     * 获取选中合同信息
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/8/1
     * Time: 16:35
     * @return mixed|string|void
     */
    public function actionGetContractDetail()
    {
        $data = ContractLogic::getContractDetail();
        return json_encode($data);
    }

}


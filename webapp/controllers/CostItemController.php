<?php

namespace webapp\controllers;

use Yii;
use webapp\logic\CostItemLogic;
use common\helpers\Paging;

class CostItemController extends BaseController
{
    /**
     * 列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 14:04
     * @return string
     */
    public function actionIndex()
    {
        $res = CostItemLogic::getIndex();
        
        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }
        
        $this->_view['flag']     = $res['flag'];
        $this->_view['data']     = $res['list'];
        return $this->render('index');
        
    }
    
    /**
     * 编辑
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 14:04
     * @return mixed|string|void
     */
    public function actionAdd()
    {
        
        $data = CostItemLogic::beforeAdd();
        
        if (Yii::$app->request->isPost)
        {
            
            $res = CostItemLogic::add();
            return json_encode($res);
        }
        
        return $this->renderPartial('add',
            [
                'costItemSetId'   => $data['costItemSetId'],
                'costItemSetList' => $data['costItemSetList'],
            ]);
    }
    
    /**
     * 修改状态（禁用、启用）
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 14:04
     * @return mixed|string|void
     */
    public function actionChangeStatus()
    {
        
        $res = CostItemLogic::changeStatus();
        
        return json_encode($res);
        
    }
    /**
     * 修改状态（禁用、启用）
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 14:04
     * @return mixed|string|void
     */
    public function actionChangeStatusOn()
    {

        $res = CostItemLogic::changeStatus();

        return json_encode($res);

    }
    
}


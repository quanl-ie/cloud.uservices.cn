<?php

namespace webapp\controllers;

use Yii;
use common\models\Region;
use webapp\logic\AreaLogic;

class AreaController extends BaseController
{
    /**
     * 列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/20
     * Time: 16:40
     * @return string
     */
    public function actionIndex()
    {
        
        $data = AreaLogic::getIndex();
        
        $this->_view['flag']     = $data['flag'];
        if (!empty($data['province']) && !empty($data['city']) && !empty($data['district'])) {
            $this->_view['province'] = $data['province'];
            $this->_view['city']     = $data['city'];
            $this->_view['district'] = $data['district'];
        }
        $this->_view['provinceList'] = $data['provinceList'];
        
        return $this->render('index');
        
    }
    
    /**
     * 编辑
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/20
     * Time: 16:40
     * @return mixed|string|void
     */
    public function actionAdd()
    {

        $data = AreaLogic::beforeAdd();
        
        if (Yii::$app->request->isPost)
        {
            
            $res = AreaLogic::add();
            return json_encode($res);
        }
    
        $this->_view['province'] = [];
        $this->_view['city']     = [];
        $this->_view['district'] = [];
        
        if (!empty($data['province']) && !empty($data['city']) && !empty($data['district'])) {
            $this->_view['province'] = $data['province'];
            $this->_view['city']     = $data['city'];
            $this->_view['district'] = $data['district'];
        }
        
        $this->_view['provinceList'] = $data['provinceList'];
        
        return $this->render('add');
    }
    
    /**
     * ajax 获取城市和区县
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/20
     * Time: 16:40
     * @return mixed|string|void
     */
    public function actionCity()
    {
        $provinceId = array_filter(Yii::$app->request->post('province'));
        
        if (empty($provinceId)) {
            return json_encode([]);
        }
        
        $citys = Region::getList(['parent_id'=>$provinceId]);
    
        $data    = [];
        $citysId = [];
        
        foreach ($citys as $key => $val) {
            $citysId[] = $val['region_id'];
        }
        
        $districts = Region::getList(['parent_id'=>array_filter($citysId)]);
        
        $data['city']     = $citys;
        $data['district'] = $districts;
        
        return json_encode($data);
    }
    
    /**
     * ajax获取区县
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/20
     * Time: 16:40
     * @return mixed|string|void
     */
    public function actionDistrict()
    {
        $citysId   = array_filter(Yii::$app->request->post('citys'));
    
        if (empty($citysId)) {
            return json_encode([]);
        }
        
        $districts = Region::getList(['parent_id'=>$citysId]);
        
        return json_encode($districts);
    }
    
}


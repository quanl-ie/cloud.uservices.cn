<?php
    /**
     * Created by PhpStorm.
     * User: xingq
     * Date: 2018/3/29
     * Time: 14:11
     */
    
    namespace webapp\controllers;
    
    use Yii;
    use webapp\logic\StockLogic;
    use common\helpers\Paging;

    class StockController extends BaseController
    {
        /**
         * 列表
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/29
         * Time: 16:39
         * @return string
         */
        public function actionIndex()
        {
            $getStr = '?';
            if($_GET){
                $getStr ='?'. http_build_query($_GET) . '&';
            }

            //系统是否开启发货功能状态
            $ship_sys = '1'; //1 已开启 0 未开启
            $data = StockLogic::getBeforeList();
            
            $res = StockLogic::getIndex();



            $pageHtml = '';
            if($res['totalPage'] >=1 && !empty($res['list'])){
                $pageHtml = Paging::make($res['page'], $res['totalPage'], $getStr.'page=');
            }

            $this->_view['data']        = $res['list'];
            $this->_view['brand']       = $data['brand'];
            $this->_view['class']       = $data['class'];
            $this->_view['className']   = $res['className'];
            $this->_view['ship_sys']    = $ship_sys;
            $this->_view['pageHtml']    = $pageHtml;
            return $this->render('index');
        }

        /**
         * 库存详情
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/10
         * Time: 14:09
         * @return string
         */
        public function actionView()
        {
            
            $res = StockLogic::getView();
            
            $this->_view['data']  = $res;
            $this->_view['depot'] = $res['depot'];
            return $this->render('view');
        }
    
        /**
         * 库存明细
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/10
         * Time: 15:40
         * @return string
         */
        public function actionDetail()
        {
            $res = StockLogic::getDetail();
            
            if (isset($res['code']) && $res['code'] != 200) {
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect(Yii::$app->request->getReferrer());
            }
            
            if($res['totalPage'] >=1 && !empty($res['list'])){
                $pageHtml = Paging::make($res['page'], $res['totalPage'], '?page=');
            }else{
                $pageHtml = '';
            }
            
            $this->_view['data'] = $res['list'];
            return $this->render('detail');
        }
    }
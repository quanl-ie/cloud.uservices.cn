<?php
namespace webapp\controllers;
use webapp\logic\DismantlingConfigLogic;
use webapp\logic\FeeLogic;
use Yii;

class FeeController extends BaseController
{
    /**
     * 收费确认列表
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * 收费确认开关
     * @return string
     */
    public function actionSetting()
    {
        $res = DismantlingConfigLogic::index(self::FEE_TYPE);
        if (Yii::$app->request->isPost)
        {
            $res = DismantlingConfigLogic::add(self::FEE_TYPE);
            Yii::$app->session->setFlash('fee_setting_config_message',$res['message']);

            $this->clearMenuCache();
            return $this->redirect('setting');
        }
        $this->_view['list'] = $res['list'];
        $this->_view['data'] = $res['self'];
        return $this->render('setting');
    }
}
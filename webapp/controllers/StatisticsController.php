<?php
namespace webapp\controllers;

use Yii;
use webapp\models\OrderInfo;
use common\helpers\Paging;
use common\models\ServiceClass;
use common\models\ServiceBrand;
use common\models\ServiceType;
use webapp\models\Role;
use webapp\models\AdminUser;
use webapp\models\UserForm;
use webapp\models\UserInfo;
use webapp\controllers\BaseController;
use webapp\logic\RoleLogic;
use webapp\logic\StatisticsLogic;
use common\helpers\ExcelHelper;

/**
* 函数用途描述:统计中心
* @date: 2017年12月18日 下午2:24:17
* @author: sxz<shixiaozheng@c-ntek.com>
* @param: variable
* @return:
*/
class StatisticsController extends BaseController
{
    /**
    * 函数用途描述:统计中心
    * @date: 2017年12月18日 下午2:26:23
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionIndex()
    {   
        $request = Yii::$app->request;
        if($request->isGet){
            $params['start_time'] = $start_time = Yii::$app->request->get('start_time','');
            $params['end_time']   = $end_time   = Yii::$app->request->get('end_time','');
            $params['export']     = $export     = YIi::$app->request->get('export','');
        }else {
            $params['start_time'] = $start_time = Yii::$app->request->post('start_time','');
            $params['end_time']   = $end_time   = Yii::$app->request->post('end_time','');
            $params['export']     = $export     = YIi::$app->request->post('export','');
        }
        if(empty($start_time) && empty($end_time)){
            $params['start_time'] = $start_time = date('Y-m-d', time()); //2016-11-01 00:00:00 
            $params['end_time'] = $end_time   = date('Y-m-d', time()); //2016-11-01 23:59:59
        }
        
        $result = StatisticsLogic::getList($start_time,$end_time);
        $pageHtml = '';
        if (Yii::$app->request->isAjax) {
            return json_encode($result['show'],true);
        }
        //print_r($result);exit;
        //数据导出
        if(!empty($export) && !empty($result['showlist']['list'])){
            $title = $sheet_title = '售后订单概览'.$start_time.'至'.$end_time;
            $dataList = $result['showlist']['list'];
            $setTitle = $result['showlist']['total'];
            StatisticsLogic::setExport($setTitle,$dataList,$title,$sheet_title);
        }
        $this->_view['data']     = $result;
        //$this->_view['pageHtml'] = $pageHtml;
        return $this->render('index',['params'=>$params]);
    }
    /**
    * 函数用途描述：服务产品报表统计
    * @date: 2017年12月23日 下午2:35:57
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionServiceProduct(){
        $request = Yii::$app->request;
        if($request->isGet){
            $params['start_time']  = Yii::$app->request->get('start_time','');
            $params['end_time']    = Yii::$app->request->get('end_time','');
            $params['class_type']  = Yii::$app->request->get('class_type','');
            $params['products_id'] = Yii::$app->request->get('products_id','');
            $params['class_p_id'] = Yii::$app->request->get('class_p_id','');
            $params['export']      = $export     = YIi::$app->request->get('export','');
        }else {
            $params['start_time']  = Yii::$app->request->post('start_time','');
            $params['end_time']    = Yii::$app->request->post('end_time','');
            $params['class_type']  = Yii::$app->request->post('class_type','');
            $params['products_id'] = Yii::$app->request->post('products_id',[]);
            $params['class_p_id'] = Yii::$app->request->post('class_p_id','');
            $params['export']      = $export     = YIi::$app->request->post('export','');
        }
        $ch_id = '';
        if(!empty($params['products_id']) && is_array($params['products_id'])){
            //获取二级分类的第一个id，用于获取父级id
            $ch_id = $params['products_id'][0];
            $params['products_id'] = implode($params['products_id'],',');
        }
        //print_r($params);exit;
        //获取所有一级分类
        $classList   = ServiceClass::getServiceClass();
        $result['list'] = $result['type_total'] = $products_info = $classType1 = [];
        if($params['class_type'] == 2){
            if(empty($classList)){
                return $this->render('service-product', [
                'data_list'     =>$result['list'],
                'type_total'    =>$result['type_total'],
                'products_info' =>$products_info,
                'class_list'    =>$classList,
                'class_type1'   =>$classType1, //用于二级分类时输出使用
                'params'        =>$params
                ]);
            }
            $pid = 0;
            if($ch_id){
                //获取二级分类的一级父id
                $pid = ServiceClass::getClassPid($ch_id);
                $params['class_p_id'] = $pid;
            }else {
                //获取一级分类默认第一个值
                $pid = key($classList);
            }
            //获取二级分类的数据
            $classList              = ServiceClass::getServiceClass($pid);
            //获取一级分类数据
            $classType1             = ServiceClass::getServiceClass();
            if(empty($params['products_id'])){
               //默认获取二级分类前5个
                $newArr = [];
                $newArr = array_keys(StatisticsLogic::getArr($classList));
                $params['products_id'] = implode($newArr,',');
            }
        }
        if(empty($params['products_id']) && empty($params['class_type'])){
            if(!empty($classList)){
                //默认获取一级分类前5个
                $newArr = [];
                $newArr = array_keys(StatisticsLogic::getArr($classList));
                $params['products_id'] = implode($newArr,',');
            }
        }
        //格式化所选产品类目
        $products_info = StatisticsLogic::getProductClass($params['products_id']);
        if(empty($params['start_time']) && empty($params['end_time'])){
            $params['start_time']= date('Y-m-d', time()); //2016-11-01 00:00:00
            $params['end_time']= date('Y-m-d', time()); //2016-11-01 23:59:59
        }
        //一级类目统计数据
        $result = StatisticsLogic::getServiceProduct($params['start_time'],$params['end_time'],$params['products_id'],$params['class_type']);
        if(empty($result)){
            $result['list'] = $result['type_total'] = [];
        }
        if (Yii::$app->request->isAjax) {
            return json_encode($result['show_data'],true);
        }
        //数据导出
        if(!empty($export) && !empty($result['list'])){
            $title = $sheet_title = '服务产品概览'.$params['start_time'].'至'.$params['end_time'];
            $dataList = $result['list'];
            $setTitle = $result['type_total'];
            StatisticsLogic::setExport2($setTitle,$dataList,$title,$sheet_title);
        }
        //渲染输出
        return $this->render('service-product', [
                'data_list'     =>$result['list'],
                'type_total'    =>$result['type_total'],
                'products_info' =>$products_info,
                'class_list'    =>$classList,
                'class_type1'   =>$classType1,      //用于二级分类时输出使用
                'params'        =>$params
        ]);
    }
    /**
    * 函数用途描述:切换到二级类目时，根据产品类型一级获取二级数据
    * @date: 2017年12月26日 上午11:22:56
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionGetClasses(){
        $pid = Yii::$app->request->post('pid','');
        $data = [];
        if($pid){
            $data       = ServiceClass::getServiceClass($pid);
        }
        return json_encode($data,true);
        
        
    }
    /**
    * 函数用途描述
    * @date: 2017年12月25日 下午9:14:47
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionServiceBrand(){
        $request = Yii::$app->request;
        if($request->isGet){
            $params['start_time'] = $start_time = Yii::$app->request->get('start_time','');
            $params['end_time']   = $end_time   = Yii::$app->request->get('end_time','');
            $params['brand_ids']  = $brand_ids  = Yii::$app->request->get('brand_ids','');
            $params['export']     = $export     = YIi::$app->request->get('export','');
        }else {
            $params['start_time'] = $start_time = Yii::$app->request->post('start_time','');
            $params['end_time']   = $end_time   = Yii::$app->request->post('end_time','');
            $params['brand_ids']  = $brand_ids  = Yii::$app->request->post('brand_ids',[]);
            $params['export']     = $export     = YIi::$app->request->post('export','');
        }
        if(empty($start_time) && empty($end_time)){
            $params['start_time'] = $start_time = date('Y-m-d', time()); //2016-11-01 00:00:00
            $params['end_time']   = $end_time   = date('Y-m-d', time()); //2016-11-01 23:59:59
        }
        //获取所有品牌
        $brandList = ServiceBrand::getServiceBrand();
        if(!empty($brand_ids) && is_array($brand_ids)){
            $params['brand_ids'] = $brand_ids = implode($brand_ids,',');
        }
        if(empty($brand_ids)){
            //默认查出品牌前5个的数据
            $newArr = [];
            $newArr = array_keys(StatisticsLogic::getArr($brandList));
            $params['brand_ids'] = $brand_ids = implode($newArr,',');
        }
        //品牌类目统计数据
        $result = StatisticsLogic::getBrandInfo($start_time,$end_time,$brand_ids);
        //格式化所选产品类目
        $brand_infos = StatisticsLogic::getProductBrand($brand_ids);
        //数据导出
        if(!empty($export) && !empty($result['list'])){
            $title = $sheet_title = '服务品牌概览'.$params['start_time'].'至'.$params['end_time'];
            $dataList = $result['list'];
            $setTitle = $result['type_total'];
            StatisticsLogic::setExport2($setTitle,$dataList,$title,$sheet_title);
        }
        if (Yii::$app->request->isAjax) {
            return json_encode($result['show_data'],true);
        }
        //渲染输出
        return $this->render('service-brand', [
                'data_list'     =>$result['list'],
                'brand_total'   =>$result['type_total'],
                'brand_infos'   =>$brand_infos,
                'brand_list'    =>$brandList,
                'params'        =>$params
        ]);
        
    }
    /**
    * 函数用途描述：服务类型报表统计
    * @date: 2017年12月26日 下午7:04:39
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionServiceType(){
        $request = Yii::$app->request;
        if($request->isGet){
            $params['start_time'] = $start_time = Yii::$app->request->get('start_time','');
            $params['end_time']   = $end_time   = Yii::$app->request->get('end_time','');
            $params['type_ids']   = $type_ids   = Yii::$app->request->get('type_ids','');
            $params['export']     = $export     = YIi::$app->request->get('export','');
        }else {
            $params['start_time'] = $start_time = Yii::$app->request->post('start_time','');
            $params['end_time']   = $end_time   = Yii::$app->request->post('end_time','');
            $params['type_ids']   = $type_ids   = Yii::$app->request->post('type_ids',[]);
            $params['export']     = $export     = YIi::$app->request->post('export','');
        }
        if(empty($start_time) && empty($end_time)){
            $params['start_time'] = $start_time = date('Y-m-d', time()); //2016-11-01 00:00:00
            $params['end_time']   = $end_time   = date('Y-m-d', time()); //2016-11-01 23:59:59
        }
        $result['list'] = $result['type_total'] = $type_infos = $typeList = [];
        //获取已有服务类型
        $typeList = ServiceType::getTypeName();
        if(empty($typeList)){
                return $this->render('type', [
                'data_list'     =>$result['list'],
                'type_total'    =>$result['type_total'],
                'type_infos'    =>$type_infos,
                'type_list'     =>$typeList,
                'params'        =>$params
                ]);
        }
        
        if(!empty($type_ids) && is_array($type_ids)){
            $params['type_ids'] = $type_ids = implode($type_ids,',');
        }
        if(empty($type_ids)){
            //默认查出服务类型前5个的数据
            $newArr = [];
            $newArr = array_keys(StatisticsLogic::getArr($typeList));
            $params['type_ids'] = $type_ids = implode($newArr,',');
        }
        //服务类型类目统计数据
        $result = StatisticsLogic::getServiceTypeSaleInfo($start_time,$end_time,$type_ids);
        //格式化所选产品类目
        $type_infos = StatisticsLogic::getProductType($type_ids);
        if (Yii::$app->request->isAjax) {
            return json_encode($result['show_data'],true);
        }
        //数据导出
        if(!empty($export) && !empty($result['list'])){
            $title = $sheet_title = '服务品牌概览'.$params['start_time'].'至'.$params['end_time'];
            $dataList = $result['list'];
            $setTitle = $result['type_total'];
            StatisticsLogic::setExport2($setTitle,$dataList,$title,$sheet_title);
        }
        //渲染输出
        return $this->render('type', [
                'data_list'     =>$result['list'],
                'type_total'   =>$result['type_total'],
                'type_infos'   =>$type_infos,
                'type_list'    =>$typeList,
                'params'       =>$params
        ]);
    }
    /**
    * 函数用途描述:订单异常数据统计
    * @date: 2017年12月28日 下午1:50:35
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionAbnormalOrders(){
        $request = Yii::$app->request;
        if($request->isGet){
            $params['start_time'] = $start_time = Yii::$app->request->get('start_time','');
            $params['end_time']   = $end_time   = Yii::$app->request->get('end_time','');
            $params['reason_ids'] = $reason_ids  = Yii::$app->request->get('reason_ids',[9,6,8,7]);
            $params['export']     = $export     = YIi::$app->request->get('export','');
        }else {
            $params['start_time'] = $start_time = Yii::$app->request->post('start_time','');
            $params['end_time']   = $end_time   = Yii::$app->request->post('end_time','');
            $params['reason_ids'] = $reason_ids  = Yii::$app->request->post('reason_ids',[9,6,8,7]);
            $params['export']     = $export     = YIi::$app->request->post('export','');
        }
        if(empty($start_time) && empty($end_time)){
            $params['start_time'] = $start_time = date('Y-m-d', time()); //2016-11-01 00:00:00
            $params['end_time']   = $end_time   = date('Y-m-d', time()); //2016-11-01 23:59:59
        }
        $result['list'] = $result['type_total'] = $type_infos = $reasonList = [];
        //获取已有服务类型
        $reasonList = Orderinfo::getReasonNames($id='');
        if(empty($reasonList)){
            return $this->render('type', [
                    'data_list'     =>$result['list'],
                    'type_total'    =>$result['type_total'],
                    'type_infos'    =>$type_infos,
                    'type_list'     =>$reasonList,
                    'params'        =>$params
            ]);
        }
        if(!empty($reason_ids)){
            $params['reason_ids'] = $reason_ids = implode($reason_ids,',');
        }
        //订单异常统计数据
        $result = StatisticsLogic::getAbnormalOrders($start_time,$end_time,$reason_ids);
        //格式化所选产品类目
        $type_infos = StatisticsLogic::getReasonType($reason_ids);
        if (Yii::$app->request->isAjax) {
            return json_encode($result['show_data'],true);
        }
        //数据导出
        if(!empty($export) && !empty($result['list'])){
            $title = $sheet_title = '异常订单概览'.$params['start_time'].'至'.$params['end_time'];
            $dataList = $result['list'];
            $setTitle = $result['type_total'];
            StatisticsLogic::setExport2($setTitle,$dataList,$title,$sheet_title);
        }
        //渲染输出
        return $this->render('abnormal-orders', [
                'data_list'     =>$result['list'],
                'type_total'   =>$result['type_total'],
                'type_infos'   =>$type_infos,
                'type_list'    =>$reasonList,
                'params'       =>$params
        ]);
    }


}


<?php
namespace webapp\controllers;
/**
 * @desc:采购退货
 */
use common\helpers\Helper;
use Yii;
class ReturnPurchaseController extends BaseController
{
    public function actionCmd(){
        $str = "3901,3902,3903,3913,3933,3934,3935,3936,3937,3938,3939,3940,3974,3975,3976,3977,3978,3979,3980,3981,3982,3983,3985,3986,3988,3989,3992,3994,3996,3997,3998,3999,4001,4002,4003,4004,4005,4006,4007,4008,4009,4010,4011,4012,4013,4014,4061,4098,4099,4108,4109,4421,4435,4458,4477,4485,4488,4509,4525,4559,4576,4667,4702,4703,4852,4853,4854,4855,4856,4857,4858,4859,4860,4861,4862,4863,4864,4865,4866,4867,4868,4869,4870,4871,4872,4873,4874,4875,4876,4877,4878,4879,4880,4881,4882,4883,4884,4885,4886,4887,4888,4889,4890,4891,4892,4893,4894,4895,4896,4897,4898,4899,4900,4901,4902,4903,4904,4905,4906,4907,4908,4909,4910,4911,4912,4913,4914,4915,4916,4917,4918,4919,4920,4921,4922,4923,4924,4925,4926,4927,4928,4929,4930,4931,4932,4933,4934,4935,4936,4937,4938,4939,4940,4941,4942,4943,4944,4945,4946,4947,4948,4949,4950,4951,4952,4953,4954,4955,4956,4957,4958";
        $accountIds = explode(",",$str);
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/stastic";
        $postData = [
            'account_ids'       => $accountIds,
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        echo $jsonStr;exit;
        $jsonArr = json_decode($jsonStr,true);

    }
    // 添加
    public function actionAdd()
    {
        return $this->render('add');
    }
    // 列表
    public function actionIndex()
    {
        return $this->render('index');
    }
    // 详情
    public function actionView()
    {
        return $this->render('view');
    }
}
?>
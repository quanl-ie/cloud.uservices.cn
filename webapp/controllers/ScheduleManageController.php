<?php

namespace webapp\controllers;

use common\helpers\Helper;
use webapp\logic\BaseLogic;
use Yii;
use webapp\logic\ScheduleManageLogic;
use common\helpers\Paging;
use webapp\models\Department;

/**
 *@desc:排班管理
 * @author:
 * createdAt: 2018/5/14
 *
 */


class ScheduleManageController extends BaseController
{

    public function actionIndex()
    {
        //生成查询条件
        $result = ScheduleManageLogic::index($this->dataAuthId);

        //分页拼成数据
        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }
        if($result['totalPage'] >=1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], $getStr."&page=");
        }

        //获取当前用户的登陆信息
        $user_info = BaseLogic::getLoginUserInfo();


        //本公司及以下权限赋予本公司权限
        if($this->dataAuthId==3){
            $this->dataAuthId=2;
        }

        //生成查询条件
        $info = self::departmentforSearch();

        //搜索选择框的技师数据
       $result1 = ScheduleManageLogic::index($this->dataAuthId);


        $params['department_id']      =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']        = $params;
        $this->_view['department']    = $info;
        $this->_view['auth']          = $this->dataAuthId;
        $this->_view['company_id']    = $this->directCompanyId;
        $this->_view['data']          = $result['list'];
        $this->_view['data1']         = $result1['list'];
        $this->_view['department_id'] = $user_info->department_id;
        $this->_view['group']         = $result['group'];
        $this->_view['dataAuth']      = $this->dataAuthId;
        $this->_view['pageHtml']      = isset($pageHtml)?$pageHtml:' ';
        $this->_view['departpment_name']= isset($result['departpment_name']) ? $result['departpment_name'] : '';
        return $this->render('index');

    }

    /**
     * @return string
     * 数据到出
     */
    public function actionExportData()
    {
        //生成查询条件
        $result = ScheduleManageLogic::index($this->dataAuthId);

        //分页拼成数据
        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }
        if($result['totalPage'] >=1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], $getStr."&page=");
        }

        //获取当前用户的登陆信息
        $user_info = BaseLogic::getLoginUserInfo();


        //本公司及以下权限赋予本公司权限
        if($this->dataAuthId==3){
            $this->dataAuthId=2;
        }

        //生成查询条件
        $info = self::departmentforSearch();

        //搜索选择框的技师数据
        $result1 = ScheduleManageLogic::index($this->dataAuthId);


        $params['department_id']      =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']        = $params;
        $this->_view['department']    = $info;
        $this->_view['auth']          = $this->dataAuthId;
        $this->_view['company_id']    = $this->directCompanyId;
        $this->_view['data']          = $result['list'];
        $this->_view['data1']         = $result1['list'];
        $this->_view['department_id'] = $user_info->department_id;
        $this->_view['group']         = $result['group'];
        $this->_view['dataAuth']      = $this->dataAuthId;
        $this->_view['pageHtml']      = isset($pageHtml)?$pageHtml:' ';
        $this->_view['departpment_name']= isset($result['departpment_name']) ? $result['departpment_name'] : '';
        return $this->render('index');

    }
    
    /**
     * 获取机构分组
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/8/14
     * Time: 17:01
     * @return array
     */
    public function actionGetGroup()
    {
        $res = ScheduleManageLogic::getGroup();
        return json_encode($res);

    }

}


<?php

namespace webapp\controllers\api;

use common\helpers\Helper;
use Yii;
class AppraisalController extends BaseController
{

    /**
     * 根据星级查出标签
     * @author xi
     */
    public function actionGetTag()
    {
        $starNum = intval(Yii::$app->request->post('star_num',0));
        $workNo  = trim(Yii::$app->request->post('work_no',''));

        if($workNo == ''){
            return $this->error("工单号不能为空",20002);
        }
        if($starNum == 0){
            return $this->error("参数 star_num 不能为空",20003);
        }

        $star = 0;
        if(in_array($starNum,[1,2])){
            $star = 3;
        }
        else if(in_array($starNum,[3,4])){
            $star = 2;
        }
        else if($starNum == 5){
            $star = 1;
        }

        $postData = [
            'work_no' => $workNo,
            'star'    => $star
        ];
        $url = Yii::$app->params['order.uservices.cn']."/v3/appraisal/get-tag";
        $jsonStr = Helper::curlPost($url,$postData);
        $result = json_decode($jsonStr,true);
        if(isset($result['success']) && $result['success'] == 1)
        {
            return $this->success($result['data']);
        }
        return $this->error("未找到数据",20004);
    }

    /**
     * 发布评价
     * @return array
     * @author xi
     */
    public function actionAdd()
    {
        $params         = Yii::$app->request->post('params');

        $postData = [
            'params'    => $params,
            'user_id'   => Yii::$app->user->getId(),
            'user_type' => 2,
        ];

        $url = Yii::$app->params['order.uservices.cn']."/v3/appraisal/add";
        $jsonStr = Helper::curlPostJson($url,$postData);
        $result = json_decode($jsonStr,true);
        if(isset($result['success']) && $result['success'] == 1)
        {
            return $this->success($result['data']);
        }
        return $this->error($result['message'],$result['code']);
    }

}

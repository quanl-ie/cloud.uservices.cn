<?php
namespace webapp\controllers\api;

use webapp\models\Department;
use webapp\models\Grouping;
use webapp\models\TechnicianSignIn;
use webapp\models\TechnicianSignInCategory;
use Yii;
use yii\db\Query;

class SignInController extends BaseController
{

    /**
     * 打卡列表
     * @return array
     */
    public function actionList()
    {
        $beginTime    = trim(Yii::$app->request->post('begin_time',''));
        $endTime      = trim(Yii::$app->request->post('end_time',''));
        $departmentId = intval(Yii::$app->request->post('department_id',0));
        $groupId      = intval(Yii::$app->request->post('group_id',0));
        $technicianId = intval(Yii::$app->request->post('technician_id',0));
        $category     = intval(Yii::$app->request->post('category',0));
        $page         = intval(Yii::$app->request->post('page',1));
        $pageSize     = intval(Yii::$app->request->post('pagesize',20));
        $technicianName = trim(Yii::$app->request->post('technician_name',''));

        $where = [];
        // 1本人 2本公司 3本公司及以下 4本部门 5本部门及以下
        if($this->dataAuthId == 1){
            $where['a.direct_company_id'] = -9999;
        }
        else if($this->dataAuthId == 2 || $this->dataAuthId == 3){
            $where['a.direct_company_id'] = $this->directCompanyId;
        }
        else if($this->dataAuthId == 4){
            if($departmentId == 0) {
                $where['a.department_id'] = $this->departmentId;
            }
        }
        else if($this->dataAuthId == 5){
            if($departmentId == 0) {
                $childDepartmentIds = $this->getChildrenDepartmentIds($this->departmentId);
                $departmentIds = array_merge([$this->departmentId], $childDepartmentIds);
                $where[] = ['a.department_id', 'in', $departmentIds];
            }
        }

        $where[] = ['a.type' ,'in', [1,2,3,5]];


        $whereGj = [];
        $jcSql = '';
        
        //技师姓名，模糊搜索
        if($technicianName != ''){
            $jcSql = "b.name like '%$technicianName%'";
        }

        if($beginTime != ''){
            $whereGj[] = ['a.create_time', '>=', strtotime($beginTime)];
        }
        if($endTime){
            $whereGj[] = ['a.create_time', '<', strtotime($endTime)+86400];
        }
        if($departmentId > 0){
            $whereGj[] = ['a.department_id', '=', $departmentId];
        }
        if($groupId > 0 ){
            $whereGj[] = ['d.grouping_id', '=', $groupId];
        }
        else if($groupId == -1){
            $data = TechnicianSignIn::getDefaultTechnician($departmentId);
            if($data){
                $technicianIds = array_column($data,'id');
                $whereGj[] = ['a.technician_id','in',$technicianIds];
            }
            else {
                $whereGj[] = ['a.technician_id','=', -1];
            }
        }
        if($technicianId > 0){
            $whereGj[] = ['a.technician_id', '=', $technicianId];
        }
        if($category > 0){
            $whereGj[] = ['a.category','=',$category];
        }

        $gjSql = '';
        if($whereGj)
        {
            $q = new Query();
            foreach ($whereGj as $val){
                $q->andWhere([$val[1],$val[0],$val[2]]);
            }
            $gjSql = str_replace('SELECT * WHERE','',$q->createCommand()->rawSql);
        }

        $andWhere = '';
        if($jcSql != '' && $gjSql != ''){
            $andWhere = $jcSql . ' or ('. $gjSql .')';
        }
        else if($jcSql != '' ){
            $andWhere = $jcSql;
        }
        else if($gjSql != ''){
            $andWhere = $gjSql;
        }

        $data = TechnicianSignIn::getList($where,$andWhere,$this->directCompanyId,$page,$pageSize);
        return $this->success($data);
    }

    /**
     * 打卡搜索项
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionSearchOption()
    {
        $type         = intval(Yii::$app->request->post('type',0));
        $departmentId = intval(Yii::$app->request->post('department_id',0));
        $groupId      = intval(Yii::$app->request->post('grouping_id',0));

        if($type == 1){
            return $this->getDepartment();
        }
        else if($type == 2)
        {
            if($departmentId == 0){
                return $this->success([]);
            }
            return $this->getGrouping($departmentId);
        }
        else if($type == 3)
        {
            if($departmentId == 0){
                return $this->success([]);
            }
            if($groupId == 0){
                return $this->success([]);
            }
            return $this->getTechnician($departmentId,$groupId);
        }
    }

    /**
     * 获取部门数据
     * @return array
     */
    private function getDepartment()
    {
        $where = [
            'status'     => 1,
            'del_status' => 1
        ];

        // 1本人 2本公司 3本公司及以下 4本部门 5本部门及以下
        if($this->dataAuthId == 1){
            $where['direct_company_id'] = -9999;
        }
        else if($this->dataAuthId == 2 || $this->dataAuthId == 3){
            $where['direct_company_id'] = $this->directCompanyId;
        }
        else if($this->dataAuthId == 4){
            $where['id'] = $this->departmentId;
        }
        else if($this->dataAuthId == 5){
            $where = "(FIND_IN_SET(".$this->departmentId.",parent_ids) and type= 3 and status = 1 and del_status = 1) or (id =  ".$this->departmentId ." and status = 1 and del_status = 1)";
        }

        $data = Department::findAllByAttributes($where,'id,name');
        return $this->success($data);
    }

    /**
     * 获取分组信息
     * @param $departmentId
     * @return array
     */
    private function getGrouping($departmentId)
    {
        $where = [
            'department_id' => $departmentId,
            'status' => 1
        ];
        $data = Grouping::findAllByAttributes($where,'id,name');

        array_unshift($data,['id'=> -1, 'name'=> '默认组']);
        return $this->success($data);
    }

    /**
     * 获取技师
     * @param $departmentId
     * @param $groupingId
     * @return array|\yii\db\ActiveRecord[]
     */
    private function getTechnician($departmentId,$groupingId)
    {
        if($groupingId == -1){
            $data = TechnicianSignIn::getDefaultTechnician($departmentId);
        }
        else {
            $data = TechnicianSignIn::getTechnicianByGroupingId($departmentId,$groupingId);
        }
        return $this->success($data);
    }

    /**
     * 获取分类
     * @return array
     */
    public function actionCategory()
    {
        $where = [
            'direct_company_id' => $this->directCompanyId,
        ];
        $category = TechnicianSignInCategory::findAllByAttributes($where,"c_id,title");
        return $this->success($category);
    }

}
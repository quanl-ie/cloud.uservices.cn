<?php
/**
 * Created by PhpStorm.
 * User: quanl
 * Date: 2018/9/21
 * Time: 10:21
 */
namespace webapp\controllers\api;
use common\helpers\Helper;
use common\models\ServiceType;
use webapp\models\Department;
use webapp\models\DismantlingConfig;
use webapp\models\Technician;
use Yii;
class FeeController extends BaseController
{
    /**
     * 列表
     * @param $status
     * @return array
     */
    public function actionIndex()
    {
        $auth = $this->dataAuthId;
        $top_id = $this->directCompanyId;
        $type   = $this->departmentObj->type;
        $department_id = $this->departmentId;

        $res  = DismantlingConfig::hasEnable($top_id,2);
        if(!$res){
            return self::error("此模块未开启");
        }
        $workNo      = trim(Yii::$app->request->post('work_no',''));
        $tecName     = trim(Yii::$app->request->post('tec_name',''));
        $tecMobile   = trim(Yii::$app->request->post('tec_mobile',''));
        $serviceType = intval(Yii::$app->request->post('service_type',0));
        $startFeeTime     = trim(Yii::$app->request->post('start_fee_time',0));
        $endFeeTime      = trim(Yii::$app->request->post('end_fee_time',0));
        $status      = intval(Yii::$app->request->post('status',0));
        $page        = intval(Yii::$app->request->post('page',1));
        $pageSize    = intval(Yii::$app->request->post('pagesize',10));

        $department_ids = [];
        if($auth == 3){
            //获取上级及以下所有机构；
            $department_info = Department::getDepartmentChildren($top_id,3,0);
            foreach ($department_info as $val){
                $department_ids[] = $val['id'];
            }
        }else if($auth == 2){
            //获取上级及以下部门
            $department_info = department::getDepartmentChildren($top_id,0,1);
            foreach ($department_info as $val){
                $department_ids[] = $val['id'];
            }
        }else if($auth == 5){
            if($type == 3){
                //获取部门以下部门
                $department_info = department::getDepartmentChildren($department_id,2);
                foreach ($department_info as $val){
                    $department_ids[] = $val['id'];
                }

            }else{
                $department_ids = '-1';
            }
        }else if($auth == 4){
            if($type == 3){
                //获取部门
                $department_ids[] = $department_id;
            }else{
                $department_ids = '-1';
            }

        }else{
            $department_ids = '-1';
        }
        //本人权限 不能查看数据
        $param = [];
        $param['status'] = $status;
        if($workNo != '')
        {
            $param['work_no'] = $workNo;
        }
        if($serviceType != '')
        {
            $param['service_type'] = $serviceType;
        }
        if($startFeeTime != '')
        {
            $param['start_fee_time'] = $startFeeTime;
        }
        if($endFeeTime != '')
        {
            $param['end_fee_time'] = $endFeeTime;
        }
        $tecIds = [];
        if($tecName != '')
        {
            $param['tec_name'] = $tecName;
            $res = Technician::findAllByAttributes(['=','name',$tecName],'id');
            if($res){
                $tecIds = array_column($res, 'id');
            }else{
                $tecIds = [-1];
            }
        }
        if($tecMobile !=''){
            $param['tec_mobile'] = $tecMobile;
            $res = Technician::findAllByAttributes(['=','mobile',$tecMobile],'id');
            if($res && $tecName != ''){
                $tecIds = array_intersect($tecIds,array_column($res, 'id'));
            }else{
                $tecIds = array_column($res, 'id');
            }
            if(empty($tecIds)){
                $tecIds = [-2];
            }
        }
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/fee-list";
        $postData = [
            'department_ids' =>$department_ids,
            'work_no'       => $workNo,
            'tec_ids'       => $tecIds,
            'service_type'   => $serviceType,
            'status'         => $status,
            'start_fee_time'=>$startFeeTime,
            'end_fee_time'  =>$endFeeTime,
            'page'           => $page,
            'pageSize'       => $pageSize
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        //服务类型
        $serviceTypeList = ServiceType::findAllByAttributes(['department_id'=>$this->departmentId],"id,title");
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            if(isset($jsonArr['data']['list']) && !empty($jsonArr['data']['list']))
            {
                //查询技师名字
                $tecRelIds = array_column($jsonArr['data']['list'],'technician_id');
                $tecName = Technician::findAllByAttributes(['id'=>$tecRelIds],"name,id",'id');
                //查询服务类型名字
                $typeIds  = array_column($jsonArr['data']['list'],"work_type");
                $typeName = ServiceType::findAllByAttributes(['id'=>$typeIds],'title,id','id');
                foreach ($jsonArr['data']['list'] as $k=>$v)
                {
                    $jsonArr['data']['list'][$k]['cost_amount'] = sprintf("%.2f",$v['cost_amount']);

                    $jsonArr['data']['list'][$k]['cost_real_amount'] = sprintf("%.2f",$v['cost_real_amount']);

                    $jsonArr['data']['list'][$k]['create_time'] = date("Y-m-d H:i:s",$v['create_time']);

                    if(isset($tecName[$v['technician_id']])) {
                        $jsonArr['data']['list'][$k]['technician_name'] = $tecName[$v['technician_id']]['name'];
                    }
                    if(isset($typeName[$v['work_type']])) {
                        $jsonArr['data']['list'][$k]['type_name'] = $typeName[$v['work_type']]['title'];
                    }
                }
            }
            $jsonArr['data']['param'] = $param;
            $jsonArr['data']['type_list'] = $serviceTypeList;
            return self::success($jsonArr['data']);
        }else{
            return self::error("接口错误：$url 没有返回数据");
        }
    }
    public function actionConfirm()
    {
        $workNo  = trim(Yii::$app->request->post('work_no',''));
        if($workNo != ''){
            $url = Yii::$app->params['order.uservices.cn']."/v1/order/fee-confirm";
            $postData = [
                'work_no'       => $workNo
            ];
            $jsonStr = Helper::curlPostJson($url, $postData);
            $jsonArr = json_decode($jsonStr,true);
            if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
            {
                return self::success($workNo);
            }else{
                return self::error("接口错误：$url 没有返回数据");
            }
        }
        return self::error("参数错误");
    }
    public function actionEditRemark()
    {
        $workNo  = trim(Yii::$app->request->post('work_no',''));
        $remark  = trim(Yii::$app->request->post('remark',''));
        if($workNo != ''){
            $url = Yii::$app->params['order.uservices.cn']."/v1/order/edit-remark";
            $postData = [
                'work_no'       => $workNo,
                'remark'        => $remark
            ];
            $jsonStr = Helper::curlPostJson($url, $postData);
            $jsonArr = json_decode($jsonStr,true);
            if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
            {
                return self::success($workNo);
            }else{
                return self::error("接口错误：$url 没有返回数据");
            }
        }
        return self::error("参数错误");
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: quanl
 * Date: 2018/10/17
 * Time: 9:25
 */
namespace webapp\controllers\api;
use common\models\WorkAppraisalDict;
use webapp\models\Technician;
use webapp\models\TechnicianAppraisal;
use Yii;
/**
 * @desc:技师评价统计接口
 */
class TechnicianAppraisalController extends BaseController
{
    /**
     * 获取机构评价设置
     */
    public function actionGetTags(){
        $data = WorkAppraisalDict::getAllTags(['direct_company_id'=>$this->directCompanyId]);
        return $this->success($data);
    }
    /**
     * 列表
     * @author Eva <chengjuanjuan@c-ntek.com>
     */
    public function actionIndex()
    {
        $technicianName = trim(Yii::$app->request->post('technician_name',''));
        $date = trim(Yii::$app->request->post('month',''));
        $page        = intval(Yii::$app->request->post('page',1));
        $pageSize    = intval(Yii::$app->request->post('pagesize',10));
        $timestamp=time();
        //默认查询上个月
        $month=date('Ym',strtotime(date('Y',$timestamp).'-'.(date('m',$timestamp)-1).'-01'));
        //搜索条件
        if($technicianName){
            $technician = Technician::findOneByAttributes(['like','name',$technicianName],'id');
            if(empty($technician)){
                $technicianId = -1;
            }else{
                $technicianId = $technician['id'];
            }
            $where['technician_id'] = $technicianId;
        }
        if($date){
            $month = date("Ym",strtotime($date));
        }
        $where['month'] = $month;
        //默认为公司权限
        $where['direct_company_id'] = $this->directCompanyId;
        $res = TechnicianAppraisal::getList($where,[],$page,$pageSize);
        return $this->success($res);
    }
}
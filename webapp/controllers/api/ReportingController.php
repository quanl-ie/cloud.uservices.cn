<?php
/**
 * Created by PhpStorm.
 * User: quanl
 * Date: 2018/11/6
 * Time: 14:11
 */
namespace webapp\controllers\api;

use common\models\DictEnum;
use common\models\ServiceType;
use webapp\logic\SendOutLogic;
use webapp\models\Apll;
use webapp\models\ApllDetail;
use webapp\models\Department;
use webapp\models\Region;
use webapp\models\ReportingAccountOrder;
use webapp\models\ReportingCompanyOrder;
use webapp\models\ReportingTechnicianOrder;
use Yii;
/**
 * @desc:报表管理接口
 */
class ReportingController extends BaseController
{
    /**
     * 组织机构
     */
    public function actionDepartmentList(){
        $res = Department::findAllByAttributes(['direct_company_id'=>$this->directCompanyId,'status'=>1,'del_status'=>1],'id,name');
        return $this->success($res);
    }
    /**
     * 服务类型
     */
    public function actionServerTypeList(){
        $res = ServiceType::findAllByAttributes(['department_id'=>$this->directCompanyId,'status'=>1,'del_status'=>1],'id,title');
        return $this->success($res);
    }
    /**
     * 平台派单量统计
     */
    public function actionCompanyOrder()
    {
        $page     = intval(Yii::$app->request->post('page',1));
        $pageSize = intval(Yii::$app->request->post('pageSize',10));
        $date      = trim(Yii::$app->request->post('date',''));
        $companyId = intval(Yii::$app->request->post('company_id',0));
        $workType  = intval(Yii::$app->request->post('work_type',0));
        if($companyId) {
            $where[] = ['=','department_id',$companyId];
        }else{
            $where[] = ['=','direct_company_id',$this->directCompanyId];
        }
        if($workType) {
            $where[] = ['=','work_type',$workType];
        }
        if(isset($date) && $date != '') {
            $where[]=['>=','date',date("Y-m-01",strtotime($date))];
            $where[]=['<=','date',date("Y-m-t",strtotime($date))];
        }else{
            $where[]=['>=','date',date("Y-m-01",strtotime('-1 month'))];
            $where[]=['<=','date',date("Y-m-t",strtotime('-1 month'))];
        }
        $data = ReportingCompanyOrder::getList($where,$page,$pageSize);
        return $this->success($data);
    }
    /**
     * 技师接单统计
     */
    public function actionTechnicianOrder()
    {
        $page     = intval(Yii::$app->request->post('page',1));
        $pageSize = intval(Yii::$app->request->post('pageSize',10));
        $date      = trim(Yii::$app->request->post('date',''));
        $companyId = intval(Yii::$app->request->post('company_id',0));
        $workType  = intval(Yii::$app->request->post('work_type',0));
        $name = trim(Yii::$app->request->post('name',''));
        if($companyId) {
            $where[] = ['=','department_id',$companyId];
        }else{
            $where[] = ['=','direct_company_id',$this->directCompanyId];
        }
        if($workType) {
            $where[] = ['=','work_type',$workType];
        }
        if(isset($name) && $name != '') {
            $where[] = ['or',['like','technician_name',$name],['like','technician_mobile',$name]];
        }
        if(isset($date) && $date != '') {
            $where[]=['>=','date',date("Y-m-01",strtotime($date))];
            $where[]=['<=','date',date("Y-m-t",strtotime($date))];
        }else{
            $where[]=['>=','date',date("Y-m-01",strtotime('-1 month'))];
            $where[]=['<=','date',date("Y-m-t",strtotime('-1 month'))];
        }
        $data = ReportingTechnicianOrder::getList($where,$page,$pageSize);
        return $this->success($data);
    }
    /**
     * 客户派单量统计
     */
    public function actionAccountOrder()
    {
        $page     = intval(Yii::$app->request->post('page',1));
        $pageSize = intval(Yii::$app->request->post('pageSize',10));
        $date      = trim(Yii::$app->request->post('date',''));
        $workType  = intval(Yii::$app->request->post('work_type',0));
        $name  = trim(Yii::$app->request->post('name',''));
        $where[] = ['=','direct_company_id',$this->directCompanyId];
        if($workType) {
            $where[] = ['=','work_type',$workType];
        }
        if(isset($name) && $name != '') {
            $where[] = ['or',['like','account_name',$name],['like','account_mobile',$name]];
        }
        if(isset($date) && $date != '') {
            $where[]=['>=','order_create_time',strtotime(date("Y-m-01",strtotime($date)))];
            $where[]=['<=','order_create_time',strtotime(date("Y-m-t",strtotime($date)))];
        }else{
            $where[]=['>=','order_create_time',strtotime(date("Y-m-01",strtotime('-1 month')))];
            $where[]=['<=','order_create_time',strtotime(date("Y-m-t",strtotime('-1 month')))];
        }
        $data = ReportingAccountOrder::getList($where,$page,$pageSize);
        return $this->success($data);
    }
}
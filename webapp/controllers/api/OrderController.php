<?php

namespace webapp\controllers\api;

use common\helpers\ExcelHelper;
use common\lib\Push;
use common\models\Contract;
use common\models\Region;
use common\helpers\Helper;
use common\models\WorkRelTechnician;
use PHPUnit\Util\Log\JSON;
use webapp\logic\AccountLogic;
use webapp\logic\ContractLogic;
use webapp\logic\DepartmentLogic;
use webapp\logic\FlowLogic;
use webapp\logic\ServiceTypeLogic;
use webapp\logic\WorkLogic;
use webapp\models\BaseModel;
use webapp\models\Department;
use webapp\models\DismantlingConfig;
use webapp\models\Grouping;
use webapp\models\GroupingRelTechnician;
use webapp\models\ManufactorType;
use webapp\models\Order;
use webapp\models\OrderForm;
use webapp\models\SaleOrder;
use webapp\models\SaleOrderSn;
use webapp\models\SaleOrderView;
use webapp\models\TechnicianMode;
use webapp\models\TechnicianSkill;
use webapp\models\WorkOrder;
use webapp\models\ZbDepartmentConfig;
use Yii;
use webapp\logic\OrderLogic;
use common\helpers\Paging;
use webapp\logic\BaseLogic;
use webapp\models\AccountAddress;
use webapp\models\Account;
use webapp\models\CooporationServiceProvider;
use webapp\models\ServiceType;
use webapp\models\BrandQualification;
use webapp\models\ServiceTypeQualification;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use webapp\logic\ProductLogic;
use common\models\ServiceClass;
use common\models\ServiceFlow;
use common\models\Common;
class OrderController extends BaseController
{
    
    /**
     * 待评价工单
     * @author sxz
     * @date 2018-10-11
     */
    public function actionWaitAppraisal()
    {
        $result = OrderLogic::getList(1);
        foreach ($result['data']['list'] as $key=>$val){
            $sever_top_id = \common\models\Department::getDirectCompanyId($val['service_department_id']);
            $result['data']['list'][$key]['sever_direct_id'] = $sever_top_id;
        }
        $data = $result['data'];
        return $this->success($data);
    }
    /**
     *  @return array
     * 添加评价,获取工单信息
     * 2018 10-15 sxz
     */
    public function actionBeforeAppraisalAdd()
    {
        $workNo       = trim(Yii::$app->request->post('work_no',''));
        $departmentId = intval(Yii::$app->request->post('department_id',0));

        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/detail";
        $postData = [
            'work_no'          => $workNo,
            'department_id'    => $departmentId
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        $resData = [];
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            $data = $jsonArr['data'];
            if($data)
            {
                $resData['order_no']    = $data['order_no'];
                $resData['work_no']     = $data['work_no'];
                $resData['status_desc'] = $data['status_desc'];
                //服务类型
                $workTypeArr = BaseLogic::getTypeList();
                if(isset($workTypeArr['data'][$data['order_info']['work_type']])){
                    $resData['work_type_desc'] = $workTypeArr['data'][$data['order_info']['work_type']];
                }
                //服务流程
                $workStageRes = ServiceFlow::findOne(['id'=>$data['work_stage'],'status'=>1]);
                $resData['work_stage_desc'] = isset($workStageRes['title'])?$workStageRes['title']:'';

                //服务技师
                $jsIds = array_column($data['technicianArr'],'technician_id');
                if($jsIds)
                {
                    $technician_info = Common::getTechniciansInfo($jsIds,'id,name,mobile,icon,store_id');
                    if($technician_info)
                    {
                        //服务机构
                        $oneJs = reset($technician_info);
                        $serviceDepartment = Department::findOne(['id'=>$oneJs]);
                        $resData['service_department_name'] = isset($serviceDepartment['name'])?$serviceDepartment['name']:'';
                        foreach ($data['technicianArr'] as $key=>$jVal)
                        {
                            $avatar = $name = $mobile = '';
                            if(isset($technician_info[$jVal['technician_id']]))
                            {
                                $name   = $technician_info[$jVal['technician_id']]['name'];
                                $mobile = $technician_info[$jVal['technician_id']]['mobile'];
                                $avatar = $technician_info[$jVal['technician_id']]['icon'];
                            }
                            $title = '协助';
                            if($jVal['is_self'] == 1){
                                $title = '负责人';
                            }
                            else if($jVal['is_self'] == 2){
                                $title = '组长';
                            }
                            $data['technicianArr'][$key]['name']   = $name;
                            $data['technicianArr'][$key]['mobile'] = $mobile;
                            $data['technicianArr'][$key]['avatar'] = trim($avatar)==''?'/images/morentouxiang.png':$avatar;
                            $data['technicianArr'][$key]['title']  = $title;
                        }
                    }
                    else
                    {
                        foreach ($data['technicianArr'] as $key=>$jVal)
                        {
                            $data['technicianArr'][$key]['name']   = '';
                            $data['technicianArr'][$key]['mobile'] = '';
                            $data['technicianArr'][$key]['avatar'] = '/images/morentouxiang.png';
                            $data['technicianArr'][$key]['title']  = '';
                        }
                    }
                }
                $resData['technicianArr'] = $data['technicianArr'];
                $resData['finsh_service_time'] = isset($data['finsh_service_time'])?date('Y-m-d H:i:s',$data['finsh_service_time']):'';
                return $this->success($resData);
            }
            return $this->error('获取数据失败');
        }
        return $this->error('获取数据失败');

    }


    /**
     * 售后产品查看（修改前用）
     * @return Response
     * @author xi
     */
    public function actionProductView()
    {
        $result = [];

        $orderNo       = trim(Yii::$app->request->post('order_no', ''));
        $saleOrderId   = intval(Yii::$app->request->post('id', ''));
        $saleOrderSnId = intval(Yii::$app->request->post('snid', ''));

        if($orderNo == ''){
            return $this->error('订单号不能为空', 20002);
        }
        if($saleOrderId <= 0){
            return $this->error('产品 id 不能为空', 20003);
        }

        $select = 'id,prod_id,prod_no,model,process,intention_type,prod_name,prod_name_wechat,product_type_id,brand_name,brand_name_wechat,class_name,prod_series,serial_number,produce_time,buy_time,warranty,code,warranty_num,warranty_type,is_scope,scope_img,source,info';
        $product = SaleOrderView::findOneByAttributes(['id' => $saleOrderId], $select);

        //获取产品信息
        $saleOrderArr = $this->getOrderProduct($orderNo,$saleOrderId);
        $scopeArr = $this->recombineSaleOrder($saleOrderArr);

        //获取序列号等
        $snArr = [];
        if($saleOrderSnId) {
            $snArr = SaleOrderSn::findOneByAttributes(['id' => $saleOrderSnId], 'sn,remark,remark,extend_data');
        }

        $extendData = SaleOrderSn::getDynaactionformData($this->departmentId,(isset($snArr['extend_data']) ? json_decode($snArr['extend_data'],true) : []),$orderNo);

        if($product)
        {
            $result = [
                'order'         => $orderNo,
                'id'            => $saleOrderId,
                'snid'          => $saleOrderSnId,
                'prodNo'        => $product['prod_no'],
                'intentionType' => $product['intention_type'] == 1 ? '客户产品' : '意向产品',
                'prodName'      => $product['prod_name'],
                'brandName'     => $product['brand_name'],
                'model'         => $product['model'],
                'buyTime'       => $product['buy_time'] > 0? date('Y-m-d', $product['buy_time']) :'',
                'produceTime'   => $product['produce_time'] > 0 ? date('Y-m-d', $product['produce_time']):'' ,
                'scope'         => $scopeArr['scope'],
                'scopeNum'      => SaleOrder::getWarrant($product['warranty_num'],$product['warranty_type']),
                'sn'            => $snArr ? $snArr['sn'] : $product['serial_number'],
                'remark'        => $snArr ? $snArr['remark'] : $product['info'],
                'scope_img'     => $scopeArr['scope_img'],
                'extend_data'   => $extendData,
            ];
        }

        return $this->success($result);
    }

    /**
     * 获取产品信息
     * @param $orderNo
     * @param $saleOrderId
     * @return array
     * @author xi
     */
    private function getOrderProduct($orderNo,$saleOrderId)
    {
        $postData = [
            'order_no'   => $orderNo,
            'product_id' => $saleOrderId
        ];
        $url = Yii::$app->params['order.uservices.cn'] . '/v2/order/get-order-product';
        $jsonStr = Helper::curlPostJson($url,$postData);
        $result  = json_decode($jsonStr,true);
        if(isset($result['success']) && $result['success'] == true)
        {
            return $result['data'];
        }

        return [];
    }

    /**
     * 重组产品信息
     * @param array $saleOrderArr
     * @return array
     * @author xi
     */
    private function recombineSaleOrder(array $saleOrderArr)
    {
        $result = [
            'scope'     => '协商',
            'scope_img' => []
        ];
        if($saleOrderArr)
        {
            $result['scope']     = BaseModel::getAmountType($saleOrderArr['is_scope']);
            $result['scope_img'] = $saleOrderArr['scope_img'] != '' ? explode(',',$saleOrderArr['scope_img']) : [];
        }

        return $result;
    }

    /**
     * 编辑产品
     * @author xi
     */
    public function actionEditProduct()
    {
        $orderNo       = trim(Yii::$app->request->post('order_no', ''));
        $saleOrderId   = intval(Yii::$app->request->post('id', ''));
        $saleOrderSnId = intval(Yii::$app->request->post('snid', ''));
        $sn            = trim(Yii::$app->request->post('sn',''));
        $remark        = trim(Yii::$app->request->post('remark',''));
        $scopeImg      = Yii::$app->request->post('scope_img','');
        $extendData    = Yii::$app->request->post('extend_data','');


        if($orderNo == ''){
            return $this->error('订单号不能为空', 20002);
        }
        if($saleOrderId <= 0){
            return $this->error('产品 id 不能为空', 20003);
        }

        $model = null;
        if($saleOrderSnId > 0){
            $model = SaleOrderSn::findOne(['id' => $saleOrderSnId,'status' => 1]);
        }
        if(!$model)
        {
            $model = new SaleOrderSn();
            $model->update_time   = time();
            $model->order_no      = $orderNo;
            $model->sale_order_id = $saleOrderId;
        }

        $model->sn          = $sn;
        $model->remark      = $remark;
        $model->update_time = time();
        $model->extend_data = json_encode($extendData);
        if($model->save())
        {
            //更新凭证
            $postData = [
                'order_no'   => $orderNo,
                'product_id' => $saleOrderId,
                'scope_img'  => $scopeImg ? implode(',',$scopeImg):''
            ];
            $url = Yii::$app->params['order.uservices.cn'] . '/v2/order/update-order-scope';
            $jsonStr = Helper::curlPostJson($url,$postData);
            $result  = json_decode($jsonStr,true);
            if(isset($result['success']) && $result['success'] == true)
            {
                return $this->success([
                    'order_no' => $orderNo,
                    'id'       => $saleOrderId,
                    'snid'     => $saleOrderSnId
                ]);
            }
        }

        return $this->error('修改失败'.$jsonStr, 20004);
    }


}

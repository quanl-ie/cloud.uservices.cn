<?php
namespace webapp\controllers\api;

use Yii;
use yii\web\Controller;
use webapp\models\Department;
use webapp\logic\BaseLogic;

class BaseController extends Controller
{
    
    public $departmentId = 0;
    //用户信息
    public $user = null;
    //数据权限
    public $dataAuthId = 0;
    //部门数据
    public $departmentObj = null;
    //直属公司id
    public $directCompanyId = 0;


    public function beforeAction($action)
    {
        Yii::$app->request->parsers = [
            'application/json' => 'yii\web\JsonParser',
            'text/json' => 'yii\web\JsonParser',
        ];

        Yii::$app->response->format = 'json';

        if (Yii::$app->user->isGuest) {
            throw new \Exception("please login in",20001);
        }


        $this->user = Yii::$app->user->getIdentity();
        $this->departmentId = $this->user->department_id;

        //部门信息
        $sessionKey = 'departmentObj';
        if(Yii::$app->session->has($sessionKey))
        {
            $this->departmentObj  = Yii::$app->session->get($sessionKey);
            $this->departmentId   =  $this->departmentObj->id;
            $this->directCompanyId = $this->departmentObj->direct_company_id;
        }
        else if($this->departmentId != 0)
        {
            $this->departmentObj = Department::findOne(['id'=>$this->departmentId]);
            $this->directCompanyId = $this->departmentObj->direct_company_id;
            Yii::$app->session->set($sessionKey,$this->departmentObj);
        }

        //数据权限
        $sessionKey = 'dataAuthId';
        if(Yii::$app->session->has($sessionKey)){
            $this->dataAuthId = Yii::$app->session->get($sessionKey);
        }
        else {
            $dataAuthIds = BaseLogic::getTopDataAuth();
            Yii::$app->session->set($sessionKey,$dataAuthIds);
            $this->dataAuthId = $dataAuthIds;
        }

        return true;
    }

    /**
     * 错误处理
     */
    public function error($msg,$code = 20001)
    {
        return [
            'success' => false,
            'message' => $msg,
            'code'    => $code
        ];
    }

    /**
     * 成功处理
     * @param $data
     * @return array
     */
    public function success($data)
    {
        return [
            'success' => true,
            'message' => 'success',
            'code'    => 200,
            'data'    => $data
        ];
    }

    /**
     * 获取子部门id
     * @param $departmentId
     * @return array|int
     */
    public function getChildrenDepartmentIds($departmentId,$type = 3)
    {
        $where = "`status` = 1 and del_status = 1 AND FIND_IN_SET('$departmentId',parent_ids) and type = $type";
        $query = Department::findAllByAttributes($where,'id');
        if($query){
            return array_column($query,'id');
        }
        return [-1];
    }
}

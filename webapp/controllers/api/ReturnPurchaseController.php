<?php
/**
 * Created by PhpStorm.
 * User: quanl
 * Date: 2018/10/10
 * Time: 14:02
 */
namespace webapp\controllers\api;
use common\helpers\CreateNoRule;
use common\models\DictEnum;
use common\models\Purchase;
use common\models\PurchaseDetail;
use common\models\ReturnPurchase;
use common\models\ReturnPurchaseDetail;
use common\models\SendOut;
use common\models\StockDetail;
use common\models\StockInfo;
use webapp\logic\PurchaseLogic;
use webapp\models\Department;
use webapp\models\PurchaseDetailView;
use Yii;
class ReturnPurchaseController extends BaseController
{
    /**
     * List
     * User: quanl
     * @throws \Exception
     */
    public function actionIndex()
    {
        $no         = trim(Yii::$app->request->post('no',''));
        $subject    = trim(Yii::$app->request->post('subject',''));
        $startCreateTime     = trim(Yii::$app->request->post('start_create_time',''));
        $endCreateTime       = trim(Yii::$app->request->post('end_create_time',''));
        $status      = intval(Yii::$app->request->post('status',1));
        $page        = intval(Yii::$app->request->post('page',1));
        $pageSize    = intval(Yii::$app->request->post('pagesize',10));

        $auth = $this->dataAuthId;
        //暂时默认本公司权限
        $auth = 2;
        $top_id = $this->directCompanyId;
        $type   = $this->departmentObj->type;
        $department_id = $this->departmentId;
        $department_ids = [];
        if($auth == 3){
            //获取上级及以下所有机构；
            $department_info = Department::getDepartmentChildren($top_id,3,0);
            foreach ($department_info as $val){
                $department_ids[] = $val['id'];
            }
        }else if($auth == 2){
            //获取上级及以下部门
            $department_info = department::getDepartmentChildren($top_id,0,1);
            foreach ($department_info as $val){
                $department_ids[] = $val['id'];
            }
        }else if($auth == 5){
            if($type == 3){
                //获取部门以下部门
                $department_info = department::getDepartmentChildren($department_id,2);
                foreach ($department_info as $val){
                    $department_ids[] = $val['id'];
                }

            }else{
                $department_ids = $this->user->id;;
            }
        }else if($auth == 4){
            if($type == 3){
                //获取部门
                $department_ids[] = $department_id;
            }else{
                $department_ids = $this->user->id;;
            }

        }else{
            $department_ids = $this->user->id;;
        }
        if(is_array($department_ids)){
            $where['department_id'] = $department_ids;
        }else{
            $where['create_user_id'] = $department_ids;
        }

        $where['no']         = $no;
        $where['subject']   =  $subject;
        $where['startTime'] = $startCreateTime;
        $where['endTime']   = $endCreateTime;
        $where['status']    = $status;
        $where['del_status'] = 2;
        $where['currentPage'] = $page;
        $where['pageSize'] = $pageSize;
        $data = ReturnPurchase::getList($where);
        return $this->success($data);
    }
    /**
     * beforeEdit
     * 获取退货单信息
     */
    public function actionBeforeEdit(){
        $returnId  = intval(Yii::$app->request->post('return_id',''));
        $returnProdIds = [];
        if($returnId){
            $info = ReturnPurchase::findOneByAttributes(['id'=>$returnId]);
            $id = $info['purchase_id'];
            $returnInfo = ReturnPurchaseDetail::findAllByAttributes(['parent_id'=>$returnId],'prod_id,prod_num',"prod_id");
            $returnProdIds = array_column($returnInfo,'prod_id');
        }
        $contract = PurchaseLogic::getDetail($id,1);
        if(!empty($contract)){
            $contractInfo = $contract['purchaseInfo'];
            $contractDetail = $contract['purchaseDetail'];
        }else{
            return $this->error('此退货单异常，暂不支持修改');
        }
        $contractInfo['reason']  = ReturnPurchase::showReasonStatus();

        foreach($contractDetail as $k=>$v){
            if(in_array($v['prod_id'],$returnProdIds)){
                $contractDetail[$k]['finish_num'] = $v['total_num'] = $returnInfo[$v['prod_id']]['prod_num'];
                $v['total_amount'] = sprintf('%0.2f',$v['price']*$v['total_num']);
            }else{
                //@todo  只展示已退货产品  如需展示采购单所有产品 取消下边注释 删除unset
                //$contractDetail[$k]['finish_num'] = 0;
                unset($contractDetail[$k]);
            }
        }

        $contractDetail = array_values($contractDetail); //@todo  //如果展示全部 此行可删除
        $info['purchase_no'] = $contractInfo['no'];
        $data = ['info'=>$info,'purchaseDetail'=>$contractDetail];
        return $this->success($data);
    }
    /**
     *  beforeAdd
     *  User: quanl
     * @throws \Exception
     */
    public function actionBeforeAdd(){

        $id  = intval(Yii::$app->request->post('id',''));

        if($id == '' || $id == 0){
            return $this->error("缺少参数");
        }
        $contract = PurchaseLogic::getDetail($id,1);
        if(!empty($contract)){
            $contractInfo = $contract['purchaseInfo'];
            $contractDetail = $contract['purchaseDetail'];
        }else{
            return $this->error('此采购单异常，暂不支持退货');
        }
        $contractInfo['reason']  = ReturnPurchase::showReasonStatus();
        foreach($contractDetail as $k=>$v){
            if($v['finish_num']==0){
                unset($contractDetail[$k]);
            }else{
                $v['total_num'] = $v['finish_num'];
            }
            $contractDetail[$k]['finish_num'] = intval($v['finish_num']) - intval($v['return_num']);
            $v['total_amount'] = sprintf('%0.2f',$v['price']*$v['total_num']);
        }
        $data = ['purchase_id'=>$contractInfo['id'],'purchase_no'=>$contractInfo['no'],'purchaseDetail'=>$contractDetail];
        return $this->success($data);
    }
    /**
     * Add
     * User: quanl
     * @throws \Exception
     */
    public function actionAdd()
    {
        $id                   = trim(Yii::$app->request->post('return_id',''));
        $subject                   = trim(Yii::$app->request->post('subject',''));
        $purchaseId                = intval(Yii::$app->request->post('purchase_id',0));
        $apply_time                = trim(Yii::$app->request->post('apply_time',''));
        $reason                    = intval(Yii::$app->request->post('reason',0));
        $description                    = trim(Yii::$app->request->post('desc',''));
        if($subject == ''){
            return $this->error('退货主题不能为空',1001);
        }
        if(!$purchaseId){
            return $this->error('没有关联采购单',1002);
        }
        if($apply_time == ''){
            return $this->error('没有选择退货日期',1003);
        }
        if(!$reason){
            return $this->error('没有选择退货原因',1004);
        }
        //退货的产品处理明细
        $purchaseDetail =   yii::$app->request->post('purchase_detail');
        if(empty($purchaseDetail)){
            return $this->error('请选择正确的产品',1005);
        }
        $topId            =  $this->directCompanyId;
        $departmentId     =  $this->departmentId;
        $totalAmount = 0;
        $products = [];
        foreach($purchaseDetail as $k => $v){
            if(isset($v['prod_id'])){
                $products[$k]['department_id']   = $departmentId;
                $products[$k]['department_top_id'] = $topId;
                $products[$k]['prod_id']   = $v['prod_id'];
                $products[$k]['prod_num']   = $v['finish_num'];
                $products[$k]['finish_num']   = 0;
                $totalAmount += $v['price']*$v['prod_num'];
            }
        }

        //基本信息
        $data['department_top_id']  =  $topId;
        $data['department_id']      =   $departmentId;
        $data['subject']            =   $subject;
        $data['return_date']        =   $apply_time;
        $data['return_reason']      =   $reason;
        $data['purchase_id']        =   $purchaseId;
        $data['total_amount']       =   $totalAmount;
        //系统信息

        $data['status']            =  1;
        $data['description']       = $description;
        if($id){ //修改
            $data['id']  = $id;
            $res = ReturnPurchase::edit($data,$products);
        }else{ //添加
            $data['no']                 =   CreateNoRule::getNo('CGTH');
            $data['create_time']       =   date("Y-m-d H:i:s");
            $data['create_user_id']    =   Yii::$app->user->id;
            $res = ReturnPurchase::add($data,$products);
        }
        if ($res) {
            return $this->success(['id' => $res]);
        }
        return $this->error('操作失败');
    }
    /**
     * View
     * User: quanl
     * @throws \Exception
     */
    public function actionView()
    {
        $id = trim(Yii::$app->request->post('id',''));
        if(!$id){
            return $this->error('参数错误');
        }
        //退货单信息
        $info = ReturnPurchase::getOne(['id'=>$id],0,"id,subject,no,purchase_id,return_date,status,return_reason,create_user_id,create_time,description,audit_status,audit_suggest");
        if(empty($info))
        {
            return $this->error('此退货单异常');
        }
        $res = Purchase::findOneByAttributes(['id'=>$info['purchase_id']],'no');
        $info['purchase_no'] = $res['no'];
        if($info['return_reason']){
            $rea = DictEnum::findOneByAttributes(['dict_key'=>'return_purchase_reason','dict_enum_id'=>$info['return_reason']],"dict_enum_value");
            $info['return_reason'] = $rea['dict_enum_value'];
        }
        $info['create_user_name'] = $this->user->username;
        if($info['status'] == 1){
            $info['status_desc'] = '待审批';
        }
        if($info['status'] == 2){
            $info['status_desc'] = '执行中';
        }
        if($info['status'] == 3){
            $info['status_desc'] = '执行完毕';
        }
        if($info['status'] == 4){
            $info['status_desc'] = '审批未通过';
        }
        if($info['status'] == 5){
            $info['status_desc'] = '已终止';
        }
        //退货单产品信息
        $detail = PurchaseDetailView::findAllByAttributes(['stock_id'=>$info['purchase_id']]);
        $returnDetail = ReturnPurchaseDetail::findAllByAttributes(['parent_id'=>$id],'prod_id,prod_num as return_num','prod_id');
        foreach ($detail as $key=>$val)
        {
            if(isset($returnDetail[$val['prod_id']])){
                $returnDetail[$val['prod_id']]['prod_name'] = $val['prod_name'];
                $returnDetail[$val['prod_id']]['prod_no'] = $val['prod_no'];
                $returnDetail[$val['prod_id']]['brand_name'] = $val['brand_name'];
                $returnDetail[$val['prod_id']]['model'] = $val['model'];
                $returnDetail[$val['prod_id']]['purchase_num'] = $val['prod_num'];
                $returnDetail[$val['prod_id']]['purchase_price'] = $val['price'];
                $returnDetail[$val['prod_id']]['purchase_total_price'] = sprintf("%.2f",$val['prod_num']* $val['price']);
                $returnDetail[$val['prod_id']]['return_total_price'] = sprintf("%.2f",$returnDetail[$val['prod_id']]['return_num']* $val['price']);
            }
        }
        //出库记录
        $sendInfo = [];
        $stockInfo = StockInfo::findAllByAttributes(['rel_id'=>$id,'type'=>2,'rel_type'=>7],"id,no,status,create_time,audit_date");
        if($stockInfo)
        {
            //获取出库产品数
            $stockId = array_column($stockInfo,'id');
            $stockDetail = StockDetail::findAllByAttributes(['parent_id'=>$stockId],"parent_id,sum(`prod_num`) as prod_num",'parent_id',1000,'parent_id');
            foreach ($stockInfo as $k=>$v)
            {
                if(isset($stockDetail[$v['id']])){
                    $stockInfo[$k]['prod_num'] = $stockDetail[$v['id']]['prod_num'];
                }
                if($v['status'] == 1){
                    $stockInfo[$k]['status'] = '待审批';
                }
                if($v['status'] == 2){
                    $stockInfo[$k]['status'] = '已出库';
                }
                if($v['status'] == 3){
                    $stockInfo[$k]['status'] = '审批未通过';
                }
            }
            //发货记录
            $sendInfo = SendOut::findAllByAttributes(['stock_id'=>$stockId,'send_source'=>0],"id,stock_id,send_no,send_status,create_time");
            //获取发货产品数
            if($sendInfo)
            {
                foreach ($sendInfo as $k=>$v)
                {
                    if(isset($stockDetail[$v['stock_id']])){
                        $sendInfo[$k]['prod_num'] = $stockDetail[$v['stock_id']]['prod_num'];
                    }
                    if($v['send_status'] == 1){
                        $sendInfo[$k]['send_status'] = '运输中';
                    }
                    if($v['send_status'] == 2){
                        $sendInfo[$k]['send_status'] = '已签收';
                    }
                    unset($sendInfo[$k]['stock_id']);
                }
            }
        }
        $data['info'] = $info;
        $data['product'] = array_values($returnDetail);
        $data['stock'] = $stockInfo;
        $data['send'] = $sendInfo;
        return $this->success($data);
    }
    //审核
    function actionCheckOption(){
        $id = intval(Yii::$app->request->post('id'));
        $auditStatus = intval(Yii::$app->request->post('auditStatus'));
        $auditSuggest = trim(Yii::$app->request->post('auditSuggest'));
        if($auditStatus==1){  //审核通过
            //对比采购单数量
            //退货数量
            $return = ReturnPurchaseDetail::findAllByAttributes(['parent_id'=>$id],'prod_id,prod_num,finish_num');
            //采购数量
            $purchaseId = ReturnPurchase::getOne(['id'=>$id],0,'purchase_id');

            $PurchaseNum = PurchaseDetail::findAllByAttributes(['parent_id'=>$purchaseId],'prod_id,prod_num,return_num,finish_num','prod_id');
            foreach ($return as $v){
                if(!$v['prod_num']){
                    return $this->error('退货数量不能为0','20023');
                }
                if(isset($PurchaseNum[$v['prod_id']])){
                    if($PurchaseNum[$v['prod_id']]['finish_num'] <= $PurchaseNum[$v['prod_id']]['return_num']){
                        return self::error('某产品没有可退货数量','2002');
                    }
                    if($v['prod_num'] > $PurchaseNum[$v['prod_id']]['prod_num']){
                        return self::error('申请退货数量不能超过采购数量','2002');
                    }
                    $needUpdate[] = [
                        'parent_id' => $id,
                        'return_num' =>$v['prod_num'],
                        'prod_id'   =>$v['prod_id']
                    ];
                }

            }
            $status=2;
        }elseif($auditStatus==2){ //审核不通过
            $status=4;
            $purchaseId = '';
            $needUpdate = [];
        }
        $data = [
            'audit_status'=>$auditStatus,
            'audit_suggest'=>$auditSuggest,
            'status'=>$status,
            'audit_date'=>date("Y-m-d H:i:s"),
            'audit_user_id'=>Yii::$app->user->id,
        ];
        $res = ReturnPurchase::checkOption($data,$id,$purchaseId,$needUpdate);
        if($res){
            return $this->success(['id'=>$id]);
        }
        return $this->error("操作失败");
    }
    //删除
    function actionDelete(){
        $id = intval(Yii::$app->request->post('id'));
        if(ReturnPurchase::updateAll(['del_status'=>1],['id'=>$id])){
            return $this->success(['id'=>$id]);
        }
        return $this->error("操作失败");
    }
    //关闭
    function actionClose(){
        $id = intval(Yii::$app->request->post('id'));
        if(ReturnPurchase::updateAll(['status'=>5],['id'=>$id])){
            return $this->success(['id'=>$id]);
        }
        return $this->error("操作失败");
    }
}
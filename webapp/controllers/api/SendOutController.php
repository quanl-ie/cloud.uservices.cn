<?php
namespace webapp\controllers\api;

use common\models\DictEnum;
use webapp\logic\SendOutLogic;
use webapp\models\Apll;
use webapp\models\ApllDetail;
use webapp\models\Region;
use Yii;

class SendOutController extends BaseController
{


    public function actionT()
    {
        return [10000];
    }
    /**
     * List
     * User: quanl
     * @throws \Exception
     */
    public function actionIndex()
    {
        return SendOutLogic::index();
    }
    /**
     *  beforeAdd
     *  User: quanl
     * @throws \Exception
     */
    public function actionBeforeAdd(){

        return SendOutLogic::beforeAdd();

    }
    /**
     * Add
     * User: quanl
     * @throws \Exception
     */
    public function actionAdd()
    {
        return SendOutLogic::add();
    }
    /**
     * confirm （后台收货）
     * User: quanl
     * @throws \Exception
     */
    public function actionConfirm()
    {
        return SendOutLogic::confirm();
    }
    /**
     * detail
     * User: quanl
     * @throws \Exception
     */
    public function actionDetail()
    {
        return SendOutLogic::detail();
    }

    public function actionGetSearchCondition()
    {
        $sendTypeList = DictEnum::findAllByAttributes(['dict_key'=>'send_out_type'],'dict_enum_id as id,dict_enum_value as name');
        $sendMethodList = DictEnum::findAllByAttributes(['dict_key'=>'send_method_type'],'dict_enum_id as id,dict_enum_value as name');
        $data['send_type_list'] =$sendTypeList;
        $data['send_method_list'] =$sendMethodList;
        return  $this->success($data);
    }
    /**
     * getApll List
     * User: quanl
     * @throws \Exception
     */
    public function actionGetApll()
    {
        return  $this->success(Apll::getList());
    }
    /**
     * AddApll Detail
     * User: quanl
     * @throws \Exception
     */
    public function actionAddApllDetail()
    {
        return SendOutLogic::AddApllDetail();
    }
    /**
     * editRecInfo(edit in View PM..nb)
     */
    public function actionEditRecInfo(){
        return SendOutLogic::editRecInfo();
    }
    /**
     * editPickInfo
     */
    public function actionEditPickInfo(){
        return SendOutLogic::editPickInfo();
    }

    /**
     * getArea
     * User: quanl
     * @throws \Exception
     */
    //getProvince
    public function actionGetProvince()
    {
        return  $this->success(Region::getOpenProvince());
    }
    //getCity
    public function actionGetCity()
    {
        $provinceId  = intval(Yii::$app->request->post('province_id',0));
        return  $this->success(Region::getOpenCity($provinceId));
    }
    //getDistrict
    public function actionGetDistrict()
    {
        $cityId  = intval(Yii::$app->request->post('city_id',0));
        return  $this->success(Region::getOpenDistrict($cityId));
    }

}
<?php

namespace webapp\controllers\api;

use common\helpers\Helper;
use common\models\DictEnum;
use webapp\models\Depot;
use webapp\models\DepotProdStock;
use webapp\models\Inventory;
use webapp\models\InventoryDetail;
use Yii;
class InventoryController extends BaseController
{

    /**
     * 填加前初始化显示数据
     * @author xi
     */
    public function actionAddOption()
    {
        $data = [
            'oddNum'   => Inventory::getOddNum(),
            'typeArr'  => DictEnum::getDataByKey('inventory_type'),
            'depotArr' => Depot::getAllDepot($this->directCompanyId),
            'userName' => $this->user->username,
            'time'     => date('Y-m-d H:i:s')
        ];

        return $this->success($data);
    }

    /**
     * 产品详情
     * @author xi
     */
    public function actionProdDetail()
    {
        $depotId   = intval(Yii::$app->request->post('depot_id',0));
        $classIds  = Yii::$app->request->post('class_ids',[]);
        $page      = intval(Yii::$app->request->post('page',1));

        if($depotId <= 0){
            return $this->error("请选择仓库", 20002);
        }

        $where = [
            'a.direct_company_id' => $this->directCompanyId,
            'a.depot_id' => $depotId,
            ['b.type_id' ,'in', [1,2]]
        ];

        if($classIds){
            $where[] = ['b.class_id','in',$classIds];
        }

        return $this->success(DepotProdStock::getList($where,$page,50));
    }

    /**
     * 盘点单提交
     * @author xi
     */
    public function actionAdd()
    {
        $oddNum    = trim(Yii::$app->request->post('odd_num',''));
        $subject   = trim(Yii::$app->request->post('subject',''));
        $type      = intval(Yii::$app->request->post('type',0));
        $benginTime= trim(Yii::$app->request->post('begin_time',''));
        $endTime   = trim(Yii::$app->request->post('end_time',''));
        $depotId   = intval(Yii::$app->request->post('depot_id',0));
        $classIds  = Yii::$app->request->post('class_ids',[]);
        $remark    = trim(Yii::$app->request->post('remark',''));

        if($oddNum  == ''){
            return $this->error("盘点单号不能为空", 20002);
        }
        if($subject  == ''){
            return $this->error("盘点主题不能为空", 20003);
        }
        if($depotId <= 0){
            return $this->error("仓库不能为空", 20004);
        }
        /*if(!$classIds){
            return $this->error("产品分类不能为空", 20005);
        }*/

        $transaction = Depot::getDb()->beginTransaction();
        try
        {
            $model = new Inventory();
            $model->direct_company_id = $this->directCompanyId;
            $model->odd_num         = $oddNum;
            $model->subject         = $subject;
            $model->type            = $type;
            $model->start_time      = $benginTime !='' ? strtotime($benginTime):0;
            $model->end_time        = $endTime !='' ? strtotime($endTime):0;
            $model->warehouse_id    = $depotId;
            $model->prod_class_ids  = $classIds?implode(',',$classIds):'';
            $model->create_user_id  = $this->user->id;
            $model->create_time     = time();
            $model->remark          = $remark;
            $model->invalid_user_id = '';
            $model->invalid_time    = '';
            $model->status          = 1;
            $model->exec_status     = 2;
            if($model->save())
            {
                $transaction->commit();

                //调用脚本执行
                if( strpos( strtolower(php_uname()),'windows') !==false){
                    $php =  exec("where php");
                }
                else {
                    $php =  exec("which php");
                    if(trim($php) == ''){
                        $php = "/usr/local/php/bin/php ";
                    }
                }

                $consolePath = Yii::getAlias('@console');
                @system("$php $consolePath/yii inventory/add-product ".$model->id ." &");
                //生成报表
                @system("$php $consolePath/yii inventory/generate-report ".$model->id ." &");

                return $this->success([
                    'oddNum' => $model->odd_num
                ]);
            }
            else {
                throw new \Exception("提交失败",30001);
            }

        }
        catch (\Exception $e)
        {
            $transaction->rollBack();
            return $this->error($e->getMessage(),$e->getCode());
        }
    }


    /**
     * 盘点列表 搜索项
     * @author xi
     */
    public function actionSearchOption()
    {
        $data = [
            'statusArr' => Inventory::getStatus(),
            'typeArr'   => DictEnum::getDataByKey('inventory_type')
        ];

        return $this->success($data);
    }

    /**
     * 盘点列表 带分页
     * @author xi
     */
    public function actionList()
    {
        $subject     = trim(Yii::$app->request->post('subject',''));
        $warehouse   = trim(Yii::$app->request->post('warehouse',''));
        $status      = intval(Yii::$app->request->post('status',0));
        $type        = intval(Yii::$app->request->post('type',0));
        $beginTime   = trim(Yii::$app->request->post('begin_time',0));
        $endTime     = trim(Yii::$app->request->post('end_time',0));
        $page        = intval(Yii::$app->request->post('page',1));
        $pageSize    = intval(Yii::$app->request->post('pageSize',10));

        $where = [
            'a.direct_company_id' => $this->directCompanyId,
        ];

        if($subject){
            $where [] = ['a.subject','like',$subject];
        }
        if($warehouse){
            $where [] = ['b.name','like',$warehouse];
        }
        if($status){
            $where[] = ['a.status','=',$status];
        }
        if($type){
            $where[] = ['a.type', '=', $type];
        }
        if($beginTime){
            $where[] = ['a.create_time', '>=', strtotime($beginTime)];
        }
        if($endTime){
            $where[] = ['a.create_time', '<=', strtotime($endTime . ' 23:59:59')];
        }

        $data = Inventory::getList($where,$page,$pageSize);
        return $this->success($data);
    }

    /**
     * 盘点详情
     * @author xi
     */
    public function actionView()
    {
        $id       = intval(Yii::$app->request->post('id',0));
        $page     = intval(Yii::$app->request->post('page',1));
        $pageSize = intval(Yii::$app->request->post('pageSize',50));

        if($id <= 0){
            return $this->error("盘点id不能为空",20002);
        }

        //盘点单信息
        $data = Inventory::viewById($id);
        if($data === false){
            return $this->error("未找到数据",20003);
        }

        //盘点产品明细
        $where = [
            'inventory_id' => $id
        ];
        $prodData = InventoryDetail::getList($where,$page,$pageSize);

        return $this->success([
            'baseInfo' => $data,
            'prodList' => $prodData
        ]);
    }

    /**
     * 结束盘点、作废盘点单
     * @author xi
     */
    public function actionChangeStatus()
    {
        $id     = intval(Yii::$app->request->post('id',0));
        $type   = intval(Yii::$app->request->post('type',0));
        $remark = trim(Yii::$app->request->post('remark',''));

        if($id <= 0){
            return $this->error("盘点id不能为空",20002);
        }
        if(!in_array($type,[1,2])){
            return $this->error("参数type取值不正确",20003);
        }

        $model = Inventory::findOne(['id'=>$id,'direct_company_id'=>$this->directCompanyId,'status' => 1]);
        if($model)
        {
            $model->status = $type == 1?2:3;
            $model->remark = $remark;

            if($type == 2){
                $model->invalid_user_id = $this->user->id;
                $model->invalid_time    = time();
            }

            if($model->save(false))
            {
                if($type == 1)
                {
                    Inventory::addStock($id);

                    //调用脚本执行
                    if( strpos( strtolower(php_uname()),'windows') !==false){
                        $php =  exec("where php");
                    }
                    else {
                        $php =  exec("which php");
                        if(trim($php) == ''){
                            $php = "/usr/local/php/bin/php ";
                        }
                    }

                    $consolePath = Yii::getAlias('@console');
                    //生成报表
                    @exec("$php $consolePath/yii inventory/generate-report ".$model->id ." &");
                }

                return $this->success([
                    'id' => $id
                ]);
            }
            else {
                return $this->error("操作失败",20005);
            }
        }
        else {
            return $this->error("未找到数据",20004);
        }
    }

    /**
     * 盘点操作
     * @author xi
     */
    public function actionPandian()
    {
        $id      = intval(Yii::$app->request->post('id',0));
        $realNum = floatval(Yii::$app->request->post('real_num',0));
        $remark  = trim(Yii::$app->request->post('remark',''));

        if($id <= 0){
            return $this->error("盘点id不能为空",20002);
        }

        $model = InventoryDetail::findOne(['id'=>$id,'direct_company_id'=>$this->directCompanyId]);
        if($model)
        {
            $inventoryArr = Inventory::findOneByAttributes(['id' => $model->inventory_id] , 'status');
            if($inventoryArr['status'] != 1){
                return $this->error("不可以重复盘点",20005);
            }

            if($realNum >= 0){
                $model->real_num = $realNum;
                $model->calc_num = $realNum - $model->num;
            }
            $model->remark           = $remark;
            $model->operator_user_id = $this->user->id;
            if($model->save())
            {
                return $this->success([
                    'id' => $id,
                    'username' => $this->user->username,
                    'calcNum'  => $model->calc_num
                ]);
            }
            else {
                return $this->error("操作失败",20003);
            }
        }

        return $this->error("未找到数据",20004);
    }

}

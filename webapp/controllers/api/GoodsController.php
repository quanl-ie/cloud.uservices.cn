<?php
namespace webapp\controllers\api;

use function Amp\first;
use webapp\logic\BaseLogic;
use webapp\models\Goods;
use webapp\models\GoodsDetail;
use webapp\models\GoodsProperty;
use webapp\models\GoodsSKU;
use webapp\models\Product;
use Yii;

class GoodsController extends BaseController
{
    /**
     * 商品列表
     * @param $status
     * @return array
     */
    public function actionList()
    {
        $name     = trim(Yii::$app->request->post('name',''));
        $status   = intval(Yii::$app->request->post('status',1));
        $page     = intval(Yii::$app->request->post('page',1));
        $pageSize = intval(Yii::$app->request->post('pagesize',10));

        $where = [
            'direct_company_id' => $this->directCompanyId,
            'status'            => 1,
            'del_status'        => 1,
            'status'            => $status
        ];

        if($name != ''){
            $where[] = ['name','like',$name];
        }

        $data = Goods::getList($where,$page,$pageSize);
        return $this->success($data);
    }

    /**
     * 检索产品
     * @author xi
     */
    public function actionSearchProduct()
    {
        $keywork = trim(Yii::$app->request->post('keyword',''));
        if($keywork == ''){
            return $this->error('关键词不能为空',20002);
        }

        $data = Product::searchProductName($this->directCompanyId,$keywork);
        return $this->success($data);
    }

    /**
     * 商品填加
     * @return array
     */
    public function actionAdd()
    {
        $productId    = intval(Yii::$app->request->post('product_id',0));
        $name         = trim(Yii::$app->request->post('name',''));
        $typeId       = intval(Yii::$app->request->post('type_id',0));
        $brandId      = intval(Yii::$app->request->post('brand_id',0));
        $classId      = intval(Yii::$app->request->post('class_id',0));
        $model        = trim(Yii::$app->request->post('model',''));
        $unitId       = intval(Yii::$app->request->post('unit_id',0));
        $warrantyNum  = intval(Yii::$app->request->post('warranty_num',0));
        $warrantyUnit = intval(Yii::$app->request->post('warranty_unit',0));
        $imgUrlArr    = Yii::$app->request->post('img_url_arr',[]);
        $status       = intval(Yii::$app->request->post('status',2));

        //商品属性
        $goodsPropertyArr = Yii::$app->request->post('goods_property_arr',[]);
        //商品详情
        $goodsDetailArr = Yii::$app->request->post('goods_detail_arr',[]);
        //商品规格
        $goodsSkuArr = Yii::$app->request->post('goods_sku_arr',[]);

        if(!in_array($status,[1,2])){
            return $this->error('参数不正确',20001);
        }
        if($name == ''){
            return $this->error('商品名称不能为空',20002);
        }
        if($typeId <= 0 ){
            return $this->error('商品类型不能为空',20003);
        }
        if($brandId <= 0){
            return $this->error('商品品牌不能为空',20004);
        }
        if($classId <= 0){
            return $this->error('商品类目不能为空',20005);
        }
        if($unitId <= 0){
            return $this->error('计量单位不能为空',20006);
        }
        if($warrantyNum > 0 && !in_array($warrantyUnit,[1,2,3])){
            return $this->error('请选择质保期单位',20007);
        }

        if($goodsPropertyArr)
        {
            $flag = true;
            $goodsCount = count($goodsPropertyArr);
            foreach ($goodsPropertyArr as $val){
                if(!isset($val['attr_key']) || !isset($val['attr_value'])){
                    $flag = false;
                }
                else if($goodsCount > 1 && (trim($val['attr_key']) == '' || trim($val['attr_value'] == ''))){
                    $flag = false;
                }
            }
            if($flag == false){
                return $this->error('请正确输入商品属性',20008);
            }
        }
        if($goodsDetailArr)
        {
            $flag = true;
            foreach ($goodsDetailArr as $val){
                if(!isset($val['img_url']) || !isset($val['description'])){
                    $flag = false;
                }
            }
            if($flag == false){
                return $this->error('请正确输入商品详情',20009);
            }
        }
        if($goodsSkuArr)
        {
            $flag = true;
            $nameArr = [];
            $goodsSkuCount = count($goodsSkuArr);
            foreach ($goodsSkuArr as $val)
            {
                if(!isset($val['goods_no']) || !isset($val['name']) || !isset($val['market_price'])){
                    $flag = false;
                }
                else if($goodsSkuCount > 1 && (!preg_match('/^\d+(\.\d{1,2})?$/' ,$val['market_price']))){
                    $flag = false;
                }
                $nameArr[]= $val['name'];
            }
            if($flag == false){
                return $this->error('请正确输入商品规格',20010);
            }

            $nameValueArr = array_count_values($nameArr);
            foreach ($nameValueArr as $val){
                if($val > 1){
                    return $this->error('商品规格名称不能重复填写',20010);
                }
            }
        }

        //保存数据库
        $transaction = Goods::getDb()->beginTransaction();
        try
        {
            $goodsModel = new Goods();
            $goodsModel->department_id     = $this->departmentId;
            $goodsModel->direct_company_id = $this->directCompanyId;
            $goodsModel->name              = $name;
            $goodsModel->product_id        = $productId;
            $goodsModel->type_id           = $typeId;
            $goodsModel->brand_id          = $brandId;
            $goodsModel->class_id          = $classId;
            $goodsModel->model             = $model;
            $goodsModel->unit_id           = $unitId;
            $goodsModel->warranty_num      = $warrantyNum;
            $goodsModel->warranty_unit     = $warrantyUnit;
            $goodsModel->img_url           = $imgUrlArr?implode(',',$imgUrlArr):'';
            $goodsModel->price_range       = $this->getPriceRange($goodsSkuArr);
            $goodsModel->create_user_id    = $this->user->id;
            $goodsModel->create_time       = time();
            $goodsModel->update_time       = time();
            $goodsModel->status            = $status;
            $goodsModel->del_status        = 1;
            if($goodsModel->save())
            {
                if($goodsPropertyArr)
                {
                    foreach ($goodsPropertyArr as $val)
                    {
                        if($val['attr_key'] && $val['attr_value'])
                        {
                            $goodsPropertyModel = new GoodsProperty();
                            $goodsPropertyModel->goods_id    = $goodsModel->id;
                            $goodsPropertyModel->attr_key    = $val['attr_key'];
                            $goodsPropertyModel->attr_value  = $val['attr_value'];
                            $goodsPropertyModel->create_time = time();
                            $goodsPropertyModel->update_time = time();
                            if(!$goodsPropertyModel->save()){
                                throw new \Exception('保存商品属性失败',20011);
                            }
                        }
                    }
                }

                if($goodsDetailArr)
                {
                    foreach ($goodsDetailArr as $val)
                    {
                        if($val['img_url']!='' || $val['description']!='')
                        {
                            $goodsDetailModel = new GoodsDetail();
                            $goodsDetailModel->goods_id    = $goodsModel->id;
                            $goodsDetailModel->img_url     = $val['img_url'];
                            $goodsDetailModel->description = $val['description'];
                            $goodsDetailModel->create_time = time();
                            $goodsDetailModel->update_time = time();
                            if(!$goodsDetailModel->save()){
                                throw new \Exception('保存商品详情失败',20012);
                            }
                        }
                    }
                }

                if($goodsSkuArr)
                {
                    foreach ($goodsSkuArr as $val)
                    {
                        if($val['market_price'] > 0)
                        {
                            $goodsSkuModel = new GoodsSKU();
                            $goodsSkuModel->goods_id     = $goodsModel->id;
                            $goodsSkuModel->goods_no     = $val['goods_no'];
                            $goodsSkuModel->name         = $val['name'];
                            $goodsSkuModel->market_price = $val['market_price'];
                            $goodsSkuModel->create_time  = time();
                            $goodsSkuModel->update_time  = time();
                            if(!$goodsSkuModel->save()){
                                throw new \Exception('保存商品规格失败',20013);
                            }
                        }
                    }
                }

                $transaction->commit();
                return $this->success([
                    'goods_id' => $goodsModel->id
                ]);
            }
        }
        catch(\Exception $e)
        {
            $transaction->rollBack();
            return $this->error('保存失败'.$e->getMessage(), 20014);
        }

        return $this->error("保存失败",20015);
    }

    /**
     * 编辑商品
     * @author xi
     */
    public function actionUpdate()
    {
        $goodsId      = intval(Yii::$app->request->post('goods_id',0));
        $productId    = intval(Yii::$app->request->post('product_id',0));
        $name         = trim(Yii::$app->request->post('name',''));
        $typeId       = intval(Yii::$app->request->post('type_id',0));
        $brandId      = intval(Yii::$app->request->post('brand_id',0));
        $classId      = intval(Yii::$app->request->post('class_id',0));
        $model        = trim(Yii::$app->request->post('model',''));
        $unitId       = intval(Yii::$app->request->post('unit_id',0));
        $warrantyNum  = intval(Yii::$app->request->post('warranty_num',0));
        $warrantyUnit = intval(Yii::$app->request->post('warranty_unit',0));
        $imgUrlArr    = Yii::$app->request->post('img_url_arr',[]);
        $status       = intval(Yii::$app->request->post('status',2));

        //商品属性
        $goodsPropertyArr = Yii::$app->request->post('goods_property_arr',[]);
        //商品详情
        $goodsDetailArr = Yii::$app->request->post('goods_detail_arr',[]);
        //商品规格
        $goodsSkuArr = Yii::$app->request->post('goods_sku_arr',[]);

        if($goodsId <= 0 ){
            return $this->error('商品id不能为空',20016);
        }
        if(!in_array($status,[1,2])){
            return $this->error('参数不正确',20001);
        }
        if($name == ''){
            return $this->error('商品名称不能为空',20002);
        }
        if($typeId <= 0 ){
            return $this->error('商品类型不能为空',20003);
        }
        if($brandId <= 0){
            return $this->error('商品品牌不能为空',20004);
        }
        if($classId <= 0){
            return $this->error('商品类目不能为空',20005);
        }
        if($unitId <= 0){
            return $this->error('计量单位不能为空',20006);
        }
        if($warrantyNum > 0 && !in_array($warrantyUnit,[1,2,3])){
            return $this->error('请选择质保期单位',20007);
        }
        if($goodsPropertyArr)
        {
            $flag = true;
            $goodsCount = count($goodsPropertyArr);
            foreach ($goodsPropertyArr as $val){
                if(!isset($val['attr_key']) || !isset($val['attr_value'])){
                    $flag = false;
                }
                else if($goodsCount > 1 && (trim($val['attr_key']) == '' || trim($val['attr_value'] == ''))){
                    $flag = false;
                }
            }
            if($flag == false){
                return $this->error('请正确输入商品属性',20008);
            }
        }
        if($goodsDetailArr)
        {
            $flag = true;
            foreach ($goodsDetailArr as $val){
                if(!isset($val['img_url']) || !isset($val['description'])){
                    $flag = false;
                }
            }
            if($flag == false){
                return $this->error('请正确输入商品详情',20009);
            }
        }
        if($goodsSkuArr)
        {
            $flag = true;
            $nameArr = [];
            $goodsSkuCount = count($goodsSkuArr);
            foreach ($goodsSkuArr as $val)
            {
                if(!isset($val['goods_no']) || !isset($val['name']) || !isset($val['market_price'])){
                    $flag = false;
                }
                else if($goodsSkuCount > 1 && !preg_match('/^\d+(\.\d{1,2})?$/' ,$val['market_price'])){
                    $flag = false;
                }
                $nameArr[]= $val['name'];
            }
            if($flag == false){
                return $this->error('请正确输入商品规格',20010);
            }

            $nameValueArr = array_count_values($nameArr);
            foreach ($nameValueArr as $val){
                if($val > 1){
                    return $this->error('商品规格名称不能重复填写',20010);
                }
            }
        }

        //保存数据库
        $transaction = Goods::getDb()->beginTransaction();
        try
        {
            $goodsModel = Goods::findOne(['id'=>$goodsId]);
            if($goodsModel)
            {
                if($imgUrlArr)
                {
                    foreach ($imgUrlArr as $key=>$val){
                        if(strpos($val,'?') !==false){
                            $imgUrlArr[$key] = substr($val,0, strpos($val,'?'));
                        }
                    }
                }

                $goodsModel->name              = $name;
                $goodsModel->product_id        = $productId;
                $goodsModel->type_id           = $typeId;
                $goodsModel->brand_id          = $brandId;
                $goodsModel->class_id          = $classId;
                $goodsModel->model             = $model;
                $goodsModel->unit_id           = $unitId;
                $goodsModel->warranty_num      = $warrantyNum;
                $goodsModel->warranty_unit     = $warrantyUnit;
                $goodsModel->img_url           = $imgUrlArr?implode(',',$imgUrlArr):'';
                $goodsModel->price_range       = $this->getPriceRange($goodsSkuArr);
                $goodsModel->create_user_id    = $this->user->id;
                $goodsModel->update_time       = time();
                $goodsModel->status            = $status;
                if($goodsModel->save())
                {
                    GoodsProperty::updateAll(['status'=>0,'update_time'=>time()],['goods_id'=>$goodsModel->id]);
                    if($goodsPropertyArr)
                    {
                        foreach ($goodsPropertyArr as $val)
                        {
                            if($val['attr_key'] && $val['attr_value'])
                            {
                                $goodsPropertyModel = new GoodsProperty();
                                $goodsPropertyModel->goods_id    = $goodsModel->id;
                                $goodsPropertyModel->attr_key    = $val['attr_key'];
                                $goodsPropertyModel->attr_value  = $val['attr_value'];
                                $goodsPropertyModel->create_time = time();
                                $goodsPropertyModel->update_time = time();
                                if(!$goodsPropertyModel->save()){
                                    throw new \Exception('保存商品属性失败',20011);
                                }
                            }
                        }
                    }

                    GoodsDetail::updateAll(['status'=>0,'update_time'=>time()],['goods_id'=>$goodsModel->id]);
                    if($goodsDetailArr)
                    {
                        foreach ($goodsDetailArr as $val)
                        {
                            $imgUrl = $val['img_url'];
                            if(strpos($val['img_url'],'?') !== false){
                                $imgUrl = substr($val['img_url'],0, strpos($val['img_url'],'?'));
                            }

                            $goodsDetailModel = new GoodsDetail();
                            $goodsDetailModel->goods_id    = $goodsModel->id;
                            $goodsDetailModel->img_url     = $imgUrl;
                            $goodsDetailModel->description = $val['description'];
                            $goodsDetailModel->create_time = time();
                            $goodsDetailModel->update_time = time();
                            if(!$goodsDetailModel->save()){
                                throw new \Exception('保存商品详情失败',20012);
                            }
                        }
                    }

                    GoodsSKU::updateAll(['status'=>0,'update_time'=>time()],['goods_id'=>$goodsModel->id]);
                    if($goodsSkuArr)
                    {
                        foreach ($goodsSkuArr as $val)
                        {
                            if($val['market_price'] > 0)
                            {
                                $goodsSkuModel = new GoodsSKU();
                                $goodsSkuModel->goods_id     = $goodsModel->id;
                                $goodsSkuModel->goods_no     = $val['goods_no'];
                                $goodsSkuModel->name         = $val['name'];
                                $goodsSkuModel->market_price = $val['market_price'];
                                $goodsSkuModel->create_time  = time();
                                $goodsSkuModel->update_time  = time();
                                if(!$goodsSkuModel->save()){
                                    throw new \Exception('保存商品规格失败',20013);
                                }
                            }
                        }
                    }

                    $transaction->commit();
                    return $this->success([
                        'goods_id' => $goodsModel->id
                    ]);
                }
            }
        }
        catch(\Exception $e)
        {
            $transaction->rollBack();
            return $this->error('保存失败'.$e->getTraceAsString(), 20014);
        }

        return $this->error("保存失败",20015);
    }

    /**
     * 查看
     * @author xi
     */
    public function actionView()
    {
        $goodsId = intval(Yii::$app->request->post('goods_id',0));
        if($goodsId <= 0){
            return $this->error("商品id不能为空",20002);
        }

        $data = Goods::view($goodsId,$this->directCompanyId);
        return $this->success($data);
    }

    /**
     * 查看
     * @author xi
     */
    public function actionUpdateView()
    {
        $goodsId = intval(Yii::$app->request->post('goods_id',0));
        if($goodsId <= 0){
            return $this->error("商品id不能为空",20002);
        }

        $data = Goods::updateView($goodsId,$this->directCompanyId);
        return $this->success($data);
    }
    /**
     * 上线下线接口
     * @return array
     * @author xi
     */
    public function actionOnoff()
    {
        $goodsId = intval(Yii::$app->request->post('goods_id',0));
        $status  = intval(Yii::$app->request->post('status',0));
        if($goodsId <= 0){
            return $this->error("商品id不能为空",20002);
        }
        if(!in_array($status,[1,2])){
            return $this->error("参数 status 不能为空",20003);
        }

        $model = Goods::findOne(['id'=>$goodsId,'status'=> $status==1?2:1]);
        if($model){
            $model->status = $status;
            $model->update_time = time();
            if($model->save()){
                return $this->success([
                    'goods_id' => $goodsId
                ]);
            }
        }
        return $this->error('操作失败',20004);
    }

    /**
     * 获取基础配置
     * @author xi
     */
    public function actionGetBasicsConfig()
    {
        $brandArr =  BaseLogic::getBrand([],1);
        $unitArr  = BaseLogic::getUnit();
        $typeArr  = Goods::getTypeDesc(false);
        $warrantyUnitArr = [
            1 => '天',
            2 => '月',
            3 => '年'
        ];

        $result = [
            'brandArr' => $brandArr,
            'unitArr'  => $unitArr,
            'typeArr'  => $typeArr,
            'warrantyUnitArr' => $warrantyUnitArr
        ];
        return $this->success($result);
    }


    /**
     * 获取价格区间
     * @param $goodsSkuArr
     * @return mixed|string
     */
    private function getPriceRange($goodsSkuArr)
    {
        $result = '';
        if($goodsSkuArr)
        {
            $priceArr = [];
            foreach ($goodsSkuArr as $val){
                $priceArr[] = $val['market_price'];
            }

            if(count($priceArr) == 1){
                $result = floatval($priceArr[0]);
            }
            else {
                sort($priceArr);

                $min = floatval(current($priceArr));
                $max = floatval(end($priceArr));

                if( $min != $max) {
                    $result =  $min. ' - ' . $max;
                }
                else {
                    $result = $min;
                }
            }
        }

        return $result;
    }

}
<?php

namespace webapp\controllers;
use webapp\logic\BaseLogic;
use webapp\models\CostRule;
use webapp\models\CostRuleRelation;
use Yii;
use yii\web\Controller;
use common\helpers\Paging;
use webapp\logic\TechnicianCostRuleLogic;
use webapp\models\CostItem;
use webapp\models\Technician;
/**
 *@desc:客户管理
 * @author: chengjuanjuan <chengjuanjuan@c-ntek.com>
 * createdAt: 2017/12/14 14:09
 *
 */


class TechnicianCostRuleController extends BaseController
{



    /**
     * @desc 结算规则列表
     **/
    public function actionIndex(){
        $result = TechnicianCostRuleLogic::index();
        if($result['totalPage'] >=1){
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?page=');
        }
        $this->_view['data']     = $result['list'];
        $this->_view['pageHtml'] = isset($pageHtml)?$pageHtml:' ';
        return $this->render('index');

    }



    // 添加
    public function actionAdd()
    {
        if (yii::$app->request->isPost) {
            //判断是否存在id
            if (yii::$app->request->get('id', 0) == 0) {
                $res = TechnicianCostRuleLogic::add();
                if ($res['code'] == 200) {
                    Yii::$app->session->setFlash('message','提交成功');
                    return $this->redirect('/technician-cost-rule/index');
                } else {
                    Yii::$app->session->setFlash('message', $res['message']);
                }
            }
        }

        $itemList       = CostItem::getList();
        $technicianList = Technician::getList(['store_id'=>BaseLogic::getManufactorId(),'audit_status'=>5],'',0,100);

        $data = [
            'costItem'     => $itemList,
            'technician'   => $technicianList['list']
        ];

        return $this->render('add',['data'=>$data]);
    }



    // 添加
    public function actionEdit()
    {
        if (yii::$app->request->isPost) {
            //判断是否存在id
                $res = TechnicianCostRuleLogic::edit();
                if ($res['code'] == 200) {
                    Yii::$app->session->setFlash('message','提交成功');
                    return $this->redirect('/technician-cost-rule/index');
                } else {
                    Yii::$app->session->setFlash('message', $res['message']);
                }

        }
        $data = TechnicianCostRuleLogic::showEdit();
   /*     echo '<pre>';
        print_R($data);exit;*/
        $this->_view['info']     = $data['info'];
        $this->_view['costItem']     = $data['costItem'];
        $this->_view['technician']     = $data['technician'];
        return $this->render('edit');
    }

}


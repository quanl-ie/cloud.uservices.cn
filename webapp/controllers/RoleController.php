<?php
namespace webapp\controllers;

use Faker\Provider\Base;
use webapp\models\BaseModel;
use webapp\models\ModelBase;
use Yii;
use webapp\models\Role;
use webapp\models\Menu;
use webapp\controllers\BaseController;
use webapp\logic\BaseLogic;
use webapp\logic\RoleLogic;
use common\models\User;
use webapp\models\Department;

/**
 * 角色管理
 * @author  liwenyong<liwenyong@c-ntek.com>
 * @date    2017-08-17
 */
class RoleController extends BaseController
{
    /**
     * 分组列表
     * @author sxz
     * @date   2018-06-6
     * @return object
     */
    public function actionIndex()
    {
        $where = $data = [];
        // 用户权限
        $dataAuthId = $this->dataAuthId;
        $department_id   = BaseLogic::getDepartmentId();  //自身的id
        $department_pids = implode(',',BaseLogic::getParentIds());
        $login_user_id   = BaseLogic::getLoginUserId();
        $topdepartmentId = BaseLogic::getTopId();
        $directCompanyId = BaseLogic::getDirectCompanyId();   //直属公司id
        //获取直属机构是经销商还是部门
        $directCompanyInfo = Department::findOne(['id'=>$department_id]);
        $directCompanyType = isset($directCompanyInfo['type'])?$directCompanyInfo['type']:'';
        if($dataAuthId == 1){
            //本人数据权限，仅能查看本人创建的角色
            $where = ['create_user_id'=>$login_user_id,'status'=>[1,2]];
        }elseif ($dataAuthId == 4){
            //本部门，查看本部门用户创建的角色，可操作本人创建的角色，
            if(isset($directCompanyType) && $directCompanyType == 1){
                //如果用户直属为经销商，但数据权限为本部门或部门及以下，则数据仅显示本人数据  type 1经销商 3部门
                $where = ['create_user_id'=>$login_user_id,'status'=>[1,2]];
            }else{
                //获取本部门的所有用户
                $userIds = '';
                $userList = User::getList(['department_id'=>$department_id]);
                if($userList){
                    $userIds = array_column($userList,'id');
                }
                $where = ['create_user_id'=>$userIds,'status'=>[1,2]];
            }
        }elseif ($dataAuthId == 5){
            //本部门及以下，查看该用户所属部门及以下部门用户创建的角色，可操作本人创建的角色
            if(isset($directCompanyType) && $directCompanyType == 1){
                //如果用户直属为经销商，但数据权限为本部门或部门及以下，则数据仅显示本人数据  type 1经销商 3部门
                $where = ['create_user_id'=>$login_user_id,'status'=>[1,2]];
            }else{
                $userIds = '';
                $where   = [];
                //获取本部门的所有用户及以下部门的用户
                //$nextDepartment = Department::getDepartmentSonIds($department_id,[3]);
                $nextDepartment = Department::getDepartmentChildren($department_id,2);
                if($nextDepartment){
                    $department_ids = array_column($nextDepartment,'id');
                    $userList = User::getList(['department_id'=>$department_ids]);
                    if($userList){
                        $userIds = array_column($userList,'id');
                    }
                    $where = ['create_user_id'=>$userIds,'status'=>[1,2]];
                }
            }

        }elseif ($dataAuthId == 2){
            //本公司，查看本公司用户创建的角色，可操作本人创建的角色
            $userIds = '';
            $where   = [];
            //获取本公司的所有用户
            $top_id = $this->directCompanyId;
            $nextDepartment = Department::getDepartmentChildren($top_id,1,1);
            if($nextDepartment){
                $department_ids = array_column($nextDepartment,'id');
                $userList = User::getList(['department_id'=>$department_ids]);
                if($userList){
                    $userIds = array_column($userList,'id');
                }
                $where = ['create_user_id'=>$userIds,'status'=>[1,2]];
            }
        }elseif ($dataAuthId == 3){
            //本公司及以下，查看本公司用户创建的角色，可操作本人创建的角色
            $userIds = '';
            $where   = [];
            //获取本公司的所有用户
            $top_id = $this->directCompanyId;
            $nextDepartment = Department::getDepartmentChildren($top_id,0,1);
            if($nextDepartment){
                $department_ids = array_column($nextDepartment,'id');
                $userList = User::getList(['department_id'=>$department_ids]);
                if($userList){
                    $userIds = array_column($userList,'id');
                }
                $where = ['create_user_id'=>$userIds,'status'=>[1,2]];
            }
        }
        $data = Role::find()->where($where)->andWhere(['<>','department_id',0])->andWhere(['<>','create_user_id',''])->asArray()->all();
        if($data){
            foreach ($data as $k => $v) {
                $data[$k]['create_user_name']= RoleLogic::getUserInfo($v['create_user_id']);
                if (in_array($v['id'],[9,10,11,12])) {
                    unset($data[$k]);
                }
            }
        }
        return $this->render('index',['data' => $data,'login_user_id'=>$login_user_id]);
    }
    
    /**
     * 添加角色
     * @author sxz
     * @date   2018-06-06
     * @return 
     */
    public function actionAdd()
    {
        $model = new Role();
        if($model->load(Yii::$app->request->post()))
        {
            $menu_ids = Yii::$app->request->post('menu');
            if (!$menu_ids) {
                $model->addError('menu_ids','请选择权限配置');
            } else {
                $where = [
                    'name'    => $model->name,
                    'status'  => 1,
                    'department_id'   => BaseLogic::getDepartmentId()
                ];
                $count = Role::find()->where($where)->count();
                if ($count>0) {
                    $model->addError('name','该角色名称已存在');
                }
                $error = $model->getErrors();

                if (empty($error))
                {
                    $model->menu_ids = implode(',', $menu_ids);
                    $model->department_id      = BaseLogic::getDepartmentId();
                    $model->department_pids    = implode(',',BaseLogic::getParentIds());
                    $model->direct_company_id  = BaseLogic::getDirectCompanyId();
                    $model->create_user_id     = BaseLogic::getLoginUserId();
                    $model->status = 1;
                    $model->created_at = $model->updated_at = time();
                    if ($model->save(false)) {
                        Yii::$app->session->setFlash('message','添加成功!');
                        return $this->redirect('/role/index');
                    }
                }
            }
        }

        $menu_ids = '';
        //不是初始化权限的时候查出当前账号的权限role_id
        $role_ids = Yii::$app->user->getIdentity()->role_ids;
        if(!in_array($role_ids,[9,10,11,12]))
        {
            $info = Role::getOne(['id'=>$role_ids,'status'=>1]);
            $menu_ids =  $info['menu_ids'];
        }

        //ajax 获取菜单
        if(Yii::$app->request->isAjax)
        {
            $dataRole = Yii::$app->request->get('data_role',1);
            $menu = Menu::getHierarchy(0,$menu_ids,$dataRole);
            $html = $this->renderPartial('_form',['menu' => $menu]);
            return json_encode(['html'=>$html]);
        }

        //菜单列表
        $menu = Menu::getHierarchy(0,$menu_ids,1);
        //数据权限列表
        $dataAuthList = Menu::getDataAuth();
        return $this->render('add',[
            'menu'         => $menu,
            'model'        => $model,
            'dataAuthList' => $dataAuthList
        ]);
    }
    
    /**
     * 编辑角色信息
     * @author sxz
     * @date   2018-06-6
     * @return 
     */
    public function actionEdit() 
    {
        $department_id  = BaseLogic::getDepartmentId();
        $id = intval(Yii::$app->request->get('id',0));
        $where = [
            'id'      => $id,
            'status'  => [1,2],
            'department_id'   => $department_id,
        ];
        $model = Role::findOne($where);
        if ($id<=0 || !$model) {
            return $this->redirect("/role/index");
        }

        if ($model->load(Yii::$app->request->post()))
        {
            $menu_ids = Yii::$app->request->post('menu');
            if (!$menu_ids) {
                $model->addError('menu_ids','请选择权限配置');
            } else {
                $where = "department_id=".$department_id." and name='".$model->name."' and status=1 and id<>".$model->id;
                $count = Role::find()->where($where)->count();
                if ($count>0) {
                    $model->addError('name','该角色名称已存在');
                }
                $error = $model->getErrors();
                if (empty($error)) {
                    $model->menu_ids = implode(',', $menu_ids);
                    $model->updated_at = time();
                    if ($model->save(false)) {
                        Yii::$app->session->setFlash('message','修改成功!');
                        return $this->redirect('/role/index');
                    }
                }
            }
        }
        $menu_ids = '';
        //不是初始化权限的时候查出当前账号的权限role_id
        $role_ids = Yii::$app->user->getIdentity()->role_ids;
        if(!in_array($role_ids,[9,10,11,12]))
        {
            $info = Role::getOne(['id'=>$role_ids,'status'=>1]);
            $menu_ids =  $info['menu_ids'];
        }

        //ajax 获取菜单
        if(Yii::$app->request->isAjax)
        {
            $dataRole = Yii::$app->request->get('data_role',1);
            $menu = Menu::getHierarchy(0,$menu_ids,$dataRole);
            $menu = $this->recombinedData($menu, $model->menu_ids);
            $html = $this->renderPartial('_form',['menu' => $menu]);
            return json_encode(['html'=>$html]);
        }

        //菜单列表
        $menu = Menu::getHierarchy(0,$menu_ids,$model->data_auth_id);
        $menu = $this->recombinedData($menu, $model->menu_ids);

        //数据权限列表
        $dataAuthList = Menu::getDataAuth();

        return $this->render('edit',[
            'menu'         => $menu,
            'model'        => $model,
            'dataAuthList' => $dataAuthList,
        ]);
    }
    
    /**
     * 查看角色信息
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return array
     */
    public function actionView()
    {
        $id = intval(Yii::$app->request->get('id',0));
        $where = [
            'id' => $id,
            'status' => [1,2],
        ];
        $model = Role::findOne($where);
        if ($id<=0 || !$model) {
            return $this->redirect("/role/index");
        }

        //菜单列表
        $menu_ids = $model->menu_ids;
        $menu = Menu::getHierarchy(0,$menu_ids,$model->data_auth_id);
        $menuIds = explode(',', $model->menu_ids);

        foreach ($menu as $key=>$val)
        {
            if (in_array($val['id'], $menuIds)) {
                $menu[$key]['checked'] = 'checked';
            }
            unset($tempArr,$tempKey);
            if (isset($val['sub'])) {
                foreach ($val['sub'] as $k=>$v)
                {
                    if (in_array($v['id'], $menuIds)) {
                        $menu[$key]['sub'][$k]['checked'] = 'checked';
                    }
                    if (isset($v['sub']) && $v['sub']) {
                        foreach ($v['sub'] as $k2=>$v2)
                        {
                            if (in_array($v2['id'], $menuIds)) {
                                $menu[$key]['sub'][$k]['sub'][$k2]['checked'] = 'checked';
                            }
                        }
                    }
                }
            }
        }
        //数据权限列表
        $dataAuthList = Menu::getDataAuth();
        $model->data_auth_id = isset($dataAuthList[$model->data_auth_id])?$dataAuthList[$model->data_auth_id]:'';
        return $this->render('view',['menu' => $menu, 'model' => $model]);
    }
    
    /**
     * 删除角色
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return String
     */
    public function actionDelete() 
    {
        $department_id  = BaseLogic::getDepartmentId();
        $id   = intval(Yii::$app->request->get('id',0));
        if ($id<= 0) {
            return $this->redirect('/role/index');
        }
        $where = [
            'id'     => $id,
            'status' => [1,2],
            'department_id' => $department_id
        ];
        $model = Role::findOne($where);
        if (!empty($model)) {
            $oldModel = clone $model;
            $model->status = 0;
            $model->updated_at = time();
            if ($model->save(false)) {
                Yii::$app->session->setFlash('message','操作成功!');
                return $this->redirect ('/role/index');
            }
        }
        return $this->redirect('/role/index');
    }
    
    /**
     * 禁用角色
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return String
     */
    public function actionBan()
    {
        $id   = intval(Yii::$app->request->get('id',0));
        if ($id<= 0) {
            return $this->redirect('/role/index');
        }
        $where = [
            'id'     => $id,
            'status' => 1
        ];
        $model = Role::findOne($where);
        if (!empty($model)) {
            $oldModel = clone $model;
            $model->status = 2;
            $model->updated_at = time();
            if ($model->save(false)) {
                Yii::$app->session->setFlash('message','操作成功!');
                return $this->redirect ('/role/index');
            }
        }
        return $this->redirect('/role/index');
    }
    
    /**
     * 启用角色
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return String
     */
    public function actionUnBan()
    {
        $id   = intval(Yii::$app->request->get('id',0));
        
        if($id<= 0){
            return $this->redirect('/role/index');
        }
        
        $where = [
            'id'     => $id,
            'status' => 2
        ];
        $model = Role::findOne($where);
        if(!empty($model))
        {
            $oldModel = clone $model;
            $model->status = 1;
            $model->updated_at = time();
            if($model->save(false))
            {
                Yii::$app->session->setFlash('message','操作成功!');
                return $this->redirect ('/role/index');
            }
        }
        return $this->redirect('/role/index');
    }

    /**
     * 重组数据
     * @param $menu
     * @param $menuIdsStr
     * @return mixed
     */
    private function recombinedData($menu, $menuIdsStr)
    {
        $menuIds = explode(',', $menuIdsStr);
        foreach ($menu as $key=>$val)
        {
            if (in_array($val['id'], $menuIds)) {
                $menu[$key]['checked'] = 'checked';
            }
            unset($tempArr,$tempKey);
            if (isset($val['sub']))
            {
                foreach ($val['sub'] as $k=>$v)
                {
                    if (in_array($v['id'], $menuIds)) {
                        $menu[$key]['sub'][$k]['checked'] = 'checked';
                    }
                    if (isset($v['sub']) && $v['sub'])
                    {
                        foreach ($v['sub'] as $k2=>$v2)
                        {
                            if (in_array($v2['id'], $menuIds)) {
                                $menu[$key]['sub'][$k]['sub'][$k2]['checked'] = 'checked';
                            }
                        }
                    }
                }
            }
        }

        return $menu;
    }
}


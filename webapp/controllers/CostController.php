<?php

namespace webapp\controllers;

use webapp\models\Cooporation;
use Yii;
use webapp\logic\CostLogic;
use webapp\controllers\BaseController;
use common\helpers\Paging;
use common\components\Upload;


/**
 * @desc:收费列表管理
 * @author Eva <chengjuanjuan@c-ntek.com>
 * @date 2018-2-2
 *
 */

class CostController extends BaseController
{
    /**
     * 结算首页
     * @author Eva <chengjuanjuan@c-ntek.com>
     */
    public function actionIndex()
    {
        $res = CostLogic::getIndex();
        $this->_view['data'] = $res;
        return $this->render('index');

    }

    //待对账
    public function actionWaitCheck()
    {
        $res = CostLogic::getList();
        if($res['totalPage'] >=1){
            $pageHtml = Paging::make($res['page'],  $res['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }
        $this->_view['data'] = isset($res['data'])?$res['data']:$res['list'];
        $this->_view['pageHtml'] = $pageHtml;
        return $this->render('wait-check');

    }

    //待结算
    public function actionWaitCost()
    {
        $res = CostLogic::getList();
        if($res['totalPage'] >=1){
            $pageHtml = Paging::make($res['page'],  $res['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }
        $this->_view['data'] = isset($res['data'])?$res['data']:$res['list'];
        $this->_view['pageHtml'] = $pageHtml;
        return $this->render('wait-cost');

    }

    //已结算
    public function actionHaveCosted()
    {
        $res = CostLogic::getList();
        if($res['totalPage'] >=1){
            $pageHtml = Paging::make($res['page'],  $res['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }
        $this->_view['data'] = isset($res['data'])?$res['data']:$res['list'];
        $this->_view['pageHtml'] = $pageHtml;
        return $this->render('have-costed');

    }

    //申请对账单
    public function actionApply(){
        $data = Cooporation::getListByParam('');
        if (Yii::$app->request->isPost){
            if(Yii::$app->request->post('is_submit')){
                $res= CostLogic::checkListSubmit();
                if($res) {
                    return $this->redirect('/cost/wait-check');
                }
            }

            if(Yii::$app->request->post('mark')){
                return CostLogic::getData();
            }
            $res = CostLogic::updateStatus();

            if($res['code'] == 200) {
                return $this->redirect('/cost/wait-check');
            }
            else {
                Yii::$app->session->setFlash('message',$res['message']);
            }
        }
        return $this->render('apply',
            [
                'data'  => $data,
            ]);
    }


    //对账单详情
    public function actionDetail(){
        if(Yii::$app->request->get('export')){
            CostLogic::getExport();
        }
        $detail = CostLogic::getDetail();
        if(!$detail['info']){
            Yii::$app->session->setFlash('message','该结算单不存在');
        }
        return $this->render('detail',
            [
                'data'  => $detail['info'],
                'costItem' =>$detail['costItem']
            ]);
    }
    
    
    /**
     * 待结算订单列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/22
     * Time: 14:10
     * @return string
     */
    public function actionWaitCostOrder()
    {
        
        $result = CostLogic::getWaitCostOrder();
    
        $pageHtml = '';
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }
    
        $this->_view['data']     = $result['list'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['sumPrice'] = isset($result['sumPrice']) ? $result['sumPrice'] : 0.00;
        $this->_view['sum']      = $result['totalCount'];
        $this->_view['type']     = isset($result['type']) ? $result['type'] : [];
        return $this->render('wait-cost-order');
    }




}
<?php
/**
 * Created by PhpStorm.
 * 产品管理
 * User: xingq
 * Date: 2018/3/20
 * Time: 14:02
 */

namespace webapp\controllers;

use Yii;
use webapp\logic\ProductLogic;
use common\helpers\Paging;

class ProductController extends BaseController
{
    /**
     * 产品列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 15:27
     * @return string
     */
    public function actionIndex()
    {
        $q          = Yii::$app->request->get('q','');
        $brforeList = ProductLogic::getBeforeList();
        $res        = ProductLogic::index();

        if($q){
            //未找到相应信息
            $search_res = $res['list'];
            if(!$search_res){
                $search_res[] = [
                    'id' => '0',
                    'prod_name' => '未找到相应产品',
                    'model'=>'',
                    'prod_no'=>''  //产品编号
                ];
                return json_encode($search_res);
            }
            //将结果按照产品名称拼音正序
            $search_res = ProductLogic::setRank($search_res);
            return json_encode($search_res);
        }

        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }
        $pageHtml = '';
        if($res['totalPage'] >1){
            $pageHtml = Paging::make($res['page'], $res['totalPage'], $getStr.'page=');
        }
        $this->_view['data']      = $res['list'];
        $this->_view['className'] = $res['className'];
        $this->_view['classId']   = $res['classId'];
        $this->_view['pageHtml']  = $pageHtml;
        $this->_view['brand']     = $brforeList['brand'];
        $this->_view['class']     = $brforeList['class'];
        $this->_view['flag']      = $brforeList['flag'];
        $this->_view['dcId']      = $brforeList['dcId'];
        return $this->render('index');
    }

    /**
     * 添加
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 15:28
     * @return string
     */
    public function actionAdd()
    {
        $brforeList = ProductLogic::getBeforeList(1);
        if (!$brforeList) {
            return $this->redirect('index');
        }
        if (Yii::$app->request->isPost)
        {
            $res = ProductLogic::add();
            return json_encode($res);
            Yii::$app->session->setFlash('message',$res['message']);
            if ($res['code'] == 200) {
                return $this->redirect('index');
            }
        }
        $this->_view['warranty'] = $brforeList['warranty'];
        $this->_view['unit']     = $brforeList['unit'];
        $this->_view['brand']    = $brforeList['brand'];
        $this->_view['class']    = $brforeList['class'];
        $this->_view['flag']     = $brforeList['flag'];
        return $this->render('add');
    }

    /**
     * 编辑
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 15:28
     * @return string
     */
    public function actionEdit()
    {
        if (Yii::$app->request->isPost) {
            $res = ProductLogic::edit();
            return json_encode($res);
        }
        
        $brforeList = ProductLogic::getBeforeList();
        if (!$brforeList) {
            return '数据错误';
        }
        
        $data = ProductLogic::editList();
        
        if (isset($data['data']['model'])) {
            $this->_view['data'] = $data['data']['model'];
        }
        if (isset($data['data']['class'])) {
            $this->_view['class_name'] = $data['data']['class'];
        }
    
        if (isset($data['data']['materials'])) {
            $this->_view['materials'] = $data['data']['materials'];
        }
    
        if (isset($data['data']['prod'])) {
            $this->_view['prod'] = $data['data']['prod'];
        }
    
        if (isset($data['data']['contract'])) {
            $this->_view['contract'] = $data['data']['contract'];
        }
        $this->_view['warranty'] = $brforeList['warranty'];
        $this->_view['unit']     = $brforeList['unit'];
        $this->_view['brand']    = $brforeList['brand'];
        $this->_view['class']    = $brforeList['class'];
        $this->_view['flag']     = $brforeList['flag'];
        
        return $this->render('edit');
    }

    /**
     * ajax更改状态
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 17:43
     * @return string
     */
    public function actionAjaxEditStatus()
    {
        $res = ProductLogic::ajaxEditStatus();
        return json_encode($res);
    }

    /**
     * @return string
     * 启用
     */
    public function actionAjaxEditStatusOn()
    {
        $res = ProductLogic::ajaxEditStatus();
        return json_encode($res);
    }
    
    /**
     * 获取调用产品列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 17:55
     * @return string
     */
    public function actionGetList()
    {
        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }

        $res = ProductLogic::getList();
        $pageHtml = '';
        if($res['totalPage'] >1){
            $pageHtml = Paging::make($res['page'], $res['totalPage'], $getStr.'page=');
        }
        return $this->renderPartial('list',
            [
                'data'     => $res['list'],
                'pageHtml' => $pageHtml,
            ]);
    }
    
    /**
     * 获取调用产品详细
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/31
     * Time: 15:40
     * @return mixed|string|void
     */
    public function actionGetDetail(){
        $info =   ProductLogic::getDetail();

        return json_encode($info);
    }
    
    /**
     * 查看
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/30
     * Time: 19:05
     * @return string|\yii\web\Response
     */
    public function actionView()
    {
//        $res = ProductLogic::getView();
        $brforeList = ProductLogic::getBeforeList();
        if (!$brforeList) {
            return '数据错误';
        }
        $data = ProductLogic::editList();
        if (isset($data['data']['model'])) {
            $this->_view['data'] = $data['data']['model'];
        }
        if (isset($data['data']['class'])) {
            $this->_view['class_name'] = $data['data']['class'];
        }
        if (isset($data['data']['materials'])) {
            $this->_view['materials'] = $data['data']['materials'];
        }

        if (isset($data['data']['prod'])) {
            $this->_view['prod'] = $data['data']['prod'];
        }

        if (isset($data['data']['contract'])) {
            $this->_view['contract'] = $data['data']['contract'];
        }
        $this->_view['warranty'] = $brforeList['warranty'];
        $this->_view['unit']     = $brforeList['unit'];
        $this->_view['brand']    = $brforeList['brand'];
        $this->_view['class']    = $brforeList['class'];
        $this->_view['flag']     = $brforeList['flag'];
        return $this->render('detail');
    }
    
    /**
     * 获取物料包列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/31
     * Time: 15:41
     */
    public function actionMaterials()
    {
        $res = ProductLogic::materials();

        $pageHtml = '';
        if($res['totalPage'] >1){
            $pageHtml = Paging::make($res['page'], $res['totalPage'], '?page=');
        }
        return $this->renderPartial('materials',
            [
                'data'     => $res['list'],
                'pageHtml' => $pageHtml,
            ]);
    }
    
    /**
     * 合同与产品管理共用弹窗列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/8/1
     * Time: 14:45
     * @return string
     */
    public function actionShareList()
    {
        $type = intval(Yii::$app->request->get('type', 0));
        $res  = ProductLogic::shareList($type);
        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }
        $pageHtml = '';
        if($res['totalPage'] >1){
            $pageHtml = Paging::make($res['page'], $res['totalPage'], $getStr.'page=');
        }
        return $this->renderPartial('share-list',
            [
                'data'     => $res['list'],
                'type'     => $type,
                'pageHtml' => $pageHtml,
            ]);
    }
    
    /**
     * 合同与产品管理共用返回值详情
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/8/1
     * Time: 14:45
     * @return mixed|string|void
     */
    public function actionShareDetail()
    {
        $info = ProductLogic::shareDetail();
        return json_encode($info);
    }

    /**
     * 获取物料包详情
     * User: sxz
     * Date: 2018/7/31
     * Time: 17:41
     */
    public function actionMaterialsDetail()
    {
        $res = ProductLogic::materialsDetail();
        return $this->renderPartial('view-materials-detail',
            [
                'data'     => $res,
            ]);
    }
}
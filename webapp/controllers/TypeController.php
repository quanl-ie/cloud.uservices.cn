<?php

namespace webapp\controllers;

use Yii;
use webapp\logic\TypeLogic;
use common\helpers\Paging;

class TypeController extends BaseController
{
    /**
     * 列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 15:32
     * @return string
     */
    public function actionIndex()
    {
        $res = TypeLogic::getIndex();

        $this->_view['flag']     = $res['flag'];
        $this->_view['data']     = $res['list'];
        return $this->render('index');
        
    }
    
    /**
     * 添加
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 15:33
     * @return mixed|string|void
     */
    public function actionAdd()
    {
        
        $data = TypeLogic::beforeAdd();

        if (Yii::$app->request->isPost)
        {
            
            $res = TypeLogic::add();
            return json_encode($res);
        }
        
        return $this->renderPartial('add',
            [
                'type_ids'    => $data['type_ids'],
                'type_list'   => $data['type_list'],
            ]);
    }
    
    /**
     * 改变状态
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 15:33
     * @return mixed|string|void
     */
    public function actionChangeStatus()
    {
        
        $res = TypeLogic::changeStatus();
        
        return json_encode($res);
        
    }
    /**
     * 改变状态  启用
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 15:33
     * @return mixed|string|void
     */
    public function actionChangeStatusOn()
    {

        $res = TypeLogic::changeStatus();

        return json_encode($res);

    }

}


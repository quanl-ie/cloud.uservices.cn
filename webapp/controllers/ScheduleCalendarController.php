<?php

namespace webapp\controllers;

use Yii;
use webapp\logic\BaseLogic;
use common\helpers\Paging;
use webapp\logic\ScheduleManageLogic;
use webapp\models\Department;

/**
 *@desc:工作日历
 * @author:
 * createdAt: 2018/5/14
 *
 */


class ScheduleCalendarController extends BaseController
{

    public function actionIndex(){

        //生成查询条件
        $result = ScheduleManageLogic::index($this->dataAuthId);

        //分页拼成数据
        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }
        if($result['totalPage'] >=1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], $getStr."&page=");
        }

        //获取当前用户的登陆信息
        $user_info = BaseLogic::getLoginUserInfo();


        //本公司及以下权限赋予本公司权限
        if($this->dataAuthId==3){
            $this->dataAuthId=2;
        }

        //生成查询条件
        $info = self::departmentforSearch();

        //搜索选择框的技师数据
        $result1 = ScheduleManageLogic::index($this->dataAuthId);


        $params['department_id']      =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']        = $params;
        $this->_view['dataAuth']      = $this->dataAuthId;
        $this->_view['department']    = $info;
        $this->_view['auth']          = $this->dataAuthId;
        $this->_view['company_id']    = $this->directCompanyId;
        $this->_view['department_id'] = $user_info->department_id;
        $this->_view['data']          = $result['list'];
        $this->_view['data1']         = $result1['list'];
        $this->_view['group']         = $result['group'];
        $this->_view['pageHtml']      = isset($pageHtml)?$pageHtml:' ';
        $this->_view['departpment_name']= isset($result['departpment_name']) ? $result['departpment_name'] : '';
        return $this->render('index');

    }

    /**
     * @return string
     * 工作日历导出
     */
    public function actionExportData()
    {

        //生成查询条件
        $result = ScheduleManageLogic::index($this->dataAuthId);

        //分页拼成数据
        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }
        if($result['totalPage'] >=1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], $getStr."&page=");
        }

        //获取当前用户的登陆信息
        $user_info = BaseLogic::getLoginUserInfo();


        //本公司及以下权限赋予本公司权限
        if($this->dataAuthId==3){
            $this->dataAuthId=2;
        }

        //生成查询条件
        $info = self::departmentforSearch();

        //搜索选择框的技师数据
        $result1 = ScheduleManageLogic::index($this->dataAuthId);


        $params['department_id']      =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']        = $params;
        $this->_view['dataAuth']      = $this->dataAuthId;
        $this->_view['department']    = $info;
        $this->_view['auth']          = $this->dataAuthId;
        $this->_view['company_id']    = $this->directCompanyId;
        $this->_view['department_id'] = $user_info->department_id;
        $this->_view['data']          = $result['list'];
        $this->_view['data1']         = $result1['list'];
        $this->_view['group']         = $result['group'];
        $this->_view['pageHtml']      = isset($pageHtml)?$pageHtml:' ';
        $this->_view['departpment_name']= isset($result['departpment_name']) ? $result['departpment_name'] : '';
        return $this->render('index');

    }

    /**
     * 根据用户权限查询需要出现的数据的所属机构ID
     */
    public function actionGetSreachId() {
        //当前登录的部门ID
        $department_id =  $this->departmentId;
        $department_type = $this->departmentObj->type;

        //获取当前部门
        $department = Department::getDepartmentInfo($department_id,1,0,1);
        //查询是否还有下级
        $nextDepartment = Department::getDepartmentInfo($department_id,0,1,1);
        if(!empty($nextDepartment)){
            $department[0]['exist'] = 1; //有下级
        }else{
            $department[0]['exist'] = 2; //无
        }
        //根据用户数据权限查询当前用户应该出现的数据
        if($this->dataAuthId == 3)//本公司及以下权限
        {
            $top_id = $department_id;
            //如果当前机构不是公司级别的
            if($department_type != 1) {
                //获取当前机构直属公司ID
                $top_id = $this->directCompanyId;
            }
            //根据当前直属公司ID获取公司下所有子部门（不包含公司）的ID
            $findDepartmentsArr = Department::find()->where(['direct_company_id' => $top_id, 'type' => 3])->asArray()->all();
            $findDepartmentsId = array_column($findDepartmentsArr, 'id');
            array_push($findDepartmentsId,$top_id);
        } else if($this->dataAuthId == 2)//本公司的权限
        {
            $top_id = $department_id;
            //如果当前机构不是公司级别的
            if($department_type != 1) {
                //获取当前机构直属公司ID
                $top_id = $this->directCompanyId;
            }
            //根据当前直属公司ID获取公司下所有子部门（不包含公司）的ID
            $findDepartmentsArr = Department::find()->where(['direct_company_id' => $top_id, 'type' => 3])->asArray()->all();
            $findDepartmentsId = array_column($findDepartmentsArr, 'id');
            array_push($findDepartmentsId,$top_id);
        } else if($this->dataAuthId == 5)//本部门及以下(如果上级机构为公司，当前用户数据权限为部门时，不展示)
        {
            $findDepartmentsId = [];
            $top_id = $this->directCompanyId;
            //如果当前机构不是部门
            if($department_type == 3) {
                $top_id = $department_id;
                //根据当前直属公司ID获取公司下所有子部门（不包含公司）的ID
                $findDepartmentsArr = Department::find()->where(['direct_company_id' => $top_id, 'type' => 3])->asArray()->all();
                $findDepartmentsId = array_column($findDepartmentsArr, 'id');
                array_push($findDepartmentsId,$top_id);
            }
        } else if($this->dataAuthId == 4)//本部门权限(如果上级机构为公司，当前用户数据权限为部门时，不展示)
        {
            $top_id = $findDepartmentsId[] = $department_id;
            if($department_type != 3) {
                $top_id = $findDepartmentsId = [];
            }
        } else {//如果数据权限为本人展示为空
            $findDepartmentsId = [];
            $top_id = 0;
        }
        return ['findDepartmentsId' => $findDepartmentsId, 'top_id' => $top_id];
    }


}


<?php

namespace webapp\controllers;


use Yii;
use webapp\logic\BaseLogic;
use webapp\models\BaseModel;
use yii\web\Controller;
use common\helpers\Paging;
use webapp\logic\SupplierLogic;
use common\models\Region;
use common\models\Supplier;
/**
 *@desc:供货商管理
 *
 */

class SupplierController extends BaseController
{

    /**
     * 函数用途描述:供货商列表
     * @date: 2018年3月21日 下午17:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionIndex()
    {
        $result = SupplierLogic::getList();
        //print_r($result);die;
        $pageHtml = '';
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }
        return $this->render('index',[
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params'],
            'levelSelect'   =>$result['levelSelect']
        ]);
    }
    /**
     * 函数用途描述:添加/编辑供货商
     * @date: 2018年3月21日 下午3:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionAdd()
    {
        if (Yii::$app->request->isPost){
            //判断是否存在id 区分添加和修改
            if (yii::$app->request->post('id',0) != 0) {
                $res = SupplierLogic::edit();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/supplier/index');
            }else{
                $res = SupplierLogic::add();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/supplier/index');
            }
            //return json_encode($res);
        }
        $infos = [];
        if (yii::$app->request->isGet) {
            $id      = yii::$app->request->get('id',0);
            //获取库房信息
            $infos = SupplierLogic::getSupplierInfo($id);
        }
        $province = Region::getList(['parent_id'=>1]);
        $levelSelect = SupplierLogic::getLevelSelect();
        return $this->render('add',[
            'infos'=>$infos,
            'province'=>$province,
            'levelSelect'   =>$levelSelect
        ]);
    }
    /**
     * 函数用途描述:编辑供货商
     * @date: 2018年3月21日 下午3:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionEdit()
    {
        if (Yii::$app->request->isPost){
            //判断是否存在id 区分添加和修改
            if (yii::$app->request->post('id',0) != 0) {
                $res = SupplierLogic::edit();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/supplier/index');
            }else{
                $res = SupplierLogic::add();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/supplier/index');
            }
            //return json_encode($res);
        }
        $infos = [];
        if (yii::$app->request->isGet) {
            $id      = yii::$app->request->get('id',0);
            //获取库房信息
            $infos = SupplierLogic::getSupplierInfo($id);
        }
        $province = Region::getList(['parent_id'=>1]);
        $levelSelect = SupplierLogic::getLevelSelect();
        return $this->render('add',[
            'infos'=>$infos,
            'province'=>$province,
            'levelSelect'   =>$levelSelect
        ]);
    }
    //验证信息重复性
    public function actionVerifyData(){
        if (yii::$app->request->isPost) {
            $post = yii::$app->request->post();
            $no = $post['str'];
            $id = $post['id'];
            //print_r($post['str']);exit;
            if($no != ''){
                //验证数据
                $res = Supplier::getOne(['no'=>$no,'direct_company_id'=> $this->directCompanyId]);
                if($id){
                    if($res && $id != $res['id']){
                        return json_encode(['msg'=>'供货商编号已存在！','code'=>'20002'],true);
                    }
                }else{
                    if($res){
                        return json_encode(['msg'=>'供货商编号已存在！','code'=>'20002'],true);
                    }
                }
            }

            return json_encode(['msg'=>'ok','code'=>'200']);
        }
    }
    /**
     * 函数用途描述:禁用
     * @date: 2018年3月21日 下午17:23:05
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionSetStatus()
    {
        $t    = intval(Yii::$app->request->get('t',1));
        $id   = intval(Yii::$app->request->get('id',0));
        if ($id<= 0) {
            return $this->redirect('/supplier/index');
        }
        if($t == 1){
            $status = 0;
        }
        if($t == 0){
            $status = 1;
        }
        $where = [
            'id'        => $id,
            'status'    => $t,
            'direct_company_id'    => BaseLogic::getDirectCompanyId()
        ];
        $model = Supplier::findOne($where);
        if (!empty($model)) {
            $model->status = $status;
            if ($model->save(false)) {
                Yii::$app->session->setFlash('message','操作成功!');
                return $this->redirect ('/supplier/index');
            }
        }
        return $this->redirect('/supplier/index');
    }
    /**
     * 函数用途描述:启用供货商账号
     * @date: 2018年3月21日 下午17:23:05
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionSetStatusOff()
    {
        $t    = intval(Yii::$app->request->get('t',1));
        $id   = intval(Yii::$app->request->get('id',0));
        if ($id<= 0) {
            return $this->redirect('/supplier/index');
        }
        if($t == 1){
            $status = 0;
        }
        if($t == 0){
            $status = 1;
        }
        $where = [
            'id'        => $id,
            'status'    => $t,
            'direct_company_id'    => BaseLogic::getDirectCompanyId()
        ];
        $model = Supplier::findOne($where);
        if (!empty($model)) {
            $model->status = $status;
            if ($model->save(false)) {
                Yii::$app->session->setFlash('message','操作成功!');
                return $this->redirect ('/supplier/index');
            }
        }
        return $this->redirect('/supplier/index');
    }
    /**
     * 函数用途描述:获取效用供货商列表
     * @date: 2018年3月21日 下午17:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionGetList()
    {
        $result = SupplierLogic::getList(1);
        //print_r($result);die;
        $pageHtml = '';
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }
        return $this->renderPartial('list',[
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params'],
            //'levelSelect'   =>$result['levelSelect']
        ]);
    }


}


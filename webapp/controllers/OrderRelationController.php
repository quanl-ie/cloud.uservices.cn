<?php

namespace webapp\controllers;

use Yii;
/* use app\models\Order;
use app\models\ServiceBrand;
use app\models\ServiceClass;
use app\models\ServiceType;
use app\models\ServiceBrandClass;
use app\models\ServiceClassType;
use app\controllers\BaseController;
use app\models\OrderRemind;
use app\models\Region;
use app\models\OrderRelation;
use app\models\OrderImages; */


use webapp\models\Order;
use webapp\models\ServiceBrand;
use webapp\models\ServiceClass;
use webapp\models\ServiceClassType;
use webapp\models\ServiceBrandClass;
use webapp\models\ServiceType;
use webapp\models\Region;
use app\models\StoreMember;
use app\models\OrderRemind;
use webapp\models\BrandQualification;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use webapp\controllers\BaseController;
use app\models\OrderRelation;
use common\helpers\Helper;
use yii\base\BaseObject;
/**
 * OrderRelationController implements the CRUD actions for Order model.
 */
class OrderRelationController extends BaseController
{
    /**
	 * @desc 显示订单列表
	 * @date 2017-09-01
	 * @author lxq < lxq@c-ntek.com >
	 * @return  mixed
	 */
    public function actionIndex()
    {
        $orderId = OrderRelation::OrderId();
        // 创建一个 DB 查询来获得所有 status 为 1 的文章
        $query = Order::find()->where(['in','id',$orderId]);
        //过滤参数
        $filter = ['type_id' => '-1', 'class_id' => '-1','order_status' => '-1' , 'start_time' => '', 'end_time' => ''];
        if (yii::$app->request->isPost) {
            $filter['type_id']      = Yii::$app->request->post('type_id');
            if ($filter['type_id'] != -1) {
                $query->andFilterWhere(['type_id' => $filter['type_id']]);
            }
            $filter['class_id']     = Yii::$app->request->post('class_id');
            if ($filter['class_id'] != -1) {
                $query->andFilterWhere(['class_id' => $filter['class_id']]);
            }
            $filter['order_status'] = Yii::$app->request->post('order_status');
            if ($filter['order_status'] != -1) {
                $query->andFilterWhere(['order_status' => $filter['order_status']]);
            }
            $filter['start_time']   = Yii::$app->request->post('start_time');
            $filter['end_time']     = Yii::$app->request->post('end_time'); 
            if(!empty($filter['start_time']) && !empty($filter['end_time']) ){
                $startTime = OrderRelation::Time($filter['start_time'],'0');
                $endTime   = OrderRelation::Time($filter['end_time'],'1');
                $query->andFilterWhere([
                            'and',
                            ['>=', 'create_time', $startTime],
                            ['<=', 'create_time', $endTime]
                        ]);
            }
        }
        $data = OrderRelation::OrderPagination($query);
        return $this->render('index', [
            'data'   => $data,
            'filter' => $filter,
        ]);
    }    
    /**
	 * @desc 取消订单
	 * @date 2017-09-04  18：00
	 * @author lxq < lxq@c-ntek.com >
	 * @return  mixed
	 */
    public function actionRemove($id)
    {
        $model =  Order::find()->where(['id'=>$id])->one();
        if($model->order_status == 5){
            Yii::$app->session->setFlash('message','订单状态已变更，不能取消');
            return $this->redirect(['index']);
        }
        if (Order::UnRemove($id) == true) {
            Yii::$app->session->setFlash('message','订单状态已变更，不能取消');
            return $this->redirect(['index']);
        }
        if (empty($model)) {
            Yii::$app->session->setFlash('message','取消失败');
            return $this->redirect(['index']);
        }
        $error = $model->getErrors();
        if (empty($error)) {
            $model->order_status = 5;  //置为取消状态  1等待接单, 2：等待服务 ，3：服务中, 4：已完成, 5：取消
            $model->update_time = time();
            $result = $model->save(false);
            if ($result) {
                Yii::$app->session->setFlash('message','成功');
            }
        }
        return $this->redirect(['index']);
    }
    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = Order::find()->where(['id'=>$id])->one();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
    * 函数用途描述:添加订单
    * @date: 2017年9月1日 下午3:54:49
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionCreate()
    {
        $order      = new Order();
        //获取父级产品类型
        $data['class']          = ServiceClass::getServiceClass();
        //获取服务类型
        $data['type']           = Order::getType();
        //获取已有品牌
        $data['brand']          = Order::getBrand();
        //获取已经开通省份数据
        $data['province']       = Region::getOpenProvince();
        //获取当天开始时间
        $data['start_time']     = Order::getTime();
        $time = time();
        //获取当前登陆的用户信息
        $userInfo['id']         = yii::$app->user->identity->id;  //当然登陆人员id
        $userInfo['mobile']     = yii::$app->user->identity->mobile; //当前登陆的手机号
        if(!$userInfo['id']){
            return '当前用户不存在';
        }
        if(Yii::$app->request->post()){
            $subData = Yii::$app->request->post();
            //print_r($subData);exit;
            $image = $subData['Order']['image'];
            unset($subData['Order']);
            //过滤post参数
            foreach ($subData as $key =>$val) {
                $subData[$key] = htmlspecialchars(trim($val));
            }
            if (is_array($image) && $image) {
                foreach ($image as $key =>$val) {
                    $image[$key] = htmlspecialchars(trim($val));
                }
            }else{
                $image = '';
            }
            //如果有产品类型子分类则产品类型id为子分类的id
            $params['class_id']     = (int)$subData['order_class_id'];
            if(!empty((int)$subData['order_class_s_id']) && (int)$subData['order_class_s_id'] != '0'){
                $params['class_id']   = (int)$subData['order_class_s_id'];
            }
            $params['brand_id']     = (int)$subData['order_brand_id'];
            $params['type_id']      = (int)$subData['order_type_id'];
            $params['note']         = $subData['order_note'];
            $params['total_price'] = intval($subData['total_price']);
            $params['price']        = 0;
            $params['order_status'] = '1';
            $params['name']         = $subData['order_name'];
            $params['tel']          = $subData['order_tel'];
    
            $params['province']     = Region::getCityName($subData['order_province_id']);
            $params['city']         = Region::getCityName($subData['order_city']);
            $params['district']     = Region::getCityName($subData['order_district']);
            $params['address']      = $subData['order_address'];
            //判断预约时间类型
            //order_make_time_type  1 2小时内立即上门   2 预约上门
            $params['make_time'] = $params['create_time'] = $params['update_time'] = $time;
            if ($subData['order_make_time_type'] == '2' && !empty($subData['make_time'])){
                $params['make_time'] = strtotime($subData['make_time']);
                $params['time_type'] = $subData['order_make_time_type'];
            }else{
                $params['time_type'] = '1';
                $params['make_time'] = $time+60*60*2;
            }
            $params['is_pay'] = '0';

            //支付类型
            $relation['pay_type']     = $subData['order_pay_type'];
            //订单来源
            $source = Order::ORDER_SOURCE; //订单来源 1微信公众号 2移动浏览器 3PC端 4服务商
            $relation['source']       = $source[3];
            //获取规则生成订单id
            $token = Yii::$app->params['token'];
            //$params['order_id']     = Order::getOrderNumber($token,$params['source'],$params['pay_type']);
            $params['order_id']     = Order::getOrderNumber($token,$relation['source'],$relation['pay_type']);
    
            //获取服务商id
            $relation['manufactor_id']       = $userInfo['home_id'];
            //获取服务商操作人员id
            $relation['manufactor_user_id']  = $userInfo['id'];
            $relation['city_parent_id']       = $subData['order_province_id'];
            $params['open_city_id']           = $subData['order_city'];
            $relation['district_id']          = $subData['order_district'];
            $params['goods_title']            = ServiceClass::getTitle($id=$params['class_id'])."(".ServiceBrand::getTitle($id=$params['brand_id']).")";
            $params['serve_type']             = ServiceType::getTitle($id=$params['type_id']);
            $params['buy_type']               = 3;    //支付购买方式 1微信 2支付宝 3线下支付
            $params['buy_distinction']        = 2;
            $params['latlon_address']         = $params['province'].$params['city'].$params['district'].$params['address'];
            $params['lat']                    = $subData['lat'];
            $params['lon']                    = $subData['lon'];
            $params['create_time']            = $params['update_time'] = $time;
            $params['coupon_id'] = ''; //优惠券id
            $params['address_id'] = '';//地址id
            $img_list = $image;//故障图片
            if(is_array($img_list) && $img_list){
                $params['image'] = $img_list;
            }else {
                $params['image'] = '';
            }
            //print_r($params);exit;
            ////////////将数据传入到下单接口///////////////
            $mem_tel        = $userInfo['mobile']; //登陆用户的手机号
            $from           = '4001'; //来源系统
            $buyDistinction = 3;//浏览器类型 1微信浏览器，2移动端浏览器，3PC端浏览器
            $post_data = array('mem_tel' => $mem_tel, 'data'=>json_encode($params), 'from' => $from, 'buyDistinction' => $buyDistinction);
            $url = Yii::$app->params['apiUrl'];
            $fiedCode = Helper::curlPost($url,$post_data);
            $repot  = json_decode($fiedCode);
            //print_r($repot);exit;
            if($repot->code == '1' && $repot->id){
                //写入关联数据
                //开始插入数据
                $transaction = Yii::$app->db->beginTransaction();
                try{
                    //插入订单关联表manufactor_order_relation
                    $orderRelation = new OrderRelation();
                    $orderRelation->order_id            = $repot->id;
                    $orderRelation->pay_type            = $relation['pay_type'];
                    $orderRelation->source              = $relation['source'];
                    $orderRelation->manufactor_id      = $relation['manufactor_id'];
                    $orderRelation->manufactor_user_id = $relation['manufactor_user_id'];
                    $orderRelation->error_description   = $params['note'];
                    $orderRelation->city_parent_id      = $relation['city_parent_id'];
                    $orderRelation->city_id             = $params['open_city_id'];
                    $orderRelation->district_id         = $relation['district_id'];
                    $orderRelation->created_at          = $orderRelation->updated_at = $time;
                    if ( !$orderRelation->save() ) {
                        $transaction->rollBack();
                        $orderRelation->addError('orderRelation','添加订单关联失败！');
                    }
                    $error = $orderRelation->getErrors();
                    if(!empty($error)){
                        return json_encode($error);
                    }
                    $error = $order->getErrors();
                    if (empty($error)) {
                        $transaction->commit();
                        $id = $repot->id;
                        return $this->redirect(array('/order-relation/wait','id'=>$id));
                    }
                }catch (\Exception $e){
                    $transaction->rollBack();
                    throw $e;
                }
            }else {
            return $this->render('create', [
                    'data' =>$data,
                    'model'=>$order
            ]);
        }
        } else {
            return $this->render('create', [
                    'data' =>$data,
                    'model'=>$order
            ]);
        }
    }
    public function actionWait(){
        $orderId    = Yii::$app->request->get('id','');
        $data       = [];
        $timeOut    = 60*10;    //超时查询时间
        if($orderId != '' && $orderId !='0'){
            $orderInfo=Order::getOrderInfo($orderId);
        }
        if(!empty($orderInfo)){
            $data       = OrderRemind::getStatus($orderId);
            if(empty($data)){
                $data['order_status'] = '';
            }
            $data['create_time']    = $orderInfo['create_time'];
            $data['time_out']       = $orderInfo['create_time']+$timeOut;
            $data['dif_time']       = ($data['time_out']-time() <= 0) ? 0 : $data['time_out']-time();  //超时时间
            $data['order_id']       = $orderInfo['id'] ;
            $data['show_last_time'] = date("i:s",$data['dif_time']);
            $data['order_status']   =$orderInfo['order_status'];
        }
        return $this->render('wait',['data'=>$data]);
    }
    /**
     * 函数用途描述:根据分类父id获取子类数据
     * @date: 2017年9月5日 下午8:09:22
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: $GLOBALS
     * @return:
     */
    public function actionGetClass()
    {
        $parentId   = Yii::$app->request->post('parentId','');
        $data       = ServiceClass::getServiceClass($parentId);
        $str        = '';
        if(!empty($data)){
            $str = "<option value=''>请选择</option>";
            foreach ($data as $k => $v) {
                $str.= "<option value=$k>$v</option>";
            }
        }
        return $str;
    }
    /**
    * 函数用途描述:根据产品类型获取对应服务类型数据
    * @date: 2017年9月14日 下午4:49:41
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionGetClassType() {
        $classId    = Yii::$app->request->post('class_id','');
        $data       = ServiceClassType::getClassType($classId);
        $type       = ServiceType::getTypeName();
        if(!empty($data) && !empty($type)){
            foreach ($data as $k=>$v){
                $data[$k]['title'] = $type[$v['type_id']];
            }
        }
        $str = '';
        if(!empty($data)){
            $str = "<option value=''>请选择</option>";
            foreach ($data as $k => $v) {
                $str.= '<option value="'.$v['type_id'].'">'.$v['title'].'</option>';
            }
        }
        return $str;
    }
    /**
    * 函数用途描述:根据产品类型获取对应品牌数据
    * @date: 2017年9月14日 下午6:21:56
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionGetBrandClass() {
        $classId    = Yii::$app->request->post('class_id');
        $data       = ServiceBrandClass::getBrandClass($classId);
        $brand      = ServiceBrand::getBrandName();
        $arr = [];
        foreach ($data as $k => $v) {
            if (isset($brand[$v['brand_id']])) {
                $arr[$k]['title'] =$brand[$v['brand_id']];
                $arr[$k]['brand_id'] =$v['brand_id'];
            }
        }
        $str = '';
        if(!empty($arr)){
            $str = "<option value=''>请选择</option>";
            foreach ($arr as $k => $v) {
                $str.= '<option value="'.$v['brand_id'].'">'.$v['title'].'</option>';
            }
        }
        return $str;
    }
    /**
     * 函数用途描述:根据品牌id获取上门服务费
     * @date: 2017年9月6日 上午10:02:20
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionGetBrandPrice(){
        $brandId    = Yii::$app->request->post('brand_id','');
        $data       = ServiceBrand::getPrice($brandId);
        return isset($data['price'])?$data['price']:'30.00';
    }
    /**
     * 函数用途描述:根据分类省份id获取子类城市数据
     * @date: 2017年9月5日 下午8:53:01
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionGetCity()
    {
        $province   = Yii::$app->request->post('province','');
        $data       = Region::getOpenCity($province);
        $str        = "<option value=''>市</option>";
        foreach ($data as $v) {
            $str.= "<option value=".$v['region_id'].">".$v['region_name']."</option>";
        }
        return $str;
    }
    /**
     * 函数用途描述:根据城市获取区县
     * @date: 2017年9月7日 下午6:22:04
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionGetDistrict()
    {
        $city_id    = Yii::$app->request->post('city_id','');
        $data       = Region::getOpenDistrict($city_id);
        $str        = "<option value=''>区</option>";
        foreach ($data as $v) {
            $str.= "<option value=".$v['region_id'].">".$v['region_name']."</option>";
        }
        //var_dump($str);die;
        return $str;
    }
    /**
     * Finds the GoodsManagement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return GoodsManagement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * 函数用途描述:获取订单状态
     * @date: 2017年9月11日 下午5:08:26
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionGetData(){        
        $param = Yii::$app->request->post();
        $param['order_id'] = 394;
        $arr = [];
        if(!empty($param['order_id'])){
            //查询订单状态
            $data = OrderRemind::getStatus($param['order_id']);
            print_r($data);exit;
            $orderIonfo = Order::getOrderInfo($param['order_id']);
            if($data['order_status'] == 3){
                $info = StoreMember::getInfos($orderIonfo['technician_id']);
                $info['order_id'] = $param['order_id'];
                $arr=array('success'=>"1",'name'=>'技师已接单,信息如下','text'=>$data['order_status'],'info'=>$info);
                return json_encode($arr);
            }
            if($data['order_status'] == 2){
                $arr=array('success'=>"0",'name'=>'无技师接单','text'=>$data['order_status'],'info'=>$orderIonfo['name']);
                return json_encode($arr);
            }
            return json_encode($arr);
        }
        return json_encode($arr);
    }
}

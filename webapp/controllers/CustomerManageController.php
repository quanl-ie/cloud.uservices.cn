<?php

namespace webapp\controllers;

use Yii;
use webapp\logic\CustomerManageLogic;
use yii\web\Controller;

/**
 *@desc:客户管理
 * @author: chengjuanjuan <chengjuanjuan@c-ntek.com>
 * createdAt: 2017/12/14 14:09
 *
 */


class CustomerManageController extends Controller
{
    //
    function actionTest(){
        $typeList         =  CustomerManageLogic::getTypeList();
         $classList       =  CustomerManageLogic::getClassList($pid=26);
        $brandList        =  CustomerManageLogic::getBrandList();
        $oneBrand         =  CustomerManageLogic::getOneBrand();
        $getOpenProvince  =  CustomerManageLogic::getOpenProvince();
        $getOpenCity      =  CustomerManageLogic::getOpenCity(2);
        $OpenDistrict     =  CustomerManageLogic::getOpenDistrict(38);
        echo '<pre>';
        print_R($OpenDistrict);
    }
}


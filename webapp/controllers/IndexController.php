<?php
namespace webapp\controllers;

//use webapp\logic\OrderLogic;
use webapp\logic\OrderLogic;
use webapp\models\BaseModel;
use Yii;
use webapp\controllers\BaseController;
use common\helpers\SendSmg;
use common\helpers\Helper;
use webapp\logic\MessageLogic;

/**
 * 首页
 * @author liwenyong<liwenyong@c-ntek.com>
 * @date    2017-08-17
 */
class IndexController extends BaseController
{
    public function actionIndex()
    {
        //实时指标
        $_GET['pageSize'] = '1';
        //1.待审核
        $_GET['status'] = '10';
        $result = OrderLogic::getList();
        if ($result['code'] == 200) {
            $data['waitAudit'] = $result['data']['totalCount'];
        } else {
            $data['waitAudit'] = 0;
        }

        //2.待指派
        $_GET['status'] = '1_8';
        $result = OrderLogic::getList();
        if ($result['code'] == 200) {
            $data['waitAssign'] = $result['data']['totalCount'];
        } else {
            $data['waitAssign'] = 0;
        }

        //3.待服务
        $_GET['status'] = '2_9';
        $result = OrderLogic::getList();

        if ($result['code'] == 200) {
            $data['waitService'] = $result['data']['totalCount'];
        } else {
            $data['waitService'] = 0;
        }

        //4.服务中
        $_GET['status'] = 3;
        $result = OrderLogic::getList();
        if ($result['code'] == 200) {
            $data['inService'] = $result['data']['totalCount'];
        } else {
            $data['inService'] = 0;
        }

        //5.待验收
        $_GET['status'] = '4_7';
        $result = OrderLogic::getList();
        if ($result['code'] == 200) {
            $data['waitConfirm'] = $result['data']['totalCount'];
        } else {
            $data['waitConfirm'] = 0;
        }

        //6.已完成
        $_GET['status'] = '5';
        $result = OrderLogic::getList();

        if ($result['code'] == 200) {
            $data['finish'] = $result['data']['totalCount'];
        } else {
            $data['finish'] = 0;
        }

        //7.被驳回
        $_GET['status'] = '8';
        $result = OrderLogic::getList();
        if ($result['code'] == 200) {
            $data['serviceProvidersReject'] = $result['data']['totalCount'];
        } else {
            $data['serviceProvidersReject'] = 0;
        }

        //8.上门超时

        $_GET['status'] = '9';
        $result = OrderLogic::getList();
        if ($result['code'] == 200) {
            $data['serviceTimeout'] = $result['data']['totalCount'];
        } else {
            $data['serviceTimeout'] = 0;
        }

        //消息信息
        //$data = MessageLogic::getMainId();
        //获取实时消息
        $statisticsData = $message['list'] = [];
        //查出菜单信息
        $selfMenuArr = [];
        $role_ids = isset(Yii::$app->user->getIdentity()->role_ids)?Yii::$app->user->getIdentity()->role_ids:'';
        if($role_ids){
            $selfMenuArr = self::getCacheSelfMenu($role_ids,$this->departmentId);
        }
        return $this->render('index',[
            'statistics'=> $statisticsData,
            'orderData' => $data,
            'message'   => $message['list'],
            'selfRoles' =>isset($selfMenuArr['url'])?$selfMenuArr['url']:[]
        ]);
    }

    /**
     * 首页
     */
    public function actionMain()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect('/site/login');
        }

        $this->layout = 'main-v2';

        $backurl = Yii::$app->request->get('backurl');
        if($backurl != ''){
            $backurl = str_replace(Yii::$app->params['fws.uservices.cn'],'',$backurl);
        }
        Yii::$app->params['backurl'] = urldecode($backurl);
        Yii::$app->params['avatar'] = trim($this->user->avatar)?$this->user->avatar:'/images/logo.png';

        return $this->render('main');
    }
}

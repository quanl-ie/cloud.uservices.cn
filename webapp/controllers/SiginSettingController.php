<?php
namespace webapp\controllers;

use webapp\logic\SiginSettingLogic;
use webapp\models\BaseModel;
use Yii;
use webapp\controllers\BaseController;

class SiginSettingController extends BaseController
{
    /**
     * 服务商设置技师签到
     * @author
     * @date
     * @return
     */
    public function actionIndex()
    {
        $data = SiginSettingLogic::BeforeList();
        if ($data) {
            $this->_view['list']    = $data['list'];
            $this->_view['option'] = $data['option'];
        }

        if (Yii::$app->request->isPost) {
            $res = SiginSettingLogic::edit();
            Yii::$app->session->setFlash('sys_config_message',$res['message']);
            return $this->redirect('/sigin-setting/index');
        }

        return $this->render('index');
    }
      //支付开关
      public function actionSetPay(){
            $data = SiginSettingLogic::BeforeList("pay");
            if ($data) {
                  $this->_view['list']    = $data['list'];
                  $this->_view['option'] = $data['option'];
            }

            if (Yii::$app->request->isPost) {
                  $res = SiginSettingLogic::edit("pay");
                  Yii::$app->session->setFlash('sys_config_message',$res['message']);
                  return $this->redirect('/sigin-setting/set-pay');
            }

            return $this->render('set-pay');
      }
      //签名开关
      public function actionSetSign(){
            $data = SiginSettingLogic::BeforeList("sign");
            if ($data) {
                  $this->_view['list']    = $data['list'];
                  $this->_view['option'] = $data['option'];
            }

            if (Yii::$app->request->isPost) {
                  $res = SiginSettingLogic::edit("sign");
                  Yii::$app->session->setFlash('sys_config_message',$res['message']);
                  return $this->redirect('/sigin-setting/set-sign');
            }

            return $this->render('set-sign');
      }
}

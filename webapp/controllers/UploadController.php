<?php
namespace webapp\controllers;

use Yii;
use yii\web\Controller;
//use yii\filters\VerbFilter;
//use yii\filters\AccessControl;
//use common\models\LoginForm;
use common\components\Upload;
use yii\helpers\Json;

/**
 * 图片上传
 */
class UploadController extends Controller
{
    public function init()
    {
        $this->enableCsrfValidation = false;
    }

    public function actionUpload($source = null)
    {
        $this->enableCsrfValidation = false;
        $source = Yii::$app->request->get('source','');
        try {
            $model = new Upload();
            $info = $model->upImage($source);


            $info && is_array($info) ?
            exit(Json::htmlEncode($info)) :
            exit(Json::htmlEncode([
                'code' => 1,
                'msg' => 'error'
            ]));


        } catch (\Exception $e) {
            exit(Json::htmlEncode([
                'code' => 1,
                'msg' => $e->getMessage()
            ]));
        }
    }
    public function actionIndex()
    {
        $url = Yii::$app->params['webuploader']['uploadUrl'];
        print_r($url);exit();
        $path = $relativePath = Yii::$app->params['imageUploadRelativePath'];
        $dir = date('Ymd', time());
        //$c_relativePath = date('Ymd', time());
        $relativePath = $path.'/'.$dir.'/';
        if (!is_dir($relativePath)) {
            FileHelper::createDirectory($c_relativePath);
        }
        var_dump($path.$dir.'/');die();

        $dir = date('Ymd', time());
        print_r($dir);exit();
        print_r( Yii::$app->params['webuploader']['uploadUrl']);exit;
        echo '1111';exit();
    }

    /***
     * @param null $source
     * sxz 2018-8-2
     * 上传多种格式文件
     */
    public function actionUploadOther($source = null)
    {
        $this->enableCsrfValidation = false;
        $source = Yii::$app->request->get('source','');
        try {
            $model = new Upload();
            $info = $model->upOtherType($source);


            $info && is_array($info) ?
                exit(Json::htmlEncode($info)) :
                exit(Json::htmlEncode([
                    'code' => 1,
                    'msg' => 'error'
                ]));


        } catch (\Exception $e) {
            exit(Json::htmlEncode([
                'code' => 1,
                'msg' => $e->getMessage()
            ]));
        }
    }
}
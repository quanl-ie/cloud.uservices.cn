<?php
namespace webapp\controllers;

use common\components\Upload;
use common\lib\openapi\MiniProgramBasic;
use common\lib\openapi\MiniProgramCode;
use common\lib\openapi\MiniProgramCodeTemplate;
use common\lib\openapi\MiniProgramTemplate;
use common\lib\openapi\WeChatAuthorization;
use common\lib\openapi\WXBizMsgCrypt;
use common\models\Common;
use webapp\logic\BaseLogic;
use webapp\models\MiniProgram;
use Yii;


class OpenWeixinController extends BaseController
{

    /**
     * 微信每隔10分钟推送一次
     * @author xi
     * @date 2018-4-4
     */
    public function actionIndex()
    {
        $arr = [ ];
        $postStr = isset( $GLOBALS["HTTP_RAW_POST_DATA"] ) ? $GLOBALS["HTTP_RAW_POST_DATA"] : '';
        if ( $postStr != '' )
        {
            libxml_disable_entity_loader( true );
            $arr = (array)simplexml_load_string( $postStr , 'SimpleXMLElement' , LIBXML_NOCDATA );
            if ( $arr && isset( $arr['Encrypt'] ) )
            {
                $token = WeChatAuthorization::getAppToken();
                $encodingAesKey = WeChatAuthorization::getAppEncodingAesKey();
                $appId = WeChatAuthorization::getAppId();

                $crypt = new WXBizMsgCrypt( $token , $encodingAesKey , $appId );

                $format = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><Encrypt><![CDATA[%s]]></Encrypt></xml>";
                $postData = sprintf( $format , $arr['Encrypt'] );

                $msgSignature = Yii::$app->request->get( 'msg_signature' );
                $nonce = Yii::$app->request->get( 'nonce' );
                $timeStamp = Yii::$app->request->get( 'timestamp' );

                $resXml = '';
                $res = $crypt->decryptMsg( $msgSignature , $nonce , $timeStamp , $postData , $resXml );

                if ( $resXml != '' )
                {
                    $ticketArr = (array)simplexml_load_string( $resXml , 'SimpleXMLElement' , LIBXML_NOCDATA );
                    if ( $ticketArr && isset( $ticketArr['ComponentVerifyTicket'] ) )
                    {
                        Yii::$app->cache->set( 'ComponentVerifyTicket' , $ticketArr['ComponentVerifyTicket'] );
                    }
                    else if ( isset( $ticketArr['InfoType'] ) && $ticketArr['InfoType'] == 'unauthorized' )
                    {
                        //解绑处理
                        MiniProgram::unauthorized( $ticketArr['AuthorizerAppid'] );
                    }
                }
            }
        }
    }

    /**
     * 第二步 点击进行绑定
     * @return string
     */
    public function actionBind()
    {
        //查询一下是否已绑定
        $where = [
            'direct_company_id' => BaseLogic::getDirectCompanyId(),
            'department_id'     => BaseLogic::getDepartmentId(),
            'is_authorization'  => 1,
            'status'            => 1
        ];
        $publicNumArr = MiniProgram::find()->where($where)->asArray()->one();
        if($publicNumArr)
        {
            $accessToken = $publicNumArr['access_token'];

            $message = '微信审核约7个工作日，请耐心等待！';
            if($publicNumArr['audit_status'] != 4)
            {
                switch ($publicNumArr['audit_status'])
                {
                    case 0:
                        //设置服务器域名
                        if(!$this->setDomain($accessToken,$publicNumArr['id'])){
                            break;
                        }
                    case 1:
                        //设置业务域名
                        $this->setWebViewDomain($accessToken,$publicNumArr['id']);
                    case 2:
                        //上传代码
                        if(!$this->uploadXcxCode($accessToken,$publicNumArr['appid'],$publicNumArr['id'])){
                            break;
                        }
                    case 3:
                        //提交审核
                        $this->submitAudit($accessToken,$publicNumArr['id']);
                        break;
                    case 5:
                        $message = '审核通过';
                        break;
                }
            }
            //微信审核失败
            else if($publicNumArr['weixin_audit_status'] == 3)
            {
                $message = urlencode($publicNumArr['weixin_audit_reason']);
            }

            $bindMessage = base64_encode(json_encode([
                'status'              => $publicNumArr['audit_status']==5?1:0,
                'message'             => $message,
                'weixin_audit_status' => $publicNumArr['weixin_audit_status']
            ]));
            return $this->redirect('bind-status?bindMessage='.$bindMessage);
        }
        else
        {
            $redirect_uri = Yii::$app->params['fws.uservices.cn'] . "/open-weixin/bind-success";

            $link = WeChatAuthorization::getComponentLoginPage($redirect_uri,2);

            $this->_view['link'] = $link;
            return $this->render('bind');
        }
    }

    /**
     * 第三步 绑定成功处理逻辑
     */
    public function actionBindSuccess()
    {
        $auth_code = Yii::$app->request->get( 'auth_code' );
        //有效时间
        $expires_in = Yii::$app->request->get( 'expires_in' );

        if ( $auth_code && $expires_in )
        {
            //$res = $this->bindSaveInfo( $auth_code , $this->manufactorId );
            $res = $this->bindSaveInfo( $auth_code , BaseLogic::getDepartmentId() );
            if( $res !==false )
            {
                $xcxArr = MiniProgram::find()->where(['id'=>$res])->asArray()->one();
                $accessToken = $xcxArr['access_token'];
                $xcxId = $xcxArr['id'];

                //设置服务器域名
                if($this->setDomain($accessToken,$xcxId)){
                    //设置业务域名(个人的不能设置业务域名)
                    $this->setWebViewDomain($accessToken,$xcxId);
                    //设置模板消息
                    $this->addTemplate($accessToken,$xcxId);
                    //上传代码
                    if($this->uploadXcxCode($accessToken,$xcxArr['appid'],$xcxId)){
                        //提交审核
                        $this->submitAudit($accessToken,$xcxId);
                    }
                }
            }

            $xcxArr = MiniProgram::find()->where(['id'=>$res])->asArray()->one();
            $bindMessage = base64_encode(json_encode([
                'status'              => $xcxArr['audit_status']==5?1:0,
                'message'             => '微信审核约7个工作日，请耐心等待！',
                'weixin_audit_status' => $xcxArr['weixin_audit_status']
            ]));
            return $this->redirect('bind-status?bindMessage='.$bindMessage);
        }
        else
        {
            Yii::$app->session->setFlash('message','绑定失败，请重新绑定!');
            return $this->redirect('bind');
        }
    }

    /**
     * 结果页
     * @return string|\yii\web\Response
     */
    public function actionBindStatus()
    {
        $bindMessage = trim(Yii::$app->request->get('bindMessage',''));
        if($bindMessage == ''){
            return $this->redirect('bind');
        }

        $this->_view['bindMessage'] = json_decode(base64_decode($bindMessage),true);
        return $this->render('bind-status');
    }


    /**
     * 绑定成功后进行数据保存
     *
     * @param string $auth_code
     * @return array
     * @author xi
     */
    private function bindSaveInfo( $auth_code , $sjId )
    {
        $direct_company_id = BaseLogic::getDirectCompanyId();
        $department_id     = BaseLogic::getDepartmentId();

        $arr = WeChatAuthorization::apiQueryAuth( $auth_code );

        //判断公众号是否绑定了期他公众号了
        $where = [
            'appid' => $arr['authorization_info']['authorizer_appid'],
        ];
        $model = MiniProgram::findOne($where);
        if($model && $model->is_authorization == 1 && $model->department_id!=$sjId && $model->direct_company_id == $direct_company_id)
        {
            Yii::$app->session->setFlash('message','您的微信小程序已经绑定过本系统，如有疑问请联系客服');
            header('location:/open-weixin/bind');
            die;
        }
        if(!$model){
            $model = new MiniProgram();
        }

        $model->direct_company_id      = $direct_company_id;
        $model->department_id          = $sjId;
        $model->appid         = $arr['authorization_info']['authorizer_appid'];
        $model->access_token  = $arr['authorization_info']['authorizer_access_token'];
        $model->expires_in    = ( time() + $arr['authorization_info']['expires_in'] );
        $model->refresh_token = $arr['authorization_info']['authorizer_refresh_token'];
        $model->create_time   = date( 'Y-m-d H:i:s' );
        $model->update_time   = date( 'Y-m-d H:i:s' );
        $model->is_authorization = 1;

        //公众号权限 1-15
        $func_info = [ ];
        foreach ( $arr['authorization_info']['func_info'] as $val )
        {
            $func_info[] = $val['funcscope_category']['id'];
        }

        $model->func_info = implode( ',' , $func_info );

        //查询公众号的信息
        $info = WeChatAuthorization::apiGetAuthorizerInfo( $arr['authorization_info']['authorizer_appid'] );

        if ( isset( $info['authorizer_info'] ) )
        {
            $qrcode_url = $info['authorizer_info']['qrcode_url'] ;
            if($info['authorizer_info']['qrcode_url'] != ''){
                $qrcode_url = $this->saveAvatar2local($info['authorizer_info']['qrcode_url'] );
            }

            $model->head_img          = isset( $info['authorizer_info']['head_img'] ) ? $info['authorizer_info']['head_img'] : '';
            $model->nick_name         = $info['authorizer_info']['nick_name'];
            $model->user_name         = $info['authorizer_info']['user_name'];
            $model->alias             = $info['authorizer_info']['alias'];
            $model->qrcode_url        = $qrcode_url;
            $model->service_type_info = $info['authorizer_info']['service_type_info']['id'];
            $model->verify_type_info  = $info['authorizer_info']['verify_type_info']['id'];
            $model->open_store        = $info['authorizer_info']['business_info']['open_store'];
            $model->open_scan         = $info['authorizer_info']['business_info']['open_scan'];
            $model->open_pay          = $info['authorizer_info']['business_info']['open_pay'];
            $model->open_card         = $info['authorizer_info']['business_info']['open_card'];
            $model->open_shake        = $info['authorizer_info']['business_info']['open_shake'];
            $model->principal_name    = $info['authorizer_info']['principal_name'];
            $model->signature         = $info['authorizer_info']['signature'];
            $model->network           = json_encode($info['authorizer_info']['MiniProgramInfo']['network']);
            $model->categories        = json_encode($info['authorizer_info']['MiniProgramInfo']['categories']);
            $model->visit_status      = $info['authorizer_info']['MiniProgramInfo']['visit_status'];

            if($model->save()){
                return $model->id;
            }
        }
        return false;
    }

    /**
     * 图片上传
     * @param $avatarUrl
     * @return mixed
     * @author xi
     */
    private function saveAvatar2local($avatarUrl)
    {
        $tempImageUrl = Yii::getAlias('@runtime') . '/logs/' . uniqid() . ".png";
        $res = @file_put_contents($tempImageUrl,file_get_contents($avatarUrl));
        if($res && file_exists($tempImageUrl)){
            $resultArr = Upload::uploadLocalFile($tempImageUrl,'avatar');
            if($resultArr['code'] == 0){
                unlink($tempImageUrl);
                return $resultArr['url'];
            }
        }
        return $avatarUrl;
    }

    /**
     * 设置服务器域名
     * @param $accessToken
     * @return bool
     * @throws \Exception
     */
    private function setDomain($accessToken,$id)
    {
        $requestdomain = $wsrequestdomain = $uploaddomain = $downloaddomain = [Yii::$app->params['xcx.api.uservices.cn']];
        $res = MiniProgramBasic::modifyDomain($accessToken,'set', $requestdomain,$wsrequestdomain,$uploaddomain,$downloaddomain);
        if($res['errcode'] == 0){
            return MiniProgram::changeAuditStatus($id,1);
        }
        else {
            $res['step'] = '设置服务器域名';
            MiniProgram::addMessage($id,json_encode($res));
        }
        return false;
    }

    /**
     * 设置小程序业务域名
     * @param $accessToken
     * @return bool
     * @throws \Exception
     */
    private function setWebViewDomain($accessToken,$id)
    {
        $webviewdomain = [Yii::$app->params['xcx.api.uservices.cn']];
        $res = MiniProgramBasic::setWebViewDomain($accessToken,'set', $webviewdomain);
        if($res['errcode'] == 0){
            return MiniProgram::changeAuditStatus($id,2);
        }
        else {
            $res['step'] = '设置小程序业务域名';
            MiniProgram::addMessage($id,json_encode($res));
        }
        return false;
    }

    /**
     * 上传小程序代码
     * @param $accessToken
     * @param $appid
     * @param $id
     * @return bool
     */
    private function uploadXcxCode($accessToken,$appid,$id)
    {
        $templateArr = $this->getLastTemplate($id);
        if($templateArr !==false)
        {
            $templateId = $templateArr['template_id'];

            $direct_company_id = BaseLogic::getDirectCompanyId();
            $department_id     = BaseLogic::getDepartmentId();
            $extJson = '{"extEnable": true,
                        "extAppid": "'.$appid.'",
                        "directCommit": false,
                        "ext": {
                                "departmentId":"'.$department_id.'"
                        }}';
            $userVersion = $templateArr['user_version'];
            $userDesc = 'uservices.cn';
            $res = MiniProgramCode::commit($accessToken,$templateId,$extJson,$userVersion,$userDesc);
            if($res['errcode'] == 0){
                return MiniProgram::changeAuditStatus($id,3);
            }
            else {
                $res['step'] = '上传代码';
                MiniProgram::addMessage($id,json_encode($res));
            }
        }
        return false;
    }

    /**
     * 提交审核
     * @param $accessToken
     */
    private function submitAudit($accessToken,$id)
    {
        $res = MiniProgramCode::getCategory($accessToken);
        if(isset($res['errcode']) && $res['errcode'] == 0 && $res['category_list'])
        {
            $xcxArr = MiniProgram::findOneByAttributes(['id'=>$id]);
            $xcxPageAddress = [
                'pages/index/index',
                'pages/order/order-list/order-list',
                'pages/personal/index',
                'pages/product/index',
                'pages/address/index'
            ];

            $count = 0;
            foreach ($res['category_list'] as $val)
            {
                if($count >3){
                    break;
                }

                $firstClass  = $val['first_class'];
                $secondClass = $val['second_class'];
                $firstId     = $val['first_id'];
                $secondId    = $val['second_id'];

                $itemList[]= [
                    "address"      => $xcxPageAddress[$count],
                    "tag"          => "预约安装 维修 保养",
                    "first_class"  => $firstClass,
                    "second_class" => $secondClass,
                    "first_id"     => $firstId,
                    "second_id"    => $secondId,
                    "title"        => $xcxArr['nick_name']
               ];
                $count ++;
            }

            $res = MiniProgramCode::submitAudit($accessToken,$itemList);
            if($res['errcode'] == 0){
                MiniProgram::setAuditId($id,$res['auditid']);
                MiniProgram::changeAuditStatus($id,4);
                MiniProgram::updateAll(['weixin_audit_status'=>1],['id'=>$id]);
                return true;
            }
            else {
                $res['step'] = '提交审核';
                MiniProgram::addMessage($id,json_encode($res));
            }
        }
        else {
            $res['step'] = '提交审核获取模板';
            MiniProgram::addMessage($id,json_encode($res));
        }

        return false;
    }

    /**
     * 获取体验二维码
     * @param $accessToken
     */
    public function actionTyQrCode()
    {
        $where = [
            /*'src_type' => 14,
            'src_id'   => $this->manufactorId,*/
            'direct_company_id' => BaseLogic::getDirectCompanyId(),
            'department_id'     => BaseLogic::getDepartmentId(),
            'is_authorization' => 1,
            'status' => 1
        ];
        $publicNumArr = MiniProgram::find()->where($where)->asArray()->one();
        if($publicNumArr) {
            $accessToken = $publicNumArr['access_token'];

            header("Content-type:image/png");
            $qrcode = MiniProgramCode::getQrcode($accessToken);
            print_r(file_get_contents($qrcode));
        }
    }

    /**
     * 获取模板消息id
     * @return int
     */
    private function getLastTemplate($id)
    {
        $res = MiniProgramCodeTemplate::getTemplateList();
        if(isset($res['errcode']) && $res['errcode'] == 0)
        {
            $list = $res['template_list'];
            $lastArr = end($list);
            return $lastArr;
        }
        else {
            $res['step'] = '获取模板消息id';
            MiniProgram::addMessage($id,json_encode($res));
        }
        return false;
    }

    /**
     * 加上模板消息
     * @param $id
     * @param $accessToken
     */
    private function addTemplate($accessToken,$id)
    {
        $res = MiniProgramTemplate::add($accessToken,'AT0229',[2,63,74,23]);
        if(isset($res['errcode']) && $res['errcode'] == 0)
        {
            MiniProgram::addTemplateId($id, $res['template_id']);
        }
    }

    /**
     * 重新提交审核
     * @return string
     */
    public function actionReSubmitAudit()
    {
        if(Yii::$app->request->isAjax)
        {
            $status = 0;
            $message = '提交失败,请联系我们工作人员';

            $where = [
                /*'src_type' => 14,
                'src_id'   => $this->manufactorId,*/
                'direct_company_id' => BaseLogic::getDirectCompanyId(),
                'department_id'     => BaseLogic::getDepartmentId(),
                'is_authorization' => 1,
                'status' => 1
            ];
            $publicNumArr = MiniProgram::find()->where($where)->asArray()->one();
            if($publicNumArr)
            {
                $accessToken = $publicNumArr['access_token'];
                if($this->submitAudit($accessToken,$publicNumArr['id'])){
                    $status = 1;
                    $message = '提交成功，微信审核约7个工作日，请耐心等待！';
                }
            }

            return json_encode([
                'status'  => $status,
                'message' => $message
            ]);
        }
    }
}

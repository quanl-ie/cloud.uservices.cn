<?php
namespace webapp\controllers;
use common\helpers\ExcelHelper;
use common\models\WorkAppraisalDict;
use webapp\models\ReportingAccountOrder;
use webapp\models\ReportingCompanyOrder;
use webapp\models\ReportingTechnicianOrder;
use webapp\models\Technician;
use webapp\models\TechnicianAppraisal;
use Yii;
/**
 * @desc:报表管理
 */
class ReportingController extends BaseController
{
    //平台派单
    public function actionOrderCount()
    {
        return $this->render('order-count');
    }
    //客户派单
    public function actionAccountOrderCount()
    {
        return $this->render('account-order-count');
    }
    //服务收费统计
    public function actionFeeCount()
    {
        return $this->render('fee-count');
    }
    //技师接单
    public function actionTechnicianOrderCount()
    {
        return $this->render('technician-order-count');
    }
    //异常订单
    public function actionAbnormalOrderCount()
    {
        return $this->render('abnormal-order-count');
    }
    //备件消耗
    public function actionMateReduceCount()
    {
        return $this->render('mate-reduce-count');
    }
    //售后品牌统计
    public function actionSaleBrandCount()
    {
        return $this->render('sale-brand-count');
    }

    /**
     * 平台派单导出
     * @param $result
     * @param $xlsName
     */
    public function actionCompanyOrderExcel()
    {
        $date      = trim(Yii::$app->request->get('date',''));
        $companyId = intval(Yii::$app->request->get('company_id',0));
        $workType  = intval(Yii::$app->request->get('work_type',0));
        if($companyId) {
            $where[] = ['=','department_id',$companyId];
        }else{
            $where[] = ['=','direct_company_id',$this->directCompanyId];
        }
        if($workType) {
            $where[] = ['=','work_type',$workType];
        }
        if(isset($date) && $date != '') {
            $where[]=['>=','date',date("Y-m-01",strtotime($date))];
            $where[]=['<=','date',date("Y-m-t",strtotime($date))];
        }else{
            $where[]=['>=','date',date("Y-m-01",strtotime('-1 month'))];
            $where[]=['<=','date',date("Y-m-t",strtotime('-1 month'))];
        }
        $data = ReportingCompanyOrder::getList($where,1,100000);
        $exportData = $data['list'];
        $cells = [
            'date'             => '日期',
            'new_work_num'    => '新增子工单数量',
            'finish_work_num' => '完成单数/单',
            'cancel_work_num' => '取消单数/单'
        ];
        $newArr = [];
        foreach ($cells as $key=>$val)
        {
            $newArr[$key]['key']    = $key;
            $newArr[$key]['value']  = $val;
            $newArr[$key]['width']  = 30;
        }
        $cells = array_values($newArr);
        $xlsName = '平台派单量统计';
        ExcelHelper::CreateExcel($cells,$exportData,$xlsName,$xlsName);
    }
    /**
     * 技师接单统计导出
     * @param $result
     * @param $xlsName
     */
    public function actionTecOrderExcel()
    {
        $date      = trim(Yii::$app->request->get('date',''));
        $companyId = intval(Yii::$app->request->get('company_id',0));
        $workType  = intval(Yii::$app->request->get('work_type',0));
        $name      = trim(Yii::$app->request->get('name',''));
        if($companyId) {
            $where[] = ['=','department_id',$companyId];
        }else{
            $where[] = ['=','direct_company_id',$this->directCompanyId];
        }
        if($workType) {
            $where[] = ['=','work_type',$workType];
        }
        if(isset($name) && $name != '') {
            $where[] = ['like','technician_name',$name];
        }
        if(isset($date) && $date != '') {
            $where[]=['>=','date',date("Y-m-01",strtotime($date))];
            $where[]=['<=','date',date("Y-m-t",strtotime($date))];
        }else{
            $where[]=['>=','date',date("Y-m-01",strtotime('-1 month'))];
            $where[]=['<=','date',date("Y-m-t",strtotime('-1 month'))];
        }
        $data = ReportingTechnicianOrder::getList($where,1,100000);
        $exportData = $data['list'];
        $cells = [
            'technician_name' => '技师',
            'department_name' => '机构',
            'work_num'    => '接单数',
            'finish_num' => '完成数',
            'goods_appraise_num' => '好评率',
            'bad_appraise_num'    => '好评率',
            'on_time' => '按时上门率',
        ];
        $newArr = [];
        foreach ($cells as $key=>$val)
        {
            $newArr[$key]['key']    = $key;
            $newArr[$key]['value']  = $val;
            $newArr[$key]['width']  = 30;
        }
        $cells = array_values($newArr);
        $xlsName = '技师接单统计';
        ExcelHelper::CreateExcel($cells,$exportData,$xlsName,$xlsName);
    }
    /**
     * 客户派单统计导出
     * @param $result
     * @param $xlsName
     */
    public function actionAccountOrderExcel()
    {
        $date      = trim(Yii::$app->request->get('date',''));
        $workType  = intval(Yii::$app->request->get('work_type',0));
        $name  = trim(Yii::$app->request->get('name',''));
        $mobile  = trim(Yii::$app->request->get('mobile',''));
        $where[] = ['=','direct_company_id',$this->directCompanyId];
        if($workType) {
            $where[] = ['=','work_type',$workType];
        }
        if(isset($name) && $name != '') {
            $where[] = ['like','account_name',$name];
        }
        if(isset($mobile) && $mobile != '') {
            $where[] = ['like','account_mobile',$mobile];
        }
        if(isset($date) && $date != '') {
            $where[]=['>=','order_create_time',strtotime(date("Y-m-01",strtotime($date)))];
            $where[]=['<=','order_create_time',strtotime(date("Y-m-t",strtotime($date)))];
        }else{
            $where[]=['>=','order_create_time',strtotime(date("Y-m-01",strtotime('-1 month')))];
            $where[]=['<=','order_create_time',strtotime(date("Y-m-t",strtotime('-1 month')))];
        }
        $data = ReportingAccountOrder::getList($where,1,10000);
        $exportData = $data['list'];
        $cells = [
            'account_name' => '客户',
            'account_area' => '省 市',
            'order_num'=> '实际售后数'
        ];
        $newArr = [];
        foreach ($cells as $key=>$val)
        {
            $newArr[$key]['key']    = $key;
            $newArr[$key]['value']  = $val;
            $newArr[$key]['width']  = 30;
        }
        $cells = array_values($newArr);
        $xlsName = '客户派单量统计';
        ExcelHelper::CreateExcel($cells,$exportData,$xlsName,$xlsName);
    }
}
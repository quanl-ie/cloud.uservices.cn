<?php
    /**
     * Created by PhpStorm.
     * User: xingq
     * Date: 2018/3/23
     * Time: 10:09
     */
    
    namespace webapp\controllers;
    
    use function JPush\Model\message;
    use Yii;
    use webapp\logic\StockInfoLogic;
    use common\helpers\Paging;
    
    class StockInController extends BaseController
    {
        /**
         * 入库管理列表
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/23
         * Time: 16:18
         * @return string
         */
        public function actionIndex()
        {
            $status = Yii::$app->request->get('status',1);
            
            $res = StockInfoLogic::index($type = 1,$status);
            
            $pageHtml = '';
            if($res['totalPage'] >1){
                $pageHtml = Paging::make($res['page'], $res['totalPage'], '?page=');
            }
            
            $this->_view['data']          = $res['list'];
            $this->_view['pageHtml']      = $pageHtml;
            $this->_view['stockTypeList'] = isset($res['stockTypeList']) ? $res['stockTypeList'] : '';
            
            if ($status == 1) {
                return $this->render('wait-check');
            }elseif($status == 2) {
                return $this->render('finished');
            }elseif ($status == 3) {
                return $this->render('unchecked');
            }
        }
        
        /**
         * 待审批入库列表
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/26
         * Time: 15:09
         * @return string
         */
        public function actionWaitCheck()
        {
            
            $status = Yii::$app->request->get('status',1);
            
            $res = StockInfoLogic::index($type = 1,$status);
            
            $pageHtml = '';
            if($res['totalPage'] >1){
                $pageHtml = Paging::make($res['page'], $res['totalPage'], '?page=');
            }
            
            $this->_view['data']          = $res['list'];
            $this->_view['pageHtml']      = $pageHtml;
            $this->_view['stockTypeList'] = isset($res['stockTypeList']) ? $res['stockTypeList'] : '';
            
            return $this->render('wait-check');
            
        }
        
        /**
         * 已入库列表
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/26
         * Time: 15:11
         * @return string
         */
        public function actionFinished()
        {
            
            $status = Yii::$app->request->get('status',2);
            
            $res = StockInfoLogic::index($type = 1,$status);
            
            $pageHtml = '';
            if($res['totalPage'] >1){
                $pageHtml = Paging::make($res['page'], $res['totalPage'], '?page=');
            }
            
            $this->_view['data']          = $res['list'];
            $this->_view['pageHtml']      = $pageHtml;
            $this->_view['stockTypeList'] = isset($res['stockTypeList']) ? $res['stockTypeList'] : '';
            
            return $this->render('finished');
            
        }
        
        /**
         * 未通过列表
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/26
         * Time: 15:12
         * @return string
         */
        public function actionUnchecked()
        {
            
            $status = Yii::$app->request->get('status',3);
            
            $res = StockInfoLogic::index($type = 1,$status);
            
            $pageHtml = '';
            if($res['totalPage'] >1){
                $pageHtml = Paging::make($res['page'], $res['totalPage'], '?page=');
            }
            
            $this->_view['data']          = $res['list'];
            $this->_view['pageHtml']      = $pageHtml;
            $this->_view['stockTypeList'] = isset($res['stockTypeList']) ? $res['stockTypeList'] : '';
            
            return $this->render('unchecked');
        }
        
        /**
         * 添加
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/26
         * Time: 15:28
         * @return string
         * @throws \Exception
         */
        public function actionAdd()
        {
            $data = StockInfoLogic::beforeList(1);

            if ($data['code'] == 200) {
                $this->_view['data']      = $data['data']['model'];
                $this->_view['product']   = isset($data['data']['product']) ? $data['data']['product'] : '';
                $this->_view['stockType'] = $data['data']['stock_type'];
                $this->_view['depot']     = $data['data']['depot'];
            }else{
                echo "<script language=javascript>alert('剩余产品不足');history.back();</script>";exit;
            }
            if (Yii::$app->request->isPost) {
                //入库
                $res = StockInfoLogic::add(1);
                Yii::$app->session->setFlash('message',$res['message']);
                if ($res['code'] == 200) {
                    return $this->redirect('/stock-in/wait-check');
                }
            }
            
            return $this->render('add');
            
        }
        
        /**
         * 查看入库单详情
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/26
         * Time: 10:13
         */
        public function actionView()
        {
            
            $res = StockInfoLogic::view();
            
            if ($res['code'] == 200) {
                $this->_view['stockInfo']   = $res['data']['stockInfo'];
                $this->_view['stockDetail'] = $res['data']['stockDetail'];
            }else{
                Yii::$app->session->setFlash('message',$res['message']);
            }
            
            return $this->render('view');
            
        }
        
        /**
         * 审批页面
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/26
         * Time: 16:21
         * @return string
         */
        public function actionExamine()
        {
            
            $res = StockInfoLogic::view(1);
            
            if (Yii::$app->request->isPost) {
                $res = StockInfoLogic::ajaxExamine(1);
                
                return json_encode($res);
            }
            
            if ($res['code'] == 200) {
                $this->_view['depot']       = $res['data']['depot'];
                $this->_view['stockInfo']   = $res['data']['stockInfo'];
                $this->_view['stockDetail'] = $res['data']['stockDetail'];
            }else{
                Yii::$app->session->setFlash('message',$res['message']);
            }
            
            return $this->render('examine');
        }
        
        /**
         * 修改库房与序列号
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/24
         * Time: 17:22
         * @return string
         */
        public function actionEdit()
        {
            $res = StockInfoLogic::edit(1);
            return json_encode($res);
        }
        
    }
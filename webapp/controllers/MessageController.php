<?php
namespace webapp\controllers;

use webapp\logic\BaseLogic;
use Yii;
use common\helpers\Paging;
use common\models\ServiceClass;
use webapp\models\Role;
use webapp\models\AdminUser;
use webapp\models\UserForm;
use webapp\models\UserInfo;
use webapp\controllers\BaseController;
use webapp\logic\RoleLogic;
use webapp\logic\MessageLogic;
use yii\data\Pagination;
use webapp\models\BaseModel;
/**
* 函数用途描述:消息中心
* @date: 2017年12月14日 下午2:24:17
* @author: sxz<shixiaozheng@c-ntek.com>
* @param: variable
* @return:
*/
class MessageController extends BaseController
{
    /**
    * 函数用途描述:消息列表
    * @date: 2017年12月14日 下午2:26:23
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionIndex()
    {
        $param    = Yii::$app->request->get();
        $user_id  = Yii::$app->user->identity->id;  //当然登陆人员id
        $src_id   = BaseLogic::getManufactorId();  //当前商家机构所属id
        $msg_type = isset($param['msg_type'])?$param['msg_type']:'2';
        $src_type = BaseModel::SRC_FWS; //来源：14 厂商 13 服务商 12 门店 11 技师 20 c端用户
        $page = isset($param['page'])?$param['page']:'';
        $page_size = '20';
        $read_status = '';
        $result = MessageLogic::getlist($user_id,$src_id,$msg_type,$src_type,$page,$page_size,$read_status);
        $pageHtml = '';
        if($result['total_count'] >1){
            $pageHtml = Paging::make($result['page'], $result['total_page'], '?msg_type='.$msg_type.'&page=');
        }
        return $this->render('index', [
                'dataProvider'   => $result['list'],
                'pageHtml'       =>$pageHtml,
                'msg_type'       =>$msg_type
        ]);
    }
    /**
    * 函数用途描述:获取弹窗消息
    * @date: 2017年12月20日 下午4:37:06
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionGetNewMsg(){
        $param     = Yii::$app->request->post();
        $user_id   = yii::$app->user->id;
        $src_id    = MessageLogic::getManufactorId();
        $msg_type  = isset($param['msg_type'])?$param['msg_type']:'2';
        $src_type  = BaseModel::SRC_FWS; //来源：14 厂商 13 服务商 12 门店 11 技师 20 c端用户
        $page      = isset($param['page'])?$param['page']:'';
        $page_size = '';
        $data      = [];
        $result    = MessageLogic::getNewMsg($user_id,$src_id,$msg_type,$src_type,$page,$page_size);
        if($result['total_count'] >0){
            $data = $result['list'][0];
        }
        $data = json_encode($data,true);
        return $data;
        
    }
    /**
    * 函数用途描述:消息设置为已读
    * @date: 2017年12月20日 下午6:04:46
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionSetMsgReadStatus(){
        $msg_id = Yii::$app->request->post('msg_id','');
        if($msg_id){
            $result = MessageLogic::setMsgReadStatus($msg_id);
            return $result;
        }
        
    }
    /**
    * 函数用途描述:消息设置为忽略
    * @date: 2017年12月21日 上午10:27:01
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionSetMsgNoticeStatus() {
        $msg_id = Yii::$app->request->post('msg_id','');
        if($msg_id){
            $result = MessageLogic::setMsgNoticeStatus($msg_id);
            return $result;
        }
    }
}


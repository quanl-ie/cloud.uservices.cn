<?php
namespace webapp\controllers;

use webapp\logic\BaseLogic;
use webapp\models\BaseModel;
use Yii;
use webapp\controllers\BaseController;

class SysController extends BaseController
{
    /**
     * 系统设置列表
     * @author
     * @date
     * @return
     */
    public function actionIndex()
    {
        $data = [];
        $data['departmentId']    = BaseLogic::getDepartmentId();
        $data['directCompanyId'] = BaseLogic::getDirectCompanyId();
        $data['topId']           = BaseLogic::getTopId();
        return $this->render('index',['data' => $data]);
    }
}

<?php
namespace webapp\controllers;
use Yii;

class SendOutController extends BaseController
{
    /**
     * 发货列表
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    /**
     * 发货单添加
     * @return string
     */
    public function actionAdd()
    {
        return $this->render('add');
    }
    /**
     * 发货单详情
     * @return string
     */
    public function actionView()
    {
        return $this->render('view');
    }
}
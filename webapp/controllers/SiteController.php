<?php
namespace webapp\controllers;

use webapp\models\BaseModel;
use webapp\models\Department;
use webapp\models\LoginForm;
use Yii;
use yii\web\Controller;
use common\models\ServiceClass;
use webapp\models\RegisterForm;
use webapp\models\UserInfo;
use webapp\models\AdminUser;
use common\models\User;
use common\helpers\Helper;

/**
 * Site controller
 * @author liwenyong<liwenyong@c-ntek.com>
 * @date   2017-08-17
 */
class SiteController extends Controller
{
    public $source = false;
    //调用公共类
    public $layout = 'main-login';
    //屏蔽使用某个方法表单令牌
    public function beforeAction($action) {
        $source = $this->checkmobile();
        if($source){
            $this->source = true;
        }
        $currentaction = $action->id;
        $novalidactions = ['get-add','reset-pwd'];
        if(in_array($currentaction,$novalidactions)) {
            //$action->controller->enableCsrfValidation = false;
        }
        //parent::beforeAction($action);
        return true;
    }
    public function actions()
    {
        return  [
            'captcha'=>[
                'class' => 'yii\captcha\CaptchaAction',
                'backColor'=>0xFFFFFF,  //背景颜色
                'minLength'=>4,  //最短为4位
                'maxLength'=>4,   //是长为4位
                'transparent'=>true,  //显示为透明
                'foreColor' => 0x2693ff,
            ],
        ];
    }

    /**
     * Displays homepage.
     * @return string
     */
    public function actionIndex()
    {
        if(!Yii::$app->user->isGuest)
        {
            return $this->redirect(Yii::$app->params['fws.uservices.cn'].'/index/main');
        }
        return $this->redirect('/site/logout');
    }

    //运营后台用户添加
    public function actionGetAdd()
    {
        $postData = yii::$app->request->post();
        $mobileHaved = RegisterForm::getCount(['mobile'=> json_decode($postData['phone'])]);
        if($mobileHaved){
            $data = [
                'code' => 300,
                'msg' =>'该手机号已注册',
            ];
            return json_encode($data);
        }
        $user = new AdminUser();
        $user->username = $postData['phone'];
        $user->nick_name = $postData['phone'];
        $user->mobile = $postData['phone'];
        $user->password_hash = Yii::$app->security->generatePasswordHash($postData['password_hash']);
        $user->email = $postData['email'];
        $user->status=1;
        $user->role_ids = 1;
        $user->identity = 0;
        $user->created_at = $user->updated_at = time();

        $userInfo = new UserInfo();
        //$userInfo->user_type = $postData['user_type'];
        $userInfo->mobile = $postData['mobile'];
        $userInfo->company = $postData['company'];
        $userInfo->contact = $postData['contact'];
        $userInfo->email = $postData['email'];
        $userInfo->logo = $postData['logo'];
        $userInfo->address = $postData['address'];
        $userInfo->business_licence = $postData['business_licence'];
        $userInfo->company_desc = $postData['company_desc'];
        $userInfo->auditor = $postData['auditor'];
        $userInfo->class_id = $postData['class_id'];
        $userInfo->status = 2;
        $userInfo->created_at = $userInfo->updated_at = time();
        //开启事务
        $transaction = Yii::$app->db->beginTransaction();
        $code = 200;
        $msg = '成功';
        try {
                if(!$user->save(false)){
                    $code = 300;
                    $msg = '用户登陆数据错误';
                }else{
                    $userInfo->user_id = $user->id;
                    if (!$userInfo->save(false)) {
                        $code = 300;
                        $msg = '企业数据错误';
                    }
                }
            $transaction->commit();
        }catch (Exception $e) {
            $transaction->rollBack();
        }
        return json_encode(array('code'=>$code,'msg'=>$msg));
    }
    //运营后改重置密码
    public function actionResetPwd()
    {
        $id = yii::$app->request->post('id');
        $password_hash = yii::$app->request->post('password_hash');
        $user = User::find()->where(['id' => $id])->one();
        $user->password_hash = Yii::$app->security->generatePasswordHash($password_hash);
        $user->updated_at = time();
        $code = 200;
        $msg = '成功';
        if(!$user->save(false)){
            $code = 300;
            $msg = '数据错误';
        }
        return json_encode(array('code'=>$code,'msg'=>$msg));
    }
    /*
     * @desc 登陆系统
     * @author sxz
     * @date 2018-06-05
     * @return mixed
     * **/
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest && !$this->source) {
            return $this->goHome();
        }
        $postData = Yii::$app->request->post();
        if($postData)
        {
            if($this->source){
                $newData = array();
                $newData['LoginForm']['department_id'] = isset($postData['department_id'])?$postData['department_id']:'';
                $newData['LoginForm']['username'] = isset($postData['username'])?$postData['username']:'';
                $newData['LoginForm']['password'] = isset($postData['password'])?$postData['password']:'';
                unset($postData);
                $postData = $newData;
            }
            $postData['LoginForm']['department_id'] = isset($postData['LoginForm']['department_id'])?$postData['LoginForm']['department_id']:'';
            $mobile = $postData['LoginForm']['username'];
            if($mobile)
            {
                $info = AdminUser::getAllList(['mobile'=>$mobile,'del_status'=>1]);
                if(count($info) == 1 ){
                    $postData['LoginForm']['department_id'] = $info[0]['department_id'];
                }
            }
        }
        $model = new LoginForm();
        if(!$this->source) {
            $model->setScenario('login');
        }
        if ($model->load($postData) && $model->login())
        {
            if($this->source){
                $res = array(
                    'success' => true,
                    'code'=> 200,
                    'message' => '登录成功',
                    'data'=>array(
                        'company'=>$model->user->company,
                        'username'=>$model->user->username,
                        'mobile'=>$model->user->mobile,
                        'avatar'=>$model->user->avatar
                    )
                );
                return json_encode($res);
            }
            return $this->redirect('/site/index');
        }
        else
        {
            if($this->source){
                $res = array(
                    'success' => false,
                    'code'=> 10005,
                    'message' => $model->getErrors(),
                    'data'=>[]
                );
                return json_encode($res);
            }
            $model->password = '';
            $model->verifyCode = '';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * 退出
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date 2017-08-17
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        Yii::$app->session->destroy();
        Yii::$app->session->removeAll();
        if($this->source){
            $res = array(
                'success' => true,
                'code'=> 200,
                'message' => 'success',
            );
            return json_encode($res);
        }
        return  $this->redirect('/site/login');
    }

    /*
     * @desc 注册
     * @author chengjuanjuan <chengjuanjuan@c-ntek.com>
     * @date 2017-09-03
     * @return mixed
     * **/
    public function actionRegister()
    {
        $model = new RegisterForm();
        if ($model->load(Yii::$app->request->post()))
        {
            $mobileHaved = RegisterForm::getCount(['mobile'=>$model->mobile]);
            if($mobileHaved){
                $model->addError('mobile','该手机号已注册');
            }

            $sendSource = Yii::$app->params['sendMsg']['sendSource'];
            $sendType   = Yii::$app->params['sendMsg']['sendType']['register'];

            $key = $sendSource.$sendType.$model->mobile;;
            $verifiedCode =  $cache = Yii::$app->cache->get($key);

            if($model->verified_code!=$verifiedCode){
                $model->addError('verified_code','验证码错误');
            }

            $errorMsg = $model->getErrors();
            if(!$errorMsg)
            {
                $model->username = $model->mobile;
                $model->nick_name = $model->mobile;
                $model->department_id    = 0;
                $model->status   = 1;
                $model->role_ids = 9;
                $model->identity = 1;
                $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
                if($model->save(false))
                {
                    Yii::$app->session->setFlash('loginUserId',$model->id);
                    return $this->redirect('register-succ');
                }
            }
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }
    
    /*
     * @desc 注册成功展示页
     * @date 2018-06-05
     * @author sxz
     * @return mixed
     * **/
    public function actionRegisterSucc()
    {
        if(!Yii::$app->user->isGuest){
            return $this->redirect('/index/index');
        }

        if(Yii::$app->session->hasFlash('loginUserId'))
        {
            $userId = Yii::$app->session->getFlash('loginUserId');
            $user   = AdminUser::findOne(['id'=>$userId]);
            if($user){
                Yii::$app->user->login($user,3600*24*365);
            }
        }
        else {
            return $this->redirect('/site/login');
        }

        return $this->render('register-succ');
    }
    
    /*
     * @desc 注册第二步，填写资料
     * @date 2017-09-05
     * @author chengjuanjuan<chengjuanjuan@c-ntek.com>
     * @return mixed
     * **/
    public function actionSetProfile(){
        $userId  = Yii::$app->request->get('id');
        $model   = UserInfo::findOne(['user_id'=>$userId]);
        $data['goodsClass'] = ServiceClass::getClass();
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return  $this->redirect('/site/check-profile');
        }else{
            return $this->render('/site/set-profile',['model'=>$model,'data'=>$data]);
        }
    }

    /*
     * @desc 注册第三部 审核中
     * @date 2017-09-05
     * @author chengjuanjuan <chengjuanjuan@c-ntek.com>
     * @return mixed
     * **/
    public function actionCheckProfile(){
        $data = ['title'=>'资料提交成功，请等待审核'];
        return $this->render('check-profile',['data'=>$data]);
    }


    /*
     * @desc 获取短信验证码
     * @date 2017-09-03
     * @return Code  -2短信请求失败；-1手机号已注册;1短信发送成功;
     * */
    public function actionGetVerifiedCode()
    {
        $mobile     = Yii::$app->request->post('mobile');
        $type       = Yii::$app->request->post('type');
        $sendUrl = Yii::$app->params['sms.uservices.cn']."/notice/sms-single";
        $sendSource = Yii::$app->params['sendMsg']['sendSource'];
        $sendType   = Yii::$app->params['sendMsg']['sendType'][$type];
         //注册页面调用
        if($type=='register'){
            $mobileHaved = RegisterForm::getCount(['mobile'=>$mobile]);
            //账号已注册
            if($mobileHaved){
                $code = -1;
            }else{
                 $key = $sendSource.$sendType.$mobile;

               // if (!$verifiedCode = Yii::$app->cache->get($key)){
                    $verifiedCodeNum = rand(100000, 999999);
                    $post_data = array('mobile' => $mobile, 'message'=>json_encode(['code' => $verifiedCodeNum]), 'source' => $sendSource, 'type' => $sendType);
                    $verifiedCode = Helper::curlPost($sendUrl,$post_data);
                    Yii::$app->cache->set($key,$verifiedCodeNum,60*10);
                    $code = 1;
               // }
            }
        }else{
            $sendType = '2';
            $code = 1234;
        }

/*        if($mobileHaved){
            $code ='-1';
        }else{
            //如果cache里存在，直接调用cache的，不存在，调用短信接口
            $type = 1;
            $action = 'register';
          /*  if( Yii::$app->cache->get($action.$mobile)){
                $code = Yii::$app->cache->set('13436841028','233123',60*10);($action.$mobile);
            }else{*/
               /* $post_data = array('mobile' => $mobile, 'message'=>json_encode(['code' => rand(100000, 999999)]), 'source' => 'serviceChannels', 'type' => $sendType);
                $code = Helper::curlPost(Yii::$app->params['sendMsg'],$post_data);
                echo '<pre>';
                print_R($code);exit;*/
            //}

       // }*/
        echo $code;exit;
        return  $code;
    }
    /*
     * @desc 查询此号码注册的所有系统
     * @date 2018-06-05
     * @return array
     * */
    public function actionCheckDep()
    {
        $mobile = Yii::$app->request->post('mobile');
        $data = AdminUser::checkDepartment($mobile,$this->source);
        if($this->source){
            return json_encode(array('success'=>true,'code'=>200,'data'=>$data));
        }
        return $data;
    }
    /**
     * 判断请求是否来自移动端
     */
    function checkmobile() {
        $mobile = array();
        static $mobilebrowser_list =array('iphone', 'android', 'phone', 'mobile', 'wap', 'netfront', 'java', 'opera mobi', 'opera mini',
            'ucweb', 'windows ce', 'symbian', 'series', 'webos', 'sony', 'blackberry', 'dopod', 'nokia', 'samsung',
            'palmsource', 'xda', 'pieplus', 'meizu', 'midp', 'cldc', 'motorola', 'foma', 'docomo', 'up.browser',
            'up.link', 'blazer', 'helio', 'hosin', 'huawei', 'novarra', 'coolpad', 'webos', 'techfaith', 'palmsource',
            'alcatel', 'amoi', 'ktouch', 'nexian', 'ericsson', 'philips', 'sagem', 'wellcom', 'bunjalloo', 'maui', 'smartphone',
            'iemobile', 'spice', 'bird', 'zte-', 'longcos', 'pantech', 'gionee', 'portalmmm', 'jig browser', 'hiptop',
            'benq', 'haier', '^lct', '320x320', '240x320', '176x220');
        $useragent = strtolower($_SERVER['HTTP_USER_AGENT']);
        if( $this->dstrpos($useragent, $mobilebrowser_list, true)) {
            return true;
        }
        return false;
    }

    function dstrpos($string, &$arr, $returnvalue = false) {
        if(empty($string)) return false;
        foreach((array)$arr as $v) {
            if(strpos($string, $v) !== false) {
                $return = $returnvalue ? $v : true;
                return $return;
            }
        }
        return false;
    }
}

<?php

namespace webapp\controllers;

use Yii;
use webapp\logic\BrandLogic;
use common\helpers\Paging;

class BrandController extends BaseController
{
    /**
     * 列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 13:30
     * @return string
     */
    public function actionIndex()
    {
        $res = BrandLogic::getIndex();
        $this->_view['flag']     = $res['flag'];
        $this->_view['data']     = $res['list'];
        return $this->render('index');
        
    }
    
    /**
     * 编辑
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 17:56
     * @return mixed|string|void
     */
    public function actionAdd()
    {
        
        $data = BrandLogic::beforeAdd();

        if (Yii::$app->request->isPost)
        {
            
            $res = BrandLogic::add();
            return json_encode($res);
        }
        
        return $this->renderPartial('add',
            [
                'brand_ids'    => $data['brand_ids'],
                'brand_list'   => $data['brand_list'],
                'brand'        => $data['manufactor_brand']
            ]);
    }
    
    /**
     * 修改品牌状态（禁用、启用）
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 13:52
     * @return mixed|string|void
     */
    public function actionChangeStatus()
    {
        
        $res = BrandLogic::changeStatus();
        
        return json_encode($res);
        
    }

    /**
     * @return string
     * 品牌启用
     */
    public function actionChangeStatusOn()
    {

        $res = BrandLogic::changeStatus();

        return json_encode($res);

    }

}


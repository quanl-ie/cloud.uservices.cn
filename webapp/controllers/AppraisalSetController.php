<?php
namespace webapp\controllers;

use Yii;
use webapp\models\BaseModel;
use webapp\controllers\BaseController;
use webapp\logic\AppraisalSetLogic;
use common\models\AppraisalSetConfig;
class AppraisalSetController extends BaseController
{
    /**
     * 订单评价设置
     * @author
     * @date
     * @return
     */
    public function actionIndex()
    {
        $derectCompanyId = $this->directCompanyId;
        $info = AppraisalSetConfig::findOne(['direct_company_id'=>$derectCompanyId,'type'=>3,'status'=>1]);
        if (Yii::$app->request->isPost) {
            $res = AppraisalSetLogic::edit();
            Yii::$app->session->setFlash('appraisal_set_message',$res['message']);
            return $this->redirect('/appraisal-set/index');
        }
        if ($info) {
            $this->_view['data']    = $info;
        }
        return $this->render('index');
    }
}

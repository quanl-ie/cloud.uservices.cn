<?php

namespace webapp\controllers;

use webapp\models\CooporationServiceProvider;
use Yii;
use webapp\logic\TechnicianCheckListLogic;
use webapp\controllers\BaseController;
use common\helpers\Paging;


/**
 * @desc:技师结算单管理
 * @author Eva <chengjuanjuan@c-ntek.com>
 * @date 2018-2-2
 *
 */

class TechnicianCheckListController extends BaseController
{

    public function actionWaitCheck()
    {

        $res = TechnicianCheckListLogic::getList();
        if($res['totalPage'] >=1){
            $pageHtml = Paging::make($res['page'],  $res['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }

        //$search = CostOrderLogic::getData();

        $this->_view['data'] = isset($res['data'])?$res['data']:$res['list'];
        $this->_view['pageHtml'] = $pageHtml;
        //$this->_view['search'] = $search;
        return $this->render('wait-check');
    }


    /**
     * 列表
     * @author Eva <chengjuanjuan@c-ntek.com>
     */
    public function actionHaveCosted()
    {
        $res = TechnicianCheckListLogic::getList();
        if($res['totalPage'] >=1){
            $pageHtml = Paging::make($res['page'],  $res['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }

        $this->_view['data'] = isset($res['data'])?$res['data']:$res['list'];
        $this->_view['pageHtml'] = $pageHtml;
        return $this->render('have-costed');

    }

    public function actionWaitCost()
    {
        $res = TechnicianCheckListLogic::getList();
        if($res['totalPage'] >=1){
            $pageHtml = Paging::make($res['page'],  $res['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }

        $this->_view['data'] = isset($res['data'])?$res['data']:$res['list'];
        $this->_view['pageHtml'] = $pageHtml;
        return $this->render('wait-cost');
    }



    //申请对账单
    public function actionApply(){
        if (Yii::$app->request->isPost){
            if(Yii::$app->request->Post('mark')){
                return TechnicianCheckListLogic::getData();
            }

            $res = TechnicianCheckListLogic::updateStatus();
            if($res['code'] == 200) {
                return $this->redirect('/technician-check-list/wait-check');
            }
            else {
                Yii::$app->session->setFlash('message',$res['message']);
            }
        }

        return $this->render('apply');
    }


    //对账单详情
    public function actionDetail(){
        $data = TechnicianCheckListLogic::getDetail();
        return $this->render('detail',
            [
                'info'         => $data['info'],
                'costItemList' => $data['costItemList'],
                'workList'     => $data['workList']
            ]);
    }


    public  function actionCheck(){
        if (Yii::$app->request->isPost) {
            $res = TechnicianCheckListLogic::check();
            if($res['code'] == 200) {
                return $this->redirect('/technician-check-list/wait-cost');
            }
            else {
                Yii::$app->session->setFlash('message',$res['message']);
            }
        }
        $data = TechnicianCheckListLogic::getDetail();
        return $this->render('check',
            [
                'info'         => $data['info'],
                'costItemList' => $data['costItemList'],
                'workList'     => $data['workList']
            ]);

    }
        public  function actionCost(){
            if (Yii::$app->request->isPost) {
                $res = TechnicianCheckListLogic::check(3);
                if($res['code'] == 200) {
                    return $this->redirect('/technician-check-list/have-costed');
                }
                else {
                    Yii::$app->session->setFlash('message',$res['message']);
                }

            }

            $data = TechnicianCheckListLogic::getDetail();
            return $this->render('cost-option',
                [
                    'info'         => $data['info'],
                    'costItemList' => $data['costItemList'],
                    'workList'     => $data['workList']
                ]);
    }


}
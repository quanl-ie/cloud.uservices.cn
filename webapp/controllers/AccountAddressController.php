<?php

namespace webapp\controllers;

use common\helpers\Helper;
use yii;
use webapp\logic\AccountAddressLogic;
use webapp\controllers\BaseController;
use webapp\models\AccountAddress;
use webapp\models\Region;

/**
 *@desc:客户管理
 * @author: chengjuanjuan <chengjuanjuan@c-ntek.com>
 * createdAt: 2017/12/14 14:09
 *
 */

class AccountAddressController extends BaseController
{
    /**
     * 添加
     * @return string|yii\web\Response
     */
    public function actionAdd()
    {
        $model = new AccountAddress();
        $id = intval(yii::$app->request->get('id',0));

        if (yii::$app->request->isPost)
        {
            //调用逻辑层
            $res = AccountAddressLogic::add();
            if($res['success'] == true)
            {
                return json_encode([
                    'info'   => '添加成功',
                    'status' => 'y',
                    'id' => $res['data']['id']
                ]);
            }
            else
            {
                return json_encode([
                    'info'   => $res['message'],
                    'status' => 'n'
                ]);
            }
        }
        $province = Region::getOpenProvince();
        return $this->renderPartial('add',[
            'id'      => $id,
            'model'   => $model,
            'province' => $province,
        ]);
    }

    /**
     * 智邦拉取后提交订单修正地址坐标
     * @return string
     * @author xi
     */
    public function actionChangeCoordinate()
    {
        $this->layout = false;
        $id = intval(Yii::$app->request->get('id',0));
        $accountAddress = AccountAddress::getAddressById($id);


        if(Yii::$app->request->isPost)
        {
            $id  = intval(Yii::$app->request->post('address_id',0));
            $lng = Yii::$app->request->post('lng',0);
            $lat = Yii::$app->request->post('lat',0);

            $model = AccountAddress::findOne(['id'=>$id]);
            if($model)
            {
                $model->lon = $lng;
                $model->lat = $lat;
                if($model->save(false)){
                    return json_encode([
                       'info'   => '提交成功',
                       'status' => 'y'
                    ]);
                }
            }
            return json_encode([
                'info'   => '提交失败',
                'status' => 'n'
            ]);
        }

        $this->_view['id'] = $id;
        $this->_view['accountAddress'] = $accountAddress;
        return $this->render('change-coordinate');
    }

    /**
     * 显示地址经纬度
     * @return string
     * @author xi
     */
    public function actionShowCoordinate()
    {
        $this->layout = false;
        $id = intval(Yii::$app->request->get('id',0));
        $accountAddress = AccountAddress::getAddressById($id);

        $this->_view['accountAddress'] = $accountAddress;
        return $this->render('show-coordinate');
    }

    /**
     * 查地址坐标是否有
     * @author xi
     */
    public function actionGetCoordinate()
    {
        $addressId = intval(Yii::$app->request->get('address_id',0));
        if($addressId <= 0){
            return json_encode([
                'status' => 0,
                'message' => '参数 address_id 不能为空'
            ]);
        }

        $addressArr = AccountAddress::findOneByAttributes(['id'=>$addressId],'lon,lat');
        if($addressArr && $addressArr['lon'] > 0 && $addressArr['lat'] > 0){
            return json_encode([
                'status' => 1,
                'message' => '坐标不为侬'
            ]);
        }

        return json_encode([
            'status' => 0,
            'message' => '坐标为空'
        ]);
    }
}


<?php

namespace webapp\controllers;

use common\helpers\ExcelHelper;
use common\lib\Push;
use common\models\Contract;
use common\models\Dynaactionform;
use common\models\Region;
use common\helpers\Helper;
use common\models\Work;
use common\models\WorkRelTechnician;
use PHPUnit\Util\Log\JSON;
use webapp\logic\AccountLogic;
use webapp\logic\ContractLogic;
use webapp\logic\DepartmentLogic;
use webapp\logic\FlowLogic;
use webapp\logic\ServiceTypeLogic;
use webapp\logic\WorkLogic;
use webapp\models\BaseModel;
use webapp\models\Department;
use webapp\models\DismantlingConfig;
use webapp\models\Grouping;
use webapp\models\GroupingRelTechnician;
use webapp\models\ManufactorType;
use webapp\models\Order;
use webapp\models\OrderForm;
use webapp\models\SaleOrder;
use webapp\models\SaleOrderView;
use webapp\models\TechnicianMode;
use webapp\models\TechnicianSkill;
use webapp\models\WorkForm;
use webapp\models\WorkOrder;
use webapp\models\ZbDepartmentConfig;
use Yii;
use webapp\logic\OrderLogic;
use common\helpers\Paging;
use webapp\logic\BaseLogic;
use webapp\models\AccountAddress;
use webapp\models\Account;
use webapp\models\CooporationServiceProvider;
use webapp\models\ServiceType;
use webapp\models\BrandQualification;
use webapp\models\ServiceTypeQualification;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use webapp\logic\ProductLogic;
use common\models\ServiceClass;


class OrderController extends BaseController
{

    /**
     * 全部订单
     * @author xi
     * @date 2017-12-18
     */
    public function actionIndex()
    {
        $brforeList = ProductLogic::getBeforeList();
        $provinceId = Yii::$app->request->get('province_id', 0);
        $cityId = Yii::$app->request->get('city_id', 0);
        $appraisal_status = Yii::$app->request->get('appraisal_status', 0);


        $getStr = '?';
        if ($_GET) {
            $getStr = '?' . http_build_query($_GET) . '&';
        }
        if (isset($_GET['class_id'])) {
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids'] = implode(',', $classId);
            } else {
                $_GET['class_ids'] = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',', $classId);
            }
            $_GET['class_name'] = implode(',', ServiceClass::getName($classId));
        }
        if (isset($_GET['department_id'])) {
            $departmentId = $_GET['department_id'];
            //机构搜索条件
            if (is_array($departmentId)) {
                $_GET['department_ids'] = implode(',', $departmentId);
            } else {
                $_GET['department_ids'] = $departmentId;
            }
            if (!is_array($departmentId)) {
                $departmentId = explode(',', $departmentId);
            }
            $_GET['department_names'] = implode(',', Department::getDepartmentNames($departmentId));
        }
        $result = OrderLogic::getList(1);
        //导出 excel
        if (Yii::$app->request->get('export') == 1) {
            if ($appraisal_status == 1) {
                self::exportExcel($result, '待评价订单');
            } else {
                self::exportExcel($result, '全部订单');
            }
        }
        if (isset($result['data']['list']) && $result['data']['list']) {
            foreach ($result['data']['list'] as $key => $val) {
                $sever_top_id = \common\models\Department::getDirectCompanyId($val['service_department_id']);
                $result['data']['list'][$key]['sever_direct_id'] = $sever_top_id;
            }
        }

        $pageHtml = '';
        if ($result['success'] == true && $result['data']['totalPage'] > 1) {
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr . 'page=');
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        //获取搜索结果的市区信息
        $cityList = $districtList = [];
        if ($provinceId > 0) {
            $cityList = Region::getList(['parent_id' => $provinceId]);
        }
        if ($cityId > 0) {
            $districtList = Region::getList(['parent_id' => $cityId]);
        }
        //获取省份
        $province = Region::getList(['parent_id' => 1]);
        $info = self::departmentforSearch();
        $this->_view['department_id'] = BaseLogic::getDirectCompanyId();
        $this->_view['department'] = $info;
        $this->_view['auth'] = $this->dataAuthId;
        $this->_view['province'] = $province;
        $this->_view['cityList'] = $cityList;
        $this->_view['districtList'] = $districtList;
        $this->_view['statusArr'] = OrderLogic::getOrderStatus();
        $this->_view['data'] = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = $workTypeArr;
        $this->_view['exportParams'] = self::getExportParams($_GET);
        $this->_view['departmentId'] = $this->departmentId;
        $this->_view['directCompanyId'] = $this->directCompanyId;
        $this->_view['departmentObj'] = $this->departmentObj;
        $this->_view['class'] = $brforeList['class'];  //产品类目数据

		
        if($this->source){
            foreach($result['data']['list'] as $key=>$val){
                if($val['plan_time'] == 0 || $val['plan_time'] == null){
                    $result['data']['list'][$key]['plan_time'] = '';
                }
            }
            $user = array(
                'company' => $this->user->company,
                'username' => $this->user->username,
                'mobile' => $this->user->mobile,
                'avatar' => $this->user->avatar
            );
            $result['data']['userInfo'] = $user;
            $res = array(
                'success' => true,
                'code' => 200,
                'message' => '',
                'data' => $result['data']
            );
            return json_encode($res);
        }
        return $this->render('index');
    }

    /**
     * 待指派
     * @author xi
     * @date 2018-6-19
     */
    public function actionWaitAssign()
    {
        $brforeList = ProductLogic::getBeforeList();
        $provinceId = Yii::$app->request->get('province_id', 0);
        $cityId = Yii::$app->request->get('city_id', 0);

        $getStr = '?';
        if ($_GET) {
            $getStr = '?' . http_build_query($_GET) . '&';
        }
        if (isset($_GET['class_id'])) {
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids'] = implode(',', $classId);
            } else {
                $_GET['class_ids'] = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',', $classId);
            }
            $_GET['class_name'] = implode(',', ServiceClass::getName($classId));
        }
        if (isset($_GET['department_id'])) {
            $departmentId = $_GET['department_id'];
            //机构搜索条件
            if (is_array($departmentId)) {
                $_GET['department_ids'] = implode(',', $departmentId);
            } else {
                $_GET['department_ids'] = $departmentId;
            }
            if (!is_array($departmentId)) {
                $departmentId = explode(',', $departmentId);
            }
            $_GET['department_names'] = implode(',', Department::getDepartmentNames($departmentId));
        }
        $_GET['status'] = '1_8';
        $result = OrderLogic::getList();
        //导出 excel
        if (Yii::$app->request->get('export') == 1) {
            self::exportExcel($result, '待指派订单');
        }
        foreach ($result['data']['list'] as $key => $val) {
            $sever_top_id = \common\models\Department::getDirectCompanyId($val['service_department_id']);
            $result['data']['list'][$key]['sever_direct_id'] = $sever_top_id;
        }

        $pageHtml = '';
        if ($result['success'] == true && $result['data']['totalPage'] > 1) {
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr . "page=");
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        //获取搜索结果的市区信息
        $cityList = $districtList = [];
        if ($provinceId > 0) {
            $cityList = Region::getList(['parent_id' => $provinceId]);
        }
        if ($cityId > 0) {
            $districtList = Region::getList(['parent_id' => $cityId]);
        }
        //获取省份
        $province = Region::getList(['parent_id' => 1]);
        $info = self::departmentforSearch();

        $this->_view['department_id'] = BaseLogic::getDirectCompanyId();
        $this->_view['department'] = $info;
        $this->_view['auth'] = $this->dataAuthId;
        $this->_view['province'] = $province;
        $this->_view['cityList'] = $cityList;
        $this->_view['districtList'] = $districtList;
        $this->_view['data'] = isset($result['data']) ? $result['data'] : '';
        $this->_view['pageHtml'] = $pageHtml ? $pageHtml : '';
        $this->_view['workTypeArr'] = isset($workTypeArr) ? $workTypeArr : '';
        $this->_view['exportParams'] = self::getExportParams($_GET);
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        $this->_view['departmentId'] = $this->departmentId;
        $this->_view['directCompanyId'] = $this->directCompanyId;
        return $this->render('wait-assign');
    }

    /**
     * 待服务
     * @author xi
     * @date 2017-12-19
     */
    public function actionWaitService()
    {
        $brforeList = ProductLogic::getBeforeList();
        $provinceId = Yii::$app->request->get('province_id', 0);
        $cityId = Yii::$app->request->get('city_id', 0);

        $getStr = '?';
        if ($_GET) {
            $getStr = '?' . http_build_query($_GET) . '&';
        }
        if (isset($_GET['class_id'])) {
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids'] = implode(',', $classId);
            } else {
                $_GET['class_ids'] = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',', $classId);
            }
            $_GET['class_name'] = implode(',', ServiceClass::getName($classId));
        }
        if (isset($_GET['department_id'])) {
            $departmentId = $_GET['department_id'];
            //机构搜索条件
            if (is_array($departmentId)) {
                $_GET['department_ids'] = implode(',', $departmentId);
            } else {
                $_GET['department_ids'] = $departmentId;
            }
            if (!is_array($departmentId)) {
                $departmentId = explode(',', $departmentId);
            }
            $_GET['department_names'] = implode(',', Department::getDepartmentNames($departmentId));
        }
        $_GET['status'] = '2_9';
        $work_status = [1, 2];
        $result = OrderLogic::getList(1, $work_status);
        //导出 excel
        if (Yii::$app->request->get('export') == 1) {
            self::exportExcel($result, '待服务订单');
        }
        foreach ($result['data']['list'] as $key => $val) {
            $sever_top_id = \common\models\Department::getDirectCompanyId($val['service_department_id']);
            $result['data']['list'][$key]['sever_direct_id'] = $sever_top_id;
        }

        $pageHtml = '';
        if ($result['success'] == true && $result['data']['totalPage'] > 1) {
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr . "page=");
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        //获取搜索结果的市区信息
        $cityList = $districtList = [];
        if ($provinceId > 0) {
            $cityList = Region::getList(['parent_id' => $provinceId]);
        }
        if ($cityId > 0) {
            $districtList = Region::getList(['parent_id' => $cityId]);
        }
        //获取省份
        $province = Region::getList(['parent_id' => 1]);
        $info = self::departmentforSearch();
        $this->_view['department_id'] = BaseLogic::getDirectCompanyId();
        $this->_view['department'] = $info;
        $this->_view['auth'] = $this->dataAuthId;
        $this->_view['province'] = $province;
        $this->_view['cityList'] = $cityList;
        $this->_view['districtList'] = $districtList;
        $this->_view['data'] = isset($result['data']) ? $result['data'] : '';
        $this->_view['pageHtml'] = $pageHtml ? $pageHtml : '';
        $this->_view['workTypeArr'] = isset($workTypeArr) ? $workTypeArr : '';
        $this->_view['exportParams'] = self::getExportParams($_GET);
        $this->_view['departmentId'] = $this->departmentId;
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        $this->_view['departmentId'] = $this->departmentId;
        $this->_view['directCompanyId'] = $this->directCompanyId;
        $this->_view['departmentObj'] = $this->departmentObj;
        return $this->render('wait-service');
    }

    /**
     * 服务中
     * @author xi
     * @date 2017-12-19
     */
    public function actionInService()
    {
        $brforeList = ProductLogic::getBeforeList();
        $provinceId = Yii::$app->request->get('province_id', 0);
        $cityId = Yii::$app->request->get('city_id', 0);

        $getStr = '?';
        if ($_GET) {
            $getStr = '?' . http_build_query($_GET) . '&';
        }
        if (isset($_GET['class_id'])) {
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids'] = implode(',', $classId);
            } else {
                $_GET['class_ids'] = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',', $classId);
            }
            $_GET['class_name'] = implode(',', ServiceClass::getName($classId));
        }
        if (isset($_GET['department_id'])) {
            $departmentId = $_GET['department_id'];
            //机构搜索条件
            if (is_array($departmentId)) {
                $_GET['department_ids'] = implode(',', $departmentId);
            } else {
                $_GET['department_ids'] = $departmentId;
            }
            if (!is_array($departmentId)) {
                $departmentId = explode(',', $departmentId);
            }
            $_GET['department_names'] = implode(',', Department::getDepartmentNames($departmentId));
        }
        $_GET['status'] = 3;
        $work_status = [3, 5];
        $result = OrderLogic::getList(1, $work_status);
        //导出 excel
        if (Yii::$app->request->get('export') == 1) {
            self::exportExcel($result, '服务中订单');
        }
        foreach ($result['data']['list'] as $key => $val) {
            $sever_top_id = \common\models\Department::getDirectCompanyId($val['service_department_id']);
            $result['data']['list'][$key]['sever_direct_id'] = $sever_top_id;
        }


        $pageHtml = '';
        if ($result['success'] == true && $result['data']['totalPage'] > 1) {
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr . "page=");
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        //获取搜索结果的市区信息
        $cityList = $districtList = [];
        if ($provinceId > 0) {
            $cityList = Region::getList(['parent_id' => $provinceId]);
        }
        if ($cityId > 0) {
            $districtList = Region::getList(['parent_id' => $cityId]);
        }
        //获取省份
        $province = Region::getList(['parent_id' => 1]);
        $info = self::departmentforSearch();

        $this->_view['department_id'] = BaseLogic::getDirectCompanyId();
        $this->_view['department'] = $info;
        $this->_view['auth'] = $this->dataAuthId;
        $this->_view['province'] = $province;
        $this->_view['cityList'] = $cityList;
        $this->_view['districtList'] = $districtList;
        $this->_view['data'] = isset($result['data']) ? $result['data'] : '';
        $this->_view['pageHtml'] = $pageHtml ? $pageHtml : '';
        $this->_view['workTypeArr'] = isset($workTypeArr) ? $workTypeArr : '';
        $this->_view['exportParams'] = self::getExportParams($_GET);
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        $this->_view['departmentId'] = $this->departmentId;
        $this->_view['directCompanyId'] = $this->directCompanyId;
        return $this->render('in-service');
    }

    /**
     * 待收款
     * @author xi
     * @date 2017-12-19
     */
    public function actionWaitMoney()
    {
        $brforeList = ProductLogic::getBeforeList();
        $provinceId = Yii::$app->request->get('province_id', 0);
        $cityId = Yii::$app->request->get('city_id', 0);

        $getStr = '?';
        if ($_GET) {
            $getStr = '?' . http_build_query($_GET) . '&';
        }
        if (isset($_GET['class_id'])) {
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids'] = implode(',', $classId);
            } else {
                $_GET['class_ids'] = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',', $classId);
            }
            $_GET['class_name'] = implode(',', ServiceClass::getName($classId));
        }
        if (isset($_GET['department_id'])) {
            $departmentId = $_GET['department_id'];
            //机构搜索条件
            if (is_array($departmentId)) {
                $_GET['department_ids'] = implode(',', $departmentId);
            } else {
                $_GET['department_ids'] = $departmentId;
            }
            if (!is_array($departmentId)) {
                $departmentId = explode(',', $departmentId);
            }
            $_GET['department_names'] = implode(',', Department::getDepartmentNames($departmentId));
        }
        $_GET['status'] = 12;
        $result = OrderLogic::getList();

        //导出 excel
        if (Yii::$app->request->get('export') == 1) {
            self::exportExcel($result, '待收款订单');
        }

        $pageHtml = '';
        if ($result['success'] == true && $result['data']['totalPage'] > 1) {
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr . "page=");
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        //获取搜索结果的市区信息
        $cityList = $districtList = [];
        if ($provinceId > 0) {
            $cityList = Region::getList(['parent_id' => $provinceId]);
        }
        if ($cityId > 0) {
            $districtList = Region::getList(['parent_id' => $cityId]);
        }
        //获取省份
        $province = Region::getList(['parent_id' => 1]);
        $info = self::departmentforSearch();

        $this->_view['department_id'] = BaseLogic::getDirectCompanyId();
        $this->_view['department'] = $info;
        $this->_view['auth'] = $this->dataAuthId;
        $this->_view['province'] = $province;
        $this->_view['cityList'] = $cityList;
        $this->_view['districtList'] = $districtList;
        $this->_view['data'] = isset($result['data']) ? $result['data'] : '';
        $this->_view['pageHtml'] = $pageHtml ? $pageHtml : '';
        $this->_view['workTypeArr'] = isset($workTypeArr) ? $workTypeArr : '';
        $this->_view['exportParams'] = self::getExportParams($_GET);
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        $this->_view['departmentId'] = $this->departmentId;
        $this->_view['directCompanyId'] = $this->directCompanyId;
        return $this->render('wait-money');
    }

    /**
     * 待验收 wait-confirm
     * @author xi
     * @date 2017-12-18
     */
    public function actionWaitConfirm()
    {
        $brforeList = ProductLogic::getBeforeList();
        $provinceId = Yii::$app->request->get('province_id', 0);
        $cityId = Yii::$app->request->get('city_id', 0);

        $getStr = '?';
        if ($_GET) {
            $getStr = '?' . http_build_query($_GET) . '&';
        }
        if (isset($_GET['class_id'])) {
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids'] = implode(',', $classId);
            } else {
                $_GET['class_ids'] = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',', $classId);
            }
            $_GET['class_name'] = implode(',', ServiceClass::getName($classId));
        }
        if (isset($_GET['department_id'])) {
            $departmentId = $_GET['department_id'];
            //机构搜索条件
            if (is_array($departmentId)) {
                $_GET['department_ids'] = implode(',', $departmentId);
            } else {
                $_GET['department_ids'] = $departmentId;
            }
            if (!is_array($departmentId)) {
                $departmentId = explode(',', $departmentId);
            }
            $_GET['department_names'] = implode(',', Department::getDepartmentNames($departmentId));
        }
        $_GET['status'] = '4_7_13';

        $result = OrderLogic::getList();
        //导出 excel
        if (Yii::$app->request->get('export') == 1) {
            self::exportExcel($result, '待验收订单');
        }
        foreach ($result['data']['list'] as $key => $val) {
            $sever_top_id = \common\models\Department::getDirectCompanyId($val['service_department_id']);
            $result['data']['list'][$key]['sever_direct_id'] = $sever_top_id;
        }


        $pageHtml = '';
        if ($result['success'] == true && $result['data']['totalPage'] > 1) {
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr . "page=");
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        //获取搜索结果的市区信息
        $cityList = $districtList = [];
        if ($provinceId > 0) {
            $cityList = Region::getList(['parent_id' => $provinceId]);
        }
        if ($cityId > 0) {
            $districtList = Region::getList(['parent_id' => $cityId]);
        }
        //获取省份
        $province = Region::getList(['parent_id' => 1]);
        $info = self::departmentforSearch();

        $this->_view['department_id'] = BaseLogic::getDirectCompanyId();
        $this->_view['department'] = $info;
        $this->_view['auth'] = $this->dataAuthId;
        $this->_view['province'] = $province;
        $this->_view['cityList'] = $cityList;
        $this->_view['districtList'] = $districtList;
        $this->_view['statusArr'] = OrderLogic::getOrderStatus();
        $this->_view['data'] = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = $workTypeArr;
        $this->_view['exportParams'] = self::getExportParams($_GET);
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        $this->_view['departmentId'] = $this->departmentId;
        $this->_view['directCompanyId'] = $this->directCompanyId;
        return $this->render('wait-confirm');
    }

    /**
     * 已完成
     * @author xi
     * @date 2017-12-19
     */
    public function actionFinish()
    {
        $brforeList = ProductLogic::getBeforeList();
        $provinceId = Yii::$app->request->get('province_id', 0);
        $cityId = Yii::$app->request->get('city_id', 0);

        $getStr = '?';
        if ($_GET) {
            $getStr = '?' . http_build_query($_GET) . '&';
        }
        if (isset($_GET['class_id'])) {
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids'] = implode(',', $classId);
            } else {
                $_GET['class_ids'] = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',', $classId);
            }
            $_GET['class_name'] = implode(',', ServiceClass::getName($classId));
        }
        if (isset($_GET['department_id'])) {
            $departmentId = $_GET['department_id'];
            //机构搜索条件
            if (is_array($departmentId)) {
                $_GET['department_ids'] = implode(',', $departmentId);
            } else {
                $_GET['department_ids'] = $departmentId;
            }
            if (!is_array($departmentId)) {
                $departmentId = explode(',', $departmentId);
            }
            $_GET['department_names'] = implode(',', Department::getDepartmentNames($departmentId));
        }
        //@todo 传个参数  status = 5
        $_GET['status'] = 5;
        $work_status = [4];
        $result = OrderLogic::getList(0, $work_status);

        //导出 excel
        if (Yii::$app->request->get('export') == 1) {
            self::exportExcel($result, '已完成订单');
        }

        $pageHtml = '';
        if ($result['success'] == true && $result['data']['totalPage'] > 1) {
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr . 'page=');
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        //获取搜索结果的市区信息
        $cityList = $districtList = [];
        if ($provinceId > 0) {
            $cityList = Region::getList(['parent_id' => $provinceId]);
        }
        if ($cityId > 0) {
            $districtList = Region::getList(['parent_id' => $cityId]);
        }
        //获取省份
        $province = Region::getList(['parent_id' => 1]);
        $info = self::departmentforSearch();

        $this->_view['department_id'] = BaseLogic::getDirectCompanyId();
        $this->_view['department'] = $info;
        $this->_view['auth'] = $this->dataAuthId;
        $this->_view['province'] = $province;
        $this->_view['cityList'] = $cityList;
        $this->_view['districtList'] = $districtList;
        $this->_view['data'] = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = $workTypeArr;
        $this->_view['exportParams'] = self::getExportParams($_GET);
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        $this->_view['departmentId'] = $this->departmentId;
        $this->_view['directCompanyId'] = $this->directCompanyId;
        return $this->render('finish');
    }

    /**
     * 已取消
     * @author xi
     * @date 2017-12-19
     */
    public function actionCanc()
    {
        $brforeList = ProductLogic::getBeforeList();
        $provinceId = Yii::$app->request->get('province_id', 0);
        $cityId = Yii::$app->request->get('city_id', 0);

        $getStr = '?';
        if ($_GET) {
            $getStr = '?' . http_build_query($_GET) . '&';
        }
        if (isset($_GET['class_id'])) {
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids'] = implode(',', $classId);
            } else {
                $_GET['class_ids'] = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',', $classId);
            }
            $_GET['class_name'] = implode(',', ServiceClass::getName($classId));
        }
        if (isset($_GET['department_id'])) {
            $departmentId = $_GET['department_id'];
            //机构搜索条件
            if (is_array($departmentId)) {
                $_GET['department_ids'] = implode(',', $departmentId);
            } else {
                $_GET['department_ids'] = $departmentId;
            }
            if (!is_array($departmentId)) {
                $departmentId = explode(',', $departmentId);
            }
            $_GET['department_names'] = implode(',', Department::getDepartmentNames($departmentId));
        }
        $_GET['status'] = 6;
        $result = OrderLogic::getList();

        //导出 excel
        if (Yii::$app->request->get('export') == 1) {
            self::exportExcel($result, '已取消订单');
        }

        $pageHtml = '';
        if ($result['success'] == true && $result['data']['totalPage'] > 1) {
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr . 'page=');
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        //获取搜索结果的市区信息
        $cityList = $districtList = [];
        if ($provinceId > 0) {
            $cityList = Region::getList(['parent_id' => $provinceId]);
        }
        if ($cityId > 0) {
            $districtList = Region::getList(['parent_id' => $cityId]);
        }
        //获取省份
        $province = Region::getList(['parent_id' => 1]);
        $info = self::departmentforSearch();

        $this->_view['department_id'] = BaseLogic::getDirectCompanyId();
        $this->_view['department'] = $info;
        $this->_view['auth'] = $this->dataAuthId;
        $this->_view['province'] = $province;
        $this->_view['cityList'] = $cityList;
        $this->_view['districtList'] = $districtList;
        $this->_view['data'] = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = $workTypeArr;
        $this->_view['exportParams'] = self::getExportParams($_GET);
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        $this->_view['departmentId'] = $this->departmentId;
        $this->_view['directCompanyId'] = $this->directCompanyId;
        return $this->render('canc');
    }

    /**
     * 上门超时
     * @author  xi
     * @date 2017-12-20
     */
    public function actionServiceTimeout()
    {
        $brforeList = ProductLogic::getBeforeList();
        $provinceId = Yii::$app->request->get('province_id', 0);
        $cityId = Yii::$app->request->get('city_id', 0);

        $getStr = '?';
        if ($_GET) {
            $getStr = '?' . http_build_query($_GET) . '&';
        }
        if (isset($_GET['class_id'])) {
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids'] = implode(',', $classId);
            } else {
                $_GET['class_ids'] = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',', $classId);
            }
            $_GET['class_name'] = implode(',', ServiceClass::getName($classId));
        }
        if (isset($_GET['department_id'])) {
            $departmentId = $_GET['department_id'];
            //机构搜索条件
            if (is_array($departmentId)) {
                $_GET['department_ids'] = implode(',', $departmentId);
            } else {
                $_GET['department_ids'] = $departmentId;
            }
            if (!is_array($departmentId)) {
                $departmentId = explode(',', $departmentId);
            }
            $_GET['department_names'] = implode(',', Department::getDepartmentNames($departmentId));
        }
        $_GET['status'] = 9;
        $result = OrderLogic::getList();
        //导出 excel
        if (Yii::$app->request->get('export') == 1) {
            self::exportExcel($result, '上门超时订单');
        }
        foreach ($result['data']['list'] as $key => $val) {
            $sever_top_id = \common\models\Department::getDirectCompanyId($val['service_department_id']);
            $result['data']['list'][$key]['sever_direct_id'] = $sever_top_id;
        }


        $pageHtml = '';
        if ($result['success'] == true && $result['data']['totalPage'] > 1) {
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr . 'page=');
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        //获取搜索结果的市区信息
        $cityList = $districtList = [];
        if ($provinceId > 0) {
            $cityList = Region::getList(['parent_id' => $provinceId]);
        }
        if ($cityId > 0) {
            $districtList = Region::getList(['parent_id' => $cityId]);
        }
        //获取省份
        $province = Region::getList(['parent_id' => 1]);
        $info = self::departmentforSearch();

        $this->_view['department_id'] = BaseLogic::getDirectCompanyId();
        $this->_view['department'] = $info;
        $this->_view['auth'] = $this->dataAuthId;
        $this->_view['province'] = $province;
        $this->_view['cityList'] = $cityList;
        $this->_view['districtList'] = $districtList;
        $this->_view['data'] = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = $workTypeArr;
        $this->_view['exportParams'] = self::getExportParams($_GET);
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        $this->_view['departmentId'] = $this->departmentId;
        $this->_view['directCompanyId'] = $this->directCompanyId;
        return $this->render('service-timeout');
    }

    /**
     * 被驳回
     * @author  xi
     * @date 2017-12-20
     */
    public function actionServiceProvidersReject()
    {
        $brforeList = ProductLogic::getBeforeList();
        $provinceId = Yii::$app->request->get('province_id', 0);
        $cityId = Yii::$app->request->get('city_id', 0);

        $getStr = '?';
        if ($_GET) {
            $getStr = '?' . http_build_query($_GET) . '&';
        }
        if (isset($_GET['class_id'])) {
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids'] = implode(',', $classId);
            } else {
                $_GET['class_ids'] = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',', $classId);
            }
            $_GET['class_name'] = implode(',', ServiceClass::getName($classId));
        }
        if (isset($_GET['department_id'])) {
            $departmentId = $_GET['department_id'];
            //机构搜索条件
            if (is_array($departmentId)) {
                $_GET['department_ids'] = implode(',', $departmentId);
            } else {
                $_GET['department_ids'] = $departmentId;
            }
            if (!is_array($departmentId)) {
                $departmentId = explode(',', $departmentId);
            }
            $_GET['department_names'] = implode(',', Department::getDepartmentNames($departmentId));
        }
        $_GET['status'] = '8';
        $result = OrderLogic::getList();
        //导出 excel
        if (Yii::$app->request->get('export') == 1) {
            self::exportExcel($result, '被驳回接订单');
        }
        foreach ($result['data']['list'] as $key => $val) {
            $sever_top_id = \common\models\Department::getDirectCompanyId($val['service_department_id']);
            $result['data']['list'][$key]['sever_direct_id'] = $sever_top_id;
        }


        $pageHtml = '';
        if ($result['success'] == true && $result['data']['totalPage'] > 1) {
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr . 'page=');
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        //获取搜索结果的市区信息
        $cityList = $districtList = [];
        if ($provinceId > 0) {
            $cityList = Region::getList(['parent_id' => $provinceId]);
        }
        if ($cityId > 0) {
            $districtList = Region::getList(['parent_id' => $cityId]);
        }
        //获取省份
        $province = Region::getList(['parent_id' => 1]);
        $info = self::departmentforSearch();

        $this->_view['department_id'] = BaseLogic::getDirectCompanyId();
        $this->_view['department'] = $info;
        $this->_view['auth'] = $this->dataAuthId;
        $this->_view['province'] = $province;
        $this->_view['cityList'] = $cityList;
        $this->_view['districtList'] = $districtList;
        $this->_view['data'] = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = $workTypeArr;
        $this->_view['exportParams'] = self::getExportParams($_GET);
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        $this->_view['departmentId'] = $this->departmentId;
        $this->_view['directCompanyId'] = $this->directCompanyId;
        return $this->render('service-providers-reject');
    }

    /**
     * 待审核
     * @author xi
     * @date 2017-12-19
     */
    public function actionWaitAudit()
    {
        $brforeList = ProductLogic::getBeforeList();
        $provinceId = Yii::$app->request->get('province_id', 0);
        $cityId = Yii::$app->request->get('city_id', 0);

        $getStr = '?';
        if ($_GET) {
            $getStr = '?' . http_build_query($_GET) . '&';
        }
        if (isset($_GET['class_id'])) {
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids'] = implode(',', $classId);
            } else {
                $_GET['class_ids'] = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',', $classId);
            }
            $_GET['class_name'] = implode(',', ServiceClass::getName($classId));
        }
        if (isset($_GET['department_id'])) {
            $departmentId = $_GET['department_id'];
            //机构搜索条件
            if (is_array($departmentId)) {
                $_GET['department_ids'] = implode(',', $departmentId);
            } else {
                $_GET['department_ids'] = $departmentId;
            }
            if (!is_array($departmentId)) {
                $departmentId = explode(',', $departmentId);
            }
            $_GET['department_names'] = implode(',', Department::getDepartmentNames($departmentId));
        }
        $_GET['status'] = '10';
        $result = OrderLogic::getList();

        //导出 excel
        if (Yii::$app->request->get('export') == 1) {
            self::exportExcel($result, '待审核订单');
        }

        $pageHtml = '';
        if ($result['success'] == true && $result['data']['totalPage'] > 1) {
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr . 'page=');
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        //获取搜索结果的市区信息
        $cityList = $districtList = [];
        if ($provinceId > 0) {
            $cityList = Region::getList(['parent_id' => $provinceId]);
        }
        if ($cityId > 0) {
            $districtList = Region::getList(['parent_id' => $cityId]);
        }
        //获取省份
        $province = Region::getList(['parent_id' => 1]);
        $info = self::departmentforSearch();

        $this->_view['department_id'] = BaseLogic::getDirectCompanyId();
        $this->_view['department'] = $info;
        $this->_view['auth'] = $this->dataAuthId;
        $this->_view['province'] = $province;
        $this->_view['cityList'] = $cityList;
        $this->_view['districtList'] = $districtList;
        $this->_view['data'] = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = $workTypeArr;
        $this->_view['exportParams'] = self::getExportParams($_GET);
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        $this->_view['departmentId'] = $this->departmentId;
        $this->_view['directCompanyId'] = $this->directCompanyId;
        return $this->render('wait-audit');
    }

    /**
     * 未通过
     * @return string
     */
    public function actionWaitAuditFaild()
    {
        $brforeList = ProductLogic::getBeforeList();
        $provinceId = Yii::$app->request->get('province_id', 0);
        $cityId = Yii::$app->request->get('city_id', 0);

        $getStr = '?';
        if ($_GET) {
            $getStr = '?' . http_build_query($_GET) . '&';
        }
        if (isset($_GET['class_id'])) {
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids'] = implode(',', $classId);
            } else {
                $_GET['class_ids'] = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',', $classId);
            }
            $_GET['class_name'] = implode(',', ServiceClass::getName($classId));
        }
        if (isset($_GET['department_id'])) {
            $departmentId = $_GET['department_id'];
            //机构搜索条件
            if (is_array($departmentId)) {
                $_GET['department_ids'] = implode(',', $departmentId);
            } else {
                $_GET['department_ids'] = $departmentId;
            }
            if (!is_array($departmentId)) {
                $departmentId = explode(',', $departmentId);
            }
            $_GET['department_names'] = implode(',', Department::getDepartmentNames($departmentId));
        }
        $_GET['status'] = '11';
        $result = OrderLogic::getList();

        //导出 excel
        if (Yii::$app->request->get('export') == 1) {
            self::exportExcel($result, '未通过订单');
        }

        $pageHtml = '';
        if ($result['success'] == true && $result['data']['totalPage'] > 1) {
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr . 'page=');
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        //获取搜索结果的市区信息
        $cityList = $districtList = [];
        if ($provinceId > 0) {
            $cityList = Region::getList(['parent_id' => $provinceId]);
        }
        if ($cityId > 0) {
            $districtList = Region::getList(['parent_id' => $cityId]);
        }

        //获取省份
        $province = Region::getList(['parent_id' => 1]);
        $info = self::departmentforSearch();

        $this->_view['department_id'] = BaseLogic::getDirectCompanyId();
        $this->_view['department'] = $info;
        $this->_view['auth'] = $this->dataAuthId;
        $this->_view['province'] = $province;
        $this->_view['cityList'] = $cityList;
        $this->_view['districtList'] = $districtList;
        $this->_view['data'] = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = $workTypeArr;
        $this->_view['exportParams'] = self::getExportParams($_GET);
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        $this->_view['departmentId'] = $this->departmentId;
        $this->_view['directCompanyId'] = $this->directCompanyId;
        return $this->render('wait-audit-faild');
    }

    /**
     * 订单详情
     * @author xi
     * @date 2017-12-18
     */
    public function actionView()
    {
        $result = OrderLogic::view();

        //动态表单
        $dynaactionform = Dynaactionform::getFormData($this->directCompanyId);

        //质保状态
        $amount_type = BaseModel::getAmountType();
        if ($this->source) {
            if (!$result['success']) {
                return json_encode($result);
            }
            $res = [
                'success' => true,
                'code' => 200,
                'message' => '',
                'data' => array(
                    'orderDetail' => $result['data'],
                    'directCompanyId' => $this->directCompanyId,
                    'departmentId' => $this->departmentId,
                    'amount_type' => $amount_type,
                    'dynaactionform' => $dynaactionform
                )
            ];
            return json_encode($res);
        }

        return $this->render('view', [
            'result'          => $result,
            'directCompanyId' => $this->directCompanyId,
            'departmentId'    => $this->departmentId,
            'amount_type'     => $amount_type,
            'dynaactionform'  => $dynaactionform
        ]);
    }

    /**
     * 查看服务产品列表
     * @author sxz
     * @date 2018-7-24
     */
    public function actionViewAllProduct()
    {
        $result = OrderLogic::viewAllProduct();
        return $this->renderPartial('view-all-product', ['data' => $result['data']]);
    }

    /**
     * 创建订单
     * @author xi
     * @date 2018-06-15
     */
    public function actionAdd()
    {
        //判断是否是重新下单
        $orderNo = Yii::$app->request->get('order_no', '');
        $accountId = intval(Yii::$app->request->get('account_id', 0));

        //合同下单参数
        $type = intval(Yii::$app->request->get('type', 0));
        $contract_id = intval(Yii::$app->request->get('contract_id', 0));
        $data = $address = $service = $provider = [];
        if ($orderNo) {
            //根据ID查询当前订单信息
            $data = OrderLogic::getOrderInfo($orderNo);
            //客户对应地址
            $address = AccountAddress::getAddress($data['account_id']);
            //客户对应服务
            $service = SaleOrder::getServiceBrand($data['account_id']);
            //客户对应服务商
            $provider = [];
        }

        //下单处理
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $assign = Yii::$app->request->post('assign');
            //调用逻辑层
            $result = OrderLogic::add();
            if ($result['code'] == 200) {
                if ($assign == 2) {
                    $url = '/order/wait-assign';
                    if ($result['data']['order_no'] != '' && $result['data']['work_no'] != '') {
                        $url = '/order/assign?order_no=' . $result['data']['order_no'] . '&work_no=' . $result['data']['work_no'] . '&local_url=/order/index';
                    }
                    return BaseLogic::success(['url' => $url]);
                }
                return BaseLogic::success(['url' => '/order/index']);
            } else {
                return BaseLogic::error($result['message']);
            }
        }

        //服务类型
        $serviceType = ManufactorType::getServiceType();
        //质保状态
        $amount_type = BaseModel::getAmountType();
        $info['data'] = [];
        //合同下单
        if ($type == 1) {
            //获取合同信息
            if ($contract_id) {
                $info = ContractLogic::getDetail($contract_id, '', 9);

                if ($info['success'] && $info['data']) {
                    //合同下的客户
                    $accountId = $info['data']['info']['account_id'];
                    //所属机构
                    $info['data']['info']['department_name'] = Department::getDepartmentName($info['data']['info']['department_id']);
                    //查询售后产品是否存在
                    $s_res = SaleOrder::findAllByAttributes(['contract_id' => $info['data']['info']['id']]);
                    //已存在的产品
                    $exsit_ids = array_column($s_res, 'prod_id');
                    //不存在的
                    $add_data = [];
                    foreach ($info['data']['detail'] as $key => $val) {
                        if (!in_array($val['prod_id'], $exsit_ids)) {
                            $add_data[] = $val;
                        }
                    }
                    //不存在的添加到sale_order
                    if (!empty($add_data)) {
                        //添加售后产品
                        SaleOrder::batchSale($add_data, 1);
                    }
                    //获取合同产品对应的售后产品id
                    foreach ($info['data']['detail'] as $k => $v) {
                        $sale_res = SaleOrder::findOneByAttributes(['contract_id' => $v['parent_id'], 'status' => 1, 'department_id' => $this->departmentId, 'contract_order' => 1, 'contract_detail_id' => $v['id']], 'id');
                        $info['data']['detail'][$k]['sale_order_id'] = $sale_res['id'];
                    }
                }
            } else {
                $this->redirect("/contract/wait-check");
            }
            foreach ($serviceType as $key => $val) {
                if ($val['id'] == Yii::$app->params['chaidan']['workType']) {
                    unset($serviceType[$key]);
                }
            }
            if (!empty($serviceType)) {
                $data['work_type'] = reset($serviceType)['id'];
            }
        }

        //获取服务流程
        $serviceWorkStage = [];
        if ($serviceType) {

            if ($type == 1) {
                $serviceTypeId = $data['work_type'];
            } else {
                $serviceTypeId = $serviceType[0]['id'];
            }
            $re = FlowLogic::getFlowStage($serviceTypeId, [1, 2]);
            if ($re['code'] == 200) {
                $serviceWorkStageNum = isset($re['data'][1]) ? $re['data'][1] : [];
                if ($serviceWorkStageNum) {
                    $serviceWorkStage = $re['data'];
                } else {
                    $serviceWorkStage = [];
                }
            }
        }
        //客户信息
        $accountArr = [];
        if ($accountId > 0) {
            $accountArr = Account::findOneByAttributes(['id' => $accountId], 'id,account_name,mobile');
        }

        $model = new OrderForm();
        if ($orderNo != '' && isset($data['workDetail'][0]['fault_img_arr'])) {
            $model->fault_img = $data['workDetail'][0]['fault_img_arr'];
        }

        //设置时间类型
        $setTimeType = Helper::setTimeType();
        //获取机构所需数据
        $DepartmentInfo = DepartmentLogic::getList(3);
        $DepartmentInfo = isset($DepartmentInfo['list'][0]) ? $DepartmentInfo['list'][0] : [];
        //获取拆单开关状态
        $dismantling_status = 2; //默认不启用
        $res = DismantlingConfig::getOne(['direct_company_id' => $this->directCompanyId]);
        if (!empty($res)) {
            $dismantling_status = $res['status'];
        }
        //获取智邦权限的机构

        $auth_zb = ZbDepartmentConfig::findAllByAttributes(['status' => 1]);
        $department_ids_arr = array_column($auth_zb, 'department_id');
        //默认为创建普通订单
        $tmp = 'add';
        if ($type == 0 && in_array($this->departmentId, $department_ids_arr)) {
            $tmp = 'account-zb';
        }
        return $this->render($tmp, [
            'type' => $type,
            'contract_info' => $info['data'],
            'serviceType' => $serviceType,
            'amount_type' => $amount_type,
            'data' => $data,
            'address' => $address,
            'service' => $service,
            'provider' => $provider,
            'accountArr' => $accountArr,
            'model' => $model,
            'setTimeType' => $setTimeType,
            'dismantling_status' => $dismantling_status,
            'serviceWorkStage' => $serviceWorkStage,
            'DepartmentInfo' => $DepartmentInfo
        ]);
    }

    /**
     * 取消订单
     * @author xi
     * @date 2017-12-18
     */
    public function actionCancel()
    {
        if (Yii::$app->request->isAjax) {
            $result = OrderLogic::cancel();
            return json_encode($result);
        }
    }

    //检查收费标准
    public function actionCheckFeeScale()
    {
        $res = OrderLogic::CheckScale();
        return $res;
    }

    /**
     * 服务商拒单
     * @author liuxingqi <lxq@c-ntek.com>
     */
    public function actionReject()
    {
        $res = OrderLogic::reject();
        return json_encode($res);
    }

    /**
     * 获取用户地址信息
     * @author liwenyong
     * @data    2017-12-20
     */
    public function actionGetAddress()
    {
        $account_id = Yii::$app->request->post('account_id', 0);
        $address = AccountAddress::getAddressInfo($account_id);
        $str = '';
        if ($address) {
            foreach ($address as $k => $v) {
                $str .= "<option value='" . $v['id'] . "'>" . $v['conact_name'] . "/" . $v['mobile'] . "/" . $v['address'] . "</option>";
            }
        } else {
            $str = "<option value=''>该客户暂无联系地址</option>";
        }
        return $str;
    }

    /**
     * 获取用户对应服务产品
     * @author liwenyong<liwenyong@uservices.cn>
     * @date   2017-12-20
     */
    public function actionGetServiceBrand()
    {
        $account_id = Yii::$app->request->post('account_id', 0);
        $type_id = Yii::$app->request->post('type_id', 0);
        if ($account_id == 0) {
            return "<option value=''>请先选择客户及服务类型</option>";
        }
        $address = SaleOrder::getServiceBrand($account_id, $type_id);
        $str = "<option value=''>该客户暂无产品</option>";
        if ($address) {
            $str = "<option value=''>请选择</option>";
            foreach ($address as $k => $v) {
                $str .= "<option data-name='" . $v['unit'] . "' data-intent='" . $v['intent'] . "' value='" . $v['id'] . "'>" . $v['value'] . "</option>";
            }
        }
        return $str;
    }

    /**
     * 获取用户对应服务产品
     * @author liwenyong<liwenyong@uservices.cn>
     * @date   2017-12-20
     * @return  json
     */
    public function actionGetSaleInfo()
    {
        $account_id = Yii::$app->request->post('account_id', 0);
        $intent = Yii::$app->request->post('intent', 0);
        $address = SaleOrder::getServiceBrand($account_id, 0, $intent);
        $data = array("msg" => false, 'data' => '');
        if (!empty($address)) {
            $data = array("msg" => true, 'data' => $address);
        }
        return json_encode($data);
    }

    /**
     * 服务地址定位
     * @author liuxingqi <lxq@c-ntek.com>
     */
    public function actionPosition()
    {
        $orderNo = trim(yii::$app->request->get('order_no', ''));
        $res = OrderLogic::getPosition($orderNo);
        return $this->renderPartial('position', ['data' => $res]);
    }

    /**
     * 验收通过
     * @author xi
     * @date 2018-2-5
     */
    public function actionAjaxConfirmFinish()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $orderNo = trim(Yii::$app->request->get('order_no', ''));
        $flag = intval(Yii::$app->request->get('flag', 1));
        if ($orderNo == '') {
            return BaseLogic::error('订单号不能为空', 20002);
        }

        return OrderLogic::confirmFinish($orderNo, $flag);
    }

    /**
     * 验收未通过
     * @return array
     */
    public function actionAjaxAcceptanceFailure()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $orderNo = trim(Yii::$app->request->get('order_no', ''));
        $reason = trim(Yii::$app->request->get('reason', ''));

        if ($orderNo == '') {
            return BaseLogic::error('订单号不能为空', 20002);
        }

        return OrderLogic::acceptanceFailure($orderNo, $reason);
    }

    /**
     * 修改服务时间
     * @author xi
     * @date 2018-2-22
     */
    public function actionChangeTime()
    {
        $orderNo = trim(Yii::$app->request->get('order_no', ''));
        if (Yii::$app->request->isAjax) {
            $orderNo = trim(Yii::$app->request->post('order_no', ''));

            //服务时间拼接
            $selectDate = Yii::$app->request->post("select_date", '');
            $planTimeType = Yii::$app->request->post("plan_time_type", '');
            $setTime = Yii::$app->request->post("set_time", '');


            if ($selectDate == '') {
                return json_encode(BaseLogic::error('请选择日期', 20002));
            }
            if ($planTimeType == '') {
                return json_encode(BaseLogic::error('请选择服务时间类型', 20003));
            }

            if ($planTimeType == 5 && $setTime == '') {
                return json_encode(BaseLogic::error('请选择具体的服务时间', 20004));
            }

            $planTime = Helper::changeTime($planTimeType, $selectDate, $setTime);


            if ($orderNo == '') {
                return json_encode(BaseLogic::error('订单号不能为空', 20001));
            }
            if ($planTime == '') {
                return json_encode(BaseLogic::error('预约服务时间不能为空', 20002));
            }

            return json_encode(OrderLogic::chagePlanTime($orderNo, $planTime, $planTimeType));
        }
        //获取时间
        $setTimeType = Helper::setTimeType();
        $setTime = Helper::setTime();

        return $this->renderPartial('change-time', [
            'orderNo' => $orderNo,
            'setTimeType' => $setTimeType,
            'setTime' => $setTime
        ]);
    }

    /**
     * 更改订单质保状态
     * @author sxz
     * @date 2018-2-26
     */
    public function actionEditScope()
    {
        if (yii::$app->request->isPost) {
            $res = OrderLogic::editScope();
            if (isset($res['success']) && $res['success'] == 1) {
                return $this->redirect('/work/view?work_no=' . $res['data']['work_no']);
            }
        }
        $model = new \webapp\models\OrderForm();
        $orderNo = trim(Yii::$app->request->get('order_no', ''));
        $workNo = trim(Yii::$app->request->get('work_no', ''));
        $orderScope = trim(Yii::$app->request->get('order_scope', ''));
        $srcType = trim(Yii::$app->request->get('src_type', ''));
        $srcId = trim(Yii::$app->request->get('src_id', ''));
        //获取质保状态图片
        $scopeImg = [];
        $workData = WorkLogic::getWorkDetails($workNo);
        if ($workData && $workData['data']['scope_img']) {
            $scopeImg = $workData['data']['scope_img'];
        }
        //质保状态
        $amount_type = BaseModel::getAmountType();
        $data['amount_type'] = $amount_type;
        $data['scope_img'] = $scopeImg;
        $data['order_no'] = $orderNo;
        $data['work_no'] = $workNo;
        $data['order_scope'] = $orderScope;
        $data['src_type'] = $srcType;
        $data['src_id'] = $srcId;
        $data['model'] = $model;
        return $this->render('edit-scope', ['data' => $data]);
    }

    /**
     * 检查技师是否与服务时间冲突
     * @return array
     * @author xi
     */
    public function actionAjaxHasPlantimeConflict()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $technicianId = intval(Yii::$app->request->get('technician_id'));

            $plantimeDate = trim(Yii::$app->request->get('plan_time_date', ''));
            $plantimeHour = trim(Yii::$app->request->get('plan_time_hour', ''));
            $plantimetype = intval(Yii::$app->request->get('plan_time_type', 5));
            $workNo = trim(Yii::$app->request->get('work_no', ''));

            if (!$technicianId) {
                return BaseLogic::error('技师id 不能为空', 20002);
            }
            if ($plantimetype == 0 || $plantimeDate == '') {
                return BaseLogic::error('请选择服务时间', 20003);
            }
            if (!$workNo) {
                return BaseLogic::error('工单号不能为空', 20004);
            }

            $planTime = Helper::changeTime($plantimetype, $plantimeDate, $plantimeHour);
            $postData = [
                'technician_id' => $technicianId,
                'plantime' => $planTime,
                'work_no' => $workNo
            ];
            $url = Yii::$app->params['order.uservices.cn'] . "/v1/work-pc/has-plantime-conflict";
            $jsonStr = Helper::curlPostJson($url, $postData);
            $result = json_decode($jsonStr, true);

            if (isset($result['success']) && $result['success'] == 1) {
                $data = $result['data'];
                return BaseLogic::success($data);
            } else {
                return BaseLogic::error($result['message'], $result['code']);
            }
        }
    }

    /**
     * 检查是否满足技能
     */
    public function actionAjaxCheckHaveSkills()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $workType = intval(Yii::$app->request->get('work_type', 0));
        $addressId = intval(Yii::$app->request->get('address_id', 0));
        $saleOrderId = intval(Yii::$app->request->get('sale_order_id', 0));
        $technicianId = intval(Yii::$app->request->get('technician_id', 0));

        $plantimeDate = trim(Yii::$app->request->get('plan_time_date', ''));
        $plantimeHour = trim(Yii::$app->request->get('plan_time_hour', ''));
        $plantimetype = intval(Yii::$app->request->get('plan_time_type', 5));

        if (!$workType) {
            return BaseLogic::error('参数 work_type 不能为空', 20003);
        }
        if (!$addressId) {
            return BaseLogic::error('参数 address_id 不能为空', 20004);
        }
        if (!$saleOrderId) {
            return BaseLogic::error('参数 sale_order_id 不能为空', 20005);
        }
        if (!$technicianId) {
            return BaseLogic::error('参数 technician_id 不能为空', 20006);
        }
        if ($plantimetype == 0 || $plantimeDate == '') {
            return BaseLogic::error('请选择服务时间', 20007);
        }

        $planTime = Helper::changeTime($plantimetype, $plantimeDate, $plantimeHour);

        $jsModeArr = TechnicianMode::findOneByAttributes(['technician_id' => $technicianId], 'lon,lat,service_radius,service_time,service_work');
        $addressArr = AccountAddress::findOneByAttributes(['id' => $addressId], 'lon,lat');
        if ($addressArr && $jsModeArr) {
            $jsDistance = Helper::getDistance($jsModeArr['lat'], $jsModeArr['lon'], $addressArr['lat'], $addressArr['lon']);
            if ($jsDistance > $jsModeArr['service_radius']) {
                return BaseLogic::error("该技师服不在服务范围内", 20008);
            }


            $week = date('w', $planTime);
            $hour = date('H', $planTime);
            $serviceTime = json_decode($jsModeArr['service_time'], true);

            if ($jsModeArr['service_work'] && !in_array($week, json_decode($jsModeArr['service_work'], true)) || count($serviceTime) != 2 || $hour < $serviceTime[0] || $hour > $serviceTime[1]) {
                return BaseLogic::error("该技师服务时间不在范围内", 20009);
            }
        }

        $saleOrderArr = SaleOrder::findOneByAttributes(['id' => $saleOrderId], 'class_id');
        if ($saleOrderArr) {
            $where = [
                'technician_id' => $technicianId,
                'class_id' => $saleOrderArr['class_id'],
                'service_id' => $workType
            ];
            $jsSkillArr = TechnicianSkill::findOneByAttributes($where, 'technician_id');
            if (!$jsSkillArr) {
                return BaseLogic::error("该技师不符合技能", 20010);
            }
        }

        return BaseLogic::success([]);
    }

    /**
     * 编辑服务回单信息
     */
    public function actionEditServiceReceipt()
    {
        $workNo = Yii::$app->request->get('no', '');
        $model = new WorkForm();
        if (Yii::$app->request->isPost) {
            $orderNo = trim(Yii::$app->request->post('order_no', ''));       //工单号码
            $workNo = trim(Yii::$app->request->post('work_no', ''));       //工单号码
            $serviceRecord = trim(Yii::$app->request->post('service_record', ''));  //服务记录
            $workImg = Yii::$app->request->post('work_img', '');            //服务工单
            $sceneImg = Yii::$app->request->post('scene_img', '');           //现场拍照
            //故障图片

            if ($workImg) {
                $workImg = implode(',', $workImg);
            }
            if ($sceneImg) {
                $sceneImg = implode(',', $sceneImg);
            }
            if ($workNo == '') {
                return json_encode(BaseLogic::error('工单号不能为空', 20002));
            }
            $res = Work::updateAll(['work_img' => $workImg, 'scene_img' => $sceneImg, 'service_record' => $serviceRecord], ['work_no' => $workNo]);
            if ($res) {
                return $this->redirect('/order/view?order_no=' . $orderNo);
            } else {
                Yii::$app->session->setFlash('message', "操作失败");
            }
        }
        $workService = Work::findOneByAttributes(['work_no' => $workNo], 'id,order_no,work_no,work_img,scene_img,service_record');
        $arr = [
            'work_img' => array_filter(explode(",", $workService['work_img'])),
            'scene_img' => array_filter(explode(",", $workService['scene_img'])),
            'order_no' => $workService['order_no'],
            'work_no' => $workService['work_no'],
            'service_record' => $workService['service_record'],
            'model' => $model
        ];
        $data['data'] = json_encode($arr);
        return $this->render('edit-service-receipt',$data);
    }

    /**
     * 编辑产品回单信息
     */
    public function actionEditProductReceipt()
    {
        return $this->render('edit-product-receipt');
    }

    /*
     * 自定义时间类型时请求的ajax处理
     * @author chengjuanjuan
     * @date 2018-05-25
     */
    public function actionSetTime()
    {
        $timeType = Yii::$app->request->post('time_type', '');
        $date = Yii::$app->request->post('date', '');
        $setTime = Helper::setTime();
        $time = '';
        $str = '';
        $time = time();
        foreach ($setTime as $v) {
            $selectTime = strtotime($date . ' ' . $v);
            $last = ($selectTime - $time);
            $last = $last > 0 ? $last : 0;
            $max = strtotime($date) - strtotime(date('Y-m-d 23:59:59'));
            $max = $max > 0 ? $max : 0;
            if ($last < 86400 && $max < 1) {
                if ($last <= 0 && $max <= 0) {
                    continue;
                } else if (($last > 0 && $last < (60 * 15)) && $max < 1) {
                    continue;
                }
            }

            $str .= '<option value="' . $v . '">' . $v . '</option>';
        }
        if ($str == '') {
            $str = '<option>当前暂无匹配时间</option>';
        }
        $str = '<select id="setTime" class="form-control" name="setTime">' . $str . '</select>';
        exit($str);
    }

    /**
     * 导出参数组合
     * @param $params
     * @return string
     */
    private static function getExportParams($params)
    {
        $params['pageSize'] = 100;
        $params['page'] = 1;
        $params['export'] = 1;

        return http_build_query($params);
    }

    /**
     * 导出
     * @param $result
     * @param $xlsName
     */
    private static function exportExcelOld($result, $xlsName)
    {
        $exportData = $result['data']['list'];
        while ($result['data']['totalPage'] > $result['data']['page']) {
            $_GET['page'] = $result['data']['page'] + 1;
            $result = OrderLogic::getList();
            $exportData = array_merge($exportData, $result['data']['list']);
        }

        $cells = [
            'order_no' => '订单编号',
            'service_depar_name' => '订单所属机构',
            'account_name' => '客户姓名',
            'account_mobile' => '客户电话',
            'address_desc' => '服务地址',
            'work_type_desc' => '服务类型',
            'prod_desc' => '产品名称',
            'class_desc' => '产品类目',
            'brand_name' => '品牌',
            'serial_number' => '序列号',
            'is_scope_desc' => '质保状态',
            'plan_time' => '预约服务时间',
            'finish_time' => '服务完成时间',
            'create_user_desc' => '下单人',
            'create_time' => '下单时间',
            'status_desc' => '订单状态',
            'order_amount' => '订单金额',
            'description' => '备注'
        ];
        $newArr = [];
        foreach ($cells as $key => $val) {
            $newArr[$key]['key'] = $key;
            $newArr[$key]['value'] = $val;
            $newArr[$key]['width'] = 30;
        }
        $cells = array_values($newArr);
        ExcelHelper::CreateExcel($cells, $exportData, $xlsName, $xlsName);
    }

    /**
     * 导出
     * 2018-9-19 修改新需求
     * @param $result
     * @param $xlsName
     */
    private static function exportExcel($result, $xlsName)
    {
        $exportData = $result['data']['list'];
        while ($result['data']['totalPage'] > $result['data']['page']) {
            $exportData = isset($exportData['data']) ? $exportData : [];
            $_GET['page'] = $result['data']['page'] + 1;
            $result = OrderLogic::getList();
            $exportData = array_merge($exportData, $result['data']['list']);
        }
        $cells = [
            'order_no' => '工单号',
            'work_no' => '子工单号',
            'department_name' => '所属机构',
            'account_name' => '客户姓名',
            'account_mobile' => '客户电话',
            'technicianInfos' => '服务技师',
            'address_desc' => '服务地址',
            'work_type_desc' => '服务类型',
            'work_stage_desc' => '服务流程',
            'prod_name' => '服务产品',
            'class_name' => '产品类目',
            'brand_name' => '品牌',
            'sn' => '序列号',
            'amount_type_name' => '质保状态',
            'plan_time' => '预约服务时间',
            'finsh_service_time' => '服务完成时间',
            'create_user' => '下单人',
            'create_time' => '下单时间',
            'cost_list' => '收费项',
            'total_cost_amount' => '总计金额',
            'total_cost_rel_amount' => '实收金额',
            'pay_type_name' => '收费方式',
            'status_desc' => '子订单状态',
            'description' => '备注'
        ];
        $newArr = [];
        foreach ($cells as $key => $val) {
            $newArr[$key]['key'] = $key;
            $newArr[$key]['value'] = $val;
            $newArr[$key]['width'] = 30;
        }
        $cells = array_values($newArr);
        ExcelHelper::CreateExcel($cells, $exportData, $xlsName, $xlsName);
    }

    /**
     * 通过审核
     * @author xi
     * @date 2018-6-23
     */
    public function actionThroughAudit()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $orderNo = Yii::$app->request->get('order_no', '');

            if ($orderNo == '') {
                return BaseLogic::error('订单号不能为空', 20002);
            }

            return OrderLogic::throughAudit($orderNo);
        }
    }

    /**
     * 审核不通过操作
     * @author xi
     */
    public function actionNotThrough()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $orderNo = Yii::$app->request->get('order_no', '');
            $reason = Yii::$app->request->get('reason', '');

            if ($orderNo == '') {
                return BaseLogic::error('订单号不能为空', 20002);
            }
            if ($reason == '') {
                return BaseLogic::error('不通过原因不能为空', 20002);
            }

            return OrderLogic::notThrough($orderNo, $reason);
        }
    }

    /**
     * 指派
     * @author xi
     * @return type
     */
    public function actionAssign()
    {
        //ajax 搜索
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = OrderLogic::assign();
            /*推送主动获取位置*/
            if ($result['data']['jsList']) {
                $arr = $result['data']['jsList'];
                $technician = array_column($arr, 'technician_id');
                if ($technician) {
                    Push::sendCustom($technician);
                }
            }
            return $result['data']['jsList'];
        }

        $result = OrderLogic::assign();

        if (isset($result['success']) && !$result['success']) {
            return $this->goBack();
        }


        $groupingList = Grouping::getGroupingAttr($this->departmentId, 'id,name,total_technician,group_leader,department_id');

        ArrayHelper::multisort($groupingList, 'total_technician', SORT_DESC);

        $defaultData = GroupingRelTechnician::getDefaultTechnician(['department_id' => BaseLogic::getDepartmentId()], 1, 1000);
        /*  echo '<pre>';
          print_R($groupingList);exit;*/

        //获取客户信息
        $accountData = [];
        if (isset($result['data']['work']['account_id']) && !empty($result['data']['work']['account_id'])) {
            $account = Account::getOne(['id' => $result['data']['work']['account_id']]);
            if (!empty($account)) {
                $accountData['name'] = $account->account_name;
                $accountData['mobile'] = $account->mobile;
                $accountData['avatar_url'] = $account->avatar_url;
            }
        }

        if (isset($result['data']['work']['address_id']) && !empty($result['data']['work']['address_id'])) {
            $accountAddress = AccountAddress::getOne(['id' => $result['data']['work']['address_id']]);
            if (!empty($accountAddress)) {
                $region = Region::getCityName(['region_id' => [$accountAddress->province_id, $accountAddress->city_id, $accountAddress->district_id]]);
                $regionName = array_column($region, 'region_name', 'region_id');

                if (!empty($regionName)) {
                    $accountData['address'] = $regionName[$accountAddress->province_id] . $regionName[$accountAddress->city_id] . $regionName[$accountAddress->district_id] . $accountAddress->address;
                }
            }
        }

        //获取时间
        $this->_view['setTimeType'] = Helper::setTimeType();
        $this->_view['departmentId'] = BaseLogic::getDepartmentId();
        $this->_view['defaultGroupCount'] = $defaultData['totalCount'];
        $this->_view['groupingArr'] = $groupingList;
        $this->_view['data'] = $result['data']['jsList'];
        $this->_view['workData'] = $result['data']['work'];
        $this->_view['orderNo'] = trim(Yii::$app->request->get('order_no', ''));
        $this->_view['referrer'] = Yii::$app->request->getReferrer();
        $this->_view['addressArr'] = $result['data']['addressArr'];
        $this->_view['title'] = $result['data']['title'];
        $this->_view['local_url'] = trim(Yii::$app->request->get('local_url', ''));
        $this->_view['account'] = $accountData;

        return $this->render('assign');
    }

    /**
     * 指派操作
     * @author xi
     * @date 2018-4-26
     */
    public function actionAjaxAssign()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return OrderLogic::assignTechnician();
        }
    }


    /**
     * 改派技师
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public function actionReAssign()
    {
        $orderNo = trim(Yii::$app->request->get('order_no', ''));
        $workNo = trim(Yii::$app->request->get('work_no', ''));

        $result = OrderLogic::assign();

        if (isset($result['success']) && !$result['success']) {
            return $this->goBack();
        }
        if (!$workNo) {
            $workList = $result['data']['work']['workArr'];
            //理论上只有1条数据
            foreach ($workList as $v) {
                if ($v['status'] <= 2) {
                    $workNo = $v['work_no'];
                    break;
                }
            }
        }

        $oldTechnicianArr = WorkRelTechnician::getTechnicianIdByWorkNo($orderNo);
        $groupingList = Grouping::getGroupingAttr($this->departmentId, 'id,name,total_technician,group_leader');
        ArrayHelper::multisort($groupingList, 'total_technician', SORT_DESC);

        $defaultData = GroupingRelTechnician::getDefaultTechnician($this->manufactorId, 1, 1000);

        //获取客户信息
        $accountData = [];
        if (isset($result['data']['work']['account_id']) && !empty($result['data']['work']['account_id'])) {
            $account = Account::getOne(['id' => $result['data']['work']['account_id']]);
            if (!empty($account)) {
                $accountData['name'] = $account->account_name;
                $accountData['mobile'] = $account->mobile;
                $accountData['avatar_url'] = $account->avatar_url;
            }
        }

        if (isset($result['data']['work']['address_id']) && !empty($result['data']['work']['address_id'])) {
            $accountAddress = AccountAddress::getOne(['id' => $result['data']['work']['address_id']]);
            if (!empty($accountAddress)) {
                $region = Region::getCityName(['region_id' => [$accountAddress->province_id, $accountAddress->city_id, $accountAddress->district_id]]);
                $regionName = array_column($region, 'region_name', 'region_id');

                if (!empty($regionName)) {
                    $accountData['address'] = $regionName[$accountAddress->province_id] . $regionName[$accountAddress->city_id] . $regionName[$accountAddress->district_id] . $accountAddress->address;
                }
            }
        }

        //获取时间
        $this->_view['setTimeType'] = Helper::setTimeType();
        $this->_view['defaultGroupCount'] = $defaultData['totalCount'];
        $this->_view['groupingArr'] = $groupingList;
        $this->_view['data'] = $result['data']['jsList'];
        $this->_view['workData'] = $result['data']['work'];
        $this->_view['workNo'] = $workNo;
        $this->_view['orderNo'] = $orderNo;
        $this->_view['referrer'] = Yii::$app->request->getReferrer();
        $this->_view['addressArr'] = $result['data']['addressArr'];
        $this->_view['title'] = $result['data']['title'];
        $this->_view['oldTechnicianArr'] = $oldTechnicianArr;
        $this->_view['local_url'] = trim(Yii::$app->request->get('local_url', ''));
        $this->_view['account'] = $accountData;
        return $this->render('re-assign');
    }

    /**
     * 指派
     * @author xi
     * @date 2018-4-26
     */
    public function actionAjaxReAssign()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return OrderLogic::reAssignTechnician();
        }
    }

    /*
     * 指派机构
     * @author chengjuanjuan
     * @date 2018-06-22
     */
    public function actionAssiginInstitution()
    {
        //指派操作
        if (Yii::$app->request->post()) {
            $orderNo = trim(Yii::$app->request->post('order_no', ''));
            $serviceDepartmentId = intval(Yii::$app->request->post('service_depart_id', ''));

            $res = OrderLogic::AssiginInstitution($orderNo, $serviceDepartmentId);
            if ($res['success']) {
                return json_encode(BaseLogic::success('', '指派成功'));
            }

            return json_encode(BaseLogic::error($res['message']));
        }

        $url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        if ($url) {
            $urlArr = explode('/', $url);
            $url = '/' . $urlArr[3] . '/' . $urlArr[4];
        }
        $totalWork = intval(Yii::$app->request->get('total_work', '1'));
        $serviceDemartmenId = intval(Yii::$app->request->get('service_demartment_id', '0'));
        return $this->renderPartial('assigin-institution',
            [
                'department_id' => BaseLogic::getDepartmentId(),
                'top_id' => BaseLogic::getTopId(),
                'order_no' => trim(Yii::$app->request->get('order_no', '')),
                'address_id' => intval(Yii::$app->request->get('address_id', '')),
                'location_url' => $url,
                'total_work' => $totalWork,
                'service_department_id' => $serviceDemartmenId,
                'selfRoles' => $this->selfRoles
            ]);

    }

    /*
     * 改派机构
     * @author chengjuanjuan
     * @date 2018-06-22
     */
    public function actionReAssiginInstitution()
    {
        if (Yii::$app->request->post()) {
            $orderNo = trim(Yii::$app->request->post('order_no', ''));
            $serviceDepartmentId = intval(Yii::$app->request->post('service_depart_id', ''));
            $res = OrderLogic::ReAssiginInstitution($orderNo, $serviceDepartmentId, '改派机构');

            if ($res['success']) {
                return json_encode(BaseLogic::success('', '改派成功'));
            }
            return json_encode(BaseLogic::error($res['message']));
        }

        $url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        if ($url) {
            $urlArr = explode('/', $url);
            $url = '/' . $urlArr[3] . '/' . $urlArr[4];
        }
        $totalWork = intval(Yii::$app->request->get('total_work', '1'));
        $serviceDemartmenId = intval(Yii::$app->request->get('service_demartment_id', '0'));
        return $this->renderPartial('re-assigin-institution',
            [
                'department_id' => BaseLogic::getDepartmentId(),
                'top_id' => BaseLogic::getTopId(),
                'order_no' => trim(Yii::$app->request->get('order_no', '')),
                'address_id' => intval(Yii::$app->request->get('address_id', '')),
                'location_url' => $url,
                'total_work' => $totalWork,
                'service_department_id' => $serviceDemartmenId,
                'selfRoles' => $this->selfRoles
            ]);

    }

    //插入直属部门id
    public static function actionInsertDirId()
    {
        $list = WorkOrder::findAllByAttributes(['and', ['!=', 'direct_company_id', 19], ['=', 'direct_company_id', 0]], "id,department_id");
        foreach ($list as $val) {
            $dir_id = Department::findOne(['id' => $val['department_id']]);
            if ($dir_id) {
                WorkOrder::updateAll(['direct_company_id' => $dir_id->direct_company_id], ['id' => $val['id']]);
            }
        }
        unset($list);
    }

    //获取智邦数据
    public function actionAjaxGetZbData()
    {
        Yii::$app->response->format = "json";
        $zb_mobile = Yii::$app->request->post('zb_mobile', '');
        $selectId = Yii::$app->request->post('select_id', '');
        $search = '/^0?1[3|4|5|6|7|8][0-9]\d{8}$/';
        if (preg_match($search, $zb_mobile)) {  //根据手机号获取客户信息
            $zb_data = AccountLogic::ZbData($zb_mobile, $selectId, 0);
            if (count($zb_data) > 1 && $selectId == '' && !isset($zb_data['search_user'])) {
                return BaseLogic::success($zb_data);
            }
            //登录异常 多调用一次
            if (isset($zb_data['code']) && $zb_data['code'] == 20001) {
                $zb_data = AccountLogic::ZbData($zb_mobile, $selectId, 0);
                if (isset($zb_data['code']) && $zb_data['code'] == 20001) {
                    return BaseLogic::error($zb_data['message'], 20002);
                }
                if (count($zb_data) > 1 && $selectId == '' && !isset($zb_data['search_user'])) {
                    return BaseLogic::success($zb_data);
                }
            }
        } else { //地址
            $zb_data = AccountLogic::ZbData($zb_mobile, $selectId, 1);
            if (count($zb_data) > 1 && $selectId == '' && !isset($zb_data['search_user'])) {
                return BaseLogic::success($zb_data);
            }
            //登录异常 多调用一次
            if (isset($zb_data['code']) && $zb_data['code'] == 20001) {
                $zb_data = AccountLogic::ZbData($zb_mobile, $selectId, 1);
                if (isset($zb_data['code']) && $zb_data['code'] == 20001) {
                    return BaseLogic::error($zb_data['message'], 20002);
                }
                if (count($zb_data) > 1 && $selectId == '' && !isset($zb_data['search_user'])) {
                    return BaseLogic::success($zb_data);
                }
            }
        }
        //添加客户及联系地址
        $account_id = AccountLogic::AddZbAccount($zb_data);
        if ($account_id > 0) {
            $zb_data['uid'] = $account_id;
            return BaseLogic::success($zb_data);
        } else if ($account_id == 0) {
            return BaseLogic::error("未找到相关客户，请重新填写搜索信息后再次尝试");
        } else {
            return BaseLogic::success($zb_data, "success", $account_id);
        }
    }

    //智邦合同列表
    public function actionGetContract()
    {
        Yii::$app->response->format = "json";
        $page = intval(Yii::$app->request->post('page', 1));
        $user_id = intval(Yii::$app->request->post('user_id', ''));
        if ($user_id == '') {
            return BaseLogic::error("参数错误，请重新搜索！");
        }
        $list = AccountLogic::getContractList($user_id, $page);
        return BaseLogic::success($list);
    }

    //智邦合同详情
    public function actionGetContractDetail()
    {
        Yii::$app->response->format = "json";
        $contract_id = intval(Yii::$app->request->post('contract_id', ''));
        if ($contract_id == '') {
            return BaseLogic::error("参数错误，请重新搜索！");
        }
        $detail = AccountLogic::getContractDetail($contract_id);
        return BaseLogic::success($detail);
    }

    //智邦售后服务列表
    public function actionGetService()
    {
        Yii::$app->response->format = "json";
        $page = intval(Yii::$app->request->post('page', 1));
        $user_id = intval(Yii::$app->request->post('user_id', ''));
        if ($user_id == '') {
            return BaseLogic::error("参数错误，请重新搜索！");
        }
        $list = AccountLogic::getServiceList($user_id, $page);
        return BaseLogic::success($list);
    }

    //智邦合同详情
    public function actionGetServiceDetail()
    {
        Yii::$app->response->format = "json";
        $service_id = intval(Yii::$app->request->post('service_id', ''));
        if ($service_id == '') {
            return BaseLogic::error("参数错误，请重新搜索！");
        }
        $detail = AccountLogic::getServiceDetail($service_id);
        return BaseLogic::success($detail);
    }

    // 智邦产品添加到订单
    public function actionAddZbProduct()
    {
        Yii::$app->response->format = "json";
        $res = AccountLogic::AddZbProduct();
        if ($res == 20007) {
            return BaseLogic::error('您还未选择产品，请选择产品后，再次尝试!', 20007);
        }
        if ($res == 20008) {
            return BaseLogic::error('请先添加客户至优服务后，再添加产品!', 20008);
        }
        if ($res) {
            return BaseLogic::success($res);
        }
        return BaseLogic::success('', "产品已添加至订单");
    }

    public function actionAccountZb()
    {
        //判断是否是重新下单
        $orderNo = Yii::$app->request->get('order_no', '');
        $accountId = intval(Yii::$app->request->get('account_id', 0));

        //合同下单参数
        $type = intval(Yii::$app->request->get('type', 0));
        $contract_id = intval(Yii::$app->request->get('contract_id', 0));

        $page = Yii::$app->request->get('page', 1);
        //$zb_data = AccountLogic::ZbData('18612585020',$page);
        //print_r($zb_data);exit;
        //$account_id = AccountLogic::AddZbAccount($zb_data);
        //if($account_id){
        //  $zb_data['custom_Info']['account_id'] = $account_id;
        //}
        //AccountLogic::AddZbProduct();
        $data = $address = $service = $provider = [];
        if ($orderNo) {
            //根据ID查询当前订单信息
            $data = OrderLogic::getOrderInfo($orderNo);
            //客户对应地址
            $address = AccountAddress::getAddress($data['account_id']);
            //客户对应服务
            $service = SaleOrder::getServiceBrand($data['account_id']);
            //客户对应服务商
            $provider = [];
        }

        //下单处理
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $assign = Yii::$app->request->post('assign');
            //调用逻辑层
            $result = OrderLogic::add();
            if ($result['code'] == 200) {
                if ($assign == 2) {
                    $url = '/order/wait-assign';
                    if ($result['data']['order_no'] != '' && $result['data']['work_no'] != '') {
                        $url = '/order/assign?order_no=' . $result['data']['order_no'] . '&work_no=' . $result['data']['work_no'] . '&local_url=/order/index';
                    }
                    return BaseLogic::success(['url' => $url]);
                }
                return BaseLogic::success(['url' => '/order/index']);
            } else {
                return BaseLogic::error($result['message']);
            }
        }

        //服务类型
        $serviceType = ManufactorType::getServiceType();
        //质保状态
        $amount_type = BaseModel::getAmountType();
        $info['data'] = [];
        //合同下单
        if ($type == 1) {
            //获取合同信息
            if ($contract_id) {
                $info = ContractLogic::getDetail($contract_id, '', 9);

                if ($info['success'] && $info['data']) {
                    //合同下的客户
                    $accountId = $info['data']['info']['account_id'];
                    //所属机构
                    $info['data']['info']['department_name'] = Department::getDepartmentName($info['data']['info']['department_id']);
                    //查询售后产品是否存在
                    $s_res = SaleOrder::findAllByAttributes(['contract_id' => $info['data']['info']['id']]);
                    //已存在的产品
                    $exsit_ids = array_column($s_res, 'prod_id');
                    //不存在的
                    $add_data = [];
                    foreach ($info['data']['detail'] as $key => $val) {
                        if (!in_array($val['prod_id'], $exsit_ids)) {
                            $add_data[] = $val;
                        }
                    }
                    //不存在的添加到sale_order
                    if (!empty($add_data)) {
                        //添加售后产品
                        SaleOrder::batchSale($add_data, 1);
                    }
                    //获取合同产品对应的售后产品id
                    foreach ($info['data']['detail'] as $k => $v) {
                        $sale_res = SaleOrder::findOneByAttributes(['contract_id' => $v['parent_id'], 'status' => 1, 'department_id' => $this->departmentId, 'contract_order' => 1, 'contract_detail_id' => $v['id']], 'id');
                        $info['data']['detail'][$k]['sale_order_id'] = $sale_res['id'];
                    }
                }
            } else {
                $this->redirect("/contract/wait-check");
            }
            foreach ($serviceType as $key => $val) {
                if ($val['id'] == Yii::$app->params['chaidan']['workType']) {
                    unset($serviceType[$key]);
                }
            }
            if (!empty($serviceType)) {
                $data['work_type'] = reset($serviceType)['id'];
            }
        }

        //获取服务流程
        $serviceWorkStage = [];
        if ($serviceType) {

            if ($type == 1) {
                $serviceTypeId = $data['work_type'];
            } else {
                $serviceTypeId = $serviceType[0]['id'];
            }
            $re = FlowLogic::getFlowStage($serviceTypeId, [1, 2]);
            if ($re['code'] == 200) {
                $serviceWorkStageNum = isset($re['data'][1]) ? $re['data'][1] : [];
                if ($serviceWorkStageNum) {
                    $serviceWorkStage = $re['data'];
                } else {
                    $serviceWorkStage = [];
                }
            }
        }
        //客户信息
        $accountArr = [];
        if ($accountId > 0) {
            $accountArr = Account::findOneByAttributes(['id' => $accountId], 'id,account_name,mobile');
        }

        $model = new OrderForm();
        if ($orderNo != '' && isset($data['workDetail'][0]['fault_img_arr'])) {
            $model->fault_img = $data['workDetail'][0]['fault_img_arr'];
        }

        //设置时间类型
        $setTimeType = Helper::setTimeType();
        //获取机构所需数据
        $DepartmentInfo = DepartmentLogic::getList(3);
        $DepartmentInfo = isset($DepartmentInfo['list'][0]) ? $DepartmentInfo['list'][0] : [];
        //获取拆单开关状态
        $dismantling_status = 2; //默认不启用
        $res = DismantlingConfig::getOne(['direct_company_id' => $this->directCompanyId]);
        if (!empty($res)) {
            $dismantling_status = $res['status'];
        }
        return $this->render('account-zb', [
            'type' => $type,
            'contract_info' => $info['data'],
            'serviceType' => $serviceType,
            'amount_type' => $amount_type,
            'data' => $data,
            'address' => $address,
            'service' => $service,
            'provider' => $provider,
            'accountArr' => $accountArr,
            'model' => $model,
            'setTimeType' => $setTimeType,
            'dismantling_status' => $dismantling_status,
            'serviceWorkStage' => $serviceWorkStage,
            'DepartmentInfo' => $DepartmentInfo
        ]);
        // return $this->render('account-zb');
    }

    // 添加评论
    public function actionAddAppraisal()
    {
        return $this->render('add-appraisal');
    }

    // 待评论列表
    public function actionWaitAppraisal()
    {
        return $this->render('wait-appraisal');
    }

}

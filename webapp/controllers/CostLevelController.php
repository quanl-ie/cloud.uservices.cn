<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace webapp\controllers;

use Yii;
use webapp\logic\BaseLogic;
use webapp\logic\CostLevelLogic;
use common\helpers\Paging;

/**
 * Description of CostLevel
 *
 * @author liuxingqi <lxq@c-ntek.com>
 */
class CostLevelController extends BaseController
{
    //put your code here
    /**
     * 列表
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public function actionIndex()
    {
        $result = CostLevelLogic::getList();
        $pageHtml = '';
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }

        $this->_view['data']     = $result['list'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['departmentId'] = BaseLogic::getDepartmentId();
        $this->_view['directTopId'] = BaseLogic::getTopId();
        $this->_view['directCompanyId'] = BaseLogic::getDirectCompanyId();
        return $this->render('index');
    }
    /**
     * 添加
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public function actionAdd()
    {
        $departmentId  = BaseLogic::getDepartmentId();
        $directTopId = BaseLogic::getTopId();
        if($departmentId != $directTopId){
            //return $this->redirect('/cost-level/index');
        }
        $data = CostLevelLogic::getAddList();
        if (Yii::$app->request->isPost) {
            $id  = Yii::$app->request->post('levelId',0);
            if ($id == 0) {
                $res = CostLevelLogic::add();
                return json_encode($res);
            }else{
                $res = CostLevelLogic::edit($id);
                return json_encode($res);
            }
        }
        if (Yii::$app->request->isGet) {
            $editData = CostLevelLogic::getEditList();
            if ($editData['code'] == 200) {
                $edit = $editData['data'];
                $this->_view['provinceId']      = isset($edit['provinceId']) ? $edit['provinceId'] : '';
                $this->_view['cityId']          = isset($edit['cityId']) ? $edit['cityId'] : '';
                $this->_view['districtId']      = isset($edit['districtId']) ? $edit['districtId'] : '';
                $this->_view['level']           = isset($edit['level']) ? $edit['level'] : '';
                $this->_view['class_ids']  = isset($edit['class_ids']) ? $edit['class_ids'] : '';
                $this->_view['class_ids_name']  = isset($edit['class_ids_name']) ? $edit['class_ids_name'] : '';
                $this->_view['cost_level_id']  = isset($edit['cost_level_id']) ? $edit['cost_level_id'] : '';
                $this->_view['cost_level_item_ids']  = isset($edit['cost_level_item_ids']) ? $edit['cost_level_item_ids'] : '';
            }
        }
        $this->_view['province']  = isset($data['province']) ? $data['province'] : '';
        $this->_view['city']      = isset($data['city']) ? $data['city'] : '';
        $this->_view['district']  = isset($data['district']) ? $data['district'] : '';
        $this->_view['brand']     = isset($data['brand']) ? $data['brand'] : '';
        $this->_view['type']      = isset($data['type']) ? $data['type'] : '';
        $this->_view['class']     = isset($data['class']) ? $data['class'] : '';
        $this->_view['cost_item'] = isset($data['cost_item']) ? $data['cost_item'] : '';
        $this->_view['unit']      = isset($data['unit']) ? $data['unit'] : '';
        return $this->render('add');
    }
    /**
     * 添加
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public function actionEdit()
    {
        $departmentId  = BaseLogic::getDepartmentId();
        $directTopId = BaseLogic::getTopId();
        if($departmentId != $directTopId){
            //return $this->redirect('/cost-level/index');
        }
        $data = CostLevelLogic::getAddList();
        if (Yii::$app->request->isPost) {
            $id  = Yii::$app->request->post('levelId',0);
            if ($id == 0) {
                $res = CostLevelLogic::add();
                return json_encode($res);
            }else{
                $res = CostLevelLogic::edit($id);
                return json_encode($res);
            }
        }
        if (Yii::$app->request->isGet) {
            $editData = CostLevelLogic::getEditList();
            if ($editData['code'] == 200) {
                $edit = $editData['data'];
                $this->_view['provinceId']      = isset($edit['provinceId']) ? $edit['provinceId'] : '';
                $this->_view['cityId']          = isset($edit['cityId']) ? $edit['cityId'] : '';
                $this->_view['districtId']      = isset($edit['districtId']) ? $edit['districtId'] : '';
                $this->_view['level']           = isset($edit['level']) ? $edit['level'] : '';
                $this->_view['class_ids']  = isset($edit['class_ids']) ? $edit['class_ids'] : '';
                $this->_view['class_ids_name']  = isset($edit['class_ids_name']) ? $edit['class_ids_name'] : '';
                $this->_view['cost_level_id']  = isset($edit['cost_level_id']) ? $edit['cost_level_id'] : '';
                $this->_view['cost_level_item_ids']  = isset($edit['cost_level_item_ids']) ? $edit['cost_level_item_ids'] : '';
            }
        }
        $this->_view['province']  = isset($data['province']) ? $data['province'] : '';
        $this->_view['city']      = isset($data['city']) ? $data['city'] : '';
        $this->_view['district']  = isset($data['district']) ? $data['district'] : '';
        $this->_view['brand']     = isset($data['brand']) ? $data['brand'] : '';
        $this->_view['type']      = isset($data['type']) ? $data['type'] : '';
        $this->_view['class']     = isset($data['class']) ? $data['class'] : '';
        $this->_view['cost_item'] = isset($data['cost_item']) ? $data['cost_item'] : '';
        $this->_view['unit']      = isset($data['unit']) ? $data['unit'] : '';
        return $this->render('add');
    }
    /**
     * 查看收费标准
     * @author sxz
     * @return type
     */
    public function actionView()
    {
        $result = CostLevelLogic::getCostDetail();
        if ($result['code'] != 200) {
            Yii::$app->session->setFlash('message',$result['message']);
        }
        $this->_view['data']     = isset($result['data']) ? $result['data'] : '';
        return $this->render('view');
    }
    /**
     * ajax品牌、产品类目、服务类型联动获取收费标准
     * @author sxz
     * @return type
     */
    public function actionRuleLinkAge()
    {
        $data = CostLevelLogic::ruleLinkAge();
        return json_encode($data);
    }
    /**
     * ajax删除收费项目
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public function actionDel()
    {
        $idArr = Yii::$app->request->post('id',[]);
        $data = CostLevelLogic::del($idArr);
        return json_encode($data);
    }
    /**
     * 保存收费标准
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public function actionSubmit()
    {
        $data = CostLevelLogic::submit();
        return json_encode($data);
    }
    /**
     * ajax品牌、产品类目、服务类型联动获取收费项目
     * @return type
     */
    public function actionLinkAge()
    {
        $data = CostLevelLogic::linkAge();
        return json_encode($data);
    }
    /**
     * ajax 获取服务区域城市和区县
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public function actionCity()
    {
        $provinceId = Yii::$app->request->post('province');
        $provinceIds = array_filter($provinceId);
        $data = CostLevelLogic::getCity($provinceIds);
        return json_encode($data);
    }
    /**
     * ajax获取服务区域区县
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public function actionDistrict()
    {
        $citysId = Yii::$app->request->post('citys');
        $citysIds = array_filter($citysId);
        $data = CostLevelLogic::getDistrict($citysIds);
        return json_encode($data);
    }
    /**
     * 添加收费标准获取产品类目
     * @author sxz 2018-06-13
     * @return type
     */
    public function actionGetClass()
    {
        $result = CostLevelLogic::getClass(0,1);
        if (Yii::$app->request->isPost) {
            $pid  = Yii::$app->request->post('pid',0);
            $res = CostLevelLogic::getClass($pid,1);
            return json_encode($res);
        }

        return $this->renderPartial('get-class',[
            'data'=>$result,
        ]);
    }
    /**
     * ajax设置收费项目状态
     * @author sxz
     * @return type
     */
    public function actionSetStatus()
    {
        $t    = intval(Yii::$app->request->get('t',1));
        $id   = intval(Yii::$app->request->get('id',0));
        if ($id<= 0) {
            return $this->redirect('/cost-level/index');
        }
        if($t == 1){
            $status = 2;
        }
        if($t == 2){
            $status = 1;
        }
        $data = CostLevelLogic::setStatus($id,$t,$status);
        if($data['success']){
            Yii::$app->session->setFlash('message','操作成功!');
            return $this->redirect ('/cost-level/set-status');
        }
        return $this->redirect('/cost-level/set-status');
    }
    /**
     * ajax设置收费项目状态  启用
     * @author sxz
     * @return type
     */
    public function actionSetStatusOn()
    {
        $t    = intval(Yii::$app->request->get('t',1));
        $id   = intval(Yii::$app->request->get('id',0));
        if ($id<= 0) {
            return $this->redirect('/cost-level/index');
        }
        if($t == 1){
            $status = 2;
        }
        if($t == 2){
            $status = 1;
        }
        $data = CostLevelLogic::setStatus($id,$t,$status);
        if($data['success']){
            Yii::$app->session->setFlash('message','操作成功!');
            return $this->redirect ('/cost-level/set-status');
        }
        return $this->redirect('/cost-level/set-status');
    }
    /**
     *取消编辑收费标准后重置数据
     *
     *
     */
    public function actionResetItem(){
        $res = CostLevelLogic::ResetItem();
    }

}

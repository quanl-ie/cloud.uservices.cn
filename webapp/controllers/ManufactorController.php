<?php
namespace webapp\controllers;

use common\models\User;
use webapp\logic\DepartmentLogic;
use webapp\models\Department;
use webapp\models\Manufactor;
use Yii;
use webapp\controllers\BaseController;
use webapp\logic\ManufactorLogic;
use webapp\logic\BaseLogic;
use common\models\Region;
use common\helpers\Helper;


/**
 * 企业资料
 * @author sxz
 * @date   2018-06-07
 */
class ManufactorController extends BaseController
{
    /**
     * 查看详情
     * @author sxz
     * @date   2018-06-07
     */
    public function actionView()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect('/site/login');
        }
        //获取当前系统的id
        $id              = BaseLogic::getDepartmentId();
        $derectCompanyId = BaseLogic::getDirectCompanyId();
        $topId           = BaseLogic::getTopId();
        //总公司信息
        $parentInfo     = Department::getOne(['id'=>$topId]);
        $data           = ManufactorLogic::getOne([$parentInfo->manufactor_id]);
        $province_name  = Region::getCityNameOne(['region_id'=>$data['model']->province_id]);
        $city_name      = Region::getCityNameOne(['region_id'=>$data['model']->city_id]);
        $district_name  = Region::getCityNameOne(['region_id'=>$data['model']->district_id]);
        //经销商信息
        if($topId != $derectCompanyId && $derectCompanyId == $id)
        {
            $mainInfo = Department::getOne(['id'=>$id]);
            $city_name = Region::getCityNameOne(['region_id'=>$mainInfo->city_id]);
            $district_name = Region::getCityNameOne(['region_id'=>$mainInfo->district_id]);
            $data['model']->company     = $mainInfo->name;
            $data['model']->contact     = $mainInfo->contacts;
            $data['model']->mobile      = $mainInfo->phone;
            $data['model']->email       = $mainInfo->email;
            $data['model']->province_id = $mainInfo->province_id;
            $data['model']->city_id     = $mainInfo->city_id;
            $data['model']->district_id = $mainInfo->district_id;
            $data['model']->city_name   = $city_name;
            $data['model']->district_name = $district_name;
            $data['model']->company_desc = $mainInfo->remark;
            //本公司 or 本公司一下统一拿总公司信息
        }
        else
        {
            if($id==$topId)
            {
                $mainId = $id;
                $mainInfo = ManufactorLogic::getInfo($mainId);
                $data['model'] = $mainInfo;
            }
            else
            {
                $mainId        = $topId;
                $mainInfo      = ManufactorLogic::getInfo($mainId);
                $data['model'] = $mainInfo;
            }
        }

        $departmentArr = Department::findOneByAttributes(['id'=>$id]);
        $manufactorArr = Manufactor::findOneByAttributes(['id'=>$departmentArr['manufactor_id']]);
        if($manufactorArr){
            $data['model']->logo = $manufactorArr['logo'];
        }

        if ($id <= 0 || !$data) {
            return $this->redirect('/manufactor/add');
        }
        if (Yii::$app->request->isPost)
        {
            $postData = Yii::$app->request->post();
            $mark = Yii::$app->request->post('mark','');
            //去更改经销商的资料信息
            if($mark)
            {
                $where = [
                    'id'         => $id,
                    'status'     => 1,
                    'del_status' => 1
                ];
                $DepartmentInfo = Department::find()->where($where)->one();
                if(!$DepartmentInfo){
                    return $this->redirect('/manufactor/view');
                }
                $res = DepartmentLogic::editInfo($id,$postData);
                Yii::$app->session->setFlash('message',$res['message']);
            }
            else{
                $res = ManufactorLogic::editInfo();
                Yii::$app->session->setFlash('message',$res['message']);
            }

            return $this->redirect('/manufactor/view');
        }
        $data['model']['province_name'] = $province_name['region_name'];
        $data['model']['city_name']     = $city_name['region_name'];
        $data['model']['district_name'] = $district_name['region_name'];

        return $this->render('view',[
            'model'             => $data['model'],
            'province'          => $data['province'],
            'derect_company_id' => $derectCompanyId,
            'top_id'            => $topId,
            'department_id'     => $id,
            'hasUploadAvatar'   => $departmentArr['parent_id'] == 0?true : false
        ]);
    }
    //填写认证信息
    public function actionAdd()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect('/site/login');
        }

        $data = ManufactorLogic::addList();
        if (Yii::$app->request->isPost)
        {
            $res = ManufactorLogic::add();
            if ($res['success'] == true) {
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('wait-audit');
            }
        }

        return $this->render('add',[
            'model'    => $data['model'],
            'company'  =>Yii::$app->user->getIdentity()->company,
            'province' => $data['province'],
        ]);
    }
    //提交资料成功等待页面
    public function actionWaitAudit()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect('/site/login');
        }
        return $this->render('wait-audit');
    }
    //未认证
    public function actionUncertified()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect('/site/login');
        }
        return $this->render('uncertified');
    }
    //待审核
    public function actionToAudit()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect('/site/login');
        }
        return $this->render('to-audit');
    }
    //审核未通过
    public function actionUnAudit()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect('/site/login');
        }
        return $this->render('un-audit');
    }
}


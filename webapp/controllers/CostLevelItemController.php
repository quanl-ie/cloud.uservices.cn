<?php


namespace webapp\controllers;

use Yii;
use webapp\models\CostLevelItem;

class CostLevelItemController extends BaseController
{

    /**
     * ajax 获取收费项目详情
     *
     */
    public function actionCostDetail()
    {
        $cost_item_id = Yii::$app->request->post('cost_item_id');
        $cost_item_info= CostLevelItem::getOnes(['id'=>$cost_item_id]);
        return json_encode($cost_item_info);
    }
    /**
     * ajax获取服务区域区县
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public function actionDistrict()
    {
        $citysId = Yii::$app->request->post('citys');
        $citysIds = array_filter($citysId);
        $data = CostLevelLogic::getDistrict($citysIds);
        return json_encode($data);
    }
}

<?php

namespace webapp\controllers;

use webapp\logic\ContractTypeLogic;
use Yii;
use webapp\logic\CategoryLogic;
use common\helpers\Paging;

class ContractTypeController extends BaseController
{
    /**
     * 列表
     * User: quan
     * Date: 2018/8/6
     * Time: 10:10
     * @return string
     */
    public function actionIndex()
    {
        $res = ContractTypeLogic::getIndex();
        $this->_view['data']     = $res['list'];
        return $this->render('index');
    }
    
    /**
     * 添加
     * User: quan
     * Date: 2018/8/6
     * Time: 13:10
     * @throws \Exception
     */
    public function actionAdd()
    {
        if (Yii::$app->request->isPost)
        {
            $res = ContractTypeLogic::add();
            return json_encode($res);
        }
        return $this->renderPartial('add');
    }
    
    /**
     * 修改
     * User: quan
     * Date: 2018/8/6
     * Time: 13:10
     * @return mixed|string|void
     * @throws \Exception
     */
    public function actionEdit()
    {
        $data = ContractTypeLogic::beforeEdit();

        if (Yii::$app->request->isPost)
        {
            $res = ContractTypeLogic::edit();
            return json_encode($res);
        }
        return $this->renderPartial('edit',
            [
                'data' => $data
            ]
        );
    }

    /**
     * 更改状态
     * User: quan
     * Date: 2018/8/6
     * Time: 13:10
     * @return mixed|string|void
     * @throws \Exception
     */
    public function actionChangeStatus()
    {
        
        $res = ContractTypeLogic::changeStatus();
        
        return json_encode($res);
        
    }
    /**
     * 更改状态
     * User: sxz
     * Date: 2018/8/10
     * Time: 13:10
     * @return mixed|string|void
     * @throws \Exception
     */
    public function actionChangeStatusOn()
    {
        $res = ContractTypeLogic::changeStatus();

        return json_encode($res);

    }
}


<?php
namespace webapp\controllers;

use Yii;
use yii\web\Controller;
use common\helpers\Paging;
use webapp\logic\CooporationLogic;
use webapp\logic\BaseLogic;
/**
 *@desc:合作服务商
 * @author: chengjuanjuan <chengjuanjuan@c-ntek.com>
 * createdAt: 2017/12/14 14:09
 *
 */
class CooporationController extends BaseController
{
    /**
    * @desc 合作服务商列表
    **/
    public function actionIndex(){
        $result = CooporationLogic::index();
        if($result['totalPage'] >=1 && !empty($result['list'])){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }
        $this->_view['firstClass']  = $result['firstClass'];
        $this->_view['secondClass'] = $result['secondClass'];
        
        
        $this->_view['list']       = $result['list'];
        $this->_view['statusList'] = $result['statusList'];
        $this->_view['pageHtml']   = $pageHtml;
        return $this->render('index');
    }
    /**
     * 查看商家经营品牌
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public function actionGetBrand()
    {
        $id = Yii::$app->request->get('id');
        $data = CooporationLogic::getSelfBrand($id);
        return $this->renderPartial('brand',['brand'=>$data]);
    }
    /**
     * 查看商家经营类目
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public function actionGetClass()
    {
        $id = Yii::$app->request->get('id');
        $data = CooporationLogic::getSelfClass($id);
        return $this->renderPartial('class',['class'=>$data]);
    }
    /**
     * 查看收费标准
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public function actionGetCostLevel()
    {
        
        $result = CooporationLogic::getCostLevel();

        if ($result['code'] != 200) {
            Yii::$app->session->setFlash('message',$result['message']);
        }
        $this->_view['manufactor']  = isset($result['data']['manufactor']) ? $result['data']['manufactor'] : '';
        $this->_view['province']  = isset($result['data']['province']) ? $result['data']['province'] : '';
        $this->_view['city']      = isset($result['data']['city']) ? $result['data']['city'] : '';
        $this->_view['district']  = isset($result['data']['district']) ? $result['data']['district'] : '';
        $this->_view['brand']     = isset($result['data']['brand']) ? $result['data']['brand'] : '';
        $this->_view['type']      = isset($result['data']['type']) ? $result['data']['type'] : '';
        $this->_view['class']     = isset($result['data']['class']) ? $result['data']['class'] : '';
        return $this->render('cost-level');
    }
    //查看收费规则
    public function actionGetCostRule()
    {
        $result = CooporationLogic::getCostRule();
        if ($result['code'] != 200) {
            Yii::$app->session->setFlash('message',$result['message']);
        }
        
        $this->_view['manufactor'] = isset($result['data']['manufactor']) ? $result['data']['manufactor'] : '';
        return $this->render('cost-rule');
    }

    /**
     * ajax品牌、产品类目、服务类型联动获取收费项目
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public function actionLinkAge()
    {
        $data = CooporationLogic::linkAge();
        return json_encode($data);
    }
    
    /**
     * ajax品牌、产品类目、服务类型联动获取收费项目
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public function actionRuleLinkAge()
    {
        $data = CooporationLogic::ruleLinkAge();
        return json_encode($data);
    }
    
    public function actionGetCity()
    {
        $id = Yii::$app->request->post('id');
        $type = Yii::$app->request->post('type');
        $provinceId = Yii::$app->request->post('cityId');
        $data = CooporationLogic::getCity($id,$provinceId,$type);
        return json_encode($data);
    }
    public function actionGetDistrict()
    {
        $id = Yii::$app->request->post('id');
        $type = Yii::$app->request->post('type');
        $cityId = Yii::$app->request->post('cityId');
        $data = CooporationLogic::getDistrict($id,$cityId,$type);
        return json_encode($data);
    }

    //合作门店
    function actionMd(){
        $result = CooporationLogic::md();
        if($result['totalPage'] >=1 && !empty($result['list'])){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }
        // $this->_view['params']     = $result[$q];
        $this->_view['data']     = $result['list'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['statusArr']  = $this->status;
        return $this->render('md');
    }

    function actionAdd(){
        if (yii::$app->request->post()) {
            //调用逻辑层
            $res = CooporationLogic::add();
            return json_encode($res);
        }
        return $this->render('add');

    }

    function actionInsert(){
        $res = CooporationLogic::add();
        return json_encode($res);
    }


    //更改合作状态
    function actionChangeStatus(){
       $res=  CooporationLogic::changeStatus();
       return json_encode($res);
    }


    function actionGetAccount(){
        $res = CooporationLogic::getAccount();
    }


    function actionSendMail(){
        $mail= Yii::$app->mailer->compose();
        $mailAddress = trim(yii::$app->request->post('mail'));
        $mail->setFrom('manufactor@uservices.cn');
        $mail->setTo($mailAddress); //要发送给那个人的邮箱
        $mail->setSubject("您的合作伙伴邀请您加入优服务平台"); //邮件主题
        $mail->setTextBody('智能云集'); //发布纯文字文本
        $mail->setHtmlBody("注册地址：<a href='http://www.uservices.cn/index/merchant/register.html'>http://www.uservices.cn/index/merchant/register.html 点击注册</a>"); //发送的消息内容
        if($mail->send())
            echo "success";
        else
            echo "failse";
        die();
    }
}


<?php

namespace webapp\controllers;

use common\helpers\Helper;
use common\models\PaySiginConfig;
use webapp\models\BaseModel;
use webapp\models\Department;
use webapp\models\DismantlingConfig;
use webapp\models\Manufactor;
use webapp\models\User;
use Yii;
use webapp\models\Menu;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use webapp\logic\BaseLogic;

class BaseController extends Controller
{

    //给视图传参
    public $_view = [];

    public $access = [];

    public $selfRoles = [];

    //商家id
    public $manufactorId = 0;

    public $departmentId = 15;
    //menu_id 判段权限用
    public $menuId = 5;
    //用户信息
    public $user = null;

    //数据权限
    public $dataAuthId = 0;
    //部门数据
    public $departmentObj = null;
    //直属公司id
    public $directCompanyId = 0;

    const DIS_TYPE = 1;
    const FEE_TYPE = 2;
    //访问来源
    public $source = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'captcha'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'ajax-user-list', 'add', 'ajax-user-add', 'upload', 'file-upload', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        $ca = $this->id . '_' . $action->id;

        //指派加 token 自动登录，外面对接用
        if($ca == 'order_assign')
        {
            $accessToken = trim(Yii::$app->request->get('token',''));
            if($accessToken != ''){
                $user = User::findIdentityByAccessToken($accessToken,'openapi_client');
                if($user){
                    Yii::$app->user->login($user, 86400 * 30);
                }
            }
        }

        Yii::$app->params['ca'] = $ca;
        $source = $this->checkmobile();
        if($source){
            $this->source = true;
        }
        //排除
        $excludeArr = [
            'user_forget-pwd',
            'user_send-sms',
            'user_captcha',
            'account_get-city',
            'account_get-district',
            'manufactor_uncertified',
            'manufactor_to-audit',
            'manufactor_un-audit',
            'manufactor_add',
            'manufactor_view',
            'message_get-new-msg',
            'index_index',
            'account_account-upload',
            'product_get-list',
            'supplier_get-list',
            'technician_update-area',
            'technician_get-skills',
            'cost-level_get-class',
            'order_assigin-institution',
            'order_re-assigin-institution',
            'order_view-all-product',
            'product_materials-detail',
            'order_account-zb',
            'technician-appraisal_import',

            //临时的
            'pay_wx-create-order',
            'pay_wx-notify',
            'pay_wx-order-query',
            'pay_ali-create-order',
            'pay_ali-return',
            'pay_ali-notify',
            'pay_ali-order-query',

            'order_insert-dir-id',
            'product_share-detail',
            'product_share-list',
            'product_materials',
            'contract_get-contract',
            'index_main',
            'open-weixin_index',

            'return-purchase_cmd',
            'account-address_change-coordinate',
            'account-address_show-coordinate',
        ];
        //判断如果未登录跳转到登录页面
        if (Yii::$app->user->isGuest && !in_array($ca, $excludeArr)) {
            if($this->source){
                $res = array(
                    'success' => false,
                    'code'=> 10005,
                    'message' => "未登录",
                    'data'=>[]
                );
                echo json_encode($res);exit;
            }else{
                return $this->redirect('/site/login')->send();
            }
        }

        if (!Yii::$app->user->isGuest) {
            $this->user = Yii::$app->user->getIdentity();

            Yii::$app->params['username'] = $this->user->mobile;
            if (trim($this->user->username) != '') {
                Yii::$app->params['username'] = $this->user->username;
            }

            //强制退出
            $cacheKey = 'logout' . $this->user->getId();
            if (Yii::$app->cache->exists($cacheKey)) {
                Yii::$app->cache->delete($cacheKey);
                return $this->redirect('/site/logout')->send();
            }

            $this->departmentId = $this->user->department_id;

            if ($this->departmentId) {
                Yii::$app->params['departmentId'] = $this->departmentId;

                //查询顶级企业信息
                $topId = BaseLogic::getTopId();
                $departmentInfo = Department::getOne(['id' => $topId]);
                if ($departmentInfo) {
                    $manufactor = Manufactor::getOne(['id' => $departmentInfo->manufactor_id]);
                    $this->manufactorId = $departmentInfo->manufactor_id;
                }
                Yii::$app->params['manufactor'] = isset($manufactor) ? $manufactor : [];
            } else {
                Yii::$app->params['manufactor']['company'] = $this->user->company;
            }

            //部门信息
            $sessionKey = 'departmentObj';
            if (Yii::$app->session->has($sessionKey)) {
                $this->departmentObj = Yii::$app->session->get($sessionKey);
                $this->directCompanyId = $this->departmentObj->direct_company_id;
            } else if ($this->departmentId != 0) {
                $this->departmentObj = Department::findOne(['id' => $this->departmentId]);
                $this->directCompanyId = $this->departmentObj->direct_company_id;
                Yii::$app->session->set($sessionKey, $this->departmentObj);
            }
            //数据权限
            $sessionKey = 'dataAuthId';
            if (Yii::$app->session->has($sessionKey)) {
                $this->dataAuthId = Yii::$app->session->get($sessionKey);
            } else {
                $dataAuthIds = BaseLogic::getTopDataAuth();
                Yii::$app->session->set($sessionKey, $dataAuthIds);
                $this->dataAuthId = $dataAuthIds;
            }
        }

        if (!in_array($ca, $excludeArr)) {
            //根据角色权限跳转
            $roleId = intval($this->user->role_ids);
            switch ($roleId) {
                //未认证
                case 9:
                    header('location:/manufactor/uncertified');
                    die;
                    break;
                //审核未通过
                case 10:
                    header('location:/manufactor/un-audit');
                    die;
                    break;
                //待审核
                case 11:
                    header('location:/manufactor/to-audit');
                    die;
                    break;
            }

            //查出自已权限下的菜单信息
            $selfMenuArr = self::getCacheSelfMenu(Yii::$app->user->getIdentity()->role_ids, $this->departmentId);
            //查出菜单信息
            $menuArr = [];
            if ($selfMenuArr) {
                $menuArr = self::getCacheMenu(isset($selfMenuArr['id']) ? $selfMenuArr['id'] : []);
            }
            Yii::$app->params['menu'] = $menuArr;
            //查询顶级企业信息
            $topId = BaseLogic::getTopId();
            $departmentInfo = Department::getOne(['id' => $topId]);
            $manufactor = Manufactor::getOne(['id' => $departmentInfo->manufactor_id]);
            Yii::$app->params['manufactor'] = $manufactor;

            //获取顶级id,再查出其认证状态
            if (BaseLogic::getTopId()) {
                $topRoleInfo = User::findOne(['department_id' => BaseLogic::getTopId()]);
                Yii::$app->params['role_ids'] = $topRoleInfo->role_ids;
            } else {
                Yii::$app->params['role_ids'] = 9;
            }
            if (!empty($selfMenuArr)) {
                $url = '/' . $this->id . '/' . $this->action->id;
                $linkArr = array_filter($selfMenuArr['url']);
                if (in_array($url, $linkArr)) {
                    $this->selfRoles = $linkArr;
                    return true;
                } else {
                    if (Yii::$app->request->isAjax || 'index_index' == $ca || 'user_modify-password' == $ca) {
                        return true;
                    }
                }
            }
            header('location:/user/no-authority');
            die;
        } else {
            if (!(Yii::$app->user->isGuest)) {
                //查出自已权限下的菜单信息
                $selfMenuArr = self::getCacheSelfMenu(Yii::$app->user->getIdentity()->role_ids, $this->departmentId);
                //查出菜单信息
                $menuArr = [];
                if ($selfMenuArr) {
                    $menuArr = self::getCacheMenu(isset($selfMenuArr['id']) ? $selfMenuArr['id'] : []);
                }
                if (!empty($selfMenuArr)) {
                    $linkArr = array_filter($selfMenuArr['url']);
                    $this->selfRoles = $linkArr;
                }

                Yii::$app->params['menu'] = $menuArr;

                //获取顶级id,再查出其认证状态
                if (BaseLogic::getTopId()) {
                    $topRoleInfo = User::findOne(['department_id' => BaseLogic::getTopId()]);
                    Yii::$app->params['role_ids'] = $topRoleInfo->role_ids;
                } else {
                    Yii::$app->params['role_ids'] = 9;
                }
            }

        }

        return true;
    }

    /**
     * 重写render 加个参数
     * @author liwenyong<liwenyong@c-ntek.com>
     * @see \yii\base\Controller::render()
     * @date 2016-8-17
     */
    public function render($view, $params = [])
    {
        $this->_view['baseUrl'] = urlencode(Yii::$app->request->hostinfo . Yii::$app->request->url);
        $this->_view['selfRoles'] = $this->selfRoles;
        $this->_view = array_merge($this->_view, $params);
        $this->_view['v'] = Yii::$app->params['v'];

        Yii::$app->params['selfRoles'] = $this->selfRoles;

        return parent::render($view, $this->_view);
    }


    /**
     * 缓冲菜单信息
     * @param int $role_ids 角色ids
     * @return array
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date 2017-08-17
     */
    public static function getCacheSelfMenu($role_ids, $src_type)
    {
        $urlArr = Menu::getMenuUrlByRoleIds($role_ids, $src_type);
        return $urlArr;
    }

    /**
     * 查出菜单信息
     * @param int $menu_ids 菜单id
     * @return array
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date 2017-08-17
     */
    public static function getCacheMenu($menu_ids)
    {
        $menuArr = [];
        $cacheKey = 'admin_menu';

        //查询是否开启支付设置
        $paySiginConfig = PaySiginConfig::find()->where(['direct_company_id' => BaseLogic::getDirectCompanyId()])->asArray()->one();
        $payStatus = 1;
        if (empty($paySiginConfig) || $paySiginConfig['pay_status'] == 2) {
            $payStatus = 2;
        }
        //未开启支付设置屏蔽结算导航
        if ($payStatus == 2) {
	        if(array_search('273',$menu_ids) !==false){
	            unset($menu_ids[array_search('273',$menu_ids)]);
            }
        }

        //是否启动收费记录
        $feeEnable = DismantlingConfig::hasEnable((new BaseModel())->directCompanyId, 2);
	    if($feeEnable == false){
	        if(array_search('683',$menu_ids) !==false){
                unset($menu_ids[array_search('683',$menu_ids)]);
            }
        }

        if (Yii::$app->session->has($cacheKey)) {
            $menuArr = Yii::$app->session->get($cacheKey);
        } else {
            $menuArr = Menu::getMenu(0, $menu_ids, 13);
            if ($menuArr) {
                foreach ($menuArr as $key => $val) {
                    if ($val['label'] == '订单') {
                        if (isset($val['items'])) {
                            foreach ($val['items'] as $k1 => $v1) {
                                if ($v1['label'] == '订单管理') {
                                    if (isset($v1['items']) && is_array($v1['items'])) {
                                        $pre = self::pregMatchData($v1['items']);
                                        $menuArr[$key]['items'] = $pre;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Yii::$app->session->set($cacheKey, $menuArr);
        }

        $url = Yii::$app->request->getAbsoluteUrl();
        $activeNav = parse_url($url)['path'];

        foreach ($menuArr as $key => $val) {
            $menuArr[$key]['active'] = 0;
            if (isset($val['items'])) {
                foreach ($val['items'] as $k => $v) {
                    if ($v['url'] == $activeNav) {
                        $menuArr[$key]['active'] = 1;
                        if (strpos($url, $v['url'])) {
                            $menuArr[$key]['items'][$k]['active'] = 1;
                        } else {
                            $menuArr[$key]['items'][$k]['active'] = 0;
                        }
                    } else {
                        $menuArr[$key]['items'][$k]['active'] = 0;
                    }
                }
            }
        }
        return $menuArr;
    }


    /**
     * @desc   获取搜索数据
     * @return array
     * @author chengjuanjuan
     * @date   2018-06-07
     */
    public function departmentforSearch()
    {
        // 用户最高权限
        $dataAuthId = $this->dataAuthId;

        $selfAuth = BaseLogic::getDatAuthId();
        if (BaseLogic::getTopId() == BaseLogic::getDepartmentId() && ($selfAuth == 4 || $selfAuth == 5)) {
            $dataAuthId = $this->dataAuthId = 1;
        }

        if ($dataAuthId == 3) //本公司及以下
        {
            $topId = $this->directCompanyId;
            $info = department::getDepartmentInfo($topId, 1, 0, 1);

            //查询是否还有下级
            $nextDepartment = Department::getDepartmentChildren($topId, 0, 1);
            if (count($nextDepartment) > 1) {
                $info[0]['exist'] = 1; //有下级
            } else {
                $info[0]['exist'] = 2; //无
            }
        } else if ($dataAuthId == 2) //本公司
        {
            $topId = $this->directCompanyId;
            $info = department::getDepartmentInfo($topId, 1, 0, 1);
            $nextDepartment = Department::getDepartmentChildren($topId, 1, 1);
            if (count($nextDepartment) > 1) {
                $info[0]['exist'] = 1; //有下级
            } else {
                $info[0]['exist'] = 2; //无
            }
        } else if ($dataAuthId == 5) //其他权限
        {
            $info = department::getDepartmentInfo($this->departmentId, 1, 0, 1);
            $nextDepartment = Department::getDepartmentChildren($this->departmentId, 2);
            if (count($nextDepartment) > 1) {
                $info[0]['exist'] = 1; //有下级
            } else {
                $info[0]['exist'] = 2; //无
            }
        } else if ($dataAuthId == 4) //本部门
        {
            $info = department::getDepartmentInfo($this->departmentId, 1, 0, 1);
            $info[0]['exist'] = 2; //无
        } else {
            $info = array();
        }

        return $info;
    }

    public static function pregMatchData($array)
    {
        foreach ($array as $k => $v) {
            if ($v['label'] == '新建') {
                $array[$k]['label'] = '创建订单';
            }
            if ($v['label'] == '查看异常订单列表（除上门超时）') {
                $array[$k]['label'] = '异常订单';
            }
            if ($v['label'] == '查看待审核列表') {
                $array[$k]['label'] = '待审核';
            }
            if ($v['label'] == '查看待指派列表') {
                $array[$k]['label'] = '待指派';
            }
            if ($v['label'] == '查看待服务列表') {
                $array[$k]['label'] = '待服务';
            }
            if ($v['label'] == '查看服务中列表') {
                $array[$k]['label'] = '服务中';
            }
            if ($v['label'] == '查看待收款列表') {
                $array[$k]['label'] = '待收款';
            }
            if ($v['label'] == '查看待验收列表') {
                $array[$k]['label'] = '待验收';
            }
            if ($v['label'] == '查看已完成列表') {
                $array[$k]['label'] = '已完成';
            }
            if ($v['label'] == '查看已取消列表') {
                $array[$k]['label'] = '已取消';
            }
            if ($v['label'] == '查看未通过列表') {
                $array[$k]['label'] = '未通过';
            }
            if ($v['label'] == '查看全部订单列表') {
                $array[$k]['label'] = '全部订单';
            }
        }
        return $array;
    }

    /**
     * 删除菜单缓存
     */
    public function clearMenuCache()
    {
        $cacheKey = 'admin_menu';
        Yii::$app->session->remove($cacheKey);
    }
    /**
     * 判断请求是否来自移动端
     */
    function checkmobile() {
        $mobile = array();
        static $mobilebrowser_list =array('iphone', 'android', 'phone', 'mobile', 'wap', 'netfront', 'java', 'opera mobi', 'opera mini',
            'ucweb', 'windows ce', 'symbian', 'series', 'webos', 'sony', 'blackberry', 'dopod', 'nokia', 'samsung',
            'palmsource', 'xda', 'pieplus', 'meizu', 'midp', 'cldc', 'motorola', 'foma', 'docomo', 'up.browser',
            'up.link', 'blazer', 'helio', 'hosin', 'huawei', 'novarra', 'coolpad', 'webos', 'techfaith', 'palmsource',
            'alcatel', 'amoi', 'ktouch', 'nexian', 'ericsson', 'philips', 'sagem', 'wellcom', 'bunjalloo', 'maui', 'smartphone',
            'iemobile', 'spice', 'bird', 'zte-', 'longcos', 'pantech', 'gionee', 'portalmmm', 'jig browser', 'hiptop',
            'benq', 'haier', '^lct', '320x320', '240x320', '176x220');
        $useragent = strtolower($_SERVER['HTTP_USER_AGENT']);
        if( $this->dstrpos($useragent, $mobilebrowser_list, true)) {
            return true;
        }
        return false;
    }

    function dstrpos($string, &$arr, $returnvalue = false) {
        if(empty($string)) return false;
        foreach((array)$arr as $v) {
            if(strpos($string, $v) !== false) {
                $return = $returnvalue ? $v : true;
                return $return;
            }
        }
        return false;
    }
}

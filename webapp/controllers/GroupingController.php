<?php
namespace webapp\controllers;

use common\helpers\Helper;
use common\helpers\Paging;
use common\models\Common;
use webapp\logic\BaseLogic;
use webapp\logic\GroupingLogic;
use webapp\models\BaseModel;
use webapp\models\Department;
use webapp\models\Grouping;
use webapp\models\GroupingRelTechnician;
use Yii;
use webapp\controllers\BaseController;
use yii\web\Response;

class GroupingController extends BaseController
{
    /**
     * 列表
     * @return string
     */
    public function actionIndex()
    {

       //本公司及以下权限赋予本公司权限
        if($this->dataAuthId==3){
            $this->dataAuthId=2;
        }
        //生成查询条件
        $info = self::departmentforSearch();

        $findDepartmentsId = [];
        $user_id           = '';
        $where = ['department_id' => $findDepartmentsId];

        $result = GroupingLogic::getList($where, $user_id);
        $user_info = BaseLogic::getLoginUserInfo();

        $pageHtml = '';
        if($result['success'] == true && $result['data']['totalPage'] >1){
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], '?page=');
        }

        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['department']     = $info;
        $this->_view['auth']           = $this->dataAuthId;
        $this->_view['company_id']     = $this->directCompanyId;
        $this->_view['department_id']  = $user_info->department_id;
        $this->_view['pageHtml']       = $pageHtml;
        $this->_view['page']           = Yii::$app->request->get('page',1);
        $this->_view['pagesize']       = 10;
        $this->_view['params']         = isset($params) ?  $params : '';
        $this->_view['data']           = $result['data'];
        $this->_view['dataAuthId']     = $this->dataAuthId;
        $this->_view['defaultName']    =  $result['data']['defaultName'];;
        $this->_view['department_name']  =  $result['data']['department_name'];
        $this->_view['total_page']       = $result['data']['totalPage'] != 0 ? $result['data']['totalPage'] : 1 ;
        return $this->render('index');
    }

    /**
     * 填加
     * @return string|\yii\web\Response
     */
    public function actionAdd()
    {
        $model = new Grouping();

        if($model->load(Yii::$app->request->post()))
        {
            $model->parent_id           = 0;
            $model->department_top_id   = BaseLogic::getTopId();
            $model->department_id       = BaseLogic::getDepartmentId();
            $model->status              = 1;
            $model->create_user_id      = BaseLogic::getLoginUserId();
            $model->update_user_id      = BaseLogic::getLoginUserId();
            $model->create_time         = time();
            $model->update_time         = time();
            $model->setScenario('add');
            if($model->save())
            {
                if($model->group_leader>0)
                {
                    $relTechModel = new GroupingRelTechnician();
                    $relTechModel->department_top_id = $model->department_top_id;
                    $relTechModel->department_id     = $model->department_id;
                    $relTechModel->grouping_id       = $model->id;
                    $relTechModel->technician_id     = $model->group_leader;
                    $relTechModel->job_title         = 2;
                    $relTechModel->create_time       = time();
                    $relTechModel->update_time       = time();
                    $relTechModel->status            = 1;
                    $relTechModel->save();
                }
                return $this->redirect('index');
            }
        }
        $department_id = BaseLogic::getDepartmentId();
        $this->_view['technicianArr'] = Grouping::getNoGroupTechnician($department_id ? $department_id : $this->manufactorId);
        $this->_view['model']         = $model;
        return $this->render('add');
    }

    /**
     * 编辑
     */
    public function actionUpdate()
    {
        $id = intval(Yii::$app->request->get('id',0));
        if($id<=0){
            return $this->redirect('index');
        }
        $model = Grouping::findOne(['id'=>$id,'department_id'=>BaseLogic::getDepartmentId()]);
        if(!$model){
            return $this->redirect('index');
        }

        $oldGroupLeader = $model->group_leader;

        if($model->load(Yii::$app->request->post()))
        {
            $model->update_user_id = BaseLogic::getLoginUserId();
            $model->update_time = time();
            $model->setScenario('add');

            if($model->save())
            {
                $relTechModel = [];
                if($oldGroupLeader ==0 &&  $model->group_leader>0)
                {
                    $relTechModel = GroupingRelTechnician::findOne(['grouping_id'=>$model->id,'job_title'=>1,'status'=>1,'technician_id'=>$model->group_leader]);
                    if(!$relTechModel){
                        $relTechModel = new GroupingRelTechnician();
                        $relTechModel->grouping_id = $model->id;
                        $relTechModel->technician_id = $model->group_leader;
                        $relTechModel->job_title = 2;
                        $relTechModel->create_time = time();
                        $relTechModel->update_time = time();
                        $relTechModel->status = 1;
                    }
                    else {
                        $relTechModel->job_title = 2;
                        $relTechModel->update_time = time();
                    }
                    $relTechModel->save();

                }
                else if($model->group_leader> 0 && $oldGroupLeader !=$model->group_leader){
                    $relTechModel = GroupingRelTechnician::findOne(['grouping_id'=>$model->id,'job_title'=>2,'technician_id'=>$oldGroupLeader]);
                    $relTechModel->technician_id = $model->group_leader;
                    $relTechModel->update_time = time();
                    $relTechModel->save();
                }
                else if($model->group_leader == 0 && $oldGroupLeader>0){
                    $relTechModel = GroupingRelTechnician::findOne(['grouping_id'=>$model->id,'job_title'=>2,'technician_id'=>$oldGroupLeader]);
                    if($relTechModel){
                        $relTechModel->job_title = 1;
                        $relTechModel->update_time = time();
                        $relTechModel->save();
                    }
                }
                return $this->redirect('index');
            }
        }

        $techIds = [];
        $relTArr = GroupingRelTechnician::find()->where(['grouping_id'=>$id,'job_title'=>1,'status'=>1])->select('technician_id')->asArray()->all();

        if($relTArr){
            $techIds = array_column($relTArr,'technician_id');
        }
        $this->_view['technicianArr'] = Grouping::getNoGroupTechnician($this->manufactorId, array_merge([$model->group_leader],$techIds));
        $this->_view['model'] = $model;
        $this->_view['id'] = $id;
        return $this->render('add');
    }

    /**
     * 删除
     * @return \yii\web\Response
     */
    public function actionDel()
    {
        $result = GroupingLogic::del();

        if($result['success'] == true){
            Yii::$app->session->setFlash('message','删除成功');
        }
        else {
            Yii::$app->session->setFlash('message',$result['message']);
        }
        return $this->redirect('index');
    }

    /**
     * 默认组员
     */
    public function actionDefaultTechnician()
    {
        //当前登录的部门ID
        $department_id =  $this->departmentId;
        $department_type = $this->departmentObj->type;

        //获取当前部门
        $department = Department::getDepartmentInfo($department_id,1,0,1);
        //查询是否还有下级
        $nextDepartment = Department::getDepartmentInfo($department_id,0,1,1);
        if(!empty($nextDepartment)){
            $department[0]['exist'] = 1; //有下级
        }else{
            $department[0]['exist'] = 2; //无
        }

        //根据用户数据权限查询当前用户应该出现的数据
        if($this->dataAuthId == 3)//本公司及以下权限
        {
            $top_id = $department_id;
            //如果当前机构不是公司级别的
            if($department_type != 1) {
                //获取当前机构直属公司ID
                $top_id = $this->directCompanyId;
            }
            //根据当前直属公司ID获取公司下所有子部门（不包含公司）的ID
            $findDepartmentsArr = Department::find()->where(['direct_company_id' => $top_id, 'type' => 3])->asArray()->all();
            $findDepartmentsId = array_column($findDepartmentsArr, 'id');
            array_push($findDepartmentsId,$top_id);
        }
        else if($this->dataAuthId == 2)//本公司的权限
        {
            $top_id = $department_id;
            //如果当前机构不是公司级别的
            if($department_type != 1) {
                //获取当前机构直属公司ID
                $top_id = $this->directCompanyId;
            }
            //根据当前直属公司ID获取公司下所有子部门（不包含公司）的ID
            $findDepartmentsArr = Department::find()->where(['direct_company_id' => $top_id, 'type' => 3])->asArray()->all();
            $findDepartmentsId = array_column($findDepartmentsArr, 'id');
            array_push($findDepartmentsId,$top_id);
        }
        else if(in_array($this->dataAuthId,[1,4,5]))//本部门及以下(如果上级机构为公司，当前用户数据权限为部门时，不展示)
        {
            $findDepartmentsId = [];
            $top_id = $this->directCompanyId;
            //如果当前机构不是部门
            if($department_type == 3) {
                $top_id = $department_id;
                //根据当前直属公司ID获取公司下所有子部门（不包含公司）的ID
                $findDepartmentsArr = Department::find()->where(['direct_company_id' => $top_id, 'type' => 3])->asArray()->all();
                $findDepartmentsId = array_column($findDepartmentsArr, 'id');
                array_push($findDepartmentsId,$top_id);
            }
        }

        $where['a.store_id']  = $findDepartmentsId;


        if(Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $_GET['pagesize'] = 1000;

            $where['a.store_id']  = $this->departmentId;
            $result = GroupingLogic::getDefaultTechnician($where);
            return $result['data'];
        }

        $result = GroupingLogic::getDefaultTechnician($where);

        $pageHtml = '';
        if($result['success'] == true && $result['data']['totalPage'] >1){
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], "?page=");
        }
        $this->_view['grouping']   = $grouping = Grouping::getGroupingAttr($findDepartmentsId);
        $this->_view['data']       = $result['data'];
        $this->_view['pageHtml']   = $pageHtml;
        $this->_view['dataAuthId'] = $this->dataAuthId;
        return $this->render('default-technician');

    }

    /**
     * 把技师加入分组
     */
    public function actionAjaxJoinGrouping()
    {
        if(Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return GroupingLogic::joinGrouping();
        }
    }

    /**
     * 批量添加技师
     * @return array
     */
    public function actionAjaxBatchJoinGrouping()
    {
        if(Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return GroupingLogic::batchJoinGrouping();
        }
    }

    /**
     * 查看组员
     * @return string
     */
    public function actionTechnician()
    {
        $groupingId = intval(Yii::$app->request->get('grouping_id',0));
        $result = GroupingLogic::getTechnicianList();
        $pageHtml = '';
        if($result['success'] == true && $result['data']['totalPage'] >1){
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], "?page=");
        }

        $groupingArr = Grouping::findOneByAttributes(['id'=>$groupingId],'name');

        $this->_view['grouping'] = [];
        $this->_view['data']     = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['groupingName'] = isset($groupingArr['name'])?$groupingArr['name']:'';
        $this->_view['groupingId']   = $groupingId;
        return $this->render('technician');
    }

    /**
     * 移除
     * @return array
     */
    public function actionRemove()
    {
        if(Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return GroupingLogic::remove();
        }
    }

    /**
     * 修改分组
     */
    public function actionChange()
    {
        if(Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return GroupingLogic::change();
        }
    }

    /**
     * 修改职位
     */
    public function actionChangeTitle()
    {
        if(Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return GroupingLogic::changeTitle();
        }
    }

    /**
     * 验证选中组长是否属于别的组
     */
    public function actionCheckLeader() {
        if(Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return GroupingLogic::checkLeader();
        }
    }

    /**
     * 获取机构下的分组
     * @author li
     * @param  int $id
     * @date   2018-06-15
     */
    public function actionGetDepartmentGroup() {
        $department_id = intval(Yii::$app->request->post('id',0));
        if($department_id) {
            $str = '<option>请选择</option>';
            $grouping = Grouping::getGroupingAttr($department_id);
            foreach ($grouping as $k => $v) {
                $str.= "<option value='".$v['id']."'>".$v['name']."</option>";
            }
            return $str;
        }
        return false;
    }
}

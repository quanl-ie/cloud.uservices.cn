<?php
    /**
     * Created by PhpStorm.
     * User: xingq
     * Date: 2018/3/23
     * Time: 10:09
     */
    
    namespace webapp\controllers;
    
    use webapp\logic\PickUserLogic;
    use Yii;
    use webapp\logic\StockInfoLogic;
    use common\helpers\Paging;
    use webapp\models\Department;
    
    class StockOutController extends BaseController
    {
    
        /**
         * 待审批出库列表
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/2
         * Time: 13:02
         * @return string
         */
        public function actionWaitCheck()
        {
        
            $status = Yii::$app->request->get('status',1);
        
            $res = StockInfoLogic::index($type = 2,$status);
        
            $pageHtml = '';
            if($res['totalPage'] >1){
                $pageHtml = Paging::make($res['page'], $res['totalPage'], '?page=');
            }
        
            $this->_view['data']          = $res['list'];
            $this->_view['pageHtml']      = $pageHtml;
            $this->_view['stockTypeList'] = isset($res['stockTypeList']) ? $res['stockTypeList'] : '';
        
            return $this->render('wait-check');
        
        }
    
        /**
         * 已出库列表
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/2
         * Time: 13:17
         * @return string
         */
        public function actionFinished()
        {
            //系统是否开启发货功能状态
            $ship_sys = '1'; //1 已开启 0 未开启
            $isshipList = ['1'=>'未发货','2'=>'已发货'];
            $status = Yii::$app->request->get('status',2);

            $res = StockInfoLogic::index($type = 2,$status);
            $pageHtml = '';
            if($res['totalPage'] >1){
                $pageHtml = Paging::make($res['page'], $res['totalPage'], '?page=');
            }
        
            $this->_view['data']          = $res['list'];
            $this->_view['pageHtml']      = $pageHtml;
            $this->_view['stockTypeList'] = isset($res['stockTypeList']) ? $res['stockTypeList'] : '';
            $this->_view['ship_sys']      = $ship_sys;  //系统是否开启发货功能状态
            $this->_view['isshipList']    = $isshipList;  //发货状态列表
    
            return $this->render('finished');

        }
    
        /**
         * 未通过列表
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/2
         * Time: 13:17
         * @return string
         */
        public function actionUnchecked()
        {
            
            $status = Yii::$app->request->get('status',3);
    
            $res = StockInfoLogic::index($type = 2,$status);
        
            $pageHtml = '';
            if($res['totalPage'] >1){
                $pageHtml = Paging::make($res['page'], $res['totalPage'], '?page=');
            }
        
            $this->_view['data']          = $res['list'];
            $this->_view['pageHtml']      = $pageHtml;
            $this->_view['stockTypeList'] = isset($res['stockTypeList']) ? $res['stockTypeList'] : '';
    
            return $this->render('unchecked');
        }
    
        /**
         * 添加
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/2
         * Time: 13:17
         * @return string|\yii\web\Response
         * @throws \Exception
         */
        public function actionAdd()
        {
            $data = StockInfoLogic::beforeList(2);
            if ($data['code'] == 20003) {
                echo "<script language=javascript>alert('剩余产品不足');history.back();</script>";exit;
            }
            $dataAuthId          = $this->dataAuthId;
            if($dataAuthId == 3){  //本公司及以下
                $top_id = $this->directCompanyId;
                $info = department::getDepartmentInfo($top_id,1,0,1);
                //查询是否还有下级
                $nextDepartment = Department::getDepartmentChildren($top_id,0,1);
                if(count($nextDepartment) > 1){
                    $info[0]['exist'] = 1; //有下级
                }else{
                    $info[0]['exist'] = 2; //无
                }
            }else if($dataAuthId == 2){  //本公司
                $top_id = $this->directCompanyId;
                $info = department::getDepartmentInfo($top_id,1,0,1);
                $nextDepartment = Department::getDepartmentChildren($top_id,1,1);
                if(count($nextDepartment) > 1){
                    $info[0]['exist'] = 1; //有下级
                }else{
                    $info[0]['exist'] = 2; //无
                }
            }else if($dataAuthId == 5){   //部门以下
                if($this->departmentObj->type == 3){
                    $info = department::getDepartmentInfo($this->departmentId,1,0,1);
                    $nextDepartment = Department::getDepartmentChildren($this->departmentId,2);
                    if(count($nextDepartment) > 1){
                        $info[0]['exist'] = 1; //有下级
                    }else{
                        $info[0]['exist'] = 2; //无
                    }
                }else{
                    $info = department::getDepartmentInfo($this->departmentId,1,1,1);
                    $nextDepartment = Department::getDepartmentChildren($this->departmentId,2);
                    if(count($nextDepartment) > 1){
                        $info[0]['exist'] = 1; //有下级
                    }else{
                        $info[0]['exist'] = 2; //无
                    }
                }

            }else if($dataAuthId == 4){   //本部门
                if($this->departmentObj->type == 3){
                    $info = department::getDepartmentInfo($this->departmentId,1,0,1);
                    $info[0]['exist'] = 2; //无
                }else{
                    $info = department::getDepartmentInfo($this->departmentId,1,1,1);
                    $info[0]['exist'] = 2;
                }
            }else{  //本人
                $info = department::getDepartmentInfo($this->departmentId,1,1,1);
                $info[0]['exist'] = 2;
            }
            //提货人列表
            $pickList = PickUserLogic::getList();
            //添加 部门默认值为本部门
            $define_department = department::getDepartmentInfo($this->departmentId,1,1,1);
            $define_department = $define_department[0];
            if ($data['code'] == 200) {
                $this->_view['data']      = $data['data']['model'];
                $this->_view['product']   = isset($data['data']['product']) ? $data['data']['product'] : '';
                $this->_view['stockType'] = $data['data']['stock_type'];
                $this->_view['depot']     = $data['data']['depot'];
                $this->_view['department']= $info; //权限内的所有机构
                $this->_view['define_department']= $define_department;
                $this->_view['auth']= $dataAuthId;
                $this->_view['pickList']= $pickList;  //提货人列表
            }else{
                Yii::$app->session->setFlash('message',$data['message']);
            }
            
            if (Yii::$app->request->isPost) {
                //出库
                $res = StockInfoLogic::add(2);
                Yii::$app->session->setFlash('message',$res['message']);
                if ($res['code'] == 200) {
                    return $this->redirect('/stock-out/wait-check');
                }
            }
    
            return $this->render('add');
            
        }
    
        /**
         * 查看入库单详情
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/26
         * Time: 10:13
         */
        public function actionView()
        {
            //系统是否开启发货功能状态
            $ship_sys = '1'; //1 已开启 0 未开启
            $res = StockInfoLogic::view();
            if ($res['code'] == 200) {
                $this->_view['stockInfo']   = $res['data']['stockInfo'];
                $this->_view['stockDetail'] = $res['data']['stockDetail'];
                $this->_view['ship_sys']    = $ship_sys;
                $this->_view['shipInfo']    = isset($res['data']['shipInfo'])?$res['data']['shipInfo']:[];
            }else{
                Yii::$app->session->setFlash('message',$res['message']);
            }
            
            return $this->render('view');

        }
    
        /**
         * 审批页面
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/10
         * Time: 18:00
         * @return string
         * @throws \Exception
         */
        public function actionExamine()
        {
            
            $res = StockInfoLogic::view();
    
            if (Yii::$app->request->isPost) {
                $res = StockInfoLogic::ajaxExamine(2);

                return json_encode($res);
            }
            
            if ($res['code'] == 200) {
                $this->_view['depot']       = $res['data']['depot'];
                $this->_view['stockInfo']   = $res['data']['stockInfo'];
                $this->_view['stockDetail'] = $res['data']['stockDetail'];
            }else{
                Yii::$app->session->setFlash('message',$res['message']);
            }
            
            return $this->render('examine');
        }
    
        /**
         * 获取库存数量
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/19
         * Time: 10:56
         * @return string
         */
        public function actionGetDepotNum()
        {
            $res = StockInfoLogic::getDepotNum();
            return json_encode($res);
        }
    
        /**
         * 修改库房与序列号
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/24
         * Time: 17:22
         * @return string
         */
        public function actionEdit()
        {
            $res = StockInfoLogic::edit(2);
            return json_encode($res);
        }
    }
<?php

namespace webapp\controllers;

use Yii;
use webapp\logic\ScheduleSetOptionLogic;
use webapp\models\Department;

/**
 * 批量设置技师排班
 * Class ScheduleSetOptionController
 * @package webapp\controllers
 */

class ScheduleSetOptionController extends BaseController
{
    /**
     * 页面
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/22
     * Time: 14:35
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionIndex()
    {

        $sreachIds = $this->actionGetSreachId();
        $findDepartmentsId = $sreachIds['findDepartmentsId'];
        $result = ScheduleSetOptionLogic::index($findDepartmentsId);

        if (Yii::$app->request->isPost) {
            $result = ScheduleSetOptionLogic::add();
            Yii::$app->session->setFlash('message',$result['message']);
            return $this->redirect('/schedule-manage/index');
        }
        $this->_view['group']       = $result['group'];
        $this->_view['tech']        = $result['tech'];
        $this->_view['repeateType'] = $result['repeateType'];
        $this->_view['scheduleSet'] = $result['scheduleSet'];
        
        return $this->render('index');
    }
    
    /**
     * 动态获取技师
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/22
     * Time: 14:35
     * @return string
     */
    public function actionGetTech()
    {
        $sreachIds = $this->actionGetSreachId();
        $findDepartmentsId = $sreachIds['findDepartmentsId'];
        $result = ScheduleSetOptionLogic::getTech('',$findDepartmentsId);
        return json_encode($result);
    }
    /**
     * 函数用途描述:获取班次信息
     * @date: 2018年5月16日
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionGetTechSchedule()
    {
        
        $result = ScheduleSetOptionLogic::GetTechSchedule();
        return $this->renderPartial('set-schedule',['data'=>$result]);
    }
    /**
     * 函数用途描述:单独设置技师班次或修改
     * @date: 2018年5月16日 上午10:22:10
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionSetTechSchedule()
    {
        $result = ScheduleSetOptionLogic::SetTechSchedule();
        return json_encode($result);
    }

    /**
     * 根据用户权限查询需要出现的数据的所属机构ID
     */
    public function actionGetSreachId() {
        //当前登录的部门ID
        $department_id =  $this->departmentId;
        $department_type = $this->departmentObj->type;

        //获取当前部门
        $department = Department::getDepartmentInfo($department_id,1,0,1);
        //查询是否还有下级
        $nextDepartment = Department::getDepartmentInfo($department_id,0,1,1);
        if(!empty($nextDepartment)){
            $department[0]['exist'] = 1; //有下级
        }else{
            $department[0]['exist'] = 2; //无
        }
        //根据用户数据权限查询当前用户应该出现的数据
        if($this->dataAuthId == 3)//本公司及以下权限
        {
            $top_id = $department_id;
            //如果当前机构不是公司级别的
            if($department_type != 1) {
                //获取当前机构直属公司ID
                $top_id = $this->directCompanyId;
            }
            //根据当前直属公司ID获取公司下所有子部门（不包含公司）的ID
            $findDepartmentsArr = Department::find()->where(['direct_company_id' => $top_id, 'type' => 3])->asArray()->all();
            $findDepartmentsId = array_column($findDepartmentsArr, 'id');
            array_push($findDepartmentsId,$top_id);
        } else if($this->dataAuthId == 2)//本公司的权限
        {
            $top_id = $department_id;
            //如果当前机构不是公司级别的
            if($department_type != 1) {
                //获取当前机构直属公司ID
                $top_id = $this->directCompanyId;
            }
            //根据当前直属公司ID获取公司下所有子部门（不包含公司）的ID
            $findDepartmentsArr = Department::find()->where(['direct_company_id' => $top_id, 'type' => 3])->asArray()->all();
            $findDepartmentsId = array_column($findDepartmentsArr, 'id');
            array_push($findDepartmentsId,$top_id);
        } else if($this->dataAuthId == 5)//本部门及以下(如果上级机构为公司，当前用户数据权限为部门时，不展示)
        {
            $findDepartmentsId = [];
            $top_id = $this->directCompanyId;
            //如果当前机构不是部门
            if($department_type == 3) {
                $top_id = $department_id;
                //根据当前直属公司ID获取公司下所有子部门（不包含公司）的ID
                $findDepartmentsArr = Department::find()->where(['direct_company_id' => $top_id, 'type' => 3])->asArray()->all();
                $findDepartmentsId = array_column($findDepartmentsArr, 'id');
                array_push($findDepartmentsId,$top_id);
            }
        } else if($this->dataAuthId == 4)//本部门权限(如果上级机构为公司，当前用户数据权限为部门时，不展示)
        {
            $top_id = $findDepartmentsId[] = $department_id;
            if($department_type != 3) {
                $top_id = $findDepartmentsId = [];
            }
        } else {//如果数据权限为本人展示为空
            $findDepartmentsId = [];
            $top_id = 0;
        }
        return ['findDepartmentsId' => $findDepartmentsId, 'top_id' => $top_id];
    }
}


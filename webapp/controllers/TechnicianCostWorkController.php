<?php

namespace webapp\controllers;

use webapp\models\CooporationServiceProvider;
use Yii;
use webapp\logic\TechnicianCostWorkLogic;
use webapp\controllers\BaseController;
use common\helpers\Paging;


/**
 * @desc:技师结算单管理
 * @author Eva <chengjuanjuan@c-ntek.com>
 * @date 2018-2-2
 *
 */

class TechnicianCostWorkController extends BaseController
{
    /**
     * 列表
     * @author Eva <chengjuanjuan@c-ntek.com>
     */
    public function actionHaveCosted()
    {
        $res = TechnicianCostWorkLogic::getList();
        if($res['totalPage'] >=1){
            $technicianName = trim(Yii::$app->request->get('technician_name'));
            $pageHtml = Paging::make($res['page'],  $res['totalPage'], '?technician_name='.$technicianName.'&page=');
        }else{
            $pageHtml = '';
        }

        $this->_view['data'] = isset($res['data'])?$res['data']:$res['list'];
        $this->_view['pageHtml'] = $pageHtml;
        return $this->render('have-costed');

    }

    public function actionWaitCost()
    {
        $res = TechnicianCostWorkLogic::getList();
        if($res['totalPage'] >=1){
            $technicianName = trim(Yii::$app->request->get('technician_name'));
            $pageHtml = Paging::make($res['page'],  $res['totalPage'], '?technician_name='.$technicianName.'&page=');
        }else{
            $pageHtml = '';
        }

        $this->_view['data'] = isset($res['data'])?$res['data']:$res['list'];
        $this->_view['pageHtml'] = $pageHtml;
        return $this->render('wait-cost');
    }





}
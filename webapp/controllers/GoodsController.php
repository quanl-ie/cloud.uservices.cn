<?php

namespace webapp\controllers;

use Yii;
use webapp\models\GoodsManagement;
use webapp\models\GoodsSearch;
use common\models\ServiceClass;
use webapp\models\BrandQualification;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use webapp\controllers\BaseController;


class GoodsController extends BaseController
{

    public function actionIndex()
    {

        return $this->render('index');
    }

    public function actionAdd()
    {

        return $this->render('add');
    }

    public function actionUpdate()
    {

        return $this->render('update');
    }

    public function actionView()
    {

        return $this->render('view');
    }
}

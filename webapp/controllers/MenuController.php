<?php
namespace webapp\controllers;

use webapp\models\BaseModel;
use Yii;
use webapp\controllers\BaseController;
use webapp\models\MenuForm;
use webapp\models\Menu;

/**
 * Site controller
 * 菜单管理
 * @author liwenyong<liwenyong@c-ntek.com>
 * @date   2017-08-17
 */
class MenuController extends BaseController
{
    /**
     * 菜单列表
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return object
     */
    public function actionIndex()
    {
        $menuArr = MenuForm::getMenuList(0,BaseModel::SRC_FWS);
        return $this->render('index',['module' => $menuArr]);
    }
    
    /**
     * Ajax获取二级菜单
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return string
     */
    public function actionAjaxMenu() {
        $parentId = Yii::$app->request->get('parent_id',0);
        $menu = MenuForm::getMenuList($parentId,BaseModel::SRC_FWS);
        $menuStr = '';
        if(!empty($menu))
        {
            foreach ($menu as $val) {
                $menuStr.= "<span onclick='showMenu(".$val['id'].")'>".$val['name']."</span>";
                $menuStr.= "<a href='/menu/edit?id=".$val['id']."' style='margin-right:10px; margin-left:15px;'>编辑</a>";
                $menuStr.= "<a href='/menu/menu-del?id=".$val['id']."'>删除</a>";
                $menuStr.= "<div class='menu_".$val['id']." meunArr' style='margin-left:80px;'></div>";
            }
        }
        return $menuStr;
    }

    /**
     * 菜单添加
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return 
     */
    public function actionAdd()
    {
        $menu = new MenuForm();
        if ($menu->load(Yii::$app->request->post())) {
            $errors = $menu->getErrors();
            if(empty($errors))
            {
                $model = new Menu();
                $model->src_type  = BaseModel::SRC_FWS;
                $model->name      = $menu->name;
                $model->url       = $menu->url;
                $model->weight    = $menu->weight;
                $model->parent_id = $menu->parent_id;
                $model->is_show   = $menu->is_show;
                $model->class     = $menu->class;
                if ($model->save(false)) {
                    Yii::$app->session->setFlash('message','成功');
                    return $this->redirect('/menu/index');
                }
            }
        }
        $menu->is_show = 1;
        $firstMenu = Menu::find()
                ->where(['parent_id' => 0, 'is_show' => 1,'src_type'=>BaseModel::SRC_FWS])
                ->asArray()
                ->all();
        $menu->parent_id = 0;
        return $this->render('add', [
            'model' => $menu,
            'menu' => $firstMenu,
            'dropdownList' => Menu::getMenuArr(0)
        ]);
    }
    
    /**
     * 编辑菜单
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return s
     */
    public function actionEdit() {
        $id = Yii::$app->request->get('id',0);
        $menu = new MenuForm();
        if($menu->load(Yii::$app->request->post()))
        {
            $errors = $menu->getErrors();
            if(empty($errors))
            {
                $model = Menu::findOne(['id' => $id,'src_type'=>BaseModel::SRC_FWS]);
                $model->name      = $menu->name;
                $model->url       = $menu->url;
                $model->weight    = $menu->weight;
                $model->parent_id = $menu->parent_id;
                $model->is_show   = $menu->is_show;
                $model->class     = $menu->class;
                if ($model->save(false)) {
                    Yii::$app->session->setFlash('message','成功');
                    return $this->redirect('/menu/index');
                }
            }
        }else
        {
            $menu = Menu::findOne(['id' => $id,'src_type'=>BaseModel::SRC_FWS]);
            $model = new MenuForm();
            $model->name      = $menu->name;
            $model->url       = $menu->url;
            $model->weight    = $menu->weight;
            $model->parent_id = $menu->parent_id;
            $model->is_show   = $menu->is_show;
            $model->class     = $menu->class;
        }
        return $this->render('edit',['model' => $model]);
    }
    
    /**
     * 删除菜单
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return string
     */
    public function actionMenuDel() {
        $id = Yii::$app->request->get('id',0);
        $menu = Menu::findOne(['id' => $id,'src_type'=>BaseModel::SRC_FWS]);
        if(!empty($menu))
        {
            $menu->status = 0;
            $menu->save(false);
        }
        Yii::$app->session->setFlash('message','成功');
        return $this->redirect('/menu/index');
    }
}

<?php
namespace webapp\controllers;
use Yii;

class PurchaseReviewController extends BaseController
{
    /**
     * 收货列表
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    /**
     * 收货添加
     * @return string
     */
    public function actionAdd()
    {
        return $this->render('add');
    }
    /**
     * 收货单详情
     * @return string
     */
    public function actionView()
    {
        return $this->render('view');
    }
}
<?php

namespace webapp\controllers;

use common\models\ReturnGoods;
use webapp\logic\ServicesClassLogic;
use webapp\models\BrandQualification;
use Yii;

use yii\web\Controller;
use common\helpers\Paging;
use webapp\models\Cooporation;
use common\models\ServiceClass;
use webapp\models\SaleOrder;
use webapp\logic\ReturnGoodsLogic;
use webapp\logic\ContractLogic;
use webapp\logic\BaseLogic;
/**
 *@desc:退货管理
 * @author: chengjuanjuan <chengjuanjuan@c-ntek.com>
 * createdAt: 2018/03/21 10:47
 *
 */


class ReturnGoodsController extends BaseController
{


    /**
     * @desc 待审批列表
     **/
    public function actionWaitCheck(){
        $result = ReturnGoodsLogic::getList();
        if($result['totalPage'] >=1){
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();

        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           =  $params;
        $this->_view['department_id']    = BaseLogic::getDepartmentId();
        $this->_view['department']       = $info;
        $this->_view['auth']             =  $this->dataAuthId;
        $this->_view['company_id']       = $this->directCompanyId;
        $this->_view['data']             = $result['list'];
        $this->_view['pageHtml']         = isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  =  $result['department_name'];
        return $this->render('wait-check');
    }

    /**
     * @desc 执行中列表
     **/
    public function actionExecuting(){
        $result = ReturnGoodsLogic::getList();
        if($result['totalPage'] >=1){
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();
        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           =  $params;
        $this->_view['department_id']    = BaseLogic::getDepartmentId();
        $this->_view['department']       = $info;
        $this->_view['auth']             =  $this->dataAuthId;
        $this->_view['company_id']       = $this->directCompanyId;
        $this->_view['data']             = $result['list'];
        $this->_view['pageHtml']         = isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  =  $result['department_name'];
        return $this->render('executing');

    }


    /**
     * @desc 执行完毕列表
     **/
    public function actionExecuteFinished(){
        $result = ReturnGoodsLogic::getList();
        if($result['totalPage'] >=1){
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();

        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           =  $params;
        $this->_view['department_id']    = BaseLogic::getDepartmentId();
        $this->_view['department']       = $info;
        $this->_view['auth']             =  $this->dataAuthId;
        $this->_view['company_id']       = $this->directCompanyId;
        $this->_view['data']             = $result['list'];
        $this->_view['pageHtml']         = isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  =  $result['department_name'];
        return $this->render('execute-finished');

    }


    //审批未通过列表
    public function actionUnchecked(){
        $result = ReturnGoodsLogic::getList();
        if($result['totalPage'] >=1){
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();

        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           =  $params;
        $this->_view['department_id']    = BaseLogic::getDepartmentId();
        $this->_view['department']       = $info;
        $this->_view['auth']             =  $this->dataAuthId;
        $this->_view['company_id']       = $this->directCompanyId;
        $this->_view['data']             = $result['list'];
        $this->_view['pageHtml']         = isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  =  $result['department_name'];
        return $this->render('unchecked');
    }

    //终止列表
    public function actionTerminated(){
        $result = ReturnGoodsLogic::getList();
        if($result['totalPage'] >=1){
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();

        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           =  $params;
        $this->_view['department_id']    = BaseLogic::getDepartmentId();
        $this->_view['department']       = $info;
        $this->_view['auth']             =  $this->dataAuthId;
        $this->_view['company_id']       = $this->directCompanyId;
        $this->_view['data']             = $result['list'];
        $this->_view['pageHtml']         = isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  =  $result['department_name'];
        return $this->render('terminated');

    }


    // 添加
    public function actionAdd()
    {
        //退货编号
        $contract = ContractLogic::getDetail();

        if(!empty($contract)){
            $contractInfo = $contract['data']['info'];
            $contractDetail = $contract['data']['detail'];
         /*   foreach($contractDetail as &$v){
                $v['total_num'] = $v['prod_num']-$v['total_num'];
            }*/
        }else{
            Yii::$app->session->setFlash('message',$contract['message']);
        }
        $contractInfo['reason']  = ReturnGoods::showReasonStatus();
        if (yii::$app->request->isPost) {
            
            $res = ReturnGoodsLogic::add();
            if($res['code'] == 200) {
                return $this->redirect('/return-goods/wait-check');
            }
            else {
                Yii::$app->session->setFlash('message',$res['message']);
            }
        }
        foreach($contractDetail as $k=>&$v){
            if($v['total_num']==0){
                unset($contractDetail[$k]);
            }else{
                $v['total_num'] = $v['total_num']-$v['finish_num'];
            }
            $v['total_amount'] = sprintf('%0.2f',$v['sale_price']*$v['total_num']);
            $v['amount']  = $v['total_amount'];
        }
        return $this->render('add',['contract'=>$contractInfo,'contractDetail'=>$contractDetail]);
    }



    public function actionEdit(){
        if (yii::$app->request->isPost) {
            $res = ReturnGoodsLogic::edit();
            if($res['code'] != 200) {
                Yii::$app->session->setFlash('message',$res['message']);
            }else{
                return $this->redirect('/return-goods/wait-check');
            }
        }
        $res = ReturnGoodsLogic::getDetail();
        $list = $res['data'];
        if($res['code'] != 200) {
            Yii::$app->session->setFlash('message',$res['message']);
        }
        $list['info']['reason']  = ReturnGoods::showReasonStatus();
        $info = $list['info'];
        $detail = $list['detail'];
        foreach($detail as $v){
            $existNum[$v['prod_id']] = $v;

        }
        $haveProdIds  = array_column($detail,'prod_id');
        $contract = ContractLogic::getDetail($list['info']['contract_id']);
        $contractDetail = $contract['data']['detail'];

        foreach($contractDetail as &$v){
            if(in_array($v['prod_id'],$haveProdIds)){
                  $v['check_status'] = 1;
                  $num = $v['prod_num']-$existNum[$v['prod_id']]['prod_num'];
                  $v['prod_num'] = $existNum[$v['prod_id']]['prod_num'];
                  $v['last_num'] = $num<=0?0:$num;;//合同数量减掉退货单数量
                  $v['amount']   = sprintf('%0.2f',$v['sale_price']*$v['prod_num']);
                  $v['total_amount'] = sprintf('%0.2f',$v['amount']*$v['rate']/100);
                  $v['warranty_date'] = $existNum[$v['prod_id']]['warranty_date'];
            }else{
                $v['check_status'] = 0;
                $v['last_num'] = $v['prod_num'];
            }

        }
        return $this->render('edit',['info'=>$info,'contractDetail'=>$contractDetail]);

    }



    //详情
    function actionDetail(){
        $id = intval(Yii::$app->request->get('id'));
        $audit_status = ['1'=>'待审批','2'=>'已入库','3'=>'未通过'];
        $res = ReturnGoodsLogic::getDetail();
        return $this->render('detail',[
            'info'          =>$res['data']['info'],
            'detail'        =>$res['data']['detail'],
            'stockInList'   =>$res['data']['stockInList'],
            'statuslist'    =>$audit_status
            ]);

    }


    //审核
    function actionCheckOption(){

        $id = intval(Yii::$app->request->get('id'));
        if(Yii::$app->request->post()){
            $res = ReturnGoodsLogic::checkOption();
            return json_encode($res);
        }
        $res = ReturnGoodsLogic::getDetail();
        return $this->render('detail',[
            'info'          =>$res['data']['info'],
            'detail'        =>$res['data']['detail'],
            'stockInList'   =>$res['data']['stockInList']
            ]);
    }

    //更新状态
    function actionUpdateStatus(){
        $id = intval(Yii::$app->request->post('id'));
        $type = trim(Yii::$app->request->post('type'));
        $info = ReturnGoodsLogic::getDetail($id);
        if($info['success']){
            $res = ReturnGoodsLogic::updateStatus($type);
            return json_encode($res);
        }
    }

}


<?php

namespace webapp\controllers;

use webapp\models\Cooporation;
use Yii;
use webapp\logic\CostLogic;
use webapp\logic\CostOrderLogic;
use webapp\controllers\BaseController;
use common\helpers\Paging;


/**
 * @desc:收费列表管理
 * @author Eva <chengjuanjuan@c-ntek.com>
 * @date 2018-2-2
 *
 */

class CostOrderController extends BaseController
{
    /**
     * 列表
     * @author Eva <chengjuanjuan@c-ntek.com>
     */
    public function actionHaveCosted()
    {
        $res = CostOrderLogic::getList();
        $url = Yii::$app->params['order.uservices.cn'].'/v1/work-pc/search';
        //$res = CostLogic::getPostList($url);
        if($res['totalPage'] >=1){
            $pageHtml = Paging::make($res['page'],  $res['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }
        $search = CostOrderLogic::getData();

        $this->_view['data'] = isset($res['data'])?$res['data']:$res['list'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['search'] = $search;
        return $this->render('have-costed');

    }

    public function actionWaitCost()
    {
        $res = CostOrderLogic::getList();
        if($res['totalPage'] >=1){
            $pageHtml = Paging::make($res['page'],  $res['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }

        $search = CostOrderLogic::getData();

        $this->_view['data'] = isset($res['data'])?$res['data']:$res['list'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['search'] = $search;
        return $this->render('wait-cost');
    }





}
<?php

namespace webapp\controllers;

use webapp\logic\ServicesClassLogic;
use webapp\models\BrandQualification;
use Yii;

use webapp\logic\BaseLogic;
use webapp\logic\ExchangeGoodsLogic;
use yii\web\Controller;
use common\helpers\Paging;
use webapp\models\Cooporation;
use common\models\ServiceClass;
use webapp\models\SaleOrder;
use webapp\logic\ContractLogic;
use common\models\ExchangeGoods;
/**
 *@desc:换货管理
 * @author: chengjuanjuan <chengjuanjuan@c-ntek.com>
 * createdAt: 2018/03/21 10:47
 *
 */


class ExchangeGoodsController extends BaseController
{


    /**
     * @desc 待审批列表
     **/
    public function actionWaitCheck(){
        $result = ExchangeGoodsLogic::getList();
        if($result['totalPage'] >=1){
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();

        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           =  $params;
        $this->_view['department_id']   = BaseLogic::getDepartmentId();
        $this->_view['department']       = $info;
        $this->_view['auth']             =  $this->dataAuthId;
        $this->_view['company_id']       = $this->directCompanyId;
        $this->_view['data']             = $result['list'];
        $this->_view['pageHtml']         = isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  =  $result['department_name'];
        return $this->render('wait-check');
    }

    /**
     * @desc 执行中列表
     **/
    public function actionExecuting(){
        //系统是否开启发货功能状态
        $shipSys = '1'; //1 已开启 0 未开启
        $result = ExchangeGoodsLogic::getList($shipSys);
        if($result['totalPage'] >=1){
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();

        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           =  $params;
        $this->_view['department_id']    = BaseLogic::getDepartmentId();
        $this->_view['department']       = $info;
        $this->_view['auth']             =  $this->dataAuthId;
        $this->_view['company_id']       = $this->directCompanyId;
        $this->_view['data']             = $result['list'];
        $this->_view['pageHtml']         = isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  =  $result['department_name'];
        $this->_view['ship_sys']         = $shipSys;
        return $this->render('executing');

    }


    /**
     * @desc 执行完毕列表
     **/
    public function actionExecuteFinished(){
        $result = ExchangeGoodsLogic::getList();
        if($result['totalPage'] >=1){
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();

        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           =  $params;
        $this->_view['department_id']    = BaseLogic::getDepartmentId();
        $this->_view['department']       = $info;
        $this->_view['auth']             =  $this->dataAuthId;
        $this->_view['company_id']       = $this->directCompanyId;
        $this->_view['data']             = $result['list'];
        $this->_view['pageHtml']         = isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  =  $result['department_name'];
        return $this->render('execute-finished');

    }


    //审批未通过列表
    public function actionUnchecked(){
        $result = ExchangeGoodsLogic::getList();
        if($result['totalPage'] >=1){
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();

        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           =  $params;
        $this->_view['department_id']    = BaseLogic::getDepartmentId();
        $this->_view['department']       = $info;
        $this->_view['auth']             =  $this->dataAuthId;
        $this->_view['company_id']       = $this->directCompanyId;
        $this->_view['data']             = $result['list'];
        $this->_view['pageHtml']         = isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  =  $result['department_name'];
        return $this->render('unchecked');
    }

    //终止列表
    public function actionTerminated(){
        $result = ExchangeGoodsLogic::getList();
        if($result['totalPage'] >=1){
            $queryStr = $_SERVER['QUERY_STRING'];
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?'.$queryStr.'&page=');
        }
        $info = self::departmentforSearch();

        $params['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        $this->_view['params']           =  $params;
        $this->_view['department_id']    = BaseLogic::getDepartmentId();
        $this->_view['department']       = $info;
        $this->_view['auth']             =  $this->dataAuthId;
        $this->_view['company_id']       = $this->directCompanyId;
        $this->_view['data']             = $result['list'];
        $this->_view['pageHtml']         = isset($pageHtml)?$pageHtml:' ';
        $this->_view['department_name']  =  $result['department_name'];
        return $this->render('terminated');

    }




    public function actionAdd()
    {

        $contract = ContractLogic::getDetail();
        if(!empty($contract)){
            $contractInfo = $contract['data']['info'];
            $contractDetail = $contract['data']['detail'];
        }else{
            Yii::$app->session->setFlash('message',$contract['message']);
        }
        $contractInfo['reason']  = ExchangeGoods::showReasonStatus();
        if (yii::$app->request->isPost) {
            //判断是否存在id
            $res = ExchangeGoodsLogic::add();
            if($res['code'] == 200) {
                return $this->redirect('/exchange-goods/wait-check');
            }
            else {
                Yii::$app->session->setFlash('message',$res['message']);
            }
        }
        foreach($contractDetail as $k=>&$v){
            if($v['total_num']==0){
                unset($contractDetail[$k]);
            }else{
                $v['total_num'] = $v['total_num']-$v['finish_num'];
            }
            $v['total_amount'] = sprintf('%0.2f',$v['sale_price']*$v['total_num']);
            $v['amount']  = $v['total_amount'];
        }
        $address = ContractLogic::getAddress($contractInfo['account_id']) ;
        return $this->render('add',['contract'=>$contractInfo,'contractDetail'=>$contractDetail,'address'=>$address]);
    }



    public function actionEdit(){
        if (yii::$app->request->isPost) {
            $res = ExchangeGoodsLogic::edit();
            if($res['code'] != 200) {
                Yii::$app->session->setFlash('message',$res['message']);
            }else{
                return $this->redirect('/exchange-goods/wait-check');
            }
        }
        $res = ExchangeGoodsLogic::getDetail();
        $list = $res['data'];
        if($res['code'] != 200) {
            Yii::$app->session->setFlash('message',$res['message']);
        }
        $list['info']['reason']  = ExchangeGoods::showReasonStatus();
        $info = $list['info'];
        $detail = $list['detail'];
        foreach($detail as $v){
            if($v['type']==1){
                $existNum[$v['prod_id']] = $v;
                $haveProdIds[] =$v['prod_id'];
            }
        }
        $contract = ContractLogic::getDetail($list['info']['contract_id']);
        $contractDetail = $contract['data']['detail'];

        foreach($contractDetail as &$v){
            if(in_array($v['prod_id'],$haveProdIds)){
                $v['check_status'] = 1;
                $exist = $existNum[$v['prod_id']]['prod_num'];
                $num = $v['total_num']-$exist;
                $v['prod_num'] = $exist;
                $v['amount'] = $existNum[$v['prod_id']]['amount'];
                $v['total_amount'] = $existNum[$v['prod_id']]['total_amount'];
                $v['last_num'] = $num<=0?0:$num;
                $v['warranty_date'] = $existNum[$v['prod_id']]['warranty_date'];
            }else{
                $v['check_status'] = 0;
                $v['last_num'] = $v['prod_num'];
            }

        }
        $address = ContractLogic::getAddress($list['info']['account_id']) ;
        return $this->render('edit',['info'=>$list['info'],'detail'=>$list['detail'],'contract'=>$contract['data']['info'],'contractDetail'=>$contractDetail,'address'=>$address]);
    }


    //详情
    function actionDetail(){
        //系统是否开启发货功能状态
        $shipSys = '1'; //1 已开启 0 未开启
        $id = intval(Yii::$app->request->get('id'));
        $res = ExchangeGoodsLogic::getDetail();
        if(empty($res['data'])){
            return $res['message'];
        }
        return $this->render('detail',[
            'info'      =>isset($res['data']['info'])?$res['data']['info']:[],
            'detail'    =>isset($res['data']['detail'])?$res['data']['detail']:[],
            'inList'    =>isset($res['data']['inList'])?$res['data']['inList']:[],
            'outList'   =>isset($res['data']['outList'])?$res['data']['outList']:[],
            'sendList'  =>isset($res['data']['sendList'])?$res['data']['sendList']:[],
            'shipSys'   =>$shipSys

        ]);

    }


    //审核
    function actionCheckOption(){
        $id = intval(Yii::$app->request->get('id'));
        if(Yii::$app->request->post()){
            $res = ExchangeGoodsLogic::checkOption();
            return json_encode($res);
        }
        $res = ExchangeGoodsLogic::getDetail();
        return $this->render('check-option',['info'=>$res['data']['info'],'detail'=>$res['data']['detail']]);
    }


    //更新状态
    function actionUpdateStatus(){
        $id = intval(Yii::$app->request->post('id'));
        $type = trim(Yii::$app->request->post('type'));
        $info = ExchangeGoodsLogic::getDetail($id);
        if($info['success']){
            $res = ExchangeGoodsLogic::updateStatus($type);
            return json_encode($res);
        }
    }

}


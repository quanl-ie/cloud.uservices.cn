<?php

namespace webapp\controllers;

use Yii;
use webapp\logic\BaseLogic;
use webapp\models\BaseModel;
use yii\web\Controller;
use common\helpers\Paging;
use webapp\logic\DepotLogic;
use common\models\Region;
use common\models\Depot;
/**
 *@desc:库房管理
 *
 */

class DepotController extends BaseController
{
    /**
     * 函数用途描述:库房列表
     * @date: 2018年3月20日 下午2:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionIndex()
    {
        $result = DepotLogic::getList();
        $pageHtml = '';
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }
        return $this->render('index',[
            'data'      =>$result['list'],
            'pageHtml'  =>$pageHtml
        ]);
    }
    /**
     * 函数用途描述:添加/编辑库房
     * @date: 2018年3月20日 下午2:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionAdd()
    {
        if (Yii::$app->request->isPost){
            //判断是否存在id 区分添加和修改
            if (yii::$app->request->post('id',0) != 0) {
                $res = DepotLogic::edit();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/depot/index');
            }else{
                $res = DepotLogic::add();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/depot/index');
            }
            //return json_encode($res);
        }
        $infos = [];
        if (yii::$app->request->isGet) {
            $id      = yii::$app->request->get('id',0);
            //获取库房信息
            $infos = DepotLogic::getDepotInfo($id);
        }
        $province = Region::getList(['parent_id'=>1]);
        return $this->render('add',[
            'infos'=>$infos,
            'province'=>$province
        ]);
    }
    /**
     * 函数用途描述:编辑库房
     * @date: 2018年3月20日 下午2:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionEdit()
    {
        if (Yii::$app->request->isPost){
            //判断是否存在id 区分添加和修改
            if (yii::$app->request->post('id',0) != 0) {
                $res = DepotLogic::edit();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/depot/index');
            }else{
                $res = DepotLogic::add();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/depot/index');
            }
            //return json_encode($res);
        }
        $infos = [];
        if (yii::$app->request->isGet) {
            $id      = yii::$app->request->get('id',0);
            //获取库房信息
            $infos = DepotLogic::getDepotInfo($id);
        }
        $province = Region::getList(['parent_id'=>1]);
        return $this->render('add',[
            'infos'=>$infos,
            'province'=>$province
        ]);
    }
    //验证信息重复性
    public function actionVerifyData(){
        if (yii::$app->request->isPost) {
            $post = yii::$app->request->post();
            $no = $post['str'];
            $id = $post['id'];
            if($no != ''){
                //验证数据
                $res = Depot::getOne(['no'=>$no,'org_id'=>BaseLogic::getManufactorId(),'org_type'=>BaseModel::SRC_FWS]);
                if($id){
                    if($res && $id != $res['id']){
                        return json_encode(['msg'=>'库房编号已存在！','code'=>'20002'],true);
                    }
                }else{
                    if($res){
                        return json_encode(['msg'=>'库房编号已存在！','code'=>'20002'],true);
                    }
                }
            }
            return json_encode(['msg'=>'ok','code'=>'200']);
        }
    }
    /**
     * 函数用途描述:删除库房
     * @date: 2018年3月20日 下午2:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionDel()
    {
        if (Yii::$app->request->isPost){
            $result = DepotLogic::delData();
            return json_encode($result);
        }
    }
    //获取城市
    public function actionGetCity($province_id)
    {
        $city = Region::getList(['parent_id'=>$province_id]);
        $str = '<option value="">请选择市</option>';
        foreach ($city as $val) {
            $str = $str . '<option value="'.$val['region_id'].'">'.$val['region_name'].'</option>';
        }
        return $str;
    }
    //获取区域
    public function actionGetDistrict($city_id)
    {
        $district = Region::getList(['parent_id'=>$city_id]);
        $str = '<option value="">请选择区</option>';
        foreach ($district as $val) {
            $str = $str . '<option value="'.$val['region_id'].'">'.$val['region_name'].'</option>';
        }
        return $str;
    }

}


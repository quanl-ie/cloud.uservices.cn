<?php

namespace webapp\controllers;

use common\components\Upload;
use common\helpers\Helper;
use common\models\DictEnum;
use webapp\logic\ContractLogic;
use webapp\logic\DepartmentLogic;
use webapp\logic\SaleOrderLogic;
use webapp\models\AccountAddress;
use Yii;
use webapp\logic\AccountLogic;
use webapp\controllers\BaseController;
use webapp\models\Account;
use common\helpers\Paging;
use webapp\models\Region;
use webapp\models\AdminUser;
use webapp\logic\AccountAddressLogic;
use webapp\models\Role;
use webapp\logic\BaseLogic;
use webapp\models\Department;
use webapp\logic\OrderLogic;

    /**
 * @desc:客户管理
 * @author liquan
 * @date 2018-6-6
 *
 */

class AccountController extends BaseController
{
    //列表
    public function actionIndex()
    {
        //获取当前用户信息
        $user_info = BaseLogic::getLoginUserInfo();
        // 用户最高权限
        $dataAuthId = $this->dataAuthId;
          //搜索部门结构 第一级
          if($dataAuthId == 3){  //本公司及以下
                $top_id = $this->directCompanyId;
                $info = department::getDepartmentInfo($top_id,1,0,1);
              //查询是否还有下级
              $nextDepartment = Department::getDepartmentChildren($top_id);
              if(count($nextDepartment) > 1){
                  $info[0]['exist'] = 1; //有下级
              }else{
                  $info[0]['exist'] = 2; //无
              }
          }else if($dataAuthId == 2){  //本公司
                $top_id = $this->directCompanyId;
                $info = department::getDepartmentInfo($top_id,1,0,1);
                $nextDepartment = Department::getDepartmentChildren($top_id,1,1);
                if(count($nextDepartment) > 1){
                      $info[0]['exist'] = 1; //有下级
                }else{
                      $info[0]['exist'] = 2; //无
                }
          }else if($dataAuthId == 5){   //其他权限
                if($this->departmentObj->type == 3){
                    $info = department::getDepartmentInfo($this->departmentId,1,0,1);
                    $nextDepartment = Department::getDepartmentChildren($this->departmentId,2);
                    if(count($nextDepartment) > 1){
                        $info[0]['exist'] = 1; //有下级
                    }else{
                        $info[0]['exist'] = 2; //无
                    }
                }else{
                    $info = array();
                }

          }else if($dataAuthId == 4){   //本部门
              if($this->departmentObj->type == 3){
                  $info = department::getDepartmentInfo($this->departmentId,1,0,1);
                  $info[0]['exist'] = 2; //无
              }else{
                  $info = array();
              }
          }else{
                $info = array();
          }
        $result = AccountLogic::index();
        if(isset($result['list']) && !empty($result['list'])){
            foreach ($result['list'] as $key=>$val){
                if($val['direct_company_id'] != $this->directCompanyId){
                    $de_info = Department::getDepartmentInfo($val['direct_company_id']);
                    $result['list'][$key]['name'] = $de_info[$val['direct_company_id']];
                }
            }
        }
        // 搜索参数保留
        if($result['params']['department_ids']){
            $department_ids = explode(",",$result['params']['department_ids']);
            $department_names = Department::findAllByAttributes(['id'=>$department_ids],"name");
            foreach ($department_names as $val){
                $names[] = $val['name'];
            }
            $result['params']['department_name'] = implode("；",$names);
            //$result['params']['department_id'] = implode(",",$result['params']['department_id']);
        }
        $url = '';
        if(!empty($result['params'])){
            if(isset($result['params']['department_id']))
            unset($result['params']['department_id']);
            foreach ($result['params'] as $key=>$val){
                $url .= $key."=".$val."&";
            }
        }
        $pageHtml = '';
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?'.$url.'page=');
        }
        $this->_view['department_id']     = $user_info->department_id;
        $this->_view['company_id'] = $this->directCompanyId;
        $this->_view['department']   = $info;
        $this->_view['auth']   =  $dataAuthId;
        $this->_view['params']   = $result['params'];
        $this->_view['data']     = $result['list'];
        $this->_view['pageHtml'] = $pageHtml;
        return $this->render('index');
    }

    /**
     * 填加/修改
     * @return string
     */
    public function actionAdd()
    {
          $dataAuthId          = $this->dataAuthId;
          if($dataAuthId == 3){  //本公司及以下
                $top_id = $this->directCompanyId;
                $info = department::getDepartmentInfo($top_id,1,0,1);
                //查询是否还有下级
                $nextDepartment = Department::getDepartmentChildren($top_id,0,1);
                if(count($nextDepartment) > 1){
                      $info[0]['exist'] = 1; //有下级
                }else{
                      $info[0]['exist'] = 2; //无
                }
          }else if($dataAuthId == 2){  //本公司
                $top_id = $this->directCompanyId;
                $info = department::getDepartmentInfo($top_id,1,0,1);
                $nextDepartment = Department::getDepartmentChildren($top_id,1,1);
                if(count($nextDepartment) > 1){
                      $info[0]['exist'] = 1; //有下级
                }else{
                      $info[0]['exist'] = 2; //无
                }
          }else if($dataAuthId == 5){   //部门以下
                if($this->departmentObj->type == 3){
                    $info = department::getDepartmentInfo($this->departmentId,1,0,1);
                    $nextDepartment = Department::getDepartmentChildren($this->departmentId,2);
                    if(count($nextDepartment) > 1){
                        $info[0]['exist'] = 1; //有下级
                    }else{
                        $info[0]['exist'] = 2; //无
                    }
                }else{
                    $info = department::getDepartmentInfo($this->departmentId,1,1,1);
                    $info[0]['exist'] = 2;
                }

          }else if($dataAuthId == 4){   //本部门
              if($this->departmentObj->type == 3){
                  $info = department::getDepartmentInfo($this->departmentId,1,0,1);
                  $info[0]['exist'] = 2; //无
              }else{
                  $info = department::getDepartmentInfo($this->departmentId,1,1,1);
                  $info[0]['exist'] = 2;
              }
          }else{  //本人
                $info = department::getDepartmentInfo($this->departmentId,1,1,1);
                $info[0]['exist'] = 2;
          }
          //添加 部门默认值为本部门
          $define_department = department::getDepartmentInfo($this->departmentId,1,1,1);
          $define_department = $define_department[0];

        //判断post传值
        if (Yii::$app->request->isPost)
        {
            //判断是否存在id 区分添加和修改
            $id = intval(Yii::$app->request->post('id',0));
            if ($id != 0)
            {
                $res = AccountLogic::edit();
                if($res['success'] == true)
                {
                    return json_encode([
                        'info'   => '修改成功',
                        'status' => 'y'
                    ]);
                }
                else
                {
                    return json_encode([
                        'info'   => $res['message'],
                        'status' => 'n'
                    ]);
                }
            }
            else
            {
                $res = AccountLogic::add();
                if($res['success'] == true)
                {
                    return json_encode([
                        'info'   => '添加成功',
                        'status' => 'y',
                        'id'     => $res['data']['id'],
                        'name'   => $res['data']['name']
                    ]);
                }
                else
                {
                    return json_encode([
                        'info'   => $res['message'],
                        'status' => 'n'
                    ]);
                }
            }
        }

        //判断是否get传值 修改参数
        if (Yii::$app->request->isGet)
        {
            $id      = Yii::$app->request->get('id');
            //查询客户信息
            $account = Account::getOne(['id'=>$id]);

            //查询客户联系地址
            $accountAddress = AccountAddressLogic::getAddress($id);
            //获取机构信息
              $department_edit = array();
              if(!empty($account)){
                    $department_name = Department::getOne(['id'=>$account->department_id]);
                    $department_edit = $department_name->attributes;
              }
        }
    
        //获取客户类型
        $dictEnum   = DictEnum::getList(['dict_key'=>'client_type']);
        $clientType = array_column($dictEnum,'dict_enum_value','dict_enum_id');
    
        //获取开通省份
        $province = Region::getOpenProvince();
        
        return $this->renderPartial('add',
                [
                    'define_department' => $define_department,
                    'department'      => $info, //权限内的所有机构
                    'this_department' =>$this->departmentId, //当前
                    'auth'            =>$dataAuthId,
                    'department_edit' =>$department_edit,
                    'province'        => $province,
                    'account'         => $account,
                    'accountAddress'  => $accountAddress,
                    'clientType'      => $clientType,
                    'id'              => $id
                ]);
    }

    /**
     * @return string
     * 客户编辑
     */
    public function actionEdit()
    {
        $dataAuthId          = $this->dataAuthId;
        if($dataAuthId == 3){  //本公司及以下
            $top_id = $this->directCompanyId;
            $info = department::getDepartmentInfo($top_id,1,0,1);
            //查询是否还有下级
            $nextDepartment = Department::getDepartmentChildren($top_id,0,1);
            if(count($nextDepartment) > 1){
                $info[0]['exist'] = 1; //有下级
            }else{
                $info[0]['exist'] = 2; //无
            }
        }else if($dataAuthId == 2){  //本公司
            $top_id = $this->directCompanyId;
            $info = department::getDepartmentInfo($top_id,1,0,1);
            $nextDepartment = Department::getDepartmentChildren($top_id,1,1);
            if(count($nextDepartment) > 1){
                $info[0]['exist'] = 1; //有下级
            }else{
                $info[0]['exist'] = 2; //无
            }
        }else if($dataAuthId == 5){   //部门以下
            if($this->departmentObj->type == 3){
                $info = department::getDepartmentInfo($this->departmentId,1,0,1);
                $nextDepartment = Department::getDepartmentChildren($this->departmentId,2);
                if(count($nextDepartment) > 1){
                    $info[0]['exist'] = 1; //有下级
                }else{
                    $info[0]['exist'] = 2; //无
                }
            }else{
                $info = department::getDepartmentInfo($this->departmentId,1,1,1);
                $info[0]['exist'] = 2;
            }

        }else if($dataAuthId == 4){   //本部门
            if($this->departmentObj->type == 3){
                $info = department::getDepartmentInfo($this->departmentId,1,0,1);
                $info[0]['exist'] = 2; //无
            }else{
                $info = department::getDepartmentInfo($this->departmentId,1,1,1);
                $info[0]['exist'] = 2;
            }
        }else{  //本人
            $info = department::getDepartmentInfo($this->departmentId,1,1,1);
            $info[0]['exist'] = 2;
        }
        //添加 部门默认值为本部门
        $define_department = department::getDepartmentInfo($this->departmentId,1,1,1);
        $define_department = $define_department[0];

        //判断post传值
        if (Yii::$app->request->isPost)
        {
            //判断是否存在id 区分添加和修改
            $id = intval(Yii::$app->request->post('id',0));
            if ($id != 0) {
                $res = AccountLogic::edit();
                return json_encode($res);
            }else{
                $res = AccountLogic::add();
                return json_encode($res);
            }
        }

        //判断是否get传值 修改参数
        if (Yii::$app->request->isGet) {
            $id      = Yii::$app->request->get('id');
            //查询客户信息
            $account = Account::getOne(['id'=>$id]);

            //查询客户联系地址
            $accountAddress = AccountAddressLogic::getAddress($id);
            //获取机构信息
            $department_edit = array();
            if(!empty($account)){
                $department_name = Department::getOne(['id'=>$account->department_id]);
                $department_edit = $department_name->attributes;
            }
            //查询当前编辑的用户的直属公司信息
            $origin_dep_info = Account::findOne(['id'=>$id]);
            $origin_dep_id = isset($origin_dep_info['department_id'])?$origin_dep_info['department_id']:'';
            $origin_dic_info = Department::findOne(['id'=>$origin_dep_id]);
            $origin_dic_id = isset($origin_dic_info['direct_company_id'])?$origin_dic_info['direct_company_id']:'';
        }
    
        //获取客户类型
        $dictEnum   = DictEnum::getList(['dict_key'=>'client_type']);
        $clientType = array_column($dictEnum,'dict_enum_value','dict_enum_id');
        
        //获取开通省份
        $province = Region::getOpenProvince();

        return $this->renderPartial('add',
            [
                'define_department'     => $define_department,
                'department'     => $info, //权限内的所有机构
                'this_department' =>$this->departmentId, //当前
                'auth'            =>$dataAuthId,
                'department_edit'=>$department_edit,
                'province'       => $province,
                'account'        => $account,
                'accountAddress' => $accountAddress,
                'clientType'     => $clientType,
                'origin_dic_id'  =>$origin_dic_id,
                'this_direct_company_id' =>$this->directCompanyId, //当前的直属公司id
                'id' =>$id
            ]);
    }

    //详情
    public function actionView()
    {
        $id = Yii::$app->request->get('account_id');
        $model = Account::getOne(['id'=>$id]);

        //查询客户联系地址
        if ($model && $model->id)
        {
            $accountAddress = AccountAddressLogic::getAddress($model->id);

            if($model->create_user)
            {
                //查询创建人用户名
                $user = AdminUser::getOne(['id' => $model->create_user]);
                $username = $user->username;
            }
            else{
                $username ='';
            }
        }
        else {
            return $this->redirect(Yii::$app->request->referrer);
        }

        //查询订单数据
        $result = OrderLogic::getList();
        $pageHtml = '';
        if(!empty($result['data']) && $result['data']['totalPage'] >1){
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], "?account_id=$id&page=");
        }
        if(!isset($result['data']['list'])){
            $result['data']['list'] = array();
        }

        //类型
        $clientType = DictEnum::getDesc('client_type',$model->client_type);
        if(!$clientType){
            $clientType = '暂无';
        }
        //所属机构
        $auth = $this->dataAuthId;

        if(in_array($auth,[2,3])) {
            $deparmtmentId = BaseLogic::getDirectCompanyId();

        }else{
            $deparmtmentId =$model->department_id;
        }
        $directCompany = Department::findOne(['id' => $deparmtmentId]);
        $defaultName = $directCompany->name;


        //页面赋值
        $this->_view['clientType']     = $clientType;
        $this->_view['departmentName'] = $defaultName;
        $this->_view['data']           = $result['data']['list'];
        $this->_view['model']          = $model;
        $this->_view['pageHtml']       = $pageHtml;
        $this->_view['username']       = $username;
        $this->_view['accountAddress'] = $accountAddress;
        return $this->render('view');
    }
    /**
     * 获取客户合同信息
     */
    public function actionGetContractInfo()
    {
        $id     = intval(yii::$app->request->get('account_id'));
        $result = ContractLogic::getList();
        $model = Account::getOne(['id'=>$id]);
        $username = '';
        $accountAddress = [];
        if ($model && $model->id) {
            $accountAddress = AccountAddressLogic::getAddress($model->id);
            //查询创建人用户名
            $user = AdminUser::getOne(['id'=>$model->create_user]);
            $username = $user->username;
        }else {
            return $this->redirect(Yii::$app->request->referrer);
        }
        $pageHtml = '';
        if(isset($result['totalPage']) && $result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], "?account_id=$id&page=");
        }
        $statusList = ['1'=>'待审批','2'=>'审批通过','3'=>'执行中','4'=>'执行完毕','5'=>'审批未通过','6'=>'已终止'];
		
		
		//类型
        $clientType = DictEnum::getDesc('client_type',$model->client_type);
        if(!$clientType){
            $clientType = '暂无';
        }
        //所属机构
        $auth = $this->dataAuthId;

        if(in_array($auth,[2,3])) {
            $deparmtmentId = BaseLogic::getDirectCompanyId();

        }else{
            $deparmtmentId =$model->department_id;
        }
        $directCompany = Department::findOne(['id' => $deparmtmentId]);
        $defaultName = $directCompany->name;
		
        //页面赋值
		$this->_view['clientType']     = $clientType;
        $this->_view['departmentName'] = $defaultName;
        $this->_view['data']           = $result['list'];
        $this->_view['model']          = $model;
        $this->_view['pageHtml']       = $pageHtml;
        $this->_view['username']       = $username;
        $this->_view['accountAddress'] = $accountAddress;
        $this->_view['statusList']     = $statusList;
        return $this->render('get-contract-info');
    }
	
    public function actionGetAddress()
    {
        $id = Yii::$app->request->get('id');
        $model = Account::getOne(['id'=>$id]);
        //查询客户地址
        $accountAddress = '';
        $username = '';
        if ($model) {
            $accountAddress = AccountAddressLogic::getAddress($model->id);

            if($model->create_user) {
                //查询创建人用户名
                $user = AdminUser::getOne(['id' => $model->create_user]);
                $username = $user->username;
            }else{
                $username ='';
            }
        }

        //查询客户全部联系地址
        $result = AccountAddressLogic::getList();
        $pageHtml = '';
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], "?id=$id&page=");
        }
		
		
		//类型
        $clientType = DictEnum::getDesc('client_type',$model->client_type);
        if(!$clientType){
            $clientType = '暂无';
        }
        //所属机构
        $auth = $this->dataAuthId;

        if(in_array($auth,[2,3])) {
            $deparmtmentId = BaseLogic::getDirectCompanyId();

        }else{
            $deparmtmentId =$model->department_id;
        }
        $directCompany = Department::findOne(['id' => $deparmtmentId]);
        $defaultName = $directCompany->name;
		
        //页面赋值
		$this->_view['clientType']     = $clientType;
        $this->_view['departmentName'] = $defaultName;
        $this->_view['data']           = $result['list'];
        $this->_view['model']          = $model;
        $this->_view['pageHtml']       = $pageHtml;
        $this->_view['username']       = $username;
        $this->_view['accountAddress'] = $accountAddress;
        return $this->render('get-address');
    }

    /**
     * 客户产品
     * @return string
     */
    public function actionGetSaleOrder()
    {
        $id = Yii::$app->request->get('account_id');
        $top_id = $this->directCompanyId;
        $type   = $this->departmentObj->type;
        $department_ids = [];
        if($this->dataAuthId == 3)
        {
            //获取上级及以下所有机构；
            $department_info = department::getDepartmentChildren($top_id,3,0);
            foreach ($department_info as $val){
                $department_ids[] = $val['id'];
            }
        }
        else if($this->dataAuthId  == 2)
        {
            //获取上级及以下部门
            $department_info = department::getDepartmentChildren($top_id,0,1);
            foreach ($department_info as $val){
                $department_ids[] = $val['id'];
            }
        }
        else if($this->dataAuthId  == 5)
        {
            if($type == 3)
            {
                $department_info = department::getDepartmentChildren(self::getDepartmentId(),2);
                foreach ($department_info as $val){
                    $department_ids[] = $val['id'];
                }
            }
        }
        else if($this->dataAuthId  == 4)
        {
            $department_ids = self::getDepartmentId();
        }

		if($department_ids){
			$model = Account::getOne(['id'=>$id,'status'=>1,'direct_company_id'=>$department_ids]);
		}
		else {
			$model = Account::getOne(['id'=>$id,'status'=>1,'create_user'=> $this->user->id ]);
		}
        if(!$model){
            return $this->redirect('/account/index');
        }

        $accountAddress = '';
        $username = '';
        if ($model)
        {
            $accountAddress = AccountAddressLogic::getAddress($model->id);
            if($model->create_user)
            {
                //查询创建人用户名
                $user = AdminUser::getOne(['id' => $model->create_user]);
                $username = $user->username;
            }
            else{
                $username ='';
            }
        }

        $saleOrder = AccountLogic::getSelaOrder(1);
        $pageHtml = '';
        if($saleOrder['totalPage'] >1)
        {
            $pageHtml = Paging::make($saleOrder['page'], $saleOrder['totalPage'], "?account_id=$id&page=");
        }
        $warranty = SaleOrderLogic::warranty();

		//类型
        $clientType = DictEnum::getDesc('client_type',$model->client_type);
        if(!$clientType){
            $clientType = '暂无';
        }
        //所属机构
        $auth = $this->dataAuthId;

        if(in_array($auth,[2,3])) {
            $deparmtmentId = BaseLogic::getDirectCompanyId();

        }else{
            $deparmtmentId =$model->department_id;
        }
        $directCompany = Department::findOne(['id' => $deparmtmentId]);
        $defaultName = $directCompany->name;
		
        //页面赋值
		$this->_view['clientType']     = $clientType;
        $this->_view['departmentName'] = $defaultName;
        $this->_view['data']           = $saleOrder['list'];
        $this->_view['model']          = $model;
        $this->_view['pageHtml']       = $pageHtml;
        $this->_view['username']       = $username;
        $this->_view['accountAddress'] = $accountAddress;
        $this->_view['warrantyType']   = isset($warranty['data'])?$warranty['data']:[];
        return $this->render('get-sale-order');
    }
    
    /**
     * 客户意向产品
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/27
     * Time: 15:02
     * @return string
     */
    public function actionIntentionProduct()
    {
        $id = Yii::$app->request->get('account_id');
        $model = Account::getOne(['id'=>$id,'status'=>1]);
        //查询客户联系地址
        $accountAddress = '';
        $username = '';
        if ($model) {
            $accountAddress = AccountAddressLogic::getAddress($model->id);
        
            if($model->create_user) {
                //查询创建人用户名
                $user = AdminUser::getOne(['id' => $model->create_user]);
                $username = $user->username;
            }else{
                $username ='';
            }
        }
    
        $saleOrder = AccountLogic::getSelaOrder(2);
        $pageHtml = '';
        if($saleOrder['totalPage'] >1){
            $pageHtml = Paging::make($saleOrder['page'], $saleOrder['totalPage'], "?account_id=$id&page=");
        }
    
    
        //类型
        $clientType = DictEnum::getDesc('client_type',$model->client_type);
        if(!$clientType){
            $clientType = '暂无';
        }
        //所属机构
        $auth = $this->dataAuthId;
    
        if(in_array($auth,[2,3])) {
            $deparmtmentId = BaseLogic::getDirectCompanyId();
        
        }else{
            $deparmtmentId =$model->department_id;
        }
        $directCompany = Department::findOne(['id' => $deparmtmentId]);
        $defaultName = $directCompany->name;
    
        //页面赋值
        $this->_view['clientType']     = $clientType;
        $this->_view['departmentName'] = $defaultName;
        $this->_view['data']           = $saleOrder['list'];
        $this->_view['model']          = $model;
        $this->_view['pageHtml']       = $pageHtml;
        $this->_view['username']       = $username;
        $this->_view['accountAddress'] = $accountAddress;
        return $this->render('intention-product');
    }

    //获取开通城市
    public function actionGetCity($province_id)
    {
        $city = Region::getOpenCity($province_id);
        $str = '<option value="">请选择市</option>';
        foreach ($city as $val) {
            $str = $str . '<option value="'.$val['region_id'].'">'.$val['region_name'].'</option>';
        }
        return $str;
    }
    //获取开通城市
    public function actionGetDistrict($city_id)
    {
        $district = Region::getOpenDistrict($city_id);
        $str = '<option value="">请选择区</option>';
        foreach ($district as $val) {
            $str = $str . '<option value="'.$val['region_id'].'">'.$val['region_name'].'</option>';
        }
        return $str;
    }

    /**
     * 查询符合条件的用户
     * @author  li
     * @date    2017-12-21
     * @return  json
     */
    public function actionSreach() {
        $name = Yii::$app->request->get('q','');
        $nameArr = AccountLogic::getAccountName($name);
        return json_encode($nameArr);
    }
    
    /**
    * 删除售后产品
    * @author  quan
    * @date    2018-6-22
    * @return  json
    */
    public function actionChangeStatus()
    {
        $res=  SaleOrderLogic::changeStatus();
        return json_encode($res);
    }
    
    
    /**
     * 头像上传
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/9
     * Time: 17:26
     */
    public function actionAccountUpload()
    {
        if(isset($_FILES['avatar_url']) && $_FILES['avatar_url']['size']>0)
        {
            $upload = new Upload();
            $result = $upload->upImage('account_avatar','avatar_url');
            
            if(isset($result['code']) && $result['code'] == 0){
                $src = $result['url'];
                echo "<script>parent.showImg('$src')</script>";die;
            }else{
                echo "<script>parent.showImg('/images/morentouxiang.png')</script>";
                die;
            }
        }
    }
    /**
     * 客户服务记录
     * sxz 2018-7-23
     *
     */
    public function actionServiceRecord()
    {
        $data = OrderLogic::getServiceRecord();
        if(Yii::$app->request->isAjax)
        {
            $work_stage_arr     = [];
            $new_work_stage_arr = [] ;
            if(isset($data['data']['list']) && !empty($data['data']['list']))
            {
                $work_stage_arr = array_column($data['data']['list'],'work_stage_arr');
                if($work_stage_arr){
                    foreach ($work_stage_arr as $key=>$val) {
                        $new_work_stage_arr = array_merge($new_work_stage_arr, $val);
                    }
                }
                $sort = array_column($new_work_stage_arr, 'finsh_service_time');
                array_multisort($sort, SORT_DESC, $new_work_stage_arr);
            }
            return json_encode($new_work_stage_arr);
        }
        return $this->renderPartial('service-record',
            [
                'data'     => $data,
                'selfRoles'    => $this->selfRoles
            ]);
    }
    //AJAX 获取客户信息
    public function actionGetAccountInfo()
    {
        Yii::$app->response->format = "json";
        $id = Yii::$app->request->post('account_id','');
        if($id == ''){
            return false;
        }
        //查询客户信息
        $account = Account::findOneByAttributes(['id'=>$id]);
        //所属机构名称
        $account['department_name'] = Department::getDepartmentName($account['department_id']);

        $info['account'] = $account;
        //查询客户联系地址
        $info['address'] = AccountAddress::findAllByAttributes(['account_id'=>$id]);

        return $info;
    }
}


<?php
namespace webapp\controllers;

use common\helpers\Helper;
use common\models\Common;
use common\models\ScheduleSet;
use common\models\ServiceArea;
use Faker\Provider\Base;
use webapp\logic\BaseLogic;
use webapp\logic\DepartmentLogic;
use webapp\models\AccountAddress;
use webapp\models\AdminUser;
use webapp\models\CostItem;
use webapp\models\CostLevelRelation;
use webapp\models\Department;
use webapp\models\Manufactor;
use webapp\models\ManufactorArea;
use webapp\models\ManufactorBrand;
use webapp\models\ManufactorClass;
use webapp\models\ManufactorType;
use webapp\models\Region;
use webapp\models\Role;
use webapp\models\User;
use Yii;
use yii\web\Response;


class OrganizationController extends BaseController
{

    /**
     * 列表页
     * @author xi
     * @date 2018-6-5
     */
    public function actionIndex()
    {
        if(Yii::$app->request->isAjax)
        {
            $this->layout = false;
            Yii::$app->response->format = Response::FORMAT_JSON;

            $result  = DepartmentLogic::getList();
            if($result['type'] == 1){
                return BaseLogic::success($result['list']);
            }
            else if($result['type'] == 2 || $result['type'] == 3)
            {
                $html = $this->renderPartial('_list',[
                    'data' => $result['list'],
                    'type' => $result['type'],
                    'selfDepartmentId' => $this->departmentId,
                    'directCompanyId'  => $this->directCompanyId,
                    'selfRoles'        => $this->selfRoles
                ]);
                return BaseLogic::success($html);
            }
        }

        //右边列表数据
        $result = DepartmentLogic::getList();
        //菜单
        $navData = DepartmentLogic::searchDepartment();

        $this->_view['data']    = $result['list'];
        $this->_view['navData'] = $navData['data'];
        $this->_view['type']    = $result['type'];
        $this->_view['selfDepartmentId'] = $this->departmentId;
        $this->_view['directCompanyId']  = $this->directCompanyId;
        $this->_view['selfRoles'] = $this->selfRoles;
        return $this->render('index');
    }

    /**
     * 添加下级
     * @author xi
     * @date 2018-6-5
     */
    public function actionAddDepartment()
    {
        $id = intval(Yii::$app->request->get('id',0));
        if($id <= 0){
            return $this->redirect('/organization/index');
        }

        $tmpDepartmentId = $this->departmentId;

        //如果是部门，同时数据权限是 本公司
        if($this->departmentObj->type == 3 && in_array($this->dataAuthId,[2,3])){
            $tmpDepartmentId = $this->directCompanyId;
        }
        //如果经销商或门店,数据权限是 （本人，本部门，本部门及以下）
        else if(in_array($this->departmentObj->type,[1,2]) && in_array($this->dataAuthId,[1,4,5])){
            $tmpDepartmentId = -1;
        }

        $where = [
            'id'         => $id,
            'status'     => 1,
            'del_status' => 1,
        ];
        $departmentModel = Department::find()
                ->where($where)
                ->andWhere("(find_in_set(".$tmpDepartmentId.",parent_ids) or id =".$tmpDepartmentId . ")")
                ->one();
        if(!$departmentModel){
            return $this->redirect('/organization/index');
        }

        //如果是部门，同时数据权限是 本公司 ，只能填加（门店、部门）
        if($this->dataAuthId == 2){
            $departmentModel->type = 2;
        }

        $model = new Department();
        if($model->load(Yii::$app->request->post()))
        {
            //联系人验证
            if(trim($model->contacts) != '' && Helper::strCount(trim($model->contacts)) >30 ){
                $model->addError('contacts','不超过30个字');
            }
            //详细地址
            if(trim($model->address) != '' && Helper::strCount(trim($model->address)) >30 ){
                $model->addError('address','不超过30个字');
            }
            //联系电话
            if(trim($model->phone) != '' && (!preg_match('/^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$/',$model->phone) && !preg_match('/^1\d{10}$/', $model->phone) )){
                $model->addError('phone','联系电话格式不正确');
            }
            //备注
            if(trim($model->remark) != '' && Helper::strCount(trim($model->remark)) >200 ){
                $model->addError('remark','至多200字');
            }
            //机构添加判断
            if($departmentModel->type == 1 && !in_array($departmentModel->type,[1,2,3])){
                $model->addError('type','机构类型选择不正确');
            }
            else if($departmentModel->type == 2 && !in_array($departmentModel->type,[2,3])){
                $model->addError('type','机构类型选择不正确');
            }
            else if($departmentModel->type == 3 && $departmentModel->type != 3){
                $model->addError('type','机构类型选择不正确');
            }

            $model->manufactor_id = $this->departmentObj->manufactor_id;
            $model->parent_ids    = Department::getDepartmentParentIds($model->parent_id);
            $model->create_time   = time();
            $model->update_time   = time();
            if(!$model->getErrors() && $model->save())
            {
                $directCompanyId = Department::getDirectCompanyId($model->id, $model->type);
                Department::updateAll(['direct_company_id'=>$directCompanyId],['id'=>$model->id]);

                //如果部门不初始化
                if($model->type == 1){
                    ScheduleSet::initScheduleSet($directCompanyId,$this->user->id);
                }

                if($model->submitType == '保存'){
                    return $this->redirect('/organization/index');
                }
                else if($model->submitType == '保存并下一步'){
                    return $this->redirect('/organization/service-config?id='.$model->id);
                }
                else
                {
                    return $this->redirect('/organization/add-user?id='.$model->id);
                }
            }
        }

        $this->_view['departmentModel'] = $departmentModel;
        $this->_view['model'] = $model;
        $this->_view['id']    = $id;
        return $this->render('add-department');
    }

    /**
     * 添加用户
     * @author xi
     * @date 2018-6-5
     */
    public function actionAddUser()
    {
        $id = intval(Yii::$app->request->get('id'));
        if($id <= 0 || User::hasUserByDepartmentId($id) == true){
            return $this->redirect('/organization/index');
        }

        $tmpDepartmentId = $this->departmentId;

        //如果是部门，同时数据权限是 本公司
        if($this->departmentObj->type == 3 && in_array($this->dataAuthId,[2,3])){
            $tmpDepartmentId = $this->directCompanyId;
        }
        else if((in_array($this->departmentObj->type,[1,2]) && in_array($this->dataAuthId,[1,4,5])) || ($this->departmentObj->type == 3 && $this->dataAuthId == 1)){
            $tmpDepartmentId = -1;
        }

        $where = [
            'id'         => $id,
            'status'     => 1,
            'del_status' => 1
        ];
        $departmentModel = Department::find()
            ->where($where)
            ->andWhere("(find_in_set(".$tmpDepartmentId.",parent_ids) or id =".$tmpDepartmentId . ")")
            ->one();
        if(!$departmentModel){
            return $this->redirect('/organization/index');
        }

        $model = new User();
        if($model->load(Yii::$app->request->post()))
        {
            if(Helper::strCount($model->username) > 10){
                $model->addError('username','不超过10个字');
            }
            if(!preg_match('/^1\d{10}$/',$model->mobile)){
                $model->addError('mobile','手机格式不正确');
            }
            if(strlen($model->password) < 6 || strlen($model->password) > 16){
                $model->addError('password','6-16位数字/大小写字母/下划线');
            }

            //查重（不同经销商可以重复）
            $hasRepeatUser = Department::hasRepeatUser($id,$model->mobile);
            if($hasRepeatUser == true){
                $model->addError('mobile','手机号不能重复');
            }

            $model->department_id = $id;
            if($model->department_id > 0)
            {
                $departmentArr = Department::findOneByAttributes(['id' => $model->department_id]);
                if($departmentArr)
                {
                    $model->department_pids = $departmentArr['parent_ids'];
                    $model->identity = $departmentArr['type'] == 1 ? 1: 2;
                }
            }

            $model->password_hash   = Yii::$app->security->generatePasswordHash($model->password);
            $model->created_at      = time();
            $model->updated_at      = time();
            $model->create_direct_company_id = $this->directCompanyId;
            $model->create_user_id   = $this->user->id;
            $model->is_direct_create = ($departmentArr['direct_company_id'] == $this->directCompanyId?0:1);
            $model->department_type  = $departmentArr['type'];

            if(!$model->getErrors() && $model->save())
            {
                Yii::$app->session->setFlash('message','添加成功');
                return $this->redirect('/organization/index');
            }
        }

        //查出角色
        $roleData = Role::getRoseByDepartmentId($this->departmentId);

        $this->_view['roleData']        = $roleData;
        $this->_view['departmentModel'] = $departmentModel;
        $this->_view['departmentId']    = $id;
        return $this->render('add-user');
    }

    /**
     * 修改机构
     * @author xi
     * @date 2018-6-5
     */
    public function actionEditDepartment()
    {
        $srcPage = intval(Yii::$app->request->get('src_page',1));
        $id      = intval(Yii::$app->request->get('id',0));
        if($id <= 0){
            return $this->redirect('/organization/index');
        }

        $tmpDepartmentId = $this->departmentId;

        //如果是部门，同时数据权限是 本公司
        if($this->departmentObj->type == 3 && in_array($this->dataAuthId,[2,3])){
            $tmpDepartmentId = $this->directCompanyId;
        }
        else if((in_array($this->departmentObj->type,[1,2]) && in_array($this->dataAuthId,[1,4,5])) || ($this->departmentObj->type == 3 && $this->dataAuthId == 1)){
            $tmpDepartmentId = -1;
        }
        $where = [
            'id'         => $id,
            'status'     => 1,
            'del_status' => 1
        ];
        $model = Department::find()
            ->where($where)
            ->andWhere("(find_in_set(".$tmpDepartmentId.",parent_ids) or id =".$tmpDepartmentId . ")")
            ->one();
        if(!$model){
            return $this->redirect('/organization/index');
        }

        if($model->load(Yii::$app->request->post()))
        {
            //联系人验证
            if(trim($model->contacts) != '' && Helper::strCount(trim($model->contacts)) >30 ){
                $model->addError('contacts','不超过30个字');
            }
            //详细地址
            if(trim($model->address) != '' && Helper::strCount(trim($model->address)) >30 ){
                $model->addError('address','不超过30个字');
            }
            //联系电话
            if(trim($model->phone) != '' && (!preg_match('/^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$/',$model->phone) && !preg_match('/^1\d{10}$/', $model->phone) )){
                $model->addError('phone','联系电话格式不正确');
            }
            //备注
            if(trim($model->remark) != '' && Helper::strCount(trim($model->remark)) >200 ){
                $model->addError('remark','至多200字');
            }

            $model->update_time   = time();
            if(!$model->getErrors() && $model->save())
            {
                //同步 manfactor 表
                $manfactorModel =  Manufactor::find()->where(['id'=>$model->manufactor_id])->one();
                if($manfactorModel)
                {
                    $manfactorModel->contact = $model->contacts;
                    $manfactorModel->mobile      = $model->phone;
                    $manfactorModel->province_id = $model->province_id;
                    $manfactorModel->city_id     = $model->city_id;
                    $manfactorModel->district_id = $model->district_id;
                    $manfactorModel->address     = $model->address;
                    $manfactorModel->email       = $model->email;
                    $manfactorModel->company_desc= $model->remark;
                    $manfactorModel->save(false);
                }

                if($model->submitType == '下一步'){
                    return $this->redirect("/organization/service-config?id=".$id);
                }
                else {
                    Yii::$app->session->setFlash('message','修改成功');
                    return $this->redirect('/organization/index');
                }
            }
        }

        //父级机构
        $parentDepartmentArr = Department::findOneByAttributes(['id'=>$model->parent_id],'name,type');

        $this->_view['srcPage'] = $srcPage;
        $this->_view['model']   = $model;
        $this->_view['id']      = $id;
        $this->_view['parentDepartment']     = $parentDepartmentArr?$parentDepartmentArr['name'] :'';
        $this->_view['parentDepartmentType'] = $parentDepartmentArr?$parentDepartmentArr['type'] :'';
        $this->_view['tmpDepartmentId']      = $tmpDepartmentId;
        return $this->render('edit-department');
    }

    /**
     * 设置服务配置
     * @author xi
     * @date 2018-6-5
     */
    public function actionServiceConfig()
    {
        $srcPage      = Yii::$app->request->get('src_page',1);
        $departmentId = intval(Yii::$app->request->get('id',0));
        if($departmentId <= 0 || $departmentId == $this->departmentId){
            return $this->redirect('/organization/index');
        }

        $tmpDepartmentId = $this->departmentId;

        //如果是部门，同时数据权限是 本公司
        if($this->departmentObj->type == 3 && in_array($this->dataAuthId,[2,3])){
            $tmpDepartmentId = $this->directCompanyId;
        }
        else if((in_array($this->departmentObj->type,[1,2]) && in_array($this->dataAuthId,[1,4,5])) || ($this->departmentObj->type == 3 && $this->dataAuthId == 1)){
            $tmpDepartmentId = -1;
        }

        $where = [
            'id'         => $departmentId,
            'status'     => 1,
            'del_status' => 1
        ];
        $departmentModel = Department::find()
            ->where($where)
            ->andWhere("(find_in_set(".$tmpDepartmentId.",parent_ids) or id =".$tmpDepartmentId . ")")
            ->one();
        if(!$departmentModel){
            return $this->redirect('/organization/index');
        }

        //处理提交逻辑
        if(Yii::$app->request->isPost)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->request->parsers = [
                'application/json' => 'yii\web\JsonParser',
                'text/json' => 'yii\web\JsonParser',
            ];

            $brandArr = Yii::$app->request->post('brand');
            $classArr = Yii::$app->request->post('class');
            $typeArr  = Yii::$app->request->post('type');
            $area     = Yii::$app->request->post('area');
            $costItemArr  = Yii::$app->request->post('cost_item');
            $costLevelArr = Yii::$app->request->post('cost_level');
            $departmentId = intval(Yii::$app->request->post('department_id',0));

            if($departmentId <=0 ){
                return BaseLogic::error("部门id不能为空",20001);
            }
            if(!$brandArr){
                return BaseLogic::error("请选择品牌",20002);
            }
            if(!$classArr){
                return BaseLogic::error("请选择产品类目",20003);
            }
            if(!$typeArr){
                return BaseLogic::error("请选择服务类型",20004);
            }
            if(!$area){
                return BaseLogic::error("请选择服务区域",20005);
            }
            if(!$costItemArr){
                return BaseLogic::error("请选择收费项目",20006);
            }
            if(!$costLevelArr){
                return BaseLogic::error("请选择收费标准",20007);
            }

            $transaction = Yii::$app->db->beginTransaction();
            try
            {
                //添加品牌
                $brandRes = ManufactorBrand::batchInsert($departmentId,$brandArr);
                //添加品类目
                $classRes = ManufactorClass::batchInsert($departmentId,$classArr);
                //添加服务类型
                $typeRes = ManufactorType::batchInsert($departmentId,$typeArr);
                //服务区域
                $areaRes = ManufactorArea::setServiceArea($departmentId,$area);

                $transactionOrder = Yii::$app->order_db->beginTransaction();
                try
                {
                    //收费项目
                    $itemRes = CostItem::batchInsert($departmentId,$costItemArr);
                    //收费标准
                    $levelRes = CostLevelRelation::batchInsert($departmentId,$costLevelArr);
                }
                catch(\Exception $orderException)
                {
                    $transaction->rollBack();
                    $transactionOrder->rollBack();
                    return BaseLogic::error('保存失败'.$orderException->getMessage(), 20013);
                }
            }
            catch(\Exception $e)
            {
                $transaction->rollBack();
                return BaseLogic::error('保存失败'.$e->getMessage(), 20013);
            }

            if($brandRes && $classRes && $typeRes && $areaRes && $itemRes && $levelRes)
            {
                $transactionOrder->commit();
                $transaction->commit();

                return BaseLogic::success([]);
            }
            else
            {
                $transaction->rollBack();
                $transactionOrder->rollBack();
                return BaseLogic::error('保存失败', 20013);
            }
        }

        $parentId = 0;
        $departmentArr = Department::findOneByAttributes(['id'=>$departmentId],'parent_id');
        if($departmentArr){
            $parentId = $departmentArr['parent_id'];
        }

        //品牌数据
        $brandData = ManufactorBrand::getSelfBrand($parentId, $departmentId);
        //收费项目
        $itemData = CostItem::getSelfItem($parentId,$departmentId);
        //收费标准
        $levelData = CostLevelRelation::getSelfLevel($parentId,$departmentId);
        //服务类型
        $typeData = ManufactorType::getSelfType($parentId,$departmentId);
        //产品类目
        $classData = ManufactorClass::getSelfClass($parentId,$departmentId);
        //服务区域
        $areaData = ManufactorArea::getSelfArea($departmentId);
        //查出是否可以填加用户
        $hasAddUser = User::hasUserByDepartmentId($departmentId);

        $this->_view['brandData'] = $brandData;
        $this->_view['itemData']  = $itemData;
        $this->_view['levelData'] = $levelData;
        $this->_view['typeData']  = $typeData;
        $this->_view['classData'] = $classData;
        $this->_view['areaData']  = $areaData;
        $this->_view['hasAddUser']= $hasAddUser;
        $this->_view['departmentId'] = $departmentId;
        $this->_view['provinceIds']  = ManufactorArea::getChooseProvince($this->directCompanyId);
        $this->_view['srcPage']      = $srcPage;
        return $this->render('service-config');
    }

    /**
     * 设置服务配置查看
     * @return string
     * @author xi
     */
    public function actionServiceConfigView()
    {
        $departmentId = intval(Yii::$app->request->get('id',0));
        if($departmentId <= 0 || $departmentId == $this->departmentId){
            return $this->redirect('/organization/index');
        }

        $tmpDepartmentId = $this->departmentId;

        //如果是部门，同时数据权限是 本公司
        if($this->departmentObj->type == 3 && in_array($this->dataAuthId,[2,3])){
            $tmpDepartmentId = $this->directCompanyId;
        }
        else if((in_array($this->departmentObj->type,[1,2]) && in_array($this->dataAuthId,[1,4,5])) || ($this->departmentObj->type == 3 && $this->dataAuthId == 1)){
            $tmpDepartmentId = -1;
        }

        $where = [
            'id'         => $departmentId,
            'status'     => 1,
            'del_status' => 1
        ];
        $departmentModel = Department::find()
            ->where($where)
            ->andWhere("(find_in_set(".$tmpDepartmentId.",parent_ids) or id =".$tmpDepartmentId . ")")
            ->one();
        if(!$departmentModel){
            return $this->redirect('/organization/index');
        }

        $parentId = 0;
        $departmentArr = Department::findOneByAttributes(['id'=>$departmentId],'parent_id');
        if($departmentArr){
            $parentId = $departmentArr['parent_id'];
        }

        //品牌数据
        $brandData = ManufactorBrand::getSelfBrand($parentId, $departmentId);
        //收费项目
        $itemData = CostItem::getSelfItem($parentId,$departmentId);
        //收费标准
        $levelData = CostLevelRelation::getSelfLevel($parentId,$departmentId);

        //服务类型
        $typeData = ManufactorType::getSelfType($parentId,$departmentId);
        //产品类目
        $classData = ManufactorClass::getCheckedClass($departmentId);
        //服务区域
        $areaData = ManufactorArea::getCheckedRegion($departmentId);

        $this->_view['brandData'] = $this->getCheckedName($brandData);
        $this->_view['itemData']  = $this->getCheckedName($itemData);
        $this->_view['levelData'] = $this->getCheckedName($levelData);
        $this->_view['typeData']  = $this->getCheckedName($typeData);
        $this->_view['classData'] = implode('、',$classData);
        $this->_view['areaData']  = $areaData;
        $this->_view['departmentId'] = $departmentId;
        return $this->render('service-config-view');
    }

    /**
     * 禁用
     * @author xi
     * @date 2018-6-5
     */
    public function actionDisable()
    {
        if(Yii::$app->request->isAjax)
        {
            $this->layout = false;
            Yii::$app->response->format = Response::FORMAT_JSON;

           $id = intval(Yii::$app->request->get('id', 0));

            if ($id <= 0) {
                return BaseLogic::error('参数id不能为空', 20002);
            }
            if($id == $this->departmentId){
                return BaseLogic::error('不能禁用自已', 20004);
            }

            $tmpDepartmentId = $this->departmentId;

            //如果是部门，同时数据权限是 本公司
            if($this->departmentObj->type == 3 && in_array($this->dataAuthId,[2,3])){
                $tmpDepartmentId = $this->directCompanyId;
            }
            else if((in_array($this->departmentObj->type,[1,2]) && in_array($this->dataAuthId,[1,4,5])) || ($this->departmentObj->type == 3 && $this->dataAuthId == 1)){
                $tmpDepartmentId = -1;
            }

            $where = [
                'id'         => $id,
                'status'     => 1,
                'del_status' => 1
            ];
            $model = Department::find()
                ->where($where)
                ->andWhere("(find_in_set(".$tmpDepartmentId.",parent_ids) or id =".$tmpDepartmentId . ")")
                ->one();
            if($model)
            {
                //如果是门店、部门要看看是否有用户，如有用户不可以禁用的
                if($model->type != 1)
                {
                    $count = User::find()->where(['department_id'=>$id,'del_status'=>1,'status'=>1])->count();
                    if($count > 0){
                        return BaseLogic::error('该机构下有员⼯。⽆法禁⽤。', 20005);
                    }
                }

                $model->update_time = time();
                $model->status      = 2;

                if($model->save(false)){
                      Department::updateAll(['status'=>2,'update_time'=>time()], "find_in_set(".$model->id.",parent_ids) and status=1 and del_status=1 and manufactor_id=".$model->manufactor_id);

                      //查询该机构下 所有机构
                      $d_son_info = Department::getDepartmentChildren($id);
                      $arr = array_column($d_son_info,'id');
                      $d_son_ids = implode(",",$arr);
                      User::updateAll(['status'=>2,'updated_at'=>time()], "department_id in (".$d_son_ids.") and status=1 and del_status=1");

                    return BaseLogic::success([]);
                }
            }

            return BaseLogic::error('禁用失败',20003);
        }
    }

    /**
     * 删除
     * @author xi
     * @date 2018-6-5
     */
    public function actionDel()
    {
        if(Yii::$app->request->isAjax)
        {
            $this->layout = false;
            Yii::$app->response->format = Response::FORMAT_JSON;

            $id = intval(Yii::$app->request->get('id',0));
            if($id <= 0){
                return BaseLogic::error('参数id不能为空',20002);
            }

            $tmpDepartmentId = $this->departmentId;

            //如果是部门，同时数据权限是 本公司
            if($this->departmentObj->type == 3 && in_array($this->dataAuthId,[2,3])){
                $tmpDepartmentId = $this->directCompanyId;
            }
            else if((in_array($this->departmentObj->type,[1,2]) && in_array($this->dataAuthId,[1,4,5])) || ($this->departmentObj->type == 3 && $this->dataAuthId == 1)){
                $tmpDepartmentId = -1;
            }

            $where = [
                'id'         => $id,
                'status'     => 2,
                'del_status' => 1
            ];
            $model = Department::find()
                ->where($where)
                ->andWhere("(find_in_set(".$tmpDepartmentId.",parent_ids) or id =".$tmpDepartmentId . ")")
                ->one();
            if($model)
            {
                $model->update_time = time();
                $model->del_status  = 2;
                if($model->save(false)){
                    Department::updateAll(['del_status'=>2,'update_time'=>time()], "find_in_set(".$model->id.",parent_ids) and status=2 and del_status=1 and manufactor_id=".$model->manufactor_id);
                      //禁用 用户
                      //查询该机构下 所有机构
                      $d_son_info = Department::getDepartmentChildren($id);
                      $arr = array_column($d_son_info,'id');
                      $d_son_ids = implode(",",$arr);
                      User::updateAll(['del_status'=>2,'updated_at'=>time()], "department_id in (".$d_son_ids.") and status=2 and del_status=1");
                    return BaseLogic::success([]);
                }
            }

            return BaseLogic::error('删除失败',20003);
        }
    }

    /**
     * 启用
     * @author xi
     * @date 2018-6-5
     */
    public function actionEnable()
    {
        if(Yii::$app->request->isAjax)
        {
            $this->layout = false;
            Yii::$app->response->format = Response::FORMAT_JSON;

            $id = intval(Yii::$app->request->get('id',0));
            if($id <= 0){
                return BaseLogic::error('参数id不能为空',20002);
            }
            $tmpDepartmentId = $this->departmentId;

            //如果是部门，同时数据权限是 本公司
            if($this->departmentObj->type == 3 && in_array($this->dataAuthId,[2,3])){
                $tmpDepartmentId = $this->directCompanyId;
            }
            else if((in_array($this->departmentObj->type,[1,2]) && in_array($this->dataAuthId,[1,4,5])) || ($this->departmentObj->type == 3 && $this->dataAuthId == 1)){
                $tmpDepartmentId = -1;
            }

            $where = [
                'id'         => $id,
                'status'     => 2,
                'del_status' => 1
            ];
            $model = Department::find()
                ->where($where)
                ->andWhere("(find_in_set(".$tmpDepartmentId.",parent_ids) or id =".$tmpDepartmentId . ")")
                ->one();
            if($model)
            {
                $model->update_time = time();
                $model->status      = 1;

                if($model->save(false)){
                    Department::updateAll(['status'=>1,'update_time'=>time()], "find_in_set(".$model->id.",parent_ids) and status=2 and del_status=1 and manufactor_id=".$model->manufactor_id);
                      //禁用 用户
                      //查询该机构下 所有机构
                      $d_son_info = Department::getDepartmentChildren($id);
                      $arr = array_column($d_son_info,'id');
                      $d_son_ids = implode(",",$arr);
                      User::updateAll(['status'=>1,'updated_at'=>time()], "department_id in (".$d_son_ids.") and status=2 and del_status=1");
                    return BaseLogic::success([]);
                }
            }

            return BaseLogic::error('启用失败',20003);
        }
    }

    /**
     * ajax 获取服务类型
     * @return array
     * @author xi
     * @date 2018-6-6
     */
    public function actionAjaxGetClass()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $classId = intval(Yii::$app->request->get('class_id',0));
        $departmentId = intval(Yii::$app->request->get('id',0));
        if($classId <= 0){
            return BaseLogic::error('参数 class_id 不能为空',20002);
        }
        if($departmentId <= 0){
            return BaseLogic::error('参数 department_id 不能为空',20003);
        }

        $parentId = 0;
        $departmentArr = Department::findOneByAttributes(['id'=>$departmentId],'parent_id');
        if($departmentArr){
            $parentId = $departmentArr['parent_id'];
        }

        $result = ManufactorClass::getSelfClass($parentId,$departmentId,$classId);
        return BaseLogic::success($result);
    }

    /**
     * 部门层级列表
     * @return array
     * @author xi
     * @date 2018-6-7
     */
    public function actionAjaxGetDepartment()
    {
        if(Yii::$app->request->isAjax)
        {
            $this->layout = false;
            Yii::$app->response->format = Response::FORMAT_JSON;
            $serviceArea   = intval(Yii::$app->request->get('service_area','0'));
            $addressId     = intval(Yii::$app->request->get('address_id'));
            $pid           = intval(Yii::$app->request->get('pid','0'));
            $totalWork     = intval(Yii::$app->request->get( 'total_work','1'));
            $serviceDepartmentId = intval(Yii::$app->request->get( 'service_department_id','0'));
            //服务区域覆盖的机构判断开始，精确到省-市-区
            if($serviceArea)
            {
                $list = DepartmentLogic::searchDepartment();
               $departmentId    = BaseLogic::getDepartmentId();
               $topId           = BaseLogic::getTopId();
               $directCompanyId = BaseLogic::getDirectCompanyId();
               $selfDepartmentId = $departmentId;
               if($pid!=$topId){
                   $departmentId = $pid;
               }else{
                   if($topId == $directCompanyId){
                       $departmentId = $topId;
                   }
               }

               //预约再次服务时，只能公司内部机构指派机构

               if($totalWork>1){
                   $sonListData = Department::getDepartment(['parent_id'=>$departmentId,'type'=>3]);
               }else{
                   $sonListData = Department::getDepartment(['parent_id'=>$departmentId]);
               }

                /* //顶级公司下的所有经销商
                 $allListData = Department::getDepartment(" parent_id=$topId and type=1 and id!=$departmentId");
                 //下级部门和经销商
                 $sonListData = Department::getDepartment(['parent_id'=>$departmentId]);

                 if($pid==$topId && $topId!=$directCompanyId){ //下级经销商id,合并 顶级id下所有经销商和本账号的子部门id
                     $data =  array_merge_recursive($sonListData,$allListData);
                 }else{
                     $data = $sonListData;   //当账号是当前总公司账号或者点击展开请求时，不包括总公司下1级经销商
                 }*/
                $data = $sonListData;
                /*  $list = DepartmentLogic::searchDepartment();*/
                $department = array_column($data,'id');
                array_unshift($department,$departmentId);
                $addressInfo = AccountAddress::findOne(['id'=>$addressId]);

                if($addressInfo) {
                    $addressStr = $addressInfo->province_id . '_' . $addressInfo->city_id . '_' . $addressInfo->district_id;
                        $directCompany = Department::getDepartment(['id'=>$department]);
                        $directCompanyId = array_unique(array_column($directCompany,'direct_company_id'));
                        if ($addressStr != '') {
                            $serviceList = ManufactorArea::getList([['in', 'a.direct_company_id', $directCompanyId], ['=', 'b.area', $addressStr]]);
                            foreach($data as $vv) {

                                foreach($serviceList as $key => $v) {
                                    if ($addressStr == $v['area']) {
                                        //总公司+经销商级别的账号
                                        if ($v['department_id'] == $vv['id'] && $vv['direct_company_id'] != $directCompanyId) {
                                            $newArr[] = $vv;

                                        }
                                        //本公司以下部门级别的账号
                                        if ($vv['direct_company_id'] == BaseLogic::getDirectCompanyId() && $v['department_id'] == $vv['direct_company_id']) {
                                            if($totalWork>1){
                                                if($vv['id'] == $selfDepartmentId && $selfDepartmentId != $directCompanyId){
                                                    continue;
                                                }
                                            }
                                            $newArr[] = $vv;

                                        }
                                    }
                                }
                            }

                        }

                    if (!empty($newArr)) {
                        return BaseLogic::success($newArr);
                    } else {
                        return BaseLogic::error('没有可服务此订单的机构');
                    }

                }
                //服务区域覆盖判断结束
            }
            return DepartmentLogic::searchDepartment();
        }

    }



    /**
     * 查看备注
     * @return array
     * @author xi
     * @date 2018-6-7
     */
    public function actionViewRemark()
    {
        if(Yii::$app->request->isAjax)
        {
            $this->layout = false;
            Yii::$app->response->format = Response::FORMAT_JSON;

            $id = intval(Yii::$app->request->get('id',0));
            if($id <= 0){
                return BaseLogic::error('参数id不能为空',20002);
            }

            $query = Department::findOneByAttributes(['id'=>$id,'del_status'=>1],'remark');
            if($query && $query['remark']){
                return BaseLogic::success($query['remark']);
            }
            return BaseLogic::error('未找到数据',20003);
        }
    }

    /**
     * ajax 判断门店部门下是否有用户
     * @return array
     * @author xi
     * @date 2018-6-11
     */
    public function actionAjaxHasDepartmentUser()
    {
        if(Yii::$app->request->isAjax)
        {
            $this->layout = false;
            Yii::$app->response->format = Response::FORMAT_JSON;

            $id = intval(Yii::$app->request->get('id',0));
            if($id <= 0){
                return BaseLogic::error('参数id不能为空',20002);
            }

            $count = User::find()->where(['department_id'=>$id,'del_status'=>1,'status'=>1])->count();
            return BaseLogic::success(['res'=>$count > 0?1:0]);
        }
    }

    /**
     * 查询是否有子部门
     * @return array
     * @author xi
     * @date 2018-6-11
     */
    public function actionAjaxHasSubDepartment()
    {
        if(Yii::$app->request->isAjax)
        {
            $this->layout = false;
            Yii::$app->response->format = Response::FORMAT_JSON;

            $id = intval(Yii::$app->request->get('id',0));
            if($id <= 0){
                return BaseLogic::error('参数id不能为空',20002);
            }

            $count = Department::find()->where(['parent_id'=>$id,'del_status'=>1])->count();
            return BaseLogic::success(['res'=>$count > 0?1:0]);
        }
    }

    /**
     * 检查机构名字是否有相同的
     * @return array
     * @author xi
     * @date 2018-6-11
     */
    public function actionAjaxCheckDepartment()
    {
        if(Yii::$app->request->isAjax)
        {
            $this->layout = false;
            Yii::$app->response->format = Response::FORMAT_JSON;

            $pid   = intval(Yii::$app->request->get('pid',0));
            $name = trim(Yii::$app->request->post('param',''));
            $id   = intval(Yii::$app->request->get('id',0));

            if($name == '' ){
                return BaseLogic::error('参数name不能为空',20003);
            }

            $where = [
                'name'      => $name,
                'parent_id' => $pid,
                'del_status'=> 1
            ];

            $andWhere = '1=1';
            if($id > 0){
                $andWhere = ' id <> '.$id;
            }

            $count = Department::find()->where($where)->andWhere($andWhere)->count();
            return [
                'status' => $count>0?'n':'y',
                'info'   => $count>0?'在此级别下已经存在该机构名称':''
            ];
        }
    }

    /**
     * 检查机构名字是否有相同的
     * @return array
     * @author xi
     * @date 2018-6-11
     */
    public function actionAjaxCheckUser()
    {
        if(Yii::$app->request->isAjax)
        {
            $this->layout = false;
            Yii::$app->response->format = Response::FORMAT_JSON;

            $pid    = intval(Yii::$app->request->get('pid',0));
            $mobile = trim(Yii::$app->request->post('param',''));

            if($mobile == '' ){
                return BaseLogic::error('参数param不能为空',20003);
            }

            $count = Department::hasRepeatUser($pid,$mobile);
            return [
                'status' => $count>0?'n':'y',
                'info'   => $count>0?'手机号已占用':''
            ];
        }
    }

    /**
     * 组织已选的数据
     * @param $data
     * @return string
     */
    private function getCheckedName($data)
    {
        $result = [];
        foreach ($data as $val) {
            if($val['checked'] == 1){
                $result[] = $val['name'];
            }
        }
        return implode('、',$result);
    }

    /**
     * 查出省份城市区县
     * @return array
     * @author xi
     * @date 2018-6-7
     */
    public function actionGetRegion()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $departmentId = Yii::$app->request->get('department_id',0);
        $pid = Yii::$app->request->get('pid',0);
        if(!$pid){
            return BaseLogic::error('参数pid不能为空',20002);
        }

        $parentId = 0;
        $departmentArr = Department::findOneByAttributes(['id'=>$departmentId],'parent_id');
        if($departmentArr){
            $parentId = $departmentArr['parent_id'];
        }

        //取出自已的服务区域
        $chooseRegionArr = ManufactorArea::getSelfArea($parentId);

        $result = [];
        $count = 1;
        do
        {
            $query = Region::findAllByAttributes(['parent_id'=>$pid],'region_id,region_name,parent_id',false,1000,$count);
            if($query){
                $result = array_merge($result,$query);
            }
            $count++;
        }
        while($query);

        $filterResult = [];
        foreach ($result as $val){
            if(in_array($val['region_id'],$chooseRegionArr)){
                $filterResult [] = $val;
            }
        }

        return BaseLogic::success($filterResult);
    }
}
<?php

namespace webapp\controllers;

use webapp\logic\BaseLogic;
use webapp\logic\OrderLogic;
use webapp\models\SaleOrder;
use webapp\models\SaleOrderView;
use Yii;
//use webapp\models\BrandQualification;
use webapp\logic\SaleOrderLogic;
use common\helpers\Paging;
use common\models\ServiceClass;
use webapp\models\ProductForm;

/**
 *@desc:客户管理
 * @author: chengjuanjuan <chengjuanjuan@c-ntek.com>
 * createdAt: 2017/12/14 14:09
 *
 */


class SaleOrderController extends BaseController
{
    /**
     * @desc 售后产品列表
     **/
   public function actionIndex(){
        $result = SaleOrderLogic::index();
        if($result['totalPage'] >=1){
            $pageHtml = Paging::make($result['page'],  $result['totalPage'], '?page=');
        }
        $brandList = SaleOrderLogic::BrandList();
        $this->_view['data']     = $result['list'];
        $this->_view['pageHtml'] = isset($pageHtml)?$pageHtml:' ';
        $this->_view['account_name'] = 'chengjuanjuan';
        $this->_view['brandList'] = $brandList;
        return $this->render('index');
    }

    // 添加
    public function actionAdd()
    {
        $accountId      =  Yii::$app->request->get('accountId',0);
        $type           =  Yii::$app->request->get('type',0);
        $source         =  intval(Yii::$app->request->get('source',0)); //来源  1下单页面 2 普通创建

        if (Yii::$app->request->isPost) {
            //判断是否存在id
            if (Yii::$app->request->get('id') == 0) {
                $res = SaleOrderLogic::add();
                return json_encode($res);

            }
        }
        $account   = SaleOrderLogic::GetAccount();
        $brandList = BaseLogic::getBrand('',1);//(全部品牌)
        $classList = BaseLogic::getClass('0',1);
        $typeList  = SaleOrderLogic::getType('',1);
        $warranty        = SaleOrderLogic::warranty();

        $data = [
            'brandList' => [],
            'classList' => [],
            'typeList'  => []
        ];
        if(!empty($brandList)){
            $data['brandList'] = $brandList;
        }
        if(!empty($classList)){
            $data['classList'] = $classList;
        }
        if(!empty($typeList)){
            $data['typeList'] = $typeList;
        }

        return $this->renderPartial('add',[
            'account'  => $account,
            'data'     => $data,
            'type'     => $type,
            'source' =>$source,
            'warranty' => (isset($warranty['data'])?$warranty['data']:[])
        ]);
    }
    /**
     * @desc:订单详情产品信息编辑
     * @author: sxz
     * createdAt: 2018/05/24 14:09
     */
   public function actionEdit(){

       $accountId      =  Yii::$app->request->get('accountId',0);
       $type           =  Yii::$app->request->get('type',0);
       $saleOrderId    =  Yii::$app->request->get('id',0);
       if (Yii::$app->request->isPost) {
           $res = SaleOrderLogic::edit();
           return json_encode($res);
       }
       $account   = SaleOrderLogic::GetAccount();
       $brandList = BaseLogic::getBrand('',1);//(全部品牌)
       $classList = BaseLogic::getClass('0',1);
       $typeList  = SaleOrderLogic::getType('',1);
       $warranty        = SaleOrderLogic::warranty();

       $data = [
           'brandList' => [],
           'classList' => [],
           'typeList'  => [],
           'product'   => []
       ];
       $product = [];
       //获取已经添加的产品信息
       if($saleOrderId)
       {
           $select = 'id,prod_id,prod_name,prod_name_wechat,product_type_id,brand_id,brand_name,class_id,class_name,type_name,warranty_num,warranty_type,prod_series,serial_number,produce_time,buy_time,info,source';
           $product = SaleOrderView::findAllByAttributes(['id'=>$saleOrderId,'status'=>1],$select,'id');
       }
       if(!empty($brandList)){
           $data['brandList'] = $brandList;
       }
       if(!empty($classList)){
           $data['classList'] = $classList;
       }
       if(!empty($typeList)){
           $data['typeList'] = $typeList;
       }
       if(!empty($product)){
           $data['product'] = array_values($product);
       }

       return $this->renderPartial('edit-prod-wechat',[
           'account'  => $account,
           'data'     => $data,
           'type'     => $type,
           'warranty' => (isset($warranty['data'])?$warranty['data']:[])
       ]);
       die;


       $orderNo  = trim(Yii::$app->request->get('order_no',''));
       if (yii::$app->request->isPost) {
           $res = SaleOrderLogic::editProduct();
           return json_encode($res);
       }
       $info            = SaleOrderLogic::getInfo();
       $warranty        = SaleOrderLogic::warranty();
       $model = new \webapp\models\OrderForm();
       //获取订单图片，质保状态信息
       $orderInfo = OrderLogic::getDetail($orderNo);
       if($info->id !='' && $orderInfo){
           $info->is_scope   = $orderInfo['is_scope'];
           $model->scope_img = $orderInfo['scope_img'];
       }
       return $this->render('edit-product',
           [
           'info'=>$info,
           'warranty'=>$warranty['data'],
           'model'=>$model,
           'orderNo'=>$orderNo
           ]);
    }


    //输入客户姓名带回相关的产品信息
   public function actionGetData(){
        SaleOrderLogic::getData();

    }


    //获取用户信息
    public function actionGetAccount(){
        SaleOrderLogic::getAccount();
    }




    //productId带出产品信息
    public  function actionGetProduct(){
        SaleOrderLogic::GetProduct();
    }

    //品牌id带出分类
    public function actionGetClass(){
        SaleOrderLogic::getClassData();
    }

    //品牌id带出分类
    public function actionGetSecondClass(){
        SaleOrderLogic::GetSecondClass();
    }


    //更改合作状态
    public function actionChangeStatus(){
        $res=  SaleOrderLogic::changeStatus();
        return json_encode($res);
    }
}


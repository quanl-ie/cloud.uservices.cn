<?php

namespace webapp\controllers;


use Yii;
use webapp\logic\BaseLogic;
use webapp\models\BaseModel;
use yii\web\Controller;
use common\helpers\Paging;
use webapp\logic\TechStorageLogic;
use common\models\Region;
use common\helpers\CreateNoRule;
use yii\web\Response;
/**
 *@desc:技师备件库存
 *
 */

class  TechStorageController extends BaseController
{

    /**
     * 函数用途描述:技师备件库存
     * @date: 2018年3月30日 下午15:30:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    //待审批
    public function actionIndex()
    {
        $is_list = intval(Yii::$app->request->get('is_list',0));
        $result = TechStorageLogic::getList();
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?technician_id='.$result['params']['technician_id'].'&page=');
        }else{
            $pageHtml = '';
        }
        if($is_list){
            return $this->renderPartial('list',[
                'data'          =>$result['list'],
                'pageHtml'      =>$pageHtml,
                'params'        =>$result['params'],
            ]);
        }
        return $this->render('index',[
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params']
        ]);
    }



}


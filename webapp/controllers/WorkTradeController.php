<?php
/**
 * 订单交易记录
 * User: xingq
 * Date: 2018/6/19
 * Time: 13:59
 */

namespace webapp\controllers;


use webapp\logic\WorkTradeLogic;
use common\helpers\Paging;

class WorkTradeController extends BaseController
{
    /**
     * 列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/21
     * Time: 15:14
     * @return string
     */
    public function actionIndex()
    {
        $res = WorkTradeLogic::index();
    
        $pageHtml = '';
        if($res['totalPage'] >1){
            $pageHtml = Paging::make($res['page'], $res['totalPage'], '?page=');
        }
        
        $this->_view['data']     = $res['list'];
        $this->_view['pageHtml'] = $pageHtml;
        
        return $this->render('index');
    }
}
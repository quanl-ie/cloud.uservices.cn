<?php

namespace webapp\controllers;

use Yii;
use webapp\logic\CategoryLogic;
use common\helpers\Paging;

class CategoryController extends BaseController
{
    /**
     * 列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/11
     * Time: 17:50
     * @return string
     */
    public function actionIndex()
    {
        
        $res = CategoryLogic::getIndex();
        
        $this->_view['flag']     = $res['flag'];
        $this->_view['data']     = $res['list'];
        return $this->render('index');
        
    }
    
    /**
     * 添加
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/11
     * Time: 17:50
     * @return mixed|string|void
     * @throws \Exception
     */
    public function actionAdd()
    {
        
        $data = CategoryLogic::beforeAdd();
        
        if (Yii::$app->request->isPost)
        {
            $res = CategoryLogic::add();
            return json_encode($res);
        }
        return $this->renderPartial('add',
            [
                'classList'   => $data,
            ]
        );
    }
    
    /**
     * 修改
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/11
     * Time: 17:50
     * @return mixed|string|void
     * @throws \Exception
     */
    public function actionEdit()
    {
        $data = CategoryLogic::beforeEdit();

        if (Yii::$app->request->isPost)
        {
            $res = CategoryLogic::edit();
            return json_encode($res);
        }
        return $this->renderPartial('edit',
            [
                'classList' => $data['list'],
                'data'      => $data['data'],
            ]
        );
    }
    
    /**
     * 添加下级
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/11
     * Time: 17:51
     * @return mixed|string|void
     * @throws \Exception
     */
    public function actionAddNext()
    {
        $data = CategoryLogic::beforeEdit();
        
        if (Yii::$app->request->isPost)
        {
            $res = CategoryLogic::add();
            return json_encode($res);
        }
        return $this->renderPartial('add-next',
            [
                'classList' => $data['list'],
                'data'      => $data['data'],
            ]
        );
        

    }
    
    /**
     * 更改类目状态
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/11
     * Time: 17:51
     * @return mixed|string|void
     * @throws \Exception
     */
    public function actionChangeStatus()
    {
        
        $res = CategoryLogic::changeStatus();
        
        return json_encode($res);
        
    }

    /**
     * @return string
     * @throws \Exception
     * 分类启用
     */
    public function actionChangeStatusOn()
    {

        $res = CategoryLogic::changeStatus();

        return json_encode($res);

    }
    
}


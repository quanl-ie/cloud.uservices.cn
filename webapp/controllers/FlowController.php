<?php

namespace webapp\controllers;
use Yii;
use webapp\logic\FlowLogic;

class FlowController extends BaseController
{
    /**
     * 列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 13:30
     * @return string
     */
    public function actionIndex()
    {
        $res = FlowLogic::getIndex();
        
        $this->_view['flag']        = $res['flag'];
        $this->_view['disbled']     = isset($res['disbled']) ? $res['disbled'] : '';
        $this->_view['serviceFlow'] = $res['serviceFlow'];
        $this->_view['serviceType'] = $res['serviceType'];
        return $this->render('index');
        
    }
    
    /**
     * 添加
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/15
     * Time: 14:56
     * @return mixed|string|void
     * @throws \Exception
     */
    public function actionAdd()
    {
        $res = FlowLogic::add();
        return json_encode($res);
    }

    /**
     * 编辑
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/15
     * Time: 14:56
     * @return mixed|string|void
     * @throws \Exception
     */
    public function actionUpdate()
    {
        $res = FlowLogic::add();
        return json_encode($res);
    }
    
    /**
     * 修改服务流程状态（禁用、启用）
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/15
     * Time: 15:40
     * @return mixed|string|void
     */
    public function actionChangeStatus()
    {
        $res = FlowLogic::changeStatus();
        return json_encode($res);
    }
    
    /**
     * 动态获取服务流程
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/15
     * Time: 15:40
     * @return mixed|string|void
     */
    public function actionAjaxIndex()
    {
        $data = FlowLogic::getIndex();
        return json_encode($data);
    }
    
    /**
     * ajax查看状态
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/2
     * Time: 22:00
     * @return mixed|string|void
     */
    public function actionGetStatus()
    {
        $data = FlowLogic::getStatus();
        return json_encode($data);
    }
    
    /**
     * 下单根据服务类型获取服务流程
     * User: sxz
     * Date: 2018/6/19
     * Time: 15:40
     * @return mixed|string|void
     */
    public function actionGetFlowStage()
    {
        $id             = intval(Yii::$app->request->post('id',0));
        $is_default     = intval(Yii::$app->request->post('is_default',2));
        $res = FlowLogic::getFlowStage($id,[1,2]);
        $serviceWorkStageNum = isset($res['data'][1])?$res['data'][1]:[];
        if($serviceWorkStageNum){
            $data = $res;
        }else{
            $data = [];
        }
        return json_encode($data);
    }
}


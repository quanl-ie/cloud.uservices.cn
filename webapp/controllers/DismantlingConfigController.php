<?php

namespace webapp\controllers;

use webapp\logic\BaseLogic;
use Yii;
use webapp\logic\DismantlingConfigLogic;

/**
 * Class DismantlingConfigController 拆单设置
 * @package webapp\controllers
 */
class DismantlingConfigController extends BaseController
{
    public function actionIndex()
    {
        if(Yii::$app->params['chaidan']['directCompanyId'] != BaseLogic::getDirectCompanyId()){
            return $this->redirect('/sys/index');
        }

        $res = DismantlingConfigLogic::index(self::DIS_TYPE);
        
        if (Yii::$app->request->isPost)
        {
            $res = DismantlingConfigLogic::add(self::DIS_TYPE);
            Yii::$app->session->setFlash('dismantling_config_message',$res['message']);
            return $this->redirect('index');
        }
        
        $this->_view['list'] = $res['list'];
        $this->_view['data'] = $res['self'];
        return $this->render('index');
    }
    
}


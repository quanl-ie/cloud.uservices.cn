<?php
namespace webapp\controllers;

use common\components\Upload;
use common\helpers\Helper;
use Faker\Provider\Base;
use webapp\logic\BaseLogic;
use webapp\logic\DepartmentLogic;
use webapp\models\AdminUser;
use webapp\models\User;
use webapp\models\Department;
use webapp\models\BaseModel;
use Yii;
use common\helpers\Paging;
use common\models\ServiceClass;
use webapp\models\Role;
use webapp\models\UserForm;
use webapp\models\UserInfo;
use webapp\models\Manufactor;
use webapp\models\Region;
use yii\web\Response;

/**
 * 用户管理
 * @author liwenyong<liwenyong@c-ntek.com>
 * @date   2017-08-17
 */
class UserController extends BaseController
{

    public function actions()
    {
        return  [
            'captcha'=>[
                'class' => 'yii\captcha\CaptchaAction',
                'backColor'=>0xFFFFFF,  //背景颜色
                'minLength'=>6,  //最短为4位
                'maxLength'=>6,   //是长为4位
                'transparent'=>true,  //显示为透明
                'foreColor' => 0x00CD8C,
            ],
        ];
    }

    /**
     * 用户列表
     * @author liquan
     * @date 2018-06-07
     */
    public function actionIndex()
    {

        //只显示上级创建的经销商的管理员
        //修正数据
          /*$user  = User::getList('1=1');
          foreach ($user as $val){
              $info =  Department::findOneByAttributes(['id'=>$val['department_id']]);
              //添加机构类型
              User::updateAll(['department_type'=>$info['type']],['id'=>$val['id']]);
              if($val['create_user_id']){
                  $top_id = User::findOne(['id'=>$val['create_user_id']]);
                  //创建他的直属id
                  $res = Department::getDirectCompanyIdById($top_id->department_id);
                  //他自己的直属id
                  $res2 =  Department::getDirectCompanyIdById($val['department_id']);
                   //上级创建
                  if($res != $res2){ //不是同一个公司
                      User::updateAll(['is_direct_create'=>1],['id'=>$val['id']]);
                  }
              }
          }*/
        $searchValue = strip_tags(trim(Yii::$app->request->get('searchValue','')));
        $status    = strip_tags(trim(Yii::$app->request->get('status',1)));
        $parentId = intval(Yii::$app->request->get('pid',0));
        $type     = intval(Yii::$app->request->get('type',2));
        $page = intval(Yii::$app->request->get('page',1));
        $pageSize = 10;
        $d_type   = 0;
        //当前登录的部门ID及机构类型
        $department_id       = $this->departmentObj->id;
        $department_type     = $this->departmentObj->type;
        $directCompanyId     = $this->directCompanyId;
        //获取当前用户信息
        $user_info = BaseLogic::getLoginUserInfo();
        //比较权限 选取权限较大的使用
        //$where[] = "department_id = ".$department_id." or (department_type = 1 and is_direct_create = 1 and department_id != ".$department_id.")";
        if($parentId > 0){
            $directCompanyId = $department_id = $parentId;
            if(isset($_GET['page'])){
                $ment_info = Department::findOne(['id'=>$parentId]);
                if($ment_info->type == 3){  //搜索部门及以下
                    //$directCompanyId = $parentId;
                    $d_type = 1;
                }/*else{
                    $directCompanyId = Department::getDirectCompanyIdById($parentId);
                }*/
            }
        }
        if($this->dataAuthId == 3){  //本公司及以下
               //获取当前部门的上级ID
             $top_id = $directCompanyId;
             if($d_type == 1){
                 $department_info = department::getDepartmentChildren($top_id,2);
             }else{
                 $department_info = department::getDepartmentChildren($top_id);
             }
             foreach ($department_info as $val){
                 $department_ids[] = $val['id'];
             }
             if(isset($department_ids) && $department_ids){
                $where = "(department_id = ".$department_id." or ((department_type = 1 and is_direct_create = 1 and department_id != ".$department_id." or department_type = 3) and department_id in (".implode(",",$department_ids)."))) and status = ".$status." and del_status = 1";
             }
        }
        if($this->dataAuthId == 2){   //本公司
               //获取当前部门的顶级部门ID
               $top_id = $directCompanyId;
               //获取上级部门及上级内部机构
               if($d_type == 1){
                   $department_info = department::getDepartmentChildren($top_id,2);
               }else{
                   $department_info = department::getDepartmentChildren($top_id,0,1);
               }
               foreach ($department_info as $val){
                   $department_ids[] = $val['id'];
               }
               $where = "(department_id = ".$department_id." or (department_type = 3 and department_id in (".implode(",",$department_ids)."))) and status = ".$status." and del_status = 1";

        }
          if($this->dataAuthId == 4){   //本部门
               if($department_type == 3){
                   $where = "department_id = ".$department_id." and status = ".$status." and del_status = 1";
               }else{
                   $where = "id = ".$user_info->id." and status = ".$status." and del_status = 1";
                   //$where[] =['=','id' ,$user_info->id];
                   //$this->dataAuthId = 1;
               }
           }
           if($this->dataAuthId == 5) {//本部门及以下
                 if ($department_type == 3) {
                       //获取自身及以下
                       $department_info = department::getDepartmentChildren($department_id,2);
                       foreach ($department_info as $val) {
                             $department_ids[] = $val['id'];
                       }
                      $where = "(department_type = 3 and department_id in (".implode(",",$department_ids).")) and status = ".$status." and del_status = 1";

                    // $where[] = ['in', 'department_id', $department_ids];
                 }else{
                     $where = "id = ".$user_info->id." and status = ".$status." and del_status = 1";
                      // $where[] =['=','id' ,$user_info->id];
                      // $this->dataAuthId = 1;
                 }
           }
          if ($this->dataAuthId == 1) {
              $where = "id = ".$user_info->id." and status = ".$status." and del_status = 1";
             //   $where[] =['=','id' ,$user_info->id];
          }
          if(Yii::$app->request->isAjax)
          {
                $this->layout = false;
                Yii::$app->response->format = Response::FORMAT_JSON;
                // 获取选取导航机构下的用户
                if($parentId > 0){
                    if($this->dataAuthId == 4){
                            $low_info = department::getDepartmentInfo($parentId,1,0,1);
                      }else{
                            //获取机构类型
                            $de = Department::findOne(['id'=>$parentId]);
                            if($de->type == 1 && $this->dataAuthId != 2){
                                    //下级经销商
                                    $next_parntId = Department::getDirectCompanyIdById($parentId);
                                    if($next_parntId != $this->directCompanyId){
                                        $low_info = department::getDepartmentChildren($parentId,1);
                                    }else{
                                        $low_info = department::getDepartmentChildren($parentId);
                                    }
                            }else{
                                  $low_info = department::getDepartmentChildren($parentId,2);
                            }
                      }
                      foreach ($low_info as $val){
                            $low_ids[] = $val['id'];
                      }
                }
                if($this->dataAuthId == 1){
                    $low_where = "id =".$user_info->id." and status= ".$status." and del_status=1";
                }else{
                    if($this->departmentId == $parentId){
                        $low_where = "(department_id = ".$parentId." or ((department_type = 1 and is_direct_create = 1 and department_id != ".$parentId." or department_type = 3) and department_id in (".implode(",",$low_ids)."))) and status = ".$status." and del_status = 1";
                    }else{
                        $low_where = "((department_id = ".$parentId." and identity = 1) or (department_id = ".$parentId." and department_type = 1 and is_direct_create = 1) or ((department_type = 1 and is_direct_create = 1 and department_id != ".$parentId." or department_type = 3) and department_id in (".implode(",",$low_ids)."))) and status = ".$status." and del_status = 1";
                    }
                    // $low_where = "department_id = ".$parentId." or (department_type = 1 and is_direct_create = 1 and department_id != ".$parentId.") and department_id in (".implode(",",$low_ids).") and status=1 and del_status=1";
                   // $low_where[] =['in','department_id' ,$low_ids];
                }
                if($searchValue != ''){
                    $low_where .= " and (mobile like '%".$searchValue."%' or username like '%".$searchValue."%')";
                    //$low_where[] = (['or',['like','mobile',$searchValue],['like','username',$searchValue]]);
                }
                $data = AdminUser::getList($low_where,'created_at desc',$page,$pageSize);
               
                if($data['list'])
                {
                      foreach ($data['list'] as $key=>$val)
                      {
                            $department =  Department::findOne(['id'=>$val['department_id']]);
                            $data['list'][$key]['department'] =$department->name;
                      }
                }
                // 分页处理
                $pageHtml = '';
                if ($data['totalPage']>1) {
                      $pageHtml = Paging::make($page, $data['totalPage'], "?searchValue=$searchValue&type=$type&pid=$parentId&status=$status&page=");
                }
                if($type == 1){
                      return BaseLogic::success($data['list']);
                }else if($type == 2 || $type == 3)
                {
                      $html = $this->renderPartial('_list',[
                            'pageHtml'=>$pageHtml,
                            'pageSize'=>$pageSize,
                            'data' => $data,
                            'status' => $status,
                            'type' => $type,
                            'uid' => $user_info->id,
                            'did' => $parentId,
                           'selfRoles' => $this->selfRoles
                      ]);
                      return BaseLogic::success($html);
                }

          }
          //菜单
          if(isset($_GET['page'])){
              $navData = DepartmentLogic::searchDepartment(1);
          }else{
              $navData = DepartmentLogic::searchDepartment();
          }
          if(empty($navData['data']) && $this->dataAuthId == 1){
                $navData['data'] = Department::findAllByAttributes(['id'=>$department_id]);
                $navData['data'][0]['sub'] = 0;
          }

        if($searchValue != ''){
            $where .= " and (mobile like '%".$searchValue."%' or username like '%".$searchValue."%')";
            //$where[] = (['or',['like','mobile',$searchValue],['like','username',$searchValue]]);
        }
        $data = AdminUser::getList($where,'created_at desc',$page,$pageSize);
        if($data['list'])
        {
            foreach ($data['list'] as $key=>$val)
            {
                $department =  Department::findOne(['id'=>$val['department_id']]);
                $data['list'][$key]['department'] =$department->name;
            }
        }
        // 分页处理
        $pageHtml = '';
        if ($data['totalPage']>1) {
              $pageHtml = Paging::make($page, $data['totalPage'], "?searchValue=$searchValue&type=$type&pid=$parentId&status=$status&page=");
        }

        $this->_view['userId']  =$user_info->id;
        $this->_view['department_id'] = $department_id;
        $this->_view['searchValue']    = $searchValue;
        $this->_view['pageHtml']  = $pageHtml;
        $this->_view['pageSize']  = $pageSize;
        $this->_view['data']      = $data;
        $this->_view['status']      = $status;
        $this->_view['auth']      = $this->dataAuthId;
        $this->_view['type']      = $type;
        $this->_view['navData']      = $navData['data'];
        $this->_view['selfRoles'] = $this->selfRoles;
        return $this->render('index');
    }

    /**
     * 添加成员
     * @return \yii\web\Response|string
     * @author liquan
     * @date 2018-06-11
     */
    public function actionAdd()
    {
        $model = new AdminUser();
        $model->setScenario('adminUser');
        //当前登录的部门ID
        $department_id =  $this->departmentId;
        //当前登录机构类型
        $department_type = $this->departmentObj->type;

        $user_info = BaseLogic::getLoginUserInfo();
        $department = array();
        if($this->dataAuthId == 3){  //本公司及以下
            //获取当前部门的顶级部门ID
            $top_id = $this->directCompanyId;
            $department = Department::getDepartmentInfo($top_id,1,0,1);
            //查询是否还有下级
            $nextDepartment = Department::getDepartmentInfo($top_id,0,1,1);
            if(!empty($nextDepartment)){
                  $department[0]['exist'] = 1; //有下级
            }else{
                  $department[0]['exist'] = 2; //无
            }
              $where_role ='direct_company_id ='.$top_id;
        }
        if($this->dataAuthId == 2){    //本公司
            //获取当前部门的顶级部门ID
            $top_id = $this->directCompanyId;
            $department = Department::getDepartmentInfo($top_id,1,1,1);
            //查询是否还有下级
            $nextDepartment = Department::getDepartmentInfo($top_id,0,1,1);
            if(!empty($nextDepartment)){
                  $department[0]['exist'] = 1; //有下级
            }else{
                  $department[0]['exist'] = 2; //无
            }
              //获取自身及以下
              $department_info = department::getDepartmentChildren($department_id,0,1);
              foreach ($department_info as $val){
                    $department_ids[] = $val['id'];
              }
              $where_role ='direct_company_id ='.$top_id;
        }
        if($this->dataAuthId == 5){    //本部门及以下
            if($department_type == 3){  //机构类型为部门
                  $department = Department::getDepartmentInfo($department_id,1,1,1);
                  //查询是否还有下级
                  $nextDepartment = Department::getDepartmentChildren($department_id,2);
                  if(count($nextDepartment) > 1){
                        $department[0]['exist'] = 1; //有下级
                  }else{
                        $department[0]['exist'] = 2; //无
                  }
                    //获取自身及以下
                    $department_info = department::getDepartmentChildren($department_id);
                    foreach ($department_info as $val){
                        $department_ids[] = $val['id'];
                    }
                    $depart_ids = implode(",",$department_ids);
                    $where_role = 'department_id in ('.$depart_ids.')';
            }else{
                    $where_role = 'department_id ='.$department_id;
                    $where_role .=' and create_user_id='.$this->user->id;
            }
        }
        if($this->dataAuthId == 4){    //本部门
            if($department_type == 3){
              $department = Department::getDepartmentInfo($department_id,1,1,1);
              $department[0]['exist'] = 2; //无
              //获取自身及以下
              $where_role = 'department_id ='.$department_id;
            }else{
              $where_role = 'department_id ='.$department_id;
              $where_role .=' and create_user_id='.$this->user->id;
            }
        }
        if($this->dataAuthId == 1){
            $where_role = 'department_id ='.$department_id;
            $where_role .=' and create_user_id='.$this->user->id;
        }
        //add 员工
        if ($model->load(Yii::$app->request->post())) {
            $errors = $model->getErrors();
            if (empty($errors)) {
                $department_info = Department::findOne(['id' => $model->department_id]);
                $model->department_pids = $department_info->parent_ids;
                $model->role_ids         = rtrim(implode(",",explode(";",$model->role_ids)),",");
                $model->created_at       = time();
                $model->updated_at       = time();
                $model->create_user_id   = $user_info->id;
                $model->password_hash    = Yii::$app->security->generatePasswordHash($model->password);
                $model->identity = 2;
                $model->create_direct_company_id = $this->directCompanyId;
                $model->department_type = DepartmentLogic::getDepartmentTypeById($model->department_id);
                if($this->directCompanyId != Department::getDirectCompanyIdById($model->department_id)){
                    $model->is_direct_create = 1;
                }
                if ($model->save()) {
                    Yii::$app->session->setFlash('message','添加成功');
                    return $this->redirect('/user/index');
                }

            }
        }
        $roles = Role::getList($where_role);
        $department_info = department::getDepartmentChildren($this->directCompanyId,0,1);
        foreach ($department_info as $val){
              $ids[] = $val['id'];
        }
        $dids = implode(",",$ids);
        $this->_view['directCompanyId'] = $this->directCompanyId;
        $this->_view['department'] = $department;
        $this->_view['department_id'] = $department_id;
        $this->_view['department_ids'] = $dids;
        $this->_view['roles'] = $roles;
        $this->_view['model'] = $model;
        $this->_view['auth'] = $this->dataAuthId;
        return $this->render('add');
    }
      public function actionAjaxRegMobile()
      {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $department_id = Yii::$app->request->post('department_id','');
            $mobile        = Yii::$app->request->post('mobile','');
            $department_info = Department::findOne(['id' => $department_id]);
            //用户查重
            if($department_info->direct_company_id == $this->directCompanyId){
                  $son =  Department::getDepartmentChildren($department_info->direct_company_id,0,1);
                  foreach($son as $val){
                        $ids[] = $val['id'];
                  }
                  $user_have = User::getUserByDepartmentId($ids,$mobile);
                  if($user_have){
                        return BaseLogic::error('该机构下登录手机号已被占用，请编辑后再次尝试');
                  }
                  return BaseLogic::success([]);
            }
            return BaseLogic::success([]);
      }


    /**
     * 修改成员信息
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date 2017-08-17
     */
    public function actionUpdate()
    {
        $id = intval(Yii::$app->request->get('id',0));
        if ($id<=0) {
            return $this->redirect('index');
        }
        $model = AdminUser::findOne(['id'=>$id]);
        if (!$model) {
            return $this->redirect('index');
        }
        $model->setScenario('updateAdminUser');

        //当前部门id
        $department_id = $this->departmentId;
        //当前数据权限
        $data_auth_id = $this->dataAuthId;

        $department_type = $this->departmentObj->type;
        if($department_type == 1 && ($data_auth_id == 4 || $department_type== 5)){
              $data_auth_id = 1;
        }
        if($data_auth_id == 0){
            $data_auth_id = 3;
        }
        //当前部门信息
        $department = $this->departmentObj->attributes;
        if($data_auth_id == 3 || $data_auth_id == 2){  //本公司及以下
            //获取当前部门的顶级部门ID
            $top_id = $this->directCompanyId;
            $where_role ='direct_company_id ='.$top_id;
        }
        if($data_auth_id == 5){    //本部门及以下
            //获取自身及以下
            $department_info = department::getDepartmentChildren($department_id,2);
            foreach ($department_info as $val){
                $department_ids[] = $val['id'];
            }
            $depart_ids = implode(",",$department_ids);
            $where_role = 'department_id in ('.$depart_ids.')';
        }
        if($data_auth_id == 4){ //本部门
            //获取自身及以下
            $where_role = 'department_id ='.$department_id;
        }
        if($data_auth_id == 1){
            $where_role = 'department_id ='.$department_id;
            $where_role .=' and create_user_id='.$this->user->id;
        }
        if ($model->load(Yii::$app->request->post())) {
            $errors = $model->getErrors();
            if (empty($errors)) {
                  $model->role_ids         = rtrim(implode(",",explode(";",$model->role_ids)),",");
                  $model->updated_at       = time();
               /* if ($model->password) {
                    $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
                }*/
                if ($model->save()) {
                    Yii::$app->session->setFlash('message','编辑成功');
                    return $this->redirect('/user/index');
                }
                Helper::dump($model->getErrors());
            } else {
                //获取权限角色
                $roles = Role::getList(['department_id'=>$department_id]);
                $this->_view['roles'] = $roles;
                $this->_view['model'] = $model;
                return $this->render('update');
            }
        }

        //获取权限角色
        $roles = Role::getList($where_role);

        //保存的角色  获取角色名称
        $select_roles = Role::findAllByAttributes("id in (".$model->attributes['role_ids'].")");
        $role_arr = array();

        foreach ($select_roles as $val){
            $role_arr['name'][] = $val['name'];
            $role_arr['ids'][]  = $val['id'];
        }
        //根据部门id获取部门名称
        $department_name = Department::getDepartmentInfo($model->attributes['department_id'],1);
          $department_info = department::getDepartmentChildren($department_id,0,1);
          foreach ($department_info as $val){
                $ids[] = $val['id'];
          }
          $dids = implode(",",$ids);
          $this->_view['department_ids'] = $dids;
        $this->_view['department_name'] =   implode(",",$department_name);
        $this->_view['select_roles'] = $role_arr;
        $this->_view['department'] = $department;
        $this->_view['department_id'] = $department_id;
        $this->_view['roles'] = $roles;
        $this->_view['user_info'] = $model->attributes;
        $this->_view['model'] = $model;
        return $this->render('update');
    }


    /**
     * 查看详情
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date 2017-08-17
     */
    public function actionView()
    {
        $id = intval(Yii::$app->request->get('id',0));
        if ($id<=0) {
            return $this->redirect('index');
        }

        $model = AdminUser::findOne(['id'=>$id]);
        if (!$model) {
            return $this->redirect('index');
        }

        $roles = [];
        if ($model->role_ids !='') {
            $res = Role::find()->where(['id'=>explode(',', $model->role_ids)])->select('name')->asArray()->all();
            foreach ($res as $val){
                $roles[] = $val['name'];
            }
        }
        $this->_view['roles'] = $roles?implode('、', $roles):'';
        $this->_view['model'] = $model;
        return $this->render('view');
    }

    /**
     * 禁用
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return String
     */
    public function actionDisable()
    {
        $t    = intval(Yii::$app->request->get('t',1));
        $d    = intval(Yii::$app->request->get('d',1));
        $id   = intval(Yii::$app->request->get('id',0));
        if ($id<= 0) {
            return $this->redirect('/user/index');
        }
        if($d == 2){
              $del_status = 2;
        }else{
              $del_status = 1;
        }
        if($t == 1){
            $status = 2;
        }
        elseif($t == 2) {
            $status = 1;
        }
        else {
            $status = 0;
        }

        $where = [
            'id'  =>$id,
        ];
        $model = AdminUser::findOne($where);
          if($t ==2 ){ //
                $info = Department::getDepartmentInfo($model->department_id,1,0,1);
                if(!empty($info)){
                      if($info[0]['status'] == 2){
                            Yii::$app->session->setFlash('message','该用户所属机构被禁用，无法启用该用户，请启用机构后再次尝试!');
                            return $this->redirect ('/user/index?status=2');
                      }
                }
          }


        if (!empty($model)) {
            $model->status = $status;
            $model->del_status = $del_status;
            $model->updated_at = date("Y-m-d H:i:s");
            if ($model->save(false)) {
                Yii::$app->session->setFlash('message','操作成功!');
                return $this->redirect ('/user/index?status=2');
            }
        }
        return $this->redirect('/user/index');
    }

    /**
     * 启用
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return String
     */
    public function actionEnable()
    {
        $t    = intval(Yii::$app->request->get('t',1));
        $d    = intval(Yii::$app->request->get('d',1));
        $id   = intval(Yii::$app->request->get('id',0));
        if ($id<= 0) {
            return $this->redirect('/user/index');
        }
        if($d == 2){
            $del_status = 2;
        }else{
            $del_status = 1;
        }
        if($t == 1){
            $status = 2;
        }
        elseif($t == 2) {
            $status = 1;
        }
        else {
            $status = 0;
        }

        $where = [
            'id'  =>$id,
        ];
        $model = AdminUser::findOne($where);
        if($t ==2 ){ //
            $info = Department::getDepartmentInfo($model->department_id,1,0,1);
            if(!empty($info)){
                if($info[0]['status'] == 2){
                    Yii::$app->session->setFlash('message','该用户所属机构被禁用，无法启用该用户，请启用机构后再次尝试!');
                    return $this->redirect ('/user/index?status=2');
                }
            }
        }


        if (!empty($model)) {
            $model->status = $status;
            $model->del_status = $del_status;
            $model->updated_at = date("Y-m-d H:i:s");
            if ($model->save(false)) {
                Yii::$app->session->setFlash('message','操作成功!');
                return $this->redirect ('/user/index?status=2');
            }
        }
        return $this->redirect('/user/index');
    }
    /**
     * 删除
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return String
     */
    public function actionDel()
    {
        $t    = intval(Yii::$app->request->get('t',1));
        $d    = intval(Yii::$app->request->get('d',1));
        $id   = intval(Yii::$app->request->get('id',0));
        if ($id<= 0) {
            return $this->redirect('/user/index');
        }
        if($d == 2){
            $del_status = 2;
        }else{
            $del_status = 1;
        }
        if($t == 1){
            $status = 2;
        }
        elseif($t == 2) {
            $status = 1;
        }
        else {
            $status = 0;
        }

        $where = [
            'id'  =>$id,
        ];
        $model = AdminUser::findOne($where);
        if($t ==2 ){ //
            $info = Department::getDepartmentInfo($model->department_id,1,0,1);
            if(!empty($info)){
                if($info[0]['status'] == 2){
                    Yii::$app->session->setFlash('message','该用户所属机构被禁用，无法启用该用户，请启用机构后再次尝试!');
                    return $this->redirect ('/user/index?status=2');
                }
            }
        }


        if (!empty($model)) {
            $model->status = $status;
            $model->del_status = $del_status;
            $model->updated_at = date("Y-m-d H:i:s");
            if ($model->save(false)) {
                Yii::$app->session->setFlash('message','操作成功!');
                return $this->redirect ('/user/index?status=2');
            }
        }
        return $this->redirect('/user/index');
    }

    /**
     * 禁用
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return String
     */
    public function actionBan()
    {
        $id   = intval(Yii::$app->request->get('id',0));
        if ($id<= 0) {
            return $this->redirect('/user/index');
        }

        $where = [
            'id'       =>$id,
            'status'   => 1,
//            'identity' => 0,
        ];
        $model = AdminUser::findOne($where);
        if (!empty($model)) {
            $oldModel = clone $model;
            $model->status = 2;
            $model->updated_at = date("Y-m-d H:i:s");
            if ($model->save(false)) {
                Yii::$app->session->setFlash('message','操作成功!');
                return $this->redirect ('/user/index');
            }
        }
        return $this->redirect('/user/index');
    }
    public function actionResetPwd() {
        $id = Yii::$app->request->get('id');
        $model = AdminUser::findOne(['id'=>$id]);
        $user = new UserForm();
        if(Yii::$app->request->post()){
              Yii::$app->response->format = Response::FORMAT_JSON;
              $id = Yii::$app->request->post('user_id');
              $password = Yii::$app->request->post('password');
              $model = AdminUser::findOne(['id'=>$id]);
              $model->password_hash = Yii::$app->security->generatePasswordHash($password);
              if ($model->save(false)) {
                    return BaseLogic::success([]);
              }
        }
        return $this->renderPartial('reset-pwd',['model' => $user,'id'=>$id]);
    }

    /**
     * 修改资料
     * @author chengjuanjuan<chengjuanjuan@c-ntek.com>
     * @date 2017-09-05
     */
    public function actionUpdateProfile ()
    {
        $id = Yii::$app->session['__id'];
        if ($id <= 0) {
            return $this->redirect('index');
        }
        $model = UserInfo::findOne(['user_id' => $id]);
        if (!$model) {
            return $this->redirect('/index/nofind');
        }
        $data['goodsClass'] = ServiceClass::getClass();
        
        if($model->load(Yii::$app->request->post())){
            $model->status = 0;
            if($model->save(false)) {
                Yii::$app->session->setFlash('message','修改成功，等待审核!');
                return  $this->redirect('/user/view-profile');
            }
        }else{
            return $this->render('update-profile',['model'=>$model,'data'=>$data]);
        }
    }
    
    /**
     * 资料展示
     */
    public function actionViewProfile() 
    {
        $id = Yii::$app->session['__id'];
        if ($id <= 0) {
            return $this->redirect('index');
        }
        $model = Manufactor::findOne(['user_id' => $id]);
        if (!$model) {
            return $this->redirect('/index/nofind');
        }
        $model->logo = Yii::$app->params['imageUrl'].'/'.$model->logo;
        $model->business_licence = Yii::$app->params['imageUrl'].'/'.$model->business_licence;
        //获取开通省份
        $province = Region::getOpenProvince();
        $model->province_name = Region::getCityName($model->province_id);
        $model->city_name = Region::getCityName($model->city_id);
        $model->district_name = Region::getCityName($model->district_id);
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect('view-profile');
            }
        }
        return $this->render('view-profile',
                    [ 
                        'model'    => $model,
                        'province' => $province,
                    ]);
    }
    /**
     * 个人资料修改
     * @return string
     */
    public function actionEditProfile()
    {
        $model = AdminUser::findOne(['id'=>$this->user->id]);

        if($model->load(Yii::$app->request->post())){
            if(isset($_FILES['file']) && $_FILES['file']['size']>0)
            {
                $upload = new Upload();
                $result = $upload->upImage('profile','file');
                if(isset($result['code']) && $result['code'] == 0){
                    $model->avatar = $result['url'];
                }
            }

            $model->updated_at = time();
            if($model->save())
            {
                Yii::$app->session->setFlash('message','修改成功!');
                return  $this->redirect('/user/edit-profile');
            }
        }
        $this->_view['model'] = $model;
        return $this->render('edit-profile');
    }

    /**
     * 无权限页
     * @author xi
     */
    public function actionNoAuthority()
    {
        return $this->render('no-authority');
    }

    /**
     * 短信发送
     * @return array
     */
    public function actionSendSms()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $status  = 0;
        $message = 'error';

        if(Yii::$app->request->isAjax)
        {
            $smsToken   = trim(Yii::$app->request->post('sms_token',''));
            $mobile     = trim(Yii::$app->request->post('mobile',''));

            if(Yii::$app->request->getReferrer())
            {
                if(Yii::$app->session->get('sms_token') != $smsToken)
                {
                    return [
                        'status'  => $status,
                        'message' => '页面已过期 ，请刷新重试',
                    ];
                }
                else if(!preg_match('/^1[2|3|4|5|6|7|8|9]\d{9}$/', $mobile)){
                    return [
                        'status'  => $status,
                        'message' => '手机格式不正确',
                    ];
                }

                $model = AdminUser::findOne(['mobile'=>$mobile,'status'=>1]);
                if(!$model){
                    return [
                        'status'  => $status,
                        'message' => '手机号不存在',
                    ];
                }

                $randCode = rand(100000,999999);
                $url = Yii::$app->params['sms.uservices.cn']."/notice/sms-single";
                $postData = [
                    'message' => json_encode(['code'=>$randCode]),
                    'mobile'  => $mobile,
                    'source'  => 'business',
                    'type'    => 'businessForgetPassword'
                ];
                $jsonStr = Helper::curlPost($url,$postData);
                $result = json_decode($jsonStr,true);
                if(isset($result['code']) && $result['code'] == 10000)
                {
                    Yii::$app->cache->set( $mobile , $randCode , 600 );
                    $status = 1;
                    $message = '发送成功';
                }elseif (isset($result['code']) && $result['code'] != 10000) {
                    $message = $result['errorMsg'];
                }else{
                    $message = '获取失败，请重新获取';
                }
            }
            else {
                $message = '页面已过期 ，请刷新重试';
            }
        }
        return [
            'status'  => $status,
            'message' => $message,
        ];
    }

    /**
     * 修改密码
     * @author liwenyong
     * @date   2017-08-17
     * @return String
     */
    public function actionModifyPassword()
    {
        $model = Yii::$app->user->getIdentity();
        $user = new UserForm();

        if ($user->load(Yii::$app->request->post()))
        {
            $pwd = $model->validatePassword($user->old_pwd);

            if (!$pwd) {
                $user->addError('old_pwd','你的原密码不正确');
            }
            $errors = $user->getErrors();
            if (empty($errors))
            {
                $model->password_hash = Yii::$app->security->generatePasswordHash($user->new_pwd);
                if ($model->save()) {
                    Yii::$app->session->setFlash('message','修改密码成功');
                    return $this->redirect('/user/modify-password');
                }
            }
        }
        return $this->render('modify-password',['model'=>$user]);
    }
    /**
     * 忘记密码
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return string
     */
    public function actionForgetPwd()
    {
        $this->layout = 'main-login';
        $model = new AdminUser();
        $postData = Yii::$app->request->post();
        if($model->load($postData))
        {
            $department_id = '';
            $model->username = 'xxx';
            if($model->validate())
            {
                if(trim($model->mobile) == ''){
                    $model->addError('mobile','手机号不能为空');
                }
                if(trim($model->vcode) == ''){
                    $model->addError('vcode','验证码不能为空');
                }
                if($model->password == ''){
                    $model->addError('password','新密码不能为空');
                }
                if($model->verify_pwd == ''){
                    $model->addError('verify_pwd','验证新密码不能为空');
                }

                if($model->password != $model->verify_pwd){
                    $model->addError('verify_pwd','验证新密码与设置新密码不一致');
                }
                //账号存在多个系统的时候的department_id
                $datas = AdminUser::getAllList(['mobile'=>$model->mobile,'del_status'=>1,'status'=>1]);
                if(count($datas) == 1){
                    $department_id = $datas[0]['department_id'];
                }else{
                    $department_id = $postData['AdminUser']['department_id'];
                }
                //验证此手机号码是否注册多测系统账号
                $is_check = AdminUser::checkDepartment($model->mobile);
                if($department_id && $is_check){
                    $newModel = AdminUser::findOne(['mobile'=>$model->mobile,'status'=>1,'department_id'=>$department_id]);
                }else{
                    $newModel = AdminUser::findOne(['mobile'=>$model->mobile,'status'=>1]);
                }
                if(!$newModel){
                    $model->addError('mobile','手机号不存在');
                }
                if(Yii::$app->cache->get( $model->mobile) != trim($model->vcode) ){
                    $model->addError('vcode','验证码输入不正确');
                }

                if(!$model->hasErrors())
                {
                    $newModel->password_hash = Yii::$app->security->generatePasswordHash($model->password);
                    $newModel->updated_at    = time();
                    $newModel->auth_key      = uniqid();
                    if($newModel->save()){
                        Yii::$app->session->setFlash('alertMsg','重置成功');
                        return $this->redirect('/site/login');
                    }
                    else {
                        Yii::$app->session->setFlash('alertMsg','重置成功');
                    }
                    return $this->redirect('/user/forget-pwd');
                }
            }
        }

        //设置token
        $sms_token = uniqid();
        Yii::$app->session->set('sms_token', $sms_token);
        $this->_view['sms_token'] = $sms_token;
        $this->_view['model'] = $model;
        return $this->render('forget_pwd');
    }

    /**
     * 根据机构部门等获取用户信息,用于添加合同获取
     * sxz 2018-7-30
     *
     */
    public function actionGetUserList()
    {

        $searchValue = strip_tags(trim(Yii::$app->request->get('searchValue','')));
        $status      = strip_tags(trim(Yii::$app->request->get('status',1)));
        $parentId    = intval(Yii::$app->request->get('pid',0));
        $type        = intval(Yii::$app->request->get('type',2));
        $page        = intval(Yii::$app->request->get('page',1));
        $pageSize    = 100;
        $d_type      = 0;
        //获取当前用户信息
        $user_info = BaseLogic::getLoginUserInfo();
        if(Yii::$app->request->isAjax)
        {
            $this->layout = false;
            Yii::$app->response->format = Response::FORMAT_JSON;
            // 获取选取导航机构下的用户
            if($parentId > 0){
                if($this->dataAuthId == 4){
                    $low_info = department::getDepartmentInfo($parentId,1,0,1);
                }else{
                    //获取机构类型
                    $de = Department::findOne(['id'=>$parentId]);
                    if($de->type == 1 && $this->dataAuthId != 2){
                        //下级经销商
                        $next_parntId = Department::getDirectCompanyIdById($parentId);
                        if($next_parntId != $this->directCompanyId){
                            $low_info = department::getDepartmentChildren($parentId,1);
                        }else{
                            $low_info = department::getDepartmentChildren($parentId);
                        }
                    }else{
                        $low_info = department::getDepartmentChildren($parentId,2);
                    }
                }
                foreach ($low_info as $val){
                    $low_ids[] = $val['id'];
                }
            }
            if($this->dataAuthId == 1){
                $low_where = "id =".$user_info->id." and status= ".$status." and del_status=1";
            }else{
                if($this->departmentId == $parentId){
                    $low_where = "(department_id = ".$parentId." or ((department_type = 1 and is_direct_create = 1 and department_id != ".$parentId." or department_type = 3) and department_id in (".implode(",",$low_ids)."))) and status = ".$status." and del_status = 1";
                }else{
                    $low_where = "((department_id = ".$parentId." and identity = 1) or (department_id = ".$parentId." and department_type = 1 and is_direct_create = 1) or ((department_type = 1 and is_direct_create = 1 and department_id != ".$parentId." or department_type = 3) and department_id in (".implode(",",$low_ids)."))) and status = ".$status." and del_status = 1";
                }
                // $low_where = "department_id = ".$parentId." or (department_type = 1 and is_direct_create = 1 and department_id != ".$parentId.") and department_id in (".implode(",",$low_ids).") and status=1 and del_status=1";
                // $low_where[] =['in','department_id' ,$low_ids];
            }
            if($searchValue != ''){
                $low_where .= " and (mobile like '%".$searchValue."%' or username like '%".$searchValue."%')";
                //$low_where[] = (['or',['like','mobile',$searchValue],['like','username',$searchValue]]);
            }
            $data = AdminUser::getList($low_where,'created_at desc',$page,$pageSize);

            if($data['list'])
            {
                foreach ($data['list'] as $key=>$val)
                {
                    $department =  Department::findOne(['id'=>$val['department_id']]);
                    $data['list'][$key]['department'] =$department->name;
                }
            }
            $str = '<option value="">请选择销售人</option>';
            if($data['list'])
            {
                foreach ($data['list'] as $key=>$val) {
                    $str = $str . '<option value="'.$val['id'].'">'.$val['username'].'/'.$val['mobile'].'</option>';
                }
            }
            return $str;

        }
    }
}


<?php

namespace webapp\controllers;


use Yii;
use webapp\logic\BaseLogic;
use webapp\models\BaseModel;
use yii\web\Controller;
use common\helpers\Paging;
use webapp\logic\PurchaseLogic;
use common\models\Region;
use common\models\Purchase;
use common\models\PurchaseDetail;
use common\helpers\CreateNoRule;
use yii\web\Response;
/**
 *@desc:供货商管理
 *
 */

class PurchaseController extends BaseController
{

    /**
     * 函数用途描述:采购单列表
     * @date: 2018年3月22日 下午14:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    //待审批
    public function actionIndex()
    {
        $result = PurchaseLogic::getList();
        //print_r($result);die;
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }
        return $this->render('index',[
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params'],
            'purchaseTypeList'   =>$result['purchaseTypeList'],
            'purchaseStatusList'   =>$result['purchaseStatusList'],
            'auditStatusList'   =>$result['auditStatusList']
        ]);
    }
    //待入库
    public function actionWaitCheckIn()
    {
        $result = PurchaseLogic::getList();
        $pageHtml = '';
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }
        $shipStatusList = ['1'=>'待发货','2'=>'已发货','3'=>'已收货'];
        return $this->render('wait-check-in',[
            'data'                  =>$result['list'],
            'pageHtml'              =>$pageHtml,
            'params'                =>$result['params'],
            'purchaseTypeList'      =>$result['purchaseTypeList'],
            //'purchaseStatusList'   =>$result['purchaseStatusList'],
            'mixStatusList'         =>$result['mixStatusList'],
            'auditStatusList'       =>$result['auditStatusList'],
            'shipStatusList'        =>$shipStatusList
        ]);

    }
    //入库完毕
    public function actionFinished()
    {
        $result = PurchaseLogic::getList();
        $pageHtml = '';
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }
        return $this->render('finished',[
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params'],
            'purchaseTypeList'   =>$result['purchaseTypeList'],
            'purchaseStatusList'   =>$result['purchaseStatusList'],
            'auditStatusList'   =>$result['auditStatusList']
        ]);

    }
    //未通过
    public function actionNotPass()
    {
        $result = PurchaseLogic::getList();
        $pageHtml = '';
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }
        return $this->render('not-pass',[
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params'],
            'purchaseTypeList'   =>$result['purchaseTypeList'],
            'purchaseStatusList'   =>$result['purchaseStatusList'],
            'auditStatusList'   =>$result['auditStatusList']
        ]);

    }
    //已取消
    public function actionCancelled()
    {
        $result = PurchaseLogic::getList();
        $pageHtml = '';
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }
        return $this->render('cancelled',[
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params'],
            'purchaseTypeList'   =>$result['purchaseTypeList'],
            'purchaseStatusList'   =>$result['purchaseStatusList'],
            'auditStatusList'   =>$result['auditStatusList']
        ]);

    }

    /***
     * @return string
     * 已作废
     * 2018d-10-10 sxz
     */
    public function actionInvalid()
    {
        $result = PurchaseLogic::getList();
        $pageHtml = '';
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }
        return $this->render('invalid',[
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params'],
            'purchaseTypeList'   =>$result['purchaseTypeList'],
            'purchaseStatusList'   =>$result['purchaseStatusList'],
            'auditStatusList'   =>$result['auditStatusList']
        ]);

    }

    /**
     * 函数用途描述:添加/编辑采购单
     * @date: 2018年3月21日 下午3:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionAdd()
    {
        $infos = [];
        $no = yii::$app->request->get('no','');
        //编辑时获取数据
        if(!empty($no)){
            //获取采购单详情
            $infos = PurchaseLogic::getDetail($no,2);
        }else{
            $infos['purchaseInfo']['no'] = CreateNoRule::getNo('CG');   //获取生成的采购编号
            $infos['purchaseInfo']['purchase_user_id'] = BaseLogic::getLoginUserId(); //获取采购人id
            $infos['purchaseInfo']['purchase_user_name'] = Yii::$app->user->getIdentity()->username ; //获取采购人姓名
            $infos['purchaseDetail'] = [] ;
        }
        if (Yii::$app->request->isPost){
            //判断是否存在id 区分添加和修改
            $id = intval(yii::$app->request->post('id',0));
            if ($id != 0) {
                $res = PurchaseLogic::edit();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/purchase/index');
                //return json_encode($res);
            }else{
                $res = PurchaseLogic::add();
                //return json_encode($res);
                if($res['success'] == false){
                    Yii::$app->session->setFlash('message',$res['message']);
                    return $this->redirect('/purchase/add');
                }
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/purchase/index');
            }
        }
        $purchaseTypeList = PurchaseLogic::getPurchaseTypeList(); //获取采购类型
        //print_r($infos);die;
        return $this->render('add',[
            'data'=>$infos,
            'purchaseTypeList'   =>$purchaseTypeList
        ]);
    }
    /**
     * 函数用途描述:编辑采购单
     * @date: 2018年3月21日 下午3:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionEdit()
    {
        $infos = [];
        $no = yii::$app->request->get('no','');
        //编辑时获取数据
        if(!empty($no)){
            //获取采购单详情
            $infos = PurchaseLogic::getDetail($no,2);
        }else{
            $infos['purchaseInfo']['no'] = CreateNoRule::getNo('CG');   //获取生成的采购编号
            $infos['purchaseInfo']['purchase_user_id'] = BaseLogic::getLoginUserId(); //获取采购人id
            $infos['purchaseInfo']['purchase_user_name'] = Yii::$app->user->getIdentity()->username ; //获取采购人姓名
            $infos['purchaseDetail'] = [] ;
        }
        if (Yii::$app->request->isPost){
            //判断是否存在id 区分添加和修改
            $id = intval(yii::$app->request->post('id',0));
            if ($id != 0) {
                $res = PurchaseLogic::edit();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/purchase/index');
                //return json_encode($res);
            }else{
                $res = PurchaseLogic::add();
                //return json_encode($res);
                if($res['success'] == false){
                    Yii::$app->session->setFlash('message',$res['message']);
                    return $this->redirect('/purchase/add');
                }
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/purchase/index');
            }
        }
        $purchaseTypeList = PurchaseLogic::getPurchaseTypeList(); //获取采购类型
        //print_r($infos);die;
        return $this->render('add',[
            'data'=>$infos,
            'purchaseTypeList'   =>$purchaseTypeList
        ]);
    }
    //验证信息重复性
    public function actionVerifyData(){
        if (yii::$app->request->isPost) {
            $post = yii::$app->request->post();
            $no = $post['str'];
            $id = $post['id'];
            //print_r($post['str']);exit;
            if($no != ''){
                //验证数据
                $res = Supplier::getOne(['no'=>$no]);
                if($id){
                    if($res && $id != $res['id']){
                        return json_encode(['msg'=>'供货商编号已存在！','code'=>'20002'],true);
                    }
                }else{
                    if($res){
                        return json_encode(['msg'=>'供货商编号已存在！','code'=>'20002'],true);
                    }
                }
            }

            return json_encode(['msg'=>'ok','code'=>'200']);
        }
    }
    /**
     * 函数用途描述:取消采购单
     * @date: 2018年3月26日 下午14:23:05
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionCancel()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $no       = trim(Yii::$app->request->get('no',''));
        $opinionContent = htmlspecialchars(trim(Yii::$app->request->get('audit_suggest','')));
        if($no == ''){
            return BaseLogic::error('采购单号不能为空',20002);
        }
        if($opinionContent == ''){
            return BaseLogic::error('取消意见不能为空',20003);
        }
        return PurchaseLogic::cancel($no,$opinionContent);
    }
    /**
     * 函数用途描述:采购单详情
     * @date: 2018年3月27日 下午17:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionDetail()
    {
        $infos = [];
        $id = yii::$app->request->get('id','');
        $no = yii::$app->request->get('no','');
        $type = yii::$app->request->get('t','');
        if($id){
            $infos = PurchaseLogic::getDetail($id,1);
        }
        if($no){
            $infos = PurchaseLogic::getDetail($no,2);
        }
        //获取采购类型数据
        $purchaseTypeList = PurchaseLogic::getPurchaseTypeList();
        //获取采购状态数据
        $purchaseStatusList = PurchaseLogic::getPurchaseStatusList();
        $mixStatusList = ['2'=>'部分入库','3'=>'待入库',];  //待入库选项卡时需要区分的采购状态
        //获取审批状态数据
        $auditStatusList = PurchaseLogic::getAuditStatusList();
        $shipStatusList = ['1'=>'待发货','2'=>'已发货','3'=>'已收货'];
        //退货状态数据
        $returnStatusList = ['1'=>'待审批','2'=>'执行中','3'=>'执行完毕','4'=>'审批未通过','5'=>'已终止'];
        //发货状态
        $sendStatusList = ['1'=>'运输中','2'=>'已收货'];
        //print_r($infos);die;
        return $this->render('view',[
            'data'                  => $infos,
            'purchaseTypeList'      => $purchaseTypeList,
            'purchaseStatusList'    => $purchaseStatusList,
            'auditStatusList'       => $auditStatusList,
            'type'                  => $type,
            'shipStatusList'        => $shipStatusList,
            'returnStatusList'      => $returnStatusList,
            'sendStatusList'        => $sendStatusList
        ]);
    }
    /**
     * 函数用途描述:采购单同意/拒绝操作
     * @date: 2018年3月28日 下午13:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionSetAuditStatus()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $no       = trim(Yii::$app->request->get('no',''));
        $status   = trim(Yii::$app->request->get('status',''));
        $opinionContent = htmlspecialchars(trim(Yii::$app->request->get('audit_suggest','')));
        $a = Yii::$app->request->get();
        if($no == ''){
            return BaseLogic::error('采购单号不能为空',20002);
        }
        if($status == ''){
            return BaseLogic::error('操作状态不能为空',20003);
        }
        return PurchaseLogic::SetAuditStatus($no,$status,$opinionContent);
    }
    /**
     * 函数用途描述:采购单终止/作废操作
     * @date: 2018年10月10日 下午14:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionSetStop()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id       = intval(Yii::$app->request->get('id',0));
        $status    = intval(Yii::$app->request->get('status',0)); // 6 采购终止 5 作废
        if($id == ''){
            return BaseLogic::error('采购单错误',20002);
        }
        $res = PurchaseLogic::SetStop($id,$status);
        if($res['code'] == '200'){
            Yii::$app->session->setFlash('message','操作成功!');
            return $this->redirect ('/purchase/wait-check-in');
        }
        return $this->redirect('/purchase/wait-check-in');
    }
    /**
     * 函数用途描述:采购单作废操作
     * @date: 2018年10月10日 下午14:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionSetInvalid()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id       = intval(Yii::$app->request->get('id',0));
        $status   = 5;   // 5 采购单作废
        if($id == ''){
            return BaseLogic::error('采购单错误',20002);
        }
        $res = PurchaseLogic::SetStop($id,$status);
        if($res['code'] == '200'){
            Yii::$app->session->setFlash('message','操作成功!');
            return $this->redirect ('/purchase/wait-check-in');
        }
        return $this->redirect('/purchase/wait-check-in');
    }



}


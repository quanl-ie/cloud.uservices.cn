<?php

namespace webapp\controllers;

use webapp\logic\SysConfigLogic;
use Yii;

/**
 *@desc:仓储管理开关
 *
 */

class SysConfigController extends BaseController
{
    public function actionIndex()
    {
        $data = SysConfigLogic::BeforeList();
        if ($data) {
            $this->_view['sys']    = $data['sys'];
            $this->_view['option'] = $data['option'];
        }
        
        if (Yii::$app->request->isPost) {
            $res = SysConfigLogic::edit();
            Yii::$app->session->setFlash('sys_config_message',$res['message']);
            return $this->redirect('index');
        }

        return $this->render('index');
    }
}


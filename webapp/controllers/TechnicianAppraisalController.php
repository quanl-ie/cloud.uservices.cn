<?php
namespace webapp\controllers;
use common\models\WorkAppraisalDict;
use webapp\models\Technician;
use webapp\models\TechnicianAppraisal;
use yii;
/**
 * @desc:技师评价统计
 */
class TechnicianAppraisalController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionImport()
    {
        $technicianName = trim(Yii::$app->request->get('technician_name',''));
        $date = trim(Yii::$app->request->get('month',''));
        $timestamp=time();
        $month=date('Ym',strtotime(date('Y',$timestamp).'-'.(date('m',$timestamp)-1).'-01'));
        if($technicianName)
        {
            $technician = Technician::findOneByAttributes(['like','name',$technicianName],'id');
            if(empty($technician)){
                $technicianId = -1;
            }else{
                $technicianId = $technician['id'];
            }
            $where['technician_id'] = $technicianId;
        }
        if($date){
            $month = date("Ym",strtotime($date));
        }
        $where['month'] = $month;
        //默认为公司权限
        $where['direct_company_id'] = $this->directCompanyId;
        $result['tags'] = WorkAppraisalDict::getAllTags(['direct_company_id'=>$this->directCompanyId]);
        $result['data'] = TechnicianAppraisal::importData($where);
        self::exportExcel($result,'技师评价统计',$month);
    }

    /**
     * 导出 excel
     * @param $result
     * @param $xlsName
     * @param $month
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     * @author xi
     */
    private static function exportExcel($result,$xlsName,$month)
    {
        $exportData = $result['data'];
        $tags = $result['tags'];

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()  //获得文件属性对象，给下文提供设置资源
            ->setCreator( "Maarten Balliauw")                 //设置文件的创建者
            ->setLastModifiedBy( "Maarten Balliauw")          //设置最后修改者
            ->setTitle( "Office 2007 XLSX Test Document" )    //设置标题
            ->setSubject( "Office 2007 XLSX Test Document" )  //设置主题
            ->setDescription( "Test document for Office 2007 XLSX, generated using PHP classes.") //设置
            ->setKeywords( "office 2007 openxml php")//设置标记
            ->setCategory( "Test result file"); //设置类别


        $objPHPExcel->getActiveSheet()->setCellValue("A1", "技师");
        $objPHPExcel->getActiveSheet()->mergeCells("A1:A2");

        $objPHPExcel->getActiveSheet()->setCellValue("B1", "综合评分");
        $objPHPExcel->getActiveSheet()->mergeCells("B1:B2");

        //好评，中评，差评
        $totalTitle = 0;
        $countColumn = 0;
        foreach ($tags as $key=>$val)
        {
            $ceil = ord('C') + $totalTitle;

            $objPHPExcel->getActiveSheet()->setCellValue(chr($ceil).'1' , $key == 1?'好评':($key == 2?'中评':'差评'));
            $objPHPExcel->getActiveSheet()->mergeCells(chr($ceil)."1:" . chr($ceil + count($val)-1) . "1");

            $totalTitle+=count($val);
            foreach ($val as $k=>$v){
                $objPHPExcel->getActiveSheet()->setCellValue(chr($ceil+$k).'2' , $v);
            }

            $countColumn+=count($val);
        }

        for($i = 0; $i< $countColumn+2 ;$i++){
            $objPHPExcel->getActiveSheet()->getColumnDimension(chr(ord('A')+$i))->setWidth(12);
        }

        $objPHPExcel->getActiveSheet()->getStyle('A1:Z1000')->applyFromArray([
            'alignment' =>[
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
            ]
        ]);

        $endColumn = chr(ord('A')+$countColumn+1);
        $objPHPExcel->getActiveSheet()->getStyle('A1:'. $endColumn . '2' )->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:'. $endColumn . '2' )->getFill()->getStartColor()->setRGB('f7f8f9');


        $i = 3;
        foreach ($exportData as $key=>$val)
        {
            foreach ($val as $k=>$v){
                $objPHPExcel->getActiveSheet()->setCellValueExplicit(chr(ord('A')+$k).$i , $v,\PHPExcel_Cell_DataType::TYPE_STRING);
            }
            $i++;
        }

        //得到当前活动的表
        $objActSheet = $objPHPExcel->getActiveSheet();
        // 给当前活动的表设置名称
        $objActSheet->setTitle($xlsName);
        //生成2007excel格式的xlsx文件
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$xlsName.'.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save( 'php://output');
        exit;
    }
}
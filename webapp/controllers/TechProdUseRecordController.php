<?php

namespace webapp\controllers;


use webapp\models\TechProdUseRecord;
use Yii;
use webapp\logic\BaseLogic;
use webapp\models\BaseModel;
use yii\web\Controller;
use common\helpers\Paging;
use webapp\logic\TechProdUseRecordLogic;
use common\models\Region;
use common\helpers\CreateNoRule;
use yii\web\Response;
/**
 *@desc:技师备件使用记录
 *
 */

class  TechProdUseRecordController extends BaseController
{

    /**
     * 函数用途描述:技师备件使用详情
     * @date: 2018年4月2日 下午15:30:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    //待审批
    public function actionIndex()
    {
        $result = TechProdUseRecordLogic::getList();
        //print_r($result);die;
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?technician_id='.$result['params']['technician_id'].'&page=');
        }else{
            $pageHtml = '';
        }
        return $this->render('index',[
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params']
        ]);
    }



}


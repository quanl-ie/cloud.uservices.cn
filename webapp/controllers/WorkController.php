<?php

namespace webapp\controllers;

use common\models\ServiceType;
use webapp\models\ManufactorType;
use Yii;
use common\helpers\Helper;
use webapp\logic\WorkLogic;
use webapp\models\TechStorage;
use webapp\logic\OrderLogic;
use common\helpers\Paging;
use webapp\logic\BaseLogic;
use yii\web\Response;
use webapp\models\BaseModel;
use webapp\models\WorkForm;
use common\models\SysConfig;
use webapp\logic\ProductLogic;
use common\models\ServiceClass;
use common\models\CostPayType;
use webapp\models\CostLevelItem;

class WorkController extends BaseController
{
    
    /**
     * 待指派工单
     * @author xi
     * @date 2017-12-18
     */
    public function actionWaitAssign()
    {
        $brforeList = ProductLogic::getBeforeList();
        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }
        if(isset($_GET['class_id'])){
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids']  = implode(',',$classId);
            }else{
                $_GET['class_ids']  = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',',$classId);
            }
            $_GET['class_name'] = implode(',',ServiceClass::getName($classId));
        }
        $_GET['status'] = 1;
        $result = WorkLogic::getList();

        $pageHtml = '';
        if($result['success'] == true && $result['data']['totalPage'] >1){
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr.'page=');
        }
        
        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        //将列表按照预约时间正序排列，时间近的在前 2018-5-8
        if(isset($result['data']['list']) && $result['data']['list']){
            $sort = array_column($result['data']['list'], 'plan_sort_time');
            array_multisort($sort, SORT_ASC, $result['data']['list']);
        }
        $this->_view['statusArr']= OrderLogic::getOrderStatus();
        $this->_view['data']     = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = $workTypeArr;
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        return $this->render('wait-assign');
    }

    /**
     * 待服务的工单
     * @author xi
     * @date 2018-2-6
     */
    public function actionWaitService()
    {
        $brforeList = ProductLogic::getBeforeList();
        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }
        if(isset($_GET['class_id'])){
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids']  = implode(',',$classId);
            }else{
                $_GET['class_ids']  = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',',$classId);
            }
            $_GET['class_name'] = implode(',',ServiceClass::getName($classId));
        }
        $_GET['status']   = [2,6];

        $_GET['is_first'] = 1;

        $result = WorkLogic::getList();

        $pageHtml = '';
        if($result['success'] == true && $result['data']['totalPage'] >1){
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr.'page=');
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        //将列表按照预约时间正序排列，时间近的在前 2018-5-10
        if(isset($result['data']['list']) && $result['data']['list'])
        {
            $sort = array_column($result['data']['list'], 'plan_sort_time');
            array_multisort($sort, SORT_ASC, $result['data']['list']);
        }
        $this->_view['statusArr']= OrderLogic::getOrderStatus();
        $this->_view['data']     = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = $workTypeArr;
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        return $this->render('wait-service');
    }

    /**
     * 服务中的工单
     * @author xi
     * @date 2018-2-6
     */
    public function actionInService()
    {
        $brforeList = ProductLogic::getBeforeList();
        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }
        if(isset($_GET['class_id'])){
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids']  = implode(',',$classId);
            }else{
                $_GET['class_ids']  = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',',$classId);
            }
            $_GET['class_name'] = implode(',',ServiceClass::getName($classId));
        }
        $_GET['status'] = 3;
        $_GET['is_first'] = [1,2];

        $result = WorkLogic::getList();

        $pageHtml = '';
        if($result['success'] == true && $result['data']['totalPage'] >1){
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr.'page=');
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        $this->_view['statusArr']= OrderLogic::getOrderStatus();
        $this->_view['data']     = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = $workTypeArr;
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        return $this->render('in-service');
    }

    /**
     * 待收款的工单
     * @author xi
     * @date 2018-6-21
     */
    public function actionWaitMoney()
    {
        $brforeList = ProductLogic::getBeforeList();
        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }
        if(isset($_GET['class_id'])){
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids']  = implode(',',$classId);
            }else{
                $_GET['class_ids']  = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',',$classId);
            }
            $_GET['class_name'] = implode(',',ServiceClass::getName($classId));
        }
        $_GET['status'] = 5;
        $_GET['is_first'] = [1,2];

        $result = WorkLogic::getList();

        $pageHtml = '';
        if($result['success'] == true && $result['data']['totalPage'] >1){
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr.'page=');
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        $this->_view['statusArr']= OrderLogic::getOrderStatus();
        $this->_view['data']     = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = $workTypeArr;
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        return $this->render('wait-money');
    }

    /**
     * 已完的工单
     * @author xi
     * @date 2018-2-6
     */
    public function actionFinsh()
    {
        $brforeList = ProductLogic::getBeforeList();
        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }
        if(isset($_GET['class_id'])){
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids']  = implode(',',$classId);
            }else{
                $_GET['class_ids']  = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',',$classId);
            }
            $_GET['class_name'] = implode(',',ServiceClass::getName($classId));
        }
        $_GET['status'] = 4;
        $_GET['is_first'] = [1,2];

        $result = WorkLogic::getList();

        $pageHtml = '';
        if($result['success'] == true && $result['data']['totalPage'] >1){
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr.'page=');
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        $this->_view['statusArr']= OrderLogic::getOrderStatus();
        $this->_view['data']     = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = $workTypeArr;
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        return $this->render('finsh');
    }

    /**
     * 已取消工单
     * @author xi
     * @date 2018-2-6
     */
    public function actionCancel()
    {
        $brforeList = ProductLogic::getBeforeList();
        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }
        if(isset($_GET['class_id'])){
            $classId = $_GET['class_id'];
            //类目搜索条件
            if (is_array($classId)) {
                $_GET['class_ids']  = implode(',',$classId);
            }else{
                $_GET['class_ids']  = $classId;
            }
            if (!is_array($classId)) {
                $classId = explode(',',$classId);
            }
            $_GET['class_name'] = implode(',',ServiceClass::getName($classId));
        }
        $_GET['cancel_status'] = 2;

        $result = WorkLogic::getList();

        $pageHtml = '';
        if($result['success'] == true && $result['data']['totalPage'] >1){
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr.'page=');
        }

        //服务类型
        $workTypeArr = ManufactorType::getServiceType();

        $this->_view['statusArr']= OrderLogic::getOrderStatus();
        $this->_view['data']     = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = $workTypeArr;
        $this->_view['class'] = $brforeList['class'];  //产品类目数据
        return $this->render('cancel');
    }

    /**
     * 关闭工单
     * @author xi
     * @date 2018-2-6
     */
    public function actionClose()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $workNo = trim(Yii::$app->request->get('work_no',''));
        $reason = trim(Yii::$app->request->get('cancel_reason',''));

        if($workNo == ''){
            return BaseLogic::error('工单号不能为空',20002);
        }
        if($reason == ''){
            return BaseLogic::error('关闭原因不能为空',20003);
        }

        return WorkLogic::close($workNo,$reason);
    }

    /**
     * 修改工单时间
     * @author sxz
     * @date 2018-2-7
     */
    public function actionChangeTime()
    {
        $workNo = trim(Yii::$app->request->get('work_no',''));
        if(Yii::$app->request->isPost)
        {
            $workNo = trim(Yii::$app->request->post('work_no',''));
    
            //服务时间拼接
            $selectDate   =  Yii::$app->request->post("select_date",'');
            $planTimeType =  Yii::$app->request->post("plan_time_type",'');
            $setTime      =  Yii::$app->request->post("set_time",'');
            
            if($workNo == ''){
                return json_encode(BaseLogic::error('工单号不能为空',20001));
            }
            if($selectDate == ''){
                return json_encode(BaseLogic::error('请选择日期',20002));
            }
            if($planTimeType == ''){
                return json_encode(BaseLogic::error('请选择服务时间类型',20003));
            }
            
            if($planTimeType == 5 && $setTime == ''){
                return json_encode(BaseLogic::error('请选择具体的服务时间',20004));
            }
            
            $planTime     =  Helper::changeTime($planTimeType,$selectDate,$setTime);
            
            $res = WorkLogic::ChangeTime($workNo,$planTime,$planTimeType);
            return json_encode($res);
        }
        
        //获取时间
        $setTimeType = Helper::setTimeType();
        $setTime = Helper::setTime();
            
        return $this->renderPartial('change-time',
            [
                'work_no'     => $workNo,
                'setTimeType' => $setTimeType,
                'setTime'     => $setTime
            ]
        );
    }
    /**
     * 选择时间
     * @author sxz
     * @date 2018-3-7
     */
    public function actionSelectTime(){
        $workNo = trim(Yii::$app->request->get('work_no',''));
        return $this->renderPartial('select-time',['work_no' => $workNo]);
    }

    /**
     * 查看回访信息
     * @author xi
     * @date 2018-2-7
     */
    public function actionViewVisit()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $workNo       = trim(Yii::$app->request->get('work_no',''));

        if($workNo == ''){
            return BaseLogic::error('工单号不能为空',20002);
        }

        return WorkLogic::viewVisit($workNo);
    }

    /**
     * 对服务中的工单进行完成服务操作 set-finish
     * @author sxz
     * @date 2018-2-7
     */
    public function actionSetFinish()
    {
        //类型 1 工单号, 2 订单号
        $type   = intval(Yii::$app->request->get('type',1));
        $workNo = trim(Yii::$app->request->get('work_no',''));
        $flag   = intval(Yii::$app->request->get('flag',1)); //标记是否是本身服务机构 1 是2否

        //质保状态
        $amount_type = BaseModel::getAmountType();
        $model = new WorkForm();


        if(Yii::$app->request->isPost)
        {
            $type         = intval(Yii::$app->request->post('type',1));
            $workNo       = trim(Yii::$app->request->post('work_no',''));       //工单号码
            $serviceRecord= trim(Yii::$app->request->post('service_record',''));  //服务记录
            $workImg      = Yii::$app->request->post('work_img','');            //服务工单
            $sceneImg     = Yii::$app->request->post('scene_img','');           //现场拍照
            //故障图片
            $file = Yii::$app->request->post("WorkForm",'');

            if ($file['work_img']) {
                $workImg = implode(',', $file['work_img']);
            }
            if ($file['scene_img']) {
                $sceneImg = implode(',', $file['scene_img']);
            }
            if($workNo == ''){
                return json_encode(BaseLogic::error('工单号不能为空',20002));
            }
            $res = WorkLogic::setFinish($type,$workNo,$workImg,$sceneImg,$serviceRecord,$flag);
            if(isset($res['work_no'])) {
                return $this->redirect('/order/index');
            }
            else {
                Yii::$app->session->setFlash('message',$res['message']);
            }
        }
        return $this->render('set-finish',[
            'type'        => $type,
            'work_no'     => $workNo,
            'amount_type' => $amount_type,
            'model'       =>$model
        ]);
    }
    /**
     * 工单详情
     * @author sxz
     * @date 2018-2-8
     */
    public function actionView()
    {
        $res = $info = [];
        $info = WorkLogic::getWorkDetail();
        $res['data'] = isset($info['data'])?$info['data']:[];
        return $this->render('view',[
            'result'       => $res
        ]);
    }
    /**
     * 工单详情中的查看凭证
     * @author sxz
     * @date 2018-2-8
     */
    public function actionViewVoucher()
    {
        $id     = Yii::$app->request->get('id','');
        $department_id = Yii::$app->request->get('department_id','');
        $data = [];
        $info = workLogic::getCostDetail($id,$department_id);
        if(isset($info['success']) && $info['success'] == 1 ){
            $data = $info['data'];
        }
        return $this->renderPartial('view-voucher',[
            'data'       => $data
        ]);
    }
    /**
     * 工单预约下次上门
     * @author sxz
     * @date 2018-2-8
     */
    public function actionAdd()
    {
        $order_no= trim(Yii::$app->request->get('order_no',''));
        //获取订单详情
        $data  = [];
        $data['service_flow'] = [];
        $data['order_no'] = $order_no;
        $info = WorkLogic::getDetail();
        if($info['data']){
            $data = $info['data'];
            $data['service_flow'] = WorkLogic::getServiceFlow($info['data']['work_type']);//获取服务流程
        }
        if(Yii::$app->request->isPost)
        {
            //调用逻辑层
            $result = WorkLogic::add();
            if($result['code'] == 200)
            {
                Yii::$app->session->setFlash('message','提交成功');
                return $this->redirect('/order/wait-assign');
            }
            else {
                Yii::$app->session->setFlash('message',$result['message']);
            }
        }
        //获取时间
        $setTimeType = Helper::setTimeType();
        $setTime = Helper::setTime();
        
        return $this->render('add',[
            'data'       => $data,
            'setTimeType' =>$setTimeType,
            'setTime'     =>$setTime
        ]);
    }
    /**
     * 添加,编辑，收费项目
     * @author sxz
     * @date 2018-2-8
     */
    public function actionAddWorkCost()
    {
        $cost_id    = trim(Yii::$app->request->get('cost_id',''));  //工单中的收费项目id,编辑时候需要
        $work_no    = trim(Yii::$app->request->get('work_no',''));  //工单号码
        $department_id    = trim(Yii::$app->request->get('department_id',''));  //订单来源id
        $data =  WorkLogic::getCostTerms($department_id);
        if($cost_id){
            $res  = WorkLogic::editWorkCost();  //编辑操作
            if(isset($res['success']) && $res['success'] == 1){
                echo "<script>history.go(-2);</script>";
                //return $this->redirect('/work/view?work_no='.$res['data']['work_no']);
            }
        }else{
            if ($work_no == '') {
                return BaseLogic::error('工单号不能为空!',20002);
            }
            $res  = WorkLogic::addWorkCost(); //添加操作
            if(isset($res['success']) && $res['success'] == 1){
                echo "<script>history.go(-2);</script>";
                //return $this->redirect('/work/view?work_no='.$res['data']['work_no']);
            }
        }
        $data['work_no'] = $work_no;
        return $this->render('add-work-cost',[
            'data'       => $data
        ]);

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        $work_no    = trim(Yii::$app->request->get('work_no',''));  //工单号码
        $work_id    = trim(Yii::$app->request->get('work_id',''));       //工单中的收费项目id,编辑时候需要
        $cost_id    = trim(Yii::$app->request->get('cost_id',''));  //工单中的收费项目id,编辑时候需要
        $standard_id= trim(Yii::$app->request->get('standard_id',''));  //收费标准id
        $type       = trim(Yii::$app->request->get('type',''));  //订单来源类型
        $type_id    = trim(Yii::$app->request->get('type_id',''));  //订单来源id
        $technician_id = trim(Yii::$app->request->get('technician_id',''));  //技师id
        if(!$work_no){
            return BaseLogic::error('工单不能为空!',20001);
        }
        if(!$type){
            return BaseLogic::error('订单来源类型不能为空!',20002);
        }
        if(!$type_id){
            return BaseLogic::error('订单来源不能为空!',20003);
        }
        if(!$standard_id){
            return BaseLogic::error('收费标准不能为空!',20004);
        }
        //获取服务商是否开启仓储管理功能 1开启 0未开启
        $storageConfig = SysConfig::getOne(['config_key'=>'storage_status','org_id'=>BaseLogic::getManufactorId(),'org_type'=>BaseModel::SRC_FWS]);
        $storageSwitch = $storageConfig['config_values'];//仓储开关：1开，2关
        //获取添加编辑数据
        $data  = WorkLogic::getCostTerms($work_no,$cost_id,$type,$type_id,$standard_id);

        if($cost_id){
            $res  = WorkLogic::editWorkCost();  //编辑操作
            if(isset($res['success']) && $res['success'] == 1){
                return $this->redirect('/work/view?work_no='.$res['data']['work_no']);
            }
        }else{
            if ($work_no == '') {
                return BaseLogic::error('工单号不能为空!',20002);
            }
            $res  = WorkLogic::addWorkCost(); //添加操作
            if(isset($res['success']) && $res['success'] == 1){
                return $this->redirect('/work/view?work_no='.$res['data']['work_no']);
            }
        }
        $data['storageSwitch']    = $storageSwitch;
        $data['technician_id'] = $technician_id;
        return $this->render('add-work-cost',[
            'data'       => $data
        ]);
    }

    /**
     * 编辑工单收费项目
     * @param $workNo
     * @param $reason
     * @return array
     */
    //编辑收费项目
    public function actionEditWorkCost()
    {
        $work_no         = trim(Yii::$app->request->get('work_no',''));  //工单号码
        $department_id   = trim(Yii::$app->request->get('department_id',''));  //订单来源id
        $work_cost_id    = trim(Yii::$app->request->get('work_cost_id',''));  //工单中的收费项目id,编辑时候需要
        $payer_id        = trim(Yii::$app->request->get('payer_id',''));
        $data =  WorkLogic::getCostTerms($department_id,$work_no,$work_cost_id);
        $data['work_no'] = $work_no;
        if(Yii::$app->request->isPost){
            $res  = WorkLogic::editWorkCost();  //编辑操作
            if(isset($res['success']) && $res['success'] == 1){
                echo "<script>history.go(-2);</script>";
                //return $this->redirect('/work/view?work_no='.$res['data']['work_no']);
            }
        }
        return $this->render('edit-work-cost',[
            'data'       => $data
        ]);


        $work_no    = trim(Yii::$app->request->get('work_no',''));  //工单号码
        $work_id    = trim(Yii::$app->request->get('work_id',''));       //工单中的收费项目id,编辑时候需要
        $work_cost_id    = trim(Yii::$app->request->get('work_cost_id',''));  //工单中的收费项目id,编辑时候需要
        $standard_id= trim(Yii::$app->request->get('standard_id',''));  //收费标准id
        $type       = trim(Yii::$app->request->get('type',''));  //订单来源类型
        $type_id    = trim(Yii::$app->request->get('type_id',''));  //订单来源id
        $technician_id = trim(Yii::$app->request->get('technician_id',''));  //技师id
        //获取服务商是否开启仓储管理功能 1开启 0未开启
        $storageConfig = SysConfig::getOne(['config_key'=>'storage_status','org_id'=>BaseLogic::getManufactorId(),'org_type'=>BaseModel::SRC_FWS]);
        $storageSwitch = $storageConfig['config_values'];//仓储开关：1开，2关
        if(!$work_no){
            return BaseLogic::error('工单不能为空!',20001);
        }
        if(!$type){
            return BaseLogic::error('订单来源类型不能为空!',20002);
        }
        if(!$type_id){
            return BaseLogic::error('订单来源不能为空!',20003);
        }
        if(!$standard_id){
            return BaseLogic::error('收费标准不能为空!',20004);
        }
        $is_storage = '';
        //获取添加编辑数据
        $storageNum = 0;
        $data  = WorkLogic::getCostTerms($work_no,$work_cost_id,$type,$type_id,$standard_id);
        if($data['work_info']['cost_type']==2){//费用类型，1其他费用，2配件费
            if($storageSwitch==1){   //开启仓储
                $prodId = $data['work_info']['cost_id'];
                $storage = TechStorage::getOne(['id'=>$prodId,'tech_id'=>$technician_id]);
                if($storage){
                    $storageNum = $storage['num'];
                }
            }
        }

        $res  = WorkLogic::editWorkCost();  //编辑操作
        if(isset($res['success']) && $res['success'] == 1){
            return $this->redirect('/work/view?work_no='.$res['data']['work_no']);
        }

        $data['storageSwitch']    = $storageSwitch;
        $data['technician_id'] = $technician_id;
        $data['storageNum']    = $storageNum;
        return $this->render('edit-work-cost',[
            'data'       => $data
        ]);
    }

    /**
     * 编辑技师分成
     * @author xi
     * @date 2018-2-6
     */
    public function actionEditDivide()
    {
        //Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->request->post('data_array',[]);
        if($data) foreach ($data as $key=>$val){
            if($val['id'] == '' || $val['divide_amount'] == ''|| $val['divide_amount'] == 0){
                return json_encode(BaseLogic::error('编辑数据不能为空',20002));
            }
            $res =  WorkLogic::EditDivide($val['id'],$val['divide_amount'],$val['comment']);
        }else{
            $res = BaseLogic::error('编辑数据不能为空',20002);
        }
        return json_encode($res,true);
    }

    /**
     * 结算确认
     * @author xi
     * @date 2018-4-3
     */
    public function actionConfirmSettlement()
    {
        if(Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $workNo = trim(Yii::$app->request->get('work_no',''));
            if($workNo == ''){
                return BaseLogic::error('参数 work_no 不能为空',20002);
            }

            $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/confirm-settlement";

            $postData = [
                'work_no' => $workNo
            ];
            $jsonStr = Helper::curlPostJson($url, $postData);
            $jsonArr = json_decode($jsonStr,true);
            if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
            {
                return BaseLogic::success($jsonArr['data']['work_no']);
            }
            return BaseLogic::error('确认结算失败'.$jsonStr,20003);
        }
    }

    /**
     * 开始服务
     * @author xi
     * @date 2018-6-21
     */
    public function actionAjaxStartService()
    {
        if(Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $workNo = trim(Yii::$app->request->get('work_no',''));
            $type   = trim(Yii::$app->request->get('type',1));
            if($workNo == ''){
                return BaseLogic::error('工单号不能为空!',20002);
            }

            return WorkLogic::startService($workNo,$type);
        }
    }


}

<?php

namespace webapp\controllers;


use Yii;
use webapp\logic\BaseLogic;
use webapp\models\BaseModel;
use yii\web\Controller;
use common\helpers\Paging;
use webapp\logic\PickUserLogic;
use yii\web\Response;
use common\models\PickUser;
/**
 *@desc提货人管理
 *
 */

class PickUserController extends BaseController
{

    /**
     * 函数用途描述:提货人列表
     * @date: 2018年9月25日 下午14:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionIndex()
    {
        $directCompanyId = BaseLogic::getDirectCompanyId();
        $result = PickUser::getList(['direct_company_id'=>$directCompanyId,'status'=>1]);
        if (Yii::$app->request->isPost)
        {
            $is_json = intval(Yii::$app->request->post('is_json',''));
            if($is_json){
                return json_encode($result);
            }
            $str = '<option value="">请选择提货人</option>';
            if($result)
            {
                foreach ($result as $val) {
                    $str = $str . '<option value="'.$val['id'].'">'.$val['pick_name'].'/'.$val['pick_mobile'].'/'.$val['pick_card'].'/'.$val['pick_license'].'</option>';
                }
            }
            return $str;
        }

        //return json_encode($result);

        return $this->render('index',[
            'data'          =>json_encode($result),
        ]);
    }
    /**
     * 函数用途描述:添加/编辑采购单
     * @date: 2018年9月25日 下午3:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionAdd()
    {
        if (Yii::$app->request->isPost){
            //判断是否存在id 区分添加和修改
            $id = intval(yii::$app->request->post('id',0));
            if ($id != 0) {
                $res = PickUserLogic::edit();
                return json_encode($res);
            }else{
                $res = PickUserLogic::add();
                return json_encode($res);
            }
        }
    }

    /**
     * @return string
     * 删除提货人
     */
    public function actionDel()
    {
        if (Yii::$app->request->isPost){
            //判断是否存在id 区分添加和修改
            $ids = trim(yii::$app->request->post('ids',''));
            if($ids){
                $idsArr = explode(',',$ids);
                $res = PickUserLogic::del($idsArr);
                return json_encode($res);
            }
        }
    }
    //验证信息重复性
    public function actionVerifyData(){
        if (yii::$app->request->isPost) {
            $post = yii::$app->request->post();
            $no = $post['str'];
            $id = $post['id'];
            //print_r($post['str']);exit;
            if($no != ''){
                //验证数据
                $res = PickUser::getOne(['no'=>$no]);
                if($id){
                    if($res && $id != $res['id']){
                        return json_encode(['msg'=>'编号已存在！','code'=>'20002'],true);
                    }
                }else{
                    if($res){
                        return json_encode(['msg'=>'供货商编号已存在！','code'=>'20002'],true);
                    }
                }
            }

            return json_encode(['msg'=>'ok','code'=>'200']);
        }
    }





}


<?php

namespace webapp\controllers;

use Yii;
use webapp\logic\ScheduleSetLogic;

/**
 * 班次设置
 * Class ScheduleSetController
 * @package webapp\controllers
 */

class ScheduleSetController extends BaseController
{
    /**
     * 列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/18
     * Time: 16:03
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $data = ScheduleSetLogic::index();
        
        if (Yii::$app->request->isPost) {
            
            $res = ScheduleSetLogic::add();
            Yii::$app->session->setFlash('message',$res['message']);
            return $this->redirect('/schedule-manage/index');
            
        }
        
        $this->_view['data'] = $data;
        
        return $this->render('index');
    }

    /**
     * 班次添加权限
     * 千万不要删除
     * sxz 2018-7-16
     */
    public function actionAdd()
    {

    }
    
}


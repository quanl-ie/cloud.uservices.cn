<?php

namespace webapp\controllers;


use Yii;
use webapp\logic\BaseLogic;
use webapp\models\BaseModel;
use yii\web\Controller;
use common\helpers\Paging;
use webapp\logic\TechProdStockLogic;
use common\models\Region;
//use common\models\Purchase;
//use common\models\PurchaseDetail;
use common\helpers\CreateNoRule;
use yii\web\Response;
/**
 *@desc:供货商管理
 *
 */

class  TechProdStockReturnController extends BaseController
{

    /**
     * 函数用途描述:技师备件申请管理
     * @date: 2018年3月28日 下午16:30:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    //待审批
    public function actionApply()
    {
        $action    = Yii::$app->controller->action->id;
        //print_r($action);die;
        $stock_type = 1;
        $result = TechProdStockLogic::getList($stock_type);
        //print_r($result);die;
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }
        return $this->render('wait-examine',[
            'action'        =>'apply',
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params']
        ]);
    }
    /**
     * 函数用途描述:技师备件申请管理
     * @date: 2018年3月22日 下午14:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    //待审批
    public function actionRefund()
    {
        $action    = Yii::$app->controller->action->id;
        $stock_type = 2;
        $result = TechProdStockLogic::getList($stock_type);
        //print_r($result);die;
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?page=');
        }else{
            $pageHtml = '';
        }
        return $this->render('wait-examine',[
            'action'        =>'refund',
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params']
        ]);
    }
    //待出，入库
    public function actionWaitCheck()
    {
        $action    = Yii::$app->controller->action->id;
        $type = trim(Yii::$app->request->get('type',''));
        //print_r($type);die;
        $stock_type = '';
        if($type == 'apply'){
            $stock_type = 1;
        }
        if($type == 'refund'){
            $stock_type = 2;
        }
        //print_r($action);die;
        $result = TechProdStockLogic::getList($stock_type);
        //print_r($result);die;
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?type='.$type.'&page=');
        }else{
            $pageHtml = '';
        }
        return $this->render('wait-check',[
            'action' =>$type,
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params']
        ]);

    }
    //入库完毕
    public function actionFinished()
    {
        $action    = Yii::$app->controller->action->id;
        $type = trim(Yii::$app->request->get('type',''));
        $stock_type = '';
        if($type == 'apply'){
            $stock_type = 1;
        }
        if($type == 'refund'){
            $stock_type = 2;
        }
        //print_r($action);die;
        $result = TechProdStockLogic::getList($stock_type);
        //print_r($result);die;
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?type='.$type.'&page=');
        }else{
            $pageHtml = '';
        }
        return $this->render('finished',[
            'action' =>$type,
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params']
        ]);

    }
    //未通过
    public function actionNotPass()
    {
        $action    = Yii::$app->controller->action->id;
        $type = trim(Yii::$app->request->get('type',''));
        $stock_type = '';
        if($type == 'apply'){
            $stock_type = 1;
        }
        if($type == 'refund'){
            $stock_type = 2;
        }
        //print_r($action);die;
        $result = TechProdStockLogic::getList($stock_type);
        //print_r($result);die;
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?type='.$type.'&page=');
        }else{
            $pageHtml = '';
        }
        return $this->render('not-pass',[
            'action' =>$type,
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params']
        ]);

    }
    //已取消
    public function actionCancelled()
    {
        $action    = Yii::$app->controller->action->id;
        $type = trim(Yii::$app->request->get('type',''));
        $stock_type = '';
        if($type == 'apply'){
            $stock_type = 1;
        }
        if($type == 'refund'){
            $stock_type = 2;
        }
        //print_r($action);die;
        $result = TechProdStockLogic::getList($stock_type);
        //print_r($result);die;
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], '?type='.$type.'&page=');
        }else{
            $pageHtml = '';
        }
        return $this->render('cancelled',[
            'action' =>$type,
            'data'          =>$result['list'],
            'pageHtml'      =>$pageHtml,
            'params'        =>$result['params']
        ]);

    }
    /**
     * 函数用途描述:技师备件申请返还详情
     * @date: 2018年3月29日 下午15:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionDetail()
    {
        $infos = [];
        $id = yii::$app->request->get('id','');
        $data = TechProdStockLogic::getDetail($id);
  
        return $this->render('detail',[
            'data'                  =>$data
        ]);
    }
    /**
     * 函数用途描述:同意/拒绝操作
     * @date: 2018年3月30日 下午13:26:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionSetAudit()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $no       = trim(Yii::$app->request->get('no',''));
        $status   = trim(Yii::$app->request->get('status',''));
        $opinionContent = htmlspecialchars(trim(Yii::$app->request->get('audit_suggest','')));
        if($no == ''){
            return BaseLogic::error('采购单号不能为空',20002);
        }
        if($status == ''){
            return BaseLogic::error('操作状态不能为空',20003);
        }
        return TechProdStockLogic::SetAuditStatus($no,$status,$opinionContent);
    }



}


<?php
namespace webapp\controllers;

use common\helpers\Helper;
use common\lib\Push;
use webapp\models\Department;
use webapp\models\Grouping;
use webapp\models\GroupingRelTechnician;
use Yii;
use common\helpers\Paging;
use webapp\logic\BaseLogic;
use webapp\logic\TechnicianLogic;
use webapp\models\Technician;
use common\models\Region;
use yii\web\Response;

class TechnicianController extends BaseController
{

    /**
    * 函数用途描述:技师列表
    * @date: 2018年01月08日 上午9:26:23
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionIndex()
    {

        //本公司及以下权限赋予本公司权限
        if($this->dataAuthId==3){
            $this->dataAuthId=2;
        }
        //生成查询条件
        $info = self::departmentforSearch();

        //获取当前用户可以查看的技师列表
        $result = TechnicianLogic::getList();
        if($result['list']) {
            $technician_ids = array_column($result['list'], 'id');
            // 推送主动获取位置
            if ($technician_ids) {
                Push::sendCustom($technician_ids);
            }
        }
        //获取当前用户的登陆信息
        $user_info = BaseLogic::getLoginUserInfo();

        $technician_name    = $result['params']['technician_name'];
        $technician_mobile  = $result['params']['technician_mobile'];
        $service_type       = $result['params']['service_type'];//服务类型
        $class_type         = $result['params']['class_type'];//产品类型
        $sysConfig          = $result['sysConfig'];
        $pageHtml           = '';
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], "?technician_name=$technician_name&technician_mobile=$technician_mobile&service_type=$service_type&class_type=$class_type&page=");
        }

        //服务类型
        $workTypeArr  = BaseLogic::getType();
        //产品类型
        $classArr   = BaseLogic::getClass();

        $result['params']['department_id']         =  trim(Yii::$app->request->get('department_ids','0'));
        return $this->render('index', [
            'data'           => $result['list'],
            'department'     => $info,
            'auth'           => $this->dataAuthId,
            'department_id'  => $user_info->department_id,
            'dataAuth'       => $this->dataAuthId,
            'company_id'     => $this->directCompanyId,
            'pageHtml'       => $pageHtml,
            'params'         => $result['params'],
            'serviceType'    => $workTypeArr,
            'classType'      => $classArr,
            'sysConfig'      => $sysConfig,
            'department_name'=> $result['department_name']
        ]);
    }
    /**
     * 函数用途描述:添加技师
     * @date: 2018年1月9日 下午16:34:37
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionAdd()
    {
        $id = Yii::$app->request->get('id',0);
        //判断post传值
        if (Yii::$app->request->isPost)
        {
            //判断是否存在id 区分添加和修改
            if (Yii::$app->request->post('id',0) != 0)
            {
                $res = TechnicianLogic::edit();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/technician/index');
            }
            else
            {
                $res = TechnicianLogic::add();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/technician/index');
            }
        }
        //当前登录的部门ID
        $department_id =  $this->departmentId;
        $defaultDepatment = $this->departmentObj->name;
        $option = 1;

        //公司账号部门以及部门以下权限，默认为本人权限
        $selfAuth = BaseLogic::getDatAuthId();
        if(BaseLogic::getTopId() == $department_id && ($selfAuth == 4 ||$selfAuth == 5) ){
            $this->dataAuthId = 1;
            $defaultDepatment = '无机构';
            $option = 0;
        }

        //echo $this->dataAuthId;exit;
        if($this->dataAuthId == 3 || $this->dataAuthId == 2)
        {
            //获取当前部门的顶级部门ID
            $top_id = $this->directCompanyId;
            $department = Department::getDepartmentInfo($top_id,1,1,1);
            //查询是否还有下级
            $nextDepartment = Department::getDepartmentInfo($top_id,0,1,1);
            if(!empty($nextDepartment)){
                $department[0]['exist'] = 1; //有下级
            }else{
                $department[0]['exist'] = 2; //无
            }
        }
        elseif ($this->dataAuthId == 1) {
            $department = [];
            $defaultDepatment = '无机构';
            $option = 0;
        }
        else
        {
            if($this->departmentObj->type = 3){
                //获取当前部门
                $department = Department::getDepartmentInfo($department_id,1,0,1);
                //查询是否还有下级
                $nextDepartment = Department::getDepartmentInfo($department_id,0,1,1);

                if($this->dataAuthId == 4){    //本部门
                    $department[0]['exist'] = 2; //无
                }else{
                    if(!empty($nextDepartment)){
                        $department[0]['exist'] = 1; //有下级
                    }else{
                        $department[0]['exist'] = 2; //无
                    }
                }
            }else{
                $department = [];
            }
        }
        //判断是否get传值 修改参数
        $infos = [];
        if (Yii::$app->request->isGet)
        {
            //获取技师信息
            $infos = TechnicianLogic::getTechnicianInfo();
        }

        //如果修改 - 查一下是否有未完成的订单,如果有就不能修改服务机构
        $countNoFinshWork = 0;
        if($id > 0) {
            $countNoFinshWork = Technician::technicianNoFinshWork($id);
        }
        //获取开通省份
        $province = Region::getList(['parent_id'=>1]);
        return $this->render('add',[
            'province'             => $province,
            'infos'                => $infos,
            'auth'                 => $this->dataAuthId,
            'department'           => $department,
            'defult_department_id' => $department_id,
            'defult_department'    => $defaultDepatment,
            'countNoFinshWork'     => $countNoFinshWork,
            'option'               => $option  //表单提交确认按钮
        ]);
    }

    /**
     * 函数用途描述:添加技师
     * @date: 2018年1月9日 下午16:34:37
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionEditInfo()
    {
        $id = Yii::$app->request->get('id',0);
        //判断post传值
        if (Yii::$app->request->isPost)
        {
            //判断是否存在id 区分添加和修改
            if (Yii::$app->request->post('id',0) != 0)
            {
                $res = TechnicianLogic::edit();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/technician/index');
            }
            else
            {
                $res = TechnicianLogic::add();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/technician/index');
            }
        }
        //当前登录的部门ID
        $department_id =  $this->departmentId;
        $defaultDepatment = $this->departmentObj->name;
        $option = 1;

        //公司账号部门以及部门以下权限，默认为本人权限
        $selfAuth = BaseLogic::getDatAuthId();
        if(BaseLogic::getTopId() == $department_id && ($selfAuth == 4 ||$selfAuth == 5) ){
            $this->dataAuthId = 1;
            $defaultDepatment = '无机构';
            $option = 0;
        }

        //echo $this->dataAuthId;exit;
        if($this->dataAuthId == 3 || $this->dataAuthId == 2)
        {
            //获取当前部门的顶级部门ID
            $top_id = $this->directCompanyId;
            $department = Department::getDepartmentInfo($top_id,1,1,1);
            //查询是否还有下级
            $nextDepartment = Department::getDepartmentInfo($top_id,0,1,1);
            if(!empty($nextDepartment)){
                $department[0]['exist'] = 1; //有下级
            }else{
                $department[0]['exist'] = 2; //无
            }
        }
        elseif ($this->dataAuthId == 1) {
            $department = [];
            $defaultDepatment = '无机构';
            $option = 0;
        }
        else
        {
            if($this->departmentObj->type = 3){
                //获取当前部门
                $department = Department::getDepartmentInfo($department_id,1,0,1);
                //查询是否还有下级
                $nextDepartment = Department::getDepartmentInfo($department_id,0,1,1);

                if($this->dataAuthId == 4){    //本部门
                    $department[0]['exist'] = 2; //无
                }else{
                    if(!empty($nextDepartment)){
                        $department[0]['exist'] = 1; //有下级
                    }else{
                        $department[0]['exist'] = 2; //无
                    }
                }
            }else{
                $department = [];
            }
        }
        //判断是否get传值 修改参数
        $infos = [];
        if (Yii::$app->request->isGet)
        {
            //获取技师信息
            $infos = TechnicianLogic::getTechnicianInfo();
        }

        //如果修改 - 查一下是否有未完成的订单,如果有就不能修改服务机构
        $countNoFinshWork = 0;
        if($id > 0) {
            $countNoFinshWork = Technician::technicianNoFinshWork($id);
        }
        //获取开通省份
        $province = Region::getList(['parent_id'=>1]);
        return $this->render('add',[
            'province'             => $province,
            'infos'                => $infos,
            'auth'                 => $this->dataAuthId,
            'department'           => $department,
            'defult_department_id' => $department_id,
            'defult_department'    => $defaultDepatment,
            'countNoFinshWork'     => $countNoFinshWork,
            'option'               => $option  //表单提交确认按钮
        ]);
    }
    //验证技师信息重复性
    public function actionVerifyData()
    {
        if (Yii::$app->request->isPost)
        {
            $post = yii::$app->request->post();
            $de_arr = Department::getDepartmentChildren($this->directCompanyId,0,1);
            $department_ids = array_column($de_arr,"id");
            if($post['type'] == 'i' && $post['str'] != '')
            {
                //验证数据
                $res = Technician::verifyData(['identity_card'=>$post['str']],['store_id'=>$department_ids]);
                if($res == false){
                    return json_encode(['msg'=>'技师身份证信息已存在！','code'=>'20002'],true);
                }
            }
            return json_encode(['msg'=>'ok','code'=>'200']);
        }
    }
    //技师编辑
    public function actionEdit()
    {
        $id = intval(Yii::$app->request->get('id',0));
        if($id <= 0){
            return $this->redirect('index');
        }
        //获取技师信息
        $infos = TechnicianLogic::getTechnicianInfo();
        //获取开通省份
        $province = Region::getList(['parent_id'=>1]);
        return $this->render('add',[
            'province'=>$province,
            'infos'=>$infos,
        ]);
    }

   /**
    * 函数用途描述:查看技师详情
    * @date: 2018年1月8日 下午9:34:37
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public function actionDetail(){
        $result = TechnicianLogic::getDetail();
        return $this->render('detail',['data'=>$result]);
    }

    /**
     * 函数用途描述:获取技师技能信息
     * @date: 2018年1月9日 上午10:20:39
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionGetSkill()
    {
        $result = TechnicianLogic::getSkill();

        return $this->renderPartial('skill',[
            'field'=>$result
        ]);
    }

    /**
     * 函数用途描述:查看技师位置
     * @date: 2018年1月9日 上午10:20:39
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionViewLocation(){

        $result = TechnicianLogic::getLocation();
        return $this->renderPartial('view-location',['data'=>$result]);
    }

    /**
     * 函数用途描述:技师位置修改
     * @date: 2018年1月9日 上午10:20:39
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionEditArea(){
        $result = TechnicianLogic::editArea();
        return $this->render('edit-area',['data'=>$result]);
    }

    //更新技师位置
    public function actionUpdateArea(){
        $result = TechnicianLogic::updateArea();
        return json_encode($result);
    }

    //获取技师验证信息
    public function actionVerify(){
        $res = TechnicianLogic::Verify();
        return json_encode($res);
    }

    /**
     * 函数用途描述:重置密码
     * @date: 2018年1月9日 下午2:19:52
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionResetPwd(){
        $id = Yii::$app->request->get('id');
        if (yii::$app->request->isPost) {
            if(yii::$app->request->post('id',0) != 0){
                $res = TechnicianLogic::resetPwd();
                return json_encode($res);
            }
        }
        return $this->renderPartial('reset-pwd',['id'=>$id]);
    }

    /**
     * 函数用途描述:禁用/还原账号
     * @date: 2018年1月9日 下午1:53:05
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionSetStatus()
    {
        $t    = intval(Yii::$app->request->get('t',1));
        $id   = intval(Yii::$app->request->get('id',0));
        if ($id<= 0) {
            return $this->redirect('/technician/index');
        }
        if($t == 1){
            $status = 2;
        }
        if($t == 2){
            $status = 1;
        }
        $where = [
            'id'       =>$id,
            'status'   => $t
        ];

        $model = Technician::findOne($where);
        //验证是否有未完成的工单
        if(isset($id) && $id>0){
            $post_data = ['techician_ids' => $id];
            $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/sreach-group-work";
            $group_work_res = json_decode(Helper::curlPostJson($url,$post_data), true);
            if(!$group_work_res['success'] || !empty($group_work_res['data'])) {
                Yii::$app->session->setFlash('message','该技师仍有未完成订单，不可禁用!');
                return $this->redirect ('/technician/index');
            }
        }
        if (!empty($model))
        {
            $model->status = $status;
            $model->updated_at = time();
            if ($model->save(false)) {
                if($status == 2) {
                    Grouping::updateGroup($status,$id,$model->store_id);
                }
                Yii::$app->session->setFlash('message','操作成功!');
                return $this->redirect ('/technician/index');
            }
        }
        return $this->redirect('/technician/index');
    }
    /**
     * 函数用途描述:还原账号
     * @date: 2018年1月9日 下午1:53:05
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionSetStatusOn()
    {
        $t    = intval(Yii::$app->request->get('t',1));
        $id   = intval(Yii::$app->request->get('id',0));
        if ($id<= 0) {
            return $this->redirect('/technician/index');
        }
        if($t == 1){
            $status = 2;
        }
        if($t == 2){
            $status = 1;
        }
        $where = [
            'id'       =>$id,
            'status'   => $t
        ];

        $model = Technician::findOne($where);
        if (!empty($model))
        {
            $model->status = $status;
            $model->updated_at = time();
            if ($model->save(false)) {
                if($status == 2) {
                    Grouping::updateGroup($status,$id,$model->store_id);
                }
                Yii::$app->session->setFlash('message','操作成功!');
                return $this->redirect ('/technician/index');
            }
        }
        return $this->redirect('/technician/index');
    }

    //获取开通城市
    public function actionGetCity($province_id)
    {
        $city = Region::getList(['parent_id'=>$province_id]);
        $str = '<option value="">请选择市</option>';
        foreach ($city as $val) {
            $str = $str . '<option value="'.$val['region_id'].'">'.$val['region_name'].'</option>';
        }
        return $str;
    }

    //获取开通城市
    public function actionGetDistrict($city_id)
    {
        $district = Region::getList(['parent_id'=>$city_id]);
        $str = '<option value="">请选择区</option>';
        foreach ($district as $val) {
            $str = $str . '<option value="'.$val['region_id'].'">'.$val['region_name'].'</option>';
        }
        return $str;
    }

    //技师技能添加
    public function actionGetSkills()
    {
        $result = TechnicianLogic::getSkills();
        return $this->renderPartial('get-skills',[
            'skills'=>$result['skill'],
            'service_type'=>$result['service_type'],
            'class_type'=>$result['class_type'],
            ]);
    }

    /**
     * 函数用途描述:技师审核列表
     * @date: 2018年1月10日 下午1:53:05
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionExamineList(){
        $result = TechnicianLogic::getExamineList();
        $technician_name = $result['params']['technician_name'];
        $technician_mobile = $result['params']['technician_mobile'];
        $audit_status = $result['params']['audit_status'];
        $pageHtml = '';
        if($result['totalPage'] >1){
            $pageHtml = Paging::make($result['page'], $result['totalPage'], "?technician_name=$technician_name&technician_mobile=$technician_mobile&audit_status=$audit_status&page=");
        }
        //接单状态
        $audit_status = TechnicianLogic::auditStatus();
        return $this->render('examine-list', [
            'data'           => $result['list'],
            'pageHtml'       =>$pageHtml,
            'params'         =>$result['params'],
            'auditStatus'     =>$audit_status,
        ]);

    }

    /**
     * 函数用途描述:查看技师驳回原因
     * @date: 2018年1月10日 下午1:53:05
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionReason(){
        $res = TechnicianLogic::getReason();
        return $this->renderPartial('reason',['reason'=>$res]);
    }

    /**
     * 函数用途描述:审核技师操作
     * @date: 2018年1月10日 下午1:53:05
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionToExamine(){
        //获取技师信息
        $res = TechnicianLogic::getTechnicianInfo();
        return $this->renderPartial('to-examine',['data'=>$res]);
    }

    //设置状态
    public function actionSetExamine(){
        if(yii::$app->request->isPost){
            $data = yii::$app->request->post();
            if (empty($data)) {
                return json_encode(['code'=>203,'msg'=>'参数错误']);
            }
            $type = $data['type'];
            if (!$type) {
                return json_encode(['code'=>203,'msg'=>'参数错误']);
            }
            if ($type == 2 && empty($data['reason'])) {
                return json_encode(['code'=>202,'msg'=>'请输入审核意见']);
            }
            if ($type == 1) {
                $data['audit_status'] = 5;  //审核通过
                $pass_type = 3; //通过3，未通过2 用于通知技师推送
            }else{
                $data['audit_status'] = 3;  //审核驳回
                $url = '';
                $pass_type = 2; //通过3，未通过2 用于通知技师推送
            }
            //更新技师审核状态
            $technician = Technician::setStatus($data['id'],$data['audit_status'],$data['reason']);
            if (!$technician) {
                return json_encode(['code'=>201,'msg'=>'审核失败']);
            }
            $technicianArr = Technician::findOne(['id' => $data['id']]);
            //审核后通知技师
            if($pass_type == 3) {
                $title = '审核通过，快去抢单吧！';
                $content = "[$technicianArr->name]，恭喜您！您的实名认证已通过。";
            } else {
                $title = '审核未通过，请重新认证。';
                $content = '您的实名认证因['.$data['reason'].']不通过，请点击查看原因。';
            }
            $push = Push::pushMsg($title,$content,$data['id'],2,$data['id'],$pass_type);
            $push = json_decode($push,true);
            if(!$push['success']){
                return json_encode(['code'=>201,'msg'=>'审核成功-通知技师失败,当前技师不在线']);
            }
            return json_encode(['code'=>200,'msg'=>'操作成功']);
        }
    }

    /**
     * 函数用途描述:审核未通过修改技师信息
     * @date: 2018年1月10日 下午1:53:05
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionExamineEdit(){
        //判断post传值进行修改
        if (yii::$app->request->isPost)
        {
            //判断是否存在id 区分添加和修改
            if (yii::$app->request->post('id',0) != 0)
            {
                $res = TechnicianLogic::edit();
                Yii::$app->session->setFlash('message',$res['message']);
                return $this->redirect('/technician/examine-list');
            }
        }

        $res = TechnicianLogic::getTechnicianInfo();
        //获取开通省份
        $province = Region::getList(['parent_id'=>1]);
        return $this->render('examine-edit',[
            'infos'=>$res,
            'province'=>$province
        ]);
    }

    /**
     * 函数用途描述:查询技师接单情况
     * @date: 2018年1月10日 下午1:53:05
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public function actionTechnicianOrder()
    {
        $getStr = '?';
        if($_GET){
            $getStr ='?'. http_build_query($_GET) . '&';
        }

        $result = TechnicianLogic::getTechnicianOrder($this->directCompanyId);

        $technicianId    = intval(trim(Yii::$app->request->get('technician_id',0))); //技师id
        $Info = Technician::getOne(['id'=>$technicianId]);

        $technicianInfo['name'] = isset($Info['name'])?$Info['name']:'';
        $pageHtml = '';
        if($result['success'] == true && isset($result['data']['totalPage']) && $result['data']['totalPage'] >1){
            $pageHtml = Paging::make($result['data']['page'], $result['data']['totalPage'], $getStr."page=");
        }
        $this->_view['data']     = $result['data'];
        $this->_view['pageHtml'] = $pageHtml;
        $this->_view['workTypeArr'] = BaseLogic::getType();
        $this->_view['workClassArr'] = BaseLogic::getClass();
        $this->_view['technicianInfo'] = $technicianInfo;
        $this->_view['technicianName'] = isset($Info) ? $Info['name'].'技师的订单' :'技师订单';
        return $this->render('technician-order');
    }


    /**
     * 部门结构
     * @author li
     * @date 2018-6-13
     */
    public function actionAjaxGetDepartment()
    {
        if(Yii::$app->request->isAjax)
        {
            $pid = Yii::$app->request->get('pid',0);
            $type = Yii::$app->request->get('type',0);
            if($pid == 0){
                return BaseLogic::error('请正确输入参数');
            }
            //获取下级机构（不含部门）
            if($type == 0){
                $department_child = Department::getDepartmentInfo($pid,0,1,1,2,['3']);
            }else{
                $department_child = Department::getDepartmentInfo($pid,0,0,1,0);
            }
            //删除子机构中的经销商
            foreach($department_child as $k => $v) {
                if($v['type'] == 1) {
                    unset($department_child[$k]);
                }
            }
            $department_child = array_values($department_child);

            if (empty($department_child)) {
                return BaseLogic::success([]);
            }
            $departIds = [];
            if (!empty($department_child)) {
                $departIds = array_column($department_child,'id','id');
            }
            //查询是否还有下级
            if(!empty($department_child)){
                $nextDepartment = Department::getDepartmentInfo($departIds,0,1,1);
            }
            $nextDepartmentId = [];
            if (!empty($nextDepartment))
            {
                $nextDepartmentId = array_column($nextDepartment,'parent_id','parent_id');
            }
            $data = [];
            if (isset($department_child))
            {
                foreach ($department_child as $key => $val) {
                    $data[$key]['id']    = $val['id'];
                    $data[$key]['name'] = $val['name'];
                    if (isset($nextDepartmentId[$val['id']])) {
                        $data[$key]['exist'] = 1;
                    }else{
                        $data[$key]['exist'] = 2;
                    }
                }
            }
            return json_encode(BaseLogic::success($data));
        }
    }

    /**
     * 检查机构名字是否有相同的
     * @return array
     * @author xi
     * @date 2018-6-11
     */
    public function actionAjaxHasRepeat()
    {
        if(Yii::$app->request->isAjax)
        {
            $this->layout = false;
            Yii::$app->response->format = Response::FORMAT_JSON;

            $mobile = trim(Yii::$app->request->post('param',''));
            $id     = intval(Yii::$app->request->get('id',0));

            if($mobile == '' ){
                return BaseLogic::error('参数param不能为空',20003);
            }

            $count = Technician::hasRepeat($mobile,$id);
            return [
                'status' => $count>0?'n':'y',
                'info'   => $count>0?'手机号已占用':''
            ];
        }
    }

}


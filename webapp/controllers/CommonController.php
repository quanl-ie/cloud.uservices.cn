<?php
namespace webapp\controllers;

use common\helpers\Helper;
use common\helpers\Gaode;
use common\models\RegionView;
use common\models\ServiceClass;
use webapp\logic\BaseLogic;
use webapp\logic\CategoryLogic;
use webapp\models\BaseModel;
use webapp\models\Department;
use webapp\models\Region;
use webapp\models\ManufactorClass;
use webapp\models\User;
use Yii;
use yii\rest\Controller;
use yii\web\Response;

class CommonController extends Controller
{

    /**
     * 类目分类
     * @author xi
     * @date 2018-2-3
     */
    public function actionAjaxGetClass()
    {
        $pid      = Yii::$app->request->get('pid',0);
        $classId  = Yii::$app->request->get('class_id',0);
        $type     = Yii::$app->request->get('type','');
        $mark     = Yii::$app->request->get('mark','');
        $flag     = Yii::$app->request->get('flag',0);


        $topId = BaseLogic::getTopId();
        $dcId  = BaseLogic::getDirectCompanyId();
        //获取自有分类
        $where = [
            ['=','a.direct_company_id',$dcId],
            ['=','a.class_pid',$pid],
            ['=','b.department_id',$topId],
        ];

        if ($type == '') {
            $where[] = ['=','b.status',1];
        }

        if ($classId != 0)
        {
            $where[] = ['<>','a.class_id',$classId];
        }

        $manufactorClass = ManufactorClass::getPageList($where);

        if (empty($manufactorClass['list'])) {
            return BaseLogic::success([]);
        }

        $classIds = array_column($manufactorClass['list'],'class_id','class_id');

        if ($type == '') {
            $exist = CategoryLogic::existNext($classIds,$dcId,1);
        }else{
            $exist = CategoryLogic::existNext($classIds,$dcId);
        }

        if ($classId != 0)
        {
            $cla = ServiceClass::find()->where(['id'=>$classId])->asArray()->one();
            if (!empty($cla) && isset($cla['parent_id']) && isset($exist[$cla['parent_id']])) {
                $exist[$cla['parent_id']] = 2;
            }
        }

        $data = [];

        foreach ($manufactorClass['list'] as $key => $val) {

            $data[$key]['id']         = $val['class_id'];
            $data[$key]['class_name'] = $val['title'];

            if (isset($exist[$val['class_id']])) {
                $data[$key]['exist'] = $exist[$val['class_id']];
            }

            if ($type != '' && $type == 2)
            {
                $data[$key]['parent_id']  = $val['parent_id'];
                $data[$key]['sort']       = $val['sort'];
                $data[$key]['status']     = $val['status'];

                if ($val['status'] == 1) {
                    $data[$key]['status_desc'] = '已启用';
                }else{
                    $data[$key]['status_desc'] = '已禁用';
                }
            }

        }
        if($mark == ''){
            if ($topId == $dcId) {
                $data['flag'] = 1;
            }else{
                $data['flag'] = 2;
            }
        }

        if($flag){
            foreach ($data as $key=>$val){
                if(is_array($val)){
                    if($val['exist'] == 2){
                        $data[$key]['exist'] = 0;
                    }
                }
            }
        }
        return BaseLogic::success($data);

    }
    
    /**
     * 经纬度转换
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/2
     * Time: 10:34
     * @return array
     */
    public function actionAjaxGetGaode()
    {
        if(Yii::$app->request->isAjax)
        {
            $city    = trim(Yii::$app->request->get('city',''));
            $address = trim(Yii::$app->request->get('address',''));
            
            if($city == ''){
                return BaseLogic::error('请正确输入参数');
            }
            
            if($address == ''){
                return BaseLogic::error('请正确输入参数');
            }
            $cityDesc = Region::getCityName($city);
            $res = Gaode::getLocation(str_replace(' ','',$address),$cityDesc);
            return BaseLogic::success($res);
        }
    }
    
    /**
     * 获取时间
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/24
     * Time: 15:39
     * @return array
     */
    public function actionAjaxGetTime()
    {
        $res = Helper::setTime();
        return BaseLogic::success($res);
    }

    /**
     * 查出省份城市区县
     * @return array
     * @author xi
     * @date 2018-6-7
     */
    public function actionGetRegion()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $pid = Yii::$app->request->get('pid',0);
        if(!$pid){
            return BaseLogic::error('参数pid不能为空',20002);
        }

        $result = [];
        $count = 1;
        do
        {
            $query = Region::findAllByAttributes(['parent_id'=>$pid],'region_id,region_name,parent_id',false,1000,$count);
            if($query){
                $result = array_merge($result,$query);
            }
            $count++;
        }
        while($query);

        return BaseLogic::success($result);
    }
    /**
     * 部门结构
     * @author liquan
     * @date 2018-6-8
     */
    public function actionAjaxGetDepartment()
    {
        if(Yii::$app->request->isAjax)
        {
            $pid = Yii::$app->request->get('pid',0);
            $type = Yii::$app->request->get('type',0);
            $auth = Yii::$app->request->get('auth',0);
            $company_id = Yii::$app->request->get('company_id',0);  //当前机构直属
            $pid_company_id = Department::getDirectCompanyIdById($pid); // 获取传过来机构的直属
            if($pid == 0){
                return BaseLogic::error('请正确输入参数');
            }
            /*if($type == 0){ //获取下级机构（不含部门）
                  if($auth == 3 || $auth == 5){
                        $department_child = Department::getDepartmentInfo($pid,0,0,1,2,[1,2],1);
                  }else{
                        $department_child = Department::getDepartmentInfo($pid,0,1,1,2,[2],1);
                  }
            }else{  //获取所有无用户下级机构
                  if($auth == 3 || $auth == 5){
                        $department_child = Department::getDepartmentInfo($pid,0,0,1,0,[1,2,3],1);
                  }else{
                        $department_child = Department::getDepartmentInfo($pid,0,0,1,0,[2,3],1);
                  }
            }*/
              if($type == 0){
                    if($auth == 3){
                          //获取上级id
                          $get_company_id = Department::getDirectCompanyIdById($pid);
                          if($get_company_id == $company_id){
                                $department_child = Department::getDepartmentInfo($pid,0,0,1,2,[1,3],1);
                          }else{
                                $department_child = Department::getDepartmentInfo($pid,0,0,1,2,[1],1);
                          }
                    }else{
                          $department_child = Department::getDepartmentInfo($pid,0,0,1,2,[3],1);
                    }
              }else if($type == 2){
                    $department_child = Department::getDepartmentInfo($pid,0,0,1,2,[3],1);
              }else{
                    if($auth == 3){
                          if($company_id == $pid_company_id){
                              $department_child = Department::getDepartmentInfo($pid,0,0,1,2,[1,3],1);
                          }else{
                              $department_child = Department::getDepartmentInfo($pid,0,0,1,2,[1],1);
                          }
                    }else{
                          $department_child = Department::getDepartmentInfo($pid,0,0,1,2,[3],1);
                    }
              }
            if (empty($department_child)) {
                return BaseLogic::success([]);
            }
            $departIds = [];
            //查询是否还有下级
            if(!empty($department_child)){
                  $departIds = array_column($department_child,'id','id');
                  if($type == 0){
                        //如果下级是经销商  判断此经销商有没有下级经销商 （不查部门）
                        foreach ($department_child as $k => $v) {
                              if($v['type'] == 1){
                                    $next_jxs = Department::getDepartmentInfo($v['id'],0,1,1,2,[1],1);
                                    if(empty($next_jxs)){ //如果经销商下无经销商则 只显示本身 无下级
                                          unset($departIds[$v['id']]);
                                    }
                              }
                        }
                  }
                  $nextDepartment = Department::getDepartmentInfo($departIds,0,1,1,2,[1,3],1);
            }
            $nextDepartmentId = [];
            if (!empty($nextDepartment))
            {
                $nextDepartmentId = array_column($nextDepartment,'parent_id','parent_id');
            }

            $data = [];
            if (isset($department_child))
            {
                foreach ($department_child as $key => $val) {
                    $data[$key]['is_user'] = 0;
                    if($val['type'] == 1){
                        $user_info = User::findOne(['department_id'=>$val['id']]);
                        if($user_info){
                            $data[$key]['is_user'] = 1;
                        }
                    }
                    $data[$key]['id']    = $val['id'];
                    $data[$key]['name'] = $val['name'];
                    if (isset($nextDepartmentId[$val['id']])) {
                        $data[$key]['exist'] = 1;
                    }else{
                        $data[$key]['exist'] = 2;
                    }
                }
            }
            return BaseLogic::success($data);
        }
    }

    /**
     * 地址搜索
     * @author xi
     */
    public function actionSearchRegion()
    {
        $keyword = trim(Yii::$app->request->post('keyword',''));
        if($keyword == ''){
            return BaseLogic::error('请正确输入关键字');
        }

        $keyword = str_replace("，","",$keyword);

        $query = RegionView::find()
            ->where(['like','city',$keyword])
            ->orWhere(['like','quanpin',$keyword])
            ->orWhere(['like','jianpin',$keyword])
            ->select("city_label as city, province_id,city_id,district_id")
            ->limit(10)
            ->asArray()->all();

        if($query){
            return BaseLogic::success($query);
        }
        return BaseLogic::success([]);
    }

    /**
     * 类目分类
     * @author xi
     * @date 2018-2-3
     */
    public function actionClassList()
    {
        $topId = BaseLogic::getTopId();
        $dcId = BaseLogic::getDirectCompanyId();
        //获取自有分类
        $where = [
            ['=', 'a.direct_company_id', $dcId],
            ['=', 'b.department_id', $topId],
            ['=', 'b.status', 1]
        ];

        $manufactorClass = ManufactorClass::getPageList($where,"a.class_id,b.title,b.parent_id");
        return BaseLogic::success((self::createTree($manufactorClass['list'],0)));
    }


    /**
     * 返回所有类目
     * @param $modals
     * @param $parent_id
     * @return array
     */
    public static function createTree($modals,$parent_id)
    {
        $tree = array();

        foreach($modals as $row)
        {
            if($row['parent_id'] == $parent_id)
            {
                $tmp = self::createTree($modals,$row['class_id']);
                $row['children'] = [];
                if($tmp){
                    $row['children']= $tmp;
                }
                $tree[] = $row;
            }
        }
        return $tree;
    }


}

<?php

namespace webapp\controllers;

use yii;

use webapp\controllers\BaseController;
use webapp\logic\PositionLogic;

/**
 * @desc:客户管理
 * @author liuxingqi <lxq@c-ntek.com>
 * @date 2017-12-15
 *
 */

class PositionController extends BaseController
{
    //订单定位
    public function actionOrder()
    {
        $res = PositionLogic::order();
        $this->_view['data'] = $res;
        return $this->render('orderPosition');
    }
}


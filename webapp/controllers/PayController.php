<?php

namespace webapp\controllers;

use common\lib\WxPayNative;
use common\lib\AliPayNative;
use Yii;
use webapp\controllers\BaseController;

/**
 * @desc:支付 测试文件
 */
class PayController extends BaseController
{
      /**
       * 通过微信生产预支付交易链接
       */
      public function actionWxCreateOrder()
      {
            //生成预支付订单
            $data['product_id'] = '479';        //商品id  扫码支付必传
            $data['orderNo'] = "1431201802994902";        //订单号
            $data['payAmount'] = 0.01;          //付款金额，单位:元
            $data['orderName'] = '智能云集';    //订单标题
            $data['attach'] = $this->directCompanyId;        //附加数据 用于标识来源
            $data['payTime'] = time();      //付款时间
            $arr = WxPayNative::createOrder($data);
            //生成二维码
            $qr_code = WxPayNative::createQr($arr['code_url']);
            echo $qr_code;
      }
      //接收wx支付结果
      public function actionWxNotify(){
            $this->enableCsrfValidation = false;
            $result = WxPayNative::notify();
            if($result->return_code =="SUCCESS"){
                  if($result->result_code == "SUCCESS"){
                        var_dump($result);// insert
                  }else{
                        echo $result->err_code;
                        echo $result->err_code_des;
                  }
            }else{
                  echo $result->return_msg;
            }
      }
      //查询wx订单信息
      public function actionWxOrderQuery(){
            $order_id = '143120180131994902';
            $result = WxPayNative::orderQuery($order_id);
            if($result->return_code =="SUCCESS"){
                  if($result->result_code == "SUCCESS"){
                       var_dump($result);// insert
                  }else{
                        echo $result->err_code;
                        echo $result->err_code_des;
                  }
            }else{
                  echo $result->return_msg;
            }
      }

      /**
       * 支付宝支付
       * 参数可参照alipay当面付说明文档
       */
      public function actionAliCreateOrder(){
            $order['out_trade_no'] = '243120180131994902';
            $order['total_amount'] = '0.01';
            $order['subject'] = '智能云集测试1';
            $arr = AliPayNative::createOrder($order);
            //生成二维码
            if($arr['alipay_trade_precreate_response']['code'] == 10000){
                  $qr_code = AliPayNative::createQr($arr['alipay_trade_precreate_response']['qr_code']);
                  echo $qr_code;
            }else{
                  echo $arr['alipay_trade_precreate_response']['msg'];
            }
      }
      //同步接收ali支付结果
      public function actionAliReturn(){
            $this->enableCsrfValidation = false;
            $result = file_get_contents("php://input");
      }
      //异步接收ali支付结果
      public function actionAliNotify(){
            $this->enableCsrfValidation = false;
            $result = AliPayNative::notifyUrl();
      }
      //查询Ali订单信息
      public function actionAliOrderQuery(){
            $order_id = '243120180131994902';
            $result = AliPayNative::query($order_id);
            var_dump($result);
      }
}


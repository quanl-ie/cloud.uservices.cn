<?php

namespace webapp\controllers;

use Yii;

class InventoryController extends BaseController
{

    /**
     * 盘点列表
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * 添加
     * @return string
     */
    public function actionAdd()
    {
        return $this->render('add');
    }

    /**
     * 盘点详情
     * @return string
     */
    public function actionView()
    {
        return $this->render('view');
    }

    /**
     * 盘点中
     * @return string
     */
    public function actionInventorying()
    {
        return $this->render('inventorying');
    }

}


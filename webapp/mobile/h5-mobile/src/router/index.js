import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login/pages/index'
import loginOut from '@/components/login/pages/loginout'
import orderList from '@/components/order/pages/list'
import orderDetail from '@/components/order/pages/detail'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'orderList',
      component: orderList
    },{
      path: '/loginout',
      name: 'loginOut',
      component: loginOut
    },{
      path: '/login',
      name: 'login',
      component: login
    },{
      path: '/order/detail',
      name: 'orderDetail',
      component: orderDetail
    }
  ]
})

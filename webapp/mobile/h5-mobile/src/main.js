import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import MintUI from 'mint-ui'
import 'babel-polyfill'
import Es6Promise from 'es6-promise'
import 'element-ui/lib/theme-chalk/index.css'
import 'mint-ui/lib/style.css'
import toast from 'vue2-toast'
import 'vue2-toast/lib/toast.css';
import '@/assets/css/common.css';
import Base64 from 'js-base64';
import VuePreview from 'vue2-preview'
Vue.use(VuePreview)
Es6Promise.polyfill()
Vue.config.productionTip = false
Vue.use(MintUI);
Vue.use(ElementUI);
Vue.use(toast);
Vue.use(Base64);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

import api from '../../../api/api';
import Qs from 'qs';
export default {
  getOrderListAxios(params){
    return api.fetchGet('/order/index?'+Qs.stringify(params),'get')
  },

  getOrderDetailByIdAxios(params){
    return api.fetchGet('/order/view?order_no='+params,'get')
  }
}

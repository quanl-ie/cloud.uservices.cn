import api from '../../../api/api'
import Qs from 'qs';
export default {
  //登录
  loginAxios(params){
    return api.fetchLogin('/site/login',Qs.stringify(params),'post')
  },

  //获取机构
  getCompanyNameAxios(params){
    return api.fetchPost('/site/check-dep',Qs.stringify(params),'post')
  },

  //退出登录
  loginoutAxios(){
    return api.fetchGet('/site/logout','get')
  }
}

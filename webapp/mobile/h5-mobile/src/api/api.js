import axios from 'axios';
import config from '../api/config.js';
export default {

  fetchLogin(url,params, method = 'POST') {
    return new Promise((resolve, reject) => {
      axios({
        method: method,
        url: url,
        data:params,

      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error)
      })
    })
  },

  fetchGet(url, method = 'GET') {
    return new Promise((resolve, reject) => {
      axios({
        method: method,
        url:url,
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error)
      })
    })
  },

  fetchPost(url,params, method = 'POST'){
    return new Promise((resolve, reject) => {
      axios({
        method: method,
        url:url,
        data:params,
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error)
      })
    })
  },

  fetchPut(url,params, method = 'PUT'){
    return new Promise((resolve, reject) => {
      axios({
        method: method,
        url:url,
        data:params,
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error)
      })
    })
  },

  fetchDelete(url, method = 'DELETE'){
    return new Promise((resolve, reject) => {
      axios({
        method: method,
        url:url,
      }).then((response) => {
        resolve(response.data)
      }).catch((error) => {
        reject(error)
      })
    })
  },
}

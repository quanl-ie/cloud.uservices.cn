<?php

namespace app\models;

use Yii;
use app\models\OrderRelation;
use yii\data\Pagination;
/**
 * This is the model class for table "facilitator_order_remind".
 *
 * @property string $id 通知主键
 * @property int $order_id 订单id
 * @property int $facilitator_id 服务商id
 * @property string $title  通知标题
 * @property string $content 通知内容
 * @property int $order_status 订单状态0：订单取消,2：等待接单, 3：等待服务（已接单）, 4：服务中
 * @property int $status 是否读取   1未读  |  2 已读
 * @property int $is_del 是否删除  1 未删  |   2   已删
 * @property int $control_query 控制查询   1为未查   2为已查
 * @property int $created_at 创建日期
 * @property int $updated_at 修改时间
 */
class OrderRemind extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufactor_order_remind';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'facilitator_id', 'order_status', 'status', 'is_del','control_query', 'created_at', 'updated_at'], 'integer'],
            [['status'], 'required'],
            [['title','content'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'facilitator_id' => 'Facilitator ID',
            'title' => 'title',
            'content' => 'Content',
            'order_status' => 'Order Status',
            'status' => 'Status',
            'is_del' => 'Is Del',
            'control_query' => 'control_query',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    /**
    * 函数用途描述：查询订单通知状态
    * @date: 2017年9月8日 下午3:34:21
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function getStatus($order_id = 0) {
        $data = OrderRemind::find()
                    ->select(['id','order_id','order_status'])
                    ->where(['order_id'=>$order_id,'is_del'=>1])
                    ->asArray()->one();
        return $data;
    }
    /**
     * 分页显示
     * @author lxq <lxq@c-ntek.com>
     * @param string $query 初始查询条件
     * @date 2017-9-11 20:00
     * @return mixed
     */
    public static function RemindPagination($query)
    {
        // 得到文章的总数（但是还没有从数据库取数据）
        $count = $query->count();
        // 使用总数来创建一个分页对象        
        $pagination = new Pagination(['totalCount' => $count,'pageSize'=>3]);
        // 使用分页对象来填充 limit 子句并取得文章数据
        $orderRemind = $query->offset($pagination->offset)
            ->orderBy('created_at DESC')
            ->limit($pagination->limit)
            ->all();
        $data = [];
        $data['pagination'] = $pagination;
        $data['orderRemind']      = $orderRemind;
        return $data;
    }
    /**
     * @desc 通过服务商用户id查询服务商id
     * @author lxq <lxq@c-ntek.com>
     * @param int $userId 当前用户id
     * @date 2017-9-11 20:00
     * @return mixed
     */
    public static function FacilitatorId($userId) {
        $facilitator_id = OrderRelation::find()
                    ->select(['facilitator_id'])
                    ->where(['facilitator_user_id' => $userId])
                    ->one();
        return $facilitator_id['facilitator_id'];
    }
    /**
     * @desc 通过服务商用户id查询服务商id
     * @author lxq <lxq@c-ntek.com>
     * @param int $userId 当前用户id
     * @date 2017-9-11 20:00
     * @return mixed
     */
    public static function OrderRemindList($facilitator_id) {
        $orderRemind = OrderRemind::find()
                ->select(['id','order_id','order_status','title','created_at'])
                ->where(['facilitator_id' => $facilitator_id,'is_del' =>1 ,'control_query' =>1])
                ->orderBy('created_at DESC')
                ->asArray()
                ->all();
        $orderData = [];
        if(count($orderRemind)>0){
            $orderData['data'] = $orderRemind[0];
            foreach ($orderRemind as $key => $val) {
                $orderData['id'][] = $val['id'];
            }
        }
        return $orderData;
    }
    /**
     * @desc 修改已弹出提示的消息不在弹出
     * @author lxq <lxq@c-ntek.com>
     * @param int $userId 当前用户id
     * @date 2017-9-11 20:00
     * @return mixed
     */
    public static function ControlQuery($id)
    {
        foreach ($id as $val) {
            $model = OrderRemind::find()->where(['id'=>$val])->one();
            $model->control_query = 2;
            $model->updated_at = time();
            $model->save(false);
        }
    }
    
}

<?php
namespace webapp\models;
use Yii;
use yii\db\ActiveRecord;

class Role extends BaseModel
{

    public static function tableName()
    {
        return 'role';
    }

    //规则
    public function rules()
    {
        return [
            [['name','menu_ids','department_pids','data_auth_id'],'required'],
            [['email'],'email'],
            [['name'],'string','max' => 255],
            [['description'],'string','max' => 1000],
            [['menu_ids'],'string','max'=>5000]
        ];
    }
    
    public function attributeLabels()
    {
        return array(
            'name'            => '角色名称',
            'description'     => '备注',
            'department_pids' => '所有父 ids',
            'data_auth_id'    => '数据权限',
        );
    }

    /**
     * 查询角色
     * @return array
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date 2017-08-17
     */
    public static function getList($where,$flag=1)
    {

        $result = [];
        $res = self::find()->where(['status'=>1])->andWhere($where)->asArray()->all();

        if($flag){
            foreach ($res as $val)
            {
                $result[$val['id']] = $val['name'];
            }
            return $result;
        }else{
            return $res;
        }

    }

    public static function getOne($where = [])
    {
        return self::find()->where($where)->asArray()->one();
    }

    public static function getPageList($page = 1,$pageSize = 10,$where = [])
    {
        $db = self::find();
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }

        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['create_time'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    /**
     * 根据角色id查询数据权限
     * @author liquan
     * @date 2018-6-7
     * @return array
     * @param str role_ids  Int menu_id
     */
    public static function getDataAuth($role_ids,$menu_id='',$flag=0)
    {
        if(!$role_ids){
            return [];
        }
        $where  = 'id in ('.$role_ids.')';
        if(!$role_ids){
            return [];
        }
        if($menu_id !== ''){
            $where  = 'FIND_IN_SET('.$menu_id.',menu_ids) and id in ('.$role_ids.')';
        }else{
            $where = 'id in ('.$role_ids.')';
        }
        $dataRes = self::find()->where($where)->select('id,data_auth_id')->asArray()->all();
        if($flag){
            return $dataRes;
        }else{
            return array_column($dataRes,'data_auth_id','id');
        }
        return [];

    }

    /**
     * 查出角色
     * @param $departmentid
     * @return array|\yii\db\ActiveRecord[]
     * @author xi
     * @date 2018-6-7
     */
    public static function getRoseByDepartmentId($departmentid)
    {
        $dataArr = self::find()->where(['department_id'=>$departmentid,'status'=>1])
                ->select('id,name')
                ->limit(1000)
                ->asArray()->all();

        return $dataArr;
    }
}
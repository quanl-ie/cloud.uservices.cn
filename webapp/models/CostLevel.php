<?php

namespace webapp\models;
set_time_limit(800);
use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;
use webapp\models\CostLevelItem;
use webapp\models\CostLevelDetail;


class CostLevel extends BaseModel
{
    public static function getDb() {
        return Yii::$app->order_db;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cost_level';
    }
    /**
     * 获取单条数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @inheritdoc
     */
    public static function getOne($where = [],$andWhere=[])
    {
        return self::find()->where($where)->andWhere($andWhere)->one();
    }
    /**
     * 获取多条数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @inheritdoc
     */
    public static function getList($where = [])
    {
        return self::find()->where(['department_id'=>BaseLogic::getDepartmentId(),'status'=>1])->andWhere($where)->all();
    }
    /**
     * 获取商家列表
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getManufactorList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    public static function getCostIdList($where = [],$andWhere = [])
    {
        return self::find()->where($where)->andWhere($andWhere)->asArray()->all();
    }






    //查询服务商制定的收费规则
    public static function getCheckList($merchantId,$src_type,$service_area_id,$brand_id,$class_id,$service_type)
    {
        $data =  self::find()
                ->where(['status'=>1,'src_id'=>$merchantId,'src_type'=>$src_type])
                ->andWhere(['like','service_area',$service_area_id])
                ->andWhere(['or',"brand_id = '$brand_id'","brand_id = 0"])
                ->andWhere(['or',"class_id = '$class_id'","class_id = 0"])
                ->andWhere(['or',"type_id = '$service_type'","type_id = 0"])
                ->asArray()->all();
                //->createCommand()->getRawSql();
        if($data){
            $cost_levels = implode(',',array_column($data, 'id'));
            return $cost_levels;
        }
        return false;
    }
    /**
     * 根据接收的数据添加入库
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $costLevelData
     * @param type $costLevelDetailData
     * @param type $costLevelItemData
     * @return type
     * @throws \Exception
     */
    public static function add($costLevelData,$costLevelDetailData,$costLevelItemData)
    {
        set_time_limit(800);
        //实例化客户表模型
        $costLevel = new self();
        //处理客户表数据
        foreach ($costLevelData as $key => $val) {
            $costLevel->$key = $val;
        }
        //开启事务
        $db = self::getDb();
        $transaction = $db->beginTransaction();
        try {
            //收费标准表
            $costLevel->save(false);
            foreach ($costLevelDetailData as $key => $val) {
                $costLevelDetail = new CostLevelDetail();
                foreach ($val as $k1=>$v1){
                    $costLevelDetail->cost_level_id = $costLevel->id;
                    $costLevelDetail->$k1 = $v1;
                }
                if($costLevelDetail->save(false))
                {
                    //收费标准-项目明细表表
                    $costLevelItem = new CostLevelItem();
                    $costLevelItem->cost_level_id = $costLevel->id;
                    $costLevelItem->level_detail_id = $costLevelDetail->id;
                    foreach ($costLevelItemData as $key1 => $val1) {
                        $costLevelItem->$key1 = $val1;
                    }
                    $costLevelItem->save(false);
                }
            }
            $transaction->commit();
            return $costLevelDetail->id;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    /**
     * 根据收费项目id修改收费项目为禁用状态
     * @param type $id
     * @return boolean
     */
    public static function del($idArr)
    {
        $costLevelItem = CostLevelItem::getList(['id'=>$idArr]);
        if ($costLevelItem) {
            foreach ($costLevelItem as $key=>$val){
                $re = CostLevelItem::findOne(['id'=>$val['id']])->updateAttributes(['status'=>2]);
            }
            return true;
        }
        return false;
    }
    /**
     * 根据接收的数据进行修改或添加
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $costLevelDetailData
     * @param type $costLevelItemData
     * @return boolean
     * @throws \Exception
     */
    public static function edit($costLevelData,$costLevelDetailData,$costLevelItemData,$level_detail_ids_del)
    {
        $db = self::getDb();
        $transaction = $db->beginTransaction();
        try {
                if(!empty($level_detail_ids_del))
                {
                    foreach ($level_detail_ids_del as $v){
                        CostLevelDetail::findOne(['id'=>$v])->delete();
                    }
                    $item = CostLevelItem::getList(['level_detail_id'=>$level_detail_ids_del]);
                    foreach ($item as $val) {
                        CostLevelItem::findOne(['id'=>$val['id']])->delete();
                    }

                }

                //1 修改收费标准表数
                $costLevel = self::getOne(['id'=>$costLevelData['id']]);
                unset($costLevelData['id']);
                foreach ($costLevelData as $key => $val) {
                    $costLevel->$key = $val;
                }
                $costLevel->save(false);

                //2 修改或添加收费标准详情表数据
            $costLevelDetailIds = [];
                if (isset($costLevelDetailData) && isset($costLevelItemData))
                {

                    foreach ($costLevelDetailData as $key => $val) {

                        if (isset($val['id'])) {
                            $costLevelDetail = CostLevelDetail::getOne(['id' => $val['id']]);
                            unset( $val['id']);

                        }else{
                            $costLevelDetail = new CostLevelDetail();
                        }
                        $costLevelDetail->cost_level_id = $val['cost_level_id'];
                        $costLevelDetail->brand_id = $val['brand_id'];
                        $costLevelDetail->class_id = $val['class_id'];
                        $costLevelDetail->type_id = $val['type_id'];
                        $costLevelDetail->service_area = $val['service_area'];
                        $costLevelDetail->create_time = $val['create_time'];
                        $costLevelDetail->update_time = $val['update_time'];
                        $costLevelDetail->department_id = $val['department_id'];
                        $costLevelDetail->department_top_id = $val['department_top_id'];
                        $costLevelDetail->status = $val['status'];
                        if($costLevelDetail->save(false)){
                            //$costLevelDetailIds[]=$costLevelDetail->id;
                            $costLevelItem = new CostLevelItem();
                            foreach ($costLevelItemData as $k => $v) {
                                $costLevelItem->$k = $v;
                            }
                            $costLevelItem->level_detail_id = $costLevelDetail->id;
                            $costLevelItem->save(false);
                        }

                    }
                }
            $transaction->commit();
            //return $costLevelDetailIds;
            return $costLevelDetail->id;
//
//            foreach ($costLevelDetailData as $key => $val) {
//                $costLevelDetail->$key = $val;
//            }
//            $costLevelDetail->save(false);
//
//            //3 修改或添加收费标准收费项目表数据
//            if (isset($costLevelItemData['id'])) {
//                $costLevelItem = CostLevelItem::getOne(['id'=>$costLevelItemData['id']]);
//                unset($costLevelItemData['id']);
//            }else{
//                $costLevelItem = new CostLevelItem();
//            }
//            $costLevelItem->level_detail_id = $costLevelDetail->id;
//            foreach ($costLevelItemData as $key => $val) {
//                $costLevelItem->$key = $val;
//            }
//            $costLevelItem->save(false);


        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    /**
     * 提交收费项目数据修改所有状态为可用
     * @param type $id
     * @param type $levelTitle
     * @param type $serviceArea
     * @return boolean
     * @throws \Exception
     */
    public static function submit($id,$levelTitle,$serviceArea)
    {
        //print_r($serviceArea);die;
        $db = self::getDb();
        $transaction = $db->beginTransaction();
        try {
            $time = time();
            // 收费标准表
            $costLevel = self::getOne(['id'=>$id]);
            $costLevel->level_title  = $levelTitle;
            //$costLevel->service_area = $serviceArea;
            $costLevel->update_time  = $time;
            $costLevel->status       = 1;
            $costLevel->save(false);
            // 收费标准详情表
            $db->createCommand()->update(CostLevelDetail::tableName(), ['status' => 1,'update_time'=>$time], ['cost_level_id' => $id,'status'=>3])->execute();
            // 收费标准收费项目表
            $db->createCommand()->update(CostLevelItem::tableName(), ['status' => 1,'update_time'=>$time], ['cost_level_id' => $id,'status'=>3])->execute();

            //建立关系
            CostLevelRelation::add($id);

            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    //根据条件获取收费标准 2018-3-1
    public static function getScale($src_type,$src_id,$service_area_id,$brand_id,$class_id,$service_type)
    {
        $where = [
            'a.src_type' => $src_type,
            'a.src_id'   => $src_id,
            'b.brand_id' => [$brand_id,0],
            'b.class_id' => [$class_id,0],
            'b.type_id'  => [$service_type,0],
            'a.status'   => 1,
            'b.status'   => 1
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a ')
            ->innerJoin([CostLevelDetail::tableName() . ' as b'],' a.id = b.cost_level_id')
            ->where($where)
            ->andWhere("FIND_IN_SET('$service_area_id',service_area)")
            ->select('b.id')
            //->select('a.id')
            ->asArray()
            ->one();
        if($query)
        {
            return $query['id'];
        }
    }
    
    public static function getIndex($map,$where,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->where($map);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['create_time'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

}

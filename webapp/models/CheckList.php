<?php
namespace webapp\models;

use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;
use webapp\models\CheckListCost;
use webapp\models\CheckListDetail;

class CheckList extends BaseModel
{



    public static function getDb() {
        return Yii::$app->order_db;
    }

    
    public static function tableName()
    {
        return 'check_list';
    }
    /**
     * 获取结算单
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getOne($where = [])
    {
        return self::find()->where(['src_type'=>self::SRC_FWS,'src_id'=>BaseLogic::getManufactorId()])->andWhere($where)->asArray()->one();
    }
    /**
     * 根据条件获取所有结算单
     * @param type $where
     * @return type
     */
    public static function getAll($status=3) {
        $db = self::find()->where(['src_type'=> BaseModel::SRC_FWS,'src_id'=>BaseLogic::getManufactorId(),'status'=>$status]);
        $count = $db->count();
       // echo $db->CreateCommand()->getRawSql();echo '<br/>';
        $sum = $db->sum('amount_real');
        $sum = $sum?$sum:0;

       return ['count'=>$count,'sum'=>$sum];
    }
    /**
     * 获取结算单列表
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getList($param)
    {
        $db = self::find();
        if(!isset($param['pageSize'])){
            $pageSize = 1000;
        }else{
            $pageSize = $param['pageSize'];
        }

        if(!isset($param['currentPage'])){
            $page = 0;
        }else{
            $page = $param['currentPage']-1;
        }

        unset($param['currentPage']);
        unset($param['pageSize']);



        $db = self::find();
        foreach($param as $k=>$v){
            if(!empty($v)){
                $db->andwhere([$k=>$v]);
            }
        }
        $count = $db->count();
        $count = $db->count();
        $totalPage = ceil($count/$pageSize);
        $start = $page*$pageSize;
        $limit = $pageSize;
        $list =$db->offset($start)
            ->limit($pageSize)
            ->asArray()
            ->all();
        return [
            'page'       => $page+1,
            'totalCount' => $count,
            'totalPage'  => $totalPage,
            'list'       => $list
        ];

    }


    /**
     * 添加
     * @author chengjuanjuan <chengjuanjuan@c-ntek.com>
     * @date 2017-12-15
     */
/*    public static function add($data)
    {
        $model = new self();
        foreach($data as $k=>$v){
            $model->$k=$v;
        }
        $res = $model->save();
        if($res){
            return $model->id;
        }

    }*/

    //数据提交
    public static function add($data){

        $orderList = $data['order_list'];
        $checkList =  $data['check_list'];
        $model = new self();
        $model->number = $checkList['order_number'];
        $model->amount = $checkList['amount'];
        $model->amount_expect = $checkList['amount_expect'];
        $model->status = 0;
        $model->src_id = BaseLogic::getManufactorId();
        $model->src_type = BaseModel::SRC_FWS;
        $model->sj_id = $data['data']['sj_id'];
        $model->company = $data['data']['company'];
        $model->begin_time = strtotime($data['data']['begin_time']);
        $model->end_time = strtotime($data['data']['end_time']);
        $model->create_time = time();
        $model->update_time = time();
        $model->status=2;
        $model->save(false);
        $id = $model->id;
        if($id){

            $costItem =[];
            $costDetail =[];
            foreach ($orderList as $order_id=>$info)
            {
                $tmpArr = $info['cost_info'];
                foreach ($tmpArr as $cost_info)
                {
                    $tmp = $cost_info;
                    $cost_tmp['cost_id'] =$tmp['cost_id'];
                    $cost_tmp['cost_name'] =$tmp['cost_name'];
                    $cost_tmp['amount'] =$tmp['divide_amount'];
                    $cost_tmp['checklist_id'] = $id;
                    $cost_tmp['order_id'] = $order_id;
                    $costItem[]=$cost_tmp;
                }
                $tmpArr = $info['detail_info'];
                $tmp = [];
                $tmp['order_no']= $tmpArr['order_no'];
                $tmp['order_id']= $order_id;
                $tmp['type_id']= $tmpArr['type_id'];
                if(isset($tmpArr['type_name'])||empty($tmpArr['type_name'])){
                    $type = \common\models\ServiceType::findOne(['id'=>$tmp['type_id']]);
                    $tmpArr['type_name'] = $type->title;
                }
                $tmp['type_name']= $tmpArr['type_name'];
                $tmp['class_id']= $tmpArr['class_id'];
                $tmp['class_name']= $tmpArr['class_name'];
                $tmp['brand_id']= $tmpArr['brand_id'];
                $tmp['brand_name']= $tmpArr['brand_name'];
                $tmp['finish_time']= $tmpArr['finish_time'];
                $tmp['amount']=$info['total_amount'];
                $tmp['checklist_id']=$id;
                $costDetail[]=$tmp;
            }
            //print_r($costItem);
            //print_r($costDetail);exit;
            $result = CheckListCost::add($costItem);
            $result = CheckListDetail::add($costDetail);
        }else{
            return [];
        }

    }


    public static function updateStatus($where,$status=1,$amountReal=0){
        $time = time();
        if($amountReal){
            return self::getDb()->createCommand()->update(self::tableName(), ['status' => $status,'amount_real'=>$amountReal,'update_time'=>$time], ['id' => $where])->execute();
        }
        //echo '<pre>';print_R($where);exit;
        self::getDb()->createCommand()->update(self::tableName(), ['status' => $status,'update_time'=>$time], ['id' => $where])->execute();
    }


    public static function updateOrderSettlementStatus($where,$status=2){
        $time = time();
        return self::getDb()->createCommand()->update('work_order', ['settlement_status' => $status], ['id' => $where])->execute();
    }
}
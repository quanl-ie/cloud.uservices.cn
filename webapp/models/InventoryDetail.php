<?php
namespace webapp\models;

use Yii;

class InventoryDetail extends BaseModel
{

    public static function tableName()
    {
        return 'inventory_detail';
    }

    /**
     * 盘点产品名细列表
     * @param $where
     * @param int $page
     * @param int $pageSize
     * @return array
     * @author xi
     */
    public static function getList ($where,$page=1,$pageSize=10)
    {
        $db = self::find();

        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }
                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }

        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1){
                $page = 1;
            }

            $db->select("id,prod_name,prod_num,prod_brand_name,prod_model,prod_unit,num,real_num,calc_num,remark,operator_user_id");
            $db->orderBy('id asc');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();

            $userArr = [];
            $userIds = array_unique(array_column($list,'operator_user_id'));
            $userRes = User::find()->where(['id'=>$userIds])->select('id,username')->asArray()->all();
            if($userRes){
                $userArr = array_column($userRes,'username','id');
            }

            $count = 1;
            foreach ($list as $key=>$val)
            {
                $list[$key]['tempId'] = (($page - 1) * $pageSize) + ($count);
                $count++;

                $list[$key]['username'] = '';
                if(isset($userArr[$val['operator_user_id']])){
                    $list[$key]['username'] = $userArr[$val['operator_user_id']];
                }
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

}
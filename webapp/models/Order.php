<?php

namespace webapp\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "tp_order".
 *
 * @property int $id 订单主键
 * @property string $order_id 订单编号
 * @property string $goods_title 服务项目
 * @property string $note 订单备注
 * @property int $brand_id 品牌ID
 * @property int $type_id 服务类型id
 * @property int $class_id 产品类型
 * @property int $open_city_id 服务城市
 * @property string $serve_type 服务类型：1安装|2维修|3保养
 * @property int $technician_id 服务技师
 * @property string $image 需求图片
 * @property int $total_price 订单总价
 * @property int $price 实付金额
 * @property int $coupon_id 使用优惠券（元）
 * @property int $invoice_status 订单开发票标识，0未开 1后台审核2已开
 * @property int $invoice_id 关联invoice_list表中id
 * @property int $order_status 0：订单取消, 1：等待支付 ,2：等待接单, 3：等待服务, 4：服务中, 5：待评价, 6：已评价
 * @property string $name 订单姓名
 * @property string $tel 订单电话
 * @property string $province 省份
 * @property string $city 城市
 * @property string $district 县区
 * @property string $address 详细地址
 * @property int $clear_time 取消时间
 * @property int $make_time 预约时间
 * @property int $start_work 开始服务时间
 * @property int $end_work 结束服务时间
 * @property int $is_edit 是否改期 1-改期 | 0-未改期
 * @property int $member_id 报修会员
 * @property string $end_note 完成备注
 * @property string $lat 高德gps纬度
 * @property string $lon 高德gps经度
 * @property string $latlon_address 中文地理位置
 * @property string $clear_reason 取消原因
 * @property int $is_pay 是否已支付：1-已支付；0-未支付
 * @property int $pay_time 支付时间
 * @property int $create_time 创建时间
 * @property int $update_time 更新时间
 * @property string $rescheduled_events 改期原因
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * 订单状态数组
     * @author lxq <lxq@c-ntek.com>
     * @date 2017-9-5 09:50
     * @return mixed
     */
    const STATUS_ARRAY = [
                            1=>'等待接单',
                            2=>'等待服务',
                            3=>'服务中',
                            4=>'已完成',
                            5=>'取消',
                        ];
    const BUY_TYPE = [
                            1=>'微信',
                            2=>'支付宝',
                            3=>'线下支付',
                        ];
    /**
     * 订单来源
     * @var unknown订单来源 1微信公众号 2移动浏览器 3PC端 4服务商
     */
    const ORDER_SOURCE = [
            0=>'1',  //微信公众号
            1=>'2',  //PC端
            2=>'3',  //移动浏览器
            3=>'4',  //服务商
    
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tp_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'type_id', 'class_id', 'open_city_id', 'order_status'], 'required'],
            [['brand_id', 'type_id', 'class_id', 'open_city_id', 'technician_id', 'total_price', 'price', 'coupon_id', 'invoice_status', 'invoice_id', 'order_status', 'clear_time', 'start_work', 'end_work', 'is_edit', 'member_id', 'is_pay', 'pay_time', 'create_time', 'update_time'], 'integer'],
            [['order_id', 'goods_title', 'note', 'image', 'name', 'province', 'city', 'district', 'address', 'lat', 'lon', 'latlon_address', 'clear_reason'], 'string', 'max' => 255],
            [['serve_type'], 'string', 'max' => 4],
            [['tel'], 'string', 'max' => 14],
            [['end_note'], 'string', 'max' => 11],
            [['rescheduled_events'], 'string', 'max' => 12],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '订单主键',
            'order_id' => '订单编号',
            'goods_title' => '服务项目',
            'note' => '订单备注',
            'brand_id' => '品牌ID',
            'type_id' => '服务类型id',
            'class_id' => '产品类型',
            'open_city_id' => '服务城市',
            'serve_type' => '服务类型：1安装|2维修|3保养',
            'technician_id' => '服务技师',
            'image' => '需求图片',
            'total_price' => '订单总价',
            'price' => '实付金额',
            'coupon_id' => '使用优惠券（元）',
            'invoice_status' => '订单开发票标识，0未开 1后台审核2已开',
            'invoice_id' => '关联invoice_list表中id',
            'order_status' => '0：订单取消, 1：等待支付 ,2：等待接单, 3：等待服务, 4：服务中, 5：待评价, 6：已评价',
            'name' => '订单姓名',
            'tel' => '订单电话',
            'province' => '省份',
            'city' => '城市',
            'district' => '县区',
            'address' => '详细地址',
            'clear_time' => '取消时间',
            'make_time' => '预约时间',
            'start_work' => '开始服务时间',
            'end_work' => '结束服务时间',
            'is_edit' => '是否改期 1-改期 | 0-未改期',
            'member_id' => '报修会员',
            'end_note' => '完成备注',
            'lat' => '高德gps纬度',
            'lon' => '高德gps经度',
            'latlon_address' => '中文地理位置',
            'clear_reason' => '取消原因',
            'is_pay' => '是否已支付：1-已支付；0-未支付',
            'pay_time' => '支付时间',
            'create_time' => '创建时间',
            'update_time' => '更新时间',
            'rescheduled_events' => '改期原因',
        ];
    }
    /**
     * 分页显示
     * @author lxq <lxq@c-ntek.com>
     * @date 2017-9-4 11:00
     * @return mixed
     */
    public static function OrderPagination($query)
    {
        // 得到文章的总数（但是还没有从数据库取数据）
        $count = $query->count();
        // 使用总数来创建一个分页对象        
        $pagination = new Pagination(['totalCount' => $count,'pageSize'=>3]);
        // 使用分页对象来填充 limit 子句并取得文章数据
        $order = $query->offset($pagination->offset)
            ->orderBy('create_time DESC')
            ->limit($pagination->limit)
            ->all();
        $data = [];
        $data['pagination'] = $pagination;
        $data['order']      = $order;
        return $data;
    }
    /**
	 * @desc 判断显示取消订单按钮
	 * @date 2017-09-04 14：00
	 * @author lxq < lxq@c-ntek.com >
	 * @return  mixed
	 */
    public static function UnRemove($id)
    {
        $status = [1,2];
        $order = Order::find()
                ->select('make_time,order_status,start_work')
                ->where(['id'=>$id])
                ->one();
        if ( in_array ($order->order_status, $status)) {
            return false;
        }else{
            return true;
        }
    }
    /**
	 * @desc 获取全部产品类型
	 * @date 2017-09-05 10：10
	 * @author lxq < lxq@c-ntek.com >
	 * @return  mixed
	 */
    public static function ServiceClassList()
    {
        return ServiceClass::find()
                ->select('id,title')
                ->where(['isdelete'=>0])
                ->asArray()
                ->all();
    }
    /**
	 * @desc 获取全部服务类型
	 * @date 2017-09-05 10：20
	 * @author lxq < lxq@c-ntek.com >
	 * @return  mixed
	 */
    public static function ServiceTypeList()
    {
        return ServiceType::find()
                ->select('id,title')
                ->where(['isdelete'=>0])
                ->asArray()
                ->all();
    }
    /**
	 * @desc 关联产品类型
	 * @date 2017-09-05 10：30
	 * @author lxq < lxq@c-ntek.com >
	 * @return  mixed
	 */
    public  function getServiceClass()
    {
        return $this->hasOne(ServiceClass::className(),['id'=>'class_id']);
    }
    /**
	 * @desc 关联品牌
	 * @date 2017-09-05 10：30
	 * @author lxq < lxq@c-ntek.com >
	 * @return  mixed
	 */
    public  function getServiceBrand()
    {
        return $this->hasOne(ServiceBrand::className(),['id'=>'brand_id']);
    }
    /**
	 * @desc 关联服务类型
	 * @date 2017-09-05 10：30
	 * @author lxq < lxq@c-ntek.com >
	 * @return  mixed
	 */
    public  function getServiceType()
    {
        return $this->hasOne(ServiceType::className(),['id'=>'type_id']);
    }
    /**
	 * @desc 关联服务商订单关系
	 * @date 2017-09-05 11：30
	 * @author lxq < lxq@c-ntek.com >
	 * @return  mixed
	 */
    public  function getOrderRelation()
    {
        return $this->hasOne(OrderRelation::className(),['id'=>'type_id']);
    }
    /**
	 * @desc 关联技师信息
	 * @date 2017-09-05 11：30
	 * @author lxq < lxq@c-ntek.com >
	 * @return  mixed
	 */
    public  function getStoreMember()
    {
        return $this->hasOne(StoreMember::className(),['id'=>'technician_id']);
    }
    /**
	 * @desc 转换时间
	 * @date 2017-09-05 17：00
	 * @author lxq < lxq@c-ntek.com >
	 * @return  mixed
	 */
    public static function Time($time,$status)
    {
        $nowtime=date($time);
        if ($status == 1) {
            return strtotime($nowtime)+86399;
        }
        return strtotime($nowtime);
    }  
    /**
     * 函数用途描述：获取品牌
     * @date: 2017年9月1日 下午4:48:01
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function getBrand($id=null)
    {
        $brandData = ServiceBrand::find()
        ->select('id,title')
        ->andWhere(['status'=>'1','isdelete'=>'0']);
        $brand = $brandData->asArray()->all();
        return $brand;
    }
    /**
     * 函数用途描述：获取服务类型
     * @date: 2017年9月1日 下午4:48:15
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function getType()
    {
        $typeData = ServiceType::find()
        ->select('id,title')
        ->andWhere(['isdelete'=>'0','status'=>'1']);
        $type = $typeData->asArray()->all();
        return $type;
    }
    /**
     * 函数用途描述:获取当天开始时间
     * @date: 2017年9月5日 上午9:42:56
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function getTime() {
        $nowTime = time();
        //$setTime = strtotime(date('Ymd'))+3600*8;
        $setTime = date('Y-m-d 08:00:00', time());
        $startTime = '';
        if($nowTime >= $setTime){
            return $startTime = date('Y-m-d H:i:s',$nowTime);
        }else {
            return $startTime = date('Y-m-d H:i:s',$setTime);
        }
    }
    /**
     * 函数用途描述：判断提交的数据必填项是否为空
     * @date: 2017年9月7日 下午4:20:30
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function getCheck($data){
        if(is_array($data)){
            foreach($data as $k => $v){
                if($k == 'order_class_s_id'){
                    unset($data[$k]);
                }
                if($k == 'order_note'){
                    unset($data[$k]);
                }
                if($k == 'order_make_time'){
                    unset($data[$k]);
                }
            }
            foreach ($data as $k=>$v){
                if($v == ''){
                    return false;
                }
            }
            return true;
        }
    
    }
    /**
     * 函数用途描述:生成订单号
     * @date: 2017年9月6日 下午4:35:08
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function getOrderNumber($token=null,$source=null,$pay_type=null)
    {
        //获取年月日
        $date = date('Ymd');
        //获取SN
        $sn  = '';
        if(!empty($token) && !empty($source)){
            $sn = substr(md5($token.$source),0,5);
        }
        //生成5位随机数
        $randNumber = mt_rand(10000,99999);
        $data =$date.$sn.$randNumber;
        return $data;
    }
    /**
    * 函数用途描述:获取订单信息
    * @date: 2017年9月11日 下午7:43:25
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function getOrderInfo($id=null) {
        $data = self::find()
                ->where(['id'=>$id])
                ->asArray()->one();
        return $data;
    }
    /**
	 * @desc 获取服务项目
	 * @date 2017-10-24 22：20
	 * @author lxq < lxq@c-ntek.com >
	 * @return  mixed
	 */
    public static function getGoodsTitle($class_id=null,$brand_id=null)
    {
        
    }
}

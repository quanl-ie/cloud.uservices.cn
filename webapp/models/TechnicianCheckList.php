<?php
namespace webapp\models;

use common\models\Work;
use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;
use webapp\models\Technician;
use webapp\models\TechnicianCheckListCost;
use webapp\models\TechnicianCheckListDetail;

class TechnicianCheckList extends BaseModel
{
    public static function getDb() {
        return Yii::$app->db;
    }

    
    public static function tableName()
    {
        return 'technician_checklist';
    }

    /**
     * 获取结算单
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getOne($where = [])
    {
        return self::find()->where(['src_type'=>self::SRC_FWS,'src_id'=>BaseLogic::getManufactorId()])->andWhere($where)->asArray()->one();
    }
    /**
     * 根据条件获取结算单数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getAll($status=3) {
        $db = self::find()->where(['src_type'=> BaseModel::SRC_FWS,'src_id'=>BaseLogic::getManufactorId(),'status'=>$status]);
        $count = $db->count();
        $sum = $db->sum('amount_real');
        $sum = $sum?$sum:0;
        return ['count'=>$count,'sum'=>$sum];
    }

    /**
     * 统计技师结算金额
     * @return int|mixed
     * @author xi
     * @date 2018-4-19
     */
    public static function getTotalCostAmount()
    {
        $sum = 0;
        $count = [];

        $where = [
            'a.src_id'          => BaseLogic::getManufactorId(),
            'a.is_calc'         =>1,
            'a.`status`'        => 4,
            'a.cancel_status'   => 1,
            'settlement_status' => 1
        ];
        $query = Work::find()
            ->from(Work::tableName() . ' as a')
            ->innerJoin(['`'.WorkCostDevide::tableName().'` as b '], 'a.work_no = b.work_no')
            ->where($where)
            ->select(" `b`.`divide_amount`/10000 as 'divide_amount',`a`.`order_no`, `a`.`work_no` ")
            ->asArray()
            ->all();


        if($query)
        {
            foreach ($query as $val){
                $count[$val['work_no']] = $val['work_no'];
                $sum = bcadd($sum,$val['divide_amount'],2);
            }
        }
        return ['count'=>count($count),'sum'=>$sum];
    }

    /**
     * 获取结算单列表
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getList($param = [])
    {
        $db = self::find();
        $pageSize = $param['pageSize'];
        if(!isset($param['currentPage'])){
            $page = 0;
        }else{
            $page = $param['currentPage']-1;
        }

        unset($param['currentPage']);
        unset($param['pageSize']);



        $db->from(self::tableName() . ' as a');
        $db->select('a.id,a.amount,a.amount_real,a.begin_time,a.number,tech_id,b.name as technician_name,b.mobile as technician_mobile,create_time,a.cost_oprate_id,cost_time,check_id,check_time');
        $db->innerJoin([technician::tableName() . ' as b'],'a.tech_id = b.id');
        if(!empty($param['start_time'])){
            $db->andWhere(['>=','process_time',$param['start_time']]);
            unset($param['start_time']);
        }
        if(!empty($param['end_time'])){
            $db->andWhere(['<=','process_time',$param['end_time']]);
            unset($param['end_time']);
        }
        foreach($param as $k=>$v){
            if(!empty($v)){
                $db->andwhere([$k=>$v]);
            }
        }

        $db->createCommand()->getRawSql();
        $count = $db->count();
        $totalPage = ceil($count/$pageSize);
        $start = $page*$pageSize;
        $limit = $pageSize;
        $list =$db->offset($start)
            ->limit($pageSize)
            ->asArray()
            ->all();
        return [
            'page'       => $page+1,
            'totalCount' => $count,
            'totalPage'  => $totalPage,
            'list'       => $list
        ];
    }

    //数据提交
    public static function add($data){
        $technicians = $data['technicianList'];
        foreach($technicians as &$v){
            $model = new self();
            $model->tech_id = $v['tech_id'];
            $model->number = $v['work_number'];
            $model->amount = $v['work_amount'];
            $model->status = 0;
            $model->begin_time = $data['begin_time'];
            $model->end_time = $data['end_time'];
            $model->src_id = BaseLogic::getManufactorId();
            $model->src_type = BaseModel::SRC_FWS;
            $model->create_time = time();
            $model->update_time = time();
      /*      echo '<pre>';
            print_R($model);exit;*/
            $model->save(false);
            $id = $model->id;
            foreach($v['cost_items'] as &$value){
                $value['checklist_id'] = $id;
            }
            TechnicianCheckListCost::add($v['cost_items']);

            foreach($data['workList'] as &$vv){
                if($vv['tech_id']==$v['tech_id']){
                    $vv['checklist_id'] = $id;
                    $detailList[] = $vv;
                }
            }
            TechnicianCheckListDetail::add($detailList);
            unset($v['cost_items']);
            $v['checklist_id'] = $id;
            unset($id);
            unset($detailList);
        }
        return $technicians;
    }


    public static function updateStatus($where,$status=1,$amountReal=0){
        $time = time();
        if($amountReal>0){
            return self::getDb()->createCommand()->update(self::tableName(), ['status' => $status,'amount_real'=>$amountReal,'update_time'=>$time,'check_id'=>Yii::$app->user->id,'check_time'=>time()], ['id' => $where])->execute();
        }else{
            if($status==2){
                return self::getDb()->createCommand()->update(self::tableName(), ['status' => $status,'update_time'=>$time,'check_id'=>Yii::$app->user->id,'check_time'=>time()], ['id' => $where])->execute();
            }elseif($status==3){
                return self::getDb()->createCommand()->update(self::tableName(), ['status' => $status,'update_time'=>$time,'cost_oprate_id'=>Yii::$app->user->id,'cost_time'=>time()], ['id' => $where])->execute();
            }
            self::getDb()->createCommand()->update(self::tableName(), ['status' => $status,'update_time'=>$time,'report_id'=>Yii::$app->user->id,'create_time'=>time()], ['id' => $where])->execute();
            $list = TechnicianCheckListDetail::getList(['in','checklist_id',$where]);
            //结算单申请成功时候，修改工单结算状态
            $workIds = array_column($list,'work_id');
            return    self::updateOrderSettlementStatus($workIds);

        }
        //echo '<pre>';print_R($where);exit;
    }

    public static function updateOrderSettlementStatus($where,$status=2){
        $time = time();
        return WorkForm::getDb()->createCommand()->update('work', ['settlement_status' => $status], ['id' => $where])->execute();
    }


}
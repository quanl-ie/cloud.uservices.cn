<?php
namespace webapp\models;

use Yii;
use yii\base\Model;
class UserForm extends Model
{
    public $username;
    public $old_pwd;
    public $new_pwd;
    public $verify_pwd;
    public $verifyCode;
    public $mobile;
    public $code;

    //规则
    public function rules()
    {
        return [
            [['username','mobile','old_pwd','new_pwd','verify_pwd','code'],'required'],
            [['new_pwd'],'string','min'=>6,'max'=>15],
            [['verify_pwd'],'string','min'=>6,'max'=>15],
            ['verify_pwd', 'compare', 'compareAttribute'=>'new_pwd', 'message'=>'两次密码不一致'],
            ['verifyCode', 'captcha','captchaAction'=>'user/captcha'],
            ['mobile','match','pattern'=>'/^1[3456789]\d{9}$/','message'=>'手机号格式不正确'],
            ['code','number','min'=>100000,'max'=>999999],
        ];
    }



    public function attributeLabels()
    {
        return array(
            'name'       => '用户名',
            'old_pwd'    => '原密码',
            'new_pwd'    => '新密码',
            'verify_pwd' => '确认密码',
            'mobile'     => '手机号',
            'code'       => '验证码',
            'verifyCode' => '验证码'
        );
    }




}

<?php
namespace webapp\models;

use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;

class TechnicianCheckListCost extends BaseModel
{
    public static function getDb() {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'technician_checklist_cost';
    }


    public static function add($data){
        return $res =self::getDb()->createCommand()->batchInsert(self::tableName(), ['cost_id','cost_name','amount','checklist_id'],$data)->execute();
    }


    public static function getList($where){
        return  self::find()->where($where)->asArray()->all();
    }


}
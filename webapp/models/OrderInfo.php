<?php

namespace webapp\models;

use Yii;
use yii\db\ActiveRecord;

class OrderInfo extends BaseModel
{
    public static function getDb() {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'order_info_tmp';
    }

/*
 * 获取订单销售情况数据
 * */
    public static function getOrderSaleInfo($begin_time,$end_time)
    {
        $all_total_num = 0;
        $finish_total_num = 0;
        $cancel_total_num =0;
        $result = null;
        $tmp = [];
        //$begin_time = '2017-11-1';
        //$end_time = '2017-12-25';
        //新增订单
        $sql = "select count(*) as num,create_date from order_info_tmp where create_date BETWEEN '".$begin_time."' and '".$end_time."'   group by create_date order by create_date  ";
        $data_list = Yii::$app->order_db->createCommand($sql)->queryAll();
        if(is_array($data_list)){
            foreach ($data_list as $data)
            {
                if(is_array($data)){
                    $date = $data['create_date'];
                    $num = $data['num'];
                    $result[$date]['all'] = $num;
                    $all_total_num +=$num;
                }
            }
        }

        //取消订单
        $sql = "select count(*) as num,create_date from order_info_tmp where status=6 and create_date BETWEEN '".$begin_time."' and '".$end_time."'   group by create_date order by create_date  ";
        $data_list = Yii::$app->order_db->createCommand($sql)->queryAll();
        if(is_array($data_list)){
            foreach ($data_list as $data)
            {
                if(is_array($data)){
                    $date = $data['create_date'];
                    $num = $data['num'];
                    $result[$date]['cancel'] = $num;
                    $cancel_total_num +=$num;
                }
            }
        }

        //完成订单
        $sql = "select count(*) as num,create_date from order_info_tmp where status=5 and create_date BETWEEN '".$begin_time."' and '".$end_time."'   group by create_date order by create_date  ";
        $data_list = Yii::$app->order_db->createCommand($sql)->queryAll();
        if(is_array($data_list)){
            foreach ($data_list as $data)
            {
                if(is_array($data)){
                    $date = $data['create_date'];
                    $num = $data['num'];
                    $result[$date]['finish'] = $num;
                    $finish_total_num +=$num;
                }
            }
        }
        $type = ['all','finish','cancel'];
        if($result){
            foreach ($result as $date=>$info)
            {
                foreach ($type as $v)
                {
                    if(array_key_exists($v,$info)){
                        $tmp[$date][$v] = $info[$v];
                    }else{
                        $tmp[$date][$v] = 0;
                    }
            
                }
            
            }
        }
        return ['total'=>['all'=>$all_total_num,'finish'=>$finish_total_num,'cancel'=>$cancel_total_num],'list'=>$tmp];
    }


    /*
     * 获取品牌销售情况
     * */
    public static function getOrderBrandInfo($begin_time,$end_time,$brand_id_str)
    {
        $all_total_num = 0;
        $result = null;
        //$begin_time = '2017-11-1';
        //$end_time = '2017-12-25';
        //$brand_id_str = '1,2';
        $total_num_arr =null;

        //获取总的数量
        $sql = "select count(*) as num,create_date from order_info_tmp where  create_date BETWEEN '".$begin_time."' and '".$end_time."'   group by create_date  order by create_date  ";
        $data_list = Yii::$app->order_db->createCommand($sql)->queryAll();
        if(is_array($data_list)){
            foreach ($data_list as $data)
            {
                $total_num_arr[$data['create_date']] = $data['num'];
            }
        }

        //新增订单
        $sql = "select count(*) as num,create_date,brand,brand_name from order_info_tmp where brand in ($brand_id_str) and create_date BETWEEN '".$begin_time."' and '".$end_time."'   group by create_date,brand,brand_name order by create_date  ";
        $data_list = Yii::$app->order_db->createCommand($sql)->queryAll();
        if(is_array($data_list)){
            foreach ($data_list as $data)
            {
                if(is_array($data)){
                    $date = $data['create_date'];
                    $num = $data['num'];
                    $brand_id = $data['brand'];
                    $brand_name = $data['brand_name'];
                    $result[$date]['data'][$brand_id] = ['brand_name'=>$brand_name,'numb'=>$num];
                    $result[$date]['total_number'] = $total_num_arr[$date];
                    $all_total_num +=$num;
                }
            }
        }

        $tmp =null;
        $brand_id_arr = explode(',',$brand_id_str);
        if(!empty($result)){
            foreach ($result as $date=>$value)
            {
                foreach ($brand_id_arr as $brand)
                {
                    if(!array_key_exists($brand,$value['data'])){
                        $result[$date]['data'][$brand] = '';
                    }
                }
            }  
        }
        return ['total_number'=>$all_total_num,'list'=>$result];

    }

    /*
    * 获取服务类型销售情况
    * */
    public static function getOrderServiceTypeInfo($begin_time,$end_time,$type_id_str)
    {
        $all_total_num = 0;
        $result = null;
        //$begin_time = '2017-11-1';
        //$end_time = '2017-12-25';
        $total_num_arr =null;

        //获取总的数量
        $sql = "select count(*) as num,create_date from order_info_tmp where  create_date BETWEEN '".$begin_time."' and '".$end_time."'   group by create_date  order by create_date  ";
        $data_list = Yii::$app->order_db->createCommand($sql)->queryAll();
        if(is_array($data_list)){
            foreach ($data_list as $data)
            {
                $total_num_arr[$data['create_date']] = $data['num'];
            }
        }

        //新增订单
        $sql = "select count(*) as num,create_date,service_type from order_info_tmp where  create_date BETWEEN '".$begin_time."' and '".$end_time."'   group by create_date,service_type order by create_date  ";
        $data_list = Yii::$app->order_db->createCommand($sql)->queryAll();
        if(is_array($data_list)){
            foreach ($data_list as $data)
            {
                if(is_array($data)){
                    $date = $data['create_date'];
                    $num = $data['num'];
                    $service_type = $data['service_type'];
                    $result[$date]['data'][$service_type] = $num;
                    $result[$date]['total_number'] = $total_num_arr[$date];
                    $all_total_num +=$num;
                }
            }
        }
        
        

        $tmp =null;
        $service_type_arr = explode(',',$type_id_str);
        if(!empty($result)){
            foreach ($result as $date=>$value)
            {
                foreach ($service_type_arr as $service_type)
                {
                    if(!array_key_exists($service_type,$value['data'])){
                        $result[$date]['data'][$service_type] = 0;
                    }
                }
            } 
        }
        return ['total_number'=>$all_total_num,'list'=>$result];

    }


    /*
     * 获取商品类目销售情况
     * */

    public static function getOrderProdTypeInfo($begin_time,$end_time,$prod_class_id_str)
    {
        $all_total_num = 0;
        $result = null;
        //$begin_time = '2017-11-1';
        //$end_time = '2017-12-25';
        //$prod_class_id_str = '1,2,20';
        $total_num_arr =null;

        //获取总的数量
        $sql = "select count(*) as num,create_date from order_info_tmp where  create_date BETWEEN '".$begin_time."' and '".$end_time."'   group by create_date  order by create_date  ";
        $data_list = Yii::$app->order_db->createCommand($sql)->queryAll();
        if(is_array($data_list)){
            foreach ($data_list as $data)
            {
                $total_num_arr[$data['create_date']] = $data['num'];
            }
        }

        //新增订单
        $sql = "select count(*) as num,create_date,class_type,class_type_name from order_info_tmp where class_type in ($prod_class_id_str) and create_date BETWEEN '".$begin_time."' and '".$end_time."'   group by create_date,class_type,class_type_name order by create_date  ";
        $data_list = Yii::$app->order_db->createCommand($sql)->queryAll();
        if(is_array($data_list)){
            foreach ($data_list as $data)
            {
                if(is_array($data)){
                    $date = $data['create_date'];
                    $num = $data['num'];
                    $class_type_id = $data['class_type'];
                    $class_type_name = $data['class_type_name'];
                    $result[$date]['data'][$class_type_id] = ['class_type_name'=>$class_type_name,'numb'=>$num];
                    $result[$date]['total_number'] = $total_num_arr[$date];
                    $all_total_num +=$num;
                }
            }
        }
        $tmp =null;
        $prod_class_id_arr = explode(',',$prod_class_id_str);
        if(!empty($result)){
            foreach ($result as $date=>$value)
            {
                foreach ($prod_class_id_arr as $prod_class_id)
                {
                    if(!array_key_exists($prod_class_id,$value['data'])){
                        $result[$date]['data'][$prod_class_id] = '';
                    }
                }
            }
        }
        return ['total_number'=>$all_total_num,'list'=>$result];

    }


    /*
     * 获取商品类目销售情况
     * */

    public static function getOrderProdType2Info($begin_time,$end_time,$prod_class_id_str)
    {
        $all_total_num = 0;
        $result = null;
        //$begin_time = '2017-11-1';
        //$end_time = '2017-12-25';
        //$prod_class2_id_str = '1,2,50,30,88';
        $total_num_arr =null;

        //获取总的数量
        $sql = "select count(*) as num,create_date from order_info_tmp where  create_date BETWEEN '".$begin_time."' and '".$end_time."'   group by create_date  order by create_date  ";
        $data_list = Yii::$app->order_db->createCommand($sql)->queryAll();
        if(is_array($data_list)){
            foreach ($data_list as $data)
            {
                $total_num_arr[$data['create_date']] = $data['num'];
            }
        }

        //新增订单
        $sql = "select count(*) as num,create_date,class_type2,class_type2_name from order_info_tmp where class_type2 in ($prod_class_id_str) and create_date BETWEEN '".$begin_time."' and '".$end_time."'   group by create_date,class_type2,class_type2_name order by create_date  ";
        $data_list = Yii::$app->order_db->createCommand($sql)->queryAll();
        if(is_array($data_list)){
            foreach ($data_list as $data)
            {
                if(is_array($data)){
                    $date = $data['create_date'];
                    $num = $data['num'];
                    $class_type_id = $data['class_type2'];
                    $class_type_name = $data['class_type2_name'];
                    $result[$date]['data'][$class_type_id] = ['class_type_name'=>$class_type_name,'numb'=>$num];
                    $result[$date]['total_number'] = $total_num_arr[$date];
                    $all_total_num +=$num;
                }
            }
        }

        $tmp =null;
        $prod_class_id_arr = explode(',',$prod_class_id_str);
        if(!empty($result)){
            foreach ($result as $date=>$value)
            {
                foreach ($prod_class_id_arr as $prod_class_id)
                {
                    if(!array_key_exists($prod_class_id,$value['data'])){
                        $result[$date]['data'][$prod_class_id] = '';
                    }
                }
            }  
        }
       

        return ['total_number'=>$all_total_num,'list'=>$result];

    }





    //获取多条信息
    public static function getList($map,$where,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->where($map);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
    
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            //$db->leftJoin([AccountBehaviour::tableName() . ' as b'],' a.id = b.account_id');
            $db->select('a.*');
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['create_time'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            /*$new = $finish = $cancel = $finishList = $cancelList = $allList = [];
             if($list) foreach ($list as $key=>$val){
                if($val['status'] == 5){
                    $finish[$key] = $val;
                }
                if($val['status'] == 6){
                    $cancel[$key] = $val;
                }
                //print_r($val);exit;
            }
            $new['allTotal'] = $totalNum;
            $new['finishTotal'] = count($finish) ;
            $new['cancelTotal'] = count($cancel) ;
            //print_r($new);exit;
            //$sql_in="select count(*) from order_info_tmp where create_time < 2017-12-12 00:00:00 and create_time > 2017-12-20 23:59:59 and status=5 group by date_format(time,'%y-%m-%d')";
            //$sql_out="select sum(cash) as cashout from order_info_tmp where create_time < $dayend and create_time > $daybegin and type=1 group by date_format(time,'%y-%m-%d')";   
            $finishList['total'] = count($finish) ;
            $finishList['list'] = $finish;
            $cancelList['total'] = count($cancel) ;
            $cancelList['list'] = $cancel;
            $allList['total'] = $totalNum;
            $allList['list'] = $list;
            $new['all']=$allList;
            $new['finish']=$finishList;
            $new['cancel']=$cancelList;
            print_r($new);exit; */
            return [
                    'page'       => $page,
                    'totalCount' => $totalNum,
                    'totalPage'  => $totalPage,
                    'list'       => $list
            ];
        }
        else
        {
            return [
                    'page'       => $page,
                    'totalCount' => $totalNum,
                    'totalPage'  => 0,
                    'list'       => []
            ];
        }
    }
    //获取订单异常数据
    public static function getAbnormalOrdersInfo($begin_time,$end_time,$reason_ids)
    {
        $all_total_num = 0;
        $result = null;
        //$begin_time = '2017-11-1';
        //$end_time = '2017-12-25';
        $total_num_arr =null;
    
        //获取总的数量
        $sql = "select count(*) as num,create_date from order_info_tmp where  create_date BETWEEN '".$begin_time."' and '".$end_time."'   group by create_date  order by create_date  ";
        $data_list = Yii::$app->order_db->createCommand($sql)->queryAll();
        if(is_array($data_list)){
            foreach ($data_list as $data)
            {
                $total_num_arr[$data['create_date']] = $data['num'];
            }
        }
        // 异常订单：9上门超时、6响应超时、8指派超时、7服务商拒单
        $sql = "select count(*) as num,create_date,reason from order_info_tmp where status=6 and reason in ($reason_ids) and create_date BETWEEN '".$begin_time."' and '".$end_time."'   group by create_date,reason order by create_date  ";
        $data_list = Yii::$app->order_db->createCommand($sql)->queryAll();
        if(is_array($data_list)){
            foreach ($data_list as $data)
            {
                if(is_array($data)){
                    $date = $data['create_date'];
                    $num = $data['num'];
                    $reason_id = $data['reason'];
                    $result[$date]['data'][$reason_id] = $num;
                    /* $class_type_name = $data['reason'];
                    $result[$date]['data'][$reason_id] = ['reason'=>$class_type_name,'numb'=>$num]; */
                    $result[$date]['total_number'] = $total_num_arr[$date];
                    $all_total_num +=$num;
                }
            }
        }
        $tmp =null;
        $reason_arr = explode(',',$reason_ids);
        if(!empty($result)){
            foreach ($result as $date=>$value)
            {
                foreach ($reason_arr as $reason_type)
                {
                    if(!array_key_exists($reason_type,$value['data'])){
                        $result[$date]['data'][$reason_type] = 0;
                    }
                }
            }
        }
        
        return ['total_number'=>$all_total_num,'list'=>$result];
    }
    //异常订单类型信息
    public static function getReasonNames($id){
        /* $data = self::find()->select(['title'])->where("id in($ids)")->asArray()->all();
        $typeTitle = [];
        foreach ($data as $v) {
            $typeTitle[] = $v['title'];
        }
        return implode(',', $typeTitle);
         */
        
        /* $reasons = [
                '1'=>'商家取消',
                '2'=>'服务商取消',
                '3'=>'门店取消',
                '4'=>'用户取消',
                '5'=>'优服务平台取消',
                '6'=>'响应超时',
                '7'=>'服务商拒接',
                '8'=>'指派超时',
                '9'=>'上门超时'
        ]; */
        $reasons = [
                
                '9'=>'上门超时',
                '6'=>'响应超时',
                '8'=>'指派超时',
                '7'=>'服务商拒接'
                ];
        
        $data = isset($reasons[$id])?$reasons[$id]:'';
        if(empty($id)){
            $data = $reasons;
        }
        return $data;
    }
    
}

<?php
namespace webapp\models;

use Yii;

class ReportingCompanyOrder extends BaseModel
{
    public static function tableName()
    {
        return 'reporting_company_order';
    }

    public static function getList($map,$page,$pageSize)
    {
        $db = self::find();
        if (!empty($map)) {
            foreach ($map as $key=>$val){
                if(is_array($val)){
                    $db->andWhere([$val[0],$val[1],$val[2]]);
                }else{
                    $db->where([$key=>$val]);
                }
            }
        }
        $db->groupBy('date');
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum / $pageSize);
            if ($page < 1) {
                $page = 1;
            } else if ($page > $totalPage) {
                $page = $totalPage;
            }
            $db->select('date,sum(new_work_num) as new_work_num,sum(finish_work_num) as finish_work_num,sum(cancel_work_num) as cancel_work_num');
            $db->offset(($page - 1) * $pageSize);
            $db->limit($pageSize);
            $db->orderBy('date desc');
            $list = $db->asArray()->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        } else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
}


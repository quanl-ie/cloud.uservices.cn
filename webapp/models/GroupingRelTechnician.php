<?php
namespace webapp\models;

use common\helpers\Helper;
use common\models\Common;
use common\models\ScheduleSet;
use common\models\ScheduleSetOptionDetail;
use webapp\logic\BaseLogic;
use Yii;
use yii\db\ActiveRecord;

class  GroupingRelTechnician extends BaseModel
{
    public static function tableName()
    {
        return 'grouping_rel_technician';
    }

    //规则
    public function rules()
    {
        return [
            [['grouping_id','technician_id','job_title','create_time','update_time','status'],'required'],
            [['grouping_id','technician_id','job_title','create_time','update_time','status','department_top_id','department_id'],'number'],
        ];
    }


    /**
     * 查出符合的技师
     * @param $groupingId
     * @param $workType
     * @param $saleOrderId
     * @param $addressId
     * @param array $jobTitle
     * @param int $range
     * @param int $skill
     * @return array
     * @author xi
     * @date 2018-4-26
     */
    public static function searchTechnician($groupingId,$workType,$saleOrderId, $addressId,$plantime,$jobTitle = [1,2], $range = 0, $skill = 0,$departmentId = 0)
    {
        $result = [];

        if($groupingId>0)
        {
            $query = self::getGroupingTechnician($jobTitle,$groupingId);
        }
        else
        {
            if($jobTitle == 2){
                $query = [];
            }
            else {
                $defaultData = self::getDefaultTechnician(['store_id'=>$departmentId],1,1000,"a.id as technician_id,a.store_id");
                $query       = $defaultData['list'];
            }
        }

        if($query)
        {
            $technicianIds = array_column($query,'technician_id');
            $technicianArr = Common::getTechniciansInfo($technicianIds,"id,name,mobile,icon");


            //查出技师最后位置
            $positionArr = Position::getTechnicianPositionByIds($technicianIds);
            //查出地址的坐标
            $addressArr = AccountAddress::findOneByAttributes(['id'=>$addressId],'lon,lat');
            //查出技师接单范围
            $jsModeArr = TechnicianMode::findAllByAttributes(['technician_id'=>$technicianIds],'technician_id,lon,lat,service_radius,service_work,service_time','technician_id');
            //查出技师技能
            if($skill > 0 )
            {
                $saleOrderArr = SaleOrder::findOneByAttributes(['id'=>$saleOrderId],'first_class_id');
                if($saleOrderArr)
                {
                    $where = [
                        'technician_id'=>$technicianIds,
                        'class_id' => $saleOrderArr['first_class_id'],
                        'service_id' => $workType
                    ];
                    $jsSkillArr  = TechnicianSkill::findAllByAttributes($where,'technician_id','technician_id');
                    unset($where);
                }
            }


            foreach ($query as $val)
            {
                $lastPosition = $distance = '';
                $jsLon = $jsLat = 0;
                if(isset($positionArr[$val['technician_id']]))
                {
                    $tmpPosArr    = $positionArr[$val['technician_id']];
                    $lastPosition = Helper::getDiffDateDesc($tmpPosArr['updated_at']);

                    $jsLat = $tmpPosArr['lat'];
                    $jsLon = $tmpPosArr['lon'];


                    if($tmpPosArr['lon']>0 && $tmpPosArr['lat']>0 && $addressArr){
                        $distance = Helper::getDistance($tmpPosArr['lat'],$tmpPosArr['lon'],$addressArr['lat'],$addressArr['lon']);
                    }
                }

                //按接单范围
                if( $range > 0 )
                {
                    if( $addressArr && isset($jsModeArr[$val['technician_id']]))
                    {
                        $tmpJsMode = $jsModeArr[$val['technician_id']];
                        $jsDistance = Helper::getDistance($tmpJsMode['lat'], $tmpJsMode['lon'], $addressArr['lat'], $addressArr['lon']);
                        if ($jsDistance > $tmpJsMode['service_radius']) {
                            continue;
                        }
                    }
                    else {
                        //如果没有地址信息那就没有数据
                        continue;
                    }
                }

                //按符合技能
                if( $skill > 0 )
                {
                    if(!isset($jsSkillArr[$val['technician_id']])){
                        continue;
                    }
                }

                //是否超出服务范围 0 没有 1超出
                $outServiceRadius = 0;
                //技师服服务时间是否超过
                $outJsWorkTime = 0;

                if(isset($jsModeArr[$val['technician_id']]))
                {
                    $tmpJsModeArr = $jsModeArr[$val['technician_id']];
                    if($distance > $tmpJsModeArr['service_radius']){
                        $outServiceRadius = 1;
                    }

                    $week = date('w',strtotime($plantime));
                    $hour = date('H',strtotime($plantime));
                    $serviceTime = json_decode($tmpJsModeArr['service_time'],true);


                    if($tmpJsModeArr['service_work'] && !in_array($week,json_decode($tmpJsModeArr['service_work'],true)) || count($serviceTime)!=2 || $hour < $serviceTime[0] || $hour>$serviceTime[1] ){
                        $outJsWorkTime = 1;
                    }
                }

                $avatar = 'http://fws.uservices.cn/images/morentouxiang.png';
                if(isset($technicianArr[$val['technician_id']]) && $technicianArr[$val['technician_id']]['icon']){
                    $avatar = $technicianArr[$val['technician_id']]['icon'];
                }

                $result[] = [
                    'technician_id'     => $val['technician_id'],
                    'technician_name'   => isset($technicianArr[$val['technician_id']])?$technicianArr[$val['technician_id']]['name']:'',
                    'technician_mobile' => isset($technicianArr[$val['technician_id']])?$technicianArr[$val['technician_id']]['mobile']:'',
                    'technician_avatar' => $avatar,
                    'last_position'     => $lastPosition,
                    'distance'          => $distance,
                    'lon'               => $jsLon,
                    'lat'               => $jsLat,
                    'js_mode_lon'       => isset($jsModeArr[$val['technician_id']]) && $jsModeArr[$val['technician_id']]['lon']?$jsModeArr[$val['technician_id']]['lon']:0,
                    'js_mode_lat'       => isset($jsModeArr[$val['technician_id']]) && $jsModeArr[$val['technician_id']]['lat']?$jsModeArr[$val['technician_id']]['lat']:0,
                    'js_mode_radius'    => isset($jsModeArr[$val['technician_id']]) && $jsModeArr[$val['technician_id']]['service_radius']?$jsModeArr[$val['technician_id']]['service_radius']:0,
                    'outServiceRadius'  => $outServiceRadius,
                    'outJsWorkTime'     => $outJsWorkTime
                ];
            }


            //算出技师工单信息
            if($result)
            {
                
                //为了提高效率过滤后再查技师工单信息
                $departmentId    = BaseLogic::getDepartmentId();
                $departmentTopId = BaseLogic::getTopId();
                $directCompanyId = BaseLogic::getDirectCompanyId();
                
                //匹配技师排班数据
                //获取技师排班时间
                $startTime = strtotime(date('Y-m-01',strtotime($plantime)));
                //获取当前月份天数
                $days = date("t",$startTime);
                //获取技师id
                $techId = array_column($result,'technician_id');
                //获取技师排班数据
                $sche = ScheduleSetOptionDetail::getSetOptionDetail($techId,$departmentId,$startTime,$days);
                //获取班次
                $set = ScheduleSet::getList(['direct_company_id'=>$directCompanyId]);
                $scheduleSet = [];
                foreach ($set as $val) {
                    $scheduleSet[$val['id']] = $val;
                }
                
                //筛选技师排班
                foreach ($result as $key=>$val)
                {
    
                    if (isset($sche[$val['technician_id']][date('Y-m-d',strtotime($plantime))]['finsh_order'])) {
                        $result[$key]['noFinshWork'] = $sche[$val['technician_id']][date('Y-m-d',strtotime($plantime))]['finsh_order'];
                    }else{
                        $result[$key]['noFinshWork'] = '0';
                    }
    
                    if (isset($sche[$val['technician_id']][date('Y-m-d',strtotime($plantime))]['all_order'])) {
                        $result[$key]['totalWork']   = $sche[$val['technician_id']][date('Y-m-d',strtotime($plantime))]['all_order'];
                    }else{
                        $result[$key]['totalWork'] = '0';
                    }

                    if (isset($sche[$val['technician_id']][date('Y-m-d',strtotime($plantime))]['schedule_set_id']['name'])) {
                        $result[$key]['scheduleSet'] = $scheduleSet[$sche[$val['technician_id']][date('Y-m-d',strtotime($plantime))]['schedule_set_id']]['name'];
                    }else{
                        $result[$key]['scheduleSet'] = '全天班';
                    }

                }
                
            }


        }
        return $result;
    }

    /**
     * 根据当前用户的数据权限获取未分组的技师
     * @param array $where
     * @return array|ActiveRecord[]
     */
    public static function getDefaultTechnician($where,$page,$pageSize,$select = "a.id,a.name,a.mobile")
    {
        //设置条件
        $andwhere = "a.status=1 and a.audit_status = 5 and b.id is null";
        $db = self::find();
        $db->from(Technician::tableName() . ' as a ');
        $db->leftJoin(['`'.self::tableName().'` as b'],'a.id = b.technician_id and b.status=1');
        $db->where($where);
        $db->andWhere($andwhere);
        $db->select($select);

        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if ($pageSize <= 0) {
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum / $pageSize);

            if ($page < 1) {
                $page = 1;
            }

            $db->offset(($page - 1) * $pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $query = $db->all();

            $departmentIds = array_unique(array_column($query,'store_id'));
            $departmentRes = Department::findAllByAttributes(['id'=>$departmentIds],'id,direct_company_id');
            $departmentArr = [];
            if($departmentRes){
                $departmentArr = array_column($departmentRes,'direct_company_id','id');
            }

            foreach ($query as $key=>$val)
            {
                $query[$key]['department_top_id'] = 0;
                if(isset($val['store_id'])){
                    $storeId = $val['store_id'];
                    if(isset($departmentArr[$storeId])){
                        $query[$key]['department_top_id'] = $departmentArr[$val['store_id']];
                    }
                }

            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $query
            ];
        }

        return [
            'page'       => 0,
            'totalCount' => 0,
            'totalPage'  => 0,
            'list'       => []
        ];
    }

    /**
     * 根据服务商id 查出未分组的技师
     * @param $fwsId
     * @return array|ActiveRecord[]
     */
    public static function getTechnicianList($groupingId,$page,$pageSize)
    {
        $where = "a.status=1 and a.audit_status = 5 and b.status = 1 and b.grouping_id = $groupingId";
        $db = self::find();
        $db->from(Technician::tableName() . ' as a ');
        $db->innerJoin(['`'.self::tableName().'` as b'],'a.id = b.technician_id');
        $db->where($where);
        $db->select('a.id,a.name,a.mobile,b.job_title,a.store_id');
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if ($pageSize <= 0) {
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum / $pageSize);

            if ($page < 1) {
                $page = 1;
            }

            $db->groupBy('`b`.`job_title` desc, `a`.`name` asc');
            $db->offset(($page - 1) * $pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $query = $db->all();

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $query
            ];
        }

        return [
            'page'       => 0,
            'totalCount' => 0,
            'totalPage'  => 0,
            'list'       => []
        ];
    }

    /**
     * 把技师加入分组里
     * @param $srcId
     * @param $technicianId
     * @param $groupId
     * @return bool
     */
    public static function joinGrouping($groupObj,$technicianId,$groupId)
    {
//        print_r($groupObj);die;
        $where = [
            'department_id' => $groupObj->department_id,
            'grouping_id'   => $groupId,
            'technician_id' => $technicianId,
            'status' => 1
        ];
        $count = GroupingRelTechnician::find()->where($where)->count();
        if($count == 0)
        {
            $model = new GroupingRelTechnician();
            $model->department_id     = $groupObj->department_id;
            $model->department_top_id = $groupObj->department_top_id;
            $model->grouping_id       = $groupId;
            $model->technician_id     = $technicianId;
            $model->job_title         = 1;
            $model->create_time       = time();
            $model->update_time       = time();
            $model->status            = 1;
            if($model->save()){
                return true;
            }
        }
        return false;
    }

    /**
     * 移除
     * @param $srcId
     * @param $technicianId
     * @param $groupId
     * @return bool
     */
    public static function remove($technicianId,$groupId)
    {
        $where = [
            'grouping_id'   => $groupId,
            'technician_id' => $technicianId,
            'status'        => 1
        ];
        $model = GroupingRelTechnician::find()->where($where)->one();
        if($model)
        {
            $model->status = 0;
            $model->update_time = time();
            if($model->save()){
                if($model->job_title == 2) {
                    if(Grouping::removeLeader($groupId)) {
                        return true;
                    }
                }
                return true;
            }
        }
        return false;
    }

    /**
     * 修改分组
     * @param $srcId
     * @param $technicianId
     * @param $groupId
     * @return bool
     */
    public static function change($department_id,$technicianId,$groupId)
    {
        $where = [
            'department_id' => $department_id,
            'technician_id' => $technicianId,
            'status'        => 1
        ];
        $model = GroupingRelTechnician::find()->where($where)->one();
        if($model)
        {
            $gModel = Grouping::findOne(['id'=>$model->grouping_id,'group_leader'=>$technicianId]);
            if($gModel){
                $gModel->group_leader = 0;
                $gModel->save();
            }

            $model->status = 0;
            $model->update_time = time();
            if($model->save()){
                $gorepObj = Grouping::findOne(['id' => $groupId]);
                return self::joinGrouping($gorepObj,$technicianId,$groupId);
            }
        }
        return false;
    }

    /**
     * 修改职位
     * @param $srcId
     * @param $technicianId
     * @param $groupId
     * @param $jobTitle
     * @return bool
     */
    public static function changeTitle($department_id,$technicianId,$groupId,$jobTitle)
    {
        $where = [
            'department_id' => $department_id,
            'grouping_id'   => $groupId,
            'technician_id' => $technicianId,
            'status'        => 1
        ];
        $model = GroupingRelTechnician::find()->where($where)->one();
        if($model)
        {
            if($model->job_title == $jobTitle) {
                return true;
            }
            //修改成组长,把以前的组长改成组员
            if($jobTitle == 2){
                $where = [
                    'department_id' => $department_id,
                    'grouping_id'   => $groupId,
                    'status'        => 1,
                    'job_title'     => 2
                ];
                GroupingRelTechnician::updateAll(['job_title'=>1,'update_time'=>time()],$where);
            }

            $model->job_title   = $jobTitle;
            $model->update_time = time();
            if($model->save()){
                $groupObj = Grouping::findOne(['id' => $groupId,'status' => 1]);
                if($groupObj) {
                    $groupObj->group_leader = '';
                    if($jobTitle == 2) {
                        $groupObj->group_leader = $technicianId;
                    }
                    if($groupObj->save()) {
                        return true;
                    }
                    return false;
                }
                return true;
            }
        }

        return false;
    }

    /**
     * 根据分组ID查分组下技师
     * @param $group_id
     * @param $select
     * @return array
     * @author xi
     * @date 2018-4-26
     */
    public static function getGroupingTec($group_id,$select = 'technician_id')
    {
        $where = ['grouping_id' => $group_id,'status' => 1];
        $query = self::findAllByAttributes($where,$select);
        if($query){
            return $query;
        }
        return [];
    }

    /**
     * 删除分组下所有技师
     * @param  $group_id
     */
    public static function upGroupTec($group_id)
    {
        $tecIds = self::getGroupingTec($group_id);
        $model = new self();
        if($model->updateAll(['status' => 0],['grouping_id' => $group_id, 'technician_id' => $tecIds])) {
            return true;
        }
        return false;
    }

    /**
     * 验证
     */
    public static function checkLeader($department_id,$technicianId,$group_id)
    {
        $where = [
            'department_id' => $department_id,
            'technician_id' => $technicianId,
            'status'        => 1
        ];
        $model = GroupingRelTechnician::find()->where($where)->andWhere(['<>','grouping_id',$group_id])->one();
        if($model) {
            //查询分组名称
            $group = Grouping::findOne(['id' => $model->grouping_id]);
            return $group;
        }
        return false;
    }

    /**
     * 检查技师是否在同一组里
     * @param $techniianIds
     * @return bool
     */
    public static function hasSameGrouping($techniianIds)
    {
        if($techniianIds)
        {
            if(!is_array($techniianIds)){
                $techniianIds = [$techniianIds];
            }
            $where = [
                'technician_id' => $techniianIds,
                'status'        => 1,
                'department_id' => BaseLogic::getDepartmentId()
            ];
            $query = self::findAllByAttributes($where,'grouping_id,technician_id');
            if($query)
            {
                if(count(array_column($query,'technician_id','grouping_id'))>1){
                    return false;
                }
                else if(array_diff($techniianIds,array_column($query,'technician_id'))){
                    return false;
                }
                else {
                    return true;
                }
            }
            else
            {
                $where = [
                    'id'          => $techniianIds,
                    'status'      => 1,
                    'audit_status'=> 5,
                    'store_id'    => BaseLogic::getDepartmentId()
                ];
                $res = Technician::findAllByAttributes($where,'id');
                if($res){
                    if(array_diff($techniianIds, array_column($res,'id'))){
                        return false;
                    }
                    else {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 获取分组的技师
     * @param $jobTitle
     * @param $groupingId
     * @return array|ActiveRecord[]
     */
    public static function getGroupingTechnician($jobTitle,$groupingId)
    {
        $where =[
            'a.department_id'     => BaseLogic::getDepartmentId(),
            'b.store_id'          => BaseLogic::getDepartmentId(),
            'a.status'            => 1,
            'a.job_title'         => $jobTitle,
            'a.grouping_id'       => $groupingId,
            'b.status'            => 1,
            'b.audit_status'      => 5
        ];
        $query = self::find()
            ->from( self::tableName() . ' as a')
            ->innerJoin(['`'. Technician::tableName() .'` as b'], ' a.technician_id = b.id')
            ->where($where)
            ->select('a.technician_id,a.job_title')
            ->limit(1000)
            ->asArray()
            ->all();
      /*  echo  self::find()
            ->from( self::tableName() . ' as a')
            ->innerJoin(['`'. Technician::tableName() .'` as b'], ' a.technician_id = b.id')
            ->where($where)
            ->select('a.technician_id,a.job_title')
            ->limit(1000)->createCommand()->getRawSql();exit;*/
        return $query;
    }

    /**
     * 查出禁用未通过的技师姓名
     * @param $techniianIds
     * @return bool|string
     */
    public static function getDisableTechnicaianName($techniianIds)
    {
        if(!is_array($techniianIds)){
            $techniianIds = [$techniianIds];
        }
        $where = "id in(".implode(',',$techniianIds).") and (status <> 1 or audit_status<>5)";
        $query = Technician::find()
            ->where($where)
            ->select('name')
            ->asArray()
            ->all();
        if($query)
        {
            return implode('、', array_column($query,'name','name'));
        }
        return '';
    }

    /**
     * 检查是否是组长
     * @param $techniianId
     * @return bool
     */
    public static function hasGroupingLeader($techniianId)
    {
        $query = self::findOneByAttributes(['technician_id'=>$techniianId,'status'=>1]);
        if($query){
            if($query['job_title']!=2){
                return true;
            }
        }
        return false;
    }

    /**
     * 检查指派多技师，查一下是否有技师突然改成组长了
     * @param $techniianIds
     * @return string
     */
    public static function hasTechnicaianChangeJobTitle($techniianIds)
    {
        if(!is_array($techniianIds)){
            $techniianIds = [$techniianIds];
        }

        $where = [
            'a.technician_id' => $techniianIds,
            'a.status'        => 1,
            'a.job_title'     => 2,
            'b.status'        => 1,
            'b.audit_status'  => 5
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'.Technician::tableName().'` as b'] , 'a.technician_id = b.id')
            ->where($where)
            ->select('b.name')
            ->asArray()
            ->all();
        if($query)
        {
            return implode('、', array_column($query,'name','name'));
        }
        return '';
    }

    public static function getChangeOrRemoveGrouping($techniianIds,$groupId)
    {
        if(!is_array($techniianIds)){
            $techniianIds = [$techniianIds];
        }

        $where = [
            'a.technician_id' => $techniianIds,
            'a.status'        => [0,1],
            'b.status'        => 1,
            'b.audit_status'  => 5
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'.Technician::tableName().'` as b'] , 'a.technician_id = b.id')
            ->where($where)
            ->select('b.name, a.grouping_id')
            ->asArray()
            ->all();
        if($query)
        {
            $arr = array_column($query,'name','grouping_id');
            if(!isset($arr[$groupId])){
                return implode('、', array_column($query,'name','name'));
            }
        }
        return '';
    }

}
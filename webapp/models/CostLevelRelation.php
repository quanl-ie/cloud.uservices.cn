<?php

namespace webapp\models;

use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;


class CostLevelRelation extends BaseModel
{
    public static function getDb() {
        return Yii::$app->order_db;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cost_level_relation';
    }

    /**
     * 批量添加
     * @param $departmentId 部门id
     * @param $brandArr 品牌数组
     * @return bool
     * @author xi
     * @date 2018-6-6
     */
    public static function batchInsert($departmentId,$levelArr)
    {
        $where = [
            'department_id' => $departmentId,
            'del_status'    => 1
        ];
        $oldLevelIds = [];
        $oldlevelRes = self::findAllByAttributes($where,'cost_level_id');
        if($oldlevelRes){
            $oldLevelIds = array_column($oldlevelRes,'cost_level_id');
        }

        //算出新加的
        $newLevelIds = array_diff($levelArr,$oldLevelIds);
        //算出要删除的
        $delLevelIds = array_diff($oldLevelIds,$levelArr);

        if($newLevelIds)
        {
            foreach ($newLevelIds as $val)
            {
                $model = new self();
                $model->department_id     = $departmentId;
                $model->department_top_id = Department::getDirectCompanyIdById($departmentId);
                $model->cost_level_id     = $val;
                $model->create_time       = time();
                $model->update_time       = time();
                if(!$model->save(false)){
                    return false;
                }
            }
        }
        if($delLevelIds)
        {
            $res = self::updateAll(['del_status'=>2,'update_time'=>time()],['department_id'=>$departmentId,'cost_level_id'=>$delLevelIds]);
            if(!$res){
                return false;
            }
        }

        return true;
    }

    /**
     * 查出设置部门的收费项目
     * @param $selfDepartmentId
     * @param $departmentId
     * @return array
     * @author xi
     * @date 2018-6-6
     */
    public static function getSelfLevel($selfDepartmentId,$departmentId)
    {
        $where = [
            'a.department_id' => $selfDepartmentId,
            'a.status'        => 1,
            'a.del_status'    => 1,
            'b.del_status'    => 1
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin([ '`'.CostLevel::tableName().'` as b'], 'a.cost_level_id = b.id')
            ->where($where)
            ->select('a.cost_level_id,b.level_title,b.status')
            ->asArray()
            ->all();

        if($query)
        {
            //查出已设置部门的id
            $levelIds = [];
            $levelRes = self::findAllByAttributes(['department_id'=>$departmentId,'status'=>1,'del_status'=>1],'cost_level_id');
            if($levelRes){
                $levelIds = array_column($levelRes,'cost_level_id');
            }

            $result = [];
            foreach ($query as $val)
            {
                $result[] = [
                    'id'     => $val['cost_level_id'],
                    'name'   => $val['level_title'],
                    'status' => $val['status'],
                    'checked'=> in_array($val['cost_level_id'], $levelIds)?1:0
                ];
            }
            return $result;
        }

        return [];
    }

    /**
     * 填加
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public static function add($id)
    {
        $old_model = CostLevelRelation::findOne(['cost_level_id'=>$id,'del_status'=>1]);
        if($old_model){
            $old_model->update_time       = time();
            if($old_model->save()){
                return true;
            }
        }else{
            $model = new self();
            $model->department_id     = $model->departmentId;
            $model->department_top_id = $model->directCompanyId;
            $model->cost_level_id     = $id;
            $model->status            = 1;
            $model->del_status        = 1;
            $model->create_time       = time();
            $model->update_time       = time();
            if($model->save()){
                return true;
            }
        }
        throw new \Exception('保存失败',30002);
    }
    /**
     * 获取多条数据
     * @author sxz
     * @inheritdoc
     */
    public static function getList($where = [])
    {
        return self::find()->where(['status'=>1,'del_status'=>1])->andWhere($where)->asArray()->all();
    }
}

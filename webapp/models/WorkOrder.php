<?php
namespace webapp\models;

use webapp\logic\BaseLogic;
use Yii;
use yii\base\Model;
use webapp\models\WorkCostForm;
use webapp\models\SaleOrder;
class WorkOrder extends BaseModel
{
    public $scope_img;
    public $work_img;
    public $scene_img;

    public static function getDb() {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_order';
    }

    public function attributeLabels()
    {
        return [
            'scope_img'       => '质保凭证：',
            'work_img'       => '服务工单：',
            'scene_img'       => '现场拍照：',
        ];
    }



    public static function getList($param = [])
    {
        $db = self::find();
        $pageSize = $param['pageSize'];
        if(!isset($param['currentPage'])){
            $page = 0;
        }else{
            $page = $param['currentPage']-1;
        }

        unset($param['currentPage']);
        unset($param['pageSize']);

        $db->from(self::tableName() . ' as a');
        $db->select('a.order_no,e.create_time as finish_time,a.work_type as type_id,c.sale_order_id');
        //$db->innerJoin([WorkForm::tableName() . ' as b'],'a.order_no = b.order_no');
        $db->innerJoin(['work_order_detail as c'],'a.order_no=c.order_no');
        $db->innerJoin(['work_cost as d'],'a.order_no=d.order_no');
        $db->innerJoin(['work_order_process as e'],'a.order_no=e.order_no');
        $db->innerJoin(['work_order_fws_assign as f'],'a.order_no=f.order_no');
        $param['d.status'] = 1;
        $param['e.type'] = 24;
        $param['f.type'] = 1;
        // $db->where(['a.src_type',BaseModel::SRC_SJ]);
        $db->groupBy(['order_no']);
        if(!empty($param['start_time'])){
            $db->andWhere(['>=','a.update_time',$param['start_time']]);
            unset($param['start_time']);
        }
        if(!empty($param['end_time'])){
            $db->andWhere(['<=','a.update_time',$param['end_time']]);
            unset($param['end_time']);
        }

        foreach($param as $k=>$v){
            if(!empty($v)){
                $db->andwhere([$k=>$v]);
            }
        }

        $count = $db->count();
        $totalPage = ceil($count/$pageSize);
        $start = $page*$pageSize;
        $limit = $pageSize;
        $list =$db->offset($start)
            ->limit($pageSize)
            ->asArray()
            ->all();
        //echo $db->createCommand()->getRawSql();exit;
        if(empty($list))
            return false;
        $typeList = ServiceTypeQualification::getServiceType(BaseLogic::getManufactorId(),BaseModel::SRC_FWS);

        foreach($list as &$v){
            if(isset($typeList[$v['type_id']])){
                $v['type_name'] = $typeList[$v['type_id']];
            }
            $cost = WorkCostForm::find()->where(['order_no'=>$v['order_no'], 'status' => 1])->sum('cost_real_amount');
            $service = SaleOrder::findOne(['id'=>$v['sale_order_id']]);
            $v['brand_name'] = $service['brand_name'];
            $v['class_name'] = $service['class_name'];
            $v['amount'] = $cost;
        }
        return [
            'page'       => $page+1,
            'totalCount' => $count,
            'totalPage'  => $totalPage,
            'list'       => $list
        ];
    }

}

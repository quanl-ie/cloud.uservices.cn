<?php
namespace webapp\models;

use Yii;

class GoodsProperty extends BaseModel
{

    public static function tableName()
    {
        return 'goods_property';
    }

    public static function getListByGoodsId($goodsId)
    {
        $query = self::findAllByAttributes(['goods_id'=>$goodsId,'status'=>1],'attr_key,attr_value');
        return $query;
    }
    
}
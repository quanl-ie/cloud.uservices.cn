<?php
namespace webapp\models;

use common\helpers\Helper;
use common\models\Common;
use webapp\logic\BaseLogic;
use Yii;
use yii\db\ActiveQuery;

class  Department extends BaseModel
{
    public $submitType = 1;
    public $province_name;
    public $city_name;
    public $district_name;
    
    public static function tableName()
    {
        return 'department';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufactor_id','name','type'], 'required'],
            [['manufactor_id', 'parent_id', 'weight', 'province_id', 'city_id', 'district_id', 'create_time', 'update_time','direct_company_id'], 'integer'],
            [['name', 'contacts', 'address', 'phone', 'email','submitType'], 'string', 'max' => 255],
            [['parent_ids', 'logo','remark'], 'string', 'max' => 1000],
            [['type', 'status', 'del_status'], 'number'],
            ['name','validateName'],
            ['weight','validateWeight'],
            ['email','email','message'=>"邮箱格式不正确"]
        ];
    }

    /**
     * 获取机构类型
     * @param $index
     * @return mixed|string
     */
    public static function getTypeDesc($index)
    {
        $data = [
            1 => '经销商',
//            2 => '门店',
            3 => '部门'
        ];
        return isset($data[$index])?$data[$index]:'';
    }

    /**
     * 根据上级机构类型返回不同的类型
     * @param $type
     * @return array
     * @author xi
     */
    public static function getTypeByParentType($type,$departmentId)
    {
        $data = [
            1 => '经销商',
            3 => '部门'
        ];
        if($type == 3){
            unset($data[1]);
        }
        if($type ==1 && $departmentId != (new self())->departmentId){
            unset($data[3]);
        }
        return $data;
    }

    /**
     * 验证名称
     * @param $attributes
     * @param $params
     */
    public function validateName($attributes,$params)
    {
        $where = [
            'name'      => $this->name,
            'parent_id' => $this->parent_id,
            'del_status'=> 1
        ];

        $andWhere = '1=1';
        if($this->isNewRecord == false){
            $andWhere = ' id <> '.$this->id;
        }

        $count = self::find()->where($where)->andWhere($andWhere)->count();
        if($count>0){
            $this->addError($attributes,'在此级别下已经存在该机构名称');
        }else if(Helper::strCount($this->name) > 30){
            $this->addError($attributes,'不超过30个字');
        }
    }

    /**
     * 机构编号验证
     * @param $attributes
     * @param $params
     */
    public function validateWeight($attributes,$params)
    {
        if($this->$attributes ){
            if($this->$attributes>100000){
                $this->addError($attributes,'仅限数字，至多5位');
            }
        }
    }


    public function attributeLabels()
    {
        return [
            'name'       => '机构名称',
            'parent_id'  => ' 父',
            'parent_ids' => '上级机构',
            'type'       => '机构类型',
            'weight'     => '机构编号'
        ];
    }

    /**
     *通过id 获取顶级部门ID
     * @param $department_id
     * @return string | int
     * @author liquan
     */
    public static function getParentIdsById($department_id)
    {
        $query = self::findOneByAttributes(['id'=>$department_id],'parent_ids');
        if($query){
            $top_id_arr = explode(",",$query['parent_ids']);
            if($top_id_arr[0] == 0){  //如果本身是顶级 公司
                return $department_id;
            }else{
                foreach (array_reverse($top_id_arr) as $v){
                    $top = self::findOneByAttributes('id ='.$v.' and type = 1','id');
                    if($top){  // 直属上级 公司
                        return $v;
                    }
                }
            }
        }
        return 0;
    }
      /**
       * 获取下属机构信息
       * @author liquan
       * @param $department_ids
       * @return array
       * @date 2018-6-6
       */
      public static function getNextDepartment($department_ids)
      {
            $db = self::find();
            $db->from(self::tableName());
            $db->where(['parent_id'=>$department_ids]);
            $db->select('*');
            $db->asArray();
            $list = $db->all();
            return $list;
      }
    /**
     * 获取下属机构信息
     * @author liquan
     * @param $department_id
     *          $search 0 所有下属  1 经销商（不含部门） 2 查询下属部门 3.查询所有下级 包括下级经销商的部门
     *          $level 获取级别 0 获取所有下级 1 获取公司下属
     * @return array
     * @date 2018-6-6
     */
    public static function getDepartmentChildren($department_id,$search = 0,$level = 0)
    {
            //获取自身
            $query = self::findAllByAttributes(['id'=>$department_id]);
            //获取下属机构
            if($level == 1){ //本公司
                if(is_array($department_id)) {
                    $department_id = implode(',',$department_id);
                    $where = "parent_id in(".$department_id.') and type = 3 and del_status = 1';
                } else {
                    $where = "direct_company_id =".$department_id." and type = 3 and del_status = 1";
                }
                //获取下级部门

                $low_query = self::findAllByAttributes($where);
                 return array_merge($query,$low_query);
                /*if(!empty($low_query)){
                      $low_where = [];
                      foreach($low_query as $val){
                            $low_where [] = "FIND_IN_SET(".$val['id'].",parent_ids)";
                      }
                      $all_where = implode(" or ",$low_where);
                      if($search == 1){
                            $all_where = "type = 1 and (".$all_where.")";
                      }
                      $all_query = self::findAllByAttributes($all_where);
                      return array_merge($query,$low_query,$all_query);
                }else{
                      return $query;
                }*/

            }else{
                  if($search == 1){
                        $where = "parent_id =".$department_id." and type = 1 and del_status = 1";
                  }else if($search == 2){
                        $where = "FIND_IN_SET(".$department_id.",parent_ids) and type = 3 and del_status = 1";
                  }else if($search == 3){
                        $where = "FIND_IN_SET(".$department_id.",parent_ids) and del_status = 1";
                  }else{
                        $where = "((direct_company_id =".$department_id." and type = 3) or (FIND_IN_SET(".$department_id.",parent_ids)) and type = 1) and del_status = 1";
                  }
                  $childQuery = self::findAllByAttributes($where);
                  if(!empty($childQuery)){
                        return array_merge($query,$childQuery);
                  }
                  return $query;
            }
    }

    /**
     * 获取和用户相关的 机构信息
     * @author liquan
     * @param $department_id  部门id
     * $self   0 获取下级     1 获取自身
     * $level  0 获取所有下级 1 获取本机构下属
     * $flag 获取形式 0 一维 array('id'=>name)    1 二维 array(0=>array(id=>0,name=>'XXX','info'=>"XXX"...))
     * $is_user 0 无用户 1 有用户 2 无限制
     * $type  获取下属的机构类型
     * $sub  0 所有下级 1 直接下级
     * @return array
     * @date 2018-6-6
     */
    public static function getDepartmentInfo($department_id,$self = 1,$level = 0,$flag = 0,$is_user = 0,$type=[1,2,3],$sub=0)
    {
        if($self){
            //获取自身
            $query = self::findAllByAttributes(['id'=>$department_id]);
            if($flag){
                return $query;
            }
            if (!empty($query))
            {
                foreach ($query as $val)
                {
                    $result[$val['id']] = $val['name'];
                }
                return $result;
            }
            return array();
        }
       //获取下属机构
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        if($sub == 1){
              //直属部门
              $db->where(['a.type'=>$type,'a.parent_id'=>$department_id]);
        }else{
              if($level){   // @todo 根据数据权限 本公司不包含经销商
                    //所有下级门店、部门
                    $depart = self::getDepartmentChildren($department_id,0,1);
                    $db->where(['a.parent_id'=>array_column($depart,"id")]);
              }else{
                    $db->where(['a.parent_id'=>$department_id]);
                    //$db->where("FIND_IN_SET(".$department_id.",a.parent_ids)");
              }
        }

        if($is_user == 0){
              //查询门店部门 及 无用户经销商
            $db->andWhere("a.type = 3 or (a.type = 1 and b.id is null)");
            $db->leftJoin([AdminUser::tableName() . ' as b'],' a.id = b.department_id');
        }
        if($is_user == 1){
            $db->leftJoin([AdminUser::tableName() . ' as b'],' a.id = b.department_id');
        }
        $db->andWhere('a.del_status = 1');
        $db->select('a.*');
         // echo $db->createCommand()->rawSql;
        $db->asArray();
        $list = $db->all();
        if($flag){
            return $list;
        }
        if (!empty($list))
        {
            foreach ($list as $val)
            {
                $result[$val['id']] = $val['name'];
            }
            return $result;
        }
        return array();
    }
    /**
     * 获取 parent_ids
     * @param $parentId
     * @return array
     * @author xi
     * @date 2018-06-05
     */
    public static function getDepartmentParentIds($parentId)
    {
        $query = self::findOneByAttributes(['id'=>$parentId],'parent_ids');
        if($query){
            return implode(',' , array_unique(array_merge( array_filter(explode(',',$query['parent_ids'])),[$parentId])));
        }
        return [$parentId];
    }


    /**
     * 获取 son_ids
     * @param $departmentId,$type
     * @return array
     * @author chengjuanjuan
     * @date 2018-06-06
     */
    public static function getDepartmentSonIds($departmentId,$type= array(1,2,3),$directCompanyId='')
    {
        if(is_array($departmentId)){
            $departmentId = implode($departmentId,',');
        }
        if($directCompanyId){
            $where = " and direct_company_id=$directCompanyId ";
        }else{
            $where = '';
        }
        if(empty($type)){
            $query = self::find()->Where("find_in_set('".$departmentId."',parent_ids) $where")->asArray()->all();
        }else{
            $query = self::find()->where(['type'=>$type])->andWhere("find_in_set('".$departmentId."',parent_ids) $where")->asArray()->all();
        }
          if($query){
            $ids = array_column($query,'id');

            return $ids;
        }
        return [];
    }

    //添加
    public static function add($data)
    {
        //实例化表模型
        $model = new self();
        //处理数据
        foreach ($data as $key => $val) {
            $model->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //添加s数据
            $model->save(false);
            $transaction->commit();
            return [
                'id' => $model->id
            ];
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    //获取单条信息
    public static function getOne($where)
    {
        return self::find()->where($where)->one();
    }
    /**
     * 获取 //获取单个用户注册的所有公司或部门
     * @param $department_ids
     * @return array
     * @author sxz
     * @date 2018-06-06
     */
    public static function getAllList($department_ids,$source=false)
    {
        $data =  self::find()
            ->select(['id','name','direct_company_id'])
            ->where(['del_status'=>1,'status'=>1])
            ->andWhere(['in','id',$department_ids])
            ->asArray()->all();
        if($data){
            foreach ($data as $key=>$val){
                if($val['id'] != $val['direct_company_id']){
                    $data[$key]['name'] = self::getDepartmentName(['id'=>$val['direct_company_id']]);
                }
            }
        }
        if($source){
            return $data;
        }
        $str = '<option value="">请选择公司</option>';
        foreach ($data as $val) {
            $str = $str . '<option value="'.$val['id'].'">'.$val['name'].'</option>';
        }
        return $str;
    }

    /**
     * 订单列表
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function getList($where,$andWhere = [],$status, $dataAuthId=1,$page = 1,$pageSize = 1000)
    {
        $db = self::find();
        if($where){
            $db->where($where);
        }
        if($andWhere){
            $db->andWhere($andWhere);
        }

        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }

            $db->select('*');
            $db->orderBy('weight');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();

            $parentIds = array_unique(array_column($list,'parent_id'));
            $departRes = self::findAllByAttributes(['id'=>$parentIds],'id,name');
            $departArr = [];
            if($departRes){
                $departArr = array_column($departRes,'name','id');
            }

            //区县城市
            $areaIds = array_column($list,'province_id');
            $areaIds = array_merge($areaIds, array_column($list,'city_id'));
            $areaIds = array_merge($areaIds, array_column($list,'district_id'));
            $areaIds = array_unique($areaIds);
            $regionRes = Region::findAllByAttributes(['region_id'=>$areaIds],'region_id,region_name');
            $regionArr = [];
            if($regionRes){
                $regionArr = array_column($regionRes,'region_name','region_id');
            }

            $ids = array_column($list,'id');
            $subData = [];
            $subRes = self::find()
                ->where(['parent_id'=>$ids,'status'=>$status,'del_status'=>1]  )
                ->select('parent_id,id,type,direct_company_id')
                ->asArray()
                ->all();
            if($subRes)
            {
                foreach ($subRes as $key=>$val)
                {
                      if($dataAuthId == 2 && $val['type'] == 1){  //本公司权限 移除下属经销商
                            unset($subRes[$key]);
                      }
					  //如果部门类型，同时是本部门权限
					  else if($dataAuthId==4 && $val['type'] == 3){
						  unset($subRes[$key]);
					  }
					  else{
                            if(!($val['type'] == 3 && $val['direct_company_id'] != BaseLogic::getDirectCompanyId())){
                                 $subData[$val['parent_id']] = $val['parent_id'];
                            }
                      }

                }
            }
            foreach ($list as $key=>$val)
            {
                $list[$key]['parent_name'] = '无';
                $province = $city =  $district = '';
                if(isset($departArr[$val['parent_id']])){
                    $list[$key]['parent_name'] = $departArr[$val['parent_id']];
                }
                if(isset($regionArr[$val['province_id']])){
                    $province =$regionArr[$val['province_id']];
                }
                if(isset($regionArr[$val['city_id']])){
                    $city =$regionArr[$val['city_id']];
                }
                if(isset($regionArr[$val['district_id']])){
                    $district =$regionArr[$val['district_id']];
                }

                $list[$key]['type_desc']    = self::getTypeDesc($val['type']);
                $list[$key]['create_time']  = date('Y-m-d H:i',$val['create_time']);
                $list[$key]['address_desc'] = $province.$city.$district . $val['address'];
                $list[$key]['sub']          = isset($subData[$val['id']])?1:0;
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 获取部门
     * @param string/array $where
     * @return array
     * @author xi
     * @date 2018-6-7
     */
    public static function getDepartment($where,$dataAuthId = 3)
    {
        $query = self::find()
            ->where($where)
            ->select('id,name,type,direct_company_id')
            ->orderBy(['type'=>'asc'])
            ->limit(1000)
            ->asArray()
            ->all();
        if($query)
        {
            //查询是否有下级
            $ids = array_column($query,'id');
            $subData = [];
            $subRes = self::find()
                ->where(['parent_id'=>$ids,'status'=>1,'del_status'=>1] )
                ->select('parent_id,id,type,direct_company_id')
                ->asArray()
                ->all();
            if($subRes)
            {
                $typeArr = array_column($query,'type','id');
                foreach ($subRes as $key=>$val)
                {
                      if($dataAuthId == 2 && $val['type'] == 1){  //本公司权限 移除下属经销商
                              unset($subRes[$key]);
                      }else{
                            if(!($val['type'] == 3 && $val['direct_company_id'] != BaseLogic::getDirectCompanyId())){
                                  $subData[$val['parent_id']] = $val['parent_id'];
                            }
                      }
                }
            }

            foreach ($query as $key=>$val)
            {
                $query[$key]['sub'] = 0;
                if($dataAuthId != 4){
                    if(isset($subData[$val['id']])){
                        $query[$key]['sub'] = 1;
                    }
                }
            }
        }

        return $query;
    }

    /**
     * 根据机构ID获取机构名称
     * @author li
     * @param  int  $id
     * @date   2018-06-12
     * @return string
     */
    public static function getDepartmentName($id)
    {
        $department = self::findOne(['id' => $id]);
        return $department ? $department->name : '';
    }
    /**
     * 根据机构IDs获取机构名称
     * @author sxz
     * @param  int  $id
     * @date   2018-06-12
     * @return string
     */
    public static function getDepartmentNames($id)
    {
        $departments = self::find()->where(['id'=>$id])->asArray()->all();
        $data = [];
        if (!empty($departments)) {
            $data = array_column($departments,'name','id');
        }
        return $data;
    }

    /**
     * 根据直属公司id查询下级公司信息及当前公司的信息不包含下级公司级别机构
     * @author li
     * @param  int  $direct_company_id  直属公司id
     * @date   2018-06-13
     * @return array
     */
    public static function getDirectCompany($direct_company_id)
    {
        $companyArr = [];
        $data = self::find()->where(['direct_company_id' => $direct_company_id, 'type' => 3])->andWhere([])->asArray()->all();
        $company = self::find()->where(['id' => $direct_company_id])->asArray()->all();

        foreach (array_merge($data, $company) as $k => $v)
        {
            $companyArr['id'][]             = $v['id'];
            $companyArr['name'][]           = $v['name'];
            $companyArr['data'][$k]['id']   = $v['id'];
            $companyArr['data'][$k]['name'] = $v['name'];
        }
        return $companyArr;
    }

    /**
     * 获取直属id
     * @param $id
     * @return int
     * @author xi
     */
    public static function getDirectCompanyIdById($id)
    {
        $query = self::findOneByAttributes(['id'=>$id],'direct_company_id');
        if($query){
            return $query['direct_company_id'];
        }

        return 0;
    }

    /**
     * 获取直属id
     * @param $id
     * @param $type
     * @return int
     */
    public static function getDirectCompanyId($id,$type)
    {
        if($type == 1){
            return $id;
        }
        else
        {
            $query = self::findOneByAttributes(['id'=>$id],'parent_id');
            if($query)
            {
                $parenId = $query['parent_id'];
                $query1 = self::findOneByAttributes(['id'=>$parenId],'direct_company_id');
                if($query1){
                    return $query1['direct_company_id'];
                }
            }
            return 0;
        }
    }
    /**
     * 获取顶级公司id
     * @param $id
     * @return int
     * @author xi
     */
        public static function getTopIdById($id)
    {
        $query = self::findOneByAttributes(['id'=>$id],'parent_id');
        if($query){
            if($query['parent_id'] == 0){
                return $id;
            }else{
                return $query['parent_id'];
            }
        }
        return 0;
    }
    /**
     * //经销商修改自己的基本信息
     * sxz
     * @param $id
     * @param $type
     * @return int
     */

    public static function editInfo($id,$data)
    {
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            $where = ['id'=>$id];
            $model = self::getOne($where);
            foreach ($data as $key => $val) {
                $model->$key = $val;
            }
            if ($model->save(false)) {
                $transaction->commit();
                return 1;
            }else{
                return 2;
            }
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * 查询
     * @param $departmentId
     * @return bool
     */
    public static function hasRepeatUser($departmentId,$mobile)
    {
        $directCompanyId = self::getDirectCompanyIdById($departmentId);

        $departmentIds = [$directCompanyId];
        $departmentRes = self::findAllByAttributes(['direct_company_id'=>$directCompanyId],'id');
        if($departmentRes){
            $departmentIds = array_merge($departmentIds,array_column($departmentRes,'id'));
        }

        return User::getUserByDepartmentId($departmentIds,$mobile);
    }

    /**
     * 如果 是经销商就把他下面的部门id查出来
     * @param $departmentIds
     * @return array
     * @author xi
     * @date 2018-7-10
     */
    public static function getSubDepartmentIds($departmentIds , array $type = [1,3])
    {
        $result = [];
        //查出经销商下的有部门

        if(!$type){
            $type = [1,3];
        }

        $query = self::findAllByAttributes(['id'=>$departmentIds,'type'=>1],'id');
        if($query)
        {
            $where = [];
            $jxsIds = array_column($query,'id');
            foreach ($jxsIds as $val){
                $where [] = "FIND_IN_SET($val,parent_ids)";
            }
            unset($query);

            $where = "(" . implode(' or ', $where) .") and type in(".(implode(',',$type)).")";
            $query = self::findAllByAttributes($where,'id');
            if($query){
                $result = array_column($query,'id');
            }
        }

        return array_unique(array_merge($departmentIds,$result));
    }

}
<?php
namespace webapp\models;

use webapp\models\AdminUser;
use Yii;
use yii\base\Model;
use common\helpers\Helper;
use webapp\models\Department;

class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;
    public $verifyCode;
    private $_user;
    public $department_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password','department_id'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            ['verifyCode', 'captcha','captchaAction'=>'site/captcha','on'=>'login']
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'username'   =>'手机号',
            'password'   => '密码',
            'rememberMe' => '记住我',
            'verifyCode' => '',
            'mobile'     => '手机号',
            'department_id' =>''
        ];
    }
    
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            //根据用户department_id反推出用户状态
            $userInfos = User::findOne(['mobile'=>$this->username,'department_id'=>$this->department_id]);
            if ($userInfos && $userInfos->status == 2) {
                $this->addError('username', '该用户禁止登陆，详情请联系系统管理员');
            }else{
                if (!$user || !$user->validatePassword($this->password)) {
                    $this->addError($attribute, '用户名或密码错误');
                }
            }
            $depInfo = Department::findOne(['id'=>$this->department_id]);
            //查询当前账户有几个系统
            if ($depInfo && $depInfo->status == 2) {
                $this->addError('username', '该用户禁止登陆，详情请联系系统管理员');
            }
        }else{
            $this->addError($attribute, '用户名或密码错误');
        }

    }
    
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }
    
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = AdminUser::findByUsername($this->username,$this->department_id);
            if($this->_user){
                $this->_user->last_login_time = time();
                $this->_user->login_ip        = Helper::getIp();
                $this->_user->save(false);
            }
        }
        return $this->_user;
    }

}

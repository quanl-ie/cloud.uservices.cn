<?php

namespace webapp\models;

use common\helpers\Helper;
use common\models\ServiceClass;
use webapp\logic\BaseLogic;
use Yii;

class ManufactorClass extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacturer_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'department_id', 'department_top_id', 'class_id','class_pid', 'status', 'del_status', 'create_time', 'update_time'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'department_id'     => '（部门表）department 表id',
            'direct_company_id' => '直属公司id',
            'class_id'          => '产品类目',
            'status'            => '是否启用 1启用 2禁用',
            'del_status'        => '是否删除 1为未删  2为删除',
            'create_time'       => '创建时间',
            'update_time'       => '更新时间',
        ];
    }
    
    
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    
    public static function getPageList($where = [], $select="a.class_id,b.title,b.parent_id,b.sort,b.status,a.create_time")
    {
        $db = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin([ '`'.ServiceClass::tableName().'` as b'], 'a.class_id = b.id')
            ->andWhere(['a.del_status'=>1,'a.status'=>1])
            ->andWhere(['b.del_status'=>1])
            ->select($select);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        $db->orderBy(['sort'=>SORT_ASC,'a.create_time'=> SORT_DESC]);
        $db->asArray();
        $list = $db->all();
    
        return [
            'list' => $list
        ];
    }
  

    /**
     * 批量添加
     * @param $departmentId 部门id
     * @param $brandArr 品牌数组
     * @return bool
     * @author xi
     * @date 2018-6-6
     */
    public static function batchInsert($departmentId,$classArr)
    {
        $where = [
            'department_id' => $departmentId,
            'del_status'    => 1
        ];
        $oldClassIds = [];
        $oldClassRes = self::findAllByAttributes($where,'class_id');
        if($oldClassRes){
            $oldClassIds = array_column($oldClassRes,'class_id');
        }

        //算出新加的
        $newClassIds = array_diff($classArr,$oldClassIds);
        //算出要删除的
        $delClassIds = array_diff($oldClassIds,$classArr);

        if($newClassIds)
        {
            $classPidArr = [];
            $classPidRes = ServiceClass::findAllByAttributes(['id'=>$newClassIds],'id,parent_id');
            if($classPidRes){
                $classPidArr = array_column($classPidRes,'parent_id','id');
            }

            foreach ($newClassIds as $val)
            {
                $model = new self();
                $model->department_id     = $departmentId;
                $model->direct_company_id = Department::getDirectCompanyIdById($departmentId);
                $model->class_id          = $val;
                $model->class_pid         = isset($classPidArr[$val])?$classPidArr[$val]:0;
                $model->create_time       = time();
                $model->update_time       = time();
                if(!$model->save(false)){
                    return false;
                }
            }
        }
        if($delClassIds){
            $res = self::updateAll(['del_status'=>2,'update_time'=>time()],['department_id'=>$departmentId,'class_id'=>$delClassIds]);
            if(!$res){
                return false;
            }
        }

        return true;
    }

    /**
     * 查出设置部门的分类
     * @param $selfDepartmentId
     * @param $departmentId
     * @return array
     * @author xi
     * @date 2018-6-6
     */
    public static function getSelfClass($selfDepartmentId,$departmentId,$classId = 0)
    {
        $where = [
            'a.department_id' => $selfDepartmentId,
            'a.status'        => 1,
            'a.del_status'    => 1,
            'b.del_status'    => 1,
            'a.class_pid'     => $classId
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'.ServiceClass::tableName().'` as b'], 'a.class_id = b.id')
            ->where($where)
            ->select('a.class_id,b.title,b.status')
            ->asArray()
            ->all();

        if($query)
        {
            //查出已设置部门的id
            $classIds = [];
            $classRes = self::findAllByAttributes(['department_id'=>$departmentId,'status'=>1,'del_status'=>1],'class_id');
            if($classRes){
                $classIds = array_column($classRes,'class_id');
            }

            $ids = array_column($query,'class_id');
            $subData = self::getParentClass($ids,$selfDepartmentId);

            $result = [];
            foreach ($query as $val)
            {
                $result[] = [
                    'id'     => $val['class_id'],
                    'name'   => $val['title'],
                    'status' => $val['status'],
                    'checked'=> in_array($val['class_id'], $classIds)?1:0,
                    'sub'    => isset($subData[$val['class_id']])?1:0
                ];
            }

            return $result;
        }

        return [];
    }

    /**
     * 查出父id
     * @param $ids
     * @return array|bool
     */
    public static function getParentClass($ids,$departmentId)
    {
        $where = [
            'a.del_status' => 1,
            'b.del_status' => 1,
            'a.class_pid'   => $ids,
            'a.department_id' => $departmentId
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'.ServiceClass::tableName().'` as b'] , 'a.class_id = b.id')
            ->where($where)
            ->select('a.class_pid,a.id')
            ->asArray()
            ->all();
        if($query){
            return array_column($query,'class_pid','class_pid');
        }
        return false;
    }

    /**
     * 查出自已已选的分类
     * @param $departmentId
     * @return array
     * @author xi
     * @date 2018-6-10
     */
    public static function getCheckedClass($departmentId)
    {
        $where = [
            'a.department_id' => $departmentId,
            'a.status'        => 1,
            'a.del_status'    => 1,
            'b.del_status'    => 1,
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'. ServiceClass::tableName().'` as b'], 'a.class_id = b.id')
            ->where($where)
            ->select('a.class_id,b.title,b.status')
            ->asArray()
            ->all();

        if($query)
        {
            return  array_column($query,'title');
        }
        return [];
    }
}

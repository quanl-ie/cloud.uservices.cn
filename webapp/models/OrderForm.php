<?php
namespace webapp\models;

use Yii;
use yii\base\Model;
class OrderForm extends Model
{
    public $fault_img;
    public $scope_img;
    public function attributeLabels()
    {
        return array(
            'fault_img'       => '故障图片:',
            'scope_img'       => '质保凭证:',
        );
    }
}

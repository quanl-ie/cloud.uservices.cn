<?php
namespace webapp\models;

use common\models\Common;
use Yii;

class ApllDetail extends BaseModel
{

    public static function tableName()
    {
        return 'apll_detail';
    }

    public static function getDetail($sendId)
    {
        $db = self::find();
        $db->where(['send_id'=>$sendId]);
        $db->select('content,update_time');
        $db->orderBy('update_time desc');
        $list = $db ->asArray()->all();
        return $list;
    }   

    public static function addDetail($data)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            if (isset($data) && !empty($data))
            {
                $model = new self();
                foreach ($data as $key => $val) {
                    $model->$key = $val;
                }
                $model->save(false);
            }
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}
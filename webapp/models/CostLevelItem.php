<?php

namespace webapp\models;

use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;

class CostLevelItem extends BaseModel
{
    public static function getDb() {
        return Yii::$app->order_db;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cost_level_item';
    }
    /**
     * 获取单条数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @inheritdoc
     */
    public static function getOne($where = [])
    {
        return self::find()->where($where)->one();
    }
    /**
     * @inheritdoc
     */
    public static function getList($where = [],$andWhere = '')
    {
        return self::find()->where($where)->andWhere($andWhere)->asArray()->all();
    }

    public static function getOnes($where = [])
    {
        return self::find()->where($where)->asArray()->one();
    }
    public static function getLists($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }

}

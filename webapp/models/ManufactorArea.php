<?php

namespace webapp\models;

use common\helpers\Helper;
use webapp\logic\BaseLogic;
use common\models\ServiceArea;
use Yii;

class ManufactorArea extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacturer_area';
    }
    
    /**
     * 获取自身所有省市区字符串
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/20
     * Time: 18:00
     * @param $where
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getList($where)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        //判断where

        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        
        $db->leftJoin([ServiceArea::tableName() . ' as b'],' a.area_id = b.id');
        $db->select('a.id,b.id as area_id,b.area,b.status,a.department_id');
        $db->asArray();
        $list = $db->all();
        return $list;
      
    }
    

    /**
     * 设置服务区域
     * @param $departmentId 部门id
     * @param $brandArr 品牌数组
     * @return bool
     * @author xi
     * @date 2018-6-6
     */
    public static function setServiceArea($departmentId,$areaArr)
    {

        $where = [
            'department_id'=>BaseLogic::getTopId(),
            'area'=>$areaArr
        ];
        $query = ServiceArea::find()
            ->where($where)
            ->asArray()
            ->all();

        //删除所有的
        self::deleteAll(['department_id'=>$departmentId]);

        $data = [];
        foreach ($query as $val){
            $data[] = [$departmentId, Department::getDirectCompanyIdById($departmentId),$val['id'], BaseLogic::getLoginUserId(),time(),time()];
        }

        if($data)
        {
            self::getDb()->createCommand()->batchInsert(
                self::tableName(),
                ['department_id','direct_company_id','area_id','oprate_id','create_time','update_time'],
                $data)
            ->execute();
        }

        return true;
    }

    /**
     * 查出设置部门的收费项目
     * @param $selfDepartmentId
     * @param $departmentId
     * @return array
     * @author xi
     * @date 2018-6-6
     */
    public static function getSelfArea($departmentId)
    {
        $where = [
            'department_id' => $departmentId,
        ];

        $query = self::findAllByAttributes($where,'area_id',false,6000);
        if($query)
        {
            $regionIds = [];
            $serviceAreaIds = array_unique(array_column($query,'area_id'));
            $serviceAreaArr = ServiceArea::getList(['id'=>$serviceAreaIds,'status'=>1]);
            foreach ($serviceAreaArr as $val){
                $regionIds = array_merge($regionIds, explode('_',$val['area']));
            }
            return $regionIds;
        }

        return [];
    }

    /**
     * 查出已选过的省区
     * @param $departmentId
     * @return array
     * @author xi
     * @date 2018-6-10
     */
    public static function getCheckedRegion($departmentId)
    {
        $result = [
            'province' => [],
            'city'     => [],
            'district' => []
        ];

        $where = [
            'department_id' => $departmentId,
        ];
        $query = self::findAllByAttributes($where,'area_id');
        if($query)
        {
            $areaArr   = [];
            $regionIds = [];

            $serviceAreaIds = array_unique(array_column($query,'area_id'));
            $serviceAreaArr = ServiceArea::getList(['id'=>$serviceAreaIds,'status'=>1]);
            foreach ($serviceAreaArr as $val){
                $areaArr[] = $val['area'];
                $regionIds = array_merge($regionIds, explode('_',$val['area']));
            }

            $regionRes = Region::findAllByAttributes(['region_id'=> array_unique($regionIds)],'region_id,region_name');
            $regionArr = [];
            if($regionRes){
                $regionArr = array_column($regionRes,'region_name','region_id');
            }

            foreach ($areaArr as $val)
            {
                $tmpArr = explode('_',$val);
                if(isset($regionArr[$tmpArr[0]])){
                    $result ['province'][$tmpArr[0]] = $regionArr[$tmpArr[0]];
                }
                if(isset($regionArr[$tmpArr[1]])){
                    $result ['city'][$tmpArr[1]] = $regionArr[$tmpArr[1]];
                }
                if(isset($regionArr[$tmpArr[2]])){
                    $result ['district'][$tmpArr[2]] = $regionArr[$tmpArr[2]];
                }
            }
        }

        return $result;
    }

    /**
     * 查出已选择的省份
     * @param $departmentId
     * @return array
     * @author xi
     * @date 2018-6-25
     */
    public static function getChooseProvince($departmentId)
    {
        $where = [
            'department_id' => $departmentId,
        ];

        $query = self::findAllByAttributes($where,'area_id');
        if($query)
        {
            $provinceIds = [];
            $cityIds = [];
            $countyIds = [];

            $serviceAreaIds = array_unique(array_column($query,'area_id'));
            $serviceAreaArr = ServiceArea::getList(['id'=>$serviceAreaIds,'status'=>1]);
            foreach ($serviceAreaArr as $val)
            {
                $areaArr = explode('_',$val['area']);
                $provinceIds[] = $areaArr[0];
                $cityIds[]  = $areaArr[1];
                $countyIds[] = $areaArr[1];
            }
            return [
                'province' => array_unique($provinceIds),
                'city'     => array_unique($cityIds),
                'county'   => array_unique($countyIds)
            ];
        }

        return [
            'province' => [],
            'city'     => [],
            'county'   => []
        ];
    }
    
}

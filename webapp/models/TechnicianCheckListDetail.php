<?php
namespace webapp\models;

use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;
use webapp\models\TechnicianCheckList;

class TechnicianCheckListDetail extends BaseModel
{
    public static function getDb() {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'technician_checklist_detail';
    }

    public static function add($data){
        return $res =self::getDb()->createCommand()->batchInsert(self::tableName(), ['technician_id','work_id','work_no','finish_time','amount','service_content','checklist_id'],$data)->execute();

    }



    public static function getList($where){
        return  self::find()->where($where)->asArray()->all();
    }

    public static function saveDetail($checkListId,$status,$data,$amountReal){
        if($status==3){
           return  $res = TechnicianCheckList::updateStatus($checkListId,3);
        }
        $res = TechnicianCheckList::updateStatus($checkListId,2,$amountReal);
        if($res){
            if(!empty($data)){
                foreach($data as $k=>$v){
                    self::getDb()->createCommand()->update(self::tableName(), ['amount' => $v['amount'],'remark'=>$v['remark']], ['id' => $k])->execute();
                }
            }
            return true;
        }else{
            return false;
        }

    }


}
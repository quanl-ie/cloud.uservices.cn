<?php
namespace webapp\models;

use Yii;
use yii\base\Model;
use webapp\models\Menu;

class MenuForm extends Model
{
    public $name;
    public $url;
    public $parent_id;
    public $is_show;
    public static $result = '';
    public static $data = [];
    public static $k= 0;
    public $weight;
    public $class = '';

    public function rules()
    {
        return [
            [['name','is_show','parent_id'],'required'],
            [['url','class'],'default','value' => null],
            [['weight'],'number']
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'parent_id' => '父级菜单',
            'name'      => '名称',
            'url'       => '路径',
            'is_show'   => '是否显示',
            'weight'    => '排序',
            'class'     => '图标类名'
        ];
    }
    public static function IsShow() {
        return (array(
            0=>'不显示',
            1=>'显示',
        ));
    }

    /**
     * 获取列表
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return object
     */
    public static function getList() {
        $ids = [];
        $data =  Menu::find()->where(['parent_id'=>0,'is_show'=>1])->asArray()->all();
        foreach ($data as $value) {
            $ids[] = $value['id'];
        }
        $find = Menu::find()->where(['parent_id'=>$ids])->asArray()->all();

        foreach ($data as $key=>$value) {
            foreach ($find as $val) {
                if ($value['id']==$val['parent_id']) {
                    $data[$key]['find'][] = $val;
                }
            }
        }
        return $data;
    }
    
    /**
     * 获取菜单列表
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @param String $parentIds
     * @return array
     */
    public static function getMenuList($parentIds,$srcType){
        $arr = Menu::find()
                ->where(['parent_id' => $parentIds,'status' => 1,'src_type'=>$srcType])
                ->orderBy('weight asc')
                ->asArray()
                ->all();
        return $arr;
    }
    
}


<?php
namespace webapp\models;

use common\models\Product;
use Yii;

/**
 * Class ProductMaterials  物料清单关联产品数据
 * @package webapp\models
 */
class  ProductMaterials extends BaseModel
{

    public static function tableName()
    {
        return 'product_materials';  //物料清单关联产品数据
    }

    public function rules()
    {
        return [
            [['id','parent_id','prod_id','num','price','status'],'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'parent_id' => '物料包id（prod_id）',
            'prod_id'   => '关联产品id',
            'num'       => '数量',
            'price'     => '产品市场价',
            'status'    => '状态，1：启用，2：停用',
        ];
    }
    
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    
    public static function getMaterials($map)
    {
        $db = Product::find()
            ->from(Product::tableName() . ' as a')
            ->leftJoin([self::tableName() . ' as b'],'a.id = b.parent_id')
            ->select('b.id,b.prod_id,b.parent_id,a.prod_no,a.prod_name,a.type_id,a.brand_id,a.class_id,a.model,a.unit_id,a.sale_price,a.status,b.num,b.price')
            ->where($map)
            ->asArray();
        return $db->all();
        
    }
}
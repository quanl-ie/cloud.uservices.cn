<?php
namespace webapp\models;

class TechnicianAppraisal extends BaseModel
{
    public static function tableName()
    {
        return 'technician_appraisal_tongji';
    }
    /**
     * 取列表带分页
     * @param array $where
     * @param string $order
     * @param array $fields
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function getlist($where = array(),$fields = array(), $page = 1, $pageSize = 10)
    {
        $db = self::find();
        if ($where) {
            $db->where($where);
        }
        if ($fields) {
            $db->select($fields);
        }
        // 总数
        $totalNum = $db->count();
        // 当有结果时进行组合数据
        if ($totalNum > 0) {
            // 总页数
            $totalPage = ceil($totalNum / $pageSize);

            if ($page < 1) {
                $page = 1;
            } else
                if ($page > $totalPage) {
                    $page = $totalPage;
                }
            $db->offset(($page - 1) * $pageSize);
            $db->limit($pageSize);
            $db->orderBy("composite_score desc");
            $list  = $db->asArray()->all();
            if($list){
                $ids = array_column($list,'technician_id');
                $tec = Technician::findAllByAttributes(['id'=>$ids],'id,name',"id");

                foreach ($list as $k=>$v)
                {
                    $array = json_decode($v['ext_json'],true);
                    $count = [];
                    foreach ($array as $val)
                    {
                        foreach ($val as $value){
                            $count[] = $value['count'];
                        }
                    }
                    $list[$k]['ext_json'] = $count;
                    if(isset($tec[$v['technician_id']])){
                        $list[$k]['technician_name'] = $tec[$v['technician_id']]['name'];
                    }
                }
            }
            return array(
                'totalNum'  => $totalNum,
                'totalPage' => $totalPage,
                'page'      => $page,
                'list'      => $list
            );
        } else {
            return array(
                'totalNum'  => 0,
                'totalPage' => 0,
                'page'      => $page,
                'list'      => array()
            );
        }
    }
    public static function importData($where = array()){
        $list = self::find()->where($where)->orderBy("composite_score desc")->asArray()->all();
        if($list){
            $ids = array_column($list,'technician_id');
            $tec = Technician::findAllByAttributes(['id'=>$ids],'id,name',"id");
            $data = [];
            foreach ($list as $k=>$v)
            {
                if(isset($tec[$v['technician_id']])){
                    $data[$k][] = $tec[$v['technician_id']]['name'];
                }
                $data[$k][] = $v['composite_score'];
                $array = json_decode($v['ext_json'],true);
                foreach ($array as $val)
                {
                    foreach ($val as $value){
                        $data[$k][] = $value['count'];
                    }
                }
            }
            unset($list);
            return $data;
        }
        return array();
    }
}
<?php
namespace webapp\models;

use common\helpers\Helper;
use Yii;
use webapp\models\Region;

/**
 * @author liuxingqi <lxq@c-ntek.com>
 */
class  AccountAddress extends BaseModel
{
    
    public $province_name;
    public $city_name;
    public $district_name;
    public $cityLabel;
    public static function tableName()
    {
        return 'account_address';  //客户表
    }

    public function rules()
    {
        return [
            [['src_type','src_id','lon','lat','province_id','city_id','district_id','address','is_default','account_id','conact_name','mobile_phone'],'required'],
//            ['mobile','match','pattern'=>'/^1[3456789]\d{9}$/','message'=>'手机号格式不正确'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'src_type'     => '来源',  //14 厂商 13 服务商 12 门店 11 技师 20 c端用户',
            'src_id'       => '所有者唯一标识',
            'lon'          => '经度',
            'lat'          => '纬度',
            'province_id'   => '省份',
            'city_id'      => '市',
            'district_id'  => '区域',
            'address'      =>'详细地址',
            'is_default'   =>'是否默认', //1默认地址 2 非默认地址',
            'create_time'  => '创建时间',
            'update_time'  => '修改时间',
            'status'       => '状态', //1：正常，2：停用。
            'account_id'   => '客户标识',  //id
            'conact_name'  => '联系人',
            'mobile_phone' => '联系方式',
        ];
    }
    
    //获取单条信息
    public static function getOne($where)
    {
        return self::find()->where($where)->one();
    }
    //获取列表
    public static function getList($where,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->where($where);
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['create_time'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    //添加
    public static function add($data)
    {
        //实例化客户表模型
        $accountAddress = new self();
        //处理客户联系地址表数据
        foreach ($data as $key => $val) {
            $accountAddress->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //添加客户联系地址表数据
            $accountAddress->save(false);
            $transaction->commit();
            return $accountAddress->id;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    public static function batchInsert($data){
        $row = ["department_id","lon","lat","province_id","city_id","district_id","address","is_default","create_time","update_time","status","account_id","conact_name","mobile_phone"];
        return $res =self::getDb()->createCommand()->batchInsert(self::tableName(), $row,$data)->execute();
    }
    /**
     * 根据用户ID获取用户地址
     * @date 2017-12-20
     * @author liwenyong<liwenyong@uservices.cn>
     * @param  int  $account_id
     * @return json;
     */
    public static function getAddress($account_id) {
        $address = self::find()->where(['account_id' => intval($account_id)])->asArray()->all();
        if(!$address) {
            return array();
        }
        $addressArr = [];
        foreach ($address as $v) {
            $addressArr[$v['id']] = Region::getCityName($v['province_id']).Region::getCityName($v['city_id']).Region::getCityName($v['district_id']).$v['address'];
        }
        return $addressArr;
    }
    /**
     * 根据用户ID获取用户地址
     * @date 2017-12-20
     * @author liwenyong<liwenyong@uservices.cn>
     * @param  int  $account_id
     * @return json;
     */
    public static function getAddressInfo($account_id) {
        $address = self::find()->where(['account_id' => intval($account_id)])->asArray()->all();
        if(!$address) {
            return array();
        }
        $addressArr = [];
        foreach ($address as $k=>$v) {
            $addressArr[$k]['address'] = Region::getCityName($v['province_id']).Region::getCityName($v['city_id']).Region::getCityName($v['district_id']).$v['address'];
            $addressArr[$k]['mobile'] = $v['mobile_phone'];
            $addressArr[$k]['conact_name'] = $v['conact_name'];
            $addressArr[$k]['id'] = $v['id'];
        }
        return $addressArr;
    }
    /**
     * 获取联系人姓名
     */
    public static function getConactName($id) {
        $data = self::find()->where(['id' => $id])->select(['conact_name'])->asArray()->one();
        return $data['conact_name'];
    }

    /**
     * 查出地址id
     * @param $provinceId
     * @param $cityId
     * @param $districtId
     * @return array
     */
    public static function getIdsByRegion($departmentId,$provinceId,$cityId,$districtId)
    {
        $result = [];
        if($provinceId>0 || $cityId>0 || $districtId>0)
        {
            $where = [
                'status'        => 1,
                'department_id' => $departmentId
            ];
            if($districtId > 0){
                $where['district_id'] = $districtId;
            }
            else if($cityId>0){
                $where['city_id'] = $cityId;
            }
            else if($provinceId>0){
                $where['province_id'] = $provinceId;
            }

            $query = self::findAllByAttributes($where,'id');
            if($query){
                $result = array_column($query,'id');
            }
            else {
                $result = [-1];
            }
        }
        return $result;
    }

    /**
     * 根据id 查出详细地址
     * @param $ids
     * @return array
     */
    public static function getAddressByIds($ids)
    {
        $result = [];
        $addressArr = self::findAllByAttributes(['id'=>$ids],'id,province_id,city_id,district_id,address','id');
        if($addressArr)
        {
            $provinceIds = array_column($addressArr,'province_id');
            $cityIds     = array_column($addressArr,'city_id');
            $districtIds = array_column($addressArr,'district_id');

            $regionIds = array_merge($provinceIds,$cityIds,$districtIds);

            $regionArr = Region::findAllByAttributes(['region_id'=>$regionIds],'region_id,region_name','region_id');

            foreach ($addressArr as $val)
            {
                $address = [];
                if(isset($regionArr[$val['province_id']])){
                    $address[] = $regionArr[$val['province_id']]['region_name'];
                }
                if(isset($regionArr[$val['city_id']])){
                    $address[] = $regionArr[$val['city_id']]['region_name'];
                }
                if(isset($regionArr[$val['district_id']])){
                    $address[] = $regionArr[$val['district_id']]['region_name'];
                }
                $address[] = $val['address'];
                $result[$val['id']] = implode('',$address);
            }
        }

        return $result;
    }

    /**
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getAddressById($id)
    {
        $accountAddress = AccountAddress::getOne(['id'=>$id,'status'=>1]);

        //判断是否存在数据
        if ($accountAddress) {
            $accountAddress->province_name = Region::getCityName($accountAddress->province_id);
            $accountAddress->city_name     = Region::getCityName($accountAddress->city_id);
            $accountAddress->district_name = Region::getCityName($accountAddress->district_id);

            $accountAddress->cityLabel = $accountAddress->province_name.'，'.$accountAddress->city_name.'，'.$accountAddress->district_name;
        }
        return $accountAddress;
    }
    
}
<?php
namespace webapp\models;

use Yii;
use yii\db\ActiveRecord;

class  BaseModel extends ActiveRecord
{
    //商家
    const SRC_SJ  = 14;
    //服务商
    const SRC_FWS = 13;
    //门店
    const SRC_MD  = 12;
    //技师
    const SRC_JS  = 11;

    public $dataAuthId    = 0;
    public $departmentId  = 0;
    public $departmentObj = null;
    //直属公司id
    public $directCompanyId = 0;

    /**
     * 重写构造函数
     * BaseModel constructor.
     * @param array $config
     * @throws \Exception
     * @throws \Throwable
     */
    public function __construct(array $config = [])
    {
        //部门信息
        $sessionKey = 'departmentObj';
        if(Yii::$app->session->has($sessionKey)){
            $this->departmentObj = Yii::$app->session->get($sessionKey);
            $this->directCompanyId = $this->departmentObj->direct_company_id;
        }
        //数据权限
        $sessionKey = 'dataAuthId';
        if(Yii::$app->session->has($sessionKey)){
            $this->dataAuthId = Yii::$app->session->get($sessionKey);
        }
        if(!Yii::$app->user->isGuest){
            $this->departmentId = Yii::$app->user->getIdentity()->department_id;
        }

        parent::__construct($config);
    }

    /**
     * @todo  废弃
     * @return int
     * @throws \Exception
     * @throws \Throwable
     */
    public static function getSrcId()
    {
        if(!Yii::$app->user->isGuest){
            return Yii::$app->user->getIdentity()->department_id;
        }
        return -1;
    }

    /**
     * 根据条件查询所有数据
     * @param string/array $where
     * @param string $select
     * @return array
     * @author xi
     * @date 2017-12-15
     */
    public static function findAllByAttributes($where, $select="*",$index = false,$limit = 1000,$page=1,$type=0)
    {
        if($type == 1){ //下单 看现场
            $query = static::find()
                ->where($where)
                ->select($select)
                ->orderBy('intention_type desc,id desc')
                ->offset(($page-1)*$limit)
                ->limit($limit)
                ->asArray()
                ->all();
        }else{  //其他
            $query = static::find()
                ->where($where)
                ->select($select)
                ->offset(($page-1)*$limit)
                ->limit($limit)
                ->asArray()
                ->all();
        }

        if($query)
        {
            if( is_string($index) && in_array($index,array_keys((new static())->getAttributes())) )
            {
                $result = [];
                foreach ($query as $val){
                    $result[$val[$index]] = $val;
                }
                return $result;
            }
    
            return $query;
        }
        return [];
    }
    /**
     * 根据条件查出一条数据
     * @param array/string $where
     * @param string $returnAttr
     * @return \yii\db\ActiveRecord|NULL|string
     * @author xi
     * @date 2017-12-15
     */
    public static function findOneByAttributes($where, $select="*", $returnAttr='')
    {
        $query = static::find()
        ->where($where)
        ->select($select)
        ->asArray()
        ->one();
        if($query)
        {
            if($returnAttr!='' && is_string($returnAttr) && in_array($returnAttr,array_keys((new static())->getAttributes())) )
            {
                return $query[$returnAttr];
            }
            return $query;
        }
        return [];
    }
    
    /**
     * 日子记录
     * @param stirng $message
     * @author xi
     */
    public static function logs($message,$file='')
    {
        if($file == ''){
            $file = '/tmp/logs/'.date('Ym').'/manufactor.log';
        }
        else {
            $file = '/tmp/logs/'.date('Ym').'/'.basename($file);
        }
    
        if( !file_exists(dirname($file))){
            @mkdir(dirname($file),0777,true);
        }
        @file_put_contents($file, "[" . date('Y-m-d H:i:s') . "] ", FILE_APPEND);
        @file_put_contents($file, $message . " \n", FILE_APPEND);
    }

    /**
     * 质保状态
     * @author xi
     */
    public static function getAmountType ($key = false)
    {
        $data = [
            1 => "保内",
            2 => "保外",
            3 => "待协商"
        ];

        if($key !== false){
            return isset($data[$key])?$data[$key]:'';
        }

        return $data;
    }
    /**
     * 生成技师编号
     *
     */
    public static function get_technicianNum()
    {
        $start = rand(0000,9999);
        $middle = substr(time(), 4,9);
        $end = rand(000, 999);
        return $start.$middle.$end;
    }
    
}
<?php

namespace webapp\models;

use common\models\ServiceBrand;
use webapp\logic\BaseLogic;
use Yii;

class ManufactorBrand extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacturer_brand';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'department_id', 'department_top_id', 'brand_id', 'status', 'del_status', 'create_time', 'update_time'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'department_id'     => '（部门表）department 表id',
            'direct_company_id' => '直属公司id',
            'brand_id'          => '品牌id ( tp_service_brand 表)',
            'status'            => '是否启用 1启用 2禁用',
            'del_status'        => '是否删除 1为未删  2为删除',
            'create_time'       => '创建时间',
            'update_time'       => '更新时间',
        ];
    }
    
    
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    
    /**
     * 获取品牌分页列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 13:37
     * @param int $page
     * @param int $pageSize
     * @param array $where
     * @return array
     */
    public static function getPageList($where = [])
    {
        $db = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin([ '`'.ServiceBrand::tableName().'` as b'], 'a.brand_id = b.id')
            ->andWhere(['a.del_status'=>1,'a.status'=>1])
            ->andWhere(['b.del_status'=>1])
            ->select('a.brand_id,b.title,b.sort,b.status,a.create_time');
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        $db->orderBy(['create_time'=> SORT_DESC]);
        $db->asArray();
        $list = $db->all();
        return [
            'list' => $list
        ];
        
    }

    /**
     * 批量添加
     * @param $departmentId 部门id
     * @param $brandArr 品牌数组
     * @return bool
     * @author xi
     * @date 2018-6-6
     */
    public static function batchInsert($departmentId,$brandArr)
    {
        $where = [
            'department_id' => $departmentId,
            'del_status'    => 1
        ];
        $oldBrandIds = [];
        $oldBrandRes = ManufactorBrand::findAllByAttributes($where,'brand_id');
        if($oldBrandRes){
            $oldBrandIds = array_column($oldBrandRes,'brand_id');
        }

        //算出新加的
        $newBrandIds = array_diff($brandArr,$oldBrandIds);
        //算出要删除的
        $delBrandIds = array_diff($oldBrandIds,$brandArr);

        if($newBrandIds)
        {
            foreach ($newBrandIds as $val)
            {
                $brandModel = new ManufactorBrand();
                $brandModel->department_id     = $departmentId;
                $brandModel->direct_company_id = Department::getDirectCompanyIdById($departmentId);
                $brandModel->brand_id          = $val;
                $brandModel->create_time       = time();
                $brandModel->update_time       = time();
                if(!$brandModel->save(false)){
                     return false;
                }
            }
        }
        if($delBrandIds){
            $res = ManufactorBrand::updateAll(['del_status'=>2,'update_time'=>time()],['department_id'=>$departmentId,'brand_id'=>$delBrandIds]);
            if(!$res){
                return false;
            }
        }

        return true;
    }

    /**
     * 查出设置部门的品牌
     * @param $selfDepartmentId
     * @param $departmentId
     * @return array
     * @author xi
     * @date 2018-6-6
     */
    public static function getSelfBrand($selfDepartmentId,$departmentId)
    {
        $where = [
            'a.department_id' => $selfDepartmentId,
            'a.status'        => 1,
            'a.del_status'    => 1,
            'b.del_status'    => 1
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'.ServiceBrand::tableName().'` as b'], 'a.brand_id = b.id')
            ->where($where)
            ->select('a.brand_id,b.title,b.status')
            ->asArray()
            ->all();

        if($query)
        {
            //查出已设置部门的id
            $brandIds = [];
            $brandRes = self::findAllByAttributes(['department_id'=>$departmentId,'status'=>1,'del_status'=>1],'brand_id');
            if($brandRes){
                $brandIds = array_column($brandRes,'brand_id');
            }

            $result = [];
            foreach ($query as $val)
            {
                $result[] = [
                    'id'     => $val['brand_id'],
                    'name'   => $val['title'],
                    'status' => $val['status'],
                    'checked'=> in_array($val['brand_id'], $brandIds)?1:0
                ];
            }
            return $result;
        }

        return [];
    }
    
}

<?php
namespace webapp\models;

use common\models\Common;
use webapp\logic\BaseLogic;
use Yii;
use yii\db\ActiveRecord;

class  Grouping extends BaseModel
{
    public static function tableName()
    {
        return 'grouping';
    }

    //规则
    public function rules()
    {
        return [
            [['department_top_id','department_id','parent_id','name','status','create_user_id','update_user_id','create_time','update_time'],'required'],
            [['name'],'string','max'=>20],
            [['remark'],'string','max'=>20],
            [['department_top_id','department_id','parent_id','status','create_user_id','update_user_id','create_time','update_time','group_leader'],'number'],
            ['name','checkUnique','on'=>'add']
        ];
    }

    public function attributeLabels()
    {
        return array(
            'name' => '组名',
            'group_leader' => '组长',
            'remark' => '备注'
        );
    }

    public function checkUnique($attr,$params)
    {

        $where = [
            'name'    => trim($this->name),
            'department_id'  => BaseLogic::getDepartmentId(),
            'status'  => 1
        ];
        if($this->isNewRecord == true){
            $count = self::find()->where($where)->count();
        }
        else {
            $count = self::find()->where($where)->andWhere(' id <> '.$this->id)->count();
        }
        if($count){
            $this->addError('name','该组名已被使用，请更换！');
        }
    }

    /**
     * 列表
     * @param $where
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function getList($where,$page=1,$pageSize=10, $department_id = 0)
    {
        $db = self::find();
        if(!$department_id){
            $db->where($where);
        } else {
            $db->where(['department_id' => $department_id,'status'=>1]);
        }
       // echo $db->createCommand()->getRawSql();die;
        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }

            $db->orderBy(' id desc');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();

            $groupLeaderIds = array_column($list,'group_leader');
            $techArr = Common::getTechniciansInfo($groupLeaderIds);

            $ids = array_column($list,'id');
            $relTechArr = GroupingRelTechnician::findAllByAttributes(['grouping_id'=>$ids,'status'=>1],'grouping_id','grouping_id');

            foreach ($list as $key=>$val)
            {
                if(isset($techArr[$val['group_leader']])){
                    $list[$key]['group_leader_name'] = $techArr[$val['group_leader']]['name'];
                    $list[$key]['group_leader_mobile'] = $techArr[$val['group_leader']]['mobile'];
                }
                else {
                    $list[$key]['group_leader_name']   = '';
                    $list[$key]['group_leader_mobile'] = '';
                }

                if(isset($relTechArr[$val['id']])){
                    $list[$key]['hasMember'] = 1;
                }
                else {
                    $list[$key]['hasMember'] = 0;
                }
            }


            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 查出没有分组的技师
     * @param $fwsId
     * @return array
     */
    public static function getNoGroupTechnician($fwsId,$exTechId=0)
    {
        $where = [
            'a.store_id' => $fwsId,
            'a.audit_status' => 5,
            'a.status' => 1,
        ];
        $db = Technician::find()
            ->from(Technician::tableName() . ' as a ')
            ->leftJoin(['`'. GroupingRelTechnician::tableName() .'` as b'] ,'a.id = b.technician_id and b.status=1')
            ->where($where)
            ->andWhere('b.id is null');

        if($exTechId){
            $db->orWhere(['in','a.id',$exTechId]);
        }

        $db->select('a.id,a.name');
        $db->asArray();
        $query = $db->all();
        if($query){
            return array_column($query,'name','id');
        }
        return [];
    }

    /**
     * 删除
     * @param $id
     * @return bool
     */
    public static function del($id)
    {
        if($id>0)
        {
            $model = self::findOne(['id'=>$id,'department_top_id'=>BaseLogic::getDirectCompanyId()]);
            if($model){
                $model->status = 0;
                if($model->save()){
                    $groupTecs = GroupingRelTechnician::getGroupingTec($id);
                    if($groupTecs) {
                        if(GroupingRelTechnician::upGroupTec($id)) {
                            return true;
                        }
                    }
                    return true;
                }
            }

        }
        return false;
    }

    /**
     * 查出所有分组
     * @param $srcId
     * @param $select
     * @return array
     * @author xi
     * @date 2018-4-26
     */
    public static function getGroupingAttr($departmentId,$select = 'id,name')
    {
        $where = ['status'   => 1];
        if($departmentId) {
            $where['department_id'] = $departmentId;
        } else {
            $where['create_user_id'] = BaseLogic::getLoginUserId();
        }
        $query = self::find()->where($where)->select($select)->asArray()->all();
        if($query){
            return $query;
        }
        return [];
    }

    /**
     * 删除组长
     */
    public static function removeLeader($group_id) {
        $model = self::findOne(['id' => $group_id]);
        if($model) {
            $model->group_leader = '';
            if($model->save(false)) {
                return true;
            }
        }
        return true;
    }


    //禁用技师后操作分组
    public static function updateGroup($status,$id,$store_id) {
        $group_rel_tec = GroupingRelTechnician::findOne(['technician_id' => $id,'status' => 1,'department_id' => $store_id]);
        if($group_rel_tec && $group_rel_tec->job_title == 2) {
            $group = Grouping::findOne(['id' => $group_rel_tec->grouping_id]);
            $group->group_leader = '';
            $group->save();
            $group_rel_tec->status = 0;
            if($group_rel_tec->save()) {
                return true;
            }
            return false;
        }
        return true;

    }


}
<?php
namespace webapp\models;

use Yii;

class GoodsDetail extends BaseModel
{

    public static function tableName()
    {
        return 'goods_detail';
    }

    /**
     * 查出商品详情
     * @param $goodsId
     * @return array
     */
    public static function getListByGoodsId($goodsId)
    {
        $query = self::findAllByAttributes(['goods_id'=>$goodsId,'status'=>1],'img_url,description');
        if($query){
            foreach ($query as $key=>$val){
                $query[$key]['img_url'] = $val['img_url'] . '?x-oss-process=image/resize,m_lfit,h_500,w_500,m_pad,color_FFFFFF';
            }
        }
        return $query;
    }
    
}
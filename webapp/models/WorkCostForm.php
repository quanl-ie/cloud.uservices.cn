<?php
namespace webapp\models;

use webapp\logic\BaseLogic;
use Yii;
use yii\base\Model;
use webapp\models\ServiceTypeQualification;
use webapp\models\Technician;

class WorkCostForm extends BaseModel

{
    public $voucher_img;

    public static function getDb() {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work_cost';
    }

    public function attributeLabels()
    {
        return [
            'voucher_img'       => '凭证：',
        ];
    }



    public static function getOrderSum($status,$andWhere=[]){
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->select('a.order_no,sum(a.cost_real_amount) as cost_real_amount ');
        $db->rightJoin(WorkOrder::tableName().' as c','a.order_no=c.order_no');
        $db->where(['c.src_type'=>BaseModel::SRC_SJ,'a.src_type'=>BaseModel::SRC_FWS,'a.src_id'=>BaseLogic::getManufactorId(),'a.status'=>1,'c.settlement_status'=>$status,'c.status'=>5]);

        if($andWhere){
            $db->andWhere($andWhere);
        }

        $db->groupBy('a.order_no');
        $count = $db->count();

        $sum = $db->sum('cost_real_amount');
        $sum = $sum?$sum:0;
        //echo $db->createCommand()->getRawSql();
        return ['count'=>$count,'sum'=>$sum];
    }

    public static function getOrderTotalSum($status){
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->select('a.*,c.order_no');
        $db->rightJoin(WorkOrder::tableName().' as c','a.order_no=c.order_no');
        $db->where(['c.src_type'=>BaseModel::SRC_SJ,'a.src_type'=>BaseModel::SRC_FWS,'a.src_id'=>BaseLogic::getManufactorId(),'a.status'=>1,'c.settlement_status'=>$status,'c.status'=>5,'a.payer_id'=>1]);

        $sum      = 0;
        $orderNos = [];

        $query = $db->asArray()->all();
        if($query)
        {
            foreach ( $query as $val )
            {
                $orderNos[$val['order_no']] = $val['order_no'];

				$expectAmount = 0;
                //定额结算
                if($val['cri_cost_type'] == 1)
                {
                    $expectAmount = sprintf('%.2f',($val['cri_fixed_amount'] * $val['cost_number']));
                }
                //百分比结算
                else if($val['cri_cost_type'] == 2)
                {
                    $expectAmount = sprintf('%.2f',(($val['cri_pecent']/100) * $val['cost_real_amount'] ));
                }

                $sum+= $expectAmount;
            }
        }

        return ['count'=>count($orderNos),'sum'=>$sum];
    }



    public static function getWorkList($param = [])
    {
        $db = self::find();
        $pageSize = $param['pageSize'];
        if(!isset($param['currentPage'])){
            $page = 0;
        }else{
            $page = $param['currentPage']-1;
        }

        unset($param['currentPage']);
        unset($param['pageSize']);

        $db->from(self::tableName() . ' as a');
        $db->select('a.work_no,b.technician_id,a.cost_amount,a.cost_real_amount,process_time as finish_time,e.work_type as type_id,c.sale_order_id');
        $db->innerJoin([WorkForm::tableName() . ' as b'],'a.work_no = b.work_no');
        $db->innerJoin(['work_order_detail as c'],'b.work_no=b.work_no');
        $db->innerJoin(['work_order as e'],'on a.order_no=e.order_no');
/*        $db->innerJoin(['work_order as e'],'b.work_no=b.work_no');*/
       // $db->where(['a.src_type'=>BaseModel::SRC_FWS]);
        if(!empty($param['start_time'])){
            $db->andWhere(['>=','finish_time',$param['start_time']]);
            unset($param['start_time']);
        }
        if(!empty($param['end_time'])){
            $db->andWhere(['<=','finish_time',$param['end_time']]);
            unset($param['end_time']);
        }
        foreach($param as $k=>$v){
            if(!empty($v)){
                $db->andwhere([$k=>$v]);
            }
        }

        //$sql = $db->createCommand()->getRawSql();
        //print_r($sql);exit;
        $count = $db->count();
        $totalPage = ceil($count/$pageSize);
        $start = $page*$pageSize;
        $limit = $pageSize;
        $list =$db->offset($start)
            ->limit($pageSize)
            ->asArray()
            ->all();
        $typeList = ServiceTypeQualification::getServiceType(BaseLogic::getManufactorId(),BaseModel::SRC_FWS);
        foreach($list as &$v){
            $v['type_name'] = $typeList[$v['type_id']];
            if($v['technician_id']){
                $tech = Technician::findOne(['id'=>$v['technician_id']]);
                $v['tech_mobile'] = $tech['mobile'];
                $v['tech_name']   = $tech['name'];

            }

        }
        return [
            'page'       => $page+1,
            'totalCount' => $count,
            'totalPage'  => $totalPage,
            'list'       => $list
        ];
    }




    public static function getTechnicianSum($status){
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->select('sum(a.cost_real_amount) as cost_real_amount,a.work_no ');
        $db->innerJoin([WorkForm::tableName() . ' as b'],'a.work_no = b.work_no');
        $db->where(['a.src_type'=>BaseModel::SRC_FWS,'a.status'=>1,'a.src_id'=>BaseLogic::getManufactorId(),'settlement_status'=>$status]);
        $db->groupBy('a.work_no');

        //echo $db->createCommand()->getRawSql();
        $count = $db->count();
        $sum = $db->sum('cost_real_amount');
        $sum = $sum?$sum:0;
        return ['count'=>$count,'sum'=>$sum];
    }




}

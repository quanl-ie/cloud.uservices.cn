<?php

namespace webapp\models;

use Yii;
use yii\helpers\ArrayHelper;

class Position extends BaseModel
{
    public static function tableName()
    {
        return 'position';
    }

    public static function getOne($where) {
        return self::find()->where($where)->asArray()->one();
    }

    public static function getTechnicianPositionByIds($technicianIds)
    {
        if($technicianIds)
        {
            $sql = "SELECT * from (SELECT lon,lat,updated_at,technician_id  FROM ". self::tableName()."  where technician_id in(".implode(',',$technicianIds).") ORDER BY updated_at desc) as t GROUP BY t.technician_id";
            $query = self::getDb()->createCommand($sql)->queryAll();
            if($query)
            {
                return ArrayHelper::index($query,'technician_id');
            }
        }
        return [];
    }
}

<?php
namespace webapp\models;

use Yii;

class  MiniProgram extends BaseModel
{

    public static function tableName()
    {
        return 'mini_program';
    }

    /**
     * 取消授权
     * @param string $appid
     * @return boolean
     * @author xi
     */
    public static function unauthorized($appid)
    {
        $model = self::findOne(['appid'=>$appid]);
        if($model){
            $model->is_authorization = 2;
            $model->update_time      = date('Y-m-d H:i:s');
            return $model->save(false);
        }
        return false;
    }

    /**
     * 修改状态
     * @author xi
     */
    public static function changeAuditStatus($id,$auditStatus)
    {
        $model = self::findOne(['id'=>$id]);
        if($model){
            $model->audit_status = $auditStatus;
            return $model->save(false);
        }
        return false;
    }

    /**
     * 修改 审核id
     * @param $id
     * @param $auditid
     * @return bool
     */
    public static function setAuditId($id,$auditid)
    {
        $model = self::findOne(['id'=>$id]);
        if($model){
            $model->auditid = $auditid;
            return $model->save(false);
        }
        return false;
    }

    /**
     * 保存错误信息
     * @param $id
     * @param $errorMsg
     * @return bool
     */
    public static function addMessage($id,$errorMsg)
    {
        $model = self::findOne(['id'=>$id]);
        if($model){
            $model->error_msg = $errorMsg;
            return $model->save(false);
        }
        return false;
    }

    /**
     * 填上模板消息id
     * @param $id
     * @param $templateId
     * @return bool
     */
    public static function addTemplateId($id,$templateId)
    {
        $model = self::findOne(['id'=>$id]);
        if($model){
            $model->template_id = $templateId;
            return $model->save(false);
        }
        return false;
    }


}
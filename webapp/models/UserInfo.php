<?php
namespace webapp\models;

use Yii;
use yii\db\ActiveRecord;


class  UserInfo extends ActiveRecord
{

    public static function tableName()
    {
        return 'user_info';
    }

    public static function show() {
        return [1 => '厂家直销', 2 => '代理商'];
    }

    //规则
    public function rules()
    {
        return [
            [['user_type','company','mobile','contact','email','address','company_desc','class_id'],'required'],
            [['logo','business_licence'],'safe'],
            [['company'],'string','max'=>15],
            [['address'],'string','max'=>100],
            ['mobile','match','pattern'=>'/^1[3456789]\d{9}$/','message'=>'手机号格式不正确'],
        ];
    }



    public function attributeLabels()
    {
        return array(
            'user_type'         => '企业类型',
            'company'           => '厂家名称',
            'contact'           => '联系人',
            'email'             => '电子邮箱',
            'logo'              => '企业logo',
            'address'           => '地址',
            'business_licence'  => '营业执照',
            'company_desc'      => '公司简介',
            'class_id'          => '经营产品',
        );
    }

/*    public function scenarios()
    {
        return [
            'create' => ['user_id','user_type']
            //'update' => ['user_id','brand_id','brand_name', 'credentials_url','updated_at'],
            //'status' => ['status','updated_at'],
        ];
    }*/


    /**
     * Returns static class instance, which can be used to obtain meta information.
     * @param bool $refresh whether to re-create static instance even, if it is already cached.
     * @return static class instance.
     */
    public static function instance ($refresh = false)
    {
        // TODO: Implement instance() method.
    }
}
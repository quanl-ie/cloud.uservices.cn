<?php
namespace webapp\models;

use common\models\Common;
use Yii;

class Apll extends BaseModel
{

    public static function tableName()
    {
        return 'apll';
    }

    public static function getList(){
        return self::find()
            ->select('id,name')
            ->where(['status'=>1])
            ->orderBy('sort','desc')
            ->asArray()->all();
    }
}
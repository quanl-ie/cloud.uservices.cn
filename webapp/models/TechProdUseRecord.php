<?php

namespace webapp\models;

use Yii;
use webapp\logic\BaseLogic;
use webapp\models\BaseModel;
use common\models\Product;
class TechProdUseRecord extends BaseModel
{
    /**
     * @inheritdoc 技师备件使用记录
     */
    public static function tableName()
    {
        return 'tech_prod_use_record';
    }
    public static function getOne($where = [])
    {
        return self::find()->where($where)->asArray()->one();
    }
    public static function getList1($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    public static function getList ($maps,$where,$page,$pageSize)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->where($maps);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->leftJoin([Product::tableName() . ' as b'],' a.prod_id = b.id');
            $db->select('a.id,a.tech_id,a.prod_id,a.prod_batch,a.used_num,a.create_time,a.work_no,b.prod_name,b.brand_id,b.class_id,b.unit_id,b.prod_no,b.model');
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['a.create_time'=> SORT_DESC]);
            $db->limit($pageSize);
            //print_r($db->createCommand()->getRawSql());die;
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    //添加数据
    public static function add($data){
        $model = new self();
        //处理数据
        foreach ($data as $key => $val) {
            $model->$key = $val;
        }
        //print_r($model);exit;
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //库房表添加数据
            $model->save(false);
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

    }
}

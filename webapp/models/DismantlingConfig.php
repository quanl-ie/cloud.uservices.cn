<?php
namespace webapp\models;

/**
 * Class DismantlingConfig  拆单设置
 * @package webapp\models
 */

class  DismantlingConfig extends BaseModel
{

    public static function tableName()
    {
        return 'sys_switch_config';  //开关设置表
    }

    //规则
    public function rules()
    {
        return [
            [['id','department_id','direct_company_id','status','type','optare_id','create_time'],'required'],
            [['id','department_id','direct_company_id','status','type','optare_id','create_time','update_time'],'integer'],
            
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'department_id'     => '（部门表）department 表id   顶级id',
            'direct_company_id' => '直属公司id',
            'status'            => '状态  1：启用   2：不启用',
            'type'              => '模块',
            'optare_id'         => '操作人id',
            'create_time'       => '创建时间',
            'update_time'       => '修改时间',
        ];
    }
    
    public static function setStatus(){
        return ['1'=>'启用','2'=>'不启用'];
    }
    
    //获取单条信息
    public static function getOne($where)
    {
        return self::find()->where($where)->asArray()->one();
    }
    
    public static function add($data = [])
    {
        if (!empty($data))
        {
            if (isset($data['id']) && !empty($data['id'])){
                $model = self::findOne(['id'=>$data['id']]);
            }else{
                $model = new self();
            }
    
            foreach ($data as $key => $val) {
                $model->$key = $val;
            }
            
            if ($model->save(false)) {
                return true;
            }
            return false;
        }
    }

    /**
     * 是否启用
     * @param $directCompanyId
     * @return bool
     */
    public static function hasEnable($directCompanyId,$type = 1)
    {
        $where = [
            'direct_company_id' => $directCompanyId,
            'type' => $type,
        ];
        $query = DismantlingConfig::findOneByAttributes($where,'status');
        if($query){
            return $query['status'] == 1? true:false;
        }

        return false;
    }

}
<?php
namespace webapp\models;

use yii;
use yii\db\ActiveRecord;


class  AccountBehaviour extends ActiveRecord
{

    public static function tableName()
    {
        return 'account_behaviour';  //客户表
    }

    //规则
//    public function rules()
//    {
//        return [
//            [['user_type','company','mobile','contact','email','address','company_desc','class_id'],'required'],
//            [['logo','business_licence'],'safe'],
//            [['company'],'string','max'=>15],
//            [['address'],'string','max'=>100],
//            ['mobile','match','pattern'=>'/^1[3456789]\d{9}$/','message'=>'手机号格式不正确'],
//        ];
//    }



    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'src_id'         => '所有者唯一标识',
            'src_type'       => '来源',  //14 厂商 13 服务商 12 门店 11 技师 20 c端用户',
            'order_number'   => '工单数量',
            'account_id'     => '用户标识',  //id
            'product_number' => '产品数量',
            'create_time'    => '创建时间',
            'update_time'    => '修改时间',
            'status'         => '状态', //1：正常，2：停用。
        ];
    }
/*    public function scenarios()
    {
        return [
            'create' => ['user_id','user_type']
            //'update' => ['user_id','brand_id','brand_name', 'credentials_url','updated_at'],
            //'status' => ['status','updated_at'],
        ];
    }*/
    
    public static function getOne($where)
    {
        return self::find()->where($where)->one();
    }
    /**
     * 添加
     * @author liuxingqi <lxq@c-ntek.com>
     */
    public static function add($data)
    {
        //实例化客户表模型
        $accountAddress = new self();
        //处理客户联系地址表数据
        foreach ($data as $key => $val) {
            $accountAddress->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //添加客户联系地址表数据
            $accountAddress->save(false);
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}
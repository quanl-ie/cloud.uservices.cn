<?php
namespace webapp\models;

use Yii;
use yii\base\Model;
use common\models\User;

class RegisterForm extends User
{
    public $verified_code;
    public $password;
    public $confirm_password;

    //规则
    public function rules()
    {
        return [
            [['company','mobile','verified_code','password','confirm_password'],'required'],
            [['username','nick_name','status'],'safe'],
            [['company'],'string','max'=>20],
            [['password'],'string','min'=>6,'max'=>18],
            [['confirm_password'],'string','min'=>6,'max'=>18],
            ['confirm_password', 'compare', 'compareAttribute'=>'password', 'message'=>'两次密码不一致'],
            [['mobile'],'unique'],
            ['mobile','match','pattern'=>'/^1[3456789]\d{9}$/','message'=>'请输入正确格式的手机号'],
        ];
    }



    public function attributeLabels()
    {
        return array(
            'company'          => '企业名称',
            'nick_name'        => '昵称',
            'username'         => '用户名',
            'mobile'           => '手机号',
            'verified_code'    => '验证码',
            'password'         => '密码',
            'confirm_password' => '确认密码',
        );
    }

    public static function getCount($where){

         return static::find()->where($where)->count();
    }
}

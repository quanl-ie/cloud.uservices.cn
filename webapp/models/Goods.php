<?php
namespace webapp\models;

use common\models\Common;
use common\models\DictEnum;
use Yii;

class Goods extends BaseModel
{

    public static function tableName()
    {
        return 'goods';
    }

    /**
     * 获取商品类型
     * @param $index
     * @return mixed|string
     */
    public static function getTypeDesc($index)
    {
        $data = [
            1 => '成品',
            2 => '配件',
            4 => '虚拟服务'
        ];

        if($index === false){
            return $data;
        }
        else if(isset($data[$index])){
            return $data[$index];
        }
        return '';
    }

    /**
     * 质保期拼接
     * @param $warrantyNum
     * @param $warrantyUnit
     * @return string
     */
    public static function getWarranty($warrantyNum,$warrantyUnit)
    {
        $unit = [
            1 => '天',
            2 => '月',
            3 => '年'
        ];

        if(isset($unit[$warrantyUnit]) && $warrantyNum > 0){
            return $warrantyNum.$unit[$warrantyUnit];
        }

        return '';
    }

    /**
     * 商品列表
     * @param $where
     * @param int $page
     * @param int $pageSize
     * @return array
     * @author xi
     * @date 2018-9-4
     */
    public static function getList ($where,$page=1,$pageSize=10)
    {
        $db = self::find();

        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }
                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }
        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->orderBy('create_time desc');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $query = $db->all();

            $brandIds = array_unique(array_column($query,'brand_id'));
            $classIds = array_column($query,'class_id');

            $brandArr = Common::getBrandNameByIds($brandIds);
            $classArr = Common::getClassNameByIds($classIds);

            $list = [];
            foreach ($query as $val)
            {
                $list [] = [
                    'id'          => $val['id'],
                    'name'        => $val['name'],
                    'type_desc'   => self::getTypeDesc($val['type_id']),
                    'brand_name'  => isset($brandArr[$val['brand_id']])?$brandArr[$val['brand_id']]:'',
                    'class_name'  => isset($classArr[$val['class_id']])?$classArr[$val['class_id']]:'',
                    'model'       => $val['model']
                ];
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 联系搜索
     * @param $directCompanyId
     * @param $keyword
     * @return array
     * @author xi
     * @date 2018-9-4
     */
    public static function autocompleteSearch($directCompanyId,$keyword)
    {
        $where = [
            'direct_company_id' => $directCompanyId,
            'a.status'            => 1,
            'a.del_status'        => 1,
        ];

        $andWhere = "(a.name like :name or b.goods_no like :goods_no )";

        $db = self::find();
        $db->from( self::tableName() . ' as a');
        $db->leftJoin(['`'.GoodsSKU::tableName().'` as b'],'a.id = b.goods_id and b.status = 1');
        $db->where($where);
        $db->andWhere($andWhere,[':name'=> "%".$keyword."%", ':goods_no'=>"%".$keyword."%"]);
        $db->select('a.id,a.name,a.brand_id,a.class_id');
        $db->groupBy('a.id');
        $db->limit(10);

        $query = $db->asArray()->all();

        $list = [];
        if($query)
        {
            foreach ($query as $val)
            {
                $nameArr = [$val['name']];
                $nameArr[] = Common::getBrandName($val['brand_id']);
                $nameArr[] = Common::getClassName($val['class_id']);
                $nameArr = array_filter($nameArr);

                $list = [
                    'id'   => $val['id'],
                    'name' => implode(' ',$nameArr)
                ];
            }
        }

        return $list;
    }

    /**
     * 查看
     * @param $id
     * @param $directCompanyId
     * @return array
     */
    public static function view($id,$directCompanyId)
    {
        $result = [];

        $where = [
            'id'                => $id,
            'direct_company_id' => $directCompanyId,
            'del_status'        => 1
        ];
        $query = self::findOneByAttributes($where);
        if($query)
        {
            $thumbnail = [];
            if( trim($query['img_url']) != ''){
                $thumbnail = explode(',', $query['img_url']);
                foreach ($thumbnail as $key=>$val){
                    $thumbnail[$key] = $val.'?x-oss-process=image/resize,m_lfit,h_500,w_500,m_pad,color_FFFFFF';
                }
            }

            $brandName = Common::getBrandName($query['brand_id']);
            $className = Common::getClassName($query['class_id']);

            $nameArr = [$query['name'],$brandName, $className];
            $nameArr = array_filter($nameArr);

            $result = [
                'thumbnail' => $thumbnail,
                'price_range' => $query['price_range'],
                'name'        => implode(' ',$nameArr),
                'sku_arr'     => GoodsSKU::getListByGoodsId($query['id']),
                'model'       => $query['model'],
                'type_desc'   => self::getTypeDesc($query['type_id']),
                'unit_desc'   => DictEnum::getDesc('enum_unit_id',$query['unit_id']) ,
                'brand_name'  => $brandName,
                'class_name'  => $className,
                'warranty'    => self::getWarranty($query['warranty_num'],$query['warranty_unit']),
                'detail_arr'  => GoodsDetail::getListByGoodsId($query['id']),
                'property_arr'=> GoodsProperty::getListByGoodsId($query['id'])
            ];
        }

        return $result;
    }


    /**
     * 查看
     * @param $id
     * @param $directCompanyId
     * @return array
     */
    public static function updateView($id,$directCompanyId)
    {
        $where = [
            'id'                => $id,
            'direct_company_id' => $directCompanyId,
            'del_status'        => 1
        ];
        $select = "id,name,product_id,type_id,brand_id,class_id,model,unit_id,warranty_num,warranty_unit,img_url";
        $query = self::findOneByAttributes($where,$select);
        if($query)
        {
            $thumbnail = [];
            if( trim($query['img_url']) != ''){
                $thumbnail = explode(',', $query['img_url']);
            }

            if($query['class_id'] > 0){
                $query['class_name'] = Common::getClassName($query['class_id']);
            }
            else {
                $query['class_name'] = '';
            }

            $query['thumbnail'] = $thumbnail;
            $query['property_arr'] = GoodsProperty::getListByGoodsId($query['id']);
            $query['detail_arr']   = GoodsDetail::getListByGoodsId($query['id']);
            $query['sku_arr']      = GoodsSKU::getListByGoodsId($query['id']);
        }

        return $query;
    }
}
<?php
namespace webapp\models;

use common\models\DictEnum;
use common\models\ServiceBrand;
use Yii;

class DepotProdStock extends BaseModel
{

    public static function tableName()
    {
        return 'depot_prod_stock';
    }


    /**
     * 库存列表
     * @param $where
     * @param int $page
     * @param int $pageSize
     * @return array
     * @author xi
     * @date 2018-9-4
     */
    public static function getList ($where,$page=1,$pageSize=10)
    {
        $db = self::find();

        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }
                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }

        $db->from(self::tableName() . ' as a');
        $db->innerJoin(['`'.Product::tableName().'` as b'] , 'a.prod_id = b.id');
        $db->groupBy('a.prod_id');

        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1){
                $page = 1;
            }

            $db->select("a.prod_id, sum(a.num) AS num, b.prod_name, b.prod_no, b.brand_id,	b.model,b.unit_id");
            $db->orderBy('b.id  DESC');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();


            //查出品牌数据
            $brandIds = array_unique(array_column($list,'brand_id'));
            $brandArr = ServiceBrand::findAllByAttributes(['id'=>$brandIds],'id,title','id');

            //查出计量单
            $unitRes = DictEnum::getDataByKey('enum_unit_id');
            $unitArr = array_column($unitRes,'title','id');

            $count = 1;
            foreach ($list as $key=>$val)
            {
                $list[$key]['tempId'] = (($page - 1) * $pageSize) + ($count);
                $list[$key]['brandName'] = '';
                $list[$key]['unitDesc']  = '';
                if(isset($brandArr[$val['brand_id']])){
                    $list[$key]['brandName'] = $brandArr[$val['brand_id']]['title'];
                }
                if(isset($unitArr[$val['unit_id']])){
                    $list[$key]['unitDesc'] = $unitArr[$val['unit_id']];
                }
                $count++;
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

}
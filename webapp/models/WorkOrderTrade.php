<?php

namespace webapp\models;

use common\models\Work;
use common\models\WorkRelTechnician;
use common\models\WorkOrderDetail;
use common\models\WorkOrderProcess;
use webapp\logic\BaseLogic;
use Yii;

class WorkOrderTrade extends BaseModel
{
    
    public static function getDb()
    {
        return Yii::$app->order_db;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work_order_trade';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'department_id', 'trade_status', 'trade_pay_code', 'pay_time'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'id',
            'department_id'     => '（部门表）department 表id',
            'direct_company_id' => '直属公司id',
            'trade_no'          => '交易流水',
            'work_no'           => '工单号',
            'pay_amount'        => '实收金额   元',
            'trade_status'      => '支付状态：0 未支付 1已支付',
            'trade_pay_code'    => '支付方式：0 微信 1 支付宝 ',
            'pay_time'          => '支付时间',
        ];
    }
    
    public static function getList($where)
    {
        return self::find()->filterWhere($where)->asArray()->all();
    }
    
    /**
     * 获取工单交易记录列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/19
     * Time: 15:14
     * @param int $page
     * @param int $pageSize
     * @param array $where
     * @return array
     */
    public static function getPageList($page = 1,$pageSize = 10,$where = [])
    {
        $db = Work::find();
        $db->from(Work::tableName() . ' as a');
        $db->leftJoin([self::tableName() . ' as b'],' a.work_no = b.work_no');
        $db->leftJoin(['`'. WorkRelTechnician::tableName().'` as c'],'a.work_no = c.work_no');
        $db->andWhere(['a.status'=>4,'a.settlement_status'=>0,'b.trade_status'=>1]);
        $db->select('a.work_no,a.order_no,b.pay_amount,b.trade_pay_code,b.pay_time');
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
    
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['b.pay_time'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    
    /**
     * 待结算订单列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/22
     * Time: 14:10
     * @param int $page
     * @param int $pageSize
     * @param array $where
     * @return array
     */
    public static function waitCostOrderList($page = 1,$pageSize = 10,$where = [])
    {
    
        $db = WorkOrder::find();
        $db->from(WorkOrder::tableName() . ' as a');
        $db->leftJoin([WorkOrderDetail::tableName() . ' as b'],' a.order_no = b.order_no');
        $db->leftJoin([WorkOrderProcess::tableName() . ' as c'],' a.order_no = c.order_no');
        $db->andWhere(['c.current'=>1,'c.status'=>5]);
        $db->select('a.id,a.order_no,a.work_type,c.create_time,b.sale_order_id');
        $db->groupBy('a.id');
        
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
    
        //总数
        $totalNum = $db->count();
        
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['c.create_time'=> SORT_DESC]);
            $db->limit($pageSize);
            
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    
    
    /**
     * 获取待结算工单
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/22
     * Time: 14:10
     * @param $techIds
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getCostWork($techIds)
    {
        $db = Work::find()
            //工单表
            ->from(Work::tableName() . ' as a')
            //工单费用表
            ->innerJoin([WorkCostForm::tableName() . ' as b'],' a.order_no = b.order_no')
            //工单交易记录表
            ->innerJoin([self::tableName() . ' as c'],' a.work_no = c.work_no')
            ->innerJoin(['`'. WorkRelTechnician::tableName() .'` as d '], 'a.work_no = d.work_no')
            //工单表条件
            ->andWhere(['a.status'=>4,'d.technician_id'=>$techIds])
            //工单费用表条件
            ->andWhere(['b.payer_id'=>3,'b.status'=>1])
            //工单交易记录表条件
            ->andWhere(['c.trade_status'=>1,'c.trade_pay_code'=>[0,1]])
            //需要查询的字段
            ->select('a.order_no,a.work_no,c.pay_amount')
            ->asArray()->all();
        return $db;
    }
    
}

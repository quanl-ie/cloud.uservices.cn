<?php

namespace webapp\models;

use common\helpers\Helper;
use common\models\ServiceClass;
use common\models\ServiceType;
use Yii;
class TechnicianSkill extends BaseModel
{
    public static function tableName()
    {
        return 'technician_skill';
    }
    public static function getTechnicianIds ($where) 
    {
        $ids = [];
        $data = self::find()
                ->where($where)
                ->select(['technician_id'])
                ->asArray()->all();
        //$data->createCommand()->getRawSql();
        if($data){
            $ids = array_unique(array_column($data,'technician_id'));
        } 
        return $ids;
    }
    public static function getSkill($id)
    {
        $data = self::find()
                ->where(['technician_id'=>$id])
                ->select(['service_id','class_id'])
                ->asArray()->all();
        return $data;
    }
    //获取服务商所有服务名称
    public static function getTechnicianSkillName($id)
    {
        $technicianSkill = self::getSkill($id);
        $class = [];
        if ($technicianSkill) {
            foreach ($technicianSkill as $key => $val) {
                if ($val['service_id'] == $val['service_id']) {
                    $class[$val['service_id']][] = ServiceClass::getTitle($val['class_id']);
                }
            }
            foreach ($class as $k => $v){
                $v = join("、",$v);
                $class[$k] = $v;
            }
            foreach ($class as $key => $val) {
                $class[$key] = ServiceType::getTitle($key)."：".$val;
            }
        }

        return $class;
    }
}

<?php
namespace webapp\models;

use common\models\ServiceClass;
use Yii;


class Product extends BaseModel
{
    public static function tableName()
    {
        return 'product';
    }

    /**
     * 检索产品名称
     * @param $directCompanyId
     * @param $keyword
     * @return array|\yii\db\ActiveRecord[]
     * @author xi
     * @date 2018-9-5
     */
    public static function searchProductName($directCompanyId,$keyword)
    {
        $where = [
            'direct_company_id' => $directCompanyId,
            'status' => 1,
            'type_id' => [1,2]
        ];
        $query = self::find()
            ->where($where)
            ->andWhere(['like','prod_name',$keyword])
            ->select('id,prod_name,type_id,brand_id,class_id,model,unit_id,warranty_num,warranty_type,sale_price,prod_no')
            ->limit(10)
            ->asArray()
            ->all();

        if($query)
        {
            $classArr = [];
            $classIds = array_column($query,'class_id');
            if($classIds){
                $classArr = ServiceClass::findAllByAttributes(['id'=>$classIds],'id,title','id');
            }

            foreach ($query as $key=>$val)
            {
                $query[$key]['class_name'] = '';
                if(isset($classArr[$val['class_id']])){
                    $query[$key]['class_name'] = $classArr[$val['class_id']]['title'];
                }
            }
        }

        return $query;
    }
}

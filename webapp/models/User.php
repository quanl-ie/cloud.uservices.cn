<?php
namespace webapp\models;

use Yii;

class User extends \common\models\User
{
    public $vcode;

    public $verify_pwd;
    
    public $password;

    public static function tableName()
    {
        return 'user';
    }
    //规则
    public function rules()
    {
        return [
            [['server_city_id', 'last_login_time', 'created_at', 'updated_at'], 'integer'],
            [['username','department_id','mobile', 'password_hash', 'created_at', 'updated_at'], 'required'],
            [['department_pids', 'avatar'], 'string', 'max' => 1000],
            [['company', 'username', 'nick_name', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'role_ids', 'job_title'], 'string', 'max' => 255],
            [['mobile'], 'string', 'max' => 11],
            [['status', 'del_status', 'identity', 'sex'], 'number'],
            [['login_ip','password'], 'string', 'max' => 20],
            ['email','email']
        ];
    }
    
    public function attributeLabels()
    {
        return array(
            'department_id' => '上级机构',
            'company'        => '企业名称',
            'password'       => '初始密码',
            'verify_pwd'     => '确认密码',
            'mobile'         => '手机号',
            'username'       => '用户名',
            'nick_name'      => '昵称',
            'role_ids'       => '担任角色',
            'email'          =>  '邮箱',
            'job_title'      =>  '职位',
            'server_city_id'=> '工作城市'
        );
    }

    /**
     * 根据部门id 查出是否填加了用户
     * @param $departmentId
     * @return bool
     */
    public static function hasUserByDepartmentId($departmentId)
    {
        $where = [
            'department_id' => $departmentId,
            'status'        => 1,
            'del_status'    => 1
        ];
        $count = self::find()
            ->where($where)
            ->count();

        return $count>0?true:false;
    }

    /**
    * 根据部门id 查出是否填加了某用户
    * @param $departmentId $mobile
    * @return bool
    */
    public static function getUserByDepartmentId($departmentId,$mobile)
    {
        $where = [
              'department_id' => $departmentId,
              'mobile'        => $mobile,
              'del_status'    => 1
        ];
        $count = self::find()
              ->where($where)
              ->count();

        return $count>0?true:false;
    }

    /**
     * 获取直属id
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getDirectCompany($id)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->where(['a.id' => $id ]);
        $db->leftJoin([Department::tableName() . ' as b'],' a.department_id = b.id');
        $db->leftJoin([Department::tableName() . ' as c'],' b.direct_company_id = c.id');
        $db->select('a.id,a.username,a.department_id,c.name,c.direct_company_id,c.type');
        $db->asArray();

        return $db->One();

    }
}


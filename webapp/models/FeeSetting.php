<?php
namespace webapp\models;

/**
 * Class DismantlingConfig  拆单设置
 * @package webapp\models
 */

class  FeeSetting extends BaseModel
{

    public static function tableName()
    {
        return 'fee_setting_config';  //拆单设置表
    }

    //规则
    public function rules()
    {
        return [
            [['id','department_id','direct_company_id','status'],'required'],
            [['id','department_id','direct_company_id','status'],'integer'],
            
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'department_id'     => '（部门表）department 表id   顶级id',
            'direct_company_id' => '直属公司id',
            'status'            => '状态  1：启用   0：不启用',
        ];
    }
    
    public static function setStatus(){
        return ['1'=>'启用','0'=>'不启用'];
    }
    
    //获取单条信息
    public static function getOne($where)
    {
        return self::find()->where($where)->asArray()->one();
    }
    
    public static function add($data = [])
    {
        if (!empty($data))
        {
            if (isset($data['id']) && !empty($data['id'])){
                $model = self::findOne(['id'=>$data['id']]);
            }else{
                $model = new self();
            }
    
            foreach ($data as $key => $val) {
                $model->$key = $val;
            }
            
            if ($model->save(false)) {
                return true;
            }
            return false;
        }
    }

}
<?php
namespace webapp\models;

use webapp\logic\BaseLogic;
use Yii;
use yii\db\ActiveRecord;
use webapp\models\ServiceBrandClass;
use common\components\Upload;
use common\models\ScheduleSet;

class  Manufactor extends BaseModel
{
    public $is_main = 1;
    public $province_name;
    public $city_name;
    public $district_name;
    public static function tableName()
    {
        return 'manufactor';
    }

    public static function show() {
        return [1 => '厂家直销', 2 => '代理商'];
    }

    //规则
    public function rules()
    {
        return [
            [['user_type','company','mobile','province_id','city_id','district_id','contact','email','address','company_desc','class_id'],'required'],
            [['logo','business_licence'],'safe'],
            [['company'],'string','max'=>15],
            [['address'],'string','max'=>100],
            ['mobile','match','pattern'=>'/^1[3456789]\d{9}$/','message'=>'手机号格式不正确'],
        ];
    }

    public function attributeLabels()
    {
        return array(
            'user_type'         => '企业类型',
            'company'           => '厂家名称',
            'contact'           => '联系人',
            'email'             => '电子邮箱',
            'logo'              => '企业logo',
            'address'           => '地址',
            'business_licence'  => '营业执照：',
            'code_certificate'  => '组织机构代码证：',
            'company_desc'      => '公司简介',
            'class_id'          => '经营产品',
            
        );
    }
    
    public static function getOne($where)
    {
        return self::find()->where($where)->one();
    }

        
    //添加
    public static function add($data)
    {
        $model = new self();
        //处理数据
        foreach ($data as $key => $val) {
            $model->$key = $val;
        }

        $model->status     = 1;
        $model->created_at = $model->updated_at = time();
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try
        {
            //添加数据
            if(isset($_FILES['business_licence']) && $_FILES['business_licence']['size']>0)
            {
                $upload = new Upload();
                $result = $upload->upImage('manu_order','business_licence');
                if(isset($result['code']) && $result['code'] == 0){
                    $model->business_licence = $result['url'];
                }else{
                    return false;
                }
            }
            if(isset($_FILES['code_certificate']) && $_FILES['business_licence']['size']>0)
            {
                $upload = new Upload();
                $result = $upload->upImage('manu_order','code_certificate');
                if(isset($result['code']) && $result['code'] == 0){
                    $model->code_certificate = $result['url'];
                }
                else{
                    return false;
                }
            }
            if (!$model->save(false)) {
                $transaction->rollBack();
                return false;
            }

            //Department表添加数据
            $department_data['manufactor_id']= $model->id;
            $department_data['name']         = $model->company;
            $department_data['parent_id']    = 0;
            $department_data['parent_ids']   = '';
            $department_data['weight']       = 0;
            $department_data['type']         = 1;
            $department_data['logo']         = '';
            $department_data['contacts']     = $model->contact;
            $department_data['province_id']  = $model->province_id;
            $department_data['city_id']      = $model->city_id;
            $department_data['district_id']  = $model->district_id;
            $department_data['address']      = $model->address;
            $department_data['phone']        = $data['mobile'];
            $department_data['email']        = '';
            $department_data['remark']       = $model->company_desc;
            $department_data['status']       = $department_data['del_status'] = 1;
            $department_data['create_time']  = time();
            $department = Department::add($department_data);

            if($department['id'])
            {
                //修改用户所属商家id和权限
                $user = AdminUser::getOne(['id'=>$model->user_id]);
                $user->department_id            = $department['id'];
                $user->create_direct_company_id = $department['id'];
                $user->role_ids                 = 11;
                $user->updated_at               = time();
                if (!$user->save(false))
                {
                    $transaction->rollBack();
                    return false;
                }

                //修改department表direct_company_id数据
                $department = Department::getOne(['id'=>$department['id']]);
                $department->direct_company_id = $department['id'];
                $department->update_time       = time();
                $department->save(false);
                
            }
            else
            {
                $transaction->rollBack();
                return false;
            }
            $transaction->commit();
            return true;
        }
        catch(\Exception $e)
        {
            $transaction->rollBack();
            throw $e;
        }
        
    }
    //修改企业信息
    public static function edit($data)
    {
        //实例化
        $model = self::getOne(['id'=>$data['id']]);
        //处理数据
        foreach ($data as $key => $val) {
            $model->$key = $val;
        }
        $model->status     = 1;
        $model->updated_at = time();
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //修改数据
            if(isset($_FILES['business_licence']) && $_FILES['business_licence']['size']>0)
            {
                $upload = new Upload();
                $result = $upload->upImage('manu_order','business_licence');
                if(isset($result['code']) && $result['code'] == 0){
                    $model->business_licence = $result['url'];
                }else{
                    return false;
                }
            }
            if(isset($_FILES['code_certificate']) && $_FILES['code_certificate']['size']>0)
            {
                $upload = new Upload();
                $result = $upload->upImage('manu_order','code_certificate');
                if(isset($result['code']) && $result['code'] == 0){
                    $model->code_certificate = $result['url'];
                }else{
                    return false;
                }
            }
            if (!$model->save(false)) {
                $transaction->rollBack();
            }
            //查询department表数据

            $department = Department::getOne(['manufactor_id'=>$model->id]);
            if($department['id']){
                //修改用户所属商家id和权限
                $user = AdminUser::getOne(['id'=>$model->user_id]);
                $user->department_id = $department['id'];
                $user->role_ids = 11; //服务商待审核组
                $user->updated_at = time();
                if (!$user->save(false)) {
                    $transaction->rollBack();
                }
            }else{
                $transaction->rollBack();
            }
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    
    

    //修改基本信息
    public static function editInfo($id,$data)
    {
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            $where = ['id'=>$id];
            $model = self::getOne($where);
            if(isset($_FILES['logo']) && $_FILES['logo']['size']>0)
            {
                $upload = new Upload();
                $result = $upload->upImage('manu_order','logo');
                if(isset($result['code']) && $result['code'] == 0){
                    $model->logo = $result['url'];
                }else{
                    return false;
                }
            }
    
            if ($model->status ==2) {
                foreach ($data as $key => $val) {
                    $model->$key = $val;
                }
                if ($model->save(false)) {
					$transaction->commit();
                    return 1;
                }else{
                    return 2;
                }
            }
            
            $transaction->commit();
			return 3;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    //获取公司名称
    public static function getCompany($manufactorIds)
    {
        $manufactorIds = array_unique($manufactorIds);

        $where = [
            'a.id' => $manufactorIds
        ];
        $query = self::find()
            ->from(AdminUser::tableName() . ' as a')
            ->innerJoin(['`'.self::tableName().'` as b'] , ' a.src_id = b.id and a.src_type = b.src_type ' )
            ->where($where)
            ->select('a.id,b.company,b.mobile')
            ->asArray()
            ->all();

        if($query)
        {
            foreach ($query as $val){
                if ($val['company']) {
                    $manufactorArr[$val['id']] = $val['company'];
                } else {
                    $manufactorArr[$val['id']] = $val['mobile'];
                }
            }
        }
        return $manufactorArr;
    }

    /**
     * 获取多个数据
     * @author liuxingqi <lxq@c-ntek.com>
     */
    public static function getList($where)
    {
        return self::find()->where($where)->asArray()->all();
    }
}
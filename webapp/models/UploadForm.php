<?php
namespace webapp\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $Files;

    public function rules()
    {
        return [
            [
                ['Files'],
                'file',
                'skipOnEmpty'   => false,
                'extensions'    => 'png,jpg,gif,pdf,txt,docx,xlsx',
                'maxFiles'      => 1,
                'maxSize'       => 1024*1024*1024,
                ],
        ];
    }

    public function upload()
    {
        $filesInfo = [];
        if ($this->validate()) {
            /*$ext = $this->Files->baseName . '.' . $this->Files->extension;
            $this->Files->saveAs('uploads/' . $ext);
            $filesInfo ='uploads/' . $ext;*/
            //文件上传存放的目录
            $dir = "../uploads/".date("Ymd");
            if (!is_dir($dir))
            {
                mkdir($dir);
            }else{
                foreach ($this->Files as $file)
                {
                $ext = $file->baseName . '.' . $file->extension;
                $file->saveAs('uploads/' . $ext);
                $filesInfo[] ='uploads/' . $ext;
                }
            }
            print_r($filesInfo);die;
            //$this->imageFile->saveAs('uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return $filesInfo;
        } else {
            return false;
        }
    }


}

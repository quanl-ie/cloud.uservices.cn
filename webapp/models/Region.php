<?php

namespace webapp\models;
use common\helpers\Helper;
use webapp\models\AccountAddress;
use common\models\Common;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "tp_region".
 *
 * @property int $region_id
 * @property string $region_code
 * @property int $parent_id
 * @property string $region_name
 * @property int $region_type
 * @property int $agency_id
 * @property int $is_show
 */
class Region extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'region_type', 'agency_id', 'is_show'], 'integer'],
            [['region_code'], 'string', 'max' => 255],
            [['region_name'], 'string', 'max' => 120],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'region_id' => 'Region ID',
            'region_code' => 'Region Code',
            'parent_id' => 'Parent ID',
            'region_name' => 'Region Name',
            'region_type' => 'Region Type',
            'agency_id' => 'Agency ID',
            'is_show' => 'Is Show',
        ];
    }
    //获取已经开通省份数据
    public static function getOpenProvince()
    {
        //查询已开通城市
        $parentId =  self::find()
                ->select(['region_id'])
                ->where(['=','parent_id',0])
                ->asArray()->all();
        //根据已开通城市获取父级id
        $ids = [];
        foreach ($parentId as $key => $val) {
            $ids[] = $val['region_id'];
        }

        //根据父级id获取所属省份
          $db =  self::find();
          $db->from(self::tableName());
          $db->select(['region_id','region_name']);
          $db->where(['in','parent_id',$ids]);
          $db->asArray();
          $data = $db->all();
          return $data;
    }
    //根据省份获取已开通城市列表
    public static function getOpenCity($province)
    {
        
        //根据省级id查询已开通城市列表
        $data =  self::find()
                ->select(['region_id','region_name'])
                ->where(['parent_id'=>$province])
                ->asArray()->all();
        return $data;
    }
    //获取已开通县区列表
    public static function getOpenDistrict($city_id) {
        $data = self::find()
                ->select('region_id,region_name')
                ->where(['parent_id' =>$city_id])
                ->asArray()->all();
        return $data;
    }
    //获取所有已城市列表
    public static function getOpenCityList()
    {
        $data =  self::find()
                ->select(['region_id','region_name'])
                ->asArray()->all();
        $cityArr[0] = '请选择';
        foreach ($data as $v) {
            $cityArr[$v['region_id']] = $v['region_name'];
        }
        return $cityArr;
    }
    //根据城市id返回城市名称
    public static function getCityName($id=null){
        if(!empty($id)){
            $data = self::find()->select('region_name')->where(['region_id' => $id])->asArray()->one();
            if(!empty($data)){
                return $data['region_name'];
            }
        }
        return false;
    }
    
    /**
     * 通过地址id获取中文地理位置
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $addressIds 地址数组或字符串
     * @param type $type 分类  1、完整地址   2、详细地址
     */
    public static function getAddress($addressIds,$type)
    {
        $addressArr = [];
        $addressDesc = [];
        if ($type == 1) {
            $addressArr = AccountAddress::findAllByAttributes(['id'=>$addressIds,'status'=>1],'id,lon,lat,province_id,city_id,district_id,address','id');
        }else{
            return AccountAddress::findAllByAttributes(['id'=>$addressIds,'status'=>1],'id,address','id');
        }
        foreach ($addressArr as $key => $val) {
            $regionIds[] = $val['province_id'];
            $regionIds[] = $val['city_id'];
            $regionIds[] = $val['district_id'];
            $regionArr = Region::findAllByAttributes(['region_id'=>$regionIds],'region_name,region_id','region_id');
            
            $proviceDesc = $cityDesc = $districtDesc = '';
            if($regionArr)
            {
                if(isset($regionArr[$val['province_id']])){
                    $proviceDesc = $regionArr[$val['province_id']]['region_name'];
                }
                if(isset($regionArr[$val['city_id']])){
                    $cityDesc = $regionArr[$val['city_id']]['region_name'];
                }
                if(isset($regionArr[$val['district_id']])){
                    $districtDesc = $regionArr[$val['district_id']]['region_name'];
                }
            }
            $addressDesc[$key]['lon'] = $val['lon'];
            $addressDesc[$key]['lat'] = $val['lat'];
            $addressDesc[$key]['address'] = $proviceDesc.$cityDesc.$districtDesc.$val['address'];
        }
        if (!is_array($addressIds) && $addressDesc) {
            $addressDesc = $addressDesc[$addressIds];
        }
        return $addressDesc;
    }
}

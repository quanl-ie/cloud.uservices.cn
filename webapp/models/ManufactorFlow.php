<?php

namespace webapp\models;

use common\models\ServiceBrand;
use webapp\logic\BaseLogic;
use Yii;

class ManufactorFlow extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacturer_flow';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'department_id', 'direct_company_id', 'flow_id', 'status', 'del_status', 'create_time', 'update_time'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'department_id'     => '（部门表）department 表id',
            'direct_company_id' => '直属公司id',
            'flow_id'           => '服务流程id( service_flow表)',
            'status'            => '是否启用 1启用 2禁用',
            'del_status'        => '是否删除 1为未删  2为删除',
            'create_time'       => '创建时间',
            'update_time'       => '更新时间',
        ];
    }
    
    
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    
}

<?php
namespace webapp\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\DictEnum;
class Menu extends ActiveRecord
{
    public static $result = '';
    public static $data = [];
    public static $k= 0;

    public static function tableName()
    {
        return 'menu';
    }

    /*
     * 是否显示
     */
    public static function IsShow() {
        return (array(
            0=>'不显示',
            1=>'显示',
        ));
    }

    /**
     * 取列表带分页
     * @param array $where
     * @param string $order
     * @param array $fields
     * @param int $page
     * @param int $pageSize
     * @return array
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date 2017-08-17
     */
    public static function getlist($where = array(), $order = '',$fields = array(), $page = 1, $pageSize = 10)
    {
        $db = self::find();
        if ($where) {
            $db->where($where);
        }
        if ($fields) {
            $db->select($fields);
        }
        // 总数
        $totalNum = $db->count();
        // 当有结果时进行组合数据
        if ($totalNum > 0) {
            // 总页数
            $totalPage = ceil($totalNum / $pageSize);

            if ($page < 1) {
                $page = 1;
            } else
                if ($page > $totalPage) {
                    $page = $totalPage;
                }

            $db->offset(($page - 1) * $pageSize);
            $db->limit($pageSize);
            if ($order != '') {
                $db->orderBy($order);
            }

            $list = [];
            $list  = $db->asArray()->all();

            return array(
                'totalNum'  => $totalNum,
                'totalPage' => $totalPage,
                'page'      => $page,
                'list'      => $list
            );
        } else {
            return array(
                'totalNum'  => 0,
                'totalPage' => 0,
                'page'      => $page,
                'list'      => array()
            );
        }
    }


    /**
     * 获取一级菜单
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @return array
     */
    public static function getParent() {
        $data = self::find()->select(['id', 'name'])->where(['parent_id'=>0,'is_show'=>1])->asArray()->all();
        $menuArr[0] = '一级菜单';
        foreach ($data as $v) {
            $menuArr[$v['id']] = $v['name'];
        }
        return $menuArr;
    }

    /**
     * 获取所有菜单
     * @param type $parentIds
     * @return type
     */
    public static function getMenuArr($parentIds){
        if($parentIds == 0){
            static::$result[0] = "一级菜单";
        }
        $arr = self::find()
            ->where(['src_type'=>BaseModel::SRC_FWS])
            ->asArray()
            ->all();
        foreach($arr as $v){
            if($v['parent_id'] == $parentIds){
                static::$k++;
                $str="∟";
                for($i=0;$i<static::$k-1;$i++){
                    $str .= "∟";
                }
                static::$result[$v['id']] = $str.$v['name'];
                self::getMenuArr($v['id']);
                //static::$result = self::showclass($v['id']);
                static::$k--;
            }
        }
        return static::$result;
    }

    /**
     * 获取层级关系菜单
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     * @param  $parent_id String
     * @return array
     */
    public static function getHierarchy($parent_id=0,$menu_ids,$dataRole)
    {
        $result = [];
        $where = [
            'parent_id' => $parent_id,
            'status'    => 1,
            'is_default'=> 1,
            'FIND_IN_SET('.$dataRole.',data_role) and 1' => '1'
        ];
        $query = self::find()->where($where)->orderBy('weight asc')->asArray()->all();
        if($menu_ids)
        {
            $menu_ids_arr = explode(',',$menu_ids);
            $new_query    = array_column($query,'id');
            $new_menu_ids = array_intersect($new_query,$menu_ids_arr);
            $query = self::find()->where(['id'=>$new_menu_ids])->orderBy('weight asc')->asArray()->all();
        }
        if ($query) {
            foreach($query as $key=>$val){
                $temp = [
                    'id'    => $val['id'],
                    'name'  => $val['name'],
                ];
                $items = static::getHierarchy($val['id'],'',$dataRole);
                if ($items) {
                    $temp['sub'] = $items;
                }
                $result[$val['id']] = $temp;
            }
        }
        return $result;
    }

    /**
     * 查出所有菜单
     * @param number $parent_id
     * @param array $menu_ids
     * @return array
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date   2017-08-17
     */
    public static function getMenu($parent_id=0,$menu_ids=[],$srcType)
    {
        $result = [];
        $where = [
            'parent_id' => $parent_id,
            'status'    => 1,
            'is_show'   => 1,
            'src_type'  => $srcType
        ];
//       if ($menu_ids) {
//           $where['id'] = $menu_ids;
//       }

        $query = self::find()->where($where)->orderBy('weight asc')->select(' id,name,url,parent_id, `class` ')->asArray()->all();
        if ($query) {
            foreach($query as $val)
            {
                $url = trim($val['url'])==''?'#':$val['url'];
                //// 根据角色显示菜单     不能删除 ！！！！！！@todo
                if($menu_ids && in_array($val['id'],$menu_ids)) {
                    $temp = [
                        'label' => $val['name'],
                        'icon' => '',
                        'url' => $url,
                        'class' => $val['class']
                    ];
                    $items = self::getMenu($val['id'], $menu_ids, $srcType);
                    if ($items) {
                        $temp['items'] = $items;
                    }
                    $result[] = $temp;
                }
                //显示全部菜单
                /*if($menu_ids && !in_array($val['id'],$menu_ids)){
                    $url = '#';
                }
                //print_r($menu_ids);die;
                $temp = [
                    'label' => $val['name'],
                    'icon'  => '',
                    'url'   => $url,
                    'class' => $val['class']
                ];

                $items = self::getMenu($val['id'],$menu_ids,$srcType);
                if ($items) {
                    $temp['items'] = $items;
                }
                $result[] = $temp;*/
            }
        }

        return $result;
    }

    /**
     * 根据角色 id 查出菜单  url
     * @return array
     * @author liwenyong<liwenyong@c-ntek.com>
     * @since 2017-08-17
     * @param String $role_ids
     * */
    public static function getMenuUrlByRoleIds($role_ids,$src_type)
    {
        $result = [];

        //$where = 'id in('.$role_ids.') and department_id='.$src_type;
        $where = 'id in('.$role_ids.')';
        $roleArr = Role::find()->where($where)->andWhere(['status'=>[1,2]])->select('menu_ids')->asArray()->all();
        $menuIds = [];
        if ($roleArr) {
            foreach ($roleArr as $val)
            {
                if (trim($val['menu_ids'])!='') {
                    $menuIds = array_merge(explode(',', $val['menu_ids']),$menuIds);
                }
            }
        }
        $menuIds = array_unique($menuIds);
        if ($menuIds) {
            $menuQuery = self::find()->where(['id'=>$menuIds])->select('id,url')->asArray()->all();
            if ($menuQuery) {
                foreach ($menuQuery as $val) {
                    $result['id'][] = $val['id'];
                    $result['url'][] = $val['url'];
                }
            }
        }
        return array_filter($result);
    }
    /**
     * 从数据词典表查出数据权限列表
     * @return array
     * @author sxz
     * @since 2018-06-04
     * @param
     * @return array
     * */
    public static function getDataAuth()
    {
        $List = DictEnum::getList(['dict_key'=>'data_authority']);
        $data = array_column($List,'dict_enum_value','dict_enum_id');
        return $data;
    }
}
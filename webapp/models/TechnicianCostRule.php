<?php
namespace webapp\models;

use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;
use webapp\models\TechnicianCostRuleItem;
use webapp\models\TechnicianCostRuleRelation;
use webapp\models\Technician;
class TechnicianCostRule extends BaseModel
{
    public static function getDb() {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'technician_cost_rule';
    }
    /**
     * 获取单个收费项目
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getOne($where = [])
    {
        return self::find()->where(['src_type'=>self::SRC_SJ,'src_id'=>BaseLogic::getManufactorId()])->andWhere($where)->one();
    }
    /**
     * 获取多个收费项目
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @param type $where
     * @return type
     */

    public static function getList($param)
    {
        //return self::find()->where(['src_type'=>self::SRC_SJ,'src_id'=>BaseLogic::getManufactorId()])->andWhere($where)->all();
        $db = self::find();
        if(isset($param['pageSize'])){
            $pageSize = $param['pageSize'];
        }else{
            $pageSize = 10;
        }



        if(!isset($param['currentPage'])){
            $page = 0;
        }else{
            $page = $param['currentPage']-1;
        }
        unset($param['currentPage']);
        $start = $pageSize*$page;


        //$db->from(self::tableName() . ' as a');
        $db->select('id,technician_cost_name');
        $db->where(['status'=>1,'src_type'=>BaseModel::SRC_FWS,'src_id'=>BaseLogic::getManufactorId()]);

       /* if(isset($param['company'])&&!empty($param['company'])){
            $list = Manufactor::getListInfo($param['company']);
            foreach($list as $v){
                $ids[] = $v['id'];
            }
            $db->andWhere( ['in','service_provider_id',$ids]);
        }*/
        if(isset($param['technician_cost_name'])&&!empty($param['technician_cost_name'])){
            $db->andWhere(['like','technician_cost_name',$param['technician_cost_name']]);
        }
        if(isset($param['technician_ids'])){
            $ruleIds = TechnicianCostRuleRelation::getAll(['technician_id'=>$param['technician_ids']]);
            foreach($ruleIds as $v) {
                $id[] = $v['tech_cost_rule_id'];
            }
            $db->andWhere(['id'=>$id]);
        }

        $count = $db->count();
        $totalPage = ceil($count/$pageSize);
        $db->offset($start);
        $db->asArray();
        $db->limit($pageSize);
        $list = $db->all();
        foreach($list as &$v){
            $ruleId = $v['id'];
            $tech = TechnicianCostRuleRelation::findAll(['tech_cost_rule_id'=>$ruleId]);
            $technician = '';
            $count = count($tech);
            foreach($tech as $key=>$vv){
                $techIds= Technician::findOne(['id'=>$vv['technician_id']]);
                if($count!=($key+1)){

                    $technician .= $techIds['name'].'、';
                }else{

                    $technician .= $techIds['name'];

                }
                //$technician .= $techIds['name'].'、';

            }
            $v['technician'] = $technician;


        }
        return ['totalPage'=>$totalPage,'list'=>$list,'page'=>$page+1];

    }



    /**
     * 添加和编辑收费项目
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @param type $data
     * @return boolean
     * @throws \Exception
     */
    public static function add($costRule,$costItem = array(),$technicianIds = array())
    {

         if(!empty($costRule)){
                $db = self::getDb();
                //$transaction = $db->beginTransaction();
                try {
                    if (!empty($costRule)) {
                        //self::getDb()->createCommand()->batchInsert(self::tableName(),$column, $data['add'])->execute();
                        //验证规则是否已存在
                        $info = self::getOne(['technician_cost_name'=>$costRule['technician_cost_name']]);
                        $info = '';
                        if($info){
                            return false;
                        }else{
                            $model = new self();
                            foreach($costRule as $k=>$v){
                                $model->$k = $v;
                            }
                            $model->save(false);
                            $id = $model->id;

                        }

                        //收费项目入库
                        if($costItem){
                            $itemModel = new TechnicianCostRuleItem();
                            foreach($costItem as &$v){
                                $v['cost_item_id'] = $v['id'];
                                $v['tech_cost_rule_id'] = $id;
                                if($v['cost_type']==1){
                                    $v['fixed_amount'] = $v['value'];
                                    $v['pecent'] = 0;
                                }elseif($v['cost_type']==2){
                                    $v['fixed_amount'] = 0;
                                    $v['pecent'] = $v['value'];
                                }
                                unset($v['value']);
                                unset($v['id']);
                                $v['create_time'] = $v['update_time'] = time();
                                $v['oprate_id'] = 1;
                            }

                            $res =self::getDb()->createCommand()->batchInsert(TechnicianCostRuleItem::tableName(), ['cost_type','cost_item_id','tech_cost_rule_id','fixed_amount','pecent','create_time','update_time','oprate_id'],$costItem)->execute();
                        }

                        if($technicianIds){
                            self::setTechId($technicianIds,$id);
                        }
                       // $transaction->commit();
                    }
                    return true;
                } catch(\Exception $e) {
                    throw $e;
                }
            }
    }



    public static function edit($costRule,$costItem = array(),$technicianIds = array())
    {

        if (!empty($costRule)) {
            $db = self::getDb();
            //$transaction = $db->beginTransaction();
            try {
                if (!empty($costRule)) {
                    $id = $costRule['id'];
                    $info = self::findOne(['id' => $costRule['id']]);
                    if ($info['technician_cost_name'] != $costRule['technician_cost_name']) {
                        $info->technician_cost_name = $costRule['technician_cost_name'];
                        $info->save(false);
                    }

                    //收费项目入库
                    if ($costItem) {
                        foreach($costItem as $v) {
                            $costInfo = TechnicianCostRuleItem::findOne(['id' => $v['id']]);
                            if(!isset($v['cost_type'])){
                                continue;
                            }
                            if ($v['cost_type'] == 1) {
                                $costInfo->fixed_amount = $v['value'];
                                $costInfo->pecent = 0;
                            } elseif ($v['cost_type'] == 2) {
                                $costInfo->pecent = $v['value'];
                                $costInfo->fixed_amount = 0;
                            }
                            $costInfo->cost_type = $v['cost_type'];
                            $costInfo->update_time = time();
                            $costInfo->save(false);
                            unset($costInfo);
                        }
                    }

                    if ($technicianIds) {
                        self::setTechId($technicianIds,$id);
                    }
                    // $transaction->commit();
                }
                return true;
            } catch (\Exception $e) {
                throw $e;
            }


        }
    }



//技师规则关系表更新
    public static  function setTechId($technicianIds,$ruleId)
    {
        $relationModel = new TechnicianCostRuleRelation();
        $techHave = TechnicianCostRuleRelation::findAll(['src_type' => BaseModel::SRC_FWS, 'src_id' => BaseLogic::getManufactorId()]);
        foreach($technicianIds as $k => $v) {
            foreach($techHave as $val) {
                //判断该技师id是否在关系表里
                if ($val['technician_id'] == $v) {
                    TechnicianCostRuleRelation::deleteAll(['id'=>$val['id']]);
                }
                //如果该规则id有已存在的技师，直接删除
                if($val['tech_cost_rule_id']==$ruleId){
                    TechnicianCostRuleRelation::deleteAll(['id'=>$val['id']]);
                }
            }
            $tedata[$k]['src_type'] = BaseModel::SRC_FWS;
            $tedata[$k]['src_id'] = BaseLogic::getManufactorId();
            $tedata[$k]['tech_cost_rule_id'] = $ruleId;
            $tedata[$k]['technician_id'] = $v;

        }

        return self::getDb()->createCommand()->batchInsert($relationModel::tableName(), ['src_type', 'src_id', 'tech_cost_rule_id', 'technician_id'], $tedata)->execute();

    }



}
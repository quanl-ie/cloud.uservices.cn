<?php
namespace webapp\models;

use common\models\OrderDetail;
use phpDocumentor\Reflection\Types\Self_;
use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;
use webapp\models\CheckList;

class CheckListDetail extends BaseModel
{
    public static function getDb() {
        return Yii::$app->order_db;
    }

    
    public static function tableName()
    {
        return 'check_list_detail';
    }

    /**
     * 获取结算单
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getOne($where = [])
    {
        return self::find()->where(['src_type'=>self::SRC_FWS,'src_id'=>BaseLogic::getManufactorId()])->andWhere($where)->one();
    }

    /**
     * 获取结算单列表
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getList($param = [])
    {
        $db = self::find();
        if(!isset($param['pageSize'])){
            $pageSize = 10000;
        }else{
            $pageSize = $param['pageSize'];
        }
        if(!isset($param['currentPage'])){
            $page = 0;
        }else{
            $page = $param['currentPage']-1;
        }

        unset($param['currentPage']);
        unset($param['pageSize']);

        $db->from(self::tableName() . ' as a');
        $db->select('a.order_no,a.type_name,a.class_name,a.brand_name,a.amount,finish_time');
        $db->innerJoin([CheckList::tableName() . ' as b'],'a.check_list_id = b.id');
        if(!empty($param['start_time'])){
            $db->andWhere(['>=','process_time',$param['start_time']]);
            unset($param['start_time']);
        }
        if(!empty($param['end_time'])){
            $db->andWhere(['<=','process_time',$param['end_time']]);
            unset($param['end_time']);
        }
        foreach($param as $k=>$v){
            if(!empty($v)){
                $db->andwhere([$k=>$v]);
            }
        }
        $count = $db->count();
        $totalPage = ceil($count/$pageSize);
        $start = $page*$pageSize;
        $list =$db->offset($start)
            ->limit($pageSize)
            ->asArray()
            ->all();

        if($list)
        {
            $orderNos = array_unique(array_column($list,'order_no'));
            $accountArr = self::getAccountInfo($orderNos);

            foreach ($list as $key=>$val)
            {
                if(isset($accountArr[$val['order_no']])){
                    $list[$key] = array_merge($val,$accountArr[$val['order_no']]);
                }
                else {
                    $list[$key]['accountName']   = '';
                    $list[$key]['accountMobile'] = '';
                    $list[$key]['sn']            = '';
                    $list[$key]['address']       = '';
                }
            }
        }

        return [
            'page'       => $page+1,
            'totalCount' => $count,
            'totalPage'  => $totalPage,
            'list'       => $list
        ];
    }



    public static function add($data){
        return $res =self::getDb()->createCommand()->batchInsert(self::tableName(), ['order_no','order_id','type_id','type_name','class_id','class_name','brand_id','brand_name','finish_time','amount','check_list_id'],$data)->execute();

    }

    /**
     * 根据订单号查出用户信息
     * @param $orderNos
     * @return array
     * @author xi
     * @date 2018-5-23
     */
    public static function getAccountInfo($orderNos)
    {
        $result = [];
        $query = \common\models\Order::find()
            ->from(\common\models\Order::tableName() . ' as a')
            ->innerJoin(['`'.OrderDetail::tableName().'` as b'], 'a.order_no=b.order_no')
            ->where(['a.order_no'=>$orderNos])
            ->select('a.order_no,a.account_id,a.address_id,b.sale_order_id')
            ->asArray()
            ->all();

        if($query)
        {
            $accountIds   = array_column($query,'account_id');
            $addressIds   = array_column($query,'address_id');
            $saleOrderIds = array_column($query,'sale_order_id');

            $accountArr = Account::findAllByAttributes(['id'=>$accountIds],'id,account_name,mobile','id');
            $addressArr = AccountAddress::findAllByAttributes(['id'=>$addressIds],'id,province_id,city_id,district_id,address','id');
            $saleOrderArr = SaleOrder::findAllByAttributes(['id'=>$saleOrderIds],'id,serial_number','id');

            $regionArr = [];
            if($addressArr){
                $provinceIds = array_column($addressArr,'province_id');
                $cityIds     = array_column($addressArr,'city_id');
                $districtIds = array_column($addressArr,'district_id');

                $regionIds = array_unique(array_merge($provinceIds,$cityIds,$districtIds));
                $regionArr = Region::findAllByAttributes(['region_id'=>$regionIds],'region_id,region_name','region_id');
            }

            foreach ($query as $val)
            {
                $address = [];
                if($addressArr[$val['address_id']]){
                    $tmpAddressArr = $addressArr[$val['address_id']];
                    if(isset($regionArr[$tmpAddressArr['province_id']])){
                        $address [] = $regionArr[$tmpAddressArr['province_id']]['region_name'];
                    }
                    if(isset($regionArr[$tmpAddressArr['city_id']])){
                        $address [] = $regionArr[$tmpAddressArr['city_id']]['region_name'];
                    }
                    if(isset($regionArr[$tmpAddressArr['district_id']])){
                        $address [] = $regionArr[$tmpAddressArr['district_id']]['region_name'];
                    }
                    $address[] = $addressArr[$val['address_id']]['address'];
                }
                $result[$val['order_no']] = [
                    'accountName'   => isset($accountArr[$val['account_id']])?$accountArr[$val['account_id']]['account_name']:'',
                    'accountMobile' => isset($accountArr[$val['account_id']])?$accountArr[$val['account_id']]['mobile']:'',
                    'address'       => implode('',$address),
                    'sn'            => isset($saleOrderArr[$val['sale_order_id']])?$saleOrderArr[$val['sale_order_id']]['serial_number']:''
                ];
            }

        }
        return $result;
    }
}
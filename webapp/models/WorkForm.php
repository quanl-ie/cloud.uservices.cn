<?php
namespace webapp\models;

use Yii;
use yii\base\Model;
use common\models\ServiceFlow;
class WorkForm extends BaseModel
{
    public $scope_img;
    public $work_img;
    public $scene_img;

    public static function getDb() {
        return Yii::$app->order_db;
    }

    public static function tableName()
    {
        return 'work';
    }

    public function attributeLabels()
    {
        return [
            'scope_img'       => '质保凭证：',
            'work_img'       => '服务工单：',
            'scene_img'       => '现场拍照：',
        ];
    }



    public static function getList($param = [])
    {
        $db = self::find();
        $pageSize = $param['pageSize'];
        if(!isset($param['currentPage'])){
            $page = 0;
        }else{
            $page = $param['currentPage']-1;
        }

        unset($param['currentPage']);
        unset($param['pageSize']);

        $db->from(self::tableName() . ' as a');
        $db->select('a.work_no,a.order_no,a.technician_id,a.update_time as finish_time,b.work_type as type_id,c.sale_order_id,d.cost_amount,sum(d.cost_real_amount) as cost_real_amount,work_stage');
        //$db->innerJoin([WorkForm::tableName() . ' as b'],'a.order_no = b.order_no');
        $db->innerJoin(['work_order as b'],'a.order_no=b.order_no');
        $db->innerJoin(['work_order_detail as c'],'a.order_no=c.order_no');
        $db->innerJoin(['work_cost as d'],'a.work_no=d.work_no');
       // $db->where(['src_id'=>$param['src_id'],'src_type'=>$param['src_type']]);
        if(!empty($param['start_time'])){
            $db->andWhere(['>=','update_time',$param['start_time']]);
            unset($param['start_time']);
        }
        if(!empty($param['end_time'])){
            $db->andWhere(['<=','update_time',$param['end_time']]);
            unset($param['end_time']);
        }

        foreach($param as $k=>$v){
            if(!empty($v)){
                $db->andwhere([$k=>$v]);
            }
        }
        $db->groupBy('a.work_no');


        $count = $db->count();
        $totalPage = ceil($count/$pageSize);
        $start = $page*$pageSize;
        $limit = $pageSize;
        $list =$db->offset($start)
            ->limit($pageSize)
            ->asArray()
            ->all();
        //echo $db->createCommand()->getRawSql();exit;
        if(empty($list))
            return false;
        $typeList = ServiceTypeQualification::getServiceType($param['a.src_id'],$param['a.src_type']);

        foreach($list as &$v){
            if(isset($typeList[$v['type_id']])){
                $v['type_name'] = $typeList[$v['type_id']];
            }
           // $cost = WorkCostForm::find()->where(['order_no'=>$v['order_no']])->sum('cost_amount');
            $service = SaleOrder::findOne(['id'=>$v['sale_order_id']]);
            $v['brand_name'] = $service['brand_name'];
            $v['class_name'] = $service['class_name'];
            //$v['amount'] = $cost;
            if($v['technician_id']){
                $tech = Technician::findOne(['id'=>$v['technician_id']]);
                $v['tech_mobile'] = $tech['mobile'];
                $v['tech_name']   = $tech['name'];

            }
            $serviceFlow = ServiceFlow::getOne(['id'=>$v['work_stage']]);
			if($serviceFlow){
				$v['flow_name'] = $serviceFlow->service_flow_name;
			}
			else {
				$v['flow_name'] = '';
			}
        }

        return [
            'page'       => $page+1,
            'totalCount' => $count,
            'totalPage'  => $totalPage,
            'list'       => $list
        ];
    }

}

<?php
namespace webapp\models;

use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;
use webapp\models\CostItem;

class TechnicianCostRuleItem extends BaseModel
{

    public static function getDb() {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'technician_cost_rule_item';
    }
    /**
     * 获取单个收费项目
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getOne($where = [])
    {
        return self::find()->andWhere($where)->one();
    }


    public static function getAll($where=[]){
        return self::find()->where($where)->asArray()->all();
    }


}
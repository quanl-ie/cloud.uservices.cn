<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tp_store_member".
 *
 * @property int $id 技师主键
 * @property string $name 姓名
 * @property string $tel 手机号
 * @property string $nickname 昵称
 * @property string $password 登录密码
 * @property int $sex 性别 1、男 2、女
 * @property string $birthday 生日
 * @property string $label_id 技能标签
 * @property string $identity_card 身份证号
 * @property int $store_id 所属经销商
 * @property string $image 头像
 * @property string $native_place 籍贯
 * @property int $is_work 接单状态  0:下班 1:上班
 * @property string $province 省份
 * @property string $city 城市
 * @property string $addresss 详细地址
 * @property int $order_count 订单数
 * @property string $stars 好评星级
 * @property string $gps 地图查看
 * @property int $last_time 最后登录
 * @property string $lat 高德gps纬度
 * @property string $lon 高德gps经度
 * @property string $address 中文地理位置
 * @property string $jpush_id 极光推送标识
 * @property int $service_class_id 可服务的产品
 * @property int $money 技师当前余额
 * @property string $other_name 紧急联系人
 * @property string $other_tel 紧急联系人电话
 * @property int $isdelete 删除状态，1-删除 | 0-正常
 * @property int $status 状态，1-正常 | 0-禁用
 * @property int $create_time 创建时间
 * @property int $update_time 更新时间
 */
class StoreMember extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tp_store_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sex', 'store_id', 'is_work', 'order_count', 'last_time', 'service_class_id', 'money', 'isdelete', 'status', 'create_time', 'update_time'], 'integer'],
            [['name', 'tel', 'nickname', 'password', 'label_id', 'identity_card', 'image', 'stars', 'gps', 'lat', 'lon', 'address', 'jpush_id', 'other_name', 'other_tel'], 'string', 'max' => 255],
            [['birthday', 'province'], 'string', 'max' => 20],
            [['native_place', 'city', 'addresss'], 'string', 'max' => 222],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '技师主键',
            'name' => '姓名',
            'tel' => '手机号',
            'nickname' => '昵称',
            'password' => '登录密码',
            'sex' => '性别 1、男 2、女',
            'birthday' => '生日',
            'label_id' => '技能标签',
            'identity_card' => '身份证号',
            'store_id' => '所属经销商',
            'image' => '头像',
            'native_place' => '籍贯',
            'is_work' => '接单状态  0:下班 1:上班',
            'province' => '省份',
            'city' => '城市',
            'addresss' => '详细地址',
            'order_count' => '订单数',
            'stars' => '好评星级',
            'gps' => '地图查看',
            'last_time' => '最后登录',
            'lat' => '高德gps纬度',
            'lon' => '高德gps经度',
            'address' => '中文地理位置',
            'jpush_id' => '极光推送标识',
            'service_class_id' => '可服务的产品',
            'money' => '技师当前余额',
            'other_name' => '紧急联系人',
            'other_tel' => '紧急联系人电话',
            'isdelete' => '删除状态，1-删除 | 0-正常',
            'status' => '状态，1-正常 | 0-禁用',
            'create_time' => '创建时间',
            'update_time' => '更新时间',
        ];
    }
    public static function getInfos($id=null) {
        $data = self::find()
                ->select(['id','name','tel'])
                ->where(['isdelete' => 0,'id'=>$id])
                ->asArray()->one();
        return $data;
    }
}

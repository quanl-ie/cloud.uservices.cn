<?php
namespace webapp\models;

use Yii;

class GoodsSKU extends BaseModel
{

    public static function tableName()
    {
        return 'goods_sku';
    }

    /**
     * 查出列表
     * @param $goodsId
     * @return array
     */
    public static function getListByGoodsId($goodsId)
    {
        $query = self::findAllByAttributes(['goods_id'=>$goodsId,'status'=>1],'goods_no,name,market_price');
        return $query;
    }
}
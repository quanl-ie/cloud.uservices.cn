<?php

namespace webapp\models;

use common\models\ServiceType;
use webapp\logic\BaseLogic;
use Yii;

class ManufactorType extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacturer_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'department_id', 'department_top_id', 'type_id', 'status', 'del_status', 'create_time', 'update_time'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'department_id'     => '（部门表）department 表id',
            'direct_company_id' => '直属公司id',
            'type_id'           => '服务类型id( tp_service_type表)',
            'status'            => '是否启用 1启用 2禁用',
            'del_status'        => '是否删除 1为未删  2为删除',
            'create_time'       => '创建时间',
            'update_time'       => '更新时间',
        ];
    }
    
    /**
     * 获取数据列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 16:54
     * @param array $where
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    
    /**
     * 获取类型分页列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 13:37
     * @param int $page
     * @param int $pageSize
     * @param array $where
     * @return array
     */
    public static function getPageList($where = [])
    {
        $db = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin([ '`'.ServiceType::tableName().'` as b'], 'a.type_id = b.id')
            ->andWhere(['a.del_status'=>1,'a.status'=>1])
            ->andWhere(['b.del_status'=>1])
            ->select('a.type_id,b.title,b.sort,b.status,a.create_time');

        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        $db->orderBy(['a.create_time'=> SORT_DESC]);
        $db->asArray();
        $list = $db->all();
        return [
            'list' => $list
        ];
    
    }
    
    /**
     * 批量添加
     * @param $departmentId 部门id
     * @param $brandArr 品牌数组
     * @return bool
     * @author xi
     * @date 2018-6-6
     */
    public static function batchInsert($departmentId,$typeArr)
    {
        $where = [
            'department_id' => $departmentId,
            'del_status'    => 1
        ];
        $oldTypeIds = [];
        $oldTypeRes = self::findAllByAttributes($where,'type_id');
        if($oldTypeRes){
            $oldTypeIds = array_column($oldTypeRes,'type_id');
        }

        //算出新加的
        $newTypeIds = array_diff($typeArr,$oldTypeIds);
        //算出要删除的
        $delTypeIds = array_diff($oldTypeIds,$typeArr);

        if($newTypeIds)
        {
            foreach ($newTypeIds as $val)
            {
                $model = new self();
                $model->department_id     = $departmentId;
                $model->direct_company_id = Department::getDirectCompanyIdById($departmentId);
                $model->type_id           = $val;
                $model->create_time       = time();
                $model->update_time       = time();
                if(!$model->save(false)){
                    return false;
                }
            }
        }
        if($delTypeIds){
            $res = self::updateAll(['del_status'=>2,'update_time'=>time()],['department_id'=>$departmentId,'type_id'=>$delTypeIds]);
            if(!$res){
                return false;
            }
        }

        return true;
    }

    /**
     * 查出设置部门的收费项目
     * @param $selfDepartmentId
     * @param $departmentId
     * @return array
     * @author xi
     * @date 2018-6-6
     */
    public static function getSelfType($selfDepartmentId,$departmentId)
    {
        $where = [
            'a.department_id' => $selfDepartmentId,
            'a.status'        => 1,
            'a.del_status'    => 1,
            'b.del_status'    => 1
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin([ '`'.ServiceType::tableName().'` as b'], 'a.type_id = b.id')
            ->where($where)
            ->select('a.type_id,b.title,b.status')
            ->asArray()
            ->all();

        if($query)
        {
            //查出已设置部门的id
            $typeIds = [];
            $typeRes = self::findAllByAttributes(['department_id'=>$departmentId,'status'=>1,'del_status'=>1],'type_id');
            if($typeRes){
                $typeIds = array_column($typeRes,'type_id');
            }

            $result = [];
            foreach ($query as $val)
            {
                $result[] = [
                    'id'     => $val['type_id'],
                    'name'   => $val['title'],
                    'status' => $val['status'],
                    'checked'=> in_array($val['type_id'], $typeIds)?1:0
                ];
            }
            return $result;
        }

        return [];
    }

    /**
     * 函数用途描述：获取服务类型
     * @date: 2018年1月31日 下午4:48:01
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function getServiceType()
    {
        $departmentId =  BaseLogic::getDepartmentId();
        $directId = BaseLogic::getDirectCompanyId();
        $where = [
            'b.status'     => 1,
            'b.del_status' => 1,
            //'a.department_id' => $departmentId
            'a.direct_company_id'=>$directId
        ];

        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin(['`'. ServiceType::tableName().'` as b'],' a.type_id = b.id ')
            ->select('b.id, b.title')
            ->orderBy('sort desc')
            ->where($where)
            ->asArray()->all();

        if($query){
            return $query;
        }
        return [];
    }
    
}

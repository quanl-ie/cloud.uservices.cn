<?php
/**
 * Created by PhpStorm.
 * User: quanl
 * Date: 2018/8/6
 * Time: 10:36
 */
namespace webapp\models;
use common\helpers\Helper;
use common\models\Common;
use webapp\logic\BaseLogic;
use Yii;
class ContractType extends BaseModel
{
    public static function tableName()
    {
        return 'contract_type';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['direct_company_id','department_id','type_name','rank','status'], 'required'],
            [['type_name'], 'string', 'max' => 30],
            [['rank', 'status','create_id','create_time'], 'number'],
            ['rank','validateWeight'],
        ];
    }
    /**
     * 列表
     * @param $where
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function getList($where,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->where($where);
        //echo $db->createCommand()->getRawSql();die;
        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }

            $db->orderBy(' id desc');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    /**
     * 添加合同类型
     * User: quan
     * Date: 2018/8/6
     * Time: 11:31
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public static function add($data)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            if (isset($data) && !empty($data))
            {
                $model = new self();
                foreach ($data as $key => $val) {
                    $model->$key = $val;
                }
                $model->save(false);
            }
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    /**
     * 修改合同类型
     * User: quan
     * Date: 2018/8/6
     * Time: 11:31
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public static function edit($data)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            if (isset($data) && !empty($data))
            {
                $model = self::findOne(['id'=>$data['id']]);
                unset($data['id']);
                foreach ($data as $key => $val) {
                    $model->$key = $val;
                }
                $model->save(false);
            }
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    /**
     * 修改状态
     * User: quan
     * Date: 2018/8/6
     * Time: 14:20
     */
    public static function changeStatus($data)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            if (isset($data) && !empty($data))
            {
                $model = self::findOne(['id'=>$data['id']]);
                unset($data['id']);
                foreach ($data as $key => $val) {
                    $model->$key = $val;
                }
                $model->save(false);
            }
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public static function getLists($where)
    {
        $db = self::find();
        $db->where($where);
        $db->orderBy(' rank asc');
        $db->asArray();
        $list = $db->all();
        return $list;

    }
}
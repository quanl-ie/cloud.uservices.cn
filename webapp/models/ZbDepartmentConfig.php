<?php
namespace webapp\models;
class  ZbDepartmentConfig extends BaseModel
{
    public static function tableName()
    {
        return 'zb_department_config';
    }
    //获取多条信息
    public static function getList($map,$where,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->where($map);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }

        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->select('a.id,a.department_id,a.status');
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['a.id'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    //获取单条信息
    public static function getOne($where)
    {
        return self::find()->where($where)->one();
    }
    //添加
    public static function add($data)
    {
        //实例化
        $zb = new self();

        foreach ($data as $key => $val) {
            $zb->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //添加
            $zb->save(false);
            $transaction->commit();
            return [
                'id' => $zb->id,
                'department_id' => $zb->department_id,
                'status'=>$zb->status
            ];
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    //编辑
    public static function edit($data)
    {
        //查询客户表数据
        $where['id'] = $data['id'];
        $zb = self::getOne($where);
        foreach ($data as $key => $val) {
            $zb->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //修改客户表数据
            $zb->save(false);
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return false;
    }

}
?>
<?php

namespace webapp\models;

use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;

class CostLevelDetail extends BaseModel
{
    public static function getDb() {
        return Yii::$app->order_db;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cost_level_detail';
    }
    /**
     * 获取单条数据
     */
    public static function getOne($where = [])
    {
        return self::find()->where($where)->one();
    }
    /**
     * @inheritdoc
     */
    public static function getList($where = [],$andWhere=[])
    {
        return self::find()->where($where)->andWhere($andWhere)->asArray()->all();
    }
    //查询服务商制定的收费规则
    public static function getCheckList($cost_level_id,$brand_id,$class_id,$service_type)
    {
        $data =  self::find()
            ->where(['status'=>1,'cost_level_id'=>$cost_level_id])
            ->andWhere(['or',"brand_id = '$brand_id'","brand_id = 0"])
            ->andWhere(['or',"class_id = '$class_id'","class_id = 0"])
            ->andWhere(['or',"type_id = '$service_type'","type_id = 0"])
            ->asArray()->all();
        return $data;
    }
    //查询收费项目是否重复
    public static function checkRep($detailData,$classIdArr,$chargeObj)
    {
        $one =  self::find()
            ->where(['status'=>[1,3], 'department_top_id'=>$detailData['department_top_id'], 'brand_id'=>$detailData['brand_id'], 'type_id'=>$detailData['type_id'],'class_id'=>$classIdArr])
            //->where(['status'=>[1,3], 'cost_level_id'=>$detailData['cost_level_id'], 'brand_id'=>$detailData['brand_id'], 'type_id'=>$detailData['type_id'],'class_id'=>$classIdArr])
            ->asArray()->all();
        if (empty($one)) {
            return true;
        }
        $new_service_area = explode(',',$detailData['service_area']);
        foreach ($one as $val) {
            $old_service_area = explode(',',$val['service_area']);
            if(count($old_service_area) >= count($new_service_area)){
                $dif = array_diff($new_service_area,$old_service_area);
            }else{
                $dif = array_diff($old_service_area,$new_service_area);
            }
            if (!empty($dif)) {
                $flag[1][] = $val['id'];
            }else{
                $flag[2][] = $val['id'];
            }
        }
        if(isset($flag[2])){
            $costLevelItem = CostLevelItem::getlist(['level_detail_id'=>$flag[2],'cost_item_id'=>$chargeObj[0],'status'=>[1,3]]);
            if (!empty($costLevelItem)) {
                return false;
            }else{
                return true;
            }
        }
        return true;
    }
}

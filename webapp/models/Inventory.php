<?php
namespace webapp\models;

use common\helpers\CreateNoRule;
use common\models\Common;
use common\models\Counter;
use common\models\DictEnum;
use common\models\ServiceClass;
use Yii;

class Inventory extends BaseModel
{

    public static function tableName()
    {
        return 'inventory';
    }

    /**
     * 获取状态
     * @param bool $index
     * @return array|mixed|string
     */
    public static function getStatus($index = false)
    {
        $data = [
            1 => '盘点中',
            2 => '盘点完毕',
            3 => '已作废'
        ];

        if($index){
            return isset($data[$index])?$data[$index]:'';
        }
        return $data;
    }

    /**
     * 获取盘点单号
     * @return string
     */
    public static function getOddNum()
    {
        $count = Counter::getCounter(1);
        return "PD".date('Ymd') . sprintf('%03d',$count);
    }

    /**
     * 盘点列表
     * @param $where
     * @param int $page
     * @param int $pageSize
     * @return array
     * @author xi
     */
    public static function getList ($where,$page=1,$pageSize=10)
    {
        $db = self::find();

        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }
                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }

        $db->from(self::tableName() . ' as a');
        $db->innerJoin(['`'.Depot::tableName().'` as b'] , 'a.warehouse_id = b.id');

        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1){
                $page = 1;
            }

            $db->select("a.id,a.odd_num,a.subject,a.type,a.warehouse_id,a.status,a.create_user_id,a.create_time,b.name");
            $db->orderBy('a.id  DESC');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();

            //盘点类型
            $typeRes = DictEnum::getDataByKey('inventory_type');
            $typeArr = array_column($typeRes,'title','id');

            //盘点人
            $userArr = [];
            $userIds = array_column($list,'create_user_id');
            $userRes = User::find()->where(['id'=>$userIds])->select('id,username')->asArray()->all();
            if($userRes){
                $userArr = array_column($userRes,'username','id');
            }

            foreach ($list as $key=>$val)
            {
                $list[$key]['typeDesc']       = isset($typeArr[$val['type']])?$typeArr[$val['type']]:'';
                $list[$key]['statusDesc']     = self::getStatus($val['status']);
                $list[$key]['create_time']    = date('Y-m-d H:i:s',$val['create_time']);
                $list[$key]['createUserName'] = isset($userArr[$val['create_user_id']])?$userArr[$val['create_user_id']]:'';
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 盘点详情
     * @param $id
     */
    public static function viewById($id)
    {
        $where = [
            'direct_company_id' => (new self())->directCompanyId,
            'id' => $id
        ];

        $query = self::findOneByAttributes($where);
        if($query)
        {
            //如果盘点中就更新时间
            if($query['status'] == 1){
                $query['create_time'] = time();
                self::updateAll(['create_time'=>time()], $where);
            }

            $date = '';
            if($query['start_time'] && $query['end_time']){
                $date = date('m-d',$query['start_time']) . '至'.date('m-d',$query['end_time']);;
            }
            else if($query['start_time']){
                $date = date('m-d',$query['start_time']) . '至今';
            }
            else if($query['end_time']){
                $date = '至'.date('m-d',$query['end_time']);
            }

            //库房
            $warehouse = Depot::findOneByAttributes(['id'=>$query['warehouse_id']],'id,name','name');

            //分类
            $className = '全部类目';
            if($query['prod_class_ids']){
                $classArr = ServiceClass::getName(explode(',',$query['prod_class_ids']));
                if($classArr){
                    $className = implode('、',$classArr);
                }
            }

            $result = [
                'oddNum'          => $query['odd_num'],
                'subject'         => $query['subject'],
                'typeDesc'        => DictEnum::getDesc('inventory_type',$query['type']),
                'date'            => $date,
                'warehouse'       => $warehouse,
                'classDesc'       => $className,
                'userName'        => Common::getUserName($query['create_user_id']),
                'createTime'      => date('Y-m-d H:i:s',$query['create_time']),
                'invalidUserName' => $query['invalid_user_id'] > 0? Common::getUserName($query['invalid_user_id']):'',
                'invalidTime'     => $query['invalid_time']?date('Y-m-d H:i:s',$query['invalid_time']):'',
                'remark'          => $query['remark'],
                'downloadUrl'     => $query['download_url']
            ];

            return $result;
        }

        return false;
    }

    /**
     * 盘点完毕后生成 盘盈，盘亏单
     * @param $id
     * @throws \Exception
     * @throws \Throwable
     * @author xi
     */
    public static function addStock($id)
    {
        $inventoryArr = Inventory::findOneByAttributes(['id'=>$id,'status'=>2]);
        if($inventoryArr)
        {
            $query = InventoryDetail::findAllByAttributes(['inventory_id'=>$id]);
            if($query)
            {
                $stockInArr = $stockOutArr = [];
                foreach ($query as $val)
                {
                    if($val['calc_num'] > 0){
                        $stockInArr []  = $val;
                    }
                    else if($val['calc_num'] < 0){
                        $stockOutArr []  = $val;
                    }
                }

                //入库
                if($stockInArr)
                {
                    $model = new StockInfo();
                    $model->direct_company_id = (new self())->directCompanyId;
                    $model->no              = CreateNoRule::getNo('RK');
                    $model->subject         = $inventoryArr['subject'];
                    $model->description     = $inventoryArr['remark'];
                    $model->type            = 1;
                    $model->is_rel          = 0;
                    $model->rel_id          = 0;
                    $model->apply_user_id   = Yii::$app->user->id;
                    $model->apply_user_name = Yii::$app->user->getIdentity()->username;
                    $model->create_user_id  = Yii::$app->user->id;
                    $model->create_time     = date('Y-m-d H:i:s');
                    $model->pick_user_id    = 0; //入库没有
                    $model->department_id   = 0;//入库没有
                    $model->status          = 1;
                    $model->stock_type      = 5; //入库 5 出库 4
                    $model->rel_type        = 0;
                    $model->audit_status    = 1;
                    if($model->save())
                    {
                        $rows = [];
                        foreach ($stockInArr as $val)
                        {
                            $rows [] = [
                                $model->direct_company_id,
                                $model->id,
                                $val['prod_id'],
                                $val['calc_num'],
                                $inventoryArr['warehouse_id'],
                                '','','','', date('Y-m-d H:i:s')
                            ];
                        }

                        $column = ['direct_company_id','parent_id','prod_id','prod_num','depot_id','prod_batch','prod_date','effective_date','serial_number','create_date'];
                        self::getDb()->createCommand()->batchInsert(StockDetail::tableName() , $column, $rows)->execute();
                    }
                }

                if($stockOutArr)
                {
                    $model = new StockInfo();
                    $model->direct_company_id = (new self())->directCompanyId;
                    $model->no              = CreateNoRule::getNo('CK');
                    $model->subject         = $inventoryArr['subject'];
                    $model->description     = $inventoryArr['remark'];
                    $model->type            = 2;
                    $model->is_rel          = 0;
                    $model->rel_id          = 0;
                    $model->apply_user_id   = Yii::$app->user->id;
                    $model->apply_user_name = Yii::$app->user->getIdentity()->username;
                    $model->create_user_id  = Yii::$app->user->id;
                    $model->create_time     = date('Y-m-d H:i:s');
                    $model->pick_user_id    = 0; //入库没有
                    $model->department_id   = 0;//入库没有
                    $model->status          = 1;
                    $model->stock_type      = 4; //入库 6 出库 4
                    $model->rel_type        = 0;
                    $model->audit_status    = 1;
                    if($model->save())
                    {
                        $rows = [];
                        foreach ($stockOutArr as $val)
                        {
                            $rows [] = [
                                $model->direct_company_id,
                                $model->id,
                                $val['prod_id'],
                                abs($val['calc_num']),
                                $inventoryArr['warehouse_id'],
                                '',
                                '',
                                '',
                                '',
                                date('Y-m-d H:i:s')
                            ];
                        }

                        $column = ['direct_company_id','parent_id','prod_id','prod_num','depot_id','prod_batch','prod_date','effective_date','serial_number','create_date'];
                        self::getDb()->createCommand()->batchInsert(StockDetail::tableName() , $column, $rows)->execute();
                    }
                }
            }
         }
    }

}
<?php
namespace webapp\models;

use Yii;

class TechnicianSignIn extends BaseModel
{


    public static function tableName()
    {
        return 'technician_sign_in';
    }


    /**
     * 打卡列表
     * @param $where
     * @param int $page
     * @param int $pageSize
     * @return array
     * @author xi
     * @date 2018-9-4
     */
    public static function getList ($where,$andWhere,$directCompanyId,$page=1,$pageSize=10)
    {
        $db = self::find();

        if($where){
            foreach ($where as $key=>$val)
            {
                if(is_array($val)){
                    if(!is_numeric($key)){
                        $db->andWhere([$val[0],$key,$val[1]]);
                    }else{
                        $db->andWhere([$val[1],$val[0],$val[2]]);
                    }
                }
                else {
                    $db->andWhere([$key=>$val]);
                }
            }
        }

        $db->from(self::tableName() . ' as a');
        $db->innerJoin(['`'.Technician::tableName().'` as b'] , 'a.technician_id = b.id');
        $db->innerJoin(['`'.Department::tableName().'` as c'], 'a.department_id = c.id');
        $db->leftJoin(['`'.GroupingView::tableName().'` as d'], 'a.technician_id = d.technician_id');

        $db->andWhere($andWhere);

        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            if($pageSize <=0){
                $pageSize = 10;
            }
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1){
                $page = 1;
            }

            $db->select("a.id,a.create_time,a.address,b.`name` AS `technician_name`,d.`name` AS `group_name`,c.`name` AS `department_name`,a.work_no,a.order_no,a.order_title,a.category,a.remark,a.pic_url,a.distance_deviation");

            $db->orderBy('c.`name`,	d.`name` ASC, b.`name`,	a.create_time DESC');
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();

            $categoryArr = [];
            $category = array_column($list,'category');
            if($category){
                $category = array_unique($category);
                $categoryArr = TechnicianSignInCategory::findAllByAttributes(['direct_company_id'=>$directCompanyId,'c_id'=>$category],'c_id,title','c_id');
            }

            foreach ($list as $key=>$val)
            {
                $list [$key]['date'] = date('Y-m-d',$val['create_time']);
                $list [$key]['time'] = date('H:i:s',$val['create_time']);

                if($val['group_name'] == ''){
                    $list[$key]['group_name'] = '无';
                }
                if($val['address'] == ''){
                    $list[$key]['address'] = '地址无法获取（技师未授权优服务APP使用定位功能）';
                }

                if($val['pic_url'] !=''){
                    $list[$key]['pic_url'] = explode(',',$val['pic_url']);
                }
                else {
                    $list[$key]['pic_url'] = [];
                }
                if($val['distance_deviation'] != ''){
                    $list[$key]['remark'] = '距离偏差:'.$val['distance_deviation'] . ';'. $val['remark'];
                }

                if(isset($categoryArr[$val['category']])){
                    $list[$key]['category'] = $categoryArr[$val['category']]['title'];
                }
                else {
                    $list[$key]['category'] = '考勤';
                }
            }

            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];

        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 获取技术信息
     * @param $groupId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getTechnicianByGroupingId($departmentId,$groupId)
    {
        $where = [
            'b.department_id' => $departmentId,
            'b.grouping_id'   => $groupId,
            'b.status'        => 1,
            'a.status'        => 1,
            'a.audit_status'  => 5
        ];
        $query = Technician::find()
            ->from(Technician::tableName() . ' as a')
            ->innerJoin(['`'.GroupingRelTechnician::tableName().'` as b'] , 'a.id = b.technician_id')
            ->where($where)
            ->select('a.id,a.name')
            ->asArray()
            ->all();

        return $query;
    }

    /**
     * 获取默认分组
     * @param $departmentId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getDefaultTechnician($departmentId)
    {
        $db = Technician::find();
        $db->from(Technician::tableName() . ' as a ');
        $db->leftJoin(['`' . GroupingRelTechnician::tableName() . '` as b'], 'a.id = b.technician_id and b.status=1');
        $db->where(['a.store_id' => $departmentId]);
        $db->andWhere("a.status=1 and a.audit_status = 5 and b.id is null");
        $db->select("a.id,a.name");

        $data = $db->asArray()->all();
        return $data;
    }

}
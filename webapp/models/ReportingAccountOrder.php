<?php
namespace webapp\models;

use Yii;

class ReportingAccountOrder extends BaseModel
{
    public static function tableName()
    {
        return 'reporting_account_order';
    }
    public static function getList($map,$page,$pageSize)
    {
        $db = self::find();
        if (!empty($map)) {
            foreach ($map as $key=>$val){
                if(is_array($val)){
                    $db->andWhere([$val[0],$val[1],$val[2]]);
                }else{
                    $db->where([$key=>$val]);
                }
            }
        }
        $db->groupBy('account_id');
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum / $pageSize);
            if ($page < 1) {
                $page = 1;
            } else if ($page > $totalPage) {
                $page = $totalPage;
            }
            $db->select('account_name,account_area,sum(sale_order_num) as order_num');
            $db->offset(($page - 1) * $pageSize);
            $db->limit($pageSize);
            $db->orderBy('sale_order_num desc');
            $list = $db->asArray()->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        } else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
}


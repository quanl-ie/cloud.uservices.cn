<?php
namespace webapp\models;

use Yii;

class StoresSkill extends BaseModel
{

    public static function tableName()
    {
        return 'stores_skill';
    }

    public static function getOne($where) {
        return self::find()->where($where)->asArray()->one();
    }




    public static function getList ($storeId) { //manufactorId
        $data = self::find()
            ->where(['stores_id'=>$storeId])
            ->asArray()
            ->all();
        return $data;
    }
    
}

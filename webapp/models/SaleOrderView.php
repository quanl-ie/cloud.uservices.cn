<?php
namespace webapp\models;

use common\helpers\Helper;
use common\models\ServiceBrand;
use common\models\ServiceClass;
use Yii;
use yii\data\Pagination;
use webapp\models\ServiceGoods;

class  SaleOrderView extends SaleOrder
{

    public static function tableName()
    {
        return 'sale_order_view';
    }
    public function attributeLabels()
    {
        return [
            'scope_img'       => '凭证：',
        ];
    }
    public static function getList($where)
    {
        return self::find()->filterWhere($where)->asArray()->all();
    }

    public static function getLists($param)
    {
        $db = self::find()
            ->where(['status'=>1,'contract_order'=>0,'wechat_show'=>1]);

        if(!empty($param['account_id'])){
              $db->andwhere(['account_id'=>$param['account_id']]);
        }
        if(!empty($param['account_name'])){
              $db->andwhere(['like','account_name',$param['account_name']]);
        }
        if(!empty($param['brand_id'])){
              $db->andwhere(['brand_id'=>$param['brand_id']]);
        }
        if(!empty($param['start_time'])){
              $db->andwhere(['>=','buy_time',$param['start_time']]);
        }
        if(!empty($param['end_time'])){
              $db->andwhere(['<','buy_time',$param['end_time']]);
        }
        if(!empty($param['intention_type'])){
          $db->andwhere(['intention_type' => $param['intention_type']]);
        }
        $pageSize = $param['pageSize'];
        if(!isset($param['currentPage'])){
              $page = 0;
        }else{
              $page = $param['currentPage']-1;
        }
        unset($param['currentPage']);
        
        $count =$db->count();
        $totalPage = ceil($count/$pageSize);
        $start = $page*$pageSize;
        $order =$db->offset($start)
              ->orderBy('create_time DESC')
              ->limit($pageSize)
              ->asArray()->all();
        $data['list'] = $order;
        $data['totalPage'] = $totalPage;
        $data['page'] = $page+1;
        return $data;
    }

    public static function primaryKey()
    {
        return ['id1'];
    }
}
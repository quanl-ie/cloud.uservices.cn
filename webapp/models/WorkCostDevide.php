<?php
namespace webapp\models;

use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;
use webapp\models\Technician;

class WorkCostDevide extends BaseModel
{
    public static function getDb() {
        return Yii::$app->order_db;
    }

    
    public static function tableName()
    {
        return 'work_cost_divide';
    }

    /**
     * 获取结算单
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getOne($where = [])
    {
        return self::find()->where(['src_type'=>self::SRC_FWS,'src_id'=>BaseLogic::getManufactorId()])->andWhere($where)->one();
    }

    /**
     * 获取结算单列表
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getList($param = [])
    {
        $db = self::find();
        $pageSize = $param['pageSize'];
        if(!isset($param['currentPage'])){
            $page = 0;
        }else{
            $page = $param['currentPage']-1;
        }

        unset($param['currentPage']);
        unset($param['pageSize']);


        $db->from(self::tableName() . ' as a');
        $db->select('a.work_no,a.order_no,expect_amount,a.divide_amount,a.cost_real_amount,d.brand_name,d.type_name,d.class_name');
        $db->innerJoin(['work_order' . ' as b'],'a.order_no = b.order_no');
        $db->innerJoin(['work_order_detail as c'],'a.order_no=c.order_no');
        $db->innerJoin(['manufactor.sale_order as d'],'c.sale_order_id=d.id');
       // $db->innerJoin([technician::tableName() . ' as b'],'a.tech_id = b.id');
        if(!empty($param['start_time'])){
            $db->andWhere(['>=','process_time',$param['start_time']]);
            unset($param['start_time']);
        }
        if(!empty($param['end_time'])){
            $db->andWhere(['<=','process_time',$param['end_time']]);
            unset($param['end_time']);
        }
        foreach($param as $k=>$v){
            if(!empty($v)){
                $db->andwhere([$k=>$v]);
            }
        }
        $count = $db->count();
        $totalPage = ceil($count/$pageSize);
        $start = $page*$pageSize;
        $limit = $pageSize;
        $list =$db->offset($start)
            ->limit($pageSize)
            ->asArray()
            ->all();
        return [
            'page'       => $page+1,
            'totalCount' => $count,
            'totalPage'  => $totalPage,
            'list'       => $list
        ];
    }

    
}
<?php
namespace webapp\models;

use Symfony\Component\EventDispatcher\Tests\Service;
use Yii;
use yii\db\ActiveRecord;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use webapp\logic\BaseLogic;
use webapp\models\Manufactor;

/**
 * @desc 合作服务商模型
 * @author chengjuanjuan <chengjuanjuan@c-ntek.com>
 * @date 2017-12-19
 **/

class  Cooporation extends BaseModel
{

    public static function tableName()
    {
        return 'cooperation_service_provider';
    }

    public static function show() {
        return [1 => '厂家直销', 2 => '代理商'];
    }



    public function attributeLabels()
    {
        return array(
            'src_type'            =>  '来源',
            'service_provider_id' =>    '服务商Id',
            'service_provider_name' => '服务商名称',
            'prod_name'           => '产品名称',
            'address'            => '序列号',
            'warranty'            => '保质期',
        );
    }
    //状态
    const STRTUS_ARRAY = [1=>'待确认',2=>'合作中',3=>'被拒绝'];

    /**
     * 分页显示
     * @author lxq <lxq@c-ntek.com>
     * @return mixed
     */
    public static function getList($srcId,$page,$pageSize)
    {
        $db = self::find();
        $db->where(['src_type'=> self::SRC_SJ,'service_provider_id'=> BaseLogic::getManufactorId()]);
        $db->andFilterWhere(['src_id'=>$srcId]);
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['create_time'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();            
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }


    /**
     * 添加
     * @author chengjuanjuan <chengjuanjuan@c-ntek.com>
     * @date 2017-12-15 //同时往商品表里添加数据
     */
    public static function add($data)
    {
        $model = new self();
        foreach($data as $k=>$v){
            $model->$k=$v;
        }
        $res = $model->save();
        if($res){
            return $model->id;
        }
    }


    public static function getOne($where)
    {
        return self::find()->where($where)->one();
    }

    public static function changeData($id,$status){
        $oneInfo = self::getOne(['id'=>$id]);
        
        $oneInfo->status      = $status;
        $oneInfo->update_time = time();
        $res = $oneInfo->save();
        if($res){
            return $oneInfo->id;
        }
    }


    /**
     * 获取对应服务商信息
     * @author  liwennyong<liwenyong@uservices.cn>
     * @date    2017-12-20
     * @return  array
     */
    public static function getServiceProvider($src_id,$saleOrderId)
    {
        $providerArr = [];

        $saleOrderArr = SaleOrder::findOneByAttributes(['id'=>$saleOrderId],'class_id');
        if($saleOrderArr)
        {
            $secondClass = ServiceClass::findOne(['id'=>$saleOrderArr['class_id']]);
            if($secondClass)
            {
                $skillArr = StoresSkill::findAllByAttributes(['class_id'=>$secondClass->pid],'stores_id');
                if($skillArr)
                {
                    $serviceProviderIds = array_column($skillArr,'stores_id');
                    $data = self::find()->where(['src_id' => $src_id, 'service_provider_id'=>$serviceProviderIds, 'status' => 2,'src_type'=>BaseModel::SRC_FWS])->asArray()->all();
                    if($data)
                    {
                        foreach ($data as $v) {
                            $providerArr[$v['service_provider_id']] = $v['service_provider_name'];
                        }
                    }

                }
            }
        }

        return $providerArr;
    }

    public static function getListByParam($param){
        $model =  self::find();
        $model->from(self::tableName() . ' as a');
        $model->where(['service_provider_id'=>BaseLogic::getManufactorId(),'a.status'=>2]);
        $model->innerJoin([Manufactor::tableName().' as b'],'a.src_id=b.id');
        $model->select(['b.id','b.company']);
        //echo $model->createCommand()->getRawSql();exit;
        return $model->asArray()->all();
    }

    public static function getCooporationList(){
        $model =  self::find();
        $model->from(self::tableName() . ' as a');
        $model->where(['service_provider_id'=>BaseLogic::getManufactorId(),'a.status'=>2,'a.src_type'=>BaseModel::SRC_SJ]);
        $model->innerJoin([Manufactor::tableName().' as b'],'a.src_id=b.id');
        $model->select(['b.id','b.company']);
        //echo $model->createCommand()->getRawSql();exit;
        return $model->asArray()->all();
    }

}
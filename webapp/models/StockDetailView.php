<?php
namespace webapp\models;

use common\helpers\Helper;
use common\models\ServiceBrand;
use common\models\ServiceClass;
use Yii;
use yii\data\Pagination;
use webapp\models\ServiceGoods;

class  StockDetailView extends BaseModel
{

    public static function tableName()
    {
        return 'stock_detail_view';
    }
}
<?php

namespace webapp\models;

use Yii;
use common\components\Upload;
use common\helpers\Helper;
use webapp\models\TechnicianMode;
class Technician extends BaseModel
{
    public static function tableName()
    {
        return 'technician';
    }
    public static function getList ($maps,$where,$page,$pageSize,$select = "id,name,mobile,store_id,status,technician_number")
    {
        $db = self::find();
        $db->where($maps);


        if (!empty($where)) {
            foreach ($where as $val){
                $db->andWhere($val);
            }
        }

        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->select($select);
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['id'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    //技师审核列表查询
    public static function getEList ($maps,$where,$page,$pageSize)
    {
        //print_r($maps);exit;
        $db = self::find();
        $db->where($maps);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['audit_time'=> SORT_DESC,'updated_at'=>SORT_DESC]);
            $db->limit($pageSize);
            //print_r($db->createCommand()->getRawSql());die;
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    public static function getOne($where) {
        return self::find()->where($where)->asArray()->one();
    }
    //通过技师姓名 、电话  获取技师id
    public static function getTechnicianId($where) 
    {
        return self::find()
                ->select('id')
                ->where(['audit_status'=>5,'status'=>1])
                ->andWhere($where)
                ->asArray()
                ->all();
    }
    //重置技师密码
    public static function setPwd($id,$pwd){
        $model = self::findOne($id);
        if(!empty($model)){
            $model->password = $pwd;
            $model->password_hash = $pwd;
            $model->updated_at = time();
            $model->save(false);
            return true;
        }
        return false;
    }

    public static function add($data,$skill,$t_mode)
    {
        $model = new self();
        //处理数据
        foreach ($data as $key => $val) {
            $model->$key = $val;
        }


        $upload = new Upload();
        if(isset($_FILES['icon']) && $_FILES['icon']['size']>0)
        {
            if($_FILES['icon']['size'] > 1048576){
                return ['success' => false,'message' => '','code'=> '20008','data'=> []];
            }
            $result = $upload->upImage('service_provider','icon');
            if(isset($result['code']) && $result['code'] == 0){
                $model->icon = $result['url'];
            }else{
                return ['success' => false,'message' => '','code'=> '20010','data'=> []];
            }
        }
        if(isset($_FILES['identity_front']) && $_FILES['identity_front']['size']>0)
        {
            if($_FILES['identity_front']['size'] > 2097152){
                return ['success' => false,'message' => '','code'=> '20009','data'=> []];
            }
            $result = $upload->upImage('service_provider','identity_front');
            if(isset($result['code']) && $result['code'] == 0){
                $model->identity_front = $result['url'];
            }
        }
        if(isset($_FILES['identity_behind']) && $_FILES['identity_behind']['size']>0)
        {
            if($_FILES['identity_behind']['size'] > 2097152){
                return ['success' => false,'message' => '','code'=> '20009','data'=> []];
            }
            $result = $upload->upImage('service_provider','identity_behind');
            if(isset($result['code']) && $result['code'] == 0){
                $model->identity_behind = $result['url'];
            }
        }
        if(isset($_FILES['identity_hold']) && $_FILES['identity_hold']['size']>0)
        {
            if($_FILES['identity_hold']['size'] > 2097152){
                return ['success' => false,'message' => '','code'=> '20009','data'=> []];
            }
            $result = $upload->upImage('service_provider','identity_hold');
            if(isset($result['code']) && $result['code'] == 0){
                $model->identity_hold = $result['url'];
            }
        }

        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //技师表添加数据
            $model->save(false);
            //添加技师mode表数据
            $t_mode['technician_id'] = $model->id;
            $TechnicianMode = TechnicianMode::add($t_mode);
            if(!empty($skill)){
                //处理需要添加的技师技能数据
                $skill = explode(",",$skill);
                foreach ($skill as $key => $val) {
                    $technicianSkill[$key]['technician_id'] = $model->id;
                    $technicianSkill[$key]['class_id'] = substr($val,strpos($val,'_')+1);
                    $technicianSkill[$key]['service_id'] = substr($val,0,strpos($val, '_'));
                    $technicianSkill[$key]['created_at'] = $technicianSkill[$key]['updated_at'] =time();
                }
                $field = ['technician_id','class_id','service_id','updated_at','created_at'];
                $totalnum = Yii::$app->db->createCommand()
                    ->batchInsert(TechnicianSkill::tableName(),$field,$technicianSkill)
                    ->execute();
            }
            //添加技师技址表数据
            //$technicianSkill->save(false);
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

    }
    public static function edit($data,$skill){
        //查询技师数据
        $model = self::findOne(['id'=>$data['id']]);
        //print_r($model);exit;
        //处理数据
        foreach ($data as $key => $val) {
            $model->$key = $val;
        }
        $upload = new Upload();
        if(isset($_FILES['icon']) && $_FILES['icon']['size']>0)
        {
            $result = $upload->upImage('service_provider','icon');
            if(isset($result['code']) && $result['code'] == 0){
                $model->icon = $result['url'];
            }
        }
        if(isset($_FILES['identity_front']) && $_FILES['identity_front']['size']>0)
        {
            $result = $upload->upImage('service_provider','identity_front');
            if(isset($result['code']) && $result['code'] == 0){
                $model->identity_front = $result['url'];
            }
        }
        if(isset($_FILES['identity_behind']) && $_FILES['identity_behind']['size']>0)
        {
            $result = $upload->upImage('service_provider','identity_behind');
            if(isset($result['code']) && $result['code'] == 0){
                $model->identity_behind = $result['url'];
            }
        }
        if(isset($_FILES['identity_hold']) && $_FILES['identity_hold']['size']>0)
        {
            $result = $upload->upImage('service_provider','identity_hold');
            if(isset($result['code']) && $result['code'] == 0){
                $model->identity_hold = $result['url'];
            }
        }

        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //技师表添加数据
            $model->save(false);
            if(!empty($skill)){
                //处理需要添加的技师技能数据
                $skill = explode(",",$skill);
                foreach ($skill as $key => $val) {
                    $technicianSkill[$key]['technician_id'] = $model->id;
                    $technicianSkill[$key]['class_id'] = substr($val,strpos($val,'_')+1);
                    $technicianSkill[$key]['service_id'] = substr($val,0,strpos($val, '_'));
                    $technicianSkill[$key]['created_at'] = $technicianSkill[$key]['updated_at'] =time();
                }
                $delSkill = TechnicianSkill::deleteAll(['technician_id'=>$data['id']]);
                $field = ['technician_id','class_id','service_id','updated_at','created_at'];
                $totalnum = Yii::$app->db->createCommand()
                    ->batchInsert(TechnicianSkill::tableName(),$field,$technicianSkill)
                    ->execute();
            }
            //添加技师技址表数据
            //$technicianSkill->save(false);
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

    }
    //设置技师审核状态
    public static function setStatus($id,$status,$reason){
        $model = self::findOne($id);
        if(!empty($model)){
            $model->audit_status = $status;
            $model->reason = $reason;
            $model->updated_at = time();
            $model->save(false);
            return true;
        }
        return false;
    }
    //查询技师服务状态
    public static function getTechnicianStatus($TechnicianIds=[]){
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/technician-service-status";
        $postData = [
            'technician_ids'     => $TechnicianIds
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        $list = [];
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            $list = $jsonArr['data'];
            if( $list )
            {
                return $list;
            }
        }
        return $list;
    }
    //查询技师数据是否存在
    public static function verifyData($map,$where = '')
    {
        if (!empty($where)) {
            $res = self::find()->where($map)->andWhere($where)->one();
        }else{
            $res = self::find()->where($map)->one();
        }
        if (!empty($res)) {
            return false;
        }
        return true;
    }

    //查询技师是否禁用
    public static function getTecStatus($id) {
        $data = self::findOne(['id' => $id]);
        return isset($data->status) ? $data->status : 2;
    }
    
    public static function getTechList($maps,$where,$page,$pageSize)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->leftJoin([GroupingRelTechnician::tableName() . ' as b'],' a.id = b.technician_id');
        $db->where($maps);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
        
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->select('a.id,a.name,a.mobile,a.created_at as ac,b.grouping_id,b.job_title,b.status,b.create_time as bc');
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['bc'=> SORT_DESC,'ac'=>SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }

    /**
     * 查出技师未完成工单数
     * @param $technicianId
     * @return int
     */
    public static function technicianNoFinshWork($technicianId)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/technician-no-finsh-work";
        $postData = [
            'technician_id' => $technicianId
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            return $jsonArr['data'];
        }
        return 0;
    }

    /**
     * 查一下手机号是否重复
     * @param $mobile
     * @return bool
     * @author xi
     */
    public static function hasRepeat($mobile,$id)
    {
        $directCompanyId = (new self())->directCompanyId;

        $departmentIds = [$directCompanyId];
        $departmentRes = Department::findAllByAttributes(['direct_company_id'=>$directCompanyId],'id');
        if($departmentRes){
            $departmentIds = array_merge($departmentIds,array_column($departmentRes,'id'));
        }

        if($id>0){
            $where = "store_id in(".implode(',',$departmentIds).") and mobile = $mobile and id <> $id";
        }
        else {
            $where = [
                'store_id' => $departmentIds,
                'mobile'   => $mobile
            ];
        }
        $query = self::findOneByAttributes($where,'id');

        return $query?true:false;
    }
}

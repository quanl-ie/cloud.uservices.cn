<?php
namespace webapp\models;

use Yii;
use common\helpers\Helper;
use common\models\Region as R;

class AdminUser extends \common\models\User
{
    public $vcode;

    public $verify_pwd;
    
    public $password;

    public static function tableName()
    {
        return 'user';
    }
    //规则
    public function rules()
    {
        return [
            [['password'],'string','min'=>6,'max'=>15],
            [['verify_pwd'],'string','min'=>6,'max'=>15],
            ['mobile','match','pattern'=>'/^[1][3,4,5,6,7,8,9][0-9]{9}$/','message'=>'手机号格式不正确'],
            [['username','role_ids','mobile','department_id'],'required','on'=>['adminUser','updateAdminUser']],
            [['password'],'required','on'=>['adminUser']],
            [['nick_name','vcode'],'string','max'=>48],
            [['job_title','email','username'],'string','max'=>255],
            [['avatar'],'string','max'=>1000],
            [['sex'],'number'],
            [['username'],'required'],
            ['email','match','pattern'=>'/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/','message'=>'邮箱格式不正确'],


            //机构添加地方用
            [['server_city_id', 'last_login_time', 'created_at', 'updated_at'], 'integer','on'=>'organizationAdd'],
            [['username','department_id','mobile', 'password_hash', 'created_at', 'updated_at'], 'required','on'=>'organizationAdd'],
            [['department_pids', 'avatar'], 'string', 'max' => 1000,'on'=>['organizationAdd']],
            [['company', 'username', 'nick_name', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'role_ids', 'job_title'], 'string', 'max' => 255,'on'=>['organizationAdd']],
            [['mobile'], 'string', 'max' => 11,'on'=>['organizationAdd']],
            [['status', 'del_status', 'identity', 'sex'], 'number', 'on'=>['organizationAdd']],
            [['login_ip'], 'string', 'max' => 20,'on'=>['organizationAdd']],
            ['email','email','on'=>['organizationAdd']]
        ];
    }
    
    public function attributeLabels()
    {
        return array(
            'department_id' => '上级机构',
            'company'        => '企业名称',
            'password'       => '初始密码',
            'verify_pwd'    => '确认密码',
            'mobile'        => '手机号',
            'username'      => '用户名',
            'nick_name'     => '昵称',
            'role_ids'       => '担任角色',
            'server_city_id'=> '工作城市',
            'email'          =>  '邮箱',
            'job_title'      =>  '职位'
        );
    }
    
    //获取单条信息
    public static function getOne($where)
    {
        return self::find()->where($where)->one();
    }

    /**
     * 取列表带分页
     * @param array $where
     * @param string $order
     * @param int $page
     * @param int $pageSize
     * @return array
     * @author liwenyong<liwenyong@c-ntek.com>
     * @date 2017-08-17
     */
    public static function getList($where =array() ,$order='',$page=1,$pageSize=10)
    {
        $db = self::find();
        if ($where) {
            if(!is_array($where)){
                $db->where($where);
            }else{
                foreach ($where as $key=>$val)
                {
                    if (is_array($val)) {
                        $db->andWhere([$val[0],$val[1],$val[2]]);
                    } else {
                        $db->where($val);
                    }
                }
            }

        }
        //总数
        $totalNum = $db->count();
        //echo $db->createCommand()->rawSql;exit;
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if ($page<1) {
                $page = 1;
            } else if ($page>$totalPage) {
                $page = $totalPage;
            }

            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            if ($order!='') {
                $db->orderBy($order);
            }
            $query = $db->asArray()->all();

            $role_ids = [];
            foreach ($query  as $val)
            {
                if ($val['role_ids']!='' && $val['role_ids']!='0') {
                    $role_ids = array_merge(explode(',', $val['role_ids']),$role_ids);
                }
            }

            $roleArr = [];
            if ($role_ids) {
                $role_ids = array_unique($role_ids);
                $roleRes = Role::find()->where(['id'=>$role_ids])->select('id,name,is_init')->asArray()->all();
                foreach ($roleRes as $val){
                    $roleArr[$val['id']] = $val['is_init'] == 1?'创建人':$val['name'];
                }
            }

            $list = [];
            foreach ($query as $key=>$val)
            {
                $role_name = [];
                if ($val['role_ids']!='' && $val['role_ids']!='0') {
                    foreach (explode(',', $val['role_ids']) as $v){
                        if (isset($roleArr[$v])) {
                            $role_name[] = $roleArr[$v];
                        }
                    }
                }
                if ($role_name) {
                    $val['role_name'] = implode('、', $role_name);
                } else {
                    $val['role_name'] = '';
                }
                $val['service_city_name'] = self::getCityName($val['server_city_id']);
                $list[] = $val;
            }

            return array(
                'totalNum'  => $totalNum,
                'totalPage' => $totalPage,
                'page'      => $page,
                'list'      => $list
            );
        } else {
            return array(
                'totalNum'  => 0,
                'totalPage' => 0,
                'page'      => $page,
                'list'      => array()
            );
        }
    }

    /** 
    * 获取维护人信息
    * @param $adminIds
    * @return array
    * @author liwenyong<liwenyong@c-ntek.com>
    * @date 2017-08-17
    */
    public static function adminInfo($adminIds){ 
        $adminIds = array_unique($adminIds);
        $adminRes = self::find()->where(['id'=>$adminIds])->select('id,username,mobile')->asArray()->all();

        $adminArr = [];
        foreach ($adminRes as $val){
            if ($val['username']) { 
                $adminArr[$val['id']] = $val['username'];
            } else { 
                $adminArr[$val['id']] = $val['mobile'];
            } 
        }
        return $adminArr;
    }


    //获取商家
    public static function getListInfo ($name) {
        $data = self::find()
            ->where(['like', 'username', $name])
            ->andWhere(['status' => 1])
            ->select(['id','company as name'])
            ->asArray()->all();
        return $data;
    }
	
	/*public static function findByUsername($username,$srcType)
    {
        return static::findOne(['mobile' => $username, 'src_type'=>$srcType, 'status' => self::STATUS_ACTIVE]);
    }*/
    public static function findByUsername($username,$department_id)
    {
        return static::findOne(['mobile' => $username,'department_id'=>$department_id,'status' => self::STATUS_ACTIVE]);
    }
    /**
     * 获取城市名称
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $cityIds
     * @return type
     */
    public static function getCityName($cityIds)
    {
        $name = '';
        if ($cityIds) {
            $data = R::getCityName(['region_id'=>$cityIds]);
            foreach ($data as $key => $val) {
                $name = $val['region_name'];
            }
        }
        return $name;
    }
    /**
     * 获取多条数据
     * User: sxz
     * Date: 2018/6/5
     * Time: 10:00
     * @param array $where
     * @return array|ActiveRecord[]
     */
    public static function getAllList($where = [])
    {
        return self::find()->where($where)->asArray()->all();
    }
    /**
     * 根据id获取用户信息
     * @anthor liquan
     * @date 2018-6-7
     * @param Int user_id
     * @return array
     */
    public static function getInfoById($user_id)
    {
        return static::findOne(['id' => $user_id]);
	}
	/**
     * 验证此手机号码是否注册多测系统账号
     * User: sxz
     * Date: 2018/6/5
     * Time: 10:00
     * @param array $where
     * @return array|ActiveRecord[]
     */
    public static function checkDepartment($mobile,$source=false)
    {
        $data = self::getAllList(['mobile'=>$mobile,'del_status'=>1]);
        if(count($data) >1 )
        {
            $department_ids = array_column($data,'department_id');
            $list = Department::getAllList($department_ids,$source);
            return $list;
        }
        return false;
    }
}


<?php

namespace webapp\models;

use Yii;
class TechnicianMode extends BaseModel
{
    public static function tableName()
    {
        return 'technician_mode';
    }
    public static function getTechnicianIds ($map=[],$where=[])
    {
        $ids = [];
        $db = self::find();
        $db->where($map);
        //$db->select();
        //判断where
        if (!empty($where)) {
            foreach ($where as $key=>$val){
                $db->andFilterWhere($val);
            }
        }
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            $data = $db->asArray()->all();
            if($data){
                $ids = array_unique(array_column($data,'technician_id'));
            }
            return $ids;
        }
        return $ids;
    }
    public static function getTeIds ($map=[])
    {
        //print_r($map);exit;
        $ids = [];
        $db = self::find();
        //$db->where($map);
        //$db->select();
        //判断where
        if (!empty($map)) {
            foreach ($map as $key=>$val){
                if(is_array($val)){
                    $db->andWhere([$val[0],$val[1],$val[2]]);
                }else{
                    $db->where([$key=>$val]);
                }
                //$db->andFilterWhere($val);
            }
        }
        print_r($db->asArray()->all());exit;
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            $data = $db->asArray()->all();
            if($data){
                $ids = array_unique(array_column($data,'technician_id'));
            }
            return $ids;
        }
        return $ids;
    }
    public static function getOne($where) {
        return self::find()->where($where)->asArray()->one();
    }
    public static function getinfos($where){
        return self::find()->where($where)->asArray()->one();
    }
    //添加
    public static function add($data)
    {
        //实例化模型
        $technicianMode = new self();
        //处理数据
        foreach ($data as $key => $val) {
            $technicianMode->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //添加客户联系地址表数据
            $technicianMode->save(false);
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    //编辑
    public static function edit($data)
    {
        //查询客户表数据
        $where['technician_id'] = $data['technician_id'];
        $technicianMode = self::findOne($where);
        //处理客户表数据
        foreach ($data as $key => $val) {
            $technicianMode->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //修改技师位置表数据
            $technicianMode->save(false);
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return false;
    }
    //打点
    public static function PositionDecode($type,$array,$key='BeiJing_test2'){
        //$url = Yii::$app->params['PositionURl'];
        $url = Yii::$app->params['positionsystem'].'/index/index';
        $array['distence'] = '100';//默认10km
        $array['unit'] ='km';
        $data = ['key'=>$key,'param'=>json_encode($array),'type'=>$type];
        $res = Helper::curlPost($url,$data);
        if($res){
            return  $res = json_decode($res,true);
        }
        return $res;
    }
}

<?php
namespace webapp\models;

use Yii;

class Stores extends BaseModel
{
    public static function getDb() 
    {
        return Yii::$app->service_provider_db;
    }
    

    public static function tableName()
    {
        return 'stores';
    }

    public static function getOne($where) {
        return self::find()->where($where)->asArray()->one();
    }


    public static function getListInfo ($name, $src_id=1) {
        $data = self::find()
            ->where(['like', 'store_name', $name])
            ->andWhere(['is_status' => 2])
            ->select(['id','store_name','service_class_id','realname','tel','brand_id'])
            ->asArray()->all();
        return $data;
    }
    
}

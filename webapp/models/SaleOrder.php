<?php
namespace webapp\models;

use common\helpers\Helper;
use common\models\ServiceBrand;
use common\models\ServiceClass;
use webapp\logic\ProductLogic;
use Yii;
use yii\data\Pagination;
use webapp\models\ServiceGoods;
use common\models\Product;

class  SaleOrder extends BaseModel
{

    public static function tableName()
    {
        return 'sale_order';
    }

    public static function show() {
        return [1 => '厂家直销', 2 => '代理商'];
    }



    public function attributeLabels()
    {
        return array(
            'account_name'   => '客户名称',
            'account_address'      => '来源',
            'prod_name'     => '产品名称',
            'serial_number' => '企业logo',
            'address'       => '序列号',
            'warranty'      => '保质期',
        );
    }

    /**
     * 获取质保单位
     * @param $key
     * @return mixed|string
     * @author xi
     */
    public static function getWarrantyType($key)
    {
        $data = [
            1 => '日',
            2 => '月',
            3 => '年'
        ];

        return isset($data[$key]) ? $data[$key] : '';
    }

    /**
     * 获取质保期
     * @param $num
     * @param $unit
     * @return string
     * @author xi
     */
    public static function getWarrant($num,$unit)
    {
        if($num > 0){
            return $num . self::getWarrantyType($unit);
        }

        return '';
    }

    /**
     * 分页显示
     * @author lxq <lxq@c-ntek.com>
     * @date 2017-9-4 11:00
     * @return mixed
     */
    public static function getList($param)
    {

        $db = self::find()
            ->where(['status'=>1]);
        if(!empty($param['account_id'])){
            $db->andwhere(['account_id'=>$param['account_id']]);
        }

       if(!empty($param['account_name'])){
           $db->andwhere(['like','account_name',$param['account_name']]);
       }
        if(!empty($param['brand_id'])){
            $db->andwhere(['brand_id'=>$param['brand_id']]);
        }
       if(!empty($param['start_time'])){
           $db->andwhere(['>=','buy_time',$param['start_time']]);
       }
        if(!empty($param['end_time'])){
            $db->andwhere(['<','buy_time',$param['end_time']]);
        }
        $pageSize = $param['pageSize'];
        if(!isset($param['currentPage'])){
            $page = 0;
        }else{
            $page = $param['currentPage']-1;
        }
        unset($param['currentPage']);

        $count =$db
            ->count();
        $totalPage = ceil($count/$pageSize);
        $start = $page*$pageSize;
        $limit = $pageSize;
        $order =$db->offset($start)
            ->orderBy('create_time DESC')
            ->limit($pageSize)
            ->all();
        $data['list'] = $order;
        $data['totalPage'] = $totalPage;
        $data['page'] = $page+1;
        return $data;
    }


    /**
     * 添加
     * @author chengjuanjuan <chengjuanjuan@c-ntek.com>
     * @date 2017-12-15
     */
    public static function add($saleData)
    {
    
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
    
            $model = new self();
            foreach($saleData as $k=>$v){
                $model->$k=$v;
            }
            
            if(!$model->save(false))
            {
                $transaction->rollBack();
                return false;
            }
            
            $transaction->commit();
            return $model->id;
        }
        catch(\Exception $e)
        {
            $transaction->rollBack();
            throw $e;
        }

    }


    public static function  edit ($saleData)
    {
        //查询售后产品数据
        $OneInfo = self::find()->where(['id'=>$saleData['id']])->one();
        foreach($saleData as $k=>$v){
            $OneInfo->$k=$v;
        }
        $res = $OneInfo->save();

        return $res;
            
    }



    public static function getOne($where)
    {
        return self::find()->where($where)->one();
    }


    public static function instance ($refresh = false)
    {
        // TODO: Implement instance() method.
    }


    /**
     * 产品下拉列表
     * @return array
     * @author xi
     * @date 2017-12-19
     */
    public static function getDownList($srcType,$srcId)
    {
        $result = [];
        $query = self::findAllByAttributes(['status'=>1,'src_type'=>$srcType,'src_id'=>$srcId],'id,prod_name');
        foreach ($query as $val)
        {
            $result[$val['id']] = $val['prod_name'];
        }
        return $result;
    }

    /**
     * 客户对应产品
     * @return array
     * @author liwenyong
     * @date   2017-12-20
     */
    public static function getServiceBrand($account_id,$type_id=0,$intent=0)
    {
        $result = [];
        $where = '';
        if($type_id == 2){
            $where .= "intention_type = 1 and status = 1 and contract_order = 0 and account_id =".intval($account_id);
        }else{
            if($intent>0){
                $where .= "intention_type =".$intent." and status = 1 and contract_order = 0 and account_id =".intval($account_id);
            }else{
                $where .= "status = 1 and contract_order = 0 and account_id =".intval($account_id);
            }
        }

        $select = "id,class_id,prod_series,class_name,brand_id,brand_name,code,prod_id,prod_name,intention_type,unit_id,serial_number";
        $query = SaleOrderView::find()
            ->select($select)
            ->where($where)
            ->orderBy('intention_type desc,id desc')
            ->asArray()
            ->all();

        $prod_unit = ProductLogic::getUnit();
        foreach ($query as $key=>$val)
        {
            if($val['intention_type'] == 1){
                $intention = "客户产品";
            }else{
                $intention = "意向产品";
            }
            $tmp = [
                //$val['code'],
                $val['prod_series'],
                $val['prod_name'],
                $val['brand_name'],
                $val['serial_number']
            ];
            $result[$key]['id']     = $val['id'];
            $result[$key]['value']  = implode('/', array_filter($tmp))." (".$intention.")";
            $result[$key]['intent'] = $val['intention_type'];
            $result[$key]['unit']   = isset($prod_unit[$val['unit_id']])?$prod_unit[$val['unit_id']]:'';
        }

        return $result;
    }
    /**
     * 客户名称带出产品
     * @return array
     * @author chengjuanjuan
     * @date   2017-12-22
     */
    public static function getProductByAccount($accountName){
        $query = self::find()
            ->where(['src_type'=>BaseModel::SRC_FWS])
            ->andWhere(['like','account_name',$accountName])
            ->orderBy('update_time desc')
            ->all();
        foreach ($query as $val)
        {
            $info['id'] = $val['id'];
            $info['prod_name'] = $val['prod_name'];
            $info['account_name'] = $val['account_name'];
            $result[] = $info;
        }
        return $result;
    }




    /**
     * 产品名称称带出该商家下的产品
     * @return array
     * @author chengjuanjuan
     * @date   2017-12-22
     */
    public static function getProductByName($producName,$srcId){
        $query = self::find()
            ->where(['src_type'=>BaseModel::SRC_FWS,'src_id'=>$srcId])
            ->andWhere(['like','prod_name',$producName])
            ->orderBy('update_time desc')
            ->limit(10)
            ->all();
        return $query;
    }

    /**
    *去sale-order里拿品牌id
     **/
    public static function findBrand($query){
        $result = self::find()
                ->where($query)
                ->groupBy('brand_id')
                ->all();
        return $result;
    }


    public static function changeData($data){
        $oneInfo = self::getOne(['id'=>$data['id']]);
        $model = new self();
        foreach($data as $k=>$v){
            $oneInfo->$k=$v;
        }
        $res = $oneInfo->save();
        if($res){
            return $oneInfo->id;
        }
    }
    //根据客户查询产品数量
    public static function getSaleOrderNum($accountIds) 
    {
        $query = self::find()
            ->where(['status'=>1])
            ->andWhere(['in','account_id',$accountIds])
            ->select('account_id,count(1) as count')
            ->groupBy('account_id')
            ->asArray()
            ->all();
        if($query){
            return array_column($query,'count','account_id');
        }
        return []; 
    }

    /**
     * 根据一级二级类目、品牌查出产品id
     * @param $classId
     * @param bool $parent
     * @return array
     * @author xi
     * @date 2018-2-3
     */
    public static function getIdsByClassId($classId,$parent = false,$brandId='')
    {
        if($classId){
            if($parent == true){
                $where = ['first_class_id' => $classId,'status'=>1];
            }
            else {
                $where = ['class_id' => $classId,'status'=>1];
            }
        }

        if($brandId){
            $where['brand_id']=$brandId;
        }


        $query = self::findAllByAttributes($where,'id');
        if($query){
            return array_column($query,'id');
        }
        return [];
    }

    /**
     * 根据ids 查出产品信息
     * @param $ids
     * @return array
     */
    public static function getSaleOrderInfoByIds($ids)
    {
        $result = self::findAllByAttributes(['id'=>$ids],'id,prod_id,prod_name,brand_id,brand_name,serial_number,class_name','id');
        if($result)
        {
            $brandArr = [];
            $brandIds = array_unique(array_column($result,'brand_id'));
            $brandRes = ServiceBrand::find()->where(['id'=>$brandIds])->select('id,title')->asArray()->all();
            if($brandRes){
                $brandArr = array_column($brandRes,'title','id');
            }

            foreach ($result as $key=>$val)
            {
                $result[$key]['brand_name'] = '';
                if(isset($brandArr[$val['brand_id']])){
                    $result[$key]['brand_name'] = $brandArr[$val['brand_id']];
                }
            }
        }
        return $result;
    }
    
    /**
     * 添加客户意向产品
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/24
     * Time: 15:16
     * @param $data
     * @return bool
     */
    public static function addIntentionProduct($data)
    {
        if (!empty($data))
        {
            $model = new self();
        
            foreach ($data as $key => $val) {
                $model->$key = $val;
            }
        
            if ($model->save(false)) {
                return $model->id;
            }
            return false;
        }
        return false;
    }
    
    /**
     * 修改/删除 客户意向产品
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/24
     * Time: 14:46
     * @param $data
     * @return bool
     */
    public static function editIntentionProduct($data)
    {
        if (!empty($data) && isset($data['id']))
        {
            $model = self::findOne(['id'=>$data['id']]);
            unset($data['id']);
            
            foreach ($data as $key => $val) {
                $model->$key = $val;
            }
            
            if ($model->save(false)) {
                return true;
            }
            return false;
        }
        return false;
    }
    /**
     * 批量添加售后产品
     * User: quan
     * Date: 2018/7/30
     * @param $data
     * @return bool
     */
     public static function batchSale($data,$type=0){
         if($type == 0){
             foreach ($data as $key=>$val){
                 $terow = array_keys($val);
             }
             $tedata = $data;
         }else{  //合同下单
             $tedata = [];
             $terow = ['department_id', 'department_top_id', 'prod_id', 'status','brand_id','class_id','intention_type','contract_order','contract_id','contract_detail_id'];
             foreach ($data as $key=>$val){
                 $tedata[$key]['department_id'] = $val['department_id'];
                 $tedata[$key]['department_top_id'] = $val['department_top_id'];
                 $tedata[$key]['prod_id'] = $val['prod_id'];
                 $tedata[$key]['status'] = 1;
                 $tedata[$key]['brand_id'] = $val['brand_id'];
                 $tedata[$key]['class_id'] = $val['class_id'];
                 $tedata[$key]['intention_type'] = 1;
                 $tedata[$key]['contract_order'] = 1;
                 $tedata[$key]['contract_id'] = $val['parent_id'];
                 $tedata[$key]['contract_detail_id'] = $val['id'];
             }
         }
         return self::getDb()->createCommand()->batchInsert(self::tableName(),$terow,$tedata)->execute();
     }
    
}
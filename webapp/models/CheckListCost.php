<?php
namespace webapp\models;

use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;

class CheckListCost extends BaseModel
{
    public static function getDb() {
        return Yii::$app->order_db;
    }

    
    public static function tableName()
    {
        return 'check_list_cost';
    }

    /**
     * 获取结算单
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getOne($where = [])
    {
        return self::find()->where(['src_type'=>self::SRC_FWS,'src_id'=>BaseLogic::getManufactorId()])->andWhere($where)->one();
    }

    /**
     * 获取结算单列表
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getList($param = [])
    {
        $db = self::find();
        foreach($param as $k=>$v){
            if(!empty($v)){
                $db->andwhere([$k=>$v]);
            }
        }

        $db->select('cost_name,sum(amount) as amount');
        $db->groupBy('cost_name');



        $list = $db ->asArray()->all();
        return $list;
    }


    public static function add($data){
        return $res =self::getDb()->createCommand()->batchInsert(self::tableName(), ['cost_id','cost_name','amount','check_list_id','order_id'],$data)->execute();
    }



    public static function updateStatus($where,$status=1,$amountReal=0){
        $time = time();
        if($amountReal){
            return self::getDb()->createCommand()->update(self::tableName(), ['status' => $status,'amount_real'=>$amountReal,'update_time'=>$time], ['id' => $where])->execute();
        }
        //echo '<pre>';print_R($where);exit;
        self::getDb()->createCommand()->update(self::tableName(), ['status' => $status,'update_time'=>$time], ['id' => $where])->execute();
    }



}
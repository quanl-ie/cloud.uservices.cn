<?php
namespace webapp\models;

use common\models\CostItemSet;
use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;

class CostItem extends BaseModel
{
    public static function getDb()
    {
        return Yii::$app->order_db;
    }
    
    public static function tableName()
    {
        return 'cost_item';
    }
    
    /**
     * 获取列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 13:39
     * @param array $where
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getList($where = [])
    {
        return self::find()->filterWhere($where)->asArray()->all();
    }
    
    /**
     * 获取品牌分页列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 13:37
     * @param int $page
     * @param int $pageSize
     * @param array $where
     * @return array
     */
    public static function getPageList($where = [])
    {
        $db = self::find();
        //判断where
        if (!empty($where))
        {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }
    
        $db->orderBy(['create_time'=> SORT_DESC]);
        $db->asArray();
        $list = $db->all();
        
        return [
            'list'       => $list
        ];

    }
    
    /**
     * 批量添加
     * @param $departmentId 部门id
     * @param $brandArr 品牌数组
     * @return bool
     * @author xi
     * @date 2018-6-6
     */
    public static function batchInsert($departmentId,$itemArr)
    {
        $where = [
            'department_id' => $departmentId,
            'del_status'    => 1
        ];
        $oldItemIds = [];
        $oldItemRes = self::findAllByAttributes($where,'cost_item_set_id');
        if($oldItemRes){
            $oldItemIds = array_column($oldItemRes,'cost_item_set_id');
        }
        
        //算出新加的
        $newItemIds = array_diff($itemArr,$oldItemIds);
        //算出要删除的
        $delItemIds = array_diff($oldItemIds,$itemArr);
        
        if($newItemIds)
        {
            foreach ($newItemIds as $val)
            {
                $model = new self();
                $model->department_id     = $departmentId;
                $model->direct_company_id = Department::getDirectCompanyIdById($departmentId);
                $model->cost_item_set_id  = $val;
                $model->create_time       = time();
                $model->update_time       = time();
                if(!$model->save(false)){
                    return false;
                }
            }
        }
        if($delItemIds)
        {
            $res = self::updateAll(['del_status'=>2,'update_time'=>time()],['department_id'=>$departmentId,'cost_item_set_id'=>$delItemIds]);
            if(!$res){
                return false;
            }
        }
        
        return true;
    }

    /**
     * 查出设置部门的收费项目
     * @param $selfDepartmentId
     * @param $departmentId
     * @return array
     * @author xi
     * @date 2018-6-6
     */
    public static function getSelfItem($selfDepartmentId,$departmentId)
    {
        $dsn = CostItemSet::getDb()->dsn;
        $dbName = explode('dbname=',$dsn)[1];

        $where = [
            'a.department_id' => $selfDepartmentId,
            'a.status'        => 1,
            'a.del_status'    => 1,
            'b.del_status'    => 1
        ];
        $query = self::find()
            ->from(self::tableName() . ' as a')
            ->innerJoin([ '`'.$dbName.'`.`'.CostItemSet::tableName().'` as b'], 'a.cost_item_set_id = b.id')
            ->where($where)
            ->select('a.cost_item_set_id,b.name,b.status')
            ->asArray()
            ->all();

        if($query)
        {
            //查出已设置部门的id
            $itemSetIds = [];
            $itemSetRes = self::findAllByAttributes(['department_id'=>$departmentId,'status'=>1,'del_status'=>1],'cost_item_set_id');
            if($itemSetRes){
                $itemSetIds = array_column($itemSetRes,'cost_item_set_id');
            }

            $result = [];
            foreach ($query as $val)
            {
                $result[] = [
                    'id'     => $val['cost_item_set_id'],
                    'name'   => $val['name'],
                    'status' => $val['status'],
                    'checked'=> in_array($val['cost_item_set_id'], $itemSetIds)?1:0
                ];
            }
            return $result;
        }

        return [];
    }
    
}
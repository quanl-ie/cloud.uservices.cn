<?php
namespace webapp\models;

use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;


class TechnicianCostRuleRelation extends BaseModel
{
    public static function getDb() {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'technician_cost_rule_relation';
    }
    /**
     * 获取单个收费项目
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getOne($where = [])
    {
        return self::find()->where(['src_type'=>self::SRC_SJ,'src_id'=>BaseLogic::getManufactorId()])->andWhere($where)->one();
    }
    /**
     * 获取多个收费项目
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @param type $where
     * @return type
     */

    public static function getList($param)
    {
        //return self::find()->where(['src_type'=>self::SRC_SJ,'src_id'=>BaseLogic::getManufactorId()])->andWhere($where)->all();
        $db = self::find();
        if(isset($param['pageSize'])){
            $pageSize = $param['pageSize'];
        }else{
            $pageSize = 10;
        }



        if(!isset($param['currentPage'])){
            $page = 0;
        }else{
            $page = $param['currentPage']-1;
        }
        unset($param['currentPage']);
        $start = $pageSize*$page;

        $db->from(self::tableName() . ' as a');
        $db->select('id,cost_name');
        $db->where(['status'=>1,'src_type'=>BaseModel::SRC_SJ,'src_id'=>BaseLogic::getManufactorId()]);

        if(isset($param['company'])&&!empty($param['company'])){
            $list = Manufactor::getListInfo($param['company']);
            foreach($list as $v){
                $ids[] = $v['id'];
            }
            $db->andWhere( ['in','service_provider_id',$ids]);
        }
        if(isset($param['cost_name'])&&!empty($param['cost_name'])){
            $db->andWhere(['like','cost_name',$param['cost_name']]);
        }

        $count = $db->count();
        $totalPage = ceil($count/$pageSize);
        $db->offset($start);
        $db->asArray();
        $db->limit($pageSize);
        $list = $db->all();
        return ['totalPage'=>$totalPage,'list'=>$list,'page'=>$page+1];

    }



    public static function getAll($where=[]){
        return self::find()->where($where)->asArray()->all();
    }

   /* public static function setDelete($where){
         self::getDb()->createCommand()->delete(self::tableName(),$where);
         echo self::getDb()->createCommand()->getRawSql();exit;
    }*/






}
<?php
namespace webapp\models;

use Yii;
use webapp\models\BaseModel;
use webapp\logic\BaseLogic;
use webapp\models\Technician;
use webapp\models\TechnicianCheckListCost;
use webapp\models\TechnicianCheckListDetail;

class TechnicianBalance extends BaseModel
{
    public static function getDb() {
        return Yii::$app->db;
    }

    
    public static function tableName()
    {
        return 'technician_balance';
    }

    /**
     * 获取结算单
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @param type $where
     * @return type
     */
    public static function getOne($where = [])
    {
        return self::find()->where(['src_type'=>self::SRC_FWS,'src_id'=>BaseLogic::getManufactorId()])->andWhere($where)->asArray()->one();
    }
    /**
     * 根据条件获取结算单数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $where
     * @return type
     */
  public static function updateData($technicianId,$amount,$count){
     $info  = self::getOne(['tech_id'=>$technicianId]);

     if(isset($info)&& !empty($info)){
         $info->total_amount = ($info->total_amount+$amount);
         $info->total_work_number =  $info->total_work_number+$count;
         $info->save(false);
     }else{
         $info = new self();
         $info->total_amount = $amount;
         $info->total_work_number =  $count;
         $info->tech_id = $technicianId;
         $info->save(false);
     }
  }

}
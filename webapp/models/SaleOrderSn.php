<?php
namespace webapp\models;

use common\helpers\Helper;
use common\models\Dynaactionform;
use common\models\ServiceBrand;
use common\models\ServiceClass;
use Yii;
use yii\data\Pagination;
use webapp\models\ServiceGoods;

class  SaleOrderSn extends SaleOrder
{

    public static function tableName()
    {
        return 'sale_order_sn';
    }
    
    public static function getList($where)
    {
        return self::find()->filterWhere($where)->asArray()->all();
    }
      public static function getLists($param)
      {
            $db = self::find()
                  ->where(['status'=>1]);
            if(!empty($param['account_id'])){
                  $db->andwhere(['account_id'=>$param['account_id']]);
            }
            if(!empty($param['account_name'])){
                  $db->andwhere(['like','account_name',$param['account_name']]);
            }
            if(!empty($param['brand_id'])){
                  $db->andwhere(['brand_id'=>$param['brand_id']]);
            }
            if(!empty($param['start_time'])){
                  $db->andwhere(['>=','buy_time',$param['start_time']]);
            }
            if(!empty($param['end_time'])){
                  $db->andwhere(['<','buy_time',$param['end_time']]);
            }
            if(!empty($param['intention_type'])){
              $db->andwhere(['intention_type' => $param['intention_type']]);
            }
            $pageSize = $param['pageSize'];
            if(!isset($param['currentPage'])){
                  $page = 0;
            }else{
                  $page = $param['currentPage']-1;
            }
            unset($param['currentPage']);

            $count =$db
                  ->count();
            $totalPage = ceil($count/$pageSize);
            $start = $page*$pageSize;
            $limit = $pageSize;
            $order =$db->offset($start)
                  ->orderBy('create_time DESC')
                  ->limit($pageSize)
                  ->asArray()->all();
            echo $db->createCommand()->rawSql;
            $data['list'] = $order;
            $data['totalPage'] = $totalPage;
            $data['page'] = $page+1;
            return $data;
      }

    /**
     * 填加sn
     * @param $snIds
     * @param $orderNo
     */
      public static function addSnData($snIds,$orderNo)
      {
          $query = self::findAllByAttributes(['id'=>$snIds,'status'=>1]);
          if($query)
          {
              foreach ($query as $val)
              {
                  $model = new self();
                  $model->order_no      = $orderNo;
                  $model->sale_order_id = $val['sale_order_id'];
                  $model->sn            = $val['sn'];
                  $model->remark        = $val['remark'];
                  $model->create_time   = $val['create_time'];
                  $model->update_time   = $val['update_time'];
                  $model->status        = $val['status'];
                  $model->save();
              }
          }
      }

    /**
     * 获取动态表单
     * @param array $data
     * @return array|mixed
     * @author xi
     */
    public static  function getDynaactionformData($departmentId,array $data,$orderNo)
    {
        $dyFormArr = Dynaactionform::getFormData($departmentId);
        if($dyFormArr)
        {
            $subject = self::getFaultDesc($orderNo);
            foreach ($dyFormArr as $key => $val)
            {
                foreach ($data as $v)
                {
                    if( $v['keyName'] == $val['keyName']){
                        $dyFormArr[$key]['value'] = $v['value'];
                        break;
                    }
                }

                //故障描述
                if($val['keyName'] == 'faultDesc'){
                    $dyFormArr[$key]['value'] = $subject;
                }
            }
        }

        return $dyFormArr;
    }

    /**
     * 获取故障描述
     * @param $orderNo
     * @author xi
     */
    public static function getFaultDesc($orderNo)
    {
        $postData = [
            'order_no' => $orderNo,
        ];
        $url = Yii::$app->params['order.uservices.cn']."/v3/work-js/work-basics";
        $jsonStr = Helper::curlPostJson($url,$postData);
        $jsonArr = json_decode($jsonStr,true);
        if(isset($jsonArr['success']) && $jsonArr['success'] == true)
        {
            $data = $jsonArr['data'];
            return  $data['subject'];
        }

        return '';
    }

}
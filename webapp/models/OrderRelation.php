<?php

namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "facilitator_order_relation".
 *
 * @property string $id
 * @property int $order_id 关联订单编号
 * @property int $pay_type 支付方式1支付宝2微信3线下
 * @property int $source 订单来源
 * @property int $facilitator_id 服务商id
 * @property int $facilitator_user_id 服务商操作员id
 * @property string $error_description 故障描述
 * @property int $city_parent_id 城市父级id
 * @property int $city_id 城市id
 */
class OrderRelation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufactor_order_relation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id', 'pay_type', 'source', 'facilitator_id', 'facilitator_user_id', 'city_parent_id', 'city_id','created_at','updated_at'], 'integer'],
            [['error_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'pay_type' => 'Pay Type',
            'source' => 'Source',
            'facilitator_id' => 'Facilitator ID',
            'facilitator_user_id' => 'Facilitator User ID',
            'error_description' => 'Error Description',
            'city_parent_id' => 'City Parent ID',
            'city_id' => 'City ID',
            'created_at' => 'Create Time',
            'updated_at' => 'Update Time'
        ];
    }
    /**
     * 获取当前服务商所有订单id
     * @author lxq <lxq@c-ntek.com>
     * @date 2017-9-4 11:00
     * @return mixed
     */
    public static function OrderId()
    {
        $userId  = yii::$app->user->identity->id;
        $facilitatorId = OrderRelation::find()->select('facilitator_id')->where(['facilitator_user_id'=>$userId])->one();
        $data = OrderRelation::find()->select('order_id')->where(['facilitator_id'=>$facilitatorId['facilitator_id']])->all();
        $orderId = [];
        foreach ($data as $key => $val) {
            $orderId[$key] = $val['order_id'];
        }
        return $orderId;
    }

    /**
     * 分页显示
     * @author lxq <lxq@c-ntek.com>
     * @date 2017-9-4 11:00
     * @return mixed
     */
    public static function OrderPagination($query)
    {
        // 得到文章的总数（但是还没有从数据库取数据）
        $count = $query->count();
        // 使用总数来创建一个分页对象        
        $pagination = new Pagination(['totalCount' => $count,'pageSize'=>20]);
        // 使用分页对象来填充 limit 子句并取得文章数据
        $order = $query->offset($pagination->offset)
            ->orderBy('create_time DESC')
            ->limit($pagination->limit)
            ->all();
        $data = [];
        $data['pagination'] = $pagination;
        $data['order']      = $order;
        return $data;
    }
    /**
	 * @desc 判断显示取消订单按钮
	 * @date 2017-09-04 14：00
	 * @author lxq < lxq@c-ntek.com >
	 * @return  mixed
	 */
    public static function UnRemove($id)
    {
        $status = [1,2,3];
        $order = Order::find()
                ->select('make_time,order_status,start_work')
                ->where(['id'=>$id])
                ->one();
        if ( in_array ($order->order_status, $status)) {
            return true;
        }
        return false;
    }
    /**
	 * @desc 转换时间
	 * @date 2017-09-05 17：00
	 * @author lxq < lxq@c-ntek.com >
	 * @return  mixed
	 */
    public static function Time($time,$status)
    {
        $nowtime=date($time);
        if ($status == 1) {
            return strtotime($nowtime)+86399;
        }
        return strtotime($nowtime);
    }
}

<?php
namespace webapp\models;

use Yii;

class Depot extends BaseModel
{

    public static function tableName()
    {
        return 'depot';
    }

    /**
     * 获取公司下所有库房
     * @param $directCompanyId
     * @return array
     */
    public static function getAllDepot($directCompanyId)
    {
        $where = [
            'direct_company_id' => $directCompanyId,
            'status'    => 0,
            'is_delete' => 0
        ];
        $query = self::findAllByAttributes($where,'id,name');
        if($query){
            return $query;
        }

        return [];
    }

}
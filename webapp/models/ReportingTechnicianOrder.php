<?php
namespace webapp\models;

use Yii;

class ReportingTechnicianOrder extends BaseModel
{
    public static function tableName()
    {
        return 'reporting_technician_order';
    }
    public static function getList($map,$page,$pageSize)
    {
        $db = self::find();
        if (!empty($map)) {
            foreach ($map as $key=>$val){
                if(is_array($val)){
                    $db->andWhere([$val[0],$val[1],$val[2]]);
                }else{
                    $db->where([$key=>$val]);
                }
            }
        }
        $db->groupBy('technician_id');
        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum / $pageSize);
            if ($page < 1) {
                $page = 1;
            } else if ($page > $totalPage) {
                $page = $totalPage;
            }
            $db->select('technician_name,department_id,sum(work_num) as work_num,sum(finish_num) as finish_num,sum(all_appraise_num) as all_appraise_num,sum(goods_appraise_num) as goods_appraise_num,sum(bad_appraise_num) as bad_appraise_num,sum(on_time) as on_time');
            $db->offset(($page - 1) * $pageSize);
            $db->limit($pageSize);
            $db->orderBy('work_num desc');
            $list = $db->asArray()->all();
            if($list)
            {
                $departmentIds = array_column($list,'department_id');
                $department = Department::findAllByAttributes(['id'=>$departmentIds],'id,name',"id");
                foreach ($list as $k=>$v)
                {
                    if(isset($department[$v['department_id']])){
                        $list[$k]['department_name'] = $department[$v['department_id']]['name'];
                    }
                    if($v['all_appraise_num'] != 0){
                        $pval = sprintf("%.2f",intval($v['goods_appraise_num'])/intval($v['all_appraise_num']))*100;
                        if($pval > 100){
                            $pval = 100;
                        }
                        $list[$k]['goods_appraise_num'] = $pval."%";
                        $pval = sprintf("%.2f",intval($v['bad_appraise_num'])/intval($v['all_appraise_num']))*100;
                        if($pval > 100){
                            $pval = 100;
                        }
                        $list[$k]['bad_appraise_num'] = $pval."%";
                    }else{
                        $list[$k]['goods_appraise_num'] = "100%";
                        $list[$k]['bad_appraise_num'] = "0%";
                    }
                    if($v['finish_num'] != 0){
                        $pval = sprintf("%.2f",intval($v['on_time'])/intval($v['finish_num']))*100;
                        if($pval > 100){
                            $pval = 100;
                        }
                        $list[$k]['on_time'] = $pval ."%";
                    }else{
                        $list[$k]['on_time'] = "100%";
                    }
                    unset($list[$k]['all_appraise_num']);
                }
            }
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        } else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
}

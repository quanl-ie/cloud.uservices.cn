<?php
namespace webapp\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * 客户管理
 * @author liuxingqi <lxq@c-ntek.com>
 * @date 2017-12-15
 */
class  Account extends BaseModel
{

    public static function tableName()
    {
        return 'account';  //客户表
    }

    //规则
    public function rules()
    {
        return [
            [['src_type','src_id','account_type','account_name','sex','create_user','create_time','status','mobile','source_type'],'required'],
            ['mobile','match','pattern'=>'/^1[3456789]\d{9}$/','message'=>'手机号格式不正确'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'code'           => '编码',
            'src_type'     => '来源',  //14 厂商 13 服务商 12 门店 11 技师 20 c端用户',
            'src_id'       => '所有者唯一标识',
            'account_id'   => '用户分类',  //1：普通，2：VIP
            'account_name'   => '用户称呼',
            'sex'            => '性别', //1：先生，2：女士
            'create_user'    => '创建人',  //id
            'create_time'    => '创建时间',
            'update_time'    => '修改时间',
            'status'         => '状态', //1：正常，2：停用。
            'mobile'         => '联系方式',
            'source_type'       => '客户来源', //1：手动新建，2：售后时自动新建，3：其他渠道
        ];
    }
    
    //获取单条信息
    public static function getOne($where)
    {
        return self::find()->where($where)->one();
    }

    //获取多条信息
    public static function getList($map,$where,$page=1,$pageSize=10)
    {
        $db = self::find();
        $db->from(self::tableName() . ' as a');
        $db->where($map);
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }

        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->leftJoin([AccountBehaviour::tableName() . ' as b'],' a.id = b.account_id');
            $db->leftJoin([Department::tableName() . ' as c'],' a.department_id = c.id');
            $db->select('a.id,a.account_name,a.mobile,a.create_time,a.source_type,b.order_number,b.product_number,c.name,c.direct_company_id');
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['create_time'=> SORT_DESC]);
            $db->limit($pageSize);
            $db->asArray();
            $list = $db->all();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    
    //查询客户电话是否存在
    public static function validateMobile($map,$where = '')
    {
        if (!empty($where)) {
            $res = self::find()->where($map)->andWhere($where)->one();
        }else{
            $res = self::find()->where($map)->one();
        }
        if (!empty($res)) {
            return false;
        }
        return true;
    }

    //添加
    public static function add($accountData,$accountAddressData)
    {
        //实例化客户表模型
        $account = new self();
        //处理客户表数据
        foreach ($accountData as $key => $val) {
            $account->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //添加客户表数据
            $account->save(false);
            //实例化客户联系地址表
            $accountAddress = new AccountAddress();
            //处理客户联系地址表数据
            $accountAddress->account_id = $account->id;
            foreach ($accountAddressData as $key => $val) {
                $accountAddress->$key = $val;
            }
            //添加客户联系地址表数据
            $accountAddress->save(false);
            $transaction->commit();
            return [
                'id' => $account->id,
                'name' => $account->account_name.'/'.$account->mobile,
                'address_id'=>$accountAddress->id
            ];
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    //编辑
    public static function edit($accountData,$accountAddressData)
    {
        //查询客户表数据
        $where['id'] = $accountData['id'];
        $account = self::getOne($where);
        //处理客户表数据
        foreach ($accountData as $key => $val) {
            $account->$key = $val;
        }
        //查询客户联系地址表数据
        $map['account_id'] = $accountAddressData['account_id'];
        
        $accountAddress = AccountAddress::getOne($map);
        //判断是否存在
        if (!$accountAddress) {
            $accountAddress = new AccountAddress();
            $accountAddress->create_time = time();
        }
        //处理客户联系地址表数据
        foreach ($accountAddressData as $key => $val) {
            $accountAddress->$key = $val;
        }
        //开启事务
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            //修改客户表数据
            $account->save(false);
            //修改客户联系地址表数据
            $accountAddress->save(false);
            $transaction->commit();
            return true;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return false;
    }
    
    /**
     * 获取客户信息
     * @author li
     * @date   2017-12-21
     */
    public static function getAccountInfo($name, $departmentId,$flag = 0) {
          if($flag == 1){ //本人权限
                $andWhere = [
                      'create_user' => $departmentId,
                      'status'      => 1
                ];
          }elseif($flag == 2){
              $andWhere = [
                  'status'      => 1
              ];
          }else{
                $andWhere = [
                      'department_id' => $departmentId,
                      'status'      => 1
                ];
          }
       $data = self::find()
                ->where(['like', 'account_name', $name])
                ->orWhere(['like', 'mobile', $name])
                ->andWhere($andWhere)
                ->select(['id','department_id','account_name','mobile'])
                ->asArray()->all();
        return $data;
    }

}
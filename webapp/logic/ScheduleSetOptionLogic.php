<?php
namespace webapp\logic;

use common\helpers\Helper;
use common\models\ScheduleSet;
use common\models\ScheduleSetOption;
use webapp\models\BaseModel;
use webapp\models\GroupingRelTechnician;
use webapp\models\Technician;
use common\models\ScheduleTechnician;
use Yii;
use webapp\models\Grouping;
use webapp\models\Character;


class ScheduleSetOptionLogic extends BaseLogic
{
    /**
     * 页面
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/22
     * Time: 14:35
     * @return mixed
     */
    public static function index($findDepartmentsId)
    {

        $department_id = BaseLogic::getDirectCompanyId();

        //查询所有分组
        $groupData = Grouping::getGroupingAttr($findDepartmentsId);
    
        // 将数组按照字母重新排列
        if ($groupData) {
            $character = (new Character)->groupByInitials($groupData, 'name',1);
            $character = array_values($character);
    
            //重新组合分组
            foreach ($character as $val) {
                $data['group'][-1]         = '默认组';
                $data['group'][$val['id']] = $val['name'];
            }
        }else{
            $data['group'] = [];
        }
        
        //获取班次
        $set = ScheduleSet::getList(['direct_company_id'=>$department_id,'department_top_id'=>self::getTopId(),'is_delete'=>1]);
        if ($set) {
            foreach ($set as $val) {
                $data['scheduleSet'][$val['id']] = $val['name'];
            }
    
        }else{
            $data['scheduleSet'] = [];
        }

        //获取重复模式
        $repeateType = self::getRepeateType($department_id);
        if ($repeateType['code'] == 200) {
            $data['repeateType'] = $repeateType['data'];
        }else{
            $data['repeateType'] = [];
        }
        
        //获取技师
        $tech = self::getTech('',$findDepartmentsId);
        if ($tech['code'] == 200) {
            $data['tech'] = $tech['data'];
        }else{
            $data['tech'] = [];
        }
        
        return $data;
    }
    
    /**
     * 动态获取技师
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/22
     * Time: 14:34
     * @return array
     */
    public static function getTech($id = '',$findDepartmentsId = '')
    {
        $data = [];
        $group = intval(Yii::$app->request->get('id',0));
        $techId = [];

        if ($id) {
            $group = $id;
        }
    
        //查询分组
        if (!empty($group) && $group == -1) {
            $groWhere = ['department_id'=>$findDepartmentsId,'status'=>1];
        }elseif (!empty($group)){
            $groWhere = ['department_id'=>$findDepartmentsId,'status'=>1,'grouping_id'=>$group];
        }else{
            $groWhere = ['department_id'=>$findDepartmentsId,'status'=>1];
        }
        $groupingRelTechnician = GroupingRelTechnician::find()->where($groWhere)->asArray()->all();
    
        foreach ($groupingRelTechnician as $val) {
            $techId[$val['technician_id']] = $val['grouping_id'];
        }
        
        //查询技师
        $where = ['audit_status'=>5,'status'=>1,'store_id'=>$findDepartmentsId];
        $tech = Technician::find()->select('id,name')->where($where)->asArray()->orderBy('created_at DESC')->all();
    
        if (!empty($group) && $group == -1) {
            foreach ($tech as $key => $val) {
                if (isset($techId[$val['id']])) {
                    unset($tech[$key]);
                }
            }
        
        }elseif (!empty($group)){
            foreach ($tech as $key => $val) {
                if (!isset($techId[$val['id']])) {
                    unset($tech[$key]);
                }
            }
        }
        // 将数组按照字母重新排列
        foreach ($tech as $key => $val) {
            $data[$val['id']] = $val['name'];
        }
        
        return self::success($data);
    
    }
    
    /**
     * 添加
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/22
     * Time: 14:34
     * @return array
     * @throws \Exception
     */
    public static function add()
    {
        $techId        = Yii::$app->request->post('tech_id','');
        $scheduleSetId = intval(Yii::$app->request->post('schedule_set_id',0));
        $repeateType   = Helper::filterWord(Yii::$app->request->post('repeate_type',0));
        $repeateDate   = Yii::$app->request->post('repeate_date',0);
        $repeateWeek   = Yii::$app->request->post('repeate_week',0);
        $endDate       = Helper::filterWord(Yii::$app->request->post('end_date',0));
        $description   = Helper::filterWord(Yii::$app->request->post('description',''));
        
        if ($techId == '') {
            return self::error('请选择技师',20001);
        }
        if ($scheduleSetId == 0) {
            return self::error('请选择班次',20002);
        }
        if ($repeateType == 0) {
            return self::error('请选择重复模式',20003);
        }
        
        $data['department_top_id'] = self::getTopId();
        $data['department_id']     = self::getDepartmentId();
        $data['schedule_set_id']   = $scheduleSetId;
        $data['repeate_type']      = $repeateType;
        
        if ($repeateType == 2) {
            $data['repeate_date'] = implode(',',$repeateWeek);
        } elseif ($repeateType == 3) {
            $data['repeate_date'] = implode(',',$repeateDate);
        }else{
            $data['repeate_date']    = $repeateDate;
        }

        $data['end_date']        = $endDate;
        $data['description']     = $description;
        $data['create_time']     = date('Y-m-d H:i:s',time());
        $data['create_user']     = Yii::$app->user->id;
        $data['is_delete']       = 1;
        
        $res = ScheduleSetOption::add($data,$techId);
        if ($res) {
            return self::success('','批量排班成功');
        }
        return self::error('批量排班失败');
        
    }
    
    //获取所有班次列表
    public static function GetTechSchedule()
    {
        $where = [
            'direct_company_id' => BaseLogic::getDirectCompanyId(),
            'department_top_id' => BaseLogic::getTopId(),
            'is_delete'         => 1
        ];
        $scheduleList = ScheduleSet::getList($where);
        $scheduleList = array_column($scheduleList,'name','id');

        return $scheduleList;
    }
    //单独设置技师班次或修改
    public static function SetTechSchedule()
    {
        if (Yii::$app->request->isGet) {
            $time = Yii::$app->request->get('time');
            if (isset($time) && $time < date('Y-m-d')) {
                return self::error('无法修改今日之前的班次！',20001);
            }else{
                return self::success('','');
            }
        }


        $postData['department_top_id']   = BaseLogic::getTopId();
        $postData['tech_id']             = intval(Yii::$app->request->post('tecId',0));         //技师id
        $postData['schedule_set_id']     = intval(Yii::$app->request->post('shift',0)); //技师班次id
        $postData['apply_date']          = trim(Yii::$app->request->post('time',''));        //技师班次时间
        $postData['description']         = trim(Yii::$app->request->post('note',''));       //技师班次备注
        $postData['create_user']         = BaseLogic::getLoginUserId();
        $postData['create_time']         = date('Y-m-d H:i:s', time());
        $techArr = Technician::findOne(['id' => $postData['tech_id']]);
        $postData['department_id']       = $department_id = $techArr->store_id;

        if(!$postData['tech_id'] || $postData['tech_id'] == 0){
            return self::error('技师不能为空！',20002);
        }
        $where = [
            'department_id'=>$department_id,
            'tech_id'=>$postData['tech_id'],
            'apply_date'=>$postData['apply_date']
        ];
        $scheduleInfo = ScheduleTechnician::getOne($where);
        if($scheduleInfo){
            //修改技师的班次
            $res = ScheduleTechnician::edit($postData);
        }else{
            //添加技师班次
            $res = ScheduleTechnician::add($postData);
        }
        if ($res) {
            return self::success($res,'设置成功！');
        }
        return self::error('设置失败!',20003);
    }
}
<?php
namespace webapp\logic;


use common\models\ReturnGoods;
use common\helpers\Helper;
use webapp\controllers\StockInController;
use webapp\controllers\StockOutController;
use yii;
use webapp\models\Account;
use webapp\models\BaseModel;
use common\helpers\CreateNoRule;
use webapp\models\AccountAddress;
use webapp\models\AdminUser;
use common\models\ExchangeGoods;
use common\models\ExchangeGoodsDetail;
use common\models\Contract;
use common\models\ContractDetail;
use common\models\DictEnum;
use common\models\StockInfo;
use common\models\SendOut;
/***
 * @author chengjuanjuan<chengjuanjuan@c-ntek.com>
 * @created_at 2017-12-14 14:01
 * desc the logic of sale orders
 */


class ExchangeGoodsLogic extends BaseLogic
{


    /**
     * 函数用途描述：
     * @date: 2017年12月14日 下午14:48:01
     * @author: chengjuanjuan<chengjuanjuan@c-ntek.com>
     * @param: $id
     * @return:$data
     */
    public static function getList($shipSys='')
    {

        $account_id                  = intval(yii::$app->request->get('account_id'));  //此参数用于从客户详情中查看此客户的合同信息，不影响现有的搜索，不要删除 by sxz
        $accountMobile               = trim(Yii::$app->request->get('account_mobile'));
        $query['startTime']          =  trim(Yii::$app->request->get('plan_start_time'));
        $query['endTime']            =  trim(Yii::$app->request->get('plan_end_time'));
        $query['no']                 = trim(Yii::$app->request->get('no'));
        $query['currentPage']        = Yii::$app->request->get('page',1);
        $query['pageSize']           = Yii::$app->request->get('pageSize',10);

        $query['subject']            = Helper::filterWord(Yii::$app->request->get('subject'));
        $accountName                 =  Helper::filterWord(Yii::$app->request->get('account_name'));
        $getDataAuth                 = self::getListDataAuth();

        if(empty($getDataAuth)){
            exit('无数据权限');
        }

        $departmentIds               = $getDataAuth['department_id'];
        $dataAuth                    = $getDataAuth['dataAuth'];
        $createUserId                = $getDataAuth['create_user_id']?$getDataAuth['create_user_id']:'';
        $department_name             = $getDataAuth['department_name'];
        $query['department_id']      =  $departmentIds;
        if($createUserId){
            $query['create_user_id'] = $createUserId;
        }
        if($accountMobile)
            $accountName = $accountMobile;
        if($accountName){
            $accountIds  = '';
            $accountList = Account::getAccountInfo($accountName,self::getDepartmentId(),2);
            foreach($accountList as $v){
                $accountIds[] = $v['id'];
            }
            $query['account_id'] = $accountIds;
        }
        //此参数用于从客户详情中查看此客户的合同信息，不影响现有的搜索，不要删除 by sxz
        if($account_id){
            $query['account_id'] = $account_id;
        }

        //确定状态
        $action    = Yii::$app->controller->action->id;
        if($action == 'wait-check'){
            $query['status'] = 1;
            //审批通过的換貨單直接进入执行中
        }elseif($action == 'executing'){
            $query['status'] = 3;
        }elseif($action == 'execute-finished') {
            $query['status'] = 4;

        }elseif($action == 'unchecked') {
            $query['status'] = 5;

        }elseif($action == 'terminated') {
            $query['status'] = 6;

        }

        $data = ExchangeGoods::getList($query);

        if(!empty($data['list'])){
            $exNewNumArr = [];
            //销售开关开启时候
            $rel_type = 4; //关联类型  4 换货
            if($shipSys == 1){
                $exIds = array_column($data['list'],'id');
                //查询换货详情的数量
                $exchangeNumArr = ExchangeGoodsDetail::findAllByAttributes(['parent_id'=>$exIds,'type'=>2],'id,parent_id,prod_id,prod_num');
                $result = [];
                foreach ($exchangeNumArr as $key1=>$val1) {
                    $result[$val1['parent_id']][] = $val1['prod_num'];
                }
                foreach ($result as $m =>$n){
                    $val1 = array_values($n);
                    $exNewNumArr[$m] = array_sum($val1);
                }
            }
            if($data['list']){
                foreach($data['list'] as &$v){
                    $accountInfo = Account::getOne(['id'=>$v['account_id']]);
                    $v['account_name'] = isset($accountInfo->account_name)?$accountInfo->account_name:'';
                    $v['account_mobile'] = isset($accountInfo->mobile)?$accountInfo->mobile:'';
                    if($query['status'] = 3){
                        //销售开关开启时候 获取发货状态
                        if($shipSys == 1 && !empty($exNewNumArr)){
                            $v['prod_total_num'] = $exNewNumArr[$v['id']];
                            //获取当前合同的发货状态
                            $shipStatusInfo         = StockInfoLogic::getRelShipStatus($v['id'],$v['prod_total_num'],$rel_type);
                            $v['ship_status']       = $shipStatusInfo['status'];
                            $v['ship_status_desc']  = $shipStatusInfo['msg'];
                        }
                        $detail =  ExchangeGoodsDetail::getList(['parent_id'=>$v['id'],'type'=>1]);
                        foreach($detail as $vv){
                            if($vv['type']==1 && ($vv['prod_num'] == $vv['finish_num'])){
                                $v['returnButton'] = 0;
                                continue;
                            }else{
                                $v['returnButton'] = 1;
                            }
                            if($vv['prod_num'] == $vv['finish_num']){
                                $v['exchangeButton'] = 0;
                            }else{
                                $v['exchangeButton'] = 1;
                            }

                        }
                        $detail2 =  ExchangeGoodsDetail::getList(['parent_id'=>$v['id'],'type'=>2]);
                        foreach($detail2 as $vvv){
                            if($vvv['prod_num'] == $vvv['finish_num']){
                                $v['exchangeButton'] = 0;
                                continue;
                            }else{
                                $v['exchangeButton'] = 1;
                            }

                        }
                    }
                    $v['option']          = $dataAuth[$v['department_id']]['option'];
                    $v['department_name'] = $dataAuth[$v['department_id']]['name'];
                }
            }

        }
        //print_r($data['list']);die;
        $data['department_name'] = $department_name;
        return $data;
    }




    /**
     * @desc 换货添加
     * @author:程娟娟
     * @createAt:20171215
     **/
    public static function add()
    {//处理数据
        $accountId                 = intval(yii::$app->request->post('account_id'));
        $subject                   = trim(yii::$app->request->post('subject'));
        $contractDetail            = yii::$app->request->post('contractDetail');
        if(!$accountId)
            return self::error('用户不存在');
        //退货产品处理明细
        foreach($contractDetail as $v){

            if(isset($v['id'])){
                $id = $v['id'];
                $productInfo = ContractDetail::getOne(['id'=>$id]);
                $productInfo['prod_num'] = $v['prod_num'];
                if($v['prod_num']==0){
                    continue;
                }
                $haveInfo = [
                    'prod_id'=>$productInfo['prod_id'],
                    'prod_num'=>$v['prod_num'],
                    'sale_price'=>$productInfo['sale_price'],
                    'rate'=>$productInfo['rate'],
                    'amount'=>$v['amount'],
                    'total_amount'=>$v['total_amount'],
                    'warranty_date'=>$v['warranty_date'],
                    'type'=>1
                ];
                $products[] = isset($haveInfo)?$haveInfo:[];
            }
        }
        $exchanges = Yii::$app->request->post('prod');
        if(isset($products) && $exchanges){
            $products = array_merge_recursive($products,$exchanges);
        }else{
            return self::error('添加失败');
        }
        //基本信息
        $data['department_top_id']  =  self::getTopId();
        $data['department_id']      =   self::getDepartmentId();
        $data['no']                 =   CreateNoRule::getNo('HH');
        $data['account_id']         =   $accountId;
        $data['subject']            =   $subject;
        $data['exchange_date']      =   trim(yii::$app->request->post('exchange_date',''));
        $data['express_type']       =   trim(yii::$app->request->post('express_type'));
        $data['delivery_address']   =   trim(yii::$app->request->post('delivery_address',''));
        $data['exchange_reason']    =   intval(yii::$app->request->post('exchange_reason'));
        $data['contract_id']        =    intval(yii::$app->request->post('contract_id'));
        $data['need_stock_in']      =    intval(Yii::$app->request->post('need_stock_in'));
        $data['total_amount']       =    trim(Yii::$app->request->post('total_amount'));
        $data['account_address_id'] =    intval(yii::$app->request->post('address_id'));


        //系统信息
        $data['create_time']       =   date("Y-m-d H:i:s");
        $data['create_user_id']    =  Yii::$app->user->id;
        $data['status']            =  yii::$app->request->post('status','1');
        
        $res = ExchangeGoods::add($data,$products);
        if ($res) {
            return self::success([
                'id' => $res
            ],'添加成功');
        }
        return self::error('添加失败');
    }




    /**
     * @desc 修改
     * @author chengjuanjuan <chengjuanjuan@c-ntek.com>
     * @createAt 2017-12-18 16:06
     **/
    public static function edit()
    {   $id                         = intval(Yii::$app->request->post('id'));
        $accountId                  = intval(yii::$app->request->post('account_id'));
        $subject                    = trim(yii::$app->request->post('subject'));
        $totalAmount                = trim(Yii::$app->request->post('total_amount'));
        if(!$id)
            return self::error('该换货不存在');
        //产品处理明细
        $products                  =   yii::$app->request->post('prod');

        //退货产品处理明细
        $contractDetail            =   yii::$app->request->post('contractDetail');
        foreach($contractDetail as $k=>$v){
            if(isset($v['id'])&&!empty($v['id'])){
                $productInfo = ContractDetail::getOne(['id'=>$v['id']]);
                $productInfo['prod_num'] = $v['prod_num'];
                $haveInfo = [
                    'prod_id'=>$productInfo['prod_id'],
                    'prod_num'=>$v['prod_num'],
                    'sale_price'=>$productInfo['sale_price'],
                    'rate'=>$productInfo['rate'],
                    'amount'=>$v['amount'],
                    'total_amount'=>$v['total_amount'],
                    'warranty_date'=>$v['warranty_date'],
                    'type'=>1
                ];
                $contractProduct[] = $haveInfo;
            }
        }

        $exchanges = Yii::$app->request->post('prod');
        $haveProduct = array_merge_recursive($contractProduct,$exchanges);

        $needStockIn                =  intval(Yii::$app->request->post('need_stock_in'));
        //基本信息
        $data['id']                 =   $id;
        $data['department_id']      =   self::getDepartmentId();
        $data['no']                 =   CreateNoRule::returnGoodsNo();
        $data['subject']            =   $subject;
        $data['exchange_date']      =   trim(yii::$app->request->post('exchange_date',''));
        $data['express_type']       =   trim(yii::$app->request->post('express_type'));
        $data['delivery_address']   =   trim(yii::$app->request->post('delivery_address',''));
        $data['exchange_reason']    =   intval(yii::$app->request->post('return_reason'));
        $data['need_stock_in']      =   $needStockIn;
        $data['total_amount']       =   $totalAmount;
        //系统信息
        $data['create_time']        =   date("Y-m-d H:i:s");
        $data['create_user_id']     =  Yii::$app->user->id;
        $data['status']             =  1;
        $data['audit_status']       =  0;
        $res = ExchangeGoods::edit($data,$haveProduct);
        if ($res) {
            return self::success([
                'id' => $res
            ],'编辑成功');
        }
        return self::error('编辑失败');
    }



    public static function  getDetail($id=''){
        if(!$id){
            $id = Yii::$app->request->get('id');
        }
        if(!$id){
            return self::error('该id不存在');
        }
        $getDataAuth = self::getListDataAuth();
        $demartmentId = $getDataAuth['department_id'];
        $info       = ExchangeGoods::getOne(['id'=>$id,'department_id'=>$demartmentId]);
        if(empty($info)){
            return self::error('该换货单不存在');
        }
        $contractId            = $info['contract_id'];
        $contractInfo          = Contract::getOne(['id'=>$contractId]);
        $info['contract_info'] = $contractInfo['subject'];
        $account               = Account::getOne(['id'=>$contractInfo['account_id']]);
        $account_name       = isset($account->account_name)?$account->account_name:'';
        $account_mobile     = isset($account->mobile)?$account->mobile:'';
        $account_address    = isset($account->address)?$account->address:'';
        $address               = AccountAddress::getOne(['account_id'=>$contractInfo['account_id'],'is_default'=>1]);
        $info['account_name']  = $account_name.'/'.$account_mobile.'/'.$account_address;
        $info['account']       = $account_name;
        $showStatus            = ExchangeGoods::showStatus();
        $info['show_status']   = $showStatus[$info['status']];
        $showReasonStatus      = ExchangeGoods::showReasonStatus();
        if($info['exchange_reason']){
            $info['show_reason_status'] = $showReasonStatus[$info['exchange_reason']];
        }
        $showExpressType                = ExchangeGoods::showExpressType();

        if($info['express_type']){
            $info['show_express_type']  = $showExpressType[$info['express_type']];
        }
        if($info['create_user_id'])
        {
            $admin = AdminUser::getOne(['id'=>$info['create_user_id']]);
            $info['create_user'] = $admin['username'];
        }

        $detail = ExchangeGoodsDetail::getList(['parent_id'=>$id]);
        foreach($detail as &$v)
        {
            $productInfo = ProductLogic::getDetail($v['prod_id']);
            if(isset($productInfo['data']))
            {
                $product = ProductLogic::changeData(array($productInfo['data']), '');
                $v['prod_name'] = isset($product[0]['prod_name']) ? $product[0]['prod_name'] : '';
                $v['brand_name'] = isset($product[0]['brand_name']) ? $product[0]['brand_name'] : '';
                $v['class_name'] = isset($product[0]['class_name']) ? $product[0]['class_name'] : '';
                $v['model'] = isset($product[0]['model']) ? $product[0]['model'] : '';
            }else{
                $detail = [];
            }
        }
        //入库记录
        $reInList   = StockInfoLogic::index(1,2);
        $inList     = isset($reInList['list'])?$reInList['list']:[];
        //出库记录
        /*$reOutList  = StockInfoLogic::index(2);
        $outList    = isset($reOutList['list'])?$reOutList['list']:[];*/
        $outList = StockInfo::getList(['rel_id'=>$id,'rel_type'=>4,'direct_company_id'=>self::getDirectCompanyId()]);
        //发货记录
        //$stockInfo = StockInfo::getList(['direct_company_id' => self::getDirectCompanyId(),'rel_id' => $info['id'], 'is_rel' => 1 ,'rel_type' => 4,'type'=>2]);
        //获取发货类型名称
        $sendTypeList = array_column(DictEnum::getList(['dict_key'=>'send_out_type']),'dict_enum_value','dict_enum_id');
        //获取发货方式名称
        $sendMethodList = array_column(DictEnum::getList(['dict_key'=>'send_method_type']),'dict_enum_value','dict_enum_id');
        //发货状态
        $sendStatuslist = ['0'=>'待发货','1'=>'运输中',2=>'已签收'];
        //发货记录
        if($outList)
        {
            $stockIds = array_column($outList,'id');
            //print_r($stockIds);die;
            $sendList = SendOut::findAllByAttributes(['stock_id'=>$stockIds],'id,stock_id,send_no,send_status,send_type,send_method,create_time,rel_rec_date','stock_id');
            foreach ($outList as $key=>$val)
            {
                //产品数量
                if (!empty($val['id']))
                {
                    $prodSum = StockInfoLogic::getProdSum($val['id']);
                    $outList[$key]['prod_sum'] = $prodSum['data'];
                }
                //出入库状态
                if (!empty($val['status']))
                {
                    $auditStatus = StockInfoLogic::getStatus($val['type'],$val['status']);
                    $outList[$key]['status_desc'] = $auditStatus['data'];
                }
                if($sendList){
                    foreach ($sendList as $k1=>$v1)
                    {
                        $sendList[$k1]['send_status_desc'] = isset($sendStatuslist[$v1['send_status']])?$sendStatuslist[$v1['send_status']]:'';
                        $sendList[$k1]['send_type_desc']   = isset($sendTypeList[$v1['send_type']])?$sendTypeList[$v1['send_type']]:'';
                        $sendList[$k1]['send_method_desc'] = isset($sendMethodList[$v1['send_method']])?$sendMethodList[$v1['send_method']]:'';
                        if($k1 == $val['id']){
                            $sendList[$k1]['prod_sum'] = $outList[$key]['prod_sum'];
                        }
                    }
                }
            }
        }
        $data = [
            'info'      =>$info,
            'detail'    =>$detail,
            'inList'    =>$inList,
            'outList'   =>$outList,
            'sendList'  =>isset($sendList)?$sendList:[],
            ];
        return self::success($data);
    }


    public  static function updateStatus($type='check'){
        $id = intval(Yii::$app->request->post('id'));
        if($type=='check'){
            $auditStatus = intval(Yii::$app->request->post('auditStatus'));
            $auditSuggest = trim(Yii::$app->request->post('auditSuggest'));
            if($auditStatus==1){  //审核通过
                $status = 3;
            }elseif($auditStatus==2){ //审核不通过
                $status=5;
            }
            $data = [
                'audit_status'=>$auditStatus,
                'audit_suggest'=>$auditSuggest,
                'status'=>$status,
                'audit_date'=>date("Y-m-d H:i:s"),
                'audit_user_id'=>Yii::$app->user->id,
            ];
        }elseif($type=='delete'){
            $data['status'] = 7;
        }elseif($type='terminate'){
            $data['status'] = 6;
        }
        $res = ExchangeGoods::updateStatus($data,$id);
        if($res){
            return self::success($res);
        }else{
            return self::error('更新失败');
        }
    }




    public  static function checkOption($type='check'){
        $id = intval(Yii::$app->request->post('id'));
        //$detail  = '';
            $auditStatus = intval(Yii::$app->request->post('auditStatus'));
            $auditSuggest = trim(Yii::$app->request->post('auditSuggest'));
            if($auditStatus==1){  //审核通过
                //跟合同详情里的total_num比对，申请的数据大于当前数据直接返回错误
                $data = self::getDetail($id);
                $exchangeInfo  = $data['data']['info'];
                $exchangeDetail = $data['data']['detail'];
                $contrctId = $exchangeInfo['contract_id'];
                $contractList = ContractDetail::getList(['parent_id'=>$contrctId]);
                foreach($contractList as $v){
                    $prod[$v['prod_id']] = $v['total_num'];
                }
                foreach($exchangeDetail as $v){
                    if($v['type']==2){
                        continue;
                    }
                    if(isset($v['prod_id'])){
                        $lastNum = $prod[$v['prod_id']];
                        if($v['prod_num']==0){
                            return self::error('申请换货数量不能为0','20023');
                        }
                        if($v['prod_num']>$lastNum){
                            return self::error('申请换货数量不能超过合同数量','2002');
                        }else{
                            $needUpdate[] = [
                                'parent_id' => $contrctId,
                                'total_num' =>$lastNum-$v['prod_num'],
                                'prod_id'   =>$v['prod_id']
                            ];
                        }
                    }
                }
                $status = 3;
            }elseif($auditStatus==2){ //审核不通过
                $status=5;
                $contrctId = '';
                $needUpdate = [];
            }
            $data = [
                'audit_status'=>$auditStatus,
                'audit_suggest'=>$auditSuggest,
                'status'=>$status,
                'audit_date'=>date("Y-m-d H:i:s"),
                'audit_user_id'=>Yii::$app->user->id,
            ];

        $res = ExchangeGoods::checkOption($data,$id,$contrctId,$needUpdate);
        if($res){
            return self::success($res);
        }else{
            return self::error('审批操作失败');
        }
    }





}
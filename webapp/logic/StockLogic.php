<?php
    /**
     * Created by PhpStorm.
     * User: xingq
     * Date: 2018/3/29
     * Time: 15:18
     */
    
    namespace webapp\logic;
    
    use common\models\Depot;
    use common\models\DepotProdStock;
    use common\models\Product;
    use common\models\Purchase;
    use common\models\ServiceBrand;
    use common\models\ServiceClass;
    use common\models\StockDetail;
    use common\models\StockInfo;
    use webapp\models\TechStorage;
    use Yii;
    use webapp\models\ManufactorClass;
    use common\models\PurchaseDetail;


    class StockLogic extends BaseLogic
    {
        /**
         * 列表所需数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/29
         * Time: 16:37
         * @return array
         */
        public static function getBeforeList()
        {
            $data = [];
            //获取自有品牌
            $brand = self::getBrand('',1);
            if ($brand) {
                $data['brand'] = $brand;
            }else{
                $data['brand'] = [];
            }
            //获取自有分类
            $class = self::getClass(0,1);
            if ($class) {
                $data['class'] = $class;
            }else{
                $data['class'] = [];
            }
            return $data;
        }
    
        /**
         * 搜索数据处理
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/29
         * Time: 17:45
         * @return array
         */
        public static function search()
        {
            $prodName      = trim(Yii::$app->request->get('prod_name',''));
            $brandId       = intval(Yii::$app->request->get('brand_id',0));
            $classId       = Yii::$app->request->get('class_id', 0);
            $depotName     = trim(Yii::$app->request->get('depot_name',''));
            $directCompanyId = self::getDirectCompanyId();

            $prodId = [];
            if ($prodName != '') {
                $where = [
                    ['=','direct_company_id',$directCompanyId],
                    ['like','prod_name',$prodName],
                ];
                $product = Product::search($where);
                if ($product) {
                    $prodId = array_filter(array_column($product,'id'));
                }else{
                    $prodId = [0];
                }
            }
            if ($brandId != 0) {
                $where = [
                    ['=','direct_company_id',$directCompanyId],
                    ['=','brand_id',$brandId],
                ];
                
                if ($prodId) {
                    $where[] = ['in','id',$prodId];
                }
                $product = Product::search($where);
                
                if ($product) {
                    $prodId = array_filter(array_column($product,'id'));
                }else{
                    $prodId = [0];
                }
            }

            if ($classId != 0) {
                $where = [
                    ['=','direct_company_id',$directCompanyId],
                    ['in','class_id',$classId],
                ];

                if ($prodId) {
                    $where[] = ['in','id',$prodId];
                }
                $product = Product::search($where);
                if ($product) {
                    $prodId = array_filter(array_column($product,'id'));
                }else{
                    $prodId = [0];
                }
            }

            $depotId = [];
            if ($depotName != '') {
                $where = [
                    ['=','direct_company_id',$directCompanyId],
                    ['like','name',$depotName],
                ];
                $depot = Depot::search($where);
                if ($depot) {
                    $depotId = array_filter(array_column($depot,'id'));
                }else{
                    $depotId = [0];
                }
            }
           $where = [];
            if ($prodId) {
                $where[] = ['a.prod_id'=>$prodId];
            }
            if ($depotId) {
                $where[] = ['a.depot_id'=>$depotId];
            }
            
            return $where;
        }
    
        /**
         * 首页
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/30
         * Time: 9:59
         * @return array
         */
        public static function getIndex()
        {

            $directCompanyId = self::getDirectCompanyId();

            $page     = intval(Yii::$app->request->get('page',1));
            $pageSize = intval(Yii::$app->request->get('pageSize',10));
            $classId       = Yii::$app->request->get('class_id', 0);

            
            $map   = ['a.direct_company_id'=>$directCompanyId];
            $where = self::search();

            $data = DepotProdStock::getIndex($map,$where,$page,$pageSize);
            if (!empty($data['list'])) {
                $prodId = array_unique(array_column($data['list'],'prod_id'));
                $product = [];
                foreach ($data['list'] as $key => $val) {
                    if (in_array($val['prod_id'],$prodId)) {
                        $product[$val['prod_id']][] = $val;
                    }
                }

                $productList = [];
                foreach ($product as $key => $val) {
                    $bad_num = array_sum(array_column($val,'bad_num'));
                    $num = array_sum(array_column($val,'num'));
                    $on_sale_num = array_sum(array_column($val,'on_sale_num'));
                    if($on_sale_num < 0){
                        $on_sale_num = 0;
                    }
                    $productList[$key]['prod_id'] = $val[0]['prod_id'];
                    $productList[$key]['prod_batch'] = $val[0]['prod_batch'];
                    $productList[$key]['bad_num'] = $bad_num;
                    $productList[$key]['num'] = $num;
                    $productList[$key]['freeze_num'] = $val[0]['freeze_num'];
                    $productList[$key]['total_num'] = $num;
                    $productList[$key]['on_sale_num'] = $on_sale_num;
                    $productList[$key]['depot_id'] = $val[0]['depot_id'];
                    $productList[$key]['prod_name'] = $val[0]['prod_name'];
                    $productList[$key]['brand_id'] = $val[0]['brand_id'];
                    $productList[$key]['class_id'] = $val[0]['class_id'];
                    $productList[$key]['unit_id'] = $val[0]['unit_id'];
                    $productList[$key]['prod_no'] = $val[0]['prod_no'];
                }
                $list = self::changeData($productList);
            }
            if (!empty($list['data'])) {
                $data['list'] = $list['data'];
                //获取采购在途的产品数量
                $purchaseInData = Purchase::findAllByAttributes(['direct_company_id'=>self::getDirectCompanyId(),'isship'=>2],'id','id');
                $purchaseIds = array_column($purchaseInData,'id');
                $purchaseInDetailData = PurchaseDetail::findAllByAttributes(['parent_id'=>$purchaseIds],"prod_id,sum(`send_num`) as purchase_in_sum",'prod_id',1000,'prod_id');
                $purchaseInNums = array_column($purchaseInDetailData,'purchase_in_sum','prod_id');
                foreach($data['list'] as $key=>$val){
                    $data['list'][$key]['purchase_in_sum'] = 0;
                    foreach($purchaseInNums as $k=>$v){
                        if($key == $k) {
                            $data['list'][$key]['purchase_in_sum'] = $v;
                        }
                    }

                }
            }
            //类目搜索条件
            $className = ServiceClass::getName($classId);

            if (!empty($className))
            {
                if (is_array($className)) {
                    $data['className'] = implode(';',$className);
                }else{
                    $data['className'] = $className;
                }
            }else{
                $data['className'] = '';
            }
            return $data;
        }
    
        /**
         * 组合库存管理首页数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/29
         * Time: 21:57
         * @param $data
         * @return array
         */
        public static function changeData($data)
        {
            $directCompanyId = self::getDirectCompanyId();
            if (!$data) {
                return self::error('获取列表失败');
            }

            $brandId = array_column($data,'brand_id');
            $brand = ServiceBrand::getName($brandId);

            $classId = array_column($data, 'class_id');
            $className = ProductLogic::getClassName($classId, 2);

            $brandName = [];
            
            foreach ($brand as $key => $val) {
                if (strstr($val,'/',true)) {
                    $brandName[$key] = strstr($val,'/',true);
                }else{
                    $brandName[$key] = $val;
                }
            }
            foreach ($data as $key => $val) {
                //品牌
                if (isset($brandName[$val['brand_id']])) {
                    $data[$key]['brand_name'] = $brandName[$val['brand_id']];
                }else{
                    $data[$key]['brand_name'] = '';
                }

                //分类
                if (isset($className[$val['class_id']])) {
                    $data[$key]['class_name'] = $className[$val['class_id']]['class_name'];
                }else{
                    $data[$key]['class_name'] = '';
                }

                //计量单位
                $unit = self::getUnit();
                if (isset($unit[$val['unit_id']])) {
                    $data[$key]['unit_name'] = $unit[$val['unit_id']];
                }else{
                    $data[$key]['unit_name'] = '';
                }
                
                //申请采购数量
                $data[$key]['purchase_num'] = self::getPurchase($directCompanyId,$val['prod_id']);
                
                //技师领用（未使用）
                $data[$key]['storage_num'] = self::getTechStorageNum($val['prod_id']);
                
                //库存数量
                $data[$key]['sum'] = $val['num']+$data[$key]['storage_num']+ $val['bad_num'];
            }

            return self::success($data);
        }
    
        /**
         * 获取产品采购数量
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/29
         * Time: 21:56
         * @param $directCompanyId 直属公司ID
         * @param $prodId
         * @return float|int|string
         */
        public static function getPurchase($directCompanyId,$prodId)
        {
            $map = ['a.direct_company_id'=>$directCompanyId, 'a.audit_status'=>2];
            $where = ['b.direct_company_id'=>$directCompanyId, 'b.prod_id'=>$prodId];
            $res = Purchase::getDetail($map,$where);
            $num = 0;
            if ($res) {
                //$prodNum = array_sum(array_column($res,'prod_num'));
                $prodNum = array_sum(array_column($res,'activity_prod_num'));
                $finishNum = array_sum(array_column($res,'finish_num'));
                $num = $prodNum-$finishNum;
                if($num < 0){
                    $num = 0;
                }
            }
            return $num;
        }
    
        /**
         * 获取技师领用未使用数量
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/10
         * Time: 14:06
         * @param $prodId
         * @return float|int
         */
        public static function getTechStorageNum($prodId)
        {
            $res = TechStorage::getList(['prod_id'=>$prodId]);
    
            $num = 0;
            if ($res) {
                $num = array_sum(array_column($res,'num'));
            }
            return $num;
        }
    
        /**
         * 库存详情
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/10
         * Time: 14:10
         * @return array
         */
        public static function getView()
        {
            $id = intval(Yii::$app->request->get('id',0));
            if ($id == 0) {
                return self::error('获取库存详情失败');
            }
            //基本信息
            $directCompanyId = self::getDirectCompanyId();
            $product = Product::getOne(['id'=>$id],1);

            $data = [];
            $data['prod_no']   = $product['prod_no'];
            $data['prod_name'] = $product['prod_name'];
            $data['type_name'] = $product['type_id'] == 1 ? '产品' :'配件';
            
            //品牌
            $brandName = ServiceBrand::getName($product['brand_id']);
    
            if (isset($brandName[$product['brand_id']])) {
                $data['brand_name'] = strstr($brandName[$product['brand_id']],'/',true);
            }else{
                $data['brand_name'] = '';
            }

            //分类
            $class = ProductLogic::getClassName($product['class_id'], 2);
            if(isset($class[$product['class_id']])){
                $data['class_name'] = $class[$product['class_id']]['class_name'];
            }else{
                $data['class_name'] = '';
            }

            $data['model'] = $product['model'];
            
            //库存信息
            $depotProduct = DepotProdStock::getList(['direct_company_id'=>$directCompanyId,'prod_id'=>$id]);
            $depotProdStock = [];
            foreach ($depotProduct as $val) {
                if ($val['depot_id'] == $val['depot_id']) {
                    $depotProdStock[$val['depot_id']][] = $val;
                }
            }
            
            //预出库产品数量查询
            $stockOut = self::getStockOut($directCompanyId,$id);
            
            $depot = [];
            //库房名称
            $depotName = self::getDepot($directCompanyId);
            foreach ($depotProdStock as $key => $val) {
                $depotId = current(array_unique(array_column($val,'depot_id')));
                $depot[$key]['depot_id'] = $depotId;
                if (isset($depotName[$depotId])) {
                    $depot[$key]['depot_name'] = $depotName[$depotId];
                }else{
                    $depot[$key]['depot_name'] = '';
                }
                $bad_num = array_sum(array_column($val,'bad_num'));
                $num = array_sum(array_column($val,'num'));
                $depot[$key]['bad_num'] = $bad_num;
                $depot[$key]['total_num'] = $num+$bad_num;

                if (isset($stockOut[$depotId])) {
                    $depot[$key]['stock_out_num'] = $stockOut[$depotId];
                }else{
                    $depot[$key]['stock_out_num'] = 0;
                }
                $depot[$key]['num'] = $num;
                $depot[$key]['prod_id'] = $id;
            }
            
            $data['depot'] = $depot;
            return $data;
        }
    
        /**
         * 获取待出库产品数量
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/10
         * Time: 15:41
         * @param $directCompanyId
         * @param $prodId
         * @return array
         */
        public static function getStockOut($directCompanyId, $prodId)
        {
            $stockOut = StockInfo::getList(['direct_company_id'=>$directCompanyId,'type'=>2,'audit_status'=>1]);
            $data = [];
            if ($stockOut) {
                $stockOutId = array_column($stockOut,'id');
                $where = ['direct_company_id'=>$directCompanyId,'parent_id'=>$stockOutId,'prod_id'=>$prodId];
                $stock = StockDetail::getList($where);
                
                $depot = [];
                foreach ($stock as $val) {
                    if ($val['depot_id'] == $val['depot_id']) {
                        $depot[$val['depot_id']][] = $val;
                    }
                }
                
                foreach ($depot as $key => $val) {
                    $num = array_sum(array_column($val,'prod_num'));
                    $data[$key] = $num;
                }
            }
            return $data;
        }
    
        /**
         * 获取库存明细
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/10
         * Time: 15:42
         * @return array
         */
        public static function getDetail()
        {
    
            $page     = intval(Yii::$app->request->get('page',1));
            $pageSize = intval(Yii::$app->request->get('pageSize',10));
            $depotId = intval(Yii::$app->request->get('depotId',0));
            $prodId  = intval(Yii::$app->request->get('prodId',0));
            if ($depotId == 0 || $prodId ==0) {
                return self::error('获取库存明细失败',20001);
            }
            $directCompanyId = self::getDirectCompanyId();
            
            //查询库存明细
            $map = ['direct_company_id'=>$directCompanyId,'depot_id'=>$depotId,'prod_id'=>$prodId];
            
            $res = DepotProdStock::getPagData($map,$page,$pageSize);
            
            //获取入库单产品相关信息
            $stockIn = self::getStockIn($directCompanyId,$depotId,$prodId);
            
            $list = [];
            
            foreach ($res['list'] as $key => $val) {
                if (isset($stockIn[$val['prod_batch']])) {
                    $list[$key]                = $stockIn[$val['prod_batch']];
                    $list[$key]['total_num']   = $val['total_num']+$val['bad_num'];
                    $list[$key]['bad_num']     = $val['bad_num'];
                    $list[$key]['num']         = $val['num'];
                    $list[$key]['prod_batch']  = $val['prod_batch'];
                    $list[$key]['create_time'] = $val['create_time'];
                }
            }
            $res['list'] = $list;
            
            return $res;
        }
    
        /**
         * 获取入库单产品相关信息
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/10
         * Time: 16:13
         * @param $directCompanyId
         * @param $orgType
         * @param $depotId
         * @param $prodId
         * @return array
         */
        public static function getStockIn($directCompanyId,$depotId,$prodId)
        {
            $stockIn = StockInfo::getList(['direct_company_id'=>$directCompanyId,'type'=>1,'audit_status'=>2]);
            $data = [];
            if ($stockIn) {
                $stockOutId = array_column($stockIn,'id');
                $where = ['direct_company_id'=>$directCompanyId,'parent_id'=>$stockOutId,'depot_id'=>$depotId,'prod_id'=>$prodId];
                $stock = StockDetail::getList($where);
                
                foreach ($stock as $key => $val) {
                    $data[$val['prod_batch']]['create_date']    = $val['create_date'];
                    $data[$val['prod_batch']]['prod_date']      = $val['prod_date'];
                    $data[$val['prod_batch']]['effective_date'] = $val['effective_date'];
                }
            }
            return $data;
    
        }
    }
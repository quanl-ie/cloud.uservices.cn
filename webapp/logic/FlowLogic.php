<?php
namespace webapp\logic;

use common\models\ServiceType;
use webapp\models\ManufactorType;
use Yii;
use common\models\ServiceFlow;

class FlowLogic extends BaseLogic
{
    /**
     * 获取服务流程列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 13:33
     * @return array
     */
    public static function getIndex()
    {
    
        $dcId  = self::getDirectCompanyId();
        $topId = self::getTopId();

        $id  = Yii::$app->request->get('typeId',0);
        $str = '00';
        if (strpos($id, $str) === false) {
            $typeId = $id;
        }else{
            $typeId = substr($id,strlen($str));
        }
        
        //获取列表数据
        $serviceType = ManufactorType::getPageList([['=','a.direct_company_id',$dcId]]);
        
        $type = [];
        //根据获取到的服务类型id组合数组
        foreach ($serviceType['list'] as $key => $val) {
            
            if ($val['type_id'] == $typeId)
            {
                $type[$str.$val['type_id']]['title']  = $val['title'];
                $type[$str.$val['type_id']]['status'] = $val['status'];
            }
            elseif ($typeId == 0 && $key == 0)
            {
                $type[$str.$val['type_id']]['title']  = $val['title'];
                $type[$str.$val['type_id']]['status'] = $val['status'];
            }
            
            $type[$val['type_id']]['title']  = $val['title'];
            $type[$val['type_id']]['status'] = $val['status'];
            
        }

        //查找多余数据
        $keys = array_keys($type);
        
        //删除多余数据
        foreach ($keys as $key => $val) {
            if (substr($val,strlen($str)) == $val)
            {
                $typeId = substr($val,strlen($str));
                unset($type[substr($val,strlen($str))]);
            }
        }
        
        $serviceFlow = ServiceFlow::getList(['type_id'=>$typeId,'department_id'=>$topId,'del_status'=>1]);

        $data['serviceType'] = $type;
        $data['serviceFlow'] = [];
        
        //根据查询出的数据进行判断取出数据
        foreach ($serviceFlow as $key => $val) {
    
            if (isset($type[$str.$val['type_id']]['title']) && isset($type[$str.$val['type_id']]['status']))
            {
                if ($type[$str.$val['type_id']]['status'] == 2) {
                    $data['disbled'] = 1;
                }else{
                    $data['disbled'] = 2;
                }
    
                if ($type[$str.$val['type_id']]['title'] == $val['title'])
                {
                    $data['serviceFlow'][0]['title']  = $val['title'];
                    $data['serviceFlow'][0]['status'] = $val['status'];
                }
                else
                {
                    $data['serviceFlow'][$val['id']]['title']  = $val['title'];
                    $data['serviceFlow'][$val['id']]['status'] = $val['status'];
                }
            }
            
        }
        ksort($data['serviceFlow']);
        
        if ($dcId == $topId) {
            $data['flag'] = 1;
        }else{
            $data['flag'] = 2;
        }
        
        return $data;
       
    }
    
    /**
     * 编辑/添加数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/15
     * Time: 15:41
     * @return array
     * @throws \Throwable
     */
    public static function add()
    {
        
        $topId = self::getTopId();
        
        $type_id = trim(Yii::$app->request->post('typeId',''));
        if (strpos($type_id, '00') != false) {
            $type_id = strpos($type_id, '00');
        }else{
            $type_id = $type_id;
        }
        $id    = trim(Yii::$app->request->post('flowId'));
        $title = strip_tags(Yii::$app->request->post('flowName'));
    
        if ($type_id == '') {
            return self::error('服务类型获取失败',20001);
        }
        
        $serviceType = ServiceType::findOne(['id'=>$type_id,'status'=>2]);
        if (!empty($serviceType)) {
            return self::error('该服务类型已禁用，无法添加服务流程。请启用后，再次尝试。',20002);
        }
        
        if (!$title) {
            return self::error('请输入服务流程名称',20003);
        }
    
        $serviceFlow = ServiceFlow::getOne(['department_id'=>$topId,'title'=>$title,'type_id'=>$type_id,'status'=>[1,2]]);
        if ($serviceFlow) {
            return self::error('服务流程名称已存在',20004);
        }
        
        $time = time();
        
        $data['department_id'] = $topId;
        $data['type_id']       = $type_id;
        $data['title']         = $title;
        $data['status']        = 1;
        $data['del_status']    = 1;
        $data['oprate_id']     = self::getLoginUserId();
        $data['create_time']   = $time;
        $data['update_time']   = $time;
        
        $res = ServiceFlow::add($data,$id);
        
        if ($res) {
            return self::success($res,'保存成功');
        }else{
            return self::error('失败',20005);
        }
        
        
    }

    /**
     * 修改服务流程状态（禁用、启用）
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 13:51
     * @return array
     */
    public static function changeStatus()
    {
        $id     = intval(Yii::$app->request->post('id',0));
        $status = intval(Yii::$app->request->post('status',0));

        if ($id == 0) {
            return self::error('操作失败',20001);
        }

        if ($status == 0) {
            return self::error('操作失败',20002);
        }
    
        $time = time();
    
        $data['id']            = $id;
        $data['status']        = $status;
        $data['oprate_id']     = self::getLoginUserId();
        $data['update_time']   = $time;
        
        $res = ServiceFlow::changeStatus($data);

        if ($res) {
            return self::success('','操作成功');
        }
        return self::error('操作失败',20003);
    }
    
    /**
     * ajax查看状态
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/2
     * Time: 22:01
     * @return array
     */
    public static function getStatus()
    {
        $id = intval(Yii::$app->request->post('typeId',0));
        
        if ($id == 0) {
            return self::error('该服务类型已禁用，无法添加服务流程。请启用后，再次尝试。');
        }
    
        if (strpos($id, '00') != false) {
            $id = strpos($id, '00');
        }else{
            $id = $id;
        }
    
        $serviceType = ServiceType::findOne(['id'=>$id,'status'=>2]);
        
        if (!empty($serviceType)) {
            return self::error('该服务类型已禁用，无法添加服务流程。请启用后，再次尝试。',20002);
        }
        
        return self::success([]);
    }
    
    /**
     * 下单根据服务类型获取服务流程
     * User: sxz
     * Date: 2018/6/5
     * Time: 13:51
     * @return array
     */
    public static function getFlowStage($id=0,$is_default=1)
    {
        //$id     = intval(Yii::$app->request->post('id',0));

        if ($id == 0) {
            return self::error('数据获取失败',20001);
        }
        $res = ServiceFlow::getList(['department_id'=>self::getTopId(),'type_id'=>$id,'status'=>1,'del_status'=>1,'is_default'=>$is_default]);
        if($res){
            return self::success($res,'数据获取成功');
        }else{
            return self::error('数据获取失败',20001);
        }

    }
    
}

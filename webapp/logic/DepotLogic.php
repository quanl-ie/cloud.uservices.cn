<?php
namespace webapp\logic;
use Yii;
use common\models\Depot;
use webapp\models\BaseModel;
use common\helpers\Helper;
use webapp\models\Region;
use common\models\DepotProdStock;
/**
 * 库房管理
 * @author sxz
 * @date 2018-03-20
 */
class DepotLogic extends BaseLogic
{
    /**
     * 数据列表
     * @author sxz
     * @return string
     */
    public static function getList()
    {
        $page = intval(Yii::$app->request->get('page',1));
        $pageSize = intval(Yii::$app->request->get('pageSize',20));
        $maps = ['direct_company_id'=>BaseLogic::getDirectCompanyId(),'status'=>0,'is_delete'=>0];
        $where = [];
        $data = Depot::getIndexList($maps,$where,$page,$pageSize);
        $province_name = $city_name = '';
        if($data['list']) foreach ($data['list'] as $key=>$val ){
            $province_name = Region::getCityName(isset($val['province_id'])?$val['province_id']:'');
            $city_name = Region::getCityName(isset($val['city_id'])?$val['city_id']:'');
            $data['list'][$key]['full_address'] = $province_name.$city_name.$val['address'];
        }
        return $data;
    }
    /**
     * 数据添加和编辑
     */
    public static function add()
    {
        $data = Yii::$app->request->post();
        //print_r($data);die;
        $postData['no'] = trim(Yii::$app->request->post('no',''));
        $postData['name'] = htmlspecialchars(trim(Yii::$app->request->post('name','')));
        $postData['contact_name'] = htmlspecialchars(trim(Yii::$app->request->post('contact_name','')));
        $postData['contact_mobile'] = trim(Yii::$app->request->post('contact_mobile',''));
        $postData['province_id'] = intval(Yii::$app->request->post('province',0));
        $postData['city_id'] = intval(Yii::$app->request->post('city',0));
        $postData['district_id'] = intval(Yii::$app->request->post('district',0));
        $postData['address'] = htmlspecialchars(trim(Yii::$app->request->post('address','')));
        $postData['direct_company_id'] = BaseLogic::getDirectCompanyId();
        $postData['department_top_id'] = BaseLogic::getTopId();
        $postData['status'] = 0;
        $postData['create_time'] = date('Y-m-d H:i:s',time());
        $postData['create_user_id'] = BaseLogic::getLoginUserId();
        if($postData['no']){
            if(!preg_match('/^[A-Za-z0-9]*$/',$postData['no'])){
                return self::error('库房编号只能是字母或数字！');
            }
        }
        if(!$postData['name']){
            return self::error('库房名字不能为空！');
        }
        if(!$postData['contact_name']){
            return self::error('联系人姓名不能为空！');
        }
        if(!$postData['contact_mobile']){
            return self::error('联系人电话不能为空！');
        }
        if(!$postData['province_id']){
            return self::error('省份不能为空！');
        }
        if(!$postData['city_id']){
            return self::error('城市不能为空！');
        }
        if(!$postData['district_id']){
            return self::error('区域不能为空！');
        }
        if(!$postData['address']){
            return self::error('详细地址不能为空！');
        }
        //print_r($postData);die;
        $res = Depot::add($postData);
        if ($res) {
            return self::success($res,'成功');
        }
        return self::error('失败');
    }
    /**
     * 函数用途描述:库房编辑
     * @date: 2018年3月20日 下午6:27:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function edit(){
        $postData['id'] = intval(Yii::$app->request->post('id',0));
        $postData['no'] = trim(Yii::$app->request->post('no',''));
        $postData['name'] = htmlspecialchars(trim(Yii::$app->request->post('name','')));
        $postData['contact_name'] = htmlspecialchars(trim(Yii::$app->request->post('contact_name','')));
        $postData['contact_mobile'] = trim(Yii::$app->request->post('contact_mobile',''));
        $postData['province_id'] = intval(Yii::$app->request->post('province',0));
        $postData['city_id'] = intval(Yii::$app->request->post('city',0));
        $postData['district_id'] = intval(Yii::$app->request->post('district',0));
        $postData['address'] = htmlspecialchars(trim(Yii::$app->request->post('address','')));
        $postData['status'] = 0;
        $postData['create_time'] = date('Y-m-d H:i:s',time());
        $postData['create_user_id'] = BaseLogic::getLoginUserId();
        if($postData['no']){
            if(!preg_match('/^[A-Za-z0-9]*$/',$postData['no'])){
                return self::error('库房编号只能是字母或数字！');
            }
        }
        $res = Depot::edit($postData);
        if ($res) {
            return BaseLogic::success('','修改成功');
        }
        return BaseLogic::error('修改失败');
    }
    /**
     * 函数用途描述:获取库房信息
     * @date: 2018年3月20日 下午6:07:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function getDepotInfo($id)
    {
        $data = [];
        if($id){
            $data = Depot::getOne(['id'=>$id,'direct_company_id'=>BaseLogic::getDirectCompanyId(),'is_delete'=>0]);
            if($data){
                $data['province_name'] = Region::getCityName($data['province_id']);
                $data['city_name'] = Region::getCityName($data['city_id']);
                $data['district_name'] = Region::getCityName($data['district_id']);
            }
            return $data;
        }
        return $data;
    }
    /**
     * 函数用途描述:删除库房
     * @date: 2018年3月21日 下午13:07:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function delData()
    {
        $id     = intval(Yii::$app->request->post('id',0));
        $is_del = intval(Yii::$app->request->post('is_del',0));  //是否确认删除
        if($id && $id != 0){
            //判断库房是否存在
            $info = Depot::getOne(['id'=>$id,'direct_company_id'=>BaseLogic::getDirectCompanyId(),'is_delete'=>0]);
            if($info){
                //判断库房内是否有库存
                $stock_infos  = DepotProdStock::getList(['depot_id'=>$id]);
                $stock_count = 0;
                if($stock_infos){
                    foreach($stock_infos as $key=>$value){
                        $stock_count += (int)$value['total_num'];
                    }
                }
                if($stock_count > 0){
                    return self::error("此库房有库存，无法删除",'20007');
                }
                if($is_del == 1)
                {
                    //执行删除
                    $oneInfo = Depot::findOne(['id'=>$id,'is_delete'=>0,'direct_company_id'=>BaseLogic::getDirectCompanyId()]);
                    $oneInfo->is_delete  = 1;
                    $res = $oneInfo->save();
                    if($res){
                        return self::success('','成功');
                    }else{
                        return self::error("删除失败！");
                    }
                }
            }else{
                return self::error("未找到此仓库！");
            }
        }
        return self::error("未找到此仓库！");
    }




}
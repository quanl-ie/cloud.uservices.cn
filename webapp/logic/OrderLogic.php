<?php
namespace webapp\logic;

use common\helpers\QueuePush;
use common\models\Common;
use common\models\Contract;
use common\models\CostItemSet;
use common\models\Work;
use common\models\WorkOrderAssign;
use common\models\WorkRelTechnician;
use webapp\models\BaseModel;
use webapp\models\Department;
use webapp\models\GroupingRelTechnician;
use webapp\models\Region;
use webapp\models\SaleOrder;
use webapp\models\SaleOrderView;
use webapp\models\SaleOrderSn;
use common\models\Product;
use webapp\models\ServiceClass;
use webapp\models\Technician;
use Yii;
use common\helpers\Helper;
use webapp\models\Account;
use webapp\models\Stores;
use webapp\models\AccountAddress;
use webapp\logic\MessageLogic;
use common\lib\Push;
use webapp\models\CostLevel;
use webapp\models\CostLevelDetail;
use webapp\models\CostLevelItem;
use common\models\CostPayType;
use common\models\Order;
use common\models\User;
use webapp\models\User as User1;
use yii\helpers\ArrayHelper;


class OrderLogic extends BaseLogic
{

    /**
     * 订单列表
     * @return array
     * @author xi
     * @date 2017-12-18
     */
    public static function getList($from = 0,$workStatus = array())
    {
        $orderNo       = trim(Yii::$app->request->get('order_no',''));
        $status        = Yii::$app->request->get('status','');
        $accountName   = trim(Yii::$app->request->get('account_name',''));
        $accountMobile = trim(Yii::$app->request->get('account_mobile',''));
        $tecName   = trim(Yii::$app->request->get('tec_name',''));
        $tecMobile = trim(Yii::$app->request->get('tec_mobile',''));
        $workType      = intval(Yii::$app->request->get('work_type',0));
        $saleOrderId   = intval(Yii::$app->request->get('sale_order_id',0));
        $planStartTime = Yii::$app->request->get('plan_start_time','');
        $planEndTime   = Yii::$app->request->get('plan_end_time','');
        $createStartTime = Yii::$app->request->get('create_start_time','');  //下单开始时间  2018-5-10 新增
        $createEndTime   = Yii::$app->request->get('create_end_time','');    //下单结束时间 2018-5-10 新增
        $cancelStatus  = Yii::$app->request->get('cancel_status',-1);
        $accountId     = intval(Yii::$app->request->get('account_id',0));
        $visitStatus   = intval(Yii::$app->request->get('visit_status',0));
        $providerName  = trim(Yii::$app->request->get('provider_name',''));
        $amountType    = intval(Yii::$app->request->get('amount_type',0));
        $provinceId    = intval(Yii::$app->request->get('province_id',0));
        $cityId        = intval(Yii::$app->request->get('city_id',0));
        $districtId    = intval(Yii::$app->request->get('district_id',0));
        $firstClassId  = intval(Yii::$app->request->get('first_class_id',0));
        $secondClassId = intval(Yii::$app->request->get('second_class_id',0));
        $classId       = Yii::$app->request->get('class_id',[]);
        $searchDepartmentIds = Yii::$app->request->get('department_id','');
        $page          = intval(Yii::$app->request->get('page',1));
        $pageSize      = intval(Yii::$app->request->get('pageSize',10));
        $contractId    = intval(Yii::$app->request->get('contract_id',0));  //合同关联id
        //是否导出 1是 0不是
        $export        = Yii::$app->request->get('export',0);
        //评价订单搜索  sxz 2018-10-12 新增
        $appraisalStatus = intval(Yii::$app->request->get('appraisal_status',0));  //1 未评价 2 已评价
        $finishStartTime = Yii::$app->request->get('finish_start_time','');  //完成开始时间
        $finishEndTime   = Yii::$app->request->get('finish_end_time','');    //完成结束时间
        $fuzzy           = trim(Yii::$app->request->get('fuzzy','')); //模糊搜索条件 客户姓名或电话
        if(Yii::$app->request->isPost){
            $fuzzy           = trim(Yii::$app->request->post('fuzzy','')); //模糊搜索条件 客户姓名或电话
            $appraisalStatus = intval(Yii::$app->request->post('appraisal_status',0));  //1 未评价 2 已评价
            $accountName     = trim(Yii::$app->request->post('account_name',''));
            $accountMobile   = trim(Yii::$app->request->post('account_mobile',''));
            $finishStartTime = Yii::$app->request->post('finish_start_time','');  //完成开始时间
            $finishEndTime   = Yii::$app->request->post('finish_end_time','');    //完成结束时间
            $page            = intval(Yii::$app->request->post('page',1));
            $pageSize        = intval(Yii::$app->request->post('pageSize',10));
            $export          = intval(Yii::$app->request->post('export',0));  //是否导出 1是 0不是
        }

        $accountIds = [];
        $tecIds = [];
        //搜索
        if($accountName!='' || $fuzzy !=''){
            if($accountName != ''){
                $accountWhere = $accountName;
            }
            if($fuzzy != ''){
                $accountWhere = $fuzzy;
            }
            $res = Account::find()->andWhere(['like','account_name',$accountWhere])->select('id')->asArray()->all();
            if($res){
                $accountIds = array_column($res, 'id');
            }else{
                $accountIds = [-1];
            }

        }
        if($accountMobile != '' || $fuzzy !=''){
            if($accountMobile != ''){
                $accountWhere = $accountMobile;
            }
            if($fuzzy != ''){
                $accountWhere = $fuzzy;
            }
            $res = Account::find()->andWhere(['like','mobile',$accountWhere])->select('id')->asArray()->all();
            if($res && $accountName != ''){
                $accountIds = array_intersect($accountIds,array_column($res, 'id'));
            }
            if($res && $fuzzy != ''){
                $accountIds = array_intersect($accountIds,array_column($res, 'id'));
            }
            if($res){
                $accountIds = array_column($res, 'id');
            }
            if(empty($accountIds)){
                $accountIds = [-2];
            }
        }
        if($tecName != ''){
            $res = Technician::findAllByAttributes(['=','name',$tecName],'id');
            if($res){
                $tecIds = array_column($res, 'id');
            }else{
                $tecIds = [-1];
            }

        }
        if($tecMobile !=''){
            $res = Technician::findAllByAttributes(['=','mobile',$tecMobile],'id');
            if($res && $tecName != ''){
                $tecIds = array_intersect($tecIds,array_column($res, 'id'));
            }else{
                $tecIds = array_column($res, 'id');
            }
            if(empty($tecIds)){
                $tecIds = [-2];
            }
        }
        if($status!=''){
            $status = explode('_', $status);
        }
        if($accountId>0){
            $accountIds[] = $accountId;
        }

        if(trim($planStartTime) !=''){
            $planStartTime = strtotime($planStartTime);
        }
        if(trim($planEndTime) !=''){
            $planEndTime = strtotime($planEndTime);
        }
        //添加下单时间 条件搜索 2018-5-10 新增
        if(trim($createStartTime) !=''){
            $createStartTime = strtotime($createStartTime);
        }
        if(trim($createEndTime) !=''){
            $createEndTime = strtotime($createEndTime);
        }
        //添加服务完成时间 条件搜索 2018-10-17 新增
        if(trim($finishStartTime) !=''){
            $finishStartTime = strtotime($finishStartTime);
        }
        if(trim($finishEndTime) !=''){
            $finishEndTime = strtotime($finishEndTime);
        }
        //产品类目
        if($secondClassId>0){
            $saleOrderId = SaleOrderView::getIdsByClassId($secondClassId);
            if(!$saleOrderId){
                $saleOrderId = -1;
            }
        }
        else if($firstClassId>0){
            $saleOrderId = SaleOrderView::getIdsByClassId($firstClassId,true);
            if(!$saleOrderId){
                $saleOrderId = -1;
            }
        }
        if($classId){
            $saleOrderId = SaleOrderView::getIdsByClassId($classId);
            if(!$saleOrderId){
                $saleOrderId = -1;
            }

        }
        //服务地址
        $addressIds = AccountAddress::getIdsByRegion(self::getDepartmentId(),$provinceId,$cityId,$districtId);

        //数据权限查询
        $createUserId  = 0;
        $departmentIds = self::getDepartmentId();
		
		$datAuthId = self::getDatAuthId();
		//待指派
		if($status && (in_array(1,$status) || in_array(8,$status) ) && $datAuthId == 3){
			$datAuthId = 2;
		}
		//待验收
        else if($status && in_array(4,$status) && $datAuthId == 3){
            $datAuthId = 2;
        }
        //待收款
        else if($status && in_array(12,$status) && $datAuthId == 3){
            $datAuthId = 2;
        }
        //待服务
        else if($status && in_array(2,$status) && $datAuthId == 3){
            $datAuthId = 2;
        }
        //服务中
        else if($status && in_array(3,$status) && $datAuthId == 3){
            $datAuthId = 2;
        }

        //本人权限，只能查自已创建的订单
        if($datAuthId == 1){
            $createUserId = self::getLoginUserId();
        }
        //公司权限， 查自已公司及下属部门的订单
        else if($datAuthId == 2)
        {
            //如果是部门类型, 是查直属公司及以下所有部门订单
            $departmentObj = (new Department())->departmentObj;
            $tmpDepartmentId = self::getDepartmentId();

            if($departmentObj->type == 3)
            {
                $tmpDepartmentId = self::getDirectCompanyId();
                $childDepartments = Department::findAllByAttributes(['direct_company_id'=>$tmpDepartmentId],'id');
                $childDepartments = array_column($childDepartments,'id');
                array_push($childDepartments,$tmpDepartmentId);
                $departmentIds = array_unique($childDepartments);
            }
            else
            {
                $childDepartments = Department::getDepartmentSonIds($tmpDepartmentId,3,self::getDirectCompanyId());
                $childDepartments = array_unique($childDepartments);
                array_push($childDepartments,$tmpDepartmentId);
                $departmentIds = array_unique($childDepartments);
            }
        }
        //公司及以下权限，查出本公司及他下属部门及经销商的订单
        else if($datAuthId == 3)
        {
            //如果是部门类型, 是查直属公司及以下所有部门订单
            $departmentObj = (new Department())->departmentObj;
            $tmpDepartmentId = self::getDepartmentId();

            if($departmentObj->type == 3){
                $tmpDepartmentId = self::getDirectCompanyId();
            }

            $childDepartments = Department::getDepartmentSonIds($tmpDepartmentId,[1,3]);
            $childDepartments = array_unique($childDepartments);
            array_push($childDepartments,$tmpDepartmentId);
            $departmentIds = $childDepartments;
        }
        //本部门权限
        else if($datAuthId == 4)
        {
            $departmentObj = (new Department())->departmentObj;
            //如果是经销商类型，只能查自已的下单
            if($departmentObj->type == 1){
                $createUserId = self::getLoginUserId();
            }
            //如果是部门类型,可以查当前部门下的所有订单
            else
            {
                $departmentIds = self::getDepartmentId();
            }
        }
        //本部门及以下, 查出本部门及以下部门订单
        else if($datAuthId == 5)
        {
            $departmentObj = (new Department())->departmentObj;
            //如果是经销商类型，只能查自已的下单
            if($departmentObj->type == 1){
                $createUserId = self::getLoginUserId();
            }
            //如果是部门类型,可以查当前部门下的所有订单
            else
            {
                $childDepartments = Department::getDepartmentSonIds(self::getDepartmentId(),3);
                $childDepartments = array_unique($childDepartments);
                array_push($childDepartments,self::getDepartmentId());
                $departmentIds = $childDepartments;
            }
        }

        //搜索逻辑，如果搜索就用搜索的 department_id
        $orderDepartmentId = 0;
        if($searchDepartmentIds)
        {
            $searchDepartmentIds = explode(',',$searchDepartmentIds);

            $tmpDepartmentIds = $departmentIds;
            if(!is_array($tmpDepartmentIds)){
                $tmpDepartmentIds = [$tmpDepartmentIds];
            }
            if(!empty(array_diff($searchDepartmentIds,$tmpDepartmentIds))){
                $orderDepartmentId = -1;
            }
            else
            {
                $topDepartmentId = 0;
                if(($key = array_search(self::getDirectCompanyId(),$searchDepartmentIds)) !==false){
                    $topDepartmentId = $searchDepartmentIds[$key];
                    unset($searchDepartmentIds[$key]);
                }
                if($searchDepartmentIds){
                    $orderDepartmentId = Department::getSubDepartmentIds($searchDepartmentIds,[3]);
                }
                if($topDepartmentId){
                    if(is_array($orderDepartmentId)) {
                        $orderDepartmentId[] = $topDepartmentId;
                    }
                    else {
                        $orderDepartmentId = $topDepartmentId;
                    }
                }
            }
        }
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/search";
        $postData = [
            'order_no'          => $orderNo,
            'status'            => $status,
            'account_ids'       => $accountIds,
            'tec_ids'           => $tecIds,
            'work_status'      => $workStatus,
            'work_type'         => $workType,
            'sale_order_id'     => $saleOrderId,
            'plan_start_time'   => $planStartTime,
            'plan_end_time'     => $planEndTime,
            'create_start_time' => $createStartTime,
            'create_end_time'   => $createEndTime,
            'cancel_status'   => $cancelStatus,
            'page'            => $page,
            'pageSize'        => $pageSize,
            'department_id'   => $departmentIds,
            'visit_status'    => $visitStatus,
            'is_scope'        => $amountType,
            'address_ids'     => $addressIds,
            'create_user_id'  => $createUserId,
            'src_department_id' => self::getDepartmentId(),
            'order_department_id' => $orderDepartmentId,
            'contract_id'       => $contractId,  //合同关联id,
            'appraisal_status'  => $appraisalStatus,  //订单评价状态
            'finish_start_time' => $finishStartTime,
            'finish_end_time'   => $finishEndTime,
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {

            $list = $jsonArr['data']['list'];

            if($list)
            {
                //客户
                $accountArr = [];
                $accountIds = array_column($list, 'account_id');
                if($accountIds){
                    $accountArr = Account::findAllByAttributes(['id'=>$accountIds,'status'=>1],'id,account_name,mobile','id');
                }
                //服务地址
                $orderAddressIds = array_column($list, 'address_id');
                if($orderAddressIds){
                    $orderAddressArr = AccountAddress::getAddressByIds($orderAddressIds);
                }
                //服务类型
                $workTypeArr = self::getTypeList();
                $workTypeArr = $workTypeArr['data'];

                //服务产品
                $saleOrderIds = array_column($list, 'sale_order_id');
                $saleOrderArr = SaleOrderView::getSaleOrderInfoByIds($saleOrderIds);

                //服务机构
                $serviceDepartArr = array_filter(array_unique(array_column($list,'service_department_id')));

                //所属机构
                $departmentIds = array_unique(array_merge(array_column($list,'order_department_id'),$serviceDepartArr));
                $departmentIds = self::getRealDepartmentIds($departmentIds);
                $departmentArr = Department::findAllByAttributes(['id'=>$departmentIds],'id,name,type,direct_company_id','id');
                if($departmentArr)
                {
                    $directCompanyIds = array_column($departmentArr,'direct_company_id');
                    $directCompanyArr = Department::findAllByAttributes(['id'=>$directCompanyIds],'id,name,type,direct_company_id','id');

					foreach($directCompanyArr as $val){
						$departmentArr[$val['id']] = $val;
					}
                }
                //导出
                $newExportData = [];
                if($export == 1)
                {
                    $orderNos = array_column($list,'order_no');
                    //根据订单查询工单的详细导出数据信息
                    foreach($orderNos as $ka=>$va){
                        $resData = self::viewExport($va);
                        $newExportData = array_merge($newExportData,$resData);
                    }
                    //服务地址
                    $addressIds = array_column($list,'address_id');
                    $addressArr = AccountAddress::getAddressByIds($addressIds);
                    //下单人
                    $createUserIds = [];
                    $createUserArr = [];
                    foreach ($list as $val){
                        if($val['source'] != 4){
                            $createUserIds[$val['create_user_id']] = $val['create_user_id'];
                        }
                    }
                    if($createUserIds){
                        $createUserArr = User::getCreateNameByIds($createUserIds);
                    }

                    $orderNos = array_column($list,'order_no');
                    $orderAmountArr = self::getOrderAmount($orderNos);
                }
                if($from === 1){
                    $workInfo = Work::findAllByAttributes(['order_no'=>array_column($list,'order_no')],"order_no,work_no",'order_no',1000,"order_no,id");
                    $ordersLastWorks = array_column($workInfo,"work_no","order_no");

                    $technician = WorkRelTechnician::getTechnicianInfo($ordersLastWorks);

                    $newArray = array_column($technician,NULL,'work_no');

                    $technicianInfo = Technician::findAllByAttributes(['id'=>array_column($newArray,'technician_id')],"id,name,mobile","id");

                    foreach ($newArray as $key=>$val){
                        $newArray[$key]['technician_name'] = '';
                        $newArray[$key]['technician_mobile'] = '';

                        if(isset($technicianInfo[$val['technician_id']])){
                            $newArray[$key]['technician_name'] = $technicianInfo[$val['technician_id']]['name'];
                            $newArray[$key]['technician_mobile'] = $technicianInfo[$val['technician_id']]['mobile'];
                        }
                    }

                    foreach ($workInfo as $k=>$v){
                        $workInfo[$k]['technician_name'] = '';
                        $workInfo[$k]['technician_mobile'] = '';
                        if(isset($newArray[$v['work_no']])){
                            $workInfo[$k]['technician_name'] = $newArray[$v['work_no']]['technician_name'];
                            $workInfo[$k]['technician_mobile'] = $newArray[$v['work_no']]['technician_mobile'];
                        }
                    }
                }else{
                    $workInfo = [];
                }

                $selfOrderStatusArr = self::curlGetOrderStatus(array_column($list,'order_no'));
                //合并数据
                foreach ($jsonArr['data']['list'] as $key=>$val)
                {
                    if(isset($workInfo[$val['order_no']])){
                        $jsonArr['data']['list'][$key]['technician_name'] = $workInfo[$val['order_no']]['technician_name'];
                        $jsonArr['data']['list'][$key]['technician_mobile'] = $workInfo[$val['order_no']]['technician_mobile'];
                    }

                    $jsonArr['data']['list'][$key]['isShowBtn'] = 1;
                    if((isset($selfOrderStatusArr[$val['order_no']]) && $selfOrderStatusArr[$val['order_no']] != $val['source_status']) || ($status && !in_array($val['source_status'],$status))){
						$jsonArr['data']['list'][$key]['isShowBtn']= 0;
                    }
                    //客户
                    if( isset($accountArr[$val['account_id']]) ){
                        $accountName  = $accountArr[$val['account_id']]['account_name'];
                        $accountMobile = $accountArr[$val['account_id']]['mobile'];
                    }
                    //服务地址
                    if( isset($orderAddressArr[$val['address_id']]) ){
                        $jsonArr['data']['list'][$key]['address'] = $orderAddressArr[$val['address_id']];
                    }
                    //服务类型
                    if(isset($workTypeArr[$val['work_type']])){
                        $workTypeDesc = $workTypeArr[$val['work_type']];
                    }
                    //服务产品
                    if(isset($saleOrderArr[$val['sale_order_id']])){
                        $prodId  = $saleOrderArr[$val['sale_order_id']]['prod_id'];
                        $prodDesc = $saleOrderArr[$val['sale_order_id']]['prod_name'];
                        $prodClassName = $saleOrderArr[$val['sale_order_id']]['class_name'];
                        $brandName = $saleOrderArr[$val['sale_order_id']]['brand_name'];
                    }
                    //所属机构
                    if(isset($departmentArr[$val['order_department_id']])){
                        $departmentName = $departmentArr[$val['order_department_id']]['name'];
                        $departmentType = $departmentArr[$val['order_department_id']]['type'];
                        $directCompanyId= $departmentArr[$val['order_department_id']]['direct_company_id'];

                        if(isset($departmentArr[$directCompanyId])){
                            $directCompanyType = $departmentArr[$directCompanyId]['type'];
                        }
                    } 
					else if(isset($departmentArr[$val['direct_company_id']])){
                        $departmentName = $departmentArr[$val['direct_company_id']]['name'];
                        $departmentType = $departmentArr[$val['direct_company_id']]['type'];
                        $directCompanyId= $departmentArr[$val['direct_company_id']]['direct_company_id'];

                        if(isset($departmentArr[$directCompanyId])){
                            $directCompanyType = $departmentArr[$directCompanyId]['type'];
                        }
                    } 
                    //服务机构
                    $serviceDeparName = '';
                    if( isset($departmentArr[$val['service_department_id']])){
                        $serviceDeparName = $departmentArr[$val['service_department_id']]['name'];
                    }

                    $jsonArr['data']['list'][$key]['account_name']   = $accountName;
                    $jsonArr['data']['list'][$key]['account_mobile'] = $accountMobile;
                    $jsonArr['data']['list'][$key]['work_type_desc'] = $workTypeDesc;
                    if($val['plan_time'] != 0 && $val['plan_time'] != null){
                        $jsonArr['data']['list'][$key]['plan_time']      = date('Y-m-d H:i',$val['plan_time']);
                    }else{
                        $jsonArr['data']['list'][$key]['plan_time']      = 0;
                    }
                    $jsonArr['data']['list'][$key]['prod_desc']           = $prodDesc;
                    $jsonArr['data']['list'][$key]['prod_id']             = $prodId;
                    $jsonArr['data']['list'][$key]['prod_class_name']     = $prodClassName;
                    $jsonArr['data']['list'][$key]['brand_name']          = $brandName;
                    $jsonArr['data']['list'][$key]['department_name']     = $departmentName;
                    $jsonArr['data']['list'][$key]['department_type']     = $departmentType;
                    $jsonArr['data']['list'][$key]['direct_company_id']   = $directCompanyId;
                    $jsonArr['data']['list'][$key]['direct_company_type'] = $directCompanyType;
                    $jsonArr['data']['list'][$key]['service_depar_name']  = $serviceDeparName;

                    //导出用
                    if($export == 1)
                    {
                        $jsonArr['data']['list'] = $newExportData;
                        /*$jsonArr['data']['list'][$key]['address_desc']     = '';
                        $jsonArr['data']['list'][$key]['create_user_desc'] = '';
                        $jsonArr['data']['list'][$key]['order_amount']     = '';
                        $jsonArr['data']['list'][$key]['class_desc']       = '';
                        $jsonArr['data']['list'][$key]['brand_name']       = '';
                        $jsonArr['data']['list'][$key]['serial_number']    = '';

                        if(isset($addressArr[$val['address_id']])){
                            $jsonArr['data']['list'][$key]['address_desc'] = $addressArr[$val['address_id']];
                        }
                        if(isset($saleOrderArr[$val['sale_order_id']])){
                            $jsonArr['data']['list'][$key]['class_desc']    = implode(' - ',array_filter([$saleOrderArr[$val['sale_order_id']]['class_name'] ]));
                            $jsonArr['data']['list'][$key]['brand_name']    = $saleOrderArr[$val['sale_order_id']]['brand_name'];
                            $jsonArr['data']['list'][$key]['serial_number'] = $saleOrderArr[$val['sale_order_id']]['serial_number'];
                        }
                        if(isset($createUserArr[$val['create_user_id']])){
                            $jsonArr['data']['list'][$key]['create_user_desc'] = $createUserArr[$val['create_user_id']];
                        }
                        if(isset($orderAmountArr[$val['order_no']])){
                            $jsonArr['data']['list'][$key]['order_amount'] = sprintf('%.2f',$orderAmountArr[$val['order_no']]);
                        }
                        $jsonArr['data']['list'][$key]['create_time'] = date('Y-m-d H:i:s',$val['create_time']);*/

                    }
                }
            }
            return self::success($jsonArr['data']);
        }
        else {
            return self::error("接口：$url 没有返回数据");
        }
    }

    /**
     * 任务大厅
     * @author liuxingqi <lxq@c-ntek.com>
     * @return int
     */
    public static function taskHall($has = 0)
    {
        //初始参数
        $page            = intval(Yii::$app->request->get('page',1));
        $pageSize        = intval(Yii::$app->request->get('pageSize',10));
        $workType        = intval(Yii::$app->request->get('work_type',0));
        $serviceTimeType = intval(Yii::$app->request->get('service_time_type',0));
        $planStartTime   = intval(Yii::$app->request->get('plan_start_time',0));
        $planEndTime     = intval(Yii::$app->request->get('plan_end_time',0));
        $createStartTime = Yii::$app->request->get('create_start_time','');  //下单开始时间  2018-5-10 新增
        $createEndTime   = Yii::$app->request->get('create_end_time','');    //下单结束时间 2018-5-10 新增


        $firstClassId     = intval(Yii::$app->request->get('first_class_id',0));
        $secondClassId    = intval(Yii::$app->request->get('second_class_id',0));
       
        //产品类型
        $saleOrderId = [];
        //产品类目
        if($secondClassId>0){
            $saleOrderId = SaleOrder::getIdsByClassId($secondClassId);
            if(!$saleOrderId){
                $saleOrderId = -1;
            }
        }
        else if($firstClassId>0){
            $saleOrderId = SaleOrder::getIdsByClassId($firstClassId);
            if(!$saleOrderId){
                $saleOrderId = -1;
            }
        }

        //服务时间
        if(trim($planStartTime) !=''){
            $planStartTime = strtotime($planStartTime);
        }
        if(trim($planEndTime) !=''){
            $planEndTime = strtotime($planEndTime);
        }
        //添加下单时间 条件搜索 2018-5-10 新增
        if(trim($createStartTime) !=''){
            $createStartTime = strtotime($createStartTime);
        }
        if(trim($createEndTime) !=''){
            $createEndTime = strtotime($createEndTime);
        }

        $serviceProvideId = self::getDepartmentId();
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/task-hall";
        $postData = [
            'work_type'           => $workType,
            'sale_order_id'       => $saleOrderId,
            'plan_start_time'     => $planStartTime,
            'plan_end_time'       => $planEndTime,
            'create_start_time' => $createStartTime,
            'create_end_time'   => $createEndTime,
            'page'                => $page,
            'pageSize'            => $pageSize,
            'service_time_type'   => $serviceTimeType,
            'has_service_provide' => $has,
            'service_provide_id'  => $serviceProvideId,
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            $list = $jsonArr['data']['list'];
            if( $list )
            {
                //服务类型
                $workTypeArr = self::getTypeList();
                $workTypeArr = $workTypeArr['data'];
                //服务产品
                $saleOrderIds = array_column($list, 'sale_order_id');
                $saleOrderArr = SaleOrder::findAllByAttributes(['id'=>$saleOrderIds,'status'=>1],'id,prod_name,brand_name,class_name,type_name','id');
                //客户地址
                $accountAddressArr = [];
                $accountAddressIds = array_column($list, 'address_id');
                if($accountAddressIds){
                    $accountAddressArr = AccountAddress::findAllByAttributes(['id'=>$accountAddressIds,'status'=>1],'id,address','id');
                }
                //下单人
                $createUser = [];
                foreach ($list as $key =>$val) {
                    $createUser[$val['src_type']][] = $val['create_user_id'];
                }
                if ($createUser) {
                    $createUserArr = self::getCreateUser($createUser);
                }

                //合并数据 
                foreach ($jsonArr['data']['list'] as $key=>$val)
                {
                    $address = '';
                    $serviceContent = '';
                    $orderPrice = 0;
                    $createUserName = '';
                    //客户地址
                    if( isset($accountAddressArr[$val['address_id']]) ){
                        $address = $accountAddressArr[$val['address_id']]['address'];
                    }
                    
                    //服务类型

                    if(isset($workTypeArr[$val['work_type']])){
                        $workTypeDesc = $workTypeArr[$val['work_type']];
                    }

                    //服务内容
                    if(isset($saleOrderArr[$val['sale_order_id']])){
                        $serviceContent = $saleOrderArr[$val['sale_order_id']]['brand_name'].'-'.$saleOrderArr[$val['sale_order_id']]['class_name'].'-'.$workTypeDesc;
                    }
                    
                    //下单人
                    if(isset($createUserArr[$val['src_type']][$val['create_user_id']])){
                        $createUserName = $createUserArr[$val['src_type']][$val['create_user_id']];
                    }
                    $jsonArr['data']['list'][$key]['service_time_desc'] = $val['service_time_type'] ==1 ? '立即上门' : '预约上门';
                    $jsonArr['data']['list'][$key]['plan_time'] = Order::getPlantimeType($val['plan_time_type'],date('Y-m-d H:i',$val['plan_time']));
                    $jsonArr['data']['list'][$key]['address']   = $address;
                    $jsonArr['data']['list'][$key]['service_content'] = $serviceContent;
                    $jsonArr['data']['list'][$key]['order_price'] = $orderPrice;
                    $jsonArr['data']['list'][$key]['create_user_name'] = $createUserName;
                }
            }
            return self::success($jsonArr['data']);
        }else {
            return self::error("接口：$url 没有返回数据");
        }
        return $jsonArr;
    }

    /**
     * 取消订单 
     * @author xi
     * @date 2017-12-18
     */
    public static function cancel()
    {
        $orderNo      = trim(Yii::$app->request->get('order_no',''));
        $cancelReason = trim(Yii::$app->request->get('cancel_reason',''));
        //是谁取消的这个订单
        $processUserId= Yii::$app->user->getId();
        //取消订单来原 14商家,13服务商 12门店 20用户
        $srcType      = BaseModel::SRC_FWS;

        $url = Yii::$app->params['order.uservices.cn']."/v1/order/cancel";
        $postData = [
            'order_no'        => $orderNo,
            'cancel_reason'   => $cancelReason,
            'process_user_id' => $processUserId,
            'department_id'   => self::getDepartmentId(),
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        if(isset($result['success']) && $result['success'] == 1 )
        {
            return self::success($result['data']);
        }
        else {
            return self::error($result['message'],$result['code']);
        }
    }

    /**
     * 取消订单
     * @author xi
     * @date 2017-12-18
     */
    public static function ignore()
    {
        $orderNo      = trim(Yii::$app->request->get('order_no',''));
        $ignoreReason = trim(Yii::$app->request->get('ignore_reason',''));
        //是谁取消的这个订单
        $processUserId= Yii::$app->user->getId();//@todo 改成真实的
        //取消订单来原 14商家,13服务商 12门店 20用户
        $srcType      = BaseModel::SRC_FWS;

        $url = Yii::$app->params['order.uservices.cn']."/v1/order/ignore";
        $postData = [
            'order_no'        => $orderNo,
            'ignore_reason'   => $ignoreReason,
            'process_user_id' => $processUserId,
            'src_type'        => $srcType,
            'src_id'          => self::getDepartmentId()
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        if(isset($result['success']) && $result['success'] == 1 ){
            return self::success($result['data']);
        }
        else {
            return self::error($result['message'],$result['code']);
        }
    }
    
    /**
     * 新建订单
     * @author xi
     * @date 207-12-18
     */
    public static function add()
    {
        $accountId    = intval(Yii::$app->request->post('account_id',0));
        $orderList    = Yii::$app->request->post('orderList',[]);
        $addressId    = intval(Yii::$app->request->post('address_id',0));
        $workStage     = Yii::$app->request->post('work_stage',0);
        $workType     = Yii::$app->request->post('work_type',0);
        $is_scope    = Yii::$app->request->post('is_scope',0);
        $assign       = intval(Yii::$app->request->post('assign',''));   //指派给机构1  技师2
        $judge        = intval(Yii::$app->request->post('judge',0));
        $contract_id       = intval(Yii::$app->request->post("contract_id",0));
        $subject_name        = Yii::$app->request->post("subject_name",'');
        $order_type       = intval(Yii::$app->request->post("order_type",0)); //下单类型 1合同派单 2 智邦拉取
        //指派机构的id
        $serviceDepartId  = intval(Yii::$app->request->post('service_depart_id',0));
        $payType          = Yii::$app->request->post('pay_type',3);
        $description      = trim(Yii::$app->request->post('description',''));
        $createUserId     = Yii::$app->user->getId();
        $amount           = Yii::$app->request->post('amount',0);
        $orderForm        = Yii::$app->request->post("OrderForm",[]);

        //服务时间拼接
        $selectDate   =  Yii::$app->request->post("selectDate",'');
        $planTimeType =  Yii::$app->request->post("plan_time_type",'');
        $setTime      =  Yii::$app->request->post("setTime",'');
        $planTime     =  Helper::changeTime($planTimeType,$selectDate,$setTime);
        //合同派单时 对比产品数量不能超过合同中的数量
        if($order_type == 1 && !empty($orderList)){
            $sale_arr = array_column($orderList,'product_id');
            //获取合同下产品的数量
            $detail = ContractLogic::getDetail($contract_id);
            $detail_num_arr = array_column($detail['data']['detail'],'prod_num','id');
            $res = SaleOrder::findAllByAttributes(['contract_id'=>$contract_id,'id'=>$sale_arr]);
            $res_contra = [];
            foreach($res as $key=>$val){
                if(array_key_exists($val['contract_detail_id'],$detail_num_arr)){
                    $res_contra[$key]['id'] = $val['id'];
                    $res_contra[$key]['maxnum'] = $detail_num_arr[$val['contract_detail_id']];
                }
            }
            $contra = array_column($res_contra,'maxnum','id');
           // 对比提交的数量是否符合
            foreach ($orderList as $val){
                if(array_key_exists($val['product_id'],$contra)){
                   if($val['num'] > $contra[$val['product_id']]){ // 下单数量大于合同数量
                       return self::error("下单数量大于合同数量");
                   }
                }
            }
        }
        //服务流程为空时，根据服务类型获取默认的流程
        if((empty($workStage) || $workStage == '' || $workStage == null)  && $workType){
            $workStageData = FlowLogic::getFlowStage($workType,1);
            $workStage = isset($workStageData['data'][0])?$workStageData['data'][0]['id']:'';
        }
        $faultImg = '';
        if ($orderForm) {
             $faultImg = implode(',', $orderForm);
        }
        //去重处理
        if($judge == 2){ //拆单
            foreach($orderList as $key => $val) {
                if(is_array($val)){
                    $tmpArray = array();
                    foreach ($val as $row) {
                        if(isset($row['product_id']) && $row['product_id']){
                            $k = $row['product_id'];
                            if (array_key_exists($k, $tmpArray)) {
                                $tmpArray[$k]['num'] = $tmpArray[$k]['num'] + $row['num'];
                            } else {
                                $tmpArray[$k] = $row;
                            }
                        }
                    }
                }
                $orderList[$key] = $tmpArray;
            }
        }else{
                $tmpArray = array();
                foreach ($orderList as $row) {
                    if(isset($row['product_id']) && $row['product_id']){
                        $k = $row['product_id'];
                        if (array_key_exists($k, $tmpArray)) {
                            $tmpArray[$k]['num'] = $tmpArray[$k]['num'] + $row['num'];
                        } else {
                            $tmpArray[$k] = $row;
                        }
                    }
                }
                $orderList = $tmpArray;

        }
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/add";

        //2018-9-26 产品需求：系统下单 创建人为登陆人 部门为登陆人部门
        $useId = self::getLoginUserId();
        $departmentId = self::getDepartmentId();
        //根据客户获取机构
        if($order_type == 2)
        {
            //modify liquan 2018-9-14  产品需求：订单所属更改为销售人员所属部门
            $jfArr = Account::findOneByAttributes(['id'=>$accountId],"department_id,direct_company_id,create_user");
            $useId = $jfArr['create_user'];
            $departmentId = $jfArr['department_id'];
        }


        $postData = [
            'account_id'         => $accountId,
            'orderList'          => $orderList,
            'address_id'         => $addressId,
            'contract_id'        => $contract_id,
            'subject_name'       => $subject_name,
            'work_type'          => $workType,
            'work_stage'         => $workStage,
            'is_scope'           => $is_scope,
            'plan_time_type'     => $planTimeType,
            'plan_time'          => $planTime,
            'pay_type'           => $payType,
            'description'        => $description,
            'operator_user_id'   => $createUserId,
            'create_user_id'     => $useId,
            'department_id'      => $departmentId,
            'service_depart_id'  => $serviceDepartId,
            'amount'             => $amount,
            'fault_img'          => $faultImg,
            'standard_id'        => 0,
            'standard_type'      => 1,
            'standard_type_id'   => $departmentId,
            'source'             => 1,
            'judge'              => $judge
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        if(isset($result['success']) && $result['success'] == 1 )
        {
            $result['data']['assign'] = $assign;
            return self::success($result['data']);
        }
        else {
            return self::error($result['message'],$result['code']);
        }
    }
    
    /**
     * 抢单
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function getOrder()
    {
        $orderNo = trim(Yii::$app->request->post('order_no',''));
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/service-provider-order";
        $postData = [
            'order_no'        => $orderNo,
            'src_type'        => BaseModel::SRC_FWS,
            'src_id'          => self::getDepartmentId(),
            'process_user_id' => self::getLoginUserId()
        ];
        
        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        if (isset($result['success']) && $result['success'] == 1 ) {
            return self::success('','恭喜您，抢单成功！快去安排技师为业主上门服务吧');
        }
        return self::error($result['message']);
    }

    /**
     * 订单详情
     * @author xi
     * @date 2017-12-18
     */
    public static function view()
    {
        $orderNo = trim(Yii::$app->request->get('order_no',''));

        $url = Yii::$app->params['order.uservices.cn']."/v1/order/detail";
        $postData = [
            'order_no'               => $orderNo,
            'direct_company_id'      => BaseLogic::getDirectCompanyId(),
            'department_id'          => BaseLogic::getOrderSourceId($orderNo,$type=3),
            'service_department_id'  => WorkOrderAssign::hasDepartment($orderNo,BaseLogic::getDepartmentId())?BaseLogic::getDepartmentId():0,
            'src_department_id'      => self::getDepartmentId(),
            'show_work'=> 1
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);

        $warranty        = SaleOrderLogic::warranty();
        //计量单位数组
        $unit_arr  = ProductLogic::getUnit();
        $saleOrderArrNum = 0;
        if(isset($result['success']) && $result['success'] == 1 )
        {
            $data = $result['data'];
            if($data)
            {
                //查询合同编号
                $data['contract_no'] = '';
                $info = Contract::getOne(['id'=>$data['contract_id']]);
                if($info){
                    $data['contract_no'] = $info['no'];
                }
                $accountName   = '';
                $accountMobile = '';
                $workTypeDesc  = '';
                
                $accountArr = Account::findOneByAttributes(['id'=>$data['account_id']],'id,account_name,mobile');
                if($accountArr)
                {
                    $accountName   = $accountArr['account_name'];
                    $accountMobile = $accountArr['mobile'];
                }
                
                //服务类型 
                $workTypeArr = self::getTypeList();
                if(isset($workTypeArr['data'][$data['work_type']])){
                    $workTypeDesc = $workTypeArr['data'][$data['work_type']];
                }
                //服务产品
                $saleOrderIds = array_column($data['workDetail'], 'sale_order_id');
                $saleOrderArr = SaleOrderView::findAllByAttributes(['id'=>$saleOrderIds],'id,prod_id,prod_name,prod_name_wechat,product_type_id,brand_name,class_name,type_name,warranty_type,serial_number,info','id');
                //服务产品计量单位id数组
                $saleOrderProdIds = array_column($saleOrderArr, 'prod_id');
                $saleOrderUnitArr = Product::findAllByAttributes(['id'=>$saleOrderProdIds],'id,unit_id','id');
                $saleOrderUnitArr = array_column($saleOrderUnitArr, 'unit_id','id');
                if(!$saleOrderArr){
                    return self::success([]);
                }
                foreach ($data['workDetail'] as $k=>$v){
                    $data['workDetail'][$k]['prod_info'] = $saleOrderArr[$v['sale_order_id']];
                    $data['workDetail'][$k]['prod_info']['unit_id'] = isset($saleOrderUnitArr[$data['workDetail'][$k]['prod_info']['prod_id']])?$saleOrderUnitArr[$data['workDetail'][$k]['prod_info']['prod_id']]:'';
                    $unit_name = isset($unit_arr[$data['workDetail'][$k]['prod_info']['unit_id']])?$unit_arr[$data['workDetail'][$k]['prod_info']['unit_id']]:'';
                    $data['workDetail'][$k]['prod_info']['unit_num_name'] = $v['sale_order_num'].$unit_name;
                }
                $saleOrderProdArr = array_column($data['workDetail'], 'prod_info');
                $saleOrderArrNum  = count($saleOrderProdArr);
                $saleOrderNewArr  = array_slice($data['workDetail'], 0, 5);
                if($saleOrderNewArr)
                {
                    foreach ($saleOrderNewArr as $kk=>$vv){
                        if(!$vv['prod_info']['prod_name'] && $vv['prod_info']['prod_name_wechat']){
                            $saleOrderNewArr[$kk]['prod_info']['prod_name'] = $vv['prod_info']['prod_name_wechat'];
                        }
                    }
                }

                //产品信息数据
                $saleOrderIds = array_column($data['workDetail'], 'sale_order_id');
                $productArr = SaleOrderView::findAllByAttributes(['id'=>$saleOrderIds],'id,prod_id,intention_type,prod_name,prod_name_wechat,product_type_id,brand_name,brand_name_wechat,class_name,prod_series,serial_number,produce_time,buy_time,warranty,code,warranty_num,warranty_type,is_scope,scope_img,source','id');
                $productIds = array_column($productArr,'prod_id','prod_id');
                $productRawArr = Product::findAllByAttributes(['id'=>$productIds],'id,prod_no,model','id');

                $saleOrderNumArr = array_column($data['workDetail'], 'sale_order_num','sale_order_id');
                $scopeArr = array_column($data['workDetail'], 'is_scope','sale_order_id');
                $scopeImgArr = array_column($data['workDetail'], 'scope_img_arr','sale_order_id');
                if($productArr){
                    foreach ($productArr as $key=>$val)
                    {
                        foreach ($productRawArr as $k2=>$v2)
                        {
                            if($val['prod_id'] == $k2)
                            {
                                $productArr[$key]['prod_no'] = $v2['prod_no'];
                                $productArr[$key]['model'] = $v2['model'];
                            }
                        }
                        $productArr[$key]['sale_order_num'] = isset($saleOrderNumArr[$val['id']])?$saleOrderNumArr[$val['id']]:0;
                        $productArr[$key]['is_scope'] = isset($scopeArr[$val['id']])?$scopeArr[$val['id']]:'';
                        $productArr[$key]['scope_img'] = isset($scopeImgArr[$val['id']])?$scopeImgArr[$val['id']]:[];

                    }
                }
                //获取被技师编辑过的产品信息数据
                $productEditArr = SaleOrderSn::findAllByAttributes(['sale_order_id'=>$saleOrderIds,'status'=>1,'order_no'=>$orderNo],'id,sale_order_id,sn,remark,extend_data','id');
                $productNewArr = [];
                if($productArr)
                {
                    //重组数
                    $productSnArr = [];
                    if($productEditArr){
                        foreach ($productEditArr as $val){
                            $productSnArr [$val['sale_order_id']][] = $val;
                        }
                    }
                    foreach ($productArr as $val)
                    {
                        $val['warranty'] = isset($warranty['data'][$val['warranty_type']])?$val['warranty_num'].$warranty['data'][$val['warranty_type']]:'';

                        $totalNum = $val['sale_order_num'];
                        if(isset($productSnArr[$val['id']]))
                        {
                            $snNum    = count($productSnArr[$val['id']]);
                            $count = $snNum;
                            if($totalNum > $snNum){
                                $count = $snNum+1;
                            }

                            $tempSnProductArr =  $productSnArr[$val['id']];
                            for($i = 0; $i<$count; $i++)
                            {
                                if(isset($tempSnProductArr[$i]))
                                {
                                    $productNewArr [] = [
                                        'sn'      => $tempSnProductArr[$i],
                                        'product' => $val
                                    ];
                                }
                                else
                                {
                                    $productNewArr [] = [
                                        'sn'      => [],
                                        'product' => $val
                                    ];
                                }
                            }
                        }
                        else
                        {
                           $tempSn = [];
                           if(isset($saleOrderArr[$val['id']])){
                               $tmpSaleOrderArr = $saleOrderArr[$val['id']];
                               $tempSn = [
                                   'sn' => $tmpSaleOrderArr['serial_number'],
                                   'remark' => $tmpSaleOrderArr['info']
                               ];
                           }
                            $productNewArr [] = [
                                'sn'      => $tempSn,
                                'product' => $val
                            ];
                        }
                    }
                }
                //服务内容
                $serviceContent = '';
                /*if(isset($saleOrderArr[$saleOrderIds[0]])){
                    //$serviceContent = $saleOrderArr[$saleOrderIds[0]]['brand_name'].'-'.$saleOrderArr[$saleOrderIds[0]]['prod_name'].'-'.$workTypeDesc;
                }*/
                //组织工单项目内的收费项目详情
                $data['work_arr_list'] = [];

                if($data['workArr'])
                {
                    if(isset($data['workArr'][1])){
                        //工单存在二次上门的时候
                        $data['is_reject'] = '1';  //不能驳回
                    }else{
                        $data['is_reject'] = '2';  //能驳回
                    }

                    foreach ($data['workArr'] as $k=>$v)
                    {
                        if($v['work_cost']) foreach ($v['work_cost'] as $key=>$val)
                        {
                            $cost_item_info = CostItemSet::find()->where(['id'=>$val['cost_id']])->select('name')->asArray()->one();
                            $cost_pay_info  = CostPayType::getOne(['id'=>$val['payer_id']]);
                            if($val['cost_type'] == 2){
                                $val['work_cost_name'] = "配件费";
                            }else{
                                $val['work_cost_name'] = isset($cost_item_info['name'])?$cost_item_info['name']:'';
                            }
                            $val['cost_payer']     = isset($cost_pay_info['type_name'])?$cost_pay_info['type_name']:'';
                            $v['work_cost'][$key]  = $val;
                        }

                        $data['workArr'][$k]['work_cost'] = $v['work_cost'];

                        //技师信息
                        $technician_info = [];
                        //技师信息
                        $jsIds = [];
                        if(isset($v['technicianArr']) && $v['technicianArr']){
                            foreach ($v['technicianArr'] as $jVal){
                                $jsIds[] = $jVal['technician_id'];
                            }
                        }
                        if($jsIds)
                        {
                            $technician_info = Common::getTechniciansInfo($jsIds,'id,name,mobile,icon,store_id');
                            if($technician_info)
                            {

                                //获取技师的所属服务商
                                $new_t = $technician_info;
                                $first_technician_info = array_shift($new_t);
                                $technician_store_id = isset($first_technician_info['store_id'])?$first_technician_info['store_id']:'';
                                $data['workArr'][$k]['technician_store_id'] = $technician_store_id;
                                foreach ($v['technicianArr'] as $key=>$jVal)
                                {
                                    $avatar = $name = $mobile = '';
                                    if(isset($technician_info[$jVal['technician_id']]))
                                    {

                                        $name   = $technician_info[$jVal['technician_id']]['name'];
                                        $mobile = $technician_info[$jVal['technician_id']]['mobile'];
                                        $avatar = $technician_info[$jVal['technician_id']]['icon'];
                                    }
                                    $title = '协助';
                                    if($jVal['is_self'] == 1){
                                        $title = '负责人';
                                    }
                                    else if($jVal['is_self'] == 2){
                                        $title = '组长';
                                    }

                                    $data['workArr'][$k]['technicianArr'][$key]['name']   = $name;
                                    $data['workArr'][$k]['technicianArr'][$key]['mobile'] = $mobile;
                                    $data['workArr'][$k]['technicianArr'][$key]['avatar'] = trim($avatar)==''?'/images/morentouxiang.png':$avatar;
                                    $data['workArr'][$k]['technicianArr'][$key]['title']  = $title;
                                }
                            }
                            else
                            {
                                foreach ($v['technicianArr'] as $key=>$jVal)
                                {
                                    $data['workArr'][$k]['technicianArr'][$key]['name']   = '';
                                    $data['workArr'][$k]['technicianArr'][$key]['mobile'] = '';
                                    $data['workArr'][$k]['technicianArr'][$key]['avatar'] = '/images/morentouxiang.png';
                                    $data['workArr'][$k]['technicianArr'][$key]['title']  = '';
                                }
                            }
                        }
                        //工单预约时间
                        $data['workArr'][$k]['work_plan_time'] = Order::getPlantimeType($v['plan_time_type'],date('Y-m-d H:i',$v['plan_time']));
                        $tmp[$k] = $v['create_time'];
                    }
                    array_multisort($tmp,SORT_DESC,$data['workArr']);
                }
                $accountAddressArr = AccountAddress::findOneByAttributes(['id'=>$data['address_id']]);

                //联系地址
                $addressDesc = Region::getAddress($data['address_id'], $type = 1);
                $data['account_name']   = $accountName;
                $data['account_mobile'] = $accountMobile;
                $data['plan_time'] = Order::getPlantimeType($data['plan_time_type'],date('Y-m-d H:i',$data['plan_time']));
                $data['create_time']    = date('Y-m-d H:i',$data['create_time']);
                $data['word_type_desc'] = $workTypeDesc;
                $data['address_desc']   = isset($addressDesc['address'])?$addressDesc['address']:'';
                $data['conact_name']    = $accountAddressArr?$accountAddressArr['conact_name']:'';
                $data['conact_phone']   = $accountAddressArr?$accountAddressArr['mobile_phone']:'';
                $data['amount']         = $data['amount']/100;
                $userInfo = User1::getDirectCompany($data['create_user_id']);
                $data['create_user']    = $userInfo['username'];
                $data['direct_company_name']  = $userInfo['name'];
                if($data['source'] == 4){
                    $miniInfo = Account::findOne(['id'=>$data['create_user_id']]);
                    $data['create_user']    = $miniInfo['account_name'].' '.$miniInfo['mobile'];
                    $miniDepInfo = Department::getOne(['id'=>$miniInfo['department_id']]);
                    $data['direct_company_name']  = isset($miniDepInfo['name'])?$miniDepInfo['name']:'';
                }
                $data['direct_company_id']    = $userInfo['direct_company_id'];
                $data['direct_company_type']  = $userInfo['type'];
                $data['work_type_desc']       = $workTypeDesc;
                $data['service_content']      = $serviceContent;
                $data['sale_order_new_arr']   = $saleOrderNewArr;
                $data['sale_order_arr']       = $saleOrderArr;
                $data['sale_order_arr_num']   = $saleOrderArrNum;
                $data['product_new_arr']      = $productNewArr;
                $data['technician_store_id']  = isset($technician_store_id)?$technician_store_id:'';
            }
            $data['DatAuthId'] = self::getDatAuthId();
            return self::success($data);
        }
        else {
            return self::error($result['message'],$result['code']);
        }
    }

    /**
     * @param string $order_no
     * @return array
     * 导出工单数据组合
     */
    public static function viewExport($order_no='')
    {
        $orderNo = trim(Yii::$app->request->get('order_no',''));
        if(!$orderNo){
            $orderNo = $order_no;
        }
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/detail";
        $postData = [
            'order_no'               => $orderNo,
            'direct_company_id'      => BaseLogic::getDirectCompanyId(),
            'department_id'          => BaseLogic::getOrderSourceId($orderNo,$type=3),
            'service_department_id'  => WorkOrderAssign::hasDepartment($orderNo,BaseLogic::getDepartmentId())?BaseLogic::getDepartmentId():0,
            'src_department_id'      => self::getDepartmentId(),
            'show_work'=> 1
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        $warranty        = SaleOrderLogic::warranty();
        //计量单位数组
        $unit_arr  = ProductLogic::getUnit();
        $paymentMethodlist = ['0'=>'微信','1'=>'支付宝','3'=>'现金'];
        $saleOrderArrNum = 0;
        if(isset($result['success']) && $result['success'] == 1 )
        {
            $productNewArr = [];
            $data = $result['data'];
            if($data)
            {
                unset($data['process']);
                //查询合同编号
                $data['contract_no'] = '';
                $info = Contract::getOne(['id'=>$data['contract_id']]);
                if($info){
                    $data['contract_no'] = $info['no'];
                }
                $accountName   = '';
                $accountMobile = '';
                $workTypeDesc  = '';

                $accountArr = Account::findOneByAttributes(['id'=>$data['account_id']],'id,account_name,mobile');
                if($accountArr)
                {
                    $accountName   = $accountArr['account_name'];
                    $accountMobile = $accountArr['mobile'];
                }

                //服务类型
                $workTypeArr = self::getTypeList();
                if(isset($workTypeArr['data'][$data['work_type']])){
                    $workTypeDesc = $workTypeArr['data'][$data['work_type']];
                }
                //服务产品
                $saleOrderIds = array_column($data['workDetail'], 'sale_order_id');

                $saleOrderArr = SaleOrderView::findAllByAttributes(['id'=>$saleOrderIds,'status'=>1],'id,prod_id,prod_name,prod_name_wechat,product_type_id,brand_name,class_name,type_name,warranty_type,serial_number,info','id');
                //服务产品计量单位id数组
                $saleOrderProdIds = array_column($saleOrderArr, 'prod_id');
                $saleOrderUnitArr = Product::findAllByAttributes(['id'=>$saleOrderProdIds],'id,unit_id','id');
                $saleOrderUnitArr = array_column($saleOrderUnitArr, 'unit_id','id');
                if(!$saleOrderArr){
                    //return self::success([]);
                }
                /*foreach ($data['workDetail'] as $k=>$v){
                    $data['workDetail'][$k]['prod_info'] = $saleOrderArr[$v['sale_order_id']];
                    $data['workDetail'][$k]['prod_info']['unit_id'] = isset($saleOrderUnitArr[$data['workDetail'][$k]['prod_info']['prod_id']])?$saleOrderUnitArr[$data['workDetail'][$k]['prod_info']['prod_id']]:'';
                    $unit_name = isset($unit_arr[$data['workDetail'][$k]['prod_info']['unit_id']])?$unit_arr[$data['workDetail'][$k]['prod_info']['unit_id']]:'';
                    $data['workDetail'][$k]['prod_info']['unit_num_name'] = $v['sale_order_num'].$unit_name;
                }
                $saleOrderProdArr = array_column($data['workDetail'], 'prod_info');
                $saleOrderArrNum  = count($saleOrderProdArr);
                $saleOrderNewArr  = array_slice($data['workDetail'], 0, 5);
                if($saleOrderNewArr)
                {
                    foreach ($saleOrderNewArr as $kk=>$vv){
                        if(!$vv['prod_info']['prod_name'] && $vv['prod_info']['prod_name_wechat']){
                            $saleOrderNewArr[$kk]['prod_info']['prod_name'] = $vv['prod_info']['prod_name_wechat'];
                        }
                    }
                }*/

                //产品信息数据
                $saleOrderIds = array_column($data['workDetail'], 'sale_order_id');
                $productArr = SaleOrderView::findAllByAttributes(['id'=>$saleOrderIds],'id,prod_id,intention_type,prod_name,prod_name_wechat,product_type_id,brand_name,brand_name_wechat,class_name,prod_series,serial_number,produce_time,buy_time,warranty,code,warranty_num,warranty_type,is_scope,scope_img,source','id');
                $productIds = array_column($productArr,'prod_id','prod_id');
                $productRawArr = Product::findAllByAttributes(['id'=>$productIds],'id,prod_no,model','id');

                $saleOrderNumArr = array_column($data['workDetail'], 'sale_order_num','sale_order_id');
                $scopeArr = array_column($data['workDetail'], 'is_scope','sale_order_id');
                $scopeImgArr = array_column($data['workDetail'], 'scope_img_arr','sale_order_id');
                if($productArr){
                    foreach ($productArr as $key=>$val)
                    {
                        foreach ($productRawArr as $k2=>$v2)
                        {
                            if($val['prod_id'] == $k2)
                            {
                                $productArr[$key]['prod_no'] = $v2['prod_no'];
                                $productArr[$key]['model'] = $v2['model'];
                            }
                        }
                        $productArr[$key]['sale_order_num'] = isset($saleOrderNumArr[$val['id']])?$saleOrderNumArr[$val['id']]:0;
                        $productArr[$key]['is_scope'] = isset($scopeArr[$val['id']])?$scopeArr[$val['id']]:'';
                        $productArr[$key]['scope_img'] = isset($scopeImgArr[$val['id']])?$scopeImgArr[$val['id']]:[];

                    }
                }
                //获取被技师编辑过的产品信息数据
                $productEditArr = SaleOrderSn::findAllByAttributes(['sale_order_id'=>$saleOrderIds,'status'=>1,'order_no'=>$orderNo],'id,sale_order_id,sn,remark','id');
                $productNewArr = [];
                if($productArr)
                {
                    //重组数
                    $productSnArr = [];
                    if($productEditArr){
                        foreach ($productEditArr as $val){
                            $productSnArr [$val['sale_order_id']][] = $val;
                        }
                    }
                    foreach ($productArr as $val)
                    {
                        $val['sale_order_id'] = $val['sn'] = $val['remark'] = '';
                        $val['warranty'] = isset($warranty['data'][$val['warranty_type']])?$val['warranty_num'].$warranty['data'][$val['warranty_type']]:'';

                        $totalNum = $val['sale_order_num'];
                        if(isset($productSnArr[$val['id']]))
                        {
                            $snNum    = count($productSnArr[$val['id']]);
                            $count = $snNum;
                            if($totalNum > $snNum){
                                $count = $snNum+1;
                            }

                            $tempSnProductArr =  $productSnArr[$val['id']];
                            for($i = 0; $i<$count; $i++)
                            {
                                if(isset($tempSnProductArr[$i]))
                                {
                                    $val['sale_order_id'] = isset($tempSnProductArr[$i]['sale_order_id'])?$tempSnProductArr[$i]['sale_order_id']:'';
                                    $val['sn'] = isset($tempSnProductArr[$i]['sn'])?$tempSnProductArr[$i]['sn']:'';
                                    $val['remark'] = isset($tempSnProductArr[$i]['remark'])?$tempSnProductArr[$i]['remark']:'';
                                    $productNewArr [] = $val;
                                }
                                else
                                {
                                    $productNewArr [] = $val;
                                }
                            }
                        }
                        else
                        {
                            $tempSn = [];
                            if(isset($saleOrderArr[$val['id']])){
                                $tmpSaleOrderArr = $saleOrderArr[$val['id']];
                                $tempSn = [
                                    'sn' => $tmpSaleOrderArr['serial_number'],
                                    'remark' => $tmpSaleOrderArr['info']
                                ];
                            }
                            $val['sale_order_id'] = isset($tempSn['sale_order_id'])?$tempSn['sale_order_id']:'';
                            $val['sn'] = isset($tempSn['sn'])?$tempSn['sn']:'';
                            $val['remark'] = isset($tempSn['remark'])?$tempSn['remark']:'';
                            $productNewArr [] = $val;
                        }
                    }
                }
                //服务内容
                $serviceContent = '';
                //组织工单项目内的收费项目详情
                $data['work_arr_list'] = [];

                if($data['workArr'])
                {
                    if(isset($data['workArr'][1])){
                        //工单存在二次上门的时候
                        $data['is_reject'] = '1';  //不能驳回
                    }else{
                        $data['is_reject'] = '2';  //能驳回
                    }
                    foreach ($data['workArr'] as $k=>$v)
                    {
                        if($v['work_cost']) foreach ($v['work_cost'] as $key=>$val)
                        {
                            $cost_item_info = CostItemSet::find()->where(['id'=>$val['cost_id']])->select('name')->asArray()->one();
                            $cost_pay_info  = CostPayType::getOne(['id'=>$val['payer_id']]);
                            $val['work_cost_name'] = isset($cost_item_info['name'])?$cost_item_info['name']:'';
                            $val['cost_payer']     = isset($cost_pay_info['type_name'])?$cost_pay_info['type_name']:'';
                            $val['cost_info']      = $val['work_cost_name'].'：'.$val['cost_real_amount'];
                            $v['work_cost'][$key]  = $val;
                        }
                        $data['workArr'][$k]['work_cost'] = $v['work_cost'];

                        //技师信息
                        $technician_info = [];
                        //技师信息
                        $jsIds = [];
                        if(isset($v['technicianArr']) && $v['technicianArr']){
                            foreach ($v['technicianArr'] as $jVal){
                                $jsIds[] = $jVal['technician_id'];
                            }
                        }

                        if($jsIds)
                        {
                            $technician_info = Common::getTechniciansInfo($jsIds,'id,name,mobile,icon');
                            if($technician_info)
                            {
                                foreach ($v['technicianArr'] as $key=>$jVal)
                                {
                                    $avatar = $name = $mobile = '';
                                    if(isset($technician_info[$jVal['technician_id']]))
                                    {
                                        $name   = $technician_info[$jVal['technician_id']]['name'];
                                        $mobile = $technician_info[$jVal['technician_id']]['mobile'];
                                        $avatar = $technician_info[$jVal['technician_id']]['icon'];
                                    }
                                    $title = '协助';
                                    if($jVal['is_self'] == 1){
                                        $title = '负责人';
                                    }
                                    else if($jVal['is_self'] == 2){
                                        $title = '组长';
                                    }

                                    $data['workArr'][$k]['technicianArr'][$key]['name']   = $name;
                                    $data['workArr'][$k]['technicianArr'][$key]['mobile'] = $mobile;
                                    $data['workArr'][$k]['technicianArr'][$key]['avatar'] = trim($avatar)==''?'/images/morentouxiang.png':$avatar;
                                    $data['workArr'][$k]['technicianArr'][$key]['title']  = $title;
                                }
                            }
                            else
                            {
                                foreach ($v['technicianArr'] as $key=>$jVal)
                                {
                                    $data['workArr'][$k]['technicianArr'][$key]['name']   = '';
                                    $data['workArr'][$k]['technicianArr'][$key]['mobile'] = '';
                                    $data['workArr'][$k]['technicianArr'][$key]['avatar'] = '/images/morentouxiang.png';
                                    $data['workArr'][$k]['technicianArr'][$key]['title']  = '';
                                }
                            }
                        }
                        //工单预约时间
                        $data['workArr'][$k]['work_plan_time'] = Order::getPlantimeType($v['plan_time_type'],date('Y-m-d H:i',$v['plan_time']));
                        $tmp[$k] = $v['create_time'];
                    }
                    array_multisort($tmp,SORT_DESC,$data['workArr']);
                }
                $accountAddressArr = AccountAddress::findOneByAttributes(['id'=>$data['address_id']]);
                //print_r($data['workArr']);die;
                //联系地址
                $addressDesc = Region::getAddress($data['address_id'], $type = 1);
                $data['account_name']   = $accountName;
                $data['account_mobile'] = $accountMobile;
                $data['plan_time'] = Order::getPlantimeType($data['plan_time_type'],date('Y-m-d H:i',$data['plan_time']));
                $data['create_time']    = date('Y-m-d H:i',$data['create_time']);
                $data['word_type_desc'] = $workTypeDesc;
                $data['address_desc']   = isset($addressDesc['address'])?$addressDesc['address']:'';
                $data['conact_name']    = $accountAddressArr?$accountAddressArr['conact_name']:'';
                $data['conact_phone']   = $accountAddressArr?$accountAddressArr['mobile_phone']:'';
                $data['amount']         = $data['amount']/100;
                $userInfo = User1::getDirectCompany($data['create_user_id']);
                $data['create_user']    = $userInfo['username'];
                $data['direct_company_name']  = $userInfo['name'];
                if($data['source'] == 4){
                    $miniInfo = Account::findOne(['id'=>$data['create_user_id']]);
                    $data['create_user']    = $miniInfo['account_name'].' '.$miniInfo['mobile'];
                    $miniDepInfo = Department::getOne(['id'=>$miniInfo['department_id']]);
                    $data['direct_company_name']  = isset($miniDepInfo['name'])?$miniDepInfo['name']:'';
                }
                $data['direct_company_id']    = $userInfo['direct_company_id'];
                $data['direct_company_type']  = $userInfo['type'];
                $data['work_type_desc']       = $workTypeDesc;
                $data['service_content']      = $serviceContent;
                $data['sale_order_new_arr']   = isset($saleOrderNewArr)?$saleOrderNewArr:[];
                $data['sale_order_arr']       = $saleOrderArr;
                $data['sale_order_arr_num']   = $saleOrderArrNum;
                $data['product_new_arr']      = $productNewArr;
                $data['department_name']  = Department::getDepartmentName($data['department_id']);
            }
            $data['DatAuthId'] = self::getDatAuthId();
            //重新再次组合数据
            $newDataArr = [];
            $amount_type = BaseModel::getAmountType();
            if($productNewArr){
                foreach ($data['workArr'] as $k1=>$v1){
                    $total_cost_amount = array_sum(array_column($v1['work_cost'],'cost_amount'));
                    $total_cost_rel_amount = array_sum(array_column($v1['work_cost'],'cost_real_amount'));
                    $technicianInfos = implode(',',array_values(array_column($v1['technicianArr'],'name')));
                    $work_type_desc = $data['work_type_desc'];
                    $work_stage_desc = $v1['work_stage_desc'];
                    $finsh_service_time = '';
                    if($v1['finsh_service_time']){
                        $finsh_service_time = date('Y-m-d H:i',$v1['finsh_service_time']);
                    }
                    //收费项目合并
                    $costArrList = array_column($v1['work_cost'],'cost_info');
                    $costArrData = isset($costArrList)?implode(',',$costArrList):'';
                    foreach ($productNewArr as $k=>$v){
                        $v['order_no']  = $data['order_no'];
                        $v['work_no']   = $v1['work_no'];
                        $v['direct_company_name']  = $data['direct_company_name'];
                        $v['department_name']  = isset($data['department_name'])?$data['department_name']:'';
                        $v['account_name']  = $data['account_name'];
                        $v['account_mobile']  = $data['account_mobile'];
                        $v['technicianInfos']  = $technicianInfos;  //服务技师
                        $v['address_desc']  = $data['address_desc'];  //服务地址
                        $v['work_type_desc']  = $work_type_desc;  //服务类型
                        $v['work_stage_desc']  = $work_stage_desc;  //服务流程
                        $v['status_desc']  = $v1['status_desc'];
                        $v['cost_list']  = $costArrData;  //收费项目
                        $v['total_cost_amount']  = $total_cost_amount;  //总计金额
                        $v['total_cost_rel_amount']  = $total_cost_rel_amount;  //实收金额
                        $v['amount_type_name']  = isset($amount_type[$v['is_scope']])?$amount_type[$v['is_scope']]:'';
                        $v['plan_time']  = date('Y-m-d H:i',$v1['plan_time']); //预约时间
                        $v['finsh_service_time']  = $finsh_service_time;  //服务完成时间
                        $v['create_user']  = $data['create_user'];  //下单人
                        $v['create_time']  = $data['create_time'];  //下单时间
                        $v['description']  = $data['description'];  //备注
                        $v['pay_type_name']  = isset($paymentMethodlist[$v1['pay_type']])?$paymentMethodlist[$v1['pay_type']]:'';  //备注
                        $v['service_department_id'] = 1;
                        $newDataArr[]  = $v;
                    }
                }
            }
            return $newDataArr;
            //return self::success($data);
        }
        else {
            return self::error($result['message'],$result['code']);
        }
    }

    /**
     * 查看服务产品列表
     * @author sxz
     * @date 2018-7-24
     */
    public static function viewAllProduct()
    {
        $orderNo = trim(Yii::$app->request->get('order_no',''));
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/detail";
        $postData = [
            'order_no'               => $orderNo,
            'direct_company_id'      => BaseLogic::getDirectCompanyId(),
            'department_id'          => BaseLogic::getOrderSourceId($orderNo,$type=3),
            'service_department_id'  => WorkOrderAssign::hasDepartment($orderNo,BaseLogic::getDepartmentId())?BaseLogic::getDepartmentId():0,
            'src_department_id'      => self::getDepartmentId(),
            'show_work'=> 1
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        $warranty        = SaleOrderLogic::warranty();
        //计量单位数组
        $unit_arr  = ProductLogic::getUnit();
        if(isset($result['success']) && $result['success'] == 1 ){
            $data = $result['data'];
            if($data)
            {
                //服务产品
                $saleOrderIds = array_column($data['workDetail'], 'sale_order_id');
                $saleOrderArr = SaleOrderView::findAllByAttributes(['id'=>$saleOrderIds,'status'=>1],'id,prod_id,prod_name,brand_name,class_name,type_name,warranty_type','id');
                //服务产品计量单位id数组
                $saleOrderProdIds = array_column($saleOrderArr, 'prod_id');
                $saleOrderUnitArr = Product::findAllByAttributes(['id'=>$saleOrderProdIds],'id,unit_id','id');
                $saleOrderUnitArr = array_column($saleOrderUnitArr, 'unit_id','id');
                foreach ($data['workDetail'] as $k=>$v){
                    $data['workDetail'][$k]['prod_info'] = $saleOrderArr[$v['sale_order_id']];
                    $data['workDetail'][$k]['prod_info']['unit_id'] = isset($saleOrderUnitArr[$data['workDetail'][$k]['prod_info']['prod_id']])?$saleOrderUnitArr[$data['workDetail'][$k]['prod_info']['prod_id']]:'';
                    $unit_name = isset($unit_arr[$data['workDetail'][$k]['prod_info']['unit_id']])?$unit_arr[$data['workDetail'][$k]['prod_info']['unit_id']]:'';
                    $data['workDetail'][$k]['prod_info']['unit_num_name'] = $v['sale_order_num'].$unit_name;
                }
                $data = $data['workDetail'];
            }
            return self::success($data);
        }
        else {
            return self::error($result['message'],$result['code']);
        }
    }
    
    /**
     * 获取订单状态
     * @return mixed
     */
    public static function getOrderStatus()
    {
        $data = [
            1 => '待指派',
            2 => '待服务',
            3 => '服务中',
            4 => '待验收',
            5 => '已完成',
            6 => '已取消',
            7 => '待审核',
            8 => '未通过'

        ];
        return $data;
    }
    
    /**
     * 根据订单id获取订单信息
     * @author  li
     * @date    2017-12-25
     */
    public static function getOrderInfo($orderNo)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/detail";
        $postData = [
            'order_no' => $orderNo,
            'src_type' => BaseModel::SRC_FWS,
            'src_id'   => self::getDepartmentId()
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);

        if(isset($result['success']) && $result['success'] == 1 ) {

            $orderData = $result['data'];

            //根据订单信息查询订单客户信息
            $account   = Account::findOneByAttributes(['id' => $orderData['account_id']]);
            //根据订单信息查询订单地址信息
            $address   = AccountAddress::findOneByAttributes(['id' => $orderData['address_id']]);
            //根据订单信息查询订单指定服务商
            $url = Yii::$app->params['order.uservices.cn']."/v1/order/detail";
            $postData = [
                'order_no' => $orderData['order_no'],
                'src_type' => BaseModel::SRC_FWS,
                'src_id'   => self::getDepartmentId()
            ];
            $jsonStr = Helper::curlPostJson($url, $postData);
            $orderDetail  = json_decode($jsonStr,true);
            //$provider  = Stores::getOne(['id' => $orderDetail['data']['workDetail'][0]['service_provide_id']]);
            //数据拼接
            $orderData['account']  = $account;
            $orderData['address']  = $address;
            //$orderData['provider'] = $provider;
            $orderData['detail']   = $orderDetail;
            return $orderData;
        }
    }

    /**
     * 获取售后产品id
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $workClass 类目id
     * @return boolean
     */
    public static function getSaleOrder($workClass)
    {
        $classIds = [];
        $res = self::getClassList($workClass);
        if ($res['success'] == true) {
            $classIds = array_keys($res['data']);
        }
        $saleOrder = SaleOrder::find()
                ->select(['id'])
                ->andWhere(['in','class_id',$classIds])
                ->asArray()
                ->all();
        if ($saleOrder) {
            $saleOrderIds = array_column($saleOrder, 'id');
            return $saleOrderIds;
        }
        return false;
    }

    /**
     * 获取下单人
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $createUser
     * @return type
     */
    public static function getCreateUser($createUser)
    {
        $createUserArr = [];
        //门店
        if (isset($createUser[BaseModel::SRC_MD])) {
            $createUserArr[BaseModel::SRC_MD] = \webapp\models\Manufactor::getCompany($createUser[BaseModel::SRC_MD]);            
        }
        //商家
        if (isset($createUser[BaseModel::SRC_SJ])) {
            //$createUserArr[BaseModel::SRC_SJ] = \webapp\models\Manufactor::getCompany($createUser[BaseModel::SRC_SJ]);
            $createUserArr[BaseModel::SRC_SJ] = User::getCreateNames($createUser[BaseModel::SRC_SJ]);       //获取下单人的名字

        }
        //服务商
        if (isset($createUser[BaseModel::SRC_FWS])) {
            $createUserArr[BaseModel::SRC_FWS] = \webapp\models\Manufactor::getCompany($createUser[BaseModel::SRC_FWS]);            
        }
        //用户
        if (isset($createUser[20])) {
            $createUserArr[20] = \webapp\models\AdminUser::adminInfo($createUser[20]);            
        }
        return $createUserArr;
    }
    
    /**
     * 订单指派
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function assign()
    {
        $orderNo      = trim(Yii::$app->request->get('order_no',''));
        $groupingId   = intval(Yii::$app->request->get('grouping_id',0));
        $range        = intval(Yii::$app->request->get('range',0));
        $skill        = intval(Yii::$app->request->get('skill',0));
        $jobtitle     = intval(Yii::$app->request->get('jobtitle',1));
        $plantimeDate = trim(Yii::$app->request->post('plan_time_date',''));
        $plantimeHour = trim(Yii::$app->request->post('plan_time_hour',''));
        $plantimetype = intval(Yii::$app->request->post('plan_time_type',5));
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/detail";
        $departmentId     = self::getDepartmentId();
        $sourceId         = BaseLogic::getOrderSourceId($orderNo,$type=3);
        $postData = [
            'order_no'          => $orderNo,
            'src_type'          => BaseModel::SRC_FWS,
            'department_id'     => $sourceId,
            'show_work'         =>1

        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        if(isset($result['success']) && $result['success'] == 1 )
        {
            $data = $result['data'];

            $addressId = $data['address_id'];
            $addressArr = AccountAddress::findOneByAttributes(['id'=>$addressId],'lon,lat');

            $title = '';
            $saleOrderId = $data['workDetail'][0]['sale_order_id'];
            $saleOrderArr = SaleOrderView::findOneByAttributes(['id'=>$saleOrderId]);
            if($saleOrderArr){
                $title = $saleOrderArr['brand_name'] . $saleOrderArr['class_name'] . Common::getWorktypeName($data['work_type']);
            }
            $plantime = date('Y-m-d H:i:s',$data['plan_time']);

            if($plantimetype>0 && $plantimeDate!='' && $plantimeHour!=''){
                $plantime =  Helper::changeTime($plantimetype,$plantimeDate,$plantimeHour);
            }

            $departmentId = self::getDepartmentId();

            $jsList       = GroupingRelTechnician::searchTechnician($groupingId,$data['work_type'],$saleOrderId,$addressId,$plantime,$jobtitle,$range,$skill,$departmentId);
            return self::success([
                'work'       => $data,
                'jsList'     => $jsList,
                'addressArr' => $addressArr,
                'title'      => $title
            ]);

        }
        return self::error('未找到数据');
    }

    //获取工单数据
    public static function getPositions($workNo)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/detail";
        $postData = [
            'work_no'  => $workNo,
            'src_type' => BaseModel::SRC_FWS,
            'src_id'   => self::getDepartmentId()
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        $data = [];
        if(isset($result['success']) && $result['success'] == 1 ) {
            $orderData = $result['data'];
            if($orderData){
                //客户地址
                $address = Region::getAddress($orderData['order_info']['address_id'],$type = 1);
                //服务类型
                $workTypeArr = self::getTypeList();
                if(isset($workTypeArr['data'][$orderData['order_info']['work_type']])){
                    $workTypeDesc = $workTypeArr['data'][$orderData['order_info']['work_type']];
                }
                //服务产品
                $saleOrderIds = $orderData['order_info']['sale_order_id'];
                $saleOrderArr = SaleOrder::findAllByAttributes(['id'=>$saleOrderIds,'status'=>1],'id,prod_name,brand_name,class_name,type_name','id');
                //服务内容
                $serviceContent = '';
                if(isset($saleOrderArr[$saleOrderIds])){
                    $workTypeDesc = isset($workTypeDesc) ? $workTypeDesc : '';
                    $serviceContent = $saleOrderArr[$saleOrderIds]['brand_name'].'-'.$saleOrderArr[$saleOrderIds]['class_name'].'-'.$workTypeDesc;
                }
                $technician_id = isset($orderData['technician_id'])?$orderData['technician_id']:'';
                $data['technician_id'] = $technician_id;
                $data['service_content'] = $serviceContent;
                $data['service_time_desc'] = $orderData['order_info']['service_time_type'] == 1 ? '立即上门' : '预约上门';
                $data['plan_time'] =date('Y-m-d H:i:s', $orderData['order_info']['plan_time']);
                $data['address'] = $address['address'];
                $data['lon'] = $address['lon'];
                $data['lat'] = $address['lat'];
            }
        }
        return $data;
    }

    /**
     * 获取地址经纬度
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $orderNo
     * @return type
     */
    public static function getPosition($orderNo)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/detail";
        $postData = [
            'order_no'      => $orderNo,
            'department_id' => self::getDepartmentId()
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        $data = [];
        if(isset($result['success']) && $result['success'] == 1 ) {
            $orderData = $result['data'];
            //客户地址
            $address = Region::getAddress($orderData['address_id'],$type = 1);
            //服务类型
            $workTypeArr = self::getTypeList();
            $workTypeArr = $workTypeArr['data'];
            
            //服务产品
            $saleOrderIds = array_column($orderData['workDetail'], 'sale_order_id');
            $saleOrderArr = SaleOrderView::findAllByAttributes(['id'=>$saleOrderIds,'status'=>1],'id,prod_name,brand_name,class_name,type_name','id');

            //服务内容
            $serviceContent = '';
            if(isset($saleOrderArr[$saleOrderIds[0]])){
                $serviceContent = $saleOrderArr[$saleOrderIds[0]]['brand_name'].'-'.$saleOrderArr[$saleOrderIds[0]]['class_name'].'-'.$workTypeArr[$orderData['work_type']];
            }
            $technicianId = array_column($orderData['workDetail'], 'technician_id');
            $data['technician_id'] = isset($technicianId[0])?$technicianId[0]:'';
            $data['service_content'] = $serviceContent;
            $data['plan_time'] = date('Y-m-d H:i');
            $data['address'] = $address['address'];
            $data['lon'] = $address['lon'];
            $data['lat'] = $address['lat'];
        }
        return $data;
    }

    /**
     * 拒绝接单
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function reject()
    {
        $orderNo      = trim(yii::$app->request->get('order_no',''));
        $refuseReason = trim(Yii::$app->request->get('reject_reason',''));

        $url = Yii::$app->params['order.uservices.cn']."/v1/order/reject";
        $postData = [
            'order_no'        => $orderNo,
            'department_id'   => self::getDepartmentId(),
            'process_user_id' => yii::$app->user->getIdentity()->id,
            'reject_reason'   => $refuseReason
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        if ($result['success'] == false) {
            return $result;
        }
        return self::success('','驳回成功');
    }

    /**
     * 指派订单操作
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function assignTechnician()
    {
        $orderNo       = trim(Yii::$app->request->post('order_no',''));
        $workNo        = trim(Yii::$app->request->post('work_no',''));
        $plantimeDate  = trim(Yii::$app->request->post('plan_time_date',''));
        $plantimeHour  = trim(Yii::$app->request->post('plan_time_hour',''));
        $plantimetype  = intval(Yii::$app->request->post('plan_time_type',5));
        $type          = intval(Yii::$app->request->post('type',1)); //1 多技师 2组长
        $technicianId  = Yii::$app->request->post('technician_id');
        $leader        = intval(Yii::$app->request->post('leader',0)); //只有多技师的情况下才有用
        $groupid       = intval(Yii::$app->request->post('groupid',0));
        if(!$technicianId){
            return self::error('请选择'.($type == 2?'组长':'技师'));
        }
        if($type == 2 && GroupingRelTechnician::hasGroupingLeader($technicianId)){
            $technicianName = Common::getTechnicianName($technicianId);
            return self::error('技师【'.$technicianName.'】职位已修改为普通技师，请重新指派！',20020);
        }
        if($type == 1 && ($technicianName = GroupingRelTechnician::hasTechnicaianChangeJobTitle($technicianId)) != ''){
            return self::error('技师【'.$technicianName.'】职位已修改为组长，请重新指派！',20020);
        }
        if(($technicianName = GroupingRelTechnician::getDisableTechnicaianName($technicianId)) !=''){
            return self::error('技师【'.$technicianName.'】已移除该组，请重新指派！',20020);
        }
        if($groupid>0 && ($technicianName = GroupingRelTechnician::getChangeOrRemoveGrouping($technicianId,$groupid)) != ''){
            return self::error('技师【'.$technicianName.'】已移除该组，请重新指派！',20020);
        }
        if(GroupingRelTechnician::hasSameGrouping($technicianId) == false){
            return self::error('只能指派同一组的技师',20020);
        }

        $plantime = Helper::changeTime($plantimetype,$plantimeDate,$plantimeHour);

            if($orderNo){
                //自动生成工单开始
                $url = Yii::$app->params['order.uservices.cn']."/v1/order/detail";

                $postData = [
                    'order_no' => trim($orderNo),
                    'department_id' => self::getOrderSourceId($orderNo,3),
                    'show_work'=> 1
                ];

                $jsonStr = Helper::curlPostJson($url, $postData);
                $jsonArr  = json_decode($jsonStr,true);


                if($jsonArr['success'] == 1){
                    //已有工单
                    $workList = $jsonArr['data']['workArr'];
                    if(!empty($workList)){
                        foreach($workList as $v){
                            if($v['status']==1){ //待指派
                                $workNo = $v['work_no'];
                                break;
                            }
                        }
                    }else{
                        //创建工单开始
                        $orderDetail = $jsonArr['data'];
                        $workStage = $orderDetail['work_stage'];

                        $url = Yii::$app->params['order.uservices.cn']."/v1/work/add";
                        $workData = [
                            'department_id'        => self::getDepartmentId(),
                            'order_no'             => $orderNo,
                            'work_stage'           => $workStage,
                            'plan_time'            => $orderDetail['plan_time'],
                            'plan_time_type'       => $orderDetail['plan_time_type'],
                            'work_type'            => $type,
                            'assign_plantime_type' => 1,
                            'process_user_id' => self::getLoginUserInfo()->getId(),
                            //'reason'=>$orderDetail['reason'],
                            // 'reason_detail'=>$orderDetail['reason_detail'],
                        ];
                        $jsonStr = Helper::curlPostJson($url, $workData);
                        $workArr  = json_decode($jsonStr,true);
                        if($workArr['success']==1){
                            $workNo = $workArr['data']['work_no'];
                        }else{
                            return  self::error($workArr['message']);
                        }
                    }
                    //自动生成工单结束


            }
                if(!$workNo){
                    return self::error('该订单已指派');
                }

                //请求指派接口
                $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/assign";
                $assigmData = [
                    'order_no'          => $orderNo,
                    'work_no'           => $workNo,
                    'department_top_id' => self::getTopId(),
                    'department_id'     => self::getDepartmentId(),
                    'store_id'          => self::getDepartmentId(),
                    'op_user_type'      => BaseModel::SRC_FWS,
                    'process_user_id'   => self::getLoginUserId(),
                    'technician_id'     => $technicianId,
                    'plantime'          => $plantime,
                    'plan_time_type'    => $plantimetype,
                    'technician_id'     => $technicianId,
                    'technician_type'   => $type,
                    'technician_leader' => $leader
                ];

                $jsonStr = Helper::curlPostJson($url, $assigmData);
                $result  = json_decode($jsonStr,true);

                if ($result['success'] == 1) {
                    return $result;
                }else{
                       return self::error($result['message']);
                }
                //调取队列处理排班逻辑
                $tid = $technicianId;
                if(!is_array($technicianId)){
                    $tid = [$technicianId];
                }
                QueuePush::sendPush('increase','tech-schedule','increase',[$tid,2,self::getTopId(), self::getDepartmentId()]);

                return self::success('','指派成功');

        }else{
                return self::error('该订单不存在');

            }


    }

    /**
     * 改派技师
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function reAssignTechnician()
    {
        $workNo       = trim(Yii::$app->request->post('work_no', ''));
        $orderNo      = trim(Yii::$app->request->post('order_no', ''));
        $technicianId = Yii::$app->request->post('technician_id');
        $plantimeDate = trim(Yii::$app->request->post('plan_time_date', ''));
        $plantimeHour = trim(Yii::$app->request->post('plan_time_hour', ''));
        $plantimetype = intval(Yii::$app->request->post('plan_time_type', 5));
        $reason       = trim(Yii::$app->request->post('reason', ''));
        $type         = intval(Yii::$app->request->post('type', 1)); //1 多技师 2组长
        $leader       = intval(Yii::$app->request->post('leader', 0)); //只有多技师的情况下才有用
        $groupid      = intval(Yii::$app->request->post('groupid', 0));

        if (!$technicianId) {
            return self::error('请选择' . ($type == 2 ? '组长' : '技师'));
        }
        if ($type == 2 && GroupingRelTechnician::hasGroupingLeader($technicianId)) {
            $technicianName = Common::getTechnicianName($technicianId);
            return self::error('技师【' . $technicianName . '】职位已修改为普通技师，请重新指派！', 20020);
        }
        if ($type == 1 && ($technicianName = GroupingRelTechnician::hasTechnicaianChangeJobTitle($technicianId)) != '') {
            return self::error('技师【' . $technicianName . '】职位已修改为组长，请重新指派！', 20020);
        }
        if (($technicianName = GroupingRelTechnician::getDisableTechnicaianName($technicianId)) != '') {
            return self::error('技师【' . $technicianName . '】已移除该组，请重新指派！', 20020);
        }
        if ($groupid > 0 && ($technicianName = GroupingRelTechnician::getChangeOrRemoveGrouping($technicianId, $groupid)) != '') {
            return self::error('技师【' . $technicianName . '】已移除该组，请重新指派！', 20020);
        }
        if (GroupingRelTechnician::hasSameGrouping($technicianId) == false) {
            return self::error('只能指派同一组的技师', 20020);
        }

        $plantime = Helper::changeTime($plantimetype, $plantimeDate, $plantimeHour);

        /****，改派时若已完成第一个工单，指派/改派都只能在公司内部指派改派*，若该工单时第一个工单，机构指派时可跨公司指派**/

                $url = Yii::$app->params['order.uservices.cn'] . "/v1/work-pc/re-assign";
                $postData = [
                            'order_no'          => $orderNo,
                            'work_no'          => $workNo,
                            'department_top_id' => self::getTopId(),
                            'department_id'     => self::getDepartmentId(),
                            'process_user_id'   => self::getLoginUserId(),
                            'technician_id'     => $technicianId,
                            'plantime'          => $plantime,
                            'plan_time_type'    => $plantimetype,
                            'reason'            => $reason,
                            'op_user_type'      => BaseModel::SRC_FWS,
                            'technician_type'   => $type,
                            'technician_leader' => $leader
                ];

                $jsonStr = Helper::curlPostJson($url, $postData);
                $result  = json_decode($jsonStr, true);


                if ($result['success'] == true) {

                    //成功后 调用对列处理排班
                    $tid = $technicianId;
                    if (!is_array($technicianId)) {
                        $tid = [$technicianId];
                    }
                    //QueuePush::sendPush('reduce','tech-schedule','reduce',[$tid, self::getTopId(),self::getDepartmentId()]);//原技师id
                   // QueuePush::sendPush('increase','tech-schedule','increase',[$tid,2,self::getTopId(),self::getDepartmentId()]);//改派的技师id

                    return self::success('', '改派成功');
                }

             return self::error(isset($result['message'])?$result['message']:'改派失败');
            //return self::error('改派失败');

    }

    /**
     * 验证订单是否已被指派
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $orderNo
     * @param type $type 0、为可指派   1、为不可指派
     * @return type
     */
    public static function checkTechnician($orderNo,$type)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/has-order-assign";
        $postData = [
            'order_no' => $orderNo,
            'type'     => $type,
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        return $result;
    }
    
    /**
     * 推送消息
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $orderNo
     * @param type $technicianId
     * @return string
     */
    public static function pushMsg($orderNo,$technicianId)
    {
        $data = self::getDetail($orderNo);
        //服务产品
        $saleOrderIds = array_column($data['workDetail'], 'sale_order_id');
        $saleOrderArr = SaleOrder::findAllByAttributes(['id'=>$saleOrderIds,'status'=>1],'id,class_name,type_name','id');
        
        //服务内容
        $className = '';
        if(isset($saleOrderArr[$saleOrderIds[0]])){
            $className = $saleOrderArr[$saleOrderIds[0]]['class_name'];
        }
        //服务类型
        $typeName = '';
        if(isset($saleOrderArr[$saleOrderIds[0]])){
            $typeName = $saleOrderArr[$saleOrderIds[0]]['type_name'];
        }
        $plan_time = date('Y-m-d H:i:s',$data['plan_time']);
        //技师id
        $technicianIds = array_column($data['workDetail'], 'technician_id');
        
        $oldTechnicianId = end($technicianIds);
        $title = '您有新的订单！';
        $content = '['.$plan_time.']['.$className.']['.$typeName.']订单派给您啦！提前安排时间噢！';
        $type = '1';
        $type_content = $orderNo;
        $order_status = $data['status'];
        $Assign = Push::pushMsg($title, $content, $oldTechnicianId, $type, $type_content, $order_status);
        $AssignArr = json_decode($Assign,true);
        if ($AssignArr['success'] == false) {
            return $AssignArr;
        }
        
        if ($technicianId > 0) {
            $title = '订单被改派了！';
            $content = '['.$plan_time.']['.$className.']['.$typeName.']订单被改派了，请查看！';
            $reAssign = Push::pushMsg($title, $content, $technicianId, $type, $type_content, $order_status);
            $reAssignArr = json_decode($reAssign,true);
            if ($reAssignArr['success'] == false) {
                return $reAssignArr;
            }
        }
        return $AssignArr;
    }

    /**
     * 指派工单推送消息
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $orderNo
     * @param type $technicianId
     * @return string
     */
    public static function pushMsgs($workNo,$technicianId)
    {
        //$data = self::getDetail($orderNo);
        $data = WorkLogic::getWorkDetails($workNo);
        $className = isset($data['data']['className'])?$data['data']['className']:'';
        $typeName  = isset($data['data']['typeName'])?$data['data']['typeName']:'';
        $plan_time = $data['data']['plan_time'];
        //技师id

        $technicianArr = $data['data']['technicianArr'];
        $title = '您有新的订单！';
        $content = '['.$plan_time.']['.$className.']['.$typeName.']订单派给您啦！提前安排时间噢！';
        $type = '1';
        $type_content = $workNo;
        $order_status = $data['data']['status'];

        foreach ($technicianArr as $val){
            $Assign = Push::pushMsg($title, $content, $val['technician_id'], $type, $type_content, $order_status);
        }

        $AssignArr = json_decode($Assign,true);
        if ($AssignArr['success'] == false) {
            return $AssignArr;
        }

        if ($technicianId > 0) {
            $title = '订单被改派了！';
            $content = '['.$plan_time.']['.$className.']['.$typeName.']订单被改派了，请查看！';
            $reAssign = Push::pushMsg($title, $content, $technicianId, $type, $type_content, $order_status);
            $reAssignArr = json_decode($reAssign,true);
            if ($reAssignArr['success'] == false) {
                return $reAssignArr;
            }
        }
        return $AssignArr;
    }

    /**
     * 获取订单详情
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $orderNo
     * @return array
     */
    public static function getDetail($orderNo)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/detail";
        /*$postData = [
            'order_no' => $orderNo,
            //'department_id'   => self::getDepartmentId(),
            'department_id'   => BaseLogic::getOrderSourceId($orderNo,$type=3),
            'direct_company_id'=>self::getDirectCompanyId(),
            'show_work'=> 1
        ];*/
        $postData = [
            'order_no'               => $orderNo,
            'direct_company_id'      => BaseLogic::getDirectCompanyId(),
            'department_id'          => BaseLogic::getOrderSourceId($orderNo,$type=3),
            'service_department_id'  => WorkOrderAssign::hasDepartment($orderNo,BaseLogic::getDepartmentId())?BaseLogic::getDepartmentId():0,
            'src_department_id'      => self::getDepartmentId(),
            'show_work'=> 1
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        $data = [];
        if(isset($result['success']) && $result['success'] == 1 ){
            return $result['data'];
        }
        return $data;
    }

    //查询收费标准
    public static function CheckScale(){
        $data  = Yii::$app->request->post();
        $merchantId         = intval(Yii::$app->request->post('merchantId',0));
        $service_area_id    = trim(Yii::$app->request->post('service_area_id',''));
        $brand_id           = intval(Yii::$app->request->post('brand_id',0));
        $class_id           = intval(Yii::$app->request->post('class_id',0));
        $service_type       = intval(Yii::$app->request->post('service_type',0));
        $fee_type           = intval(Yii::$app->request->post('fee_type',0));  //结算类型 1商家的  2自定义的
        if($fee_type == 1){
            $src_type = BaseModel::SRC_SJ;
        }
        if($fee_type == 2){
            $src_type = BaseModel::SRC_FWS;
        }
        $cost_level_id = '';
        $cost_level_id = CostLevel::getScale($src_type,$merchantId,$service_area_id,$brand_id,$class_id,$service_type);
        /*$cost_level_info = CostLevel::findOne(['status'=>1,'src_id'=>$merchantId,'src_type'=>$src_type,'service_area'=>$service_area_id]);
        if($cost_level_info){
            $res = CostLevelDetail::getCheckList($cost_level_info['id'],$brand_id,$class_id,$service_type);
            if($res){
                $cost_level_id = $cost_level_info['id'];
            }
        }*/
        return $cost_level_id;

    }

    public static function getReAssignList()
    {
        $orderNo         = trim(Yii::$app->request->get('order_no',''));
        $workType        = intval(Yii::$app->request->get('work_type',0));
        $planStartTime   = Yii::$app->request->get('plan_start_time','');
        $planEndTime     = Yii::$app->request->get('plan_end_time','');
        $createStartTime = Yii::$app->request->get('create_start_time','');  //下单开始时间  2018-5-10 新增
        $createEndTime   = Yii::$app->request->get('create_end_time','');    //下单结束时间 2018-5-10 新增
        $firstClassId    = intval(Yii::$app->request->get('first_class_id',0));
        $secondClassId   = intval(Yii::$app->request->get('second_class_id',0));
        $page            = intval(Yii::$app->request->get('page',1));
        $pageSize        = intval(Yii::$app->request->get('pageSize',10));

        $saleOrderId = [];

        if(trim($planStartTime) !=''){
            $planStartTime = strtotime($planStartTime);
        }
        if(trim($planEndTime) !=''){
            $planEndTime = strtotime($planEndTime);
        }
        //添加下单时间 条件搜索 2018-5-10 新增
        if(trim($createStartTime) !=''){
            $createStartTime = strtotime($createStartTime);
        }
        if(trim($createEndTime) !=''){
            $createEndTime = strtotime($createEndTime);
        }
        //产品类目
        if($secondClassId>0){
            $saleOrderId = SaleOrder::getIdsByClassId($secondClassId);
            if(!$saleOrderId){
                $saleOrderId = -1;
            }
        }
        else if($firstClassId>0){
            $saleOrderId = SaleOrder::getIdsByClassId($firstClassId);
            if(!$saleOrderId){
                $saleOrderId = -1;
            }
        }

        $url = Yii::$app->params['order.uservices.cn']."/v1/order/re-assign-search";
        $postData = [
            'order_no'          => $orderNo,
            'work_type'         => $workType,
            'sale_order_id'     => $saleOrderId,
            'plan_start_time'   => $planStartTime,
            'plan_end_time'     => $planEndTime,
            'create_start_time' => $createStartTime,
            'create_end_time'   => $createEndTime,
            'page'              => $page,
            'pageSize'          => $pageSize,
            'src_id'            => self::getDepartmentId(),
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            $list = $jsonArr['data']['list'];
            //print_r($list);die;
            if( $list )
            {
                //服务类型
                $workTypeArr = self::getTypeList();
                $workTypeArr = $workTypeArr['data'];

                //服务产品
                $saleOrderIds = array_column($list, 'sale_order_id');
                $saleOrderArr = SaleOrder::findAllByAttributes(['id'=>$saleOrderIds,'status'=>1],'id,prod_name,brand_name,class_name,type_name','id');

                //合并数据
                foreach ($jsonArr['data']['list'] as $key=>$val)
                {
                    $workTypeDesc  = '';
                    $serviceContent = '无';

                    //服务类型
                    if(isset($workTypeArr[$val['work_type']])){
                        $workTypeDesc = $workTypeArr[$val['work_type']];
                    }
                    //服务内容
                    if(isset($saleOrderArr[$val['sale_order_id']])){
                        $serviceContent = $saleOrderArr[$val['sale_order_id']]['brand_name'].'-'.$saleOrderArr[$val['sale_order_id']]['class_name'].'-'.$workTypeDesc;
                    }

                    $jsonArr['data']['list'][$key]['work_type_desc'] = $serviceContent;
                    $jsonArr['data']['list'][$key]['plan_time']      = date('Y-m-d H:i',$val['plan_time']);
                    $jsonArr['data']['list'][$key]['create_time']      = date('Y-m-d H:i',$val['create_time']);

                }
            }

            return self::success($jsonArr['data']);
        }
        else {
            return self::error("接口：$url 没有返回数据");
        }
    }

    /**
     * 验认验收
     * @param $orderNo  $flag 1 被指派的部门  2 创建部门或者 有权限的下级部门
     * @return array
     */
    public static function confirmFinish($orderNo,$flag=1)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/confirm-finish";
        $postData = [
            'order_no'        => $orderNo,
            'flag'        => $flag,
            'department_id'   => self::getDepartmentId(),
            'process_user_id' => Yii::$app->user->id,
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);

        if(isset($result['success']) && $result['success'] == 1 ){
            return self::success(['order_no'=>$orderNo]);
        }
        return self::error($result['message'],$result['code']);
    }

    /**
     * 验认未通过
     * @param $orderNo
     * @return array
     */
    public static function acceptanceFailure($orderNo,$reason)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/acceptance-failure";
        $postData = [
            'order_no'        => $orderNo,
            'department_id'   => self::getDepartmentId(),
            'process_user_id' => Yii::$app->user->id,
            'reason'          => $reason
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);

        if(isset($result['success']) && $result['success'] == 1 ){
            return self::success(['order_no'=>$orderNo]);
        }
        return self::error($result['message'],$result['code']);
    }

    /**
     * 编辑质保状态
     * @param $orderNo
     * @param $reason
     * @return array
     */
    public static function editScope(){
        if(Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $order_no = trim(yii::$app->request->post('order_no', ''));       //订单号
            $work_no = trim(yii::$app->request->post('work_no', ''));       //工单号
            $scope_id = intval(yii::$app->request->post('scope_id', ''));      //质保状态
            $srcType = intval(yii::$app->request->post('src_type', ''));       //订单号
            $srcId = intval(yii::$app->request->post('src_id', ''));      //质保状态
            $scope_img = trim(yii::$app->request->post('scope_img', ''));      //质保图片
            //凭证图片
            $file = Yii::$app->request->post("OrderForm", '');
            if ($file['scope_img']) {
                $scope_img = implode(',', $file['scope_img']);
            }
            //修改质保状态
            $url = Yii::$app->params['order.uservices.cn'] . "/v1/order/edit-scope";
            $postData = [
                'src_type' => $srcType,
                'src_id' => $srcId,
                'order_no' => $order_no,
                'is_scope' => $scope_id,
                'scope_img' => $scope_img,
            ];
            $jsonStr = Helper::curlPostJson($url, $postData);
            $jsonArr = json_decode($jsonStr, true);
            if (isset($jsonArr['success']) && $jsonArr['success'] == 1) {
                return self::success(['work_no' => $work_no]);
            }
            return self::error($jsonArr['message'], 20004);
        }
    }
    /**
     * 修改订单质保信息
     * @param $orderNo
     * @param $reason
     * @return array
     * 2018-7-4 sxz
     */
    public static function editScopeInfo($orderData){
        $scopeImg     = isset($orderData['scope_img'])?$orderData['scope_img']:'';
        $scopeStatus  = $orderData['is_scope'];
        $orderNo      = $orderData['order_no'];
        //修改质保信息
        $url = Yii::$app->params['order.uservices.cn'] . "/v1/order/edit-scope";
        $postData = [
            'department_id' => BaseLogic::getOrderSourceId($orderNo,$type=3),
            'order_no'      => $orderNo,
            'is_scope'      => $scopeStatus,
            'scope_img'     => $scopeImg,
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr, true);
        if (isset($jsonArr['success']) && $jsonArr['success'] == 1) {
            return self::success(['order_no' => $jsonArr['data']['order_no']]);
        }
        return self::error($jsonArr['message'], 20004);
    }

    /**
     * 修改订单服务时间
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function ChangeTime($orderNo,$planTime){
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/change-plantime";
        $postData = [
            'src_type'        => BaseModel::SRC_FWS,
            'src_id'          => self::getDepartmentId(),
            'order_no'        => $orderNo,
            'process_user_id' => self::getLoginUserId(),
            'plantime'        => $planTime,
            'source'          => \common\models\BaseModel::SOURCE_FWS
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            return self::success(['work_no'=>$orderNo]);
        }
    }

    /**
     * 获取订单总金额
     * @param $orderNos
     * @return array
     */
    private static function getOrderAmount($orderNos)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-cost/total-amount";
        $postData = [
            'order_no' => $orderNos,
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        if(isset($jsonArr['success']) && $jsonArr['success'] == true)
        {
            return $jsonArr['data'];
        }
        return [];
    }

    /**
     * 修改服务时间
     * @param $orderNo
     * @param $plantime
     * @return array
     * @author xi
     */
    public static function chagePlanTime($orderNo,$plantime,$planTimeType)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/change-plantime";
        $postData = [
            'order_no'        => $orderNo,
            'department_id'   => self::getDepartmentId(),
            'plantime'        => $plantime,
            'plan_time_type'  =>$planTimeType,
            'process_user_id' => self::getLoginUserId(),
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            return self::success($jsonArr,'提交成功');
        }
        return self::error($jsonArr['message'],20003);
    }

    /**
     * 指派机构
     * @param
     * @param
     * @return
     * $author chengjuanjuan
     */
    public static function AssiginInstitution($orderNo,$serviceDepartmentId,$totalWork='1')
    {
        $orderSourceId = Self::getOrderSourceId($orderNo);
        $url = Yii::$app->params['order.uservices.cn'] . "/v1/order/assign-institution";
        $postData = [

            'order_no'              => $orderNo,
            'order_source_id'       =>$orderSourceId,
            'process_user_id'       => Yii::$app->user->id,
            'department_id'         => self::getDepartmentId(),
            'service_department_id' => $serviceDepartmentId
        ];

        if(!$orderNo){
            return self::error('字段order_no不能为空');
        }
        if(!$serviceDepartmentId){
            return self::error('字段serviceDepartmentId不能为空');
        }
        $jsonStr = Helper::curlPostJson($url, $postData);
        //echo $jsonStr;exit;
        return json_decode($jsonStr, true);
    }

    /**
     * 改派机构
     * @param
     * @param
     * @return
     * $author chengjuanjuan
     */
    public static function ReAssiginInstitution($orderNo,$serviceepartmentId,$reason){
        $orderSourceId = Self::getOrderSourceId($orderNo);
        $url = Yii::$app->params['order.uservices.cn'] . "/v1/order/re-assign-institution";
        $postData = [

            'order_no'              => $orderNo,
            'order_source_id'       => $orderSourceId,
            'process_user_id'       => Yii::$app->user->id,
            'department_id'         => self::getDepartmentId(),
            'service_department_id' => $serviceepartmentId ,
            'reason'                =>  $reason,
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        return  $result = json_decode($jsonStr, true);

    }

    /**
     * 审核通过
     * @param $orderNo
     * @author xi
     * @date 2018-6-23
     * 2018-8-15添加验证用户下的产品
     */
    public static function throughAudit($orderNo)
    {
        //首先验证用户下的产品
        $data = self::getDetail($orderNo);
        if(isset($data['workDetail'])){
            //服务产品
            $saleOrderIds = array_column($data['workDetail'], 'sale_order_id');
            $saleOrderArr = SaleOrderView::findAllByAttributes(['id'=>$saleOrderIds,'status'=>1],'id,prod_id','id');
            if($saleOrderArr)
            {
                foreach ($saleOrderArr as $key=>$val){
                    if($val['prod_id'] == 0){
                        return self::error('客户填写的产品信息未在系统登记，请前往订单详情编辑或在产品库中添加！','-20002');
                    }
                }
            }
        }else{
            return self::error('获取数据错误','-20001');
        }
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/through-audit";
        $postData = [
            'order_no'        => $orderNo,
            'department_id'   => self::getDepartmentId(),
            'process_user_id' => self::getLoginUserId(),
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            return self::success($jsonArr,'操作成功');
        }
        return self::error($jsonArr['message'],$jsonArr['code']);
    }

    /**
     * 审核未通过
     * @param $orderNo
     * @author xi
     * @date 2018-6-23
     */
    public static function notThrough($orderNo,$reason)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/audit-not-through";
        $postData = [
            'order_no'        => $orderNo,
            'department_id'   => self::getDepartmentId(),
            'process_user_id' => self::getLoginUserId(),
            'reason'          => $reason
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            return self::success($jsonArr,'操作成功');
        }
        return self::error($jsonArr['message'],$jsonArr['code']);
    }

    /**
     * 修正部门id
     * @param $departmentIds
     * @return array
     * @author xi
     */
    private static function getRealDepartmentIds($departmentIds)
    {
        $result = [];
        $directCompanyId = self::getDirectCompanyId();
        $query = Department::findAllByAttributes(['id'=>$departmentIds],'id,direct_company_id');
        foreach ($query as $val)
        {
            if($val['direct_company_id'] == $directCompanyId){
                $result[] = $val['id'];
            }
            else {
                $result[] = $val['direct_company_id'];
            }
        }

        return $result;
    }

    /**
     * 获取订单的状态
     * @param $orderNos
     * @return array
     * @author xi
     */
    private static function curlGetOrderStatus($orderNos)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/get-order-status";
        $postData = [
            'order_nos'     => $orderNos,
            'department_id' => self::getDepartmentId()
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);

        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            return $jsonArr['data'];
        }

        return [];
    }
    /**
     * 订单状态统计
     * @param status 状态
     * 2018-7-18
     * quan
     */
    public static function orderNum(){
        $status        = Yii::$app->request->get('status','');
        $datAuthId = self::getDatAuthId();
        $is_department = 0;  // 除本人权限外
        if($datAuthId == 3){  //本公司及以下
            //获取当前部门的上级ID
            $top_id = self::getDirectCompanyId();
            $department_info = department::getDepartmentChildren($top_id);
            foreach ($department_info as $val){
                $department_ids[] = $val['id'];
            }
        }else if($datAuthId == 2){   //本公司
            //获取当前部门的顶级部门ID
            $top_id = self::getDirectCompanyId();
            $department_info = department::getDepartmentChildren($top_id,0,1);
            foreach ($department_info as $val){
                $department_ids[] = $val['id'];
            }
        }else if($datAuthId == 4){   //本部门
            if(self::getDepartmentType() == 3){
                $department_ids[] = self::getDepartmentId();
            }else{
                $is_department = 1;
                $department_ids[] = self::getLoginUserId();
            }
        }else if($datAuthId == 5) {//本部门及以下
            if(self::getDepartmentType() == 3){
                //获取自身及以下
                $department_info = department::getDepartmentChildren(self::getDepartmentId(),2);
                foreach ($department_info as $val) {
                    $department_ids[] = $val['id'];
                }
            }else{
                $is_department = 1;
                $department_ids[] = self::getLoginUserId();
            }
        }else if ($datAuthId == 1) {
            $is_department = 1;
            $department_ids[] = self::getLoginUserId();
        }
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/count";
        $postData = [
            'status'               => $status,
            'is_department'      => $is_department,
            'department_ids'          => $department_ids
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        if($result['success']){
            return $result['data'];
        }else{
            return 0;
        }
    }

    /**
     * 客户服务记录
     * sxz 2018-7-23
     */
    public static function getServiceRecord()
    {
        //查询订单数据
        $saleOrderId   = intval(Yii::$app->request->get('sale_order_id',''));
        if(!$saleOrderId){
            return [];
        }
        $result = self::getList();
        if($result['data']['list']){
            foreach ($result['data']['list'] as $k=>$v){
                if(empty($v['work_stage_arr'])){
                    unset($result['data']['list'][$k]);
                }
            }
        }
        return $result;

    }
    /**
     * 合同详情订单服务记录
     * sxz 2018-8-3
     */
    public static function getContractServiceRecord()
    {
        //查询订单数据
        $result = self::getList();
        if(isset($result['data']['list'])){
            foreach ($result['data']['list'] as $k=>$v){
                if(empty($v['work_stage_arr'])){
                    unset($result['data']['list'][$k]);
                }
            }
        }
        return $result;
    }
}
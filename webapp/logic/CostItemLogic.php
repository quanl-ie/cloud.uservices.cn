<?php
namespace webapp\logic;

use Yii;
use common\models\CostItemSet;
use webapp\models\CostItem;

class CostItemLogic extends BaseLogic
{
    /**
     * 获取收费项目列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 14:05
     * @return array
     */
    public static function getIndex()
    {
    
        $topId    = self::getTopId();
        $dcId     = self::getDirectCompanyId();
        
        if ($dcId == $topId) {
            $flag = 1;
        } else {
            $flag = 2;
        }
        
        $where = [
            ['=','direct_company_id',$dcId]
        ];
        
        //获取列表数据
        $res = CostItem::getPageList($where);
        
        if (isset($flag)) {
            $res['flag'] = $flag;
        }
        
        if (empty($res['list'])) {
            return $res;
        }
    
        //获取收费项目id
        $costIds = array_column($res['list'],'cost_item_set_id');
    
        //收费项目名称
        $cost     = CostItemSet::getList(['id'=>$costIds,'department_id' =>$topId]);
        if (empty($cost)) {
            $res['list'] = [];
            return $res;
        }
        $status    = array_column($cost,'status','id');
        $costName  = array_column($cost,'name','id');
        
        foreach ($res['list'] as $key => $val) {
            
            if (isset($costName[$val['cost_item_set_id']])) {
                $res['list'][$key]['name'] = $costName[$val['cost_item_set_id']];
            }else{
                $res['list'][$key]['name'] = '';
            }
        
            if (isset($status[$val['cost_item_set_id']])) {
                $res['list'][$key]['status'] = $status[$val['cost_item_set_id']];
            }
        
            if (isset($status[$val['cost_item_set_id']]) && $status[$val['cost_item_set_id']] == 1) {
                $res['list'][$key]['status_desc'] = '已启用';
            }else{
                $res['list'][$key]['status_desc'] = '已禁用';
            }
            if (!isset($costName[$val['cost_item_set_id']])) {
                unset($res['list'][$key]);
            }
        }
        
        return $res;
    }
    
    /**
     * 添加数据列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 14:05
     * @return mixed
     */
    public static function beforeAdd()
    {
        $topId    = self::getTopId();
        $dcId     = self::getDirectCompanyId();
        $departId = self::getDepartmentId();
        //获取已添加的收费项目
        $costItem = CostItem::getList(['direct_company_id' => $dcId]);
        
        //获取id
        $costItemSetId = [];
        if (!empty($costItem)) {
            $costItemSetId = array_column($costItem,'cost_item_set_id');
        }
        
        //获取所有收费项目数据
        $costItemSetList = CostItemSet::getList(['department_id' => [0,$topId]]);
        $selfItem        = [];
        $commonItem      = [];
        foreach ($costItemSetList as $key => $val) {
            if ($val['department_id'] == $topId) {
                $selfItem[$val['id']]   = $val['name'];
            }else{
                $commonItem[$val['id']] = $val['name'];
            }
        }
    
        foreach ($selfItem as $val) {
            unset($commonItem[array_search($val,$commonItem)]);
        }

        //组合数据
        $data['costItemSetId']   = $costItemSetId;
        $data['costItemSetList'] = array_replace_recursive($commonItem,$selfItem);
        
        return $data;
    }
    
    /**
     * 编辑添加数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 14:05
     * @return array
     */
    public static function add()
    {
        //接收参数
        $itmeId = Yii::$app->request->post('itme_id','');
        
        if ($itmeId == '') {
            return self::error('保存失败',20001);
        }
        
        //定义变量
        $time         = time();
        $topId        = self::getTopId();
        $departmentId = self::getDepartmentId();
        
        //查询自身所有收费项目
        $item    = CostItem::getList(['direct_company_id' => $topId]);
        
        //提取收费项目id
        $itmeIds = array_column($item,'cost_item_set_id','id');
        
        //计算出需要添加的收费项目
        $addCostItemId  = array_diff($itmeId,$itmeIds);
    
        $data     = [];
        $userId   = Yii::$app->user->id;
        $itmeName = CostItemSet::getName($addCostItemId);

        //组合需要添加的收费项目
        if (!empty($addCostItemId))
        {
            foreach ($addCostItemId as $key => $val) {
    
                $data[$key]['name']            = $itmeName[$val];
                $data[$key]['department_id']   = $topId;
                $data[$key]['sort']            = 1;
                $data[$key]['status']          = 1;
                $data[$key]['del_status']      = 1;
                $data[$key]['oporate_id']      = $userId;
                $data[$key]['create_time']     = $time;
                $data[$key]['update_time']     = $time;
                $data[$key]['quote_id']        = $val;
            }
        }
        
        $res = CostItemSet::add($data,$itmeIds,$departmentId);
        
        if ($res) {
            return self::success('','保存成功');
        }
        return self::error('保存失败',20002);
        
    }
    
    /**
     * 修改收费项目状态（禁用、启用）
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 14:05
     * @return array
     */
    public static function changeStatus()
    {
        $id     = intval(Yii::$app->request->post('id',0));
        $status = intval(Yii::$app->request->post('status',0));
        
        if ($id == 0) {
            return self::error('操作失败',20001);
        }
        
        if ($status == 0) {
            return self::error('操作失败',20002);
        }
        
        $res = CostItemSet::changeStatus($id,$status);
        
        if ($res) {
            return self::success('','操作成功');
        }
        return self::error('操作失败',20003);
    }
}
<?php
namespace webapp\logic;

use common\helpers\Gaode;
use common\models\Role;
use common\models\ServiceClass;
use common\models\ServiceType;
use common\models\SysConfig;
use webapp\controllers\BaseController;
use webapp\models\Department;
use webapp\models\SaleOrderView;
use Yii;
use webapp\models\BaseModel;
use webapp\models\SaleOrder;
use webapp\models\Technician;
use common\helpers\Helper;
use webapp\models\Account;
use webapp\models\Stores;
use webapp\models\AccountAddress;
use webapp\models\Region;
use common\models\User;
use webapp\models\TechnicianSkill;
use webapp\models\TechnicianMode;
use webapp\models\Position;


class TechnicianLogic extends BaseLogic
{
    //技师审核状态
    public static function auditStatus()
    {
        return [
            '1'=>'待服务商审核',
            '2'=>'待平台审核',
            '3'=>'服务商驳回',
            '4'=>'平台驳回',
            '5'=>'审核通过'
        ];
    }
    /**
     * 订单列表
     * @return array
     * @author li
     * @date 2018-06-14
     */
    public static function getList($where=[])
    {
        $getDataAuth           = self::getListDataAuth(3,2);
        $departmentIds          = $getDataAuth['department_id'];
        $dataAuth               = $getDataAuth['dataAuth'];
        $createUserId           = $getDataAuth['create_user_id']?$getDataAuth['create_user_id']:'';
        $department_name        = $getDataAuth['department_name'] ? $getDataAuth['department_name'] : '';
        if($createUserId){
            $where []= ['create_user_id'=>$createUserId];
        }else{
            $where [] = ['in','store_id' ,$departmentIds];
        }


        $params['technician_name']   = $technicianName   = strip_tags(trim(Yii::$app->request->get('technician_name',''))); //技师姓名
        $params['technician_mobile'] = $technicianMobile = strip_tags(trim(Yii::$app->request->get('technician_mobile',''))); //技师电话
        $params['service_type']      = $serviceType      = intval(Yii::$app->request->get('service_type',0)); //服务类型
        $params['class_type']        = $classType        = intval(Yii::$app->request->get('class_type',0));  //产品类型
        $params['page']              = $page             = intval(Yii::$app->request->get('page',1));
        $params['pageSize']          = $pageSize         = intval(Yii::$app->request->get('pageSize',20));

        $maps['audit_status'] = 5;

        //搜索条件
        if (Yii::$app->request->isGet)
        {
            if ($technicianName != '') {
                $where[] = ['like','name',$technicianName];
            }
            if ($technicianMobile != '') {
                $maps['mobile'] = $technicianMobile;
            }
            //服务类型，产品类型条件
            $technicianSkill = $technicianMode = [];
            if($serviceType != ''){
                $technicianSkill['service_id'] = $serviceType;
            }
            if($classType != ''){
                $technicianSkill['class_id'] = $classType;
            }
            //查询出符合技能要求的技师
            if($technicianSkill)
            {
                $tIds = TechnicianSkill::getTechnicianIds($technicianSkill);
                if($tIds){
                    $where[] = ['in','id',$tIds];
                }
            }
        }
        $dataList = Technician::getList($maps,$where,$page,$pageSize);
        if($dataList['list'])
        {
            //取出所有技师id
            $tIds = array_column($dataList['list'], 'id');
            //调用接口查询技师服务状态
            $t_status = Technician::getTechnicianStatus($tIds);
            foreach ($dataList['list'] as $key=>&$val)
            {
                if($t_status)
                {
                    foreach ($t_status as $k=>$v)
                    {
                        if($val['id'] == $k){
                            $val['status_desc'] = $v['status_desc'];
                        }
                    }
                }
                if($val['store_id']){
                    $val['department_name'] = $dataAuth[$val['store_id']]['name'];
                }
            }
        }

        //仓储开关
        $sys = SysConfig::getOne(['config_key'=>'storage_status','direct_company_id'=>self::getDirectCompanyId()]);

        $dataList['sysConfig'] = 1;
        $dataList['params']    = $params;
        $dataList['department_name'] = $department_name;;
        return $dataList;
    }
    /**
    * 函数用途描述:查询技师详情
    * @date: 2018年1月8日 下午9:36:25
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function getDetail($id=''){
        $TechnicianId = trim(Yii::$app->request->get('technician_id','')); //技师id
        $data = Technician::getOne(['id'=>$TechnicianId]);
        if($data){
            $other = TechnicianMode::getOne(['technician_id'=>$data['id']]);
            $data['service_status'] = isset($other['service_status'])?$other['service_status']:'';
            $data['work_city'] = Region::getCityName($data['province']).Region::getCityName($data['city']).Region::getCityName($data['district']);
            //查询技师服务状态
            $data['status_desc'] = '';
            $tid_Arr = explode(',',$TechnicianId);
            $res = Technician::getTechnicianStatus($tid_Arr);
            if($res){
                $status_desc = array_column($res, 'status_desc');
                $data['status_desc'] = $status_desc[0];
            }

        }
        //print_r($data);exit;
        return $data;
    }
    /**
    * 函数用途描述:获取技师技能
    * @date: 2018年1月9日 上午10:22:10
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function getSkill($id='')
    {
        $TechnicianId = trim(Yii::$app->request->get('id','')); //技师id
        $data = TechnicianSkill::getSkill($TechnicianId);

        $classArr = [];
        $classIds = array_unique(array_column($data,'class_id'));
        $classRes = ServiceClass::findAllByAttributes(['id'=>$classIds],'id,title');
        if($classRes){
            $classArr = array_column($classRes,'title','id');
        }

        $typeArr = [];
        $typeIds = array_unique(array_column($data,'service_id'));
        $typeRes = ServiceType::findAllByAttributes(['id'=>$typeIds],'id,title');
        if($typeRes){
            $typeArr = array_column($typeRes,'title','id');
        }

        $res = [];
        if($data) foreach ($data as $key=>$val)
        {
            $val['class_name'] = isset($classArr[$val['class_id']])?$classArr[$val['class_id']]:'';
            $val['service_id'] = isset($typeArr[$val['service_id']])?$typeArr[$val['service_id']]:'未知';
            $res[$val['service_id']][$val['class_id']] = $val['class_name'];
        }
        return $res;

    }
    /**
     * 函数用途描述:获取技师位置
     * @date: 2018年1月9日 下午15:22:10
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function getLocation()
    {
        $TechnicianId = trim(Yii::$app->request->get('id','')); //技师id
        $data = [];
        $technician = Technician::getOne(['id'=>$TechnicianId]);
        if ($technician) {
            $data = Position::findOne(['technician_id'=>$TechnicianId]);
            if($data)
            {
                if($data->address == '' && $data->lon > 0 && $data->lat > 0 ){
                    $data->address = Gaode::getAddressByLatLon($data->lon,$data->lat);
                    $data->save(false);
                }

                $data = $data->toArray();
            }
            if ($data) {
                $data['name'] = $technician['name'];
                $data['mobile'] = $technician['mobile'];
                $data['updated_at'] = isset($data['updated_at'])?date("Y-m-d H:i:s",$data['updated_at']):'';
            }
        }
        if ($data) {
            self::positionDecode($data['lon'], $data['lat'], $data['technician_id'], 'currentPosition');
        }
        return $data;
    }
    /**
     * 函数用途描述:获取技师位置修改
     * @date: 2018年1月9日 下午15:22:10
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function editArea(){
        $TechnicianId = trim(Yii::$app->request->get('id','')); //技师id
        $data = TechnicianMode::getOne(['technician_id'=>$TechnicianId]);
        if (!$data){
            $data['technician_id'] = $TechnicianId;
        }
        return $data;
    }
    //判断技师是否设置接单范围
    public static function Verify(){
        $TechnicianId = trim(Yii::$app->request->post('id','')); //技师id
        $data = TechnicianMode::getOne(['technician_id'=>$TechnicianId]);
        if(empty($data['service_address']) && empty($data['lon']) && empty($data['lat'])){
            return self::error('未设置接单范围');
        }
        return self::success($data);
    }
    /**
     * 函数用途描述:执行技师位置修改
     * @date: 2018年1月9日 下午15:22:10
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function updateArea(){
        $post = Yii::$app->request->post();
        $data['technician_id'] = $post['technician_id'];
        $data['service_address'] = $post['service_address'];
        $data['lat'] = $post['lat'];
        $data['lon'] = $post['lon'];
        $data['service_radius'] = $post['service_radius'];
        $data['is_ban'] = $post['is_ban'];
        $technicianMode = TechnicianMode::getOne(['technician_id'=>$post['technician_id']]);
        if (!$technicianMode) {
           //插入数据
            $result = TechnicianMode::add($data);
            if ($result) {
                self::positionDecode($data['lon'], $data['lat'], $data['technician_id'],'scopePosition');
            }
        }else{
            //更新数据
            $result = TechnicianMode::edit($data);
            if ($result) {
                self::positionDecode($data['lon'], $data['lat'], $data['technician_id'],'scopePosition');
            }
        }
        if (!$result) {
            return self::error('设置失败');
        }
        return self::success('','设置成功');
    }
    /**
     * 函数用途描述:重置技师密码
     * @date: 2018年1月9日 下午15:22:10
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function resetPwd(){
        $id       = intval(yii::$app->request->post('id',0));
        $pwd       = intval(yii::$app->request->post('userpassword',0));
        $pwd2       = intval(yii::$app->request->post('userpassword2',0));
        if($pwd != $pwd2){
            return self::error('新密码与确认新密码不一致!',20001);
        }
        //调用model执行添加
        $id = ['id'=>$id];
        $password = Yii::$app->security->generatePasswordHash($pwd);

        $res = Technician::setPwd($id,$password);
        if ($res) {
            return self::success('','修改成功');
        }
        return json_encode($res);
    }
    /**
     * 函数用途描述:技师添加
     * @date: 2018年1月9日 下午17:22:10
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function add()
    {
        $name          = trim(yii::$app->request->post('name',''));
        $mobile        = trim(yii::$app->request->post('mobile',0));
        $password      = trim(yii::$app->request->post('password',''));
        $sex           = intval(yii::$app->request->post('sex',0));
        $identity_card = trim(yii::$app->request->post('identity_card',0));
        $province      = intval(yii::$app->request->post('province',0));
        $city          = intval(yii::$app->request->post('city',0));
        $district      = intval(yii::$app->request->post('district',0));
        $skill         = trim(yii::$app->request->post('skill',''));  //获取的数据为技能字符串组合数据
        $store_id      = intval(Yii::$app->request->post('store_id',0)); //获取服务商

        //technician技师表数据
        $t_data['technician_number'] = BaseModel::get_technicianNum();
        $t_data['name']              = $name;
        $t_data['mobile']            = $mobile;
        $t_data['password']          = Yii::$app->security->generatePasswordHash($password);
        $t_data['password_hash']     = $t_data['password'];
        $t_data['sex']               = $sex;
        $t_data['identity_card']     = $identity_card;
        $t_data['province']          = $province;
        $t_data['city']              = $city;
        $t_data['district']          = $district;
        $t_data['store_id']          = $store_id;
        $t_data['created_at']        = $t_data['updated_at'] = $t_data['audit_time'] = time();
        $t_data['audit_status']      = 5;  //添加后直接就是审核通过状态
        $t_data['create_user_id']    = self::getLoginUserId();  //添加后直接就是审核通过状态
        //技师默认接单数据
        $t_mode['service_work']      = json_encode([0,1,2,3,4,5,6]);
        $t_mode['service_time']      = json_encode([8,20]);
        $t_mode['travel']            = 0;
        $t_mode['service_status']    = 0;
        $t_mode['updated_at']        = time();
        $t_mode['service_radius']    = 10;

        //验证技师信息是否存在
        $validateMobile = Technician::hasRepeat($mobile,0);
        if($validateMobile == true){
            return BaseLogic::error('技师电话已存在!',20002);
        }
        $validateId = Technician::verifyData(['store_id'=>$store_id,'identity_card'=>$identity_card]);
        if($validateId == false && !empty($identity_card)){
            return BaseLogic::error('技师身份证信息已存在!',20002);
        }
        $res = Technician::add($t_data,$skill,$t_mode);
        if($res['code'] == '20008'){
            return BaseLogic::error('技师头像不能大于1M',20002);
        }
        if($res['code'] == '20009'){
            return BaseLogic::error('身份证大小不能大于2M',20002);
        }
        if($res['code'] == '20010'){
            return BaseLogic::error('图片上传失败',20002);
        }
        if($res['code'] == '20011'){
            return BaseLogic::error('技师电话已存在!',20002);
        }
        if ($res) {
            return BaseLogic::success('','添加成功');
        }
        return BaseLogic::error('添加失败');
    }
    //技师信息编辑
    public static function edit()
    {
        $id            = trim(yii::$app->request->post('id',0));
        $name          = trim(yii::$app->request->post('name',''));
        $mobile        = trim(yii::$app->request->post('mobile',0));
        $password      = trim(yii::$app->request->post('password',''));
        $sex           = intval(yii::$app->request->post('sex',0));
        $identity_card = trim(yii::$app->request->post('identity_card',0));
        $province      = intval(yii::$app->request->post('province',0));
        $city          = intval(yii::$app->request->post('city',0));
        $district      = intval(yii::$app->request->post('district',0));
        $skill         = trim(yii::$app->request->post('skill','')); //获取的数据为技能字符串组合数据
        $store_id      = intval(Yii::$app->request->post('store_id',0)); //获取服务商

        //technician技师表数据
        $t_data['id']     = $id;
        $t_data['name']   = $name;
        $t_data['mobile'] = $mobile;
        if($password){
            $t_data['password']      = Yii::$app->security->generatePasswordHash($password);
            $t_data['password_hash'] = $t_data['password'];
        }
        $t_data['sex']            = $sex;
        $t_data['identity_card']  = $identity_card;
        $t_data['province']       = $province;
        $t_data['city']           = $city;
        $t_data['district']       = $district;
        $t_data['store_id']       = $store_id;
        $t_data['updated_at']     = time();
        $t_data['update_user_id'] = self::getLoginUserId();
        $res = Technician::edit($t_data,$skill);
        if ($res) {
            return BaseLogic::success('','修改成功');
        }
        return BaseLogic::error('修改失败');
    }

    /**
     * 获取技师添加技能所需所有技能
     * @return array
     * @author lwy
     */
    public static function getSkills()
    {
        $data = [];
        //服务类型
        $workTypeArr  = BaseLogic::getType('',1);
        $service_type = $workTypeArr;
        //产品一级类型
        $classArr   = BaseLogic::getClass(0,1);
        $class_type = $classArr;
        //组合技师技能
        $skill = [];
        //服务类型设置为键
        foreach ($service_type as $key =>$val) {
            $skill[$key] = $class_type;
        }
        $data['service_type'] = $service_type;
        $data['class_type']   = $class_type;
        $data['skill']        = $skill;
        return $data;

    }
    //技师审核列表
    public static function getExamineList(){
        $params = [];
        $params['technician_name'] = $technicianName = trim(Yii::$app->request->get('technician_name','')); //技师姓名
        $params['technician_mobile'] = $technicianMobile = trim(Yii::$app->request->get('technician_mobile','')); //技师电话
        $params['audit_status'] = $auditStatus = intval(Yii::$app->request->get('audit_status',0)); //审核状态
        $params['page'] = $page = intval(Yii::$app->request->get('page',1));
        $params['pageSize'] = $pageSize = intval(Yii::$app->request->get('pageSize',20));

        $map = $where = [];
        //$where[] = ['store_id'=>1];
        $map['store_id'] = self::getManufactorId();
        if (yii::$app->request->isGet) {
            if ($technicianName != '') {
                $where[] = ['like', 'name', $technicianName];
            }
            if ($technicianMobile != '') {
                $where[] = ['mobile'=>$technicianMobile];
            }
            //技师审核状态
            if ($auditStatus != '') {
                $where[] = ['audit_status'=>$auditStatus];
            }else{
                $where[] = ['<>','audit_status',5];
            }

        }
        //print_r($where);exit;
        $dataList = Technician::getEList($map,$where,$page,$pageSize);
        //print_r($dataList);exit;
        $dataList['params'] = $params;
        return $dataList;

    }
    //查询技师未审核通过原因
    public static function getReason(){
        $TechnicianId = intval(Yii::$app->request->get('id','')); //技师id
        $data = Technician::getOne(['id'=>$TechnicianId]);
        $reason = isset($data['reason'])?$data['reason']:'';
        return $reason;
    }
    //获取当前技师信息
    public static function getTechnicianInfo()
    {
        $TechnicianId = intval(Yii::$app->request->get('id','')); //技师id
        $data = Technician::getOne(['id'=>$TechnicianId]);
        if($data)
        {
            $other = TechnicianMode::getOne(['technician_id'=>$data['id']]);
            $data['service_status'] = isset($other['service_status'])?$other['service_status']:'';
            $data['work_city'] = Region::getCityName($data['province']).Region::getCityName($data['city']).Region::getCityName($data['district']);
            $data['province_name'] = Region::getCityName($data['province']);
            $data['city_name'] = Region::getCityName($data['city']);
            $data['district_name'] = Region::getCityName($data['district']);
            //获取技师技能
            $data['skill'] = '';
            $skills = TechnicianSkill::getSkill($TechnicianId);
            if ($skills) {
                foreach ($skills as $key => $val) {
                    $skillIds[$key] = implode('_', $val);
                }
                $data['skill'] = implode(',',$skillIds);
            };
            //获取技师技能名字
            $data['skill_name'] = TechnicianSkill::getTechnicianSkillName($TechnicianId);
        }
        return $data;
    }

    //查询技师接单情况
    public static function getTechnicianOrder($departmentId)
    {
        $workType        = intval(Yii::$app->request->get('work_type',0)); //服务类型
        $workClass       = intval(Yii::$app->request->get('work_class',0));  //产品类型
        $serviceTimeType = intval(Yii::$app->request->get('service_time_type',0));  //上门方式
        $planStartTime   = Yii::$app->request->get('plan_start_time','');  //上门开始时间
        $planEndTime     = Yii::$app->request->get('plan_end_time','');    //上门结束时间
        $technicianId    = intval(trim(Yii::$app->request->get('technician_id',0))); //技师id
        $checkTechnician = intval(trim(Yii::$app->request->get('check_technician',0)));
        $postId = '';
        if($technicianId == '' || $technicianId == 0 ){
            $postId = $technicianId = intval(trim(Yii::$app->request->post('technician_id',0))); //技师id
        }
        if($checkTechnician == 1){
            $postId = $technicianId;
        }
        $page            = intval(Yii::$app->request->get('page',1));
        $pageSize        = intval(Yii::$app->request->get('pageSize',10));
        //搜索
        $flag = true;
        if(trim($planStartTime) !=''){
            $planStartTime = strtotime($planStartTime);
        }
        if(trim($planEndTime) !=''){
            $planEndTime = strtotime($planEndTime);
        }

        $departmentRes = Department::findAllByAttributes(['direct_company_id'=>$departmentId], 'id');
        $departmentIds = array_column($departmentRes,'id');

        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/search";
        $postData = [
            'work_type'         => $workType,
            'work_class'        =>$workClass,
            'technician_id'     => $technicianId,
            'service_time_type' => $serviceTimeType,
            'plan_start_time'   => $planStartTime,
            'plan_end_time'     => $planEndTime,
            'page'              => $page,
            'pageSize'          => $pageSize,
            'department_id'     => $departmentIds,
            'sort'              => 1
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);

        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            $list = $jsonArr['data']['list'];

            //根据$postId查询当前技师是否有未完成的工单 2018-5-15
            if($postId){
                if($checkTechnician){
                    $res = self::getIsFinish($list,1);
                    return self::success(['tech_id'=>$res]);
                }else{
                    $res = self::getIsFinish($list);
                }

            }
            if( $list )
            {
                //客户
                $accountArr = [];
                $accountIds = array_column($list, 'account_id');
                if($accountIds){
                    $accountArr = Account::findAllByAttributes(['id'=>$accountIds,'status'=>1],'id,account_name,mobile','id');
                }
                //客户地址
                $accountAddressArr = [];
                $accountAddressIds = array_column($list, 'address_id');
                if($accountAddressIds){
                    $accountAddressArr = AccountAddress::findAllByAttributes(['id'=>$accountAddressIds,'status'=>1],'id,address,province_id,city_id,district_id','id');
                }
                //服务类型
                $workTypeArr = self::getTypeList();
                $workTypeArr = $workTypeArr['data'];

                //服务产品
                $saleOrderIds = array_column($list, 'sale_order_id');
                $saleOrderArr = SaleOrderView::findAllByAttributes(['id'=>$saleOrderIds,'status'=>1],'id,prod_name,brand_name,class_name,type_name','id');

                //技师
                $technicianArr = [];
                $technicianIds = array_filter(array_column($list,'technician_id'));
                if($technicianIds){
                    $technicianArr = Technician::findAllByAttributes(['id'=>$technicianIds],'id,name,mobile','id');
                }


                //合并数据
                foreach ($jsonArr['data']['list'] as $key=>$val)
                {
                    $accountName   = '';
                    $accountMobile = '';
                    $workTypeDesc  = '';
                    $prodDesc      = '';
                    $storesDesc    = '无';
                    $province = '';
                    $city = '';
                    $district = '';
                    $address = '';
                    $serviceContent = '无';

                    //客户
                    if( isset($accountArr[$val['account_id']]) ){
                        $accountName  = $accountArr[$val['account_id']]['account_name'];
                        $accountMobile = $accountArr[$val['account_id']]['mobile'];
                    }
                    //客户地址
                    if( isset($accountAddressArr[$val['address_id']]) ){
                        $province = $accountAddressArr[$val['address_id']]['province_id'];
                        $city = $accountAddressArr[$val['address_id']]['city_id'];
                        $district = $accountAddressArr[$val['address_id']]['district_id'];
                        $address = $accountAddressArr[$val['address_id']]['address'];
                    }
                    //print_r($val);exit;
                    //服务类型
                    if(isset($workTypeArr[$val['work_type']])){
                        $workTypeDesc = $workTypeArr[$val['work_type']];
                    }
                    //服务产品
                    if(isset($saleOrderArr[$val['sale_order_id']])){
                        $prodDesc = $saleOrderArr[$val['sale_order_id']]['prod_name'];
                    }
                    //服务内容
                    if(isset($saleOrderArr[$val['sale_order_id']])){
                        $serviceContent = $saleOrderArr[$val['sale_order_id']]['brand_name'].'-'.$saleOrderArr[$val['sale_order_id']]['class_name'].'-'.$workTypeDesc;
                    }

                    //技师
                    if($val['technician_id']>0 && isset($technicianArr[$val['technician_id']])){
                        $technicianName   = $technicianArr[$val['technician_id']]['name'];
                        $technicianMobile = $technicianArr[$val['technician_id']]['mobile'];
                    }else{
                        $technicianName   = '--';
                        $technicianMobile = '--';
                    }

                    $jsonArr['data']['list'][$key]['account_name']   = $accountName;
                    $jsonArr['data']['list'][$key]['account_mobile'] = $accountMobile;
                    $jsonArr['data']['list'][$key]['work_type_desc'] = $workTypeDesc;
                    $jsonArr['data']['list'][$key]['plan_time']      = date('Y-m-d H:i',$val['plan_time']);
                    $jsonArr['data']['list'][$key]['prod_desc']      = $prodDesc;
                    $jsonArr['data']['list'][$key]['stores_desc']    = $storesDesc;
                    $jsonArr['data']['list'][$key]['technician_name'] = $technicianName;
                    $jsonArr['data']['list'][$key]['technician_mobile'] = $technicianMobile;
                    $jsonArr['data']['list'][$key]['province'] = $province;
                    $jsonArr['data']['list'][$key]['city'] = $city;
                    $jsonArr['data']['list'][$key]['district'] = $district;
                    $jsonArr['data']['list'][$key]['address'] = $address;
                    $jsonArr['data']['list'][$key]['service_content'] = $serviceContent;

                }
            }

            return self::success($jsonArr['data']);
        }
        else {
            return self::error("接口：$url 没有返回数据");
        }
    }
    /**
     * 调用地图打点封装
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $lon
     * @param type $lat
     * @param type $technicianId
     * @param type $distinguish
     * @return type
     */
    public static function positionDecode($lon,$lat,$technicianId,$distinguish = ''){
        $array[$distinguish] = true;
        $array['lon'] = $lon;
        $array['lat'] = $lat;
        $array['technicianId'] = $technicianId;

        $array['distence'] = '100';//默认10km
        $array['unit'] ='km';

        $type = 'add';
        $key='Beijing_test2';

        $data = [
            'key'   => $key,
            'param' => json_encode($array),
            'type'  => $type
        ];
        $url = Yii::$app->params['positionsystem'].'/index/index';
        $jsonStr = Helper::curlPost($url,$data);
        $jsonArr  = json_decode($jsonStr,true);
        return $jsonArr;
    }

    //查询工单中是否有未完成的
    public static function getIsFinish($data,$type='')
    {
        if($data){
            //返回具体的哪个技师在服务中在服务中
            if($type){

                foreach($data as $v){
                    if($v['status'] ==2 || $v['status'] == 3){
                        return $v['technician_id'];
                    }
                }
            }
            $statusData = implode(',',array_unique(array_column($data,'status','id')));
            if(in_array($statusData,[2,3])){
                return true;
            }
            return false;
        }
        return false;
    }
}
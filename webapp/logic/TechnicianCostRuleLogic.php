<?php


namespace webapp\logic;
use webapp\models\BaseModel;
use Yii;
use common\models\Region;
use common\helpers\Helper;
use webapp\models\TechnicianCostRule;
use webapp\models\TechnicianCostRuleItem;
use webapp\models\CostItem;
use webapp\models\Technician;
use webapp\models\TechnicianCostRuleRelation;


/**
 * Created by PhpStorm.
 * User: tangfan
 * Date: 2018/1/31
 * Time: 11:23
 */

class TechnicianCostRuleLogic extends BaseLogic
{

   public static function index(){
       $param = [];
       $param['technician_cost_name']   = trim(Yii::$app->request->get('technician_cost_name'));
       $technicianName = trim(Yii::$app->request->get('technician_name'));
       if($technicianName){
           $technician = Technician::getTechnicianId(['like','name',$technicianName]);
           if($technician){
               foreach($technician as $v){
                   $technicianIds[] = $v['id'];
               }
               $param['technician_ids'] = $technicianIds;
           }

       }


       $param['currentPage']  = Yii::$app->request->get('page',1);
       return TechnicianCostRule::getList($param);
}

    public static function add(){
        $data['technician_cost_name']    = Yii::$app->request->post('technician_cost_name');
        $technician             = explode(',',trim(Yii::$app->request->post(',',Yii::$app->request->post('technician')),','));
        //$districtIds = array_filter(Yii::$app->request->post('districtIds',''));
        $item   =  Yii::$app->request->post('cost_item');
        $data['src_type']               =  BaseModel::SRC_FWS;
        $data['src_id']                 =  BaseLogic::getManufactorId();
        $data['create_time']            = $data['update_time']            = time();
        $data['oprate_id']              =  Yii::$app->user->id;
        //判断该数据是否已经存在，如果存在，不能继续添加规则
        $res = TechnicianCostRule::add($data,$item,$technician);
        if($res){
            return self::success('');
        }else{
            return self::error('数据提交失败');
        }

    }


    public static function showEdit(){
      $ruleId = Yii::$app->request->get('id');

      $info = TechnicianCostRule::findOne(['id'=>$ruleId]);
        $costItemList = TechnicianCostRuleItem::getAll(['tech_cost_rule_id'=>$ruleId]);
        $itemList       = CostItem::getList();
        foreach($itemList as $v){
            $list[$v['cs_id']] = $v['cs_name'];
        }
        $cost = [];
        foreach($costItemList as &$v){
            //if(in_array($list[$v['cost_item_id']],$costItemList)){
                $v['cs_name'] = $list[$v['cost_item_id']];
                $v['cs_id']   = $v['cost_item_id'];
                $cost[] = $v;
           // }

        }

        $technicianList = Technician::getList(['store_id'=>BaseLogic::getManufactorId(),'audit_status'=>5],'',0,100);
        $techList = TechnicianCostRuleRelation::getAll(['tech_cost_rule_id'=>$ruleId]);

        if(!empty($techList)){
            foreach($techList as $v){
                $tech[] = $v['technician_id'];
            }
        }

        foreach($technicianList['list'] as &$v){
            if(!empty($tech)&&in_array($v['id'],$tech)){
                $v['check_status']=1;
            }else{
                $v['check_status']=0;
            }
        }

        return $data = [
                        'info'=>$info,
                        'costItem'=>$cost,
                        'technician'=>$technicianList['list']
        ];
    }


    public static function edit(){
        $data['id'] = intval(Yii::$app->request->post('id'));
        $data['technician_cost_name']    = trim(Yii::$app->request->post('technician_cost_name'));
        $technician             = explode(',',trim(Yii::$app->request->post(',',Yii::$app->request->post('technician')),','));
        $item   =  Yii::$app->request->post('cost_item');
        $data['src_type']               =  BaseModel::SRC_FWS;
        $data['src_id']                 =  BaseLogic::getManufactorId();
        $data['create_time']            = $data['update_time']            = time();
        $data['oprate_id']              =  Yii::$app->user->id;
        //判断该数据是否已经存在，如果存在，不能继续添加规则

        $res = TechnicianCostRule::edit($data,$item,$technician);
        if($res){
            return self::success('');
        }else{
            return self::error('数据提交失败');
        }

    }




}
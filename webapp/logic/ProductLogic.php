<?php
/**
 * Created by PhpStorm.
 * 产品管理
 * User: xingq
 * Date: 2018/3/20
 * Time: 13:56
 */

namespace webapp\logic;

use common\models\Contract;
use webapp\controllers\ContractController;
use webapp\models\Department;
use webapp\models\ManufactorBrand;
use webapp\models\ManufactorClass;
use webapp\models\ProductMaterials;
use Yii;
use common\models\ServiceClass;
use common\models\Product;
use common\models\ServiceBrand;
use common\models\DepotProdStock;
use webapp\models\Character;

class ProductLogic extends BaseLogic
{

    /**
     * 获取产品管理所需外部数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 15:25
     * @return mixed
     */
    public static function getBeforeList($status = [1,2])
    {
        $departId = self::getDepartmentId();
        $topId    = self::getTopId();
        $dcId     = self::getDirectCompanyId();
    
        $data = [];
        //自有品牌数据
        $data['brand'] = self::getBrand([],$status);
    
        //自有分类数据
        $data['class'] = self::getClass(0,$status);
    
        //计量单位
        $data['unit']  = self::getUnit();
    
        //质保单位
        $warranty = self::getWarranty($departId, $topId);
        if ($warranty['code'] == 200) {
            $data['warranty'] = $warranty['data'];
        } else {
            $data['warranty'] = [];
        }
        
        $data['dcId'] = $dcId;
    
        if ($topId == $dcId) {
            $data['flag'] = 1;
        } else {
            $data['flag'] = 2;
        }
    
        return $data;
    }

    /**
     * 搜索条件处理
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 15:26
     * @return array
     */
    public static function search()
    {
        $dcId     = self::getDirectCompanyId();
        $q        = trim(Yii::$app->request->get('q', ''));  //这个参数不要删除，用于添加订单联想搜索产品时使用，对本产品搜索功能没有影响  2018-5-11
        $prodName = trim(Yii::$app->request->get('prod_name', ''));
        $prodNo = trim(Yii::$app->request->get('prod_no', ''));
        $typeId   = intval(Yii::$app->request->get('type_id', 0));
        $brandId  = intval(Yii::$app->request->get('brand_id', 0));
        $classId  = Yii::$app->request->get('class_id', 0);
        $type   = intval(Yii::$app->request->get('type', 0));

        $status = intval(Yii::$app->request->get('status', 0));
        
        if ($q) {
            $prodName = $q;  //这个参数不要删除，用于添加订单联想搜索产品时使用，对本产品搜索功能没有影响  2018-5-11
        }
        
        $where = [];
        
        if ($q) {
            $where[] = ['or', ['like', 'prod_name', $prodName], ['like', 'prod_no', $prodName]];
        } else {
            if ($prodName != '') {
                $where[] = ['like', 'prod_name', $prodName];
            }
        }
        if ($prodNo != '') {
            $where[] = ['like', 'prod_no', $prodNo];
        }
        if ($typeId != 0) {
            $where[] = ['=', 'type_id', $typeId];
        }
    
        if ($brandId != 0) {
            $where[] = ['=', 'brand_id', $brandId];
        } else {
            $brand = ManufactorBrand::getList(['direct_company_id' => $dcId]);
        
            $brandId = array_column($brand, 'brand_id');
            
            if (!empty($brandId)) {
                $where[] = ['in', 'brand_id', $brandId];
            }else{
                $where[] = ['=', 'brand_id', -1];
            }
            
        }
    
        if ($classId != 0) {
            
            if (!is_array($classId)) {
                $classId = explode(',',$classId);
            }
            
            $where[] = ['in', 'class_id', $classId];
            $where['classId'] = $classId;
        } else {
            $class = ManufactorClass::getList(['direct_company_id' => $dcId]);
        
            $classId = array_column($class, 'class_id');
    
            if (!empty($brandId)) {
                $where[] = ['in', 'class_id', $classId];
            }else{
                $where[] = ['=', 'class_id', -1];
            }
            
        }
        if ($type == 1) { // 标记是否查询物料清单  1 意向产品 排除物料清单  2 客户产品 查全部
            $where[] = ['in', 'type_id', [1,2]];
        }
        if ($status != 0) {
            $where[] = ['=', 'status', $status];
        }
    
        return $where;
    }

    /**
     * 产品列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/20
     * Time: 14:01
     * @return string
     */
    public static function index()
    {
    
        $topId     = self::getTopId();
        $parentIds = self::getParentIds();
        $dcId      = self::getDirectCompanyId();
    
        $page     = intval(Yii::$app->request->get('page', 1));
        $pageSize = intval(Yii::$app->request->get('pageSize', 10));
        
        if (!empty($parentIds)) {
            array_push($parentIds,$dcId);
            $map = ['direct_company_id' => $parentIds, 'department_top_id' => $topId];
        } else {
            $map = ['direct_company_id' => $dcId, 'department_top_id' => $topId];
        }
    
        $where = self::search();
        
        $classId = [];
        if (isset($where['classId'])) {
            $classId = $where['classId'];
            unset($where['classId']);
        }
        
        $data = Product::index($map, $where, $page, $pageSize);
        if (!empty($data['list'])) {
            $data['list'] = self::changeData($data['list'], 2);
        }

        //类目搜索条件暂存
        $className = ServiceClass::getName($classId);
        
        if (!empty($className))
        {
            if (is_array($className)) {
                $data['className'] = implode(';',$className);
            }else{
                $data['className'] = $className;
            }
        }else{
           
            $data['className'] = '';
        }

        if (!empty($classId)) {
            if (is_array($classId)) {
                $data['classId'] = implode(',',$classId);
            }else{
                $data['classId'] = $classId;
            }
        }else{
            $data['classId']   = '';
        }
        
        return $data;
    }

    /**
     * 表单接收数据判断
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 10:45
     * @return array
     */
    public static function receivePost()
    {
        $id               = intval(Yii::$app->request->post('id', 0));                //产品id
        $prodNo           = trim(Yii::$app->request->post('prod_no', ''));            //产品编号
        $prodName         = trim(Yii::$app->request->post('prod_name', ''));          //产品名称
        $typeId           = intval(Yii::$app->request->post('type_id', 0));           //产品类型
        $brandId          = intval(Yii::$app->request->post('brand_id', 0));          //品牌id
        $classId          = intval(Yii::$app->request->post('class_id', 0));          //分类id
        $model            = trim(Yii::$app->request->post('model', ''));              //型号
        $warrantyNum      = intval(Yii::$app->request->post('warranty_num', 0));      //质保时间
        $warrantyType     = intval(Yii::$app->request->post('warranty_type', 0));     //质保单位
        $unitId           = intval(Yii::$app->request->post('unit_id', 0));           //计量单位
        $salePrice        = trim(Yii::$app->request->post('sale_price', ''));         //市场价
        $purchasePrice    = trim(Yii::$app->request->post('purchase_price', ''));     //采购价格
        $purchaseMaxPrice = trim(Yii::$app->request->post('purchase_max_price', '')); //最高采购价格
        $suggestSalePrice = trim(Yii::$app->request->post('suggest_sale_price', '')); //销售建议价格
        $saleMaxPrice     = trim(Yii::$app->request->post('sale_max_price', ''));     //最高销售价格
        $contract         = Yii::$app->request->post('contract',[]);                  //合同组（关联数组）
        $materials        = Yii::$app->request->post('materials',[]);                 //物料包组（关联数组）
        $prod             = Yii::$app->request->post('prod',[]);                      //产品组（二维数组   需要产品id(prod_id)、产品价格（prod_sale_price）、产品数量(prod_num)）
        
        if ($prodName == '') {
            return self::error('请输入产品名称', 20001);
        }
        if ($typeId == 0) {
            return self::error('请选择产品类型', 20002);
        }
        if ($brandId == 0) {
            return self::error('请选择品牌', 20003);
        }
        if ($classId == 0) {
            return self::error('请选择产品分类', 20004);
        }
        if ($unitId == 0) {
            return self::error('请选择计量单位', 20006);
        }
        if ($typeId != 3)
        {
            if ($salePrice == '') {
                return self::error('请输入市场价', 20007);
            }
        }else{
            if (empty($prod)) {
                return self::error('请添加物料列表产品', 20008);
            }
            else
            {
                foreach ($prod as $key => $val) {
                    if (isset($val['prod_num']) && $val['prod_num'] == '') {
                        return self::error('请添加物料列表产品数量', 20008);
                    }
                }
            }
        }
    
        $dcId  = self::getDirectCompanyId();
        $topId = self::getTopId();
        
        if ($prodNo != '') {
            if ($id != 0) {
                $product = Product::getOne(['direct_company_id' => $dcId, 'department_top_id' => $topId, 'prod_no' => $prodNo]);
                if (!empty($product)) {
                    if ($product->id != $id) {
                        return self::error('此编号已被占用，请换一个', 20008);
                    }
                }
            
            } else {
                $product = Product::getOne(['direct_company_id' => $dcId, 'department_top_id' => $topId, 'prod_no' => $prodNo]);
                if (!empty($product)) {
                    return self::error('此编号已被占用，请换一个', 20009);
                }
            }
        
        }
    
        if (!empty($prodName) && $classId != 0 && $id == '') {
            $product = Product::getOne(['direct_company_id' => $dcId, 'department_top_id' => $topId, 'prod_name' => $prodName, 'class_id' => $classId]);
            if (!empty($product)) {
                return self::error('此分类下已存在当前产品名称，请换一个', 20010);
            }
        }
    
        if (($warrantyNum == 0 && $warrantyType > 0) || ($warrantyNum > 0 && $warrantyType == 0)) {
            if ($warrantyNum == 0) {
                return self::error('请输入质保期', 20011);
            }
            if ($warrantyType == 0) {
                return self::error('请选择单位', 20012);
            }
        
        }
    
        $data = [];
        if ($id != 0) {
            $data['product']['id'] = $id;
        }
    
        $time = time();
    
        $data['product']['direct_company_id'] = $dcId;
        $data['product']['department_top_id'] = $topId;
        $data['product']['prod_no']           = $prodNo;
        $data['product']['prod_name']         = $prodName;
        $data['product']['type_id']           = $typeId;
        $data['product']['brand_id']          = $brandId;
        $data['product']['class_id']          = $classId;
        $data['product']['status']            = 1;
        $data['product']['is_self']           = 1;
        $data['product']['create_user_id']    = Yii::$app->user->id;
        $data['product']['create_time']       = $time;
        $data['product']['update_time']       = $time;
        $data['product']['unit_id']           = $unitId;
        
        $departId = self::getDepartmentId();
        $topId    = self::getTopId();
        
        //判断产品类型
        if ($typeId == 3)
        {
            //物料清单
            
            //物料列表
            $totalPrice = '';
            foreach ($prod as $key => $val) {
                $totalPrice[$key] = bcmul($val['prod_num'],$val['prod_sale_price'],2);
            }
            
            if ($id == 0)
            {
                foreach ($prod as $key => $val) {
                    $data['materials'][$key]['parent_id']   = '';
                    $data['materials'][$key]['prod_id']     = $val['prod_id'];
                    $data['materials'][$key]['num']         = $val['prod_num'];
                    $data['materials'][$key]['price']       = $val['prod_sale_price'];
                    $data['materials'][$key]['status']      = 1;
                    $data['materials'][$key]['is_directly'] = 1;
                }
            }
            else
            {
                $productMaterials = ProductMaterials::getList(['parent_id'=>$id,'status'=>1,'is_directly'=>1]);
                $oldProdId = array_column($productMaterials,'prod_id','id');
                $newProdId = array_column($prod,'prod_id');
                $addProdId = array_diff($newProdId,$oldProdId);
                $delProdId = array_diff($oldProdId,$newProdId);
                
                foreach ($prod as $key => $val) {
                    //组合添加或者编辑数据
                    if (in_array($val['prod_id'],$addProdId))
                    {
                        $data['addMaterials'][$key]['parent_id']   = $id;
                        $data['addMaterials'][$key]['prod_id']     = $val['prod_id'];
                        $data['addMaterials'][$key]['num']         = $val['prod_num'];
                        $data['addMaterials'][$key]['price']       = $val['prod_sale_price'];
                        $data['addMaterials'][$key]['status']      = 1;
                        $data['addMaterials'][$key]['is_directly'] = 1;
                    }else{
                        $data['editMaterials'][$key]['parent_id']   = $id;
                        $data['editMaterials'][$key]['prod_id']     = $val['prod_id'];
                        $data['editMaterials'][$key]['num']         = $val['prod_num'];
                        $data['editMaterials'][$key]['price']       = $val['prod_sale_price'];
                    }
                    
                }
                
                if (!empty($delProdId))
                {
                    //组合删除数据
                    foreach (array_keys($delProdId) as $key => $val) {
                        $data['delMaterials'][$key]['id']     = $val;
                        $data['delMaterials'][$key]['status'] = 2;
                    }
                }
            }
     
            $data['product']['sale_price'] = array_sum($totalPrice);
            
            //合同
            if ($id != 0)
            {
                $cont = Contract::getContractDetail(['b.prod_id'=>$id,'b.status'=>1]);
                if (!empty($cont))
                {
                    $contId = array_unique(array_column($cont,'parent_id','id'));
        
                    $addContId = array_diff($contract,$contId);
                    $delContId = array_diff($contId,$contract);
                    
                    //添加合同详情数据
                    if (!empty($addContId))
                    {
                        foreach ($addContId as $key => $val) {
                            $data['editContractDetail'][$key]['department_id']     = $departId;
                            $data['editContractDetail'][$key]['department_top_id'] = $topId;
                            $data['editContractDetail'][$key]['parent_id']         = $val;
                            $data['editContractDetail'][$key]['prod_id']           = $id;
                            $data['editContractDetail'][$key]['prod_type']         = 2;
                            $data['editContractDetail'][$key]['prod_num']          = 1;
                            $data['editContractDetail'][$key]['price']             = $data['product']['sale_price'];
                            $data['editContractDetail'][$key]['amount']            = $data['product']['sale_price'];
                            $data['editContractDetail'][$key]['rate']              = 100;
                            $data['editContractDetail'][$key]['total_amount']      = $data['product']['sale_price'];
                            $data['editContractDetail'][$key]['sale_price']        = $data['product']['sale_price'];
                            $data['editContractDetail'][$key]['finish_num']        = 0;
                            $data['editContractDetail'][$key]['total_num']         = 1;
                            $data['editContractDetail'][$key]['status']            = 1;
                        }
                        //修改计算合同价格
                        $contData = Contract::findAll(['id'=>$addContId]);
                        
                        if (!empty($contData))
                        {
                            foreach ($contData as $key => $val) {
                                $data['editContract'][$key]['id']           = $val['id'];
                                $data['editContract'][$key]['total_amount'] = bcadd($val['total_amount'],$data['product']['sale_price'],2);
                            }
                        }
                    }
        
                    //被删除项
                    if (!empty($delContId)) {
                        $data['delContractDetail'] = array_keys($delContId);
            
                        //修改计算物料清单价格
                        $contData = Contract::findAll(['id'=>$delContId]);
                        if (!empty($contData))
                        {
                            foreach ($contData as $key => $val) {
                                $data['delContract'][$key]['id']           = $val['id'];
                                $data['delContract'][$key]['total_amount'] = bcsub($val['total_amount'],$data['product']['sale_price'],2);
                            }
                        }
                    }
                }
                else
                {
                    $cont = Contract::findAll(['id'=>$contract]);
                    
                    foreach ($cont as $key => $val) {
                        
                        //合同主表价格修改
                        $data['editContract'][$key]['id']           = $val['id'];
                        $data['editContract'][$key]['total_amount'] = bcadd($val['total_amount'],$data['product']['sale_price'],2);
        
                        //合同详情表数据添加
                        $data['editContractDetail'][$key]['department_id']     = $departId;
                        $data['editContractDetail'][$key]['department_top_id'] = $topId;
                        $data['editContractDetail'][$key]['parent_id']         = $val['id'];
                        $data['editContractDetail'][$key]['prod_id']           = $id;
                        $data['editContractDetail'][$key]['prod_type']         = 2;
                        $data['editContractDetail'][$key]['prod_num']          = 1;
                        $data['editContractDetail'][$key]['price']             = $data['product']['sale_price'];
                        $data['editContractDetail'][$key]['amount']            = $data['product']['sale_price'];
                        $data['editContractDetail'][$key]['rate']              = 100;
                        $data['editContractDetail'][$key]['total_amount']      = $data['product']['sale_price'];
                        $data['editContractDetail'][$key]['sale_price']        = $data['product']['sale_price'];
                        $data['editContractDetail'][$key]['finish_num']        = 0;
                        $data['editContractDetail'][$key]['total_num']         = 1;
                        $data['editContractDetail'][$key]['status']            = 1;
                    }
                }
            }
            else
            {
                $cont = Contract::findAll(['id'=>$contract]);
    
                foreach ($cont as $key => $val) {
                    //合同主表价格修改
                    $data['addContract'][$key]['id']           = $val['id'];
                    $data['addContract'][$key]['total_amount'] = bcadd($val['total_amount'],$data['product']['sale_price'],2);
        
                    //合同详情表数据添加
                    $data['addContractDetail'][$key]['department_id']     = $departId;
                    $data['addContractDetail'][$key]['department_top_id'] = $topId;
                    $data['addContractDetail'][$key]['parent_id']         = $val['id'];
                    $data['addContractDetail'][$key]['prod_id']           = '';
                    $data['addContractDetail'][$key]['prod_type']         = 2;
                    $data['addContractDetail'][$key]['prod_num']          = 1;
                    $data['addContractDetail'][$key]['price']             = $data['product']['sale_price'];
                    $data['addContractDetail'][$key]['amount']            = $data['product']['sale_price'];
                    $data['addContractDetail'][$key]['rate']              = 100;
                    $data['addContractDetail'][$key]['total_amount']      = $data['product']['sale_price'];
                    $data['addContractDetail'][$key]['sale_price']        = $data['product']['sale_price'];
                    $data['addContractDetail'][$key]['finish_num']        = 0;
                    $data['addContractDetail'][$key]['total_num']         = 1;
                    $data['addContractDetail'][$key]['status']            = 1;
                }
            }
        }
        else
        {
            //成品、配件
            $data['product']['model']              = $model;
            $data['product']['warranty_num']       = $warrantyNum;
            $data['product']['warranty_type']      = $warrantyType;
            $data['product']['sale_price']         = $salePrice;
            $data['product']['purchase_price']     = $purchasePrice;
            $data['product']['purchase_max_price'] = $purchaseMaxPrice;
            $data['product']['suggest_sale_price'] = $suggestSalePrice;
            $data['product']['sale_max_price']     = $saleMaxPrice;
    
            if ($id != 0)
            {
                $productMaterials = ProductMaterials::getList(['prod_id'=>$id,'status'=>1,'is_directly'=>2]);
                $parentId = array_unique(array_column($productMaterials,'parent_id','id'));

                $addId = array_diff($materials,$parentId);
                $delId = array_diff($parentId,$materials);
                
                //添加物料清单详情数据
                if (!empty($addId))
                {
                    foreach ($addId as $key => $val) {
        
                        $data['addMaterials'][$key]['parent_id']   = $val;
                        $data['addMaterials'][$key]['prod_id']     = $id != 0 ? $id : '';
                        $data['addMaterials'][$key]['num']         = 1;
                        $data['addMaterials'][$key]['price']       = $salePrice;
                        $data['addMaterials'][$key]['status']      = 1;
                        $data['addMaterials'][$key]['is_directly'] = 2;
                    }
                }
                
                //被删除项
                if (!empty($delId)) {
                    $data['delMaterials'] = array_keys($delId);
                }
            }
            else
            {
                //物料清单详情数据
                foreach ($materials as $key => $val) {
                    
                    $data['materials'][$key]['parent_id']   = $val;
                    $data['materials'][$key]['prod_id']     = $id != 0 ? $id : '';
                    $data['materials'][$key]['num']         = 1;
                    $data['materials'][$key]['price']       = $salePrice;
                    $data['materials'][$key]['status']      = 1;
                    $data['materials'][$key]['is_directly'] = 2;
                }
            }
        }

        return self::success($data);
    }

    /**
     * 添加
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 15:19
     * @return array
     */
    public static function add()
    {
        $data = self::receivePost();

        if ($data['code'] == 200) {
            $res = Product::add($data['data']);
            if ($res) {
                return self::success('', '成功');
            } else {
                return self::error('失败');
            }
        }
    
        return self::error($data['message']);
    }

    /**
     * 获取编辑页面数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 15:36
     * @return array
     */
    public static function editList()
    {
        $data = [];
    
        $id = Yii::$app->request->get('id', 0);
        $model = Product::getOne(['id' => $id]);
    
        if (empty($model)) {
            return self::error('失败', 20001);
        }
        
        $data['model'] = $model;
        if (isset($model->class_id) && $model->class_id > 0) {
        
            $class = ServiceClass::getName($model->class_id);
            $data['class'] = $class[$model->class_id];
        }
    
    
        if ($model->type_id != 3) {
            //获取关联物料清单
            $productMaterials = ProductMaterials::getList(['prod_id' => $id, 'status' => 1, 'is_directly' => 2]);
            $prodId = array_unique(array_column($productMaterials, 'parent_id'));
            $product = Product::getList(['id' => isset($prodId) ? $prodId : -1, 'status' => 1]);
            if (!empty($product)) {
                $data['materials'] = array_column($product, 'prod_name', 'id');
            }
        } else
        {
    
            //获取关联合同
            $contractData = ContractLogic::getList(1);
            if (!empty($contractData['list']))
            {
                
                $contractId = array_column($contractData['list'],'id');
                $contract = Contract::getContractDetail(['a.id'=>$contractId,'b.prod_id'=>$id,'b.status'=>1]);
                
                if (!empty($contract)) {
                    $data['contract'] = array_column($contract,'subject','parent_id');
                }
            }
         
            
            //获取物料列表
            $productMaterials = ProductMaterials::getList(['parent_id'=> $id,'status'=> 1,'is_directly' => 1]);
            
            if (!empty($productMaterials))
            {
                $prodId = array_column($productMaterials,'prod_id','id');
                $prodNum = array_column($productMaterials,'num','prod_id');
                $product = self::changeData(Product::getList(['id'=>$prodId]),1);
                
                foreach ($product as $key => $val) {
                    if (isset($prodNum[$val['id']])) {
                        $product[$key]['prod_num'] = $prodNum[$val['id']];
                        $product[$key]['total_price'] =  bcmul($prodNum[$val['id']],$val['sale_price'],2);
                    }
                }
                $data['prod'] = $product;
            }
        }
        
        if ($data) {
            return self::success($data, '成功');
        }
        return self::error('失败',20002);
    }

    /**
     * 编辑
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 15:19
     * @return array
     */
    public static function edit()
    {
        $data = self::receivePost();
        if ($data['code'] == 200) {
            $res = Product::edit($data['data']);
            if ($res) {
                return self::success('', '成功');
            } else {
                return self::error('失败');
            }
        }
    
        return self::error($data['message']);
    }

    /**
     * ajax更改状态
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 17:42
     * @return array
     */
    public static function ajaxEditStatus()
    {
        $topId  = self::getTopId();
        $dcId   = self::getDirectCompanyId();
        $id     = intval(Yii::$app->request->post('id', 0));
        $status = intval(Yii::$app->request->post('status', 0));
        $is_off = intval(Yii::$app->request->post('is_off', 0));
        
        $data = [];
        if ($id == 0) {
            return self::error('产品标识错误', 20001);
        } else {
            $data['id'] = $id;
        }
        if ($status == 0) {
            return self::error('产品状态错误', 20002);
        } else {
            $data['status'] = $status;
        }

        
        $product = Product::getOne(['direct_company_id' => $dcId, 'department_top_id' => $topId, 'id' => $id, 'is_self' => 1]);
        if (empty($product)) {
            return self::error('该产品不属于您，您不能操作该产品', 20003);
        }
        
        $class = ServiceClass::findOne(['id'=>$product['class_id'],'status'=>2]);
        if (!empty($class) && $status == 1) {
            return self::error('该分类被禁用，请启用该分类后再次启用该产品', 20004);
        }
        
        $brand = ServiceBrand::findOne(['id'=>$product['brand_id'],'status'=>2]);
        if (!empty($brand) && $status == 1) {
            return self::error('该品牌被禁用，请启用该品牌后再次启用该产品', 20005);
        }
        
        $depotProdStock = DepotProdStock::getList(['direct_company_id' => $dcId, 'prod_id' => $id]);
        $depotProdNum = array_sum(array_column($depotProdStock,'num'));
        if ($depotProdNum >0 && $status == 2) {
            return self::error('此产品有库存，无法禁用', 20006);
        }
        //print_r($is_off);die;
        //禁用操作
        if($is_off == 1 && $status == 2){
            $res = Product::editStatus($data);
        }
        if($is_off == 0 && $status == 2){
            return self::success('', '可以禁用','20009');
        }
        //启用操作
        if($is_off == 2 && $status == 1){
            $res = Product::editStatus($data);
        }
        if ($res) {
            return self::success('', '成功');
        }
        return self::error('失败', 20007);
    }

    /**
     * 组合列表显示数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 17:50
     * @param $list
     * @return mixed
     */
    public static function changeData($list, $type)
    {
        //品牌
        $brandId = array_unique(array_column($list, 'brand_id'));
        $brand   = ServiceBrand::getName($brandId);
    
        //分类
        $classId = array_unique(array_column($list, 'class_id'));
        $class   = self::getClassName($classId, $type);
    
        //单位
        $unit = self::getUnit();
        
        //直属公司id
        $dcId  = self::getDirectCompanyId();
        if(!empty($list))  {
            foreach ($list as $key => $val) {
                $list[$key]['model'] = isset($val['model'])?$val['model']:'';
                //品牌
                if (isset($brand[$val['brand_id']])) {
                    $list[$key]['brand_name'] = $brand[$val['brand_id']];
                    $list[$key]['brand_id'] = $val['brand_id'];
                } else {
                    $list[$key]['brand_name'] = '';
                    $list[$key]['brand_id'] = '';
                }

                if ($type == 1)
                {
                    if (isset($class[$val['class_id']])) {
                        $list[$key]['class_name'] = $class[$val['class_id']];
                        $list[$key]['class_id'] = $val['class_id'];
                    } else {
                        $list[$key]['class_name'] = '';
                        $list[$key]['class_id'] = '';
                    }
                }
                else
                {
                    if (isset($class[$val['class_id']]['class_name'])) {
                        $list[$key]['class_name'] = $class[$val['class_id']]['class_name'];
                        $list[$key]['class_id'] = $val['class_id'];
                    } else {
                        $list[$key]['class_name'] = '';
                        $list[$key]['class_id'] = '';
                    }
                }
                
                if ($val['status'] == 1) {
                    $list[$key]['status_name'] = '启用';
                } else {
                    $list[$key]['status_name'] = '禁用';
                }

                if (isset($val['direct_company_id']))
                {
                    $departName = Department::getDepartmentName($val['direct_company_id']);
                    $list[$key]['department_name'] = isset($departName) ? $departName : '';
                }

                if (isset($val['direct_company_id']) && $val['direct_company_id'] == $dcId) {
                    $list[$key]['role'] = 1;
                }else{
                    $list[$key]['role'] = 2;
                }
                
                if (!empty(Product::getTypeName($val['type_id']))) {
                    $list[$key]['type_name'] = Product::getTypeName($val['type_id']);
                }else{
                    $list[$key]['type_name'] = '';
                }
    
                
                if (!empty($list[$key]['unit_id']))
                {
                    if (isset($unit[$val['unit_id']])) {
                        $list[$key]['unit_name'] = $unit[$val['unit_id']];
                    }
                }
                
            }
        }
        return $list;
    }

    /**
     * 获取调用产品列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 17:54
     * @return array
     */
    public static function getList()
    {
        $topId     = self::getTopId();
        $parentIds = explode(',',implode(',',self::getParentIds()).','.Yii::$app->user->getIdentity()->department_id);
        $dcId      = self::getDirectCompanyId();
        $stockType = intval(Yii::$app->request->get('stock_type', 0));
        $prodName = trim(Yii::$app->request->get('prod_name', ''));
        $prodNo   = trim(Yii::$app->request->get('prod_no', ''));
        $page     = intval(Yii::$app->request->get('page', 1));
        $pageSize = intval(Yii::$app->request->get('pageSize', 10));

        //出库非良品时  只能选择包含非良品的产品
        if($stockType == 10)
        {
            $where[] = ['=','a.status',1];
            $where[] = ['>','b.bad_num',0];
            if ($prodName != '') {
                $where[] = ['like', 'a.prod_name', $prodName];
            }
            if ($prodNo != '') {
                $where[] = ['like', 'a.prod_no', $prodNo];
            }
            if (!empty($parentIds)) {
                $map = ['a.direct_company_id' => $parentIds, 'a.department_top_id' => $topId];
            } else {
                $map = ['a.direct_company_id' => $dcId, 'a.department_top_id' => $topId];
            }
            $data = Product::badList($map, $where, $page, $pageSize);
        }else {
            $where[] = ['=','status',1];
            if ($prodName != '') {
                $where[] = ['like', 'prod_name', $prodName];
            }
            if ($prodNo != '') {
                $where[] = ['like', 'prod_no', $prodNo];
            }
            if (!empty($parentIds)) {
                $map = ['direct_company_id' => $parentIds, 'department_top_id' => $topId];
            } else {
                $map = ['direct_company_id' => $dcId, 'department_top_id' => $topId];
            }
            $data = Product::index($map, $where, $page, $pageSize);
        }
        if (!empty($data['list'])) {
            $data['list'] = self::changeData($data['list'], 1);
        }
        return $data;
    }

    /**
     * 获取产品信息
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/26
     * Time: 13:38
     * @return array
     */
    public static function getDetail($id = '')
    {
        if (!$id) {
            $id = intval(Yii::$app->request->get('id', 0));
        }
        if ($id == 0) {
            return self::error('获取产品信息失败', 20001);
        }
    
        $info = Product::getOne(['id' => $id], true);
        if (!$info) {
            return self::error('获取产品信息失败', 20001);
        }
    
        //单位
        if (!empty($info['unit_id']))
        {
            $unit = self::getUnit();
            if (isset($unit[$info['unit_id']])) {
                $info['unit_name'] = $unit[$info['unit_id']];
            }
        }

        //品牌
        if (!empty($info['brand_id']))
        {
            $brand = ServiceBrand::getName($info['brand_id']);
            if (isset($brand[$info['brand_id']])) {
                $info['brand_name'] = $brand[$info['brand_id']];
            } else {
                $info['brand_name'] = '';
            }
        }

        //类目
        if (!empty($info['class_id']))
        {
            $class = self::getClassName($info['class_id'],2);
            if (isset($class[$info['class_id']])) {
                $info['class_name'] = $class[$info['class_id']]['class_name'];
            } else {
                $info['class_name'] = '';
            }
        }
        
        $info['type_name']          = Product::getTypeName($info['type_id']);
        $info['purchase_price']     = isset($info['purchase_price']) ? $info['purchase_price'] : "";
        $info['purchase_max_price'] = isset($info['purchase_max_price']) ? $info['purchase_max_price'] : "";
        $info['sale_max_price']     = isset($info['sale_max_price']) ? $info['sale_max_price'] : "";
        $info['suggest_sale_price'] = isset($info['suggest_sale_price']) ? $info['suggest_sale_price'] : "";
        $info['purchase_price']     = isset($info['purchase_price']) ? $info['purchase_price'] : "";
        $info['depot_num']          = 0;
        return self::success($info, '成功');
    
    }

    //将数据按照拼音首字母重新排序
    public static function setRank($data = [])
    {
        $twoArray = '1'; //twoArray 结果是否需要是二维数组排列   需要的话不用传，不需要传入值
        $prod_character = (new Character)->groupByInitials($data, 'prod_name', $twoArray);
        $prod_character = array_values($prod_character);  //将数组键重新排列
        return $prod_character;
    
    }

    /**
     * 获取类目名称
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/12
     * Time: 16:55
     * @param $classId
     * @param $type
     * @return array
     */
    public static function getClassName($classId, $type)
    {

        $data = [];
        if ($type == 1) {
            $data = ServiceClass::getName($classId);
            return $data;
        }
        $class = ServiceClass::getList(['id' => $classId]);
        $classPids = array_column($class, 'parent_ids', 'id');
        $status    = array_column($class, 'status', 'id');
        
        foreach ($classPids as $key => $val) {
            if (empty($val)) {
                $classPids[$key] = $key;
            }else{
                $classPids[$key] = $val.','.$key;
            }
        }
    
        $id = array_unique(explode(',', implode(',', $classPids)));
    
        $className = ServiceClass::getName($id);
    
            foreach ($classPids as $key => $val) {
                if (!empty($val)) {
                    $classPids[$key] = explode(',', $val);
                }
                
            }
        
            foreach ($classPids as $key => $val) {
                if (is_array($val)) {
                    foreach ($val as $k => $v) {
                        if (isset($className[$v])) {
                            $data[$key][$v] = $className[$v];
                        }
                    }
                }
            }

            foreach ($data as $key => $val) {
                if (!empty($val)) {
                    $data[$key]['class_name'] = implode('-', $val);
                    if (isset($status[$key])) {
                        $data[$key]['status'] = $status[$key];
                    }
                }
            }
        
            return $data;
        }
    
    /**
     * 产品详情
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/30
     * Time: 17:52
     * @return array
     */
    public static function getView()
    {
        $id = trim(Yii::$app->request->get('id',0));
        
        if ($id == 0) {
            return self::error('数据错误',20001);
        }
        
        $dcId  = self::getDirectCompanyId();
        $topId = self::getTopId();
        
        $product = Product::getOne(['id'=>$id,'department_top_id'=>$topId],1);
        
        if (empty($product)) {
            return self::error('数据错误',20002);
        }
    
        //单位
        $unit = self::getUnit();
        if (isset($unit[$product['unit_id']])) {
            $product['unit_name'] = $unit[$product['unit_id']];
        }else{
            $product['unit_name'] = '';
        }
    
        //品牌
        $brand = self::getBrand($product['brand_id']);
        if (isset($brand[$product['brand_id']])) {
            $product['brand_name'] = $brand[$product['brand_id']];
        } else {
            $product['brand_name'] = '';
        }
    
        //类目
        $class = self::getClassName($product['class_id'],2);
        if (isset($class[$product['class_id']])) {
            $product['class_name'] = $class[$product['class_id']][$product['class_id']];
        } else {
            $product['class_name'] = '';
        }
        
        //质保期

        $warrType = self::getWarranty();
        
        if (isset($warrType['data'][$product['warranty_type']])) {
            $product['warranty_type_name'] = $warrType['data'][$product['warranty_type']];
        }else{
            $product['warranty_type_name'] = '';
        }
        
        //产品类型
        if (!empty(Product::getTypeName($product['type_id'])))  {
            $product['type_name'] = Product::getTypeName($product['type_id']);
        }else{
            $product['type_name'] = '';
        }
        
        
        return self::success($product);

    }
    
    /**
     * 获取物料包列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/31
     * Time: 18:22
     * @return array
     */
    public static function materials()
    {
        $topId     = self::getTopId();
        $parentIds = explode(',',implode(',',self::getParentIds()).','.Yii::$app->user->getIdentity()->department_id);
        $dcId      = self::getDirectCompanyId();
    
        $prodName = trim(Yii::$app->request->get('prod_name', ''));
        $prodNo   = trim(Yii::$app->request->get('prod_no', ''));
        
        $where = [
            ['=','status',1],
            ['=','type_id',3],
        ];
    
        if ($prodName != '') {
            $where[] = ['like', 'prod_name', $prodName];
        }
        if ($prodNo != '') {
            $where[] = ['like', 'prod_no', $prodNo];
        }
    
        $page     = intval(Yii::$app->request->get('page', 1));
        $pageSize = intval(Yii::$app->request->get('pageSize', 10));
    
        if (!empty(array_filter($parentIds))) {
            $map = ['direct_company_id' => array_filter($parentIds), 'department_top_id' => $topId];
        } else {
            $map = ['direct_company_id' => $dcId, 'department_top_id' => $topId];
        }
    
        $product = Product::index($map, $where, $page, $pageSize);
        
        if (empty($product['list'])) {
            return $product;
        }
        
        $id = array_column($product['list'],'id','id');
        
        $res = ProductMaterials::getMaterials(['b.parent_id'=>$id,'b.status'=>1]);
   
        $productMaterials = [];
        foreach ($res as $key => $val) {
            if (isset($id[$val['parent_id']])) {
                $productMaterials[$val['parent_id']][$key] = $val;
            }
        }
    
        $list = [];
        foreach ($productMaterials as $key => $val) {
            $num   = '';
            foreach ($val as $k => $v) {
                if ($key == $v['parent_id'])
                {
                    $num   = $num + $v['num'];
    
                    $list[$v['parent_id']]['id']          = $v['parent_id'];
                    $list[$v['parent_id']]['prod_no']     = $v['prod_no'];
                    $list[$v['parent_id']]['prod_name']   = $v['prod_name'];
                    $list[$v['parent_id']]['total_num']   = $num;
                    $list[$v['parent_id']]['total_price'] = $v['sale_price'];

                }
            }
        }
    
        $product['list'] = $list;
  
        return $product;
        
    }
    
    /**
     * 合同与产品管理共用弹窗列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/8/1
     * Time: 14:46
     * @param int $type
     * @return array
     */
    public static function shareList($type = 0)
    {
        $topId     = self::getTopId();
        $parentIds = explode(',',implode(',',self::getParentIds()).','.Yii::$app->user->getIdentity()->department_id);
        $dcId      = self::getDirectCompanyId();
    
        $prodName = trim(Yii::$app->request->get('prod_name', ''));
        $prodNo   = trim(Yii::$app->request->get('prod_no', ''));
    
        $where[] = ['=','status',1];
        
        if ($type != 0) {
            $where[] = ['in','type_id',[1,2,3]];
        }else{
            $where[] = ['in','type_id',[1,2]];
        }
    
        if ($prodName != '') {
            $where[] = ['like', 'prod_name', $prodName];
        }
        if ($prodNo != '') {
            $where[] = ['like', 'prod_no', $prodNo];
        }
    
        $page     = intval(Yii::$app->request->get('page', 1));
        $pageSize = intval(Yii::$app->request->get('pageSize', 10));
    
        if (!empty($parentIds)) {
            $map = ['direct_company_id' => $parentIds, 'department_top_id' => $topId];
        } else {
            $map = ['direct_company_id' => $dcId, 'department_top_id' => $topId];
        }
    
        $data = Product::index($map, $where, $page, $pageSize);
        
        if (!empty($data['list'])) {
            $data['list'] = self::changeData($data['list'], 1);
        }
        
        return $data;
    }
    
    /**
     * 合同与产品管理共用返回值详情
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/8/1
     * Time: 14:46
     * @return array
     */
    public static function shareDetail()
    {
        
        $type = intval(Yii::$app->request->get('type', 0));
        $id   = intval(Yii::$app->request->get('id', 0));
        $prod_arr   = Yii::$app->request->get('prod_arr','');
        if ($id == 0) {
            return self::error('获取产品信息失败', 20001);
        }
        
        $info = Product::getOne(['id' => $id,'status'=>1], true);
        if (!$info) {
            return self::error('获取产品信息失败', 20002);
        }
        
        if ($type == 1)
        {
            $productMaterials = ProductMaterials::find()->where(['prod_id'=>$id,'is_directly'=>2,'status'=>1])->asArray()->all();
            
            $prodMaterId = array_filter(array_unique(array_column($productMaterials,'parent_id','parent_id')));
            
            array_push($prodMaterId,$id);
            
            $product = Product::getList(['id'=>$prodMaterId,'status'=>1]);

            if (empty($product)) {
                return self::error('获取产品信息失败', 20003);
            }
            if($prod_arr){
                if(!is_array($prod_arr)){
                    $prod_arr = explode(',',$prod_arr);
                }
                foreach ($product as $k=>$v)
                {
                    if(in_array($v['id'],$prod_arr)){
                        unset($product[$k]);
                    }
                }
            }
            $product = self::changeData($product, 1);
            return self::success($product, '成功');
        }
        else
        {
            //单位
            if (!empty($info['unit_id']))
            {
                $unit = self::getUnit();
                if (isset($unit[$info['unit_id']])) {
                    $info['unit_name'] = $unit[$info['unit_id']];
                }
            }
    
            //品牌
            if (!empty($info['brand_id']))
            {
                $brand = ServiceBrand::getName($info['brand_id']);
                if (isset($brand[$info['brand_id']])) {
                    $info['brand_name'] = $brand[$info['brand_id']];
                } else {
                    $info['brand_name'] = '';
                }
            }
    
            //类目
            if (!empty($info['class_id']))
            {
                $class = self::getClassName($info['class_id'],2);
                if (isset($class[$info['class_id']])) {
                    $info['class_name'] = $class[$info['class_id']]['class_name'];
                } else {
                    $info['class_name'] = '';
                }
            }
            $info['type_name']          = Product::getTypeName($info['type_id']);
            $info['purchase_price']     = isset($info['purchase_price']) ? $info['purchase_price'] : "";
            $info['purchase_max_price'] = isset($info['purchase_max_price']) ? $info['purchase_max_price'] : "";
            $info['sale_max_price']     = isset($info['sale_max_price']) ? $info['sale_max_price'] : "";
            $info['suggest_sale_price'] = isset($info['suggest_sale_price']) ? $info['suggest_sale_price'] : "";
            $info['purchase_price']     = isset($info['purchase_price']) ? $info['purchase_price'] : "";
            $info['depot_num']          = 0;

            return self::success($info, '成功');
        }
        
    }
    
    /**
     * 配件包详情
     * User: sxz
     * Date: 2018/8/1
     * Time: 17:46
     * @param int $type
     * @return array
     */
    public static function materialsDetail()
    {
        $id   = intval(Yii::$app->request->get('id', 0));
        $productInfo = Product::getOne(['id'=>$id]);
        $productMaterials = ProductMaterials::find()->where(['parent_id'=>$id,'status'=>1,'is_directly'=>1])->asArray()->all();

        $prod_ids = array_column($productMaterials,'prod_id');
        //产品单价数组
        $prod_price_list = array_column($productMaterials,'price','prod_id');
        //产品数量数组
        $prod_num_list = array_column($productMaterials,'num','prod_id');
        $product_list = Product::getList(['id'=>$prod_ids,'status'=>1]);
        //单位
        $unit = self::getUnit();
        if($product_list)
        {
            foreach ($product_list as $key=>$val){

                $product_list[$key]['unit_name'] = isset($unit[$val['unit_id']])?$unit[$val['unit_id']]:'';
                $product_list[$key]['price'] = isset($prod_price_list[$val['id']])?$prod_price_list[$val['id']]:'';
                $product_list[$key]['num']   = isset($prod_num_list[$val['id']])?$prod_num_list[$val['id']]:'';
                $product_list[$key]['total_price']   = $product_list[$key]['num']*$product_list[$key]['price'];
            }
        }
        $product = self::changeData($product_list, 1);
        $product_total_num      = array_sum(array_column($productMaterials,'num'));
        $product_total_price    = isset($productInfo['sale_price'])?$productInfo['sale_price']:'';
        $data['product_list']   = $product;
        $data['total_info']     = ['total_num'=>$product_total_num,'total_price'=>$product_total_price];
        
        return $data;
    }
    
    
}
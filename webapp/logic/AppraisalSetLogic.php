<?php
    
    namespace webapp\logic;
    
    use Yii;
    use webapp\models\BaseModel;
    use common\models\AppraisalSetConfig;


    /**
     * @package webapp\logic
     * @desc 评论设置
     * @autor:sxz
     * @date :2018-10-15
     */
    
    class AppraisalSetLogic extends BaseLogic
    {
        /**
         * 修改,添加
         * @return array
         * @throws \Exception
         */
        public static function edit($type='')
        {
            $id              =  intval(Yii::$app->request->post('id',0));
            $daysNum         =  intval(Yii::$app->request->post('num',0));
            $data['id']                 =  $id;
            $data['direct_company_id']  =  self::getDirectCompanyId();
            $data['type']               =  3;
            $data['num']                =  $daysNum;
            $data['department_id']      =  self::getDepartmentId();
            $data['optare_id']          =  self::getLoginUserId();
            $res = AppraisalSetConfig::edit($data);
            if ($res) {
                return self::success('','保存成功！');
            }else{
                return self::error('保存失败');
            }
            
        }
    }
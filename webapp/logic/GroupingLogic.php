<?php
namespace webapp\logic;

use common\helpers\Helper;
use webapp\models\Department;
use webapp\models\GroupingRelTechnician;
use webapp\models\Technician;
use Yii;
use webapp\models\Grouping;
use webapp\models\BaseModel;
use webapp\models\Role;


class GroupingLogic extends BaseLogic
{

    /**
     * 列表
     */
    public static function getList($where=[],$user_id = 0)
    {
        $getDataAuth           = self::getListDataAuth(3,2);
        $departmentIds          = $getDataAuth['department_id'];
        $dataAuth               = $getDataAuth['dataAuth'];
        $createUserId           = $getDataAuth['create_user_id']?$getDataAuth['create_user_id']:'';
        $department_name        = $getDataAuth['department_name'] ? $getDataAuth['department_name'] : '';
        if($createUserId){
            $where['create_user_id'] = $createUserId;
        }
        $where['department_id'] = $departmentIds;

       /* echo '<pre>';
        print_R($dataAuth);*/
        $page          = intval(Yii::$app->request->get('page',1));
        $pageSize      = intval(Yii::$app->request->get('pageSize',10));
        $department_id = Yii::$app->request->get('department_id');

        $where['status'] = 1;
        if($user_id) {
            unset($where['department_id']);
            $where['create_user_id'] = $user_id;
        }
        $data = Grouping::getList($where,$page,$pageSize,$department_id);
        foreach($data['list'] as &$v){
            $v['department_name']      = $dataAuth[$v['department_id']]['name'];
        }

        if(in_array(self::getDatAuthId(),[2,3])) {
            $dId = self::getDirectCompanyId();

        }else{
            $dId = self::getDepartmentId();
        }
        $directCompany = Department::findOne(['id' => $dId]);
        $defaultName = $directCompany->name;
        $data['defaultName']     = $defaultName;
        $data['department_name'] = $department_name;

        return self::success($data);
    }

    /**
     * 删除
     * @return array
     */
    public static function del()
    {
        $id = intval(Yii::$app->request->get('id',0));
        //验证当前分组下是否有未完成的工单
        $groupTecs = GroupingRelTechnician::getGroupingTec($id);
        
        $tecIds = [];
        foreach ($groupTecs as $k => $v) {
            $tecIds[] = $v['technician_id'];
        }
        
        if($tecIds) {
            $post_data = ['techician_ids' => $tecIds];
            $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/sreach-group-work";
            $group_work_res = json_decode(Helper::curlPostJson($url,$post_data), true);
            if(!$group_work_res['success'] || !empty($group_work_res['data'])) {
                return self::error('该组成员仍有未完成工单，请完成后删除！');
            }
        }

        $res = Grouping::del($id, $tecIds);
        if($res){
            return self::success(['id'=>$id]);
        }
        return self::error('删除失败');
    }

    /**
     * 查询当前直属公司的所有技师
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getDefaultTechnician($where,$pageSize = 0)
    {
        $page     = intval(Yii::$app->request->get('page',1));
        $pageSize = $pageSize ? $pageSize : intval(Yii::$app->request->get('pagesize',10));
        $data = GroupingRelTechnician::getDefaultTechnician($where,$page,$pageSize,$select = "a.id,a.name,a.mobile,a.store_id");
        return self::success($data);
    }

    /**
     * 把技师加入分组里
     * @return array
     */
    public static function joinGrouping()
    {
        $technicianId = intval(Yii::$app->request->get('technician_id',0));
        $groupingId   = intval(Yii::$app->request->get('grouping_id',0));
        $groupObj = Grouping::findOne(['id' => $groupingId]);
        $res = GroupingRelTechnician::joinGrouping($groupObj, $technicianId,$groupingId);
        if($res){
            return self::success([]);
        }
        return self::error('修改失败');
    }

    /**
     * 把技师加入分组里
     * @return array
     */
    public static function batchJoinGrouping()
    {
        $technicianIds= Yii::$app->request->get('technician_ids',0);
        $groupingId   = intval(Yii::$app->request->get('grouping_id',0));
        //查询当前分组所属机构
        $groupArr = Grouping::findOne(['id' => $groupingId]);
        if($technicianIds && $groupingId>0)
        {
            $count = 0;
            foreach ($technicianIds as $val) {
                if( GroupingRelTechnician::joinGrouping($groupArr, $val,$groupingId)){
                    $count++;
                }
            }
            if($count == count($technicianIds)){
                return self::success([]);
            }
        }
        return self::error('添加失败');
    }

    /**
     * 查看组员
     * @return array
     */
    public static function getTechnicianList()
    {
        $groupingId = intval(Yii::$app->request->get('grouping_id',0));
        $page     = intval(Yii::$app->request->get('page',1));
        $pageSize = intval(Yii::$app->request->get('pagesize',10));
        $data = GroupingRelTechnician::getTechnicianList($groupingId,$page,$pageSize);
        return self::success($data);
    }

    /**
     * 移除
     * @return array
     */
    public static function remove()
    {
        $technicianId = intval(Yii::$app->request->get('technician_id',0));
        $groupingId   = intval(Yii::$app->request->get('grouping_id',0));

        //验证当前技师是否禁用
        $tecStatus = Technician::getTecStatus($technicianId);
        if($tecStatus == 2) {
            return self::error('该技师已禁用！');
        }

        //验证当前分组下是否有未完成的工单
        $post_data = ['techician_ids' => $technicianId];
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/sreach-group-work";
        $group_work_res = json_decode(Helper::curlPostJson($url,$post_data), true);
        if(!$group_work_res['success'] || !empty($group_work_res['data'])) {
            return self::error('该技师有未完成的工单，暂时不可移除！');
        }

        $res = GroupingRelTechnician::remove($technicianId, $groupingId);
        if($res == true){
            return self::success([]);
        }
        return self::error('移除失败');
    }

    /**
     * 修改分组
     * @return array
     */
    public static function change()
    {
        $technicianId  = intval(Yii::$app->request->get('technician_id',0));
        $groupingId    = intval(Yii::$app->request->get('grouping_id',0));

        //验证当前技师是否禁用
        $tecStatus = Technician::getTecStatus($technicianId);
        if($tecStatus == 2) {
            return self::error('该技师已禁用！');
        }
        $techArr = Technician::getOne(['id' => $technicianId]);
        //验证当前分组下是否有未完成的工单
        $post_data = ['techician_ids' => $technicianId];
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/sreach-group-work";
        $group_work_res = json_decode(Helper::curlPostJson($url,$post_data), true);
        if(!$group_work_res['success'] || !empty($group_work_res['data'])) {
            return self::error('该技师有未完成的工单，暂时不可修改分组！');
        }

        $res = GroupingRelTechnician::change($techArr['store_id'],$technicianId,$groupingId);
        if($res == true){
            return self::success([]);
        }
        return self::error('修改失败');
    }

    /**
     * 修改职位
     * @return array
     */
    public static function changeTitle()
    {
        $technicianId = intval(Yii::$app->request->get('technician_id',0));
        $groupingId   = intval(Yii::$app->request->get('grouping_id',0));
        $jobTitle     = intval(Yii::$app->request->get('job_title',0));

        //验证当前技师是否禁用
        $tecStatus = Technician::getTecStatus($technicianId);
        if($tecStatus == 2) {
            return self::error('该技师已禁用！');
        }

        //查询组长id
        $grouping = GroupingRelTechnician::find()->where(['grouping_id'=>$groupingId,'job_title'=>2,'status'=>1])->asArray()->one();
        if (!empty($grouping)) {
            
            //验证当前分组下是否有未完成的工单
            $post_data = ['techician_ids' => $grouping['technician_id']];
            $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/sreach-group-work";
            $group_work_res = json_decode(Helper::curlPostJson($url,$post_data), true);
    
            if(!$group_work_res['success'] || !empty($group_work_res['data'])) {
                return self::error('现任组长仍有未完成工单，暂时不可修改职位。');
            }
        }

        //验证当前分组下是否有未完成的工单
        $post_data = ['techician_ids' => $technicianId];
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/sreach-group-work";
        $group_work_res = json_decode(Helper::curlPostJson($url,$post_data), true);

        if(!$group_work_res['success'] || !empty($group_work_res['data'])) {
            return self::error('该技师有未完成的工单，暂时不可修改职位！');
        }

        $res = GroupingRelTechnician::changeTitle(BaseLogic::getDepartmentId(),$technicianId,$groupingId,$jobTitle);
        if($res == true){
            return self::success([]);
        }
        return self::error('修改失败');
    }

    /**
     * 验证选中组长是否属于别的组
     * @return array
     */
    public static function checkLeader()
    {
        $technicianId = intval(Yii::$app->request->get('technician_id',0));
        $group_id = intval(Yii::$app->request->get('group_id',0));
        $res = GroupingRelTechnician::checkLeader(BaseLogic::getDepartmentId(),$technicianId,$group_id);
        if(!$res){
            return self::success([]);
        }
        return self::error('该技师已加入【'.$res->name.'】，请重新选择。');
    }
}
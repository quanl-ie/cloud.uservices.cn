<?php
namespace webapp\logic;

use Yii;
use common\models\ServiceBrand;
use webapp\models\ManufactorBrand;

class BrandLogic extends BaseLogic
{
    /**
     * 获取品牌列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 13:33
     * @return array
     */
    public static function getIndex()
    {
    
        $dcId     = self::getDirectCompanyId();
        $topId    = self::getTopId();
        
        if ($dcId == $topId) {
            $flag = 1;
        } else {
            $flag = 2;
        }
        
        $where = [
            ['=','a.direct_company_id',$dcId]
        ];

        //获取列表数据
        $where = [['=','a.direct_company_id',$dcId],['=','b.department_id',$topId]];
        
        $res = ManufactorBrand::getPageList($where);
    
        if (isset($flag)) {
            $res['flag'] = $flag;
        }

        if (empty($res['list'])) {
            return $res;
        }
        
        foreach ($res['list'] as $key => $val) {
            
            if ($val['status'] == 1) {
                $res['list'][$key]['status_desc'] = '已启用';
            }else{
                $res['list'][$key]['status_desc'] = '已禁用';
            }

        }

        return $res;
    }

    /**
     * 添加数据列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 16:07
     * @return mixed
     */
    public static function beforeAdd()
    {
        $topId    = self::getTopId();

        //获取已添加的品牌
        $manufactorBrand = ManufactorBrand::getPageList([['a.direct_company_id' => $topId]]);
        
        //获取id
        $brandIds = [];
        if (!empty($manufactorBrand['list'])) {
            $brandIds = array_column($manufactorBrand['list'],'brand_id');
        }
        
        //获取所有品牌数据
        $selfBrand   = [];
        $commonBrand = [];
        $brandList = ServiceBrand::getList(['department_id' => [0,$topId]]);
        foreach ($brandList as $key => $val) {
            if ($val['department_id'] == $topId) {
                $selfBrand[$val['id']]   = $val['title'];
            }else{
                $commonBrand[$val['id']] = $val['title'];
            }
        }
        
        foreach ($selfBrand as $val) {
            unset($commonBrand[array_search($val,$commonBrand)]);
        }

        //组合数据
        $data['brand_ids']        = $brandIds;
        $data['brand_list']       = array_replace_recursive($commonBrand,$selfBrand);
        $data['manufactor_brand'] = ServiceBrand::getName($brandIds);

        return $data;
    }

    /**
     * 编辑添加数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 17:56
     * @return array
     * @throws \Exception
     */
    public static function add()
    {
        //接收参数
        $brandId = Yii::$app->request->post('id','');

        if ($brandId == '') {
            return self::error('保存失败',20001);
        }

        //定义变量
        $time         = time();
        $topId        = self::getTopId();
        $departmentId = self::getDepartmentId();

        //查询自身所有品牌
        $brand    = ManufactorBrand::getPageList([['a.direct_company_id' => $topId]]);

        //提取品牌id
        $brandIds = array_column($brand['list'],'brand_id','id');

        //计算出需要添加的品牌
        $addBrandId  = array_diff($brandId,$brandIds);

        $data             = [];
        $userId           = Yii::$app->user->id;
        $serviceBrandName = ServiceBrand::getName($addBrandId);

        //组合需要添加的品牌
        if (!empty($addBrandId))
        {
            foreach ($addBrandId as $key => $val) {

                $data[$key]['title']           = $serviceBrandName[$val];
                $data[$key]['department_id']   = $topId;
                $data[$key]['sort']            = 1;
                $data[$key]['status']          = 1;
                $data[$key]['del_status']      = 1;
                $data[$key]['create_id']       = $userId;
                $data[$key]['create_time']     = $time;
                $data[$key]['update_id']       = $userId;
                $data[$key]['update_time']     = $time;

            }

        }
        
        $res = ServiceBrand::add($data,$brandIds,$departmentId);

        if ($res) {
            return self::success('','保存成功');
        }
        return self::error('保存失败',20002);

    }

    /**
     * 修改品牌状态（禁用、启用）
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 13:51
     * @return array
     */
    public static function changeStatus()
    {
        $id     = intval(Yii::$app->request->post('id',0));
        $status = intval(Yii::$app->request->post('status',0));

        if ($id == 0) {
            return self::error('操作失败',20001);
        }

        if ($status == 0) {
            return self::error('操作失败',20002);
        }
        
        $data = [];
        
        $data['id']          = $id;
        $data['status']      = $status;
        $data['update_id']   = self::getLoginUserId();
        $data['update_time'] = time();
        $data['topId']       = self::getTopId();

        $res = ServiceBrand::changeStatus($data);

        if ($res) {
            return self::success('','操作成功');
        }
        return self::error('操作失败',20003);
    }
}

<?php
namespace webapp\logic;

use common\helpers\Helper;
use common\models\Common;
use Yii;
use webapp\models\Department;

class DepartmentLogic extends BaseLogic
{

    /**
     * 部门树
     * @return array
     * @author xi
     * @date 2018-6-12
     */
    public static function searchDepartment($is_page = '')
    {
        $parentId = intval(Yii::$app->request->get('pid',0));
        $name     = trim(Yii::$app->request->get('name',''));
        $isSearch = trim(Yii::$app->request->get('is_search',0));
        if($is_page != ''){
            $parentId = 0;
        }
        if($name != ''){
            $name = addslashes($name);
        }

        $where = "";
        $model = new Department();
        $dataAuthId = $model->dataAuthId;
        //如果是订单来的
        if(strpos(Yii::$app->request->referrer,'order/add') !==false){
            $dataAuthId = 3;
        }
        //如果是经销商
        if($model->departmentObj->type == 1)
        {
            //本人
            if(in_array($dataAuthId ,[1,4,5])){
                $where = '1 = 2';
            }
            //本公司
            else if($dataAuthId == 2)
            {
                if($isSearch == 0 )
                {
                    if($parentId == 0){
                        $where = "status = 1 and del_status = 1 and id = ".$model->departmentId;
                    }
                    else {
                        $where = "type in(2,3) and status = 1 and del_status = 1 and parent_id = ". $parentId;
                    }
                }
                else //如果是搜索
                {
                    if($name != '')
                    {
                        $sqlArr= [];
                        $allDepartments = Department::findAllByAttributes(['parent_id'=>$model->departmentId,'status'=>1,'del_status'=>1,'type'=>[2,3]],'id');
                        if($allDepartments)
                        {
                            $ids = array_column($allDepartments,'id');
                            $sqlArr[] = "id in(".implode(',',$ids).")";
                            foreach ($allDepartments as $val){
                                $sqlArr [] = "find_in_set(".$val['id'].",parent_ids)";
                            }
                            $sql = ' ('. implode(' or ', $sqlArr). ') ';
                        }
                        else{
                            $sql = " find_in_set(".$model->departmentId .",parent_ids)";
                        }
                        $where = "($sql or id = ".$model->departmentId .") and status = 1 and del_status = 1 and name like '%$name%'";
                    }
                    else {
                        $where = "status = 1 and del_status = 1 and id = ". $model->departmentId;
                    }
                }
            }
            //本公司及以下
            else if($dataAuthId == 3)
            {
                if($isSearch == 0 )
                {
                    if($parentId == 0){
                        $where = "status = 1 and del_status = 1 and id = ".$model->departmentId;
                    }
                    else {
                        $where = "status = 1 and del_status = 1 and parent_id = ". $parentId;
                    }
                }
                else //如果是搜索
                {
                    if($name != ''){
                        $where = "(find_in_set(".$model->departmentId.",parent_ids) or id = ".$model->departmentId .") and status = 1 and del_status = 1 and name like '%$name%'";
                    }
                    else {
                        $where = " status = 1 and del_status = 1 and id = ". $model->departmentId;
                    }
                }
            }
        }
        //如果是门店
        else if($model->departmentObj->type == 2)
        {
            //本人,本部门,本部门及以下
            if(in_array($dataAuthId, [1,4,5]) ){
                $where = '1=2';
            }
            //本公司
            else if($dataAuthId == 2)
            {
                if($isSearch == 0 )
                {
                    if($parentId == 0){
                        $where = "status = 1 and del_status = 1 and id = ".$model->departmentId;
                    }
                    else {
                        $where = "type in(2,3) and status = 1 and del_status = 1 and parent_id = ". $parentId;
                    }
                }
                else //如果是搜索
                {
                    if($name != ''){
                        $where = "type in(2,3) and (find_in_set(".$model->departmentId.",parent_ids) or id = ".$model->departmentId .") and status = 1 and del_status = 1 and name like '%$name%'";
                    }
                    else {
                        $where = " status = 1 and del_status = 1 and id = ". $model->departmentId;
                    }
                }
            }
            //本公司及以下
            else if($dataAuthId == 3)
            {
                if($isSearch == 0 )
                {
                    if($parentId == 0){
                        $where = "status = 1 and del_status = 1 and id = ".$model->departmentId;
                    }
                    else {
                        $where = "status = 1 and del_status = 1 and parent_id = ". $parentId;
                    }
                }
                else //如果是搜索
                {
                    if($name != ''){
                        $where = "(find_in_set(".$model->departmentId.",parent_ids) or id = ".$model->departmentId .") and status = 1 and del_status = 1 and name like '%$name%'";
                    }
                    else {
                        $where = " status = 1 and del_status = 1 and id = ". $model->departmentId;
                    }
                }
            }
        }
        //如果是部门
        else if($model->departmentObj->type == 3)
        {
            //本人
            if($dataAuthId == 1){
                $where = '1 = 2';
            }
            //本公司
            else if($dataAuthId == 2)
            {
                //查一下他拉直属是什么类型
                $directCompanyArr = Department::findOneByAttributes(['id'=>$model->directCompanyId]);
                //如果是经销商，可以看所有的
                if(in_array($directCompanyArr['type'],[1,2]))
                {
                    if($isSearch == 0 )
                    {
                        if($parentId == 0){
                            $where = "status = 1 and del_status = 1 and id = ".$directCompanyArr['id'];
                        }
                        else {
                            $where = "type in(2,3) and status = 1 and del_status = 1 and parent_id = ". $parentId;
                        }
                    }
                    else //如果是搜索
                    {
                        if($name != '')
                        {
                            $sqlArr= [];
                            $allDepartments = Department::findAllByAttributes(['parent_id'=>$directCompanyArr['id'],'status'=>1,'del_status'=>1,'type'=>[2,3]],'id');
                            if($allDepartments)
                            {
                                $ids = array_column($allDepartments,'id');
                                $sqlArr[] = "id in(".implode(',',$ids).")";
                                foreach ($allDepartments as $val){
                                    $sqlArr [] = "find_in_set(".$val['id'].",parent_ids)";
                                }
                                $sql = ' ('. implode(' or ', $sqlArr). ') ';
                            }
                            else{
                                $sql = " find_in_set(".$directCompanyArr['id'] .",parent_ids)";
                            }

                            $where = "($sql or id = ".$directCompanyArr['id'] .") and type in(2,3) and status = 1 and del_status = 1 and name like '%$name%'";
                        }
                        else {
                            $where = "status = 1 and del_status = 1 and id = ".$directCompanyArr['id'];
                        }
                    }
                }
                else {
                    $where = "id = " . $model->departmentId;
                }
            }
            //本公司及以下
            else if($dataAuthId == 3)
            {
                //查一下他拉直属是什么类型
                $directCompanyArr = Department::findOneByAttributes(['id'=>$model->directCompanyId]);
                //如果是经销商，可以看所有的
                if(in_array($directCompanyArr['type'],[1,2]))
                {
                    if($isSearch == 0 )
                    {
                        if($parentId == 0){
                            $where = "id = ".$directCompanyArr['id'] ." and status = 1 and del_status = 1";
                        }
                        else {
                            $where = "status = 1 and del_status = 1 and parent_id = ". $parentId;
                        }
                    }
                    else
                    {
                        //如果是搜索
                        if($name != ''){
                            $where = "(find_in_set(".$directCompanyArr['id'].",parent_ids) or id = ".$directCompanyArr['id'] .") and status = 1 and del_status = 1 and name like '%$name%'";
                        }
                        else {
                            $where = "id = ".$directCompanyArr['id'] ." and status = 1 and del_status = 1";
                        }
                    }
                }
                else {
                    $where = "id = " . $model->departmentId;
                }
            }
            //本部门
            else if($dataAuthId == 4)
            {
                if($isSearch == 0 )
                {
                    if($parentId == 0){
                        $where = "id = ".$model->departmentId ." and status = 1 and del_status = 1";
                    }
                    else {
                        $where = "status = 1 and del_status = 1 and parent_id = ". $parentId;
                    }
                }
                else
                {
                    //如果是搜索
                    if($name != ''){
                        $where = "(find_in_set(".$model->departmentId.",parent_ids) or id = ".$model->departmentId .") and status = 1 and del_status = 1 and name like '%$name%'";
                    }
                    else {
                        $where = "id = ".$model->departmentId ." and status = 1 and del_status = 1";
                    }
                }
            }
            //本部门及以下
            else if($dataAuthId == 5)
            {
                if($isSearch == 0 )
                {
                    if($parentId == 0){
                        $where = "id = ".$model->departmentId ." and status = 1 and del_status = 1";

                    }
                    else {
                        $where = "status = 1 and del_status = 1 and parent_id = ". $parentId;
                    }
                }
                else
                {
                    //如果是搜索
                    if($name != ''){
                        $where = "(find_in_set(".$model->departmentId.",parent_ids) or id = ".$model->departmentId .") and status = 1 and del_status = 1 and name like '%$name%'";
                    }
                    else {
                        $where = "id = ".$model->departmentId ." and status = 1 and del_status = 1";
                    }
                }
            }

        }
        //不显示下级经销商的部门
        $departmentArr = Department::findOneByAttributes(['id'=>$parentId],'direct_company_id');
        if($departmentArr && $departmentArr['direct_company_id']!= self::getDirectCompanyId()){
            $where .= " and type = 1";
        }
        return BaseLogic::success(Department::getDepartment($where,$dataAuthId));
    }

    /**
     * 右边列表
     * @return array
     */
    public static function getList($dataAuthId = 0)
    {
        $parentId = intval(Yii::$app->request->get('pid',0));
        $status   = intval(Yii::$app->request->get('status',1));
        //1启用 2禁用 3搜索
        $type     = intval(Yii::$app->request->get('type',1));
        $name     = addslashes(trim(Yii::$app->request->get('name','')));

        $where = $andWhere = '';
        $model = new Department();
        if($dataAuthId == 0){
            $dataAuthId = $model->dataAuthId;
        }

        //如果是经销商
        if($model->departmentObj->type == 1)
        {
            //本人
            if(in_array($dataAuthId ,[1,4,5])){
                $where = '1 = 2';
            }
            //本公司
            else if($dataAuthId == 2)
            {
                //如果点击搜索，内容为空，搜索出以前内容
                if($name == '' && $type == 3){
                    $type = 2;
                }

                //展开列表
                if($type == 1)
                {
                    if($parentId > 0){
                        $where = "type in(2,3) and  status = 1 and del_status = 1 and parent_id = $parentId";
                    }
                    else {
                        $where = "status = 1 and del_status = 1 and id = ".$model->departmentId;
                    }
                }
                //启用，禁用列表
                else if($type == 2)
                {
                    if($parentId == 0){
                        $parentId = $model->departmentId;
                    }
                    //如果是禁用，查他下面的
                    if($status == 2){
                        $where = "type in(2,3) and status = 2 and del_status = 1 and parent_id = $parentId";
                    }
                    else {
                        $where = "status = 1 and del_status = 1 and id = $parentId";
                    }
                }
                //搜索按钮
                else if($type == 3)
                {
                    if($parentId == 0){
                        $parentId = $model->departmentId;
                    }
                    $sqlArr= [];
                    $allDepartments = Department::findAllByAttributes(['parent_id'=>$parentId,'status'=>1,'del_status'=>1,'type'=>[2,3]],'id');
                    if($allDepartments)
                    {
                        $ids = array_column($allDepartments,'id');
                        $sqlArr[] = "id in(".implode(',',$ids).")";
                        foreach ($allDepartments as $val){
                            $sqlArr [] = "find_in_set(".$val['id'].",parent_ids)";
                        }
                        $sql = ' and ('. implode(' or ', $sqlArr). ') ';
                    }
                    else{
                        $sql = " and find_in_set(".$parentId.",parent_ids)";
                    }

                    $where = ' status ='.$status.' and del_status = 1 and ((type in(2,3) '.$sql.') or id='.$parentId.') ';
                }


                if($name != ''){
                    $andWhere = "(name like '%$name%' or contacts like '%$name%' or phone like '%$name%')" ;
                }
            }
            //本公司及以下
            else if($dataAuthId == 3)
            {
                //如果点击搜索，内容为空，搜索出以前内容
                if($name == '' && $type == 3){
                    $type = 2;
                }

                //展开列表
                if($type == 1)
                {
                    if($parentId > 0){
                        $where = "status = $status and del_status = 1 and parent_id = $parentId";
                    }
                    else {
                        $where = "status = $status and del_status = 1 and id = ". $model->departmentId;
                    }
                }
                //启用，禁用列表
                else if($type == 2)
                {
                    if($parentId == 0){
                        $parentId = $model->departmentId;
                    }
                    //如果是禁用，查他下面的
                    if($status == 2){
                        $where = "status = 2 and del_status = 1 and parent_id = $parentId ";
                    }
                    else {
                        $where = "status = 1 and del_status = 1 and id = $parentId";
                    }
                }
                //搜索按钮
                else if($type == 3)
                {
                    if($parentId == 0){
                        $parentId = $model->departmentId;
                    }
                    $where = '(find_in_set('.($parentId).',parent_ids) or id='.$parentId.')';
                }

                if($name != ''){
                    $andWhere = "(name like '%$name%' or contacts like '%$name%' or phone like '%$name%')" ;
                }
            }
        }
        //如果是门店
        else if($model->departmentObj->type == 2)
        {
            //本人,本部门,本部门及以下
            if(in_array($dataAuthId, [1,4,5]) ){
                $where = '1=2';
            }
            //本公司 , 本公司及以下
            else if($dataAuthId == 2 || $dataAuthId == 3)
            {
                //如果点击搜索，内容为空，搜索出以前内容
                if($name == '' && $type == 3){
                    $type = 2;
                }

                //展开列表
                if($type == 1)
                {
                    if($parentId > 0){
                        $where = "status = 1 and del_status = 1 and parent_id = $parentId";
                    }
                    else {
                        $where = "status = 1 and del_status = 1 and id = ". $model->departmentId;
                    }
                }
                //启用，禁用列表
                else if($type == 2)
                {
                    if($parentId == 0){
                        $parentId = $model->departmentId;
                    }
                    //如果是禁用，查他下面的
                    if($status == 2){
                        $where = "status = 2 and del_status = 1 and parent_id = $parentId ";
                    }
                    else {
                        $where = "status = 1 and del_status = 1 and id = $parentId";
                    }
                }
                //搜索按钮
                else if($type == 3)
                {
                    if($parentId == 0){
                        $parentId = $model->departmentId;
                    }
                    $where = '(find_in_set('.($parentId).',parent_ids) or id='.$parentId.')';
                }

                if($name != ''){
                    $andWhere = "(name like '%$name%' or contacts like '%$name%' or phone like '%$name%')" ;
                }
            }
        }
        //如果是部门
        else if($model->departmentObj->type == 3)
        {
            //本人
            if($dataAuthId == 1){
                $where = '1 = 2';
            }
            //本公司
            else if($dataAuthId == 2)
            {
                //查一下他拉直属是什么类型
                $directCompanyArr = Department::findOneByAttributes(['id'=>$model->directCompanyId]);
                //如果是经销商，可以看所有的
                if(in_array($directCompanyArr['type'],[1,2]))
                {
                    //如果点击搜索，内容为空，搜索出以前内容
                    if($name == '' && $type == 3){
                        $type = 2;
                    }

                    //展开列表
                    if($type == 1)
                    {
                        if($parentId > 0){
                            $where = "type in(2,3) and  status = 1 and del_status = 1 and parent_id = $parentId";
                        }
                        else {
                            $where = "status = 1 and del_status = 1 and id = ".$directCompanyArr['id'];
                        }
                    }
                    //启用，禁用列表
                    else if($type == 2)
                    {
                        //如果是禁用，查他下面的
                        if($status == 2){
                            $where = "type in(2,3) and status = 2 and del_status = 1 and parent_id = ".($parentId >0?$parentId:$directCompanyArr['id']);
                        }
                        else {
                            $where = "status = 1 and del_status = 1 and id = ".($parentId >0?$parentId:$directCompanyArr['id']);
                        }
                    }
                    //搜索按钮
                    else if($type == 3)
                    {
                        $sqlArr= [];
                        $allDepartments = Department::findAllByAttributes(['parent_id'=>($parentId >0?$parentId:$directCompanyArr['id']),'status'=>1,'del_status'=>1,'type'=>[2,3]],'id');
                        if($allDepartments)
                        {
                            $ids = array_column($allDepartments,'id');
                            $sqlArr[] = "id in(".implode(',',$ids).")";
                            foreach ($allDepartments as $val){
                                $sqlArr [] = "find_in_set(".$val['id'].",parent_ids)";
                            }
                            $sql = ' and ('. implode(' or ', $sqlArr). ') ';
                        }
                        else{
                            $sql = " and find_in_set(".($parentId >0?$parentId:$directCompanyArr['id']).",parent_ids)";
                        }

                        $where = ' status ='.$status.' and del_status = 1 and ((type in(2,3) '.$sql.') or id='.($parentId >0?$parentId:$directCompanyArr['id']).') ';
                    }


                    if($name != ''){
                        $andWhere = "(name like '%$name%' or contacts like '%$name%' or phone like '%$name%')" ;
                    }
                }
                else {
                    $where = "id = " . $directCompanyArr['id'];
                }
            }
            //本公司及以下
            else if($dataAuthId == 3)
            {
                //查一下他拉直属是什么类型
                $directCompanyArr = Department::findOneByAttributes(['id'=>$model->directCompanyId]);
                //如果是经销商，可以看所有的
                if(in_array($directCompanyArr['type'],[1,2]))
                {
                    //如果点击搜索，内容为空，搜索出以前内容
                    if($name == '' && $type == 3){
                        $type = 2;
                    }

                    //展开列表
                    if($type == 1)
                    {
                        if($parentId > 0){
                            $where = "status = 1 and del_status = 1 and parent_id = $parentId";
                        }
                        else {
                            $where = "status = 1 and del_status = 1 and id = ". $directCompanyArr['id'];
                        }
                    }
                    //启用，禁用列表
                    else if($type == 2)
                    {
                        //如果是禁用，查他下面的
                        if($status == 2){
                            $where = "status = 2 and del_status = 1 and parent_id = " . ($parentId>0?$parentId:$directCompanyArr['id']);
                        }
                        else {
                            $where = "status = 1 and del_status = 1 and id = " . ($parentId>0?$parentId:$directCompanyArr['id']);
                        }
                    }
                    //搜索按钮
                    else if($type == 3){
                        $where = '(find_in_set('.($parentId >0 ?$parentId:$directCompanyArr['id']).',parent_ids) or id='.($parentId >0 ?$parentId:$directCompanyArr['id']).')';
                    }

                    if($name != ''){
                        $andWhere = "(name like '%$name%' or contacts like '%$name%' or phone like '%$name%')" ;
                    }
                }
                else {
                    $where = "id = " . $directCompanyArr['id'];
                }
            }
            //本部门 ,本部门及以下
            else if(in_array($dataAuthId, [4,5]))
            {
                //如果点击搜索，内容为空，搜索出以前内容
                if($name == '' && $type == 3){
                    $type = 2;
                }


                //展开列表
                if($type == 1)
                {
                    if($parentId > 0){
                        $where = "status = 1 and del_status = 1 and parent_id = $parentId";
                    }
                    else {
                        $where = "status = 1 and del_status = 1 and id = ".$model->departmentId;
                    }
                }
                //启用，禁用列表
                else if($type == 2)
                {
                    if($parentId == 0){
                        $parentId = $model->departmentId;
                    }
                    //如果是禁用，查他下面的
                    if($status == 2){
                        $where = "status = 2 and del_status = 1 and parent_id = $parentId";
                    }
                    else {
                        $where = "status = 1 and del_status = 1 and id = $parentId";
                    }
                }
                //搜索按钮
                else if($type == 3)
                {
                    if($parentId == 0){
                        $parentId = $model->departmentId;
                    }
                    $where = '(find_in_set('.$parentId.',parent_ids) or id='.$parentId.')';
                }

                if($name != ''){
                    $andWhere = "(name like '%$name%' or contacts like '%$name%' or phone like '%$name%')" ;
                }
            }
        }

        $departmentArr = Department::findOneByAttributes(['id'=>$parentId],'direct_company_id');
        if($departmentArr && $departmentArr['direct_company_id']!= self::getDirectCompanyId()){
            $where .= " and type = 1";
        }

        return [
            'type' => $type,
            'list' => Department::getList($where,$andWhere,$status,$dataAuthId)['list']
        ];
    }
    /**
     * //修改资料
     * @param
     * @param
     * @return array
     * @author sxz
     * @date   2018-06-7
     */
    public static function editInfo($id,$postData)
    {
        //联系人验证
        if(trim($postData['contact']) != '' && Helper::strCount(trim($postData['contact'])) >30 ){
            return self::error('不超过30个字',20002);
        }
        //详细地址
        if(trim($postData['address']) != '' && Helper::strCount(trim($postData['address'])) >30 ){
            return self::error('不超过30个字',20002);
        }
        //联系电话
        if(trim($postData['phone']) != '' && (!preg_match('/^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$/',$postData['phone']) && !preg_match('/^1\d{10}$/', $postData['phone']) )){
            return self::error('联系电话格式不正确',20002);
        }
        //备注
        if(trim($postData['company_desc']) != '' && Helper::strCount(trim($postData['company_desc'])) >200 ){
            return self::error('最多200字',20002);
        }
        $data['contacts']       = $postData['contact'];
        $data['phone']          = $postData['phone'];
        $data['email']          = $postData['email'];
        $data['province_id']    = $postData['province_id'];
        $data['city_id']        = $postData['city_id'];
        $data['district_id']    = $postData['district_id'];
        $data['address']        = $postData['address'];
        $data['remark']         = $postData['company_desc'];
        $data['update_time']    = time();
        $res = Department::editInfo($id,$data);
        if ($res ==1) {
            return self::success('','保存成功');
        }
        if ($res ==2) {
            return self::error('保存失败');
        }
    }
}
<?php
namespace webapp\logic;
use common\models\Product;
use Yii;
use webapp\models\BaseModel;
use common\helpers\Helper;
use webapp\models\Region;
use webapp\models\TechProdUseRecord;
use common\models\DictEnum;
use common\models\ServiceBrand;
use common\models\ServiceClass;
/**
 * 技师备件使用记录
 * @author sxz
 * @date 2018-04-02
 */
class TechProdUseRecordLogic extends BaseLogic
{
    /**
     * 数据列表
     * @author sxz
     * @return string
     */
    public static function getList()
    {
        $params = $where = $newArr = $data = $result = [];
        $tech_id                    = intval(Yii::$app->request->get('technician_id',0));
        $params['page']             = $page = intval(Yii::$app->request->get('page',1));
        $params['pageSize']         = $pageSize = intval(Yii::$app->request->get('pageSize',20));
        $params['technician_id']    = $tech_id;
        $where[] = ['tech_id'=>$tech_id];
        $maps   = ['a.direct_company_id'=>BaseLogic::getDirectCompanyId()];
        $data = TechProdUseRecord::getList($maps,$where,$page,$pageSize);
        //获取配件的详情
        if($data['list']){
            $data['list'] = self::changeData($data['list'],'');
        }
        $data['params'] = ['technician_id'=>$tech_id];
        return $data;
    }
    /**
     * 组合列表显示数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/21
     * Time: 17:50
     * @param $list
     * @return mixed
     */
    public static function changeData($list,$type)
    {
        foreach ($list as $key => $val) {

            $brandName = ServiceBrand::getName($val['brand_id']);
            $brandName = $brandName[$val['brand_id']];

            if (strstr($brandName, '/', true)) {
                $list[$key]['brand_name'] = strstr($brandName, '/', true);
            } else {
                $list[$key]['brand_name'] = $brandName;
            }
            if (!empty($val['class_id'])) {
                $list[$key]['class_name'] = ServiceClass::getTitle($val['class_id']);
            }
            unset($list[$key]['brand_id']);
            unset($list[$key]['class2_id']);
            unset($list[$key]['type_id']);
        }
        return $list;
    }



}
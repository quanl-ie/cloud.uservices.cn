<?php
namespace webapp\logic;
use common\helpers\Helper;
use common\models\ScheduleSet;
use webapp\models\BaseModel;
use Yii;


class ScheduleSetLogic extends BaseLogic
{
    /**
     * 列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/18
     * Time: 16:03
     * @return array
     */
    public static function index()
    {
        
        $dcId  = self::getDirectCompanyId();
        $topId = self::getTopId();
        $scheduleSet = ScheduleSet::getList(['direct_company_id'=>$dcId,'department_top_id'=>$topId,'is_delete'=>1]);
        $data = [];
        foreach ($scheduleSet as $key => $val) {
            
            $data[$key]['id']         = $val['id'];
            $data[$key]['name']       = $val['name'];
            $data[$key]['start_time'] = $val['start_time'];
            $data[$key]['end_time']   = $val['end_time'];
            $data[$key]['is_default'] = $val['is_default'];
            $data[$key]['color']      = $val['color'];
            
        }
        return $data;
    }
    
    /**
     * 添加
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/18
     * Time: 16:04
     * @return array
     * @throws \Exception
     */
    public static function add()
    {
        $add  = Yii::$app->request->post('Add','');
        $edit = Yii::$app->request->post('Edit','');
    
        $dcId   = self::getDirectCompanyId();
        $topId  = self::getTopId();
        $userId = self::getLoginUserId();
        $data = [];
        if (!empty($add))
        {
    
            $set = ScheduleSet::getList(['direct_company_id'=>$dcId,'department_top_id'=>$topId,'is_delete'=>1]);
            if (count($set) >= 10) {
                return self::error('添加班次超出限制');
            }
            
            foreach ($add as $key => $val) {
                $data['add'][$key]['name']              = Helper::filterWord($val['name']);
                $data['add'][$key]['start_time']        = Helper::filterWord($val['start_time']);
                $data['add'][$key]['end_time']          = Helper::filterWord($val['end_time']);
                $data['add'][$key]['direct_company_id'] = $dcId;
                $data['add'][$key]['department_top_id'] = $topId;
                $data['add'][$key]['is_default']        = Helper::filterWord($val['is_default']);
                $data['add'][$key]['is_delete']         = 1;
                $data['add'][$key]['create_time']       = date('Y-m-d H:i:s',time());
                $data['add'][$key]['oprate_id']         = $userId;
                $data['add'][$key]['color']             = self::color(self::color());
            }
            
        }
        
        if (!empty($edit))
        {
            
            $setId = array_column($edit,'id');
            $sche = ScheduleSet::getList(['id'=>$setId,'direct_company_id'=>$dcId,'department_top_id'=>$topId,'is_delete'=>1]);
            
            $scheArr = [];
            foreach ($sche as $val) {
                $scheArr[$val['id']] = $val;
            }
            
            foreach ($edit as $key => $val) {
                if (isset($val['name']))
                {
                    $data['addDef'][$key]['name'] = Helper::filterWord(@$val['name']);
                    
                    if ($val['start_time'] != $scheArr[$val['id']]['start_time']) {
                        $data['addDef'][$key]['start_time']  = Helper::filterWord(@$val['start_time']);
                    }else{
                        $data['addDef'][$key]['start_time']  = $scheArr[$val['id']]['start_time'];
                    }
                    
                    if ($val['end_time'] != $scheArr[$val['id']]['end_time']) {
                        $data['addDef'][$key]['end_time']  = Helper::filterWord(@$val['end_time']);
                    }else{
                        $data['addDef'][$key]['end_time']  = $scheArr[$val['id']]['end_time'];
                    }
    
                    $data['addDef'][$key]['direct_company_id']  = $dcId;
                    $data['addDef'][$key]['department_top_id']  = $topId;
                    $data['addDef'][$key]['is_default']         = $scheArr[$val['id']]['is_default'];
                    $data['addDef'][$key]['is_delete']          = 1;
                    $data['addDef'][$key]['create_time']        = date('Y-m-d H:i:s',time());
                    $data['addDef'][$key]['oprate_id']          = $userId;
                    $data['addDef'][$key]['color']              = $scheArr[$val['id']]['color'];
                    
                    $data['del'][] = $val['id'];
                    
                }
                else
                {

                    if ($val['start_time'] != $scheArr[$val['id']]['start_time']) {
                        $data['edit'][$key]['start_time']  = Helper::filterWord(@$val['start_time']);
                    }
    
                    if ($val['end_time'] != $scheArr[$val['id']]['end_time']) {
                        $data['edit'][$key]['end_time']  = Helper::filterWord(@$val['end_time']);
                    }
                    
                    if (isset($data['edit'][$key]))
                    {
                        $data['edit'][$key]['name']               = $scheArr[$val['id']]['name'];
                        
                        if (!isset($data['edit'][$key]['start_time'])) {
                            $data['edit'][$key]['start_time']     = $scheArr[$val['id']]['start_time'];
                        }
    
                        if (!isset($data['edit'][$key]['end_time'])) {
                            $data['edit'][$key]['end_time']       = $scheArr[$val['id']]['end_time'];
                        }
                        
                        $data['edit'][$key]['direct_company_id']  = $dcId;
                        $data['edit'][$key]['department_top_id']  = $topId;
                        $data['edit'][$key]['is_default']         = $scheArr[$val['id']]['is_default'];
                        $data['edit'][$key]['is_delete']          = 1;
                        $data['edit'][$key]['create_time']        = date('Y-m-d H:i:s',time());
                        $data['edit'][$key]['oprate_id']          = $userId;
                        $data['edit'][$key]['color']              = $scheArr[$val['id']]['color'];
                        
                        $data['del'][] = $val['id'];
                    }
                }
            }
        }
        
        $res = ScheduleSet::add($data);
        
        if ($res) {
            return self::success('','班次设置成功');
        }
        return self::error('班次设置失败');
        
    }
    
    /**
     * 获取随机颜色
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/3
     * Time: 15:35
     * @param string $key
     * @return mixed
     */
    public static function color($key = '')
    {
        $color = [
            1 => '#f0f0ff',
            2 => '#faf0ff',
            3 => '#fffff0',
            4 => '#fff7f0',
            5 => '#f0ffff',
        ];
        if ($key) {
            return $color[$key];
        }
    
        $dcId   = self::getDirectCompanyId();
        $topId  = self::getTopId();
        $set = ScheduleSet::getList(['direct_company_id'=>$dcId,'department_top_id'=>$topId,'is_delete'=>1,'is_default'=>2]);
        
        if (!empty($set))
        {
            $oldColor = array_column($set,'color');
            return array_rand(array_diff($color,$oldColor));
        }
        
        return array_rand($color);
        
    }
}
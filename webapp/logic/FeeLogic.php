<?php
namespace webapp\logic;

use webapp\models\FeeSetting;
use Yii;

class FeeLogic extends BaseLogic
{
    public static function setting()
    {
        $topId = self::getTopId();
        $dcId  = self::getDirectCompanyId();
    
        $list = FeeSetting::setStatus();
        if (!empty($list)) {
            $data['list'] = $list;
        }else{
            $data['list'] = [];
        }
        
        $disData = FeeSetting::getOne(['department_id'=>$topId,'direct_company_id'=>$dcId]);
        
        if (!empty($disData)) {
            $data['self'] = $disData;
        }else{
            $data['self'] = [];
        }
        return $data;
    }
    
    public static function add()
    {
        $id     = intval(Yii::$app->request->post('id',0));
        $status = intval(Yii::$app->request->post('status',0));
        if ($id != 0) {
            $data['id'] = $id;
        }
        $data['department_id']     = self::getTopId();
        $data['direct_company_id'] = self::getDirectCompanyId();
        $data['status']            = $status;
        
        $res = FeeSetting::add($data);
        
        if ($res) {
            return self::success('','操作成功');
        }
        return self::error('操作失败',20002);
    }
}
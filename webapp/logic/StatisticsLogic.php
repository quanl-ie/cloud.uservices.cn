<?php
namespace webapp\logic;

use Yii;
use common\helpers\Helper;
use webapp\models\OrderInfo;
use common\models\ServiceClass;
use common\models\ServiceBrand;
use common\models\ServiceType;
use common\helpers\ExcelHelper;


class StatisticsLogic extends BaseLogic
{

    public static function getList($start_time='',$end_time='')
    {
       /*  $status        = trim(Yii::$app->request->get('status',''));  //状态，1待接单 2待指派 3待服务 4服务中 5 已完成 6已取消
        $start_time    = trim(Yii::$app->request->get('start_time','2017-12-12 00:00:00'));
        $end_time      = trim(Yii::$app->request->get('end_time','2017-12-20 23:59:59'));
        $page          = intval(Yii::$app->request->get('page',1));
        $pageSize      = intval(Yii::$app->request->get('pageSize',100));
        //初始化搜索条件
        //$map['a.src_id'] = $adminId['data'];
        $map = [];        
        //搜索条件
        $params = [];
        $where = [];
        if (yii::$app->request->isGet) {
            $params['status'] = $status;
            if ($status != '') {
                $where[] = ['status'=>$status];
            }else {
                $where[] = ['in','status',[1,2,3,4,5,6]];
            }
            $params['start_time'] = $start_time;
            $params['end_time'] = $end_time;
            if ($start_time != '' && $end_time != '') {
                $where[] = ['between','create_time', $start_time, $end_time];
            }
        } */
        $data = OrderInfo::getOrderSaleInfo(trim($start_time),trim($end_time));
        $dataType = $dataTime = $dataList = [];
        if($data){
            $dataList['dataType'] = array_keys($data['total']);
            $dataList['dataTime'] = array_keys($data['list']);
            $info = [];
            foreach ($dataList['dataType'] as $v) {
                foreach($data['list'] as $val) {
                    if(isset($val[$v])) {
                        $info[$v][] = $val[$v];
                    }
                }
            }
            $i = 0;
            foreach($info as $k => $v) {
                $dataList['dataList'][$i]['name'] = $k;
                $dataList['dataList'][$i]['data'] = $v;
                $i++;
            }
        }
        $dataAll['show'] = $dataList;
        $dataAll['showlist'] = $data;
        return $dataAll;
    }
    /**
    * 函数用途描述:获取服务产品数据
    * @date: 2017年12月23日 下午2:48:04
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function getServiceProduct($start_time,$end_time,$class_ids,$class_type) {
        $data = [];
        if(empty($class_ids)){
            return $data;
        }
        $class_infos = explode(',', $class_ids);
        if($class_type){
            $data = OrderInfo::getOrderProdType2Info($start_time,$end_time,$class_ids);
        }else {
            $data = OrderInfo::getOrderProdTypeInfo($start_time,$end_time,$class_ids);
        }
        //组织页面列表显示数据
        if(!empty($data['list'])) foreach ($data['list'] as $key=>$val){
            if(!empty($val['data']))foreach ($val['data'] as $k1=>$v1){
                $val['data'][$k1]['class_type_name']  = ServiceClass::getClassName($k1);
                $val['data'][$k1]['numb']             = '0';
                $val['data'][$k1]['percent']          = '0％';
                if(!empty($v1)){
                    $val['data'][$k1]['class_type_name'] = $v1['class_type_name'];
                    $val['data'][$k1]['numb'] = $v1['numb'];
                    $val['data'][$k1]['percent'] = round($v1['numb']/$val['total_number']*100,2)."％";
                }
            }
            $data['list'][$key] = $val;
        }
        //组织页面报表显示数据
        $data['show_data']['dataTime'] = [];
        $data['show_data']['dataList'] = [];
        $data['show_data']['dataType'] = [];
        if(!empty($data['list'])){
            $data['show_data']['dataTime'] = array_keys($data['list']);
        }
        $class_infos_arr = ServiceClass::getClassName($class_ids);
        $data['show_data']['dataType'] = explode(',', $class_infos_arr);
        $newArr = $newTypeArr = [];
        if(!empty($class_infos)) foreach ($class_infos as $vv){
            if(!empty($data['list'])) foreach ($data['list'] as $key=>$val){
                foreach ($val['data'] as $kk1=>$vv1){
                    if($vv == $kk1){
                        $newArr[$vv][] = $vv1['numb'];
                    }
                }
            }
        }
        
        $i = 0;
        if(!empty($newArr)) foreach ($newArr as $k=>$v){
            $newTypeArr[$k][] = array_sum($v);
            $data['show_data']['dataList'][$i]['name']  = ServiceClass::getClassName($k);
            $data['show_data']['dataList'][$i]['type']  = 'line';
            $data['show_data']['dataList'][$i]['stack'] = '总量';
            $data['show_data']['dataList'][$i]['data']  = $v;
            $i++;
        }
        //组织一定时间段内的总数据分类数据占比
        $data['type_total'] = $typeTotal = [];
        if(!empty($newTypeArr)) foreach ($newTypeArr as $key=>$val){
            $typeTotal[$key]['name'] = ServiceClass::getClassName($key);
            $typeTotal[$key]['total'] = implode('',$val);
            $typeTotal[$key]['percent'] = round($typeTotal[$key]['total']/$data['total_number']*100,2)."％";
        }
        $data['type_total'] = $typeTotal;
        return $data;
    }
    /**
    * 函数用途描述:格式化产品类目
    * @date: 2017年12月25日 下午2:34:11
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function getProductClass($class_ids) {
        $class_infos = [];
        if(!empty($class_ids)){
            $products = explode(',', $class_ids);
            foreach ($products as $key =>$val){
                $class_infos[$val]['id'] = $val;
                $class_infos[$val]['name'] = ServiceClass::getClassName($val);
            }
        }
        return $class_infos;
    }
    /**
    * 函数用途描述:获取订单品牌数据
    * @date: 2017年12月26日 下午4:55:01
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function getBrandInfo($start_time,$end_time,$brand_ids){
        $data = [];
        if(empty($brand_ids)){
            return $data;
        }
        $brand_infos = explode(',', $brand_ids);
        $data = OrderInfo::getOrderBrandInfo($start_time,$end_time,$brand_ids);
        //组织页面列表显示数据
        if(!empty($data['list'])) foreach ($data['list'] as $key=>$val){
            if(!empty($val['data']))foreach ($val['data'] as $k1=>$v1){
                $val['data'][$k1]['brand_name']  = ServiceBrand::getBrandName($k1);
                $val['data'][$k1]['numb']             = '0';
                $val['data'][$k1]['percent']          = '0％';
                if(!empty($v1)){
                    $val['data'][$k1]['brand_name'] = $v1['brand_name'];
                    $val['data'][$k1]['numb'] = $v1['numb'];
                    $val['data'][$k1]['percent'] = round($v1['numb']/$val['total_number']*100,2)."％";
                }
            }
            $data['list'][$key] = $val;
        }
        //组织页面报表显示数据
        $data['show_data']['dataTime'] = [];
        $data['show_data']['dataList'] = [];
        $data['show_data']['dataType'] = [];
        if(!empty($data['list'])){
            $data['show_data']['dataTime'] = array_keys($data['list']);
        }
        $brand_infos_arr = ServiceBrand::getBrandName($brand_ids);
        //print_r($brand_infos_arr);exit;
        $data['show_data']['dataType'] = explode(',', $brand_infos_arr);
        $newArr = $newTypeArr = [];
        if(!empty($brand_infos)) foreach ($brand_infos as $vv){
            if(!empty($data['list'])) foreach ($data['list'] as $key=>$val){
                foreach ($val['data'] as $kk1=>$vv1){
                    if($vv == $kk1){
                        $newArr[$vv][] = $vv1['numb'];
                    }
                }
            }
        }
        $i = 0;
        if(!empty($newArr)) foreach ($newArr as $k=>$v){
            $newTypeArr[$k][] = array_sum($v);
            $data['show_data']['dataList'][$i]['name']  = ServiceBrand::getBrandName($k);
            $data['show_data']['dataList'][$i]['type']  = 'line';
            $data['show_data']['dataList'][$i]['stack'] = '总量';
            $data['show_data']['dataList'][$i]['data']  = $v;
            $i++;
        }
        //组织一定时间段内的总数据分类数据占比
        $data['type_total'] = $typeTotal = [];
        if(!empty($newTypeArr)) foreach ($newTypeArr as $key=>$val){
            $typeTotal[$key]['name'] = ServiceBrand::getBrandName($key);
            $typeTotal[$key]['total'] = implode('',$val);
            $typeTotal[$key]['percent'] = round($typeTotal[$key]['total']/$data['total_number']*100,2)."％";
        }
        $data['type_total'] = $typeTotal;
        return $data;
    }
    /**
    * 函数用途描述:格式化品牌类目
    * @date: 2017年12月26日 下午5:38:52
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function getProductBrand($brand_ids) {
        $brand_infos = [];
        if(!empty($brand_ids)){
            $brands = explode(',', $brand_ids);
            foreach ($brands as $key =>$val){
                $brand_infos[$val]['id'] = $val;
                $brand_infos[$val]['name'] = ServiceBrand::getBrandName($val);
            }
        }
        return $brand_infos;
    }
    /**
    * 函数用途描述:服务类型数据统计
    * @date: 2017年12月26日 下午7:08:09
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function getServiceTypeSaleInfo($start_time,$end_time,$type_ids){
        $data = [];
        if(empty($type_ids)){
            return $data;
        }
        $type_infos = explode(',', $type_ids);
        $data = OrderInfo::getOrderServiceTypeInfo($start_time,$end_time,$type_ids);
        //组织页面列表显示数据
        if(!empty($data['list'])) foreach ($data['list'] as $key=>$val){
            if(!empty($val['data']))foreach ($val['data'] as $k1=>$v1){
                $val['data'][$k1] = [];
                $val['data'][$k1]['type_name']  = ServiceType::getTypeNames($k1);
                $val['data'][$k1]['numb']  = '0';
                $val['data'][$k1]['percent']  = '0％';
                if(!empty($v1)){
                    $val['data'][$k1]['type_name'] = ServiceType::getTypeNames($k1);
                    $val['data'][$k1]['numb'] = $v1;
                    $val['data'][$k1]['percent'] = round($v1/$val['total_number']*100,2)."％";
                }
            }
            $data['list'][$key] = $val;
            //print_r($val);exit;
        } 
        //组织页面报表显示数据
        $data['show_data']['dataTime'] = [];
        $data['show_data']['dataList'] = [];
        $data['show_data']['dataType'] = [];
        if(!empty($data['list'])){
            $data['show_data']['dataTime'] = array_keys($data['list']);
        }
        $type_infos_arr = ServiceType::getTypeNames($type_ids);
        //print_r($brand_infos_arr);exit;
        $data['show_data']['dataType'] = explode(',', $type_infos_arr);
        $newArr = $newTypeArr = [];
        if(!empty($type_infos)) foreach ($type_infos as $vv){
            if(!empty($data['list'])) foreach ($data['list'] as $key=>$val){
                foreach ($val['data'] as $kk1=>$vv1){
                    if($vv == $kk1){
                        $newArr[$vv][] = $vv1['numb'];
                    }
                }
            }
        }
        $i = 0;
        if(!empty($newArr)) foreach ($newArr as $k=>$v){
            $newTypeArr[$k][] = array_sum($v);
            $data['show_data']['dataList'][$i]['name']  = ServiceType::getTypeNames($k);
            $data['show_data']['dataList'][$i]['type']  = 'line';
            $data['show_data']['dataList'][$i]['stack'] = '总量';
            $data['show_data']['dataList'][$i]['data']  = $v;
            $i++;
        }
        //组织一定时间段内的总数据分类数据占比
        $data['type_total'] = $typeTotal = [];
        if(!empty($newTypeArr)) foreach ($newTypeArr as $key=>$val){
            $typeTotal[$key]['name'] = ServiceType::getTypeNames($key);
            $typeTotal[$key]['total'] = implode('',$val);
            $typeTotal[$key]['percent'] = round($typeTotal[$key]['total']/$data['total_number']*100,2)."％";
        }
        $data['type_total'] = $typeTotal;
        //print_r($data);exit;
        return $data;
        
        
    }
    /**
    * 函数用途描述:格式服务类型
    * @date: 2017年12月26日 下午8:30:28
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function getProductType($type_ids) {
        $type_infos = [];
        if(!empty($type_ids)){
            $types = explode(',', $type_ids);
            foreach ($types as $key =>$val){
                $type_infos[$val]['id'] = $val;
                $type_infos[$val]['name'] = ServiceType::getTypeNames($val);
            }
        }
        return $type_infos;
    }
    /**
    * 函数用途描述
    * @date: 2017年12月26日 下午6:10:50
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function getArr($arr){
        $newArr = [];
        if(!empty($arr)){
            $i = 1;
            foreach ($arr as $key => $val) {
                if ($i <= 5){
                    $newArr[$key] = $val;
                    $i++;
                }else{
                    break;
                }
            }  
        }
        return $newArr;
        
    }
    /**
    * 函数用途描述:报表数据导出
    * @date: 2017年12月27日 下午4:29:57
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function setExport($setTitle,$dataList,$title,$sheet_title) {
        $order_status = ['all'=>'新增订单数','finish'=>'已完成订单','cancel'=>'已取消订单数'];
        if(!empty($setTitle)){
            $newArr [] = ['key'=>'date','value'=>'日期','width'=>'30'];
            foreach ($setTitle as $key=>$val){
                $newArr[$key]['key']    = $key;
                $newArr[$key]['value']  = $order_status[$key];
                $newArr[$key]['width']  = 30;
            }
            $cells = array_values($newArr);
        }
        //print_r($cells);exit;
        $plusData['总数'] = $setTitle;
        $newDataList = array_merge($dataList,$plusData);
        ExcelHelper::CreateExcel($cells,$newDataList,$title,$sheet_title);
    }
    //产品类型导出
    public static function setExport2($setTitle,$dataList,$title,$sheet_title) {
        $total = [];
        $className = [];
        if($setTitle)foreach ($setTitle as $key=>$val){
            $total[$key] = $val['total'];
            $className[$key] = $val['name'];
        }
        if(!empty($dataList)){
            $newDataList = [];
            foreach ($dataList as $key =>$val){
                foreach ($val['data'] as $k=>$v){
                    $newDataList[$key][$k] = $v['numb'];
                }
            }
        }
        $brr = [];
        if(!empty($total) && !empty($newDataList)){
            $keys = array_keys($total);
            foreach ($keys as $v){
                 foreach ($newDataList as $key=>$val){
                     $brr[$key][$v] = $val[$v];
                 }
            }
        }
        if(!empty($total)){
            $newTotal [] = ['key'=>'date','value'=>'日期','width'=>'30'];
            foreach ($total as $key=>$val){
                $newTotal[$key]['key']    = $key;
                $newTotal[$key]['value']  = $className[$key];
                $newTotal[$key]['width']  = 30;
            }
            $cells = array_values($newTotal);
        }
        $plusData['总数'] = $total;
        $newDataList = array_merge($brr,$plusData);
        ExcelHelper::CreateExcel($cells,$newDataList,$title,$sheet_title);
    }
    /**
    * 函数用途描述
    * @date: 2017年12月28日 下午1:56:03
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function getAbnormalOrders($start_time,$end_time,$reason_ids) {
        $data = [];
        if(empty($reason_ids)){
            return $data;
        }
        $reason_infos = explode(',', $reason_ids);
        $data = OrderInfo::getAbnormalOrdersInfo($start_time,$end_time,$reason_ids);
        //组织页面列表显示数据
        if(!empty($data['list'])) foreach ($data['list'] as $key=>$val){
            if(!empty($val['data']))foreach ($val['data'] as $k1=>$v1){
                $val['data'][$k1] = [];
                $val['data'][$k1]['reason_name']  = Orderinfo::getReasonNames($k1);
                $val['data'][$k1]['numb']  = '0';
                $val['data'][$k1]['percent']  = '0％';
                if(!empty($v1)){
                    $val['data'][$k1]['reason_name'] = Orderinfo::getReasonNames($k1);
                    $val['data'][$k1]['numb'] = $v1;
                    $val['data'][$k1]['percent'] = round($v1/$val['total_number']*100,2)."％";
                }
            }
            $data['list'][$key] = $val;
            //print_r($val);exit;
        }
        //组织页面报表显示数据
        $data['show_data']['dataTime'] = [];
        $data['show_data']['dataList'] = [];
        $data['show_data']['dataType'] = [];
        if(!empty($data['list'])){
            $data['show_data']['dataTime'] = array_keys($data['list']);
        }
        $data['show_data']['dataType'] = array_values(Orderinfo::getReasonNames($id=''));
        $newArr = $newTypeArr = [];
        if(!empty($reason_infos)) foreach ($reason_infos as $vv){
            if(!empty($data['list'])) foreach ($data['list'] as $key=>$val){
                foreach ($val['data'] as $kk1=>$vv1){
                    if($vv == $kk1){
                        $newArr[$vv][] = $vv1['numb'];
                    }
                }
            }
        }
        $i = 0;
        if(!empty($newArr)) foreach ($newArr as $k=>$v){
            $newTypeArr[$k][] = array_sum($v);
            $data['show_data']['dataList'][$i]['name']  = Orderinfo::getReasonNames($k);
            $data['show_data']['dataList'][$i]['type']  = 'line';
            $data['show_data']['dataList'][$i]['stack'] = '总量';
            $data['show_data']['dataList'][$i]['data']  = $v;
            $i++;
        }
        //组织一定时间段内的总数据分类数据占比
        $data['type_total'] = $typeTotal = [];
        if(!empty($newTypeArr)) foreach ($newTypeArr as $key=>$val){
            $typeTotal[$key]['name'] = Orderinfo::getReasonNames($key);
            $typeTotal[$key]['total'] = implode('',$val);
            $typeTotal[$key]['percent'] = round($typeTotal[$key]['total']/$data['total_number']*100,2)."％";
        }
        $data['type_total'] = $typeTotal;
        return $data;
    }
    //格式化订单异常原因数据
    public static function getReasonType($reason_ids) {
        $reason_infos = [];
        if(!empty($reason_ids)){
            $types = explode(',', $reason_ids);
            foreach ($types as $key =>$val){
                $reason_infos[$val]['id'] = $val;
                $reason_infos[$val]['name'] = Orderinfo::getReasonNames($val);
            }
        }
        return $reason_infos;
    }
}
<?php
namespace webapp\logic;
use webapp\models\SaleOrderSn;
use yii;
use webapp\models\SaleOrder;
use webapp\models\SaleOrderView;
use webapp\models\Account;
use common\models\ServiceClass;
use webapp\models\BaseModel;


/***
 * @author chengjuanjuan<chengjuanjuan@c-ntek.com>
 * @created_at 2017-12-14 14:01
 * desc the logic of sale orders
 */


class SaleOrderLogic extends BaseLogic
{


    /**
     * 函数用途描述：
     * @date: 2017年12月14日 下午14:48:01
     * @author: chengjuanjuan<chengjuanjuan@c-ntek.com>
     * @param: $id
     * @return:$data
     */
    public static function index($intentionType)
    {
        $query['account_id']     = intval(Yii::$app->request->get('account_id'));
        $query['account_name']   = trim(Yii::$app->request->get('account_name'));
        $query['brand_id']       = intval(Yii::$app->request->get('brand_id'));
        $query['start_time']     = strtotime(Yii::$app->request->get('plan_start_time'));
        $query['end_time']       =  strtotime(Yii::$app->request->get('plan_end_time'));
        $query['currentPage']    = Yii::$app->request->get('page',1);
        $query['pageSize']       = Yii::$app->request->get('pageSize',10);
        $query['src_type']       = BaseModel::SRC_FWS;
        $main                    = self::getDepartmentId();
        $query['src_id']         = $main['data'];
        $query['intention_type'] = $intentionType;

        $data = SaleOrderView::getLists($query);
        return $data;
    }

    /**
     * @desc 售后商品添加
     * @author:程娟娟
     * @createAt:20171215
     **/
    public static function add()
    {
        //售后订单（产品表数据）
        $saleData  = [];
        $accountId = intval(yii::$app->request->post('account_id'));
        $accountName = trim(yii::$app->request->post('account_name'));
        $source         =  intval(Yii::$app->request->post('source',0));
        if(!$accountId)
            return self::error("$accountName".'用户不存在');
        
        $time = time();
        $account = Account::findOneByAttributes(['id'=>$accountId],"department_id,direct_company_id,department_top_id");

        $saleData['prod_id'] = intval(yii::$app->request->post('prod_id'));
        $saleData['department_top_id'] = $account['department_id'];
        $saleData['direct_company_id'] = $account['direct_company_id'];
        $saleData['department_id']      = $account['department_top_id'];
        $saleData['account_id']         = $accountId;
        $saleData['type_id']       = trim(yii::$app->request->post('type_id'));
        $saleData['prod_series']   = trim(yii::$app->request->post('prod_series')); //商品系列
        $saleData['brand_id']      = Yii::$app->request->post('brand_id');
        $saleData['class_id']      = Yii::$app->request->post('class_id');
        $saleData['status']        = yii::$app->request->post('status','1');
        $saleData['produce_time']  = strtotime(yii::$app->request->post('produce_time'));
        $saleData['buy_time']      = strtotime(yii::$app->request->post('buy_time'));
        $saleData['warranty']      = intval(yii::$app->request->post('warranty', 30));
        $saleData['warranty_num']  = intval(yii::$app->request->post('warranty_num'));
        $saleData['warranty_type'] = intval(yii::$app->request->post('warranty_type'));
        $saleData['create_time']   = $time;
        $saleData['update_time']   = $time;
        $saleData['intention_type']= 1;
        $saleData['serial_number'] = trim(yii::$app->request->post('serial_number'));//商品序列号
        $saleData['info']          = trim(yii::$app->request->post('info'));

        $res = SaleOrder::add($saleData);
        if ($res)
        {
            if($source == 1){
                $sale = SaleOrder::findOneByAttributes(['id'=>$res]);
                return self::success([
                    'type' => $sale['intention_type']
                ],'添加成功');
            }
            return self::success([
                'id' => $res
            ],'添加成功');
        }
        return self::error('添加失败');
    }


    /**
    * @desc 修改
     * @author sxz
     * @createAt 2018-8-15 16:06
     * 订单详情页，小程序客户下单添加的产品，后台编辑
     **/
    public static function edit()
    {//处理数据
        //售后订单
        $time = time();
        $saleData['id']            =    trim(yii::$app->request->post('id'));
        $saleData['type_id']       =    trim(yii::$app->request->post('type_id'));
        $saleData['prod_id']       =    trim(yii::$app->request->post('prod_id'));     //产品库产品id
        $saleData['prod_series']   =    trim(yii::$app->request->post('prod_series')); //商品型号
        $saleData['serial_number'] =    trim(yii::$app->request->post('serial_number'));//商品序列号
        $saleData['brand_id']      =    Yii::$app->request->post('brand_id');
        $saleData['class_id']      =    Yii::$app->request->post('class_id');
        $saleData['produce_time']  =    strtotime(yii::$app->request->post('produce_time'));
        $saleData['buy_time']      =    strtotime(yii::$app->request->post('buy_time'));
        $saleData['warranty_num']  =    intval(yii::$app->request->post('warranty_num'));
        $saleData['warranty_type'] =    intval(yii::$app->request->post('warranty_type'));
        $saleData['info']          =    trim(yii::$app->request->post('info'));
        $saleData['update_time']   =    $time;
        $saleData['serial_number'] =    trim(yii::$app->request->post('serial_number'));//商品序列号
        $saleData['info']          =    trim(yii::$app->request->post('info'));
        $saleData['wechat_show']   = 1;
        $saleData['prod_name_wechat']   = '';
        $res = SaleOrder::edit($saleData);
        if ($res) {
            /*return self::success([
                'id' => $saleData['id']
            ],'编辑成功');*/
            return self::success('','编辑成功');
        }
        return self::error('编辑失败');
    }
    //编辑产品信息 2018-05-24
    public static function editProduct(){
        $saleData['id']            =   trim(yii::$app->request->post('id'));
        $saleData['serial_number'] =   trim(yii::$app->request->post('serial_number'));//商品序列号
        $saleData['produce_time']  =   strtotime(yii::$app->request->post('produce_time'));
        $saleData['buy_time']      =   strtotime(yii::$app->request->post('buy_time'));
        $saleData['warranty_num']  =   intval(yii::$app->request->post('warranty_num',0));
        $saleData['warranty_type'] =   intval(yii::$app->request->post('warranty_type',0));
        $saleData['info']          =   trim(yii::$app->request->post('info'));
        $saleData['update_time']   =   time();
        $saleData['scope_img']     =   trim(yii::$app->request->post('scope_img', ''));      //质保图片
        $orderData['is_scope']     =   (int)yii::$app->request->post('is_scope',0);    //订单的质保凭证
        $orderData['order_no']     =   trim(yii::$app->request->post('order_no',''));    //订单号码
        //凭证图片
        $file = Yii::$app->request->post("OrderForm", '');
        if ($file['scope_img']) {
            $orderData['scope_img'] = implode(',', $file['scope_img']);
        }
        $res      = SaleOrder::edit($saleData);
        $orderRes = OrderLogic::editScopeInfo($orderData);
        if ($res && $orderRes['code'] == 200) {
            return self::success('','编辑成功');
        }
        return self::error('编辑失败');
    }



    /**
    * @desc 获取用户信息
     * @author chengjuanjuan
     **/
    public static function getAccount(){
        $accountId    =  isset($_REQUEST['accountId'])?intval($_REQUEST['accountId']):0;
       // $accountName  = yii::$app->request->post('accountName','');
        $accountName    = yii::$app->request->get('q');
        if($accountId>0 && empty($accountName)){  //其他页面只带着客户id过来
            $account       =  Account::findOne(['id'=>$accountId]);
            return $result = ['account_id'    => $accountId,
                'account_name' =>$account['account_name']
            ];
        }else if($accountName&&!$accountId){//联想带出accountList
                $adminId = parent::getDepartmentId();
                $result = Account::getAccountInfo($accountName, $adminId['data']);
                exit(json_encode($result));
            }

    }



    //输入客户姓名带回相关的产品信息
    public static function getData(){
        $accountId    =  isset($_REQUEST['accountId'])?intval($_REQUEST['accountId']):0;
        $accountName  = yii::$app->request->post('accountName','');
        $saleId       = yii::$app->request->post('saleId',0);

        //通过客户名称带出客户产品
        if($accountName && $accountId){
            $result = SaleOrder::getProductByAccount($accountName);
            exit(json_encode($result));
        }
    }


    //通过产品id去产品库里捞数据
    public static function getProduct(){
        $productId    =  yii::$app->request->post('productId',0);
        if($productId){
            $result = SaleOrder::findOne(['id'=>$productId]);
           // $typeList = self::getTypeList();
           // $brand = self::getBrandList();
            //echo '<pre>';
            //print_R($result);exit;
            //$className = ServiceClass::getTitle($result['class_id']);
            $info = (array)$result;
            $firstClass             = self::getClassList();
            $classInfo              = ServiceClass::findOne(['id'=>$result->class_id]);
            $secondClass            = ServiceClass::getServiceClass($classInfo['pid']);
            $info['type_id']        = $result->type_id;
            $info['type_name']      = $result->type_name;
            $info['brand_id']       = $result->brand_id;
            $info['brand_name']     = $result->brand_name;
            $info['class_id']       = $result->class_id;
            $info['class_name']     = $result->class_name;
            $info['prod_name']      = $result->prod_name;
            $info['prod_series']    = $result->prod_series;
            $info['serial_number']  = $result->serial_number;
            $info['produce_time']   =  date("Y-m-d H:i:s",$result->produce_time);
            $info['buy_time']       =  date("Y-m-d H:i:s",$result->buy_time);
            $info['warranty']       = $result->warranty;
            $info['info']           = $result->info;
            $info['firstClass']     = $firstClass['data'];
            $info['secondClass']    = $secondClass;
            $info['classPid']       = $classInfo['pid'];
            exit(json_encode($info));
        }else{
            $productArr = [];
            $main = self::getDepartmentId();
            $productName = yii::$app->request->get('q');
            $list = SaleOrder::getProductByName($productName,$main['data']);
            foreach($list as $k=>$v){
                $arr['id']      = $v['id'];
                $arr['name']    = $v['prod_name'].'/'.$v['prod_series'].'/'.$v['serial_number'];
                $productArr[$v['id']] = $arr;
            }
            exit(json_encode($productArr));
        }
    }


    public static function BrandList(){
        $brand         = self::getBrandList();
        $brandList     = $brand['data'];
        $query['src_type'] = BaseModel::SRC_FWS;
        $data              = self::getDepartmentId();
        $query['src_id']   = $data['data'];
        $list = SaleOrder::findBrand($query);
        $arr = [];
        foreach($list as $v){
            $brandId = $v['brand_id'];
            if(key_exists($brandId,$brandList)){
                $arr[$brandId] = $brandList[$brandId];
            }
        }
        if(empty($arr)){
            $arr = $brandList;
        }
        return $arr;
    }

    /**
     * 更新状态
     * @return array
     */
    public static function changeStatus()
    {
        $id     = intval(yii::$app->request->post('id'));
        $status = intval(yii::$app->request->post('status'));

        if($id > 0)
        {
            $info = SaleOrder::getOne(['id'=>$id]);
            if($info['status'] == $status)
            {
                return self::error('该状态已存在，无需修改');
            }
            else
            {
                $data = ['status'=>$status,'id'=>$id];
                $res = SaleOrder::changeData($data);
                return self::success($res);
            }
        }
    }

    //品牌id带出一级分类
    public static function getClassData($brandId=0,$isExit=1){
        if(!$brandId){
            $brandId    =  intval(yii::$app->request->post('brandId'));
        }
        $mainId = self::getDepartmentId();
        $qualificationList       = BrandQualification::findOne(['src_id'=>$mainId['data'],'brand_id'=>$brandId]);
        $classIds =  $qualificationList['class_id'];
        $classList = ServiceClass::getClassName($classIds,'',1);
        if($isExit){
            exit(json_encode($classList));
        }
        $list = [];
        foreach($classList as $v){
            $list[$v['id']] = $v['title'];
        }
        return $list;
    }

    public static function getInfo($id=''){
        if(!$id){
            $id = intval(yii::$app->request->get('id'));
        }
        return  SaleOrderView::findOne(['id'=>$id]);
    }

    public static function warranty()
    {
        //质保单位
        $warranty = self::getWarranty();
        if ($warranty) {
            return  $warranty;
        }
        else{
            return $warranty = [];
        }
    }

}
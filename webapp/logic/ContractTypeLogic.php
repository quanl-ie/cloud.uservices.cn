<?php
namespace webapp\logic;

use webapp\models\ContractType;
use Yii;

class ContractTypeLogic extends BaseLogic
{
    /**
     * 获取合同类型列表
     * User: quan
     * Date: 2018/8/6
     * Time: 10:10
     * @return array
     */
    public static function getIndex()
    {

        $dcId = self::getDirectCompanyId();
        $where = ['direct_company_id' => $dcId];

        //获取列表数据
        return ContractType::getList($where);
    }


    /**
     * 添加数据
     * User: quan
     * Date: 2018/8/6
     * Time: 10:10
     * @return array
     * @throws \Exception
     */
    public static function add()
    {
        $departmentId = self::getDepartmentId();
        $dcId = self::getDirectCompanyId();

        //接收参数
        $className = trim(Yii::$app->request->post('class_name', ''));
        $sort = intval(Yii::$app->request->post('sort', 0));
        if ($className == '') {
            return self::error('保存失败', 20002);
        }
        $data['department_id'] = $departmentId;
        $data['direct_company_id'] = $dcId;
        $data['type_name'] = $className;
        $data['rank'] = $sort;
        $data['status'] = 1;
        $data['create_id'] = Yii::$app->user->id;;
        $data['create_time'] = time();
        $res = ContractType::add($data);
        if ($res) {
            return self::success('', '保存成功');
        }
        return self::error('保存失败', 20002);

    }

    /**
     * 修改数据列表
     * User: quan
     * Date: 2018/8/6
     * Time: 13:10
     * @return array
     */
    public static function beforeEdit()
    {

        $classId = intval(Yii::$app->request->get('id', 0));
        if ($classId == 0) {
            return self::error('修改失败', 20001);
        }
        $class = ContractType::getList(['id' =>$classId]);
        return $class['list'];
    }

    /**
     * 修改
     * User: quan
     * Date: 2018/8/6
     * Time: 13:10
     * @return array
     * @throws \Exception
     */
    public static function edit()
    {
        //接收参数
        $classId = intval(Yii::$app->request->post('id', 0));
        $className = trim(Yii::$app->request->post('class_name', ''));
        $sort = intval(Yii::$app->request->post('sort', 0));
        if ($classId == 0) {
            return self::error('保存失败', 20001);
        }
        if ($className == '') {
            return self::error('保存失败', 20003);
        }
        $data['id'] = $classId;
        $data['type_name'] = $className;
        $data['rank'] = $sort;

        $res = ContractType::edit($data);

        if ($res) {
            return self::success('', '保存成功');
        }
        return self::error('保存失败', 20005);
    }

    /**
     * 修改类目状态（禁用、启用）
     * User: quan
     * Date: 2018/8/6
     * Time: 13:10
     * @return array
     * @throws \Exception
     */
    public static function changeStatus()
    {
        $id = intval(Yii::$app->request->post('id', 0));
        $status = intval(Yii::$app->request->post('status', 0));

        if ($id == 0) {
            return self::error('操作失败', 20001);
        }

        if ($status == 0) {
            return self::error('操作失败', 20002);
        }
        $data['id'] = $id;
        $data['status'] = $status;
        $res = ContractType::changeStatus($data);
        if ($res) {
            return self::success('', '操作成功');
        }
        return self::error('操作失败', 20003);
    }

    /**
     * @return array
     * 创建合同调取列表
     */
    public static function getTypeList()
    {
        $Data = ContractType::getLists(['direct_company_id'=>self::getDirectCompanyId(),'status'=>1]);
        $contractClassifyList = isset($Data)?$Data:[];
        $DataList = array_column($contractClassifyList,'type_name','id');
        return $DataList;
    }

}
<?php
    
    namespace webapp\logic;
    
    use common\models\PaySiginConfig;
    use Yii;
    use webapp\models\BaseModel;
    use common\models\TechnicianSiginConfig;


    /**
     * Class SiginSettingLogic
     * @package webapp\logic
     * @desc 服务商设置签到开关
     * @autor:chengjuanjuan
     * @date :2018-06-01
     */
    
    class SiginSettingLogic extends BaseLogic
    {
        /**
         * 列表显示数据
         * User: chengjuanjuan
         * Email:chengjuanjuan@uservices.cn
         * @return mixed
         */
        public static function BeforeList($type='')
        {
            $derectCompanyId = self::getDirectCompanyId();
            if($type == ''){
                   $info = TechnicianSiginConfig::findOne(['direct_company_id'=>$derectCompanyId]);
            }else{
                  $info = PaySiginConfig::findOne(['direct_company_id'=>$derectCompanyId]);
            }
            if (!empty($info)) {
                $data['list'] = $info;
            }else{
                $data['list'] = [];
                $data['list']['department_id'] = self::getDepartmentId();
            }
              if($type == ''){
                    $data['option'] = TechnicianSiginConfig::setStatus();
              }else{
                    $data['option'] = PaySiginConfig::setStatus();
              }
            return $data;
        }
    
        /**
         * 修改
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/5/7
         * Time: 15:20
         * @return array
         * @throws \Exception
         */
        public static function edit($type='')
        {
            $id              =  intval(Yii::$app->request->post('id',0));
            $siginStatus     =  intval(Yii::$app->request->post('sigin_status',2));
            $payStatus     =  intval(Yii::$app->request->post('pay_status',2));
            $serviceRadius   =  intval(Yii::$app->request->post('service_radius',0));
            $data['id'] = $id;
            if($type == ''){
                 $data['service_radius']   = $serviceRadius;
            }
            $data['direct_company_id'] = self::getDirectCompanyId();
            if($type == 'pay'){
                  $data['pay_status']     =  $payStatus;
            }else{
                  $data['sigin_status']     =  $siginStatus;
            }
            if($type == 'pay' || $type == 'sign'){
                  $res = PaySiginConfig::edit($data);
            }else{
                  $res = TechnicianSiginConfig::edit($data);
            }
            if ($res) {
                return self::success('','保存成功！');
            }else{
                return self::error('保存失败');
            }
            
        }
    }
<?php
namespace webapp\logic;

use Yii;
use common\models\ServiceType;
use webapp\models\ManufactorType;

class TypeLogic extends BaseLogic
{
    /**
     * 列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 15:33
     * @return array
     */
    public static function getIndex()
    {

        $dcId     = self::getDirectCompanyId();
        $topId    = self::getTopId();

        if ($dcId == $topId) {
            $flag = 1;
        } else {
            $flag = 2;
        }
    
        $where = [['=','a.direct_company_id',$dcId],['=','b.department_id',$topId]];

        //获取列表数据
        $res = ManufactorType::getPageList($where);

        if (isset($flag)) {
            $res['flag'] = $flag;
        }

        if (empty($res['list'])) {
            return $res;
        }


        foreach ($res['list'] as $key => $val) {

            if ($val['status'] == 1) {
                $res['list'][$key]['status_desc'] = '已启用';
            }else{
                $res['list'][$key]['status_desc'] = '已禁用';
            }
        }

        return $res;
    }

    /**
     * 添加前数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 15:33
     * @return mixed
     */
    public static function beforeAdd()
    {
        $topId    = self::getTopId();

        //获取已添加的类型
        $manufactorType = ManufactorType::getPageList([['b.department_id' => $topId]]);

        //获取id
        $typeIds = [];
        if (!empty($manufactorType['list'])) {
            $typeIds = array_column($manufactorType['list'],'type_id');
        }

        //获取所有类型数据
        $typeList   = ServiceType::getList(['department_id' => [0,$topId]]);


        $selfType   = [];
        $commonType = [];

        foreach ($typeList as $key => $val) {
            if ($val['department_id'] == $topId) {
                $selfType[$val['id']]   = $val['title'];
            }else{
                $commonType[$val['id']] = $val['title'];
            }
        }

        foreach ($selfType as $val) {
            unset($commonType[array_search($val,$commonType)]);
        }

        //组合数据
        $data['type_ids']        = $typeIds;
        $data['type_list']       = array_replace_recursive($commonType,$selfType);

        return $data;
    }

    /**
     * 添加
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 15:33
     * @return array
     * @throws \Exception
     */
    public static function add()
    {
        //接收参数
        $typeId = Yii::$app->request->post('type_id','');

        if ($typeId == '') {
            return self::error('请添加服务类型',20001);
        }

        //定义变量
        $time         = time();
        $topId        = self::getTopId();
        $departmentId = self::getDepartmentId();

        //查询自身所有类型
        $type    = ManufactorType::getList(['direct_company_id' => $topId]);

        //提取类型id
        $typeIds = array_column($type,'type_id','id');

        //计算出需要添加的类型
        $addtypeId  = array_diff($typeId,$typeIds);

        $data            = [];
        $userId          = Yii::$app->user->id;
        $serviceTypeName = ServiceType::getName($addtypeId);
        //组合需要添加的类型
        if (!empty($addtypeId))
        {
            foreach ($addtypeId as $key => $val) {

                $data[$key]['title']           = $serviceTypeName[$val];
                $data[$key]['department_id']   = $topId;
                $data[$key]['sort']            = 1;
                $data[$key]['status']          = 1;
                $data[$key]['del_status']      = 1;
                $data[$key]['create_id']       = $userId;
                $data[$key]['create_time']     = $time;
                $data[$key]['update_id']       = $userId;
                $data[$key]['update_time']     = $time;
            }
        }

        $res = ServiceType::add($data,$typeIds,$departmentId);

        if ($res) {
            return self::success('','保存成功');
        }
        return self::error('保存失败',20002);

    }

    /**
     * 改变状态
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/6
     * Time: 15:34
     * @return array
     */
    public static function changeStatus()
    {
        $id     = intval(Yii::$app->request->post('id',0));
        $status = intval(Yii::$app->request->post('status',0));

        if ($id == 0) {
            return self::error('操作失败',20001);
        }

        if ($status == 0) {
            return self::error('操作失败',20002);
        }

        $res = ServiceType::changeStatus($id,$status);

        if ($res) {
            return self::success('','操作成功');
        }
        return self::error('操作失败',20003);
    }
}

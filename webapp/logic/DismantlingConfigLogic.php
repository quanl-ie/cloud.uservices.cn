<?php
namespace webapp\logic;

use Yii;
use webapp\models\DismantlingConfig;

class DismantlingConfigLogic extends BaseLogic
{
    public static function index($type = 0)
    {
        $topId = self::getTopId();
        $dcId  = self::getDirectCompanyId();
    
        $list = DismantlingConfig::setStatus();
        if (!empty($list)) {
            $data['list'] = $list;
        }else{
            $data['list'] = [];
        }
        
        $disData = DismantlingConfig::getOne(['department_id'=>$topId,'direct_company_id'=>$dcId,'type'=>$type]);
        
        if (!empty($disData)) {
            $data['self'] = $disData;
        }else{
            $data['self'] = [];
        }
        return $data;
    }
    
    public static function add($type = 0)
    {
        
        $id     = intval(Yii::$app->request->post('id',0));
        $status = intval(Yii::$app->request->post('status',0));
        
        if ($status == 0) {
            return self::error('操作失败',20001);
        }
        
        if ($id != 0) {
            $data['id'] = $id;
        }
        
        $time = time();
        
        $data['department_id']     = self::getTopId();
        $data['direct_company_id'] = self::getDirectCompanyId();
        $data['status']            = $status;
        $data['type']               = $type;
        $data['optare_id']         = self::getLoginUserId();
        $data['create_time']       = $time;
        $data['update_time']       = $time;
        
        $res = DismantlingConfig::add($data);
        
        if ($res) {
            return self::success('','操作成功');
        }
        return self::error('操作失败',20002);
    }
}
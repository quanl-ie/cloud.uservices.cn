<?php
namespace webapp\logic;

use common\helpers\Helper;
use common\models\SysConfigOption;
use webapp\models\Department;
use webapp\models\Menu;
use yii;
use common\models\ServiceClass;
use common\models\ServiceBrand;
use common\models\ServiceType;

use webapp\models\Region;
use common\models\User;
use webapp\models\BaseModel;

use common\models\DictEnum;
use common\models\Depot;
use common\models\ScheduleSet;
use common\models\WorkOrderAssign;

use webapp\models\ManufactorClass;
use webapp\models\ManufactorBrand;
use webapp\models\ManufactorType;

use webapp\models\Role;

class BaseLogic
{

    /**
     * 成功返回
     * @author xi
     * @date 2017-12-14
     */
    public static function success($data, $message = 'success', $code = 200)
    {
        return [
            'success' => true,
            'data'    => $data,
            'message' => $message,
            'code'    => $code
        ];
    }

    /**
     * 失败返回
     * @author xi
     * @date 2017-12-14
     */
    public static function error($message,$code=20001)
    {
        return [
            'success' => false,
            'message' => $message,
            'code'    => $code,
            'data'    => []
        ];
    }
    
    /**
     * 获取部门id
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 10:11
     * @return int
     */
    public static function getDepartmentId()
    {
        if(!Yii::$app->user->isGuest){
            $id =  Yii::$app->user->getIdentity()->department_id;
            return $id;
        }
        return 0;
    }


    /**
     * @desc:获取用户登录部门ID及其所有子ID
     * @return array
     * $param:$type [1,2,3]
     */
    public static function getDepartmentAllSonIds($departmentId=0,$type = [],$directCompanyId='')
    {
        if(!isset($departmentId)){
            $departmentId = self::getDepartmentId();
        }
        if(!is_array($departmentId)){
            $departmentId = array($departmentId);
        }
        $sons = [];
        $sons = Department::getDepartmentSonIds($departmentId,$type,$directCompanyId);
        if($sons){
            $sons = array_merge_recursive($departmentId,$sons);
        }else{
            $sons = $departmentId;
        }
        return $sons;
    }
    
    /**
     * 获取顶级id
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 10:11
     * @return int
     */
    public static function getTopId()
    {
        if(!Yii::$app->user->isGuest){
            $id =  Yii::$app->user->getIdentity()->department_id;
            if($id){
                $depart = Department::findOne(['id'=>$id]);
                if (isset($depart) && $depart->parent_id == '') {
                    return $id;
                }
                else
                {
                    $array=explode(',', $depart->parent_ids);
                    return $array[0];
                }
            }

        }
        return 0;
    }
    
    /**
     * 获取所有父级id
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 10:30
     * @return array
     */
    public static function getParentIds()
    {
        if(!Yii::$app->user->isGuest){
            $id =  Yii::$app->user->getIdentity()->department_pids;
            if (!empty($id)) {
                return explode(',', $id);
            }
            return [];
        }
        return [];
    }

    /**
     * 获取服务商登录id
     * @return int
     * @author xi
     * @date 2018-2-6
     */
    public static function getLoginUserId()
    {
        if(!Yii::$app->user->isGuest){
            return Yii::$app->user->getIdentity()->id;
        }
        return 0;
    }


    /**
     * 获取计量单位
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/29
     * Time: 16:03
     * @param $departmentId
     * @param $departmentTopId
     * @return array
     */
    public static function getUnit($departmentId='')
    {
        $unit = DictEnum::getList(['dict_key'=>'enum_unit_id']);
        $data = [];
        if ($unit) {
            foreach ($unit as $val) {
                $data[$val['dict_enum_id']] = $val['dict_enum_value'];
            }
        }
        return $data;
    }

    /**
     * 获取自有可用库房
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/29
     * Time: 16:03
     * @param $directCompanyId 直属公司ID
     * @param $departmentTopId
     * @return array
     */
    public static function getDepot($directCompanyId)
    {
        $depot = Depot::getList(['direct_company_id'=>$directCompanyId, 'status'=>0, 'is_delete'=>0]);
        $data = [];
        if ($depot) {
            foreach ($depot as $val) {
                $data[$val['id']] = $val['name'];
            }
        }
        return $data;
    }

    /**
     * 获取质保单位
     * User: xingq
     * Email: liuxingqi@services.cn
     * @param $departmentId
     * @param $departmentTopId
     * Date: 2018/3/26
     * Time: 13:38
     * @return array
     */
    public static function getWarranty()
    {

        $dict = DictEnum::getList(['dict_key'=>'enum_warranty_type']);
        $data = [];
        if ($dict) {
            foreach ($dict as $key => $val) {
                $data[$val['dict_enum_id']] = $val['dict_enum_value'];
            }
        }
        return self::success($data,'成功');
    }
    
    /**
     * 获取仓储设置列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 10:07
     * @param $directCompanyId
     * @return array
     */
    public static function getSysConfigOption($directCompanyId)
    {
        $sys = SysConfigOption::getList(['config_key'=>'storage_status','direct_company_id' => $directCompanyId]);
        $data = [];
        if ($sys) {
            foreach ($sys as $key => $val) {
                $data[$val['option_id']] = $val['option_name'];
            }
        }
        return self::success($data,'成功');
    }

    
    /**
     * 获取班次重复模式
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/26
     * Time: 13:38
     * @return array
     */
    public static function getRepeateType($departmentId,$departmentTopId = '')
    {
        
        $dict = DictEnum::getList(['dict_key'=>'schedule_repeate_type']);
        $data = [];
        if ($dict) {
            foreach ($dict as $key => $val) {
                $data[$val['dict_enum_id']] = $val['dict_enum_value'];
            }
        }
        return self::success($data,'成功');
    }
    
    
    /**
     * 服务类型获取  参数不存在返回自身所有服务类型   存在时根据参数查询
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/28
     * Time: 13:54
     * @param string $typeId  类型id
     * @param array $status 状态  1启用  2禁用
     * @return array
     */
    public static function getType($typeId = [],$status = [1,2])
    {

        $topId = self::getTopId();
        $dcId  = self::getDirectCompanyId();
    
        $where = [['=','a.direct_company_id',$dcId],['=','b.department_id',$topId],['=','b.status',$status],['in','b.id',$typeId]];
    
        $data  = ManufactorType::getPageList($where);
        
        if($data['list']){
            return array_column($data['list'],'title','type_id');
        }else {
            return [];
        }
    }
    
    /**
     * 分类获取 参数不存在返回自身所有顶级分类   存在时返回当前参数下级分类  exist 用于判断是否存在下级
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/28
     * Time: 13:53
     * @param int $pid  父级id
     * @param array $status  状态  1启用  2禁用
     * @return array
     */
    public static function getClass($pid = 0,$status = [1,2])
    {
    
        $topId    = self::getTopId();
        $dcId     = self::getDirectCompanyId();
        
        $where = [['=','a.direct_company_id',$dcId],['=','b.department_id',$topId],['in','b.status',$status],['in','b.parent_id',$pid]];
    
        $manufactorClass = ManufactorClass::getPageList($where);
        
        $classIds = [];
        if (!empty($manufactorClass['list'])) {
            $classIds = array_column($manufactorClass['list'],'class_id');
        }
        
        //查询是否还有下级
        $exist = CategoryLogic::existNext($classIds,$dcId);
    
        $data = [];
    
        if (isset($manufactorClass['list']))
        {
            foreach ($manufactorClass['list'] as $key => $val) {
                $data[$key]['id']         = $val['class_id'];
                $data[$key]['class_name'] = $val['title'];

                if (isset($exist[$val['class_id']])) {
                    $data[$key]['exist'] = $exist[$val['class_id']];
                }
            
            }
        }
        return $data;
        
    }
    
    /**
     * 品牌获取  参数不存在返回自身所有品牌   存在时根据参数查询
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/28
     * Time: 13:54
     * @param string $brandId  品牌id
     * @param array $status   状态  1启用  2禁用
     * @return array
     */
    public static function getBrand($brandId = [],$status = [1,2])
    {
        $topId    = self::getTopId();
        $dcId     = self::getDirectCompanyId();
        
        $where = [['=','a.direct_company_id',$dcId],['=','b.department_id',$topId],['=','b.status',$status],['in','b.id',$brandId]];
        
        $data = ManufactorBrand::getPageList($where);
        
        if(isset($data['list']) && !empty($data['list'])){
            return array_column($data['list'],'title','brand_id');
        }else {
            return [];
        }

    }

    //登陆信息
    public static function getLoginUserInfo()
    {
        if(!Yii::$app->user->isGuest){
            return Yii::$app->user->getIdentity();
        }
        return '';
    }


    /**
     * @desc   获取数据权限
     * $param  $specialAuth 已有某个权限，$replaceAuth 需要被替换的权限，默认0，0
     * @return array departmentId数组 和departmentList
     * @author chengjuanjuan
     * @date   2018-06-07
     */
    public static function getListDataAuth($specialAuth=0,$replaceAuth=0)
    {   $userId                 = 0;

        $deparmentId            = Yii::$app->request->get('department_ids','0');
        $derectCompanyId        = self::getDirectCompanyId();
        $topId                  = self::getTopId();
        $department_name        = '';
        //获取搜索栏传递的departmentId的角色权限
        if($deparmentId){
            if(!is_array($deparmentId)){
                $deparmentId   =  explode(',',$deparmentId);
            }
            foreach ($deparmentId as $val){


                if(Department::getDirectCompanyIdById($val) != $topId && $val!=$derectCompanyId){
                    $department_info = department::getDepartmentChildren($val,2,0);
                    if($department_info){
                        foreach ($department_info as $v){
                            $didList[] = $v['id'];
                            if($val==$v['id']){
                                $department_name .= $v['name'].';';
                            }
                        }
                    }

                }else{
                    $didList[] = $val;
                    $departmentInfo = Department::getOne(['id'=>$val]);
                    $department_name .= $departmentInfo->name.';';
                }
            }
            $department_name = trim($department_name,';');
            //$didList = $deparmentId;
        }else{
            //获取当前用户角色权限
            $deparmentId            = self::getDepartmentId();
            $derectCompanyId        = self::getDirectCompanyId();
            $topId                  = self::getTopId();

          /*  //只有下级经销商时直属公司这里才不同。
            if($derectCompanyId != $topId && $derectCompanyId != 0 ){
                //$deparmentId =  $derectCompanyId;
            }
            echo $deparmentId;exit;*/
            ////新注册用户
            $roleIds = Yii::$app->user->getIdentity()->role_ids;
          /*  if($roleIds != 12){
                //$roleArray     =  Role::getRoseByDepartmentId($derectCompanyId);
                $roleArray     =  Role::getRoseByDepartmentId(self::getTopId());
                $roleIds       =  implode(array_column($roleArray,'id'),',');
            }*/
            $authRole          =  Role::getDataAuth($roleIds,'',0);
            $didList = [];
            //特殊权限处理开始：erg：仓储管理，即使给了“公司以及以下权限”，返回的只是“公司权限”
            //公司下的账号本部门以以下权限，自动被替换成本人权限
            $selfAuth = self::getDatAuthId();
             if($topId == $deparmentId && ($selfAuth == 4 ||$selfAuth == 5) ){
                 $specialAuth = $selfAuth;
                 $replaceAuth = 1;
             }
            if($specialAuth){
                if(in_array($specialAuth,$authRole)){
                    $key = array_search($specialAuth,$authRole);
                    $authRole[$key] = $replaceAuth;
                }
            }
            //特殊权限处理结束
            if($authRole){
                //权限合并，取并集
                $authRole = array_unique($authRole);
                if(in_array(3,$authRole)){
                    //本公司及以下
                   /* //只返回下级经销商id,不返回下级经销商的子部门
                    $deparmentId = $derectCompanyId;
                    $sonCompanyies = self:: getDepartmentAllSonIds($deparmentId,[1]);
                    //返回本公司下的所有部门
                    $didList       =  self::getDepartmentAllSonIds($deparmentId,[3]);
                    $didList       = array_merge_recursive($didList,$sonCompanyies);*/
                   $deparmentId= $derectCompanyId;
                    $didList       =  self::getDepartmentAllSonIds($deparmentId,[1,3]);
                }elseif(in_array(2,$authRole)){
                    //本公司
                    $deparmentId = $derectCompanyId;
                    $didList  = self::getDepartmentAllSonIds($deparmentId,[3],$derectCompanyId);
                }elseif(in_array(4,$authRole)){
                    //本部门
                    $didList = [$deparmentId];
                }elseif(in_array(5,$authRole)){
                    //本部门及以下
                    $didList = self::getDepartmentAllSonIds($deparmentId,[3]);

                }elseif(in_array(1,$authRole)){
                    //本人
                    $didList  = [$deparmentId];
                    $userId   = self::getLoginUserId();

                }
            }

        }
        $didList = array_unique($didList);

        if($didList){
            $auth = self::getListOptionAuth($didList);
           return ['dataAuth'=>$auth,'department_id'=>$didList,'create_user_id'=>$userId,'department_name'=>$department_name];
        }
        return  [];
    }


    /**
     * @desc   获取操作权限,0不可操作，1可操作
     * @return array
     * @author chengjuanjuan
     * @date   2018-06-07
     */
    public static function getListOptionAuth($departmentIds=[])
    {
        if(!$departmentIds){
            return [];
        }
        $selfId          = self::getDepartmentId();
        $directCompanyId = self::getDirectCompanyId();
        $topId           =  self::getTopId();
        $departmentList = Department::find()->where(['id'=>$departmentIds])->asArray()->all();
        //获取所有机构的的直属id，id=>name
        $directIds = array_column($departmentList,'direct_company_id','direct_company_id');
        $directCompany = Department::find()->where(['id'=>$directIds])->asArray()->all();
        $directList    = array_column($directCompany,'name','id');
        //直属机构获取结束
        foreach($departmentList as $v){
            $list = [
                'id'=>$v['id'],
                'name'=>$v['name'],
                'type'=>$v['type']
            ];
            //非本公司账号（下级经销商账号）
            if($v['direct_company_id']!=$directCompanyId){
                if($v['type']==3){
                    $list['name']   = $directList[$v['direct_company_id']];
                }
                $list['option'] = 0; // only read the data
           }else{
                $list['option'] = 1; // could read delete update add the data
            }
            $department[$v['id']] = $list;
        }
        return $department;
    }

    /**
     * @desc   获取最高dataAuth id
     * @return array
     * @author chengjuanjuan
     * @date   2018-06-07
     */
    public static function getTopDataAuth()
    {
        $roleidStr = Yii::$app->user->getIdentity()->role_ids;
        $roleIds = explode(',',$roleidStr);
        $roleList = Role::getList(['id'=>$roleIds],0);
        $role_ids_arr = array_column($roleList,'data_auth_id');
        //权限合并，取并集
        $authRole = array_unique($role_ids_arr);
        $maxAuth = '';
        if(in_array(3,$authRole)){
            //本公司及以下
            $maxAuth = 3;
        }elseif(in_array(2,$authRole)){
            //本公司
            $maxAuth = 2;
        }elseif(in_array(5,$authRole)){
            //本部门及以下
            $maxAuth = 5;
        }elseif(in_array(4,$authRole)){
            //本部门
            $maxAuth = 4;
        }elseif(in_array(1,$authRole)){
            //本人
            $maxAuth = 1;
        }
        return $maxAuth;
    }

    /**
     * 获取直属公司id
     * @return int
     * @author xi
     * @date 2018-6-12
     */
    public static function getDirectCompanyId()
    {
        return (new BaseModel())->directCompanyId;
    }


    /**
     * @desc   获取搜索数据
     * @return array
     * @author chengjuanjuan
     * @date   2018-06-07
     */
    public static function departmentforSearch(){
        //获取当前用户信息
        $user_info = BaseLogic::getLoginUserInfo();
        // 用户最高权限
        $dataAuthId = self::dataAuthId;
        //搜索部门结构 第一级
        if($dataAuthId == 3){  //本公司及以下
            $top_id = self::directCompanyId;
            $info = department::getDepartmentInfo($top_id,1,0,1);
            //查询是否还有下级
            $nextDepartment = Department::getDepartmentChildren($top_id,0,1);
            if(count($nextDepartment) > 1){
                $info[0]['exist'] = 1; //有下级
            }else{
                $info[0]['exist'] = 2; //无
            }
        }else if($dataAuthId == 2) {  //本公司
            $top_id = self::directCompanyId;
            $info = department::getDepartmentInfo($top_id, 1, 0, 1);
            $nextDepartment = Department::getDepartmentChildren($top_id, 1, 1);
            if (!empty($nextDepartment)) {
                $info[0]['exist'] = 1; //有下级
            } else {
                $info[0]['exist'] = 2; //无
            }
        }
    /*    }else if($dataAuthId == 5){   //其他权限
            $info = department::getDepartmentInfo($this->departmentId,1,0,1);
            $nextDepartment = Department::getDepartmentChildren($this->departmentId,1);
            if(!empty($nextDepartment)){
                $info[0]['exist'] = 1; //有下级
            }else{
                $info[0]['exist'] = 2; //无
            }
        }else if($dataAuthId == 4){   //本部门
            $info = department::getDepartmentInfo($this->departmentId,1,0,1);
            $info[0]['exist'] = 2; //无
        }else{
            $info = array();
        }*/
    }

    /**
     * 服务类型获取
     * @author chengjuanjuan <chengjuanjuan@c-ntek.com>
     * @date 2017-12-14
     */
    public static function getTypeList()
    {
        $data = ServiceType::getServiceType();
        return self::success($data);
    }

    /**
     * 获取数据权限
     * @return mixed
     * @author xi
     * @date 2018-6-25
     */
    public static function getDatAuthId()
    {
        $sessionKey = 'dataAuthId';
        if(Yii::$app->session->has($sessionKey)){
            return Yii::$app->session->get($sessionKey);
        }
        return 0;
    }


    /**
     * @desc 通过订单号和type类型获取到下单人id
     * @param $orderNo
     * @author chengjuanjuan
     * @date 2018-06-27
     * return $departmentId
     */
    public static function getOrderSourceId($orderNo,$type=3)
    {
        $orderList = WorkOrderAssign::findOne(['order_no'=>$orderNo,'type'=>$type]);
        return  isset($orderList->department_id)?$orderList->department_id:'';
    }
    /**
     * 获取当前机构类型
     * @return int
     * @author quan
     * @date 2018-7-10
     */
    public  static function getDepartmentType()
    {
        return (new BaseModel())->departmentObj->type;
    }
    /**
     * 根据id获取机构类型
     * @return int
     * @author quan
     * @date 2018-7-10
     */
    public  static function getDepartmentTypeById($id)
    {
        $info = Department::findOneByAttributes(['id'=>$id]);
        return  isset($info['type'])? $info['type'] : 0;
    }
}
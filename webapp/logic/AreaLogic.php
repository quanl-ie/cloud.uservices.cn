<?php
namespace webapp\logic;

use Yii;
use common\models\Region;
use common\models\ServiceArea;
use webapp\models\ManufactorArea;

class AreaLogic extends BaseLogic
{
    /**
     * 获取服务区域列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/20
     * Time: 16:41
     * @return array
     */
    public static function getIndex()
    {
    
        $dcId     = self::getDirectCompanyId();
        $topId    = self::getTopId();
        
        if ($dcId == $topId) {
            $flag = 1;
        }else{
            $flag = 2;
        }
        //获取列表数据
        $area = self::getSelfList(1);
        
        $data = [];
        if (!empty($area)) {
            $data = self::splitArea($area);
        }

        $provinceList = Region::getList(['parent_id'=>1,'region_type'=>1]);
    
        if (empty($provinceList)) {
            $data['provinceList'] = [];
        }else{
            $data['provinceList'] = $provinceList;
        }
        
        if (isset($flag)) {
            $data['flag'] = $flag;
        }

        return $data;
    }
    
    /**
     * 添加数据列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/20
     * Time: 16:41
     * @return array
     */
    public static function beforeAdd()
    {
        
        $provinceList = Region::getList(['parent_id'=>1,'region_type'=>1]);

        $area = self::getSelfList(1);
        
        $data = [];
        
        if (!empty($area)) {
            $data = self::splitArea($area,1);
        }
    
        $data['provinceList'] = $provinceList;
        
        return $data;

    }
    
    /**
     * 编辑添加数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/20
     * Time: 16:41
     * @return array
     */
    public static function add()
    {
        $topId    = self::getTopId();
    
        $districtIds = array_filter(Yii::$app->request->post('districtIds'));
        
        if (empty($districtIds)) {
            return self::error('请选择服务区域',20001);
        }
        
        $newArea = self::groupArea($districtIds);
    
        if (empty($newArea)) {
            return self::error('保存失败',20002);
        }
        
        //查询已存在的地区数据
        $oldArea = self::getSelfList();
        
        //判断已被删除的数据
        $delArea = array_diff($oldArea,$newArea);
        
        //判断新加数据
        $addArea = array_diff($newArea,$oldArea);
        
        //判断需要修改的数据
        $editArea = array_intersect($oldArea,$newArea);

        $data = [];
        $time = time();
        
        if (!empty($addArea))
        {
            foreach ($addArea as $key => $val) {
                $data['add'][$key]['department_id'] = $topId;
                $data['add'][$key]['area']          = $val;
                $data['add'][$key]['status']        = 1;
                $data['add'][$key]['del_status']    = 1;
                $data['add'][$key]['oprate_id']     = self::getLoginUserId();
                $data['add'][$key]['create_time']   = $time;
                $data['add'][$key]['update_time']   = $time;
        
            }
        }
        
        if (!empty($delArea))
        {
            foreach ($delArea as $key => $val) {
                
                $data['del'][$key]['id']          = $key;
                $data['del'][$key]['status']      = 2;
                $data['del'][$key]['del_status']  = 1;
                $data['del'][$key]['oprate_id']   = self::getLoginUserId();
                $data['del'][$key]['update_time'] = $time;
        
            }
        }
    
        if (!empty($editArea))
        {
            foreach ($editArea as $key => $val) {
            
                $data['edit'][$key]['id']          = $key;
                $data['edit'][$key]['status']      = 1;
                $data['edit'][$key]['del_status']  = 1;
                $data['edit'][$key]['oprate_id']   = self::getLoginUserId();
                $data['edit'][$key]['update_time'] = $time;
            
            }
        }
        
        $res = ServiceArea::add($data);
        
        if ($res) {
            return self::success($res,'保存成功');
        }else{
            return self::error('保存失败',20003);
        }
        
    }
    
    /**
     * 获取自身服务区域
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/20
     * Time: 16:41
     * @return array
     */
    public static function getSelfList($type = '')
    {
        $topId    = self::getTopId();
        $dcId     = self::getDirectCompanyId();
        
        $where = [
            ['=','a.direct_company_id',$dcId],
            ['=','b.department_id',$topId],
            ['=','b.del_status',1],
        ];
        
        if (!empty($type)) {
            $where[] = ['=','b.status',1];
        }
        
        $manArea = ManufactorArea::getList($where);
        
        if (empty($manArea)) {
            return [];
        }
        
        return array_column($manArea,'area','area_id');
        
    }
    
    /**
     * 拆分地区
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/20
     * Time: 16:41
     * @param $area
     * @param string $type
     * @return array
     */
    public static function splitArea($area,$type = '')
    {
        if (empty($area)) {
            return [];
        }
        
        foreach ($area as $key => $val) {
            $res[$key] = explode('_', $val);
        }
    
        foreach ($res as $key => $val) {
            $provinceIds[] = $val[0];
            $cityIds[]     = $val[1];
            $districtIds[] = $val[2];
        }
    
        $data['province'] = Region::getCityName(['region_id'=>array_unique($provinceIds)]);
    
        if (!empty($type)) {
            $data['province'] = array_unique($provinceIds);
        }
    
        $data['city']     = Region::getCityName(['region_id'=>array_unique($cityIds)]);
        $data['district'] = Region::getCityName(['region_id'=>array_unique($districtIds)]);
    
        return $data;
    }
    
    /**
     * 组合地区
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/20
     * Time: 16:41
     * @param $districtIds
     * @return array
     */
    public static function groupArea($districtIds)
    {
        $data = [];
        //根据县区id查询数据
        $cityData = Region::getList(['region_id'=>$districtIds]);
        $citys = [];
        //组合数据以区县id为键生成数组    抽取城市id
        foreach ($cityData as $key => $val) {
            $citys[] = $val['parent_id'];
            $data[$val['region_id']]['city'] = $val['parent_id'];
            $data[$val['region_id']]['district'] = $val['region_id'];
        }
        //去除重复
        $citys = array_unique($citys);
        //根据城市id查询数据
        $provinceData = Region::getList(['region_id'=>$citys]);
        $province = [];
        //以城市id为键省份为值提取数据
        foreach ($provinceData as $val) {
            $province[$val['region_id']] = $val['parent_id'];
        }
        //组合数据
        foreach ($data as $key => $val) {
            if (isset($province[$val['city']])) {
                $data[$val['district']]['province'] = $province[$val['city']];
            }
        }
        $arr = [];
        //重组拼接数据为一维数组
        foreach ($data as $key => $val) {
            $arr[$key] = $val['province'].'_'.$val['city'].'_'.$val['district'];
        }
        return $arr;
    }
    
}

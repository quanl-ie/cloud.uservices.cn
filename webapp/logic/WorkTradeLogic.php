<?php
/**
 * Created by PhpStorm.
 * User: xingq
 * Date: 2018/6/19
 * Time: 14:09
 */

namespace webapp\logic;

use webapp\models\Department;
use webapp\models\Technician;
use Yii;
use webapp\models\WorkOrderTrade;

class WorkTradeLogic extends BaseLogic
{
    /**
     * 搜索条件处理
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/19
     * Time: 15:40
     * @return array
     */
    public static function search()
    {
        
        $workNo       = trim(Yii::$app->request->get('work_no', ''));
        $tradePayCode = intval(Yii::$app->request->get('trade_pay_code', -1));
        $startTime    = trim(Yii::$app->request->get('start_time', ''));
        $endTime      = trim(Yii::$app->request->get('end_time', ''));
        
        $where = [];
        
        if ($workNo != '') {
            $where[] = ['=', 'b.work_no', $workNo];
        }
        
        if ($tradePayCode != -1) {
            $where[] = ['=', 'b.trade_pay_code', $tradePayCode];
        }
        else {
            $where[] = ['in', 'b.trade_pay_code', [0,1]];
        }
        
        if ($startTime != '' && $endTime != '')
        {
            $where[] = ['>', 'b.pay_time', $startTime];
            $where[] = ['<', 'b.pay_time', $endTime];
        }
        
        return $where;
    }
    
    
    /**
     * 列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/21
     * Time: 15:14
     * @return array
     */
    public static function index()
    {
        $listNull = [
            'page'       => 0,
            'totalCount' => 0,
            'totalPage'  => 0,
            'list'       => []
        ];
        
        //分页参数
        $page     = intval(Yii::$app->request->get('page',1));
        $pageSize = intval(Yii::$app->request->get('pageSize',10));
        
        $where    = self::search();
        
        //获取本公司所有部门id
        $department = Department::getDepartmentChildren(self::getDirectCompanyId(),0,1);
        $departIds  = array_unique(array_column($department,'id'));
    
        //获取本公司所有技师id
        $tech = Technician::find()->where(['store_id'=>$departIds,'audit_status'=>5])->asArray()->all();
        
        if (empty($tech))
        {
            return $listNull;
        }
    
        $techIds = array_column($tech,'id');
    
        $where[] = ['in','c.technician_id',$techIds];
        

        
        $res = WorkOrderTrade::getPageList($page, $pageSize, $where);
        
        if (!empty($res['list']))
        {
            foreach ($res['list'] as $key => $val)
            {
                if ($val['trade_pay_code'] == 1) {
                    $res['list'][$key]['pay_name'] = '支付宝';
                }else if ($val['trade_pay_code'] == 0) {
                    $res['list'][$key]['pay_name'] = '微信';
                }
                else {
                    $res['list'][$key]['pay_name'] = '线金';
                }
            }
        }
        
        return $res;
    }
}
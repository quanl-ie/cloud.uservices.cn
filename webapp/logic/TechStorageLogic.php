<?php
namespace webapp\logic;
use common\models\Product;
use Yii;
use webapp\models\BaseModel;
use common\helpers\Helper;
use webapp\models\Region;
use webapp\models\TechStorage;
use common\models\DictEnum;
use common\models\ServiceBrand;
use common\models\ServiceClass;
use webapp\models\Character;
/**
 * 技师备件库存
 * @author sxz
 * @date 2018-03-30
 */
class TechStorageLogic extends BaseLogic
{
    /**
     * 数据列表
     * @author sxz
     * @return string
     */
    public static function getList()
    {
        $tech_id = intval(Yii::$app->request->get('technician_id',0));
        $params = $where = [];
        $params['page'] = $page = intval(Yii::$app->request->get('page',1));
        $params['pageSize'] = $pageSize = intval(Yii::$app->request->get('pageSize',20));
        $params['technician_id'] = $tech_id;
        $where = [
            'direct_company_id' => self::getDirectCompanyId(),
            'status'        => 0,
            'tech_id'       => $tech_id
        ];
        $result = $newArr = $prod_character = $data = [];
        $data = TechStorage::getList($where);
        if($data){
            foreach ($data as $key => $info) {
                $result[$info['prod_id']][] = $info['num'];
            }
            foreach ($result as $k=>$v){
                $result[$k] = array_sum($v);
            }
            foreach ($result as $k1=>$v1){
                /*$newArr[$k1]['info'] = isset(self::getProductDetail($k1)['data'])?self::getProductDetail($k1)['data']:[];
                $newArr[$k1]['stock_num'] = $v1;*/
                $newArr[$k1] = isset(self::getProductDetail($k1)['data'])?self::getProductDetail($k1)['data']:[];
                $newArr[$k1]['stock_num'] = $v1;
                
            }
            foreach ($result as $key => $val) {
                if ($val['stock_num'] <= 0) {
                    unset($result[$key]);
                }
            }
            $twoArray = '1'; //twoArray 结果是否需要是二维数组排列   需要的话不用传，不需要传入值
            // 将备件数组按照字母重新排列
            $prod_character = (new Character)->groupByInitials($newArr, 'prod_name',$twoArray);
            $prod_character = array_values($prod_character);  //将数组键重新排列
            return [
                'page'       => 0,
                'totalCount' => 0,
                'totalPage'  => 0,
                'list'       => $prod_character,
                'params'     =>$params
            ];
        }else{
            return [
                'page'       => 0,
                'totalCount' => 0,
                'totalPage'  => 0,
                'list'       => $data,
                'params'     =>$params
            ];
        }
    }
    //获取产品详情
    public static function getProductDetail($id){
        if ($id == 0) {
            return self::error('获取产品信息失败',20001);
        }

        $info  = Product::getOne(['id'=>$id],true);
        if (!$info) {
            return self::error('获取产品信息失败',20001);
        }

        if (!empty($info['unit_id'])) {
            $unit = self::getUnit();
            if (isset($unit[$info['unit_id']])) {
                $info['unit_name'] = $unit[$info['unit_id']];
            }
        }
        //产品类目
        if(!empty($info['class_id'])){
            $className = ServiceClass::getTitle($info['class_id']);
            $info['class_name'] = $className;
        }
        if (!empty($info['brand_id'])) {
            $brandName = ServiceBrand::getName($info['brand_id']);
            $brandName = $brandName[$info['brand_id']];

            if (strstr($brandName, '/', true)) {
                $info['brand_name'] = strstr($brandName, '/', true);
            } else {
                $info['brand_name'] = $brandName;
            }
        }
        return self::success($info,'成功');
    }

    //更新技师备件库存
    public static function updateStorage($technicianId,$prodId,$num){
        $storage = TechStorage::getOne(['tech_id'=>$technicianId,'prod_id'=>$prodId]);
        if($storage){
            $id = $storage['id'];
            $lastNum = $storage['num']-$num;
            if($lastNum>=0){
                $res = TechStorage::edit(['id'=>$id,'num'=>$lastNum]);
            }else{
                return self::error('库存不足',20001);
            }
        }else{
            return self::error('该技师无备件库存',20001);
        }
}



}
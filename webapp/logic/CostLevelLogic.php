<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace webapp\logic;

use common\models\CostItemSet;
use webapp\models\CostLevelRelation;
use Yii;
use webapp\models\CostLevel;
use webapp\models\ServiceArea;
use webapp\logic\AreaLogic;
use webapp\models\BrandQualification;
use webapp\models\ServiceTypeQualification;
use webapp\models\ServiceClassQualification;
use common\models\ServiceClass;
use common\models\ServiceType;
use common\models\ServiceBrand;
use common\models\Region;
use webapp\models\CostItem;
use webapp\models\BaseModel;
use webapp\models\CostLevelItem;
use webapp\models\CostLevelDetail;
/**
 * Description of CostLevelLogic
 *
 * @author liuxingqi <lxq@c-ntek.com>
 */
class CostLevelLogic extends BaseLogic
{
    //put your code here
    /**
     * 列表
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function getList()
    {
        $page          = intval(Yii::$app->request->get('page',1));
        $pageSize      = intval(Yii::$app->request->get('pageSize',10));
        $departmentId  = self::getDepartmentId();
        $map = "department_id = ".$departmentId." and (status <> 3)";
        $topdepartmentId = self::getTopId();
        $where = [];
        $directCompanyId = self::getDirectCompanyId();
        if($directCompanyId == $topdepartmentId){
            $CostLevelList = CostLevelRelation::getList(['department_id'=>$topdepartmentId]);
            if($CostLevelList){
                $CostLevelIds = array_column($CostLevelList,'cost_level_id');
                $map = ['id'=>$CostLevelIds];
                $where[] = ['<>','status',3];
            }
        }
        if($topdepartmentId != $departmentId) {
            //不是顶级系统的时候去查询顶级账户给下级经销商分配的收费标准
            $CostLevelList = CostLevelRelation::getList(['department_id'=>$directCompanyId]);
            if($CostLevelList){
                $CostLevelIds = array_column($CostLevelList,'cost_level_id');
                $map = ['id'=>$CostLevelIds];
                $where[] = ['<>','status',3];
            }

        }
        $res = CostLevel::getIndex($map,$where,$page,$pageSize);
        return $res;
    }
    /**
     * 添加页面初始化数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function getAddList()
    {
        $id = Yii::$app->request->get('id',0);
        $departmentId = self::getDepartmentId();
        //获取初始地区数据
        $province   = Region::getList(['parent_id'=>1,'region_type'=>1]);
        $city = $district = [];
        if($id > 0){
            $city       = Region::getList(['region_type'=>2]);
            $district   = Region::getList(['region_type'=>3]);
        }
        $classData  = BaseLogic::getClass('0',1);
        $classData  = array_column($classData,'class_name','id');
        $brandData  = BaseLogic::getBrand('',1);
        $typeData  = BaseLogic::getType('',1);
        //收费项目
        $data['cost_item'] = [];
        $costItemData = CostItemSet::find()
                        ->where(['status'=>1,'del_status'=>1,'department_id'=> self::getDirectCompanyId()])
                        ->asArray()->all();
        $costItemData = array_column($costItemData,'name','id');
        //计量单位
        $unitList = Yii::$app->params['unit'];
        if ($unitList) {
            $data['unit'] = $unitList;
        }
        $data['class'] = $classData;
        $data['brand'] = $brandData;
        $data['type'] = $typeData;
        $data['province'] = $province;
        $data['city'] = $city;
        $data['district'] = $district;
        $data['cost_item'] = $costItemData;
        return $data;
    }
    /**
     * 数据添加
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function add()
    {
        $postData = Yii::$app->request->post();
        //接收传值
        $levelTitle  = trim(Yii::$app->request->post('levelTitle',''));
        $brandId     = trim(Yii::$app->request->post('brandId',''));
        $classId     = Yii::$app->request->post('classId',[]);
        $typeId      = trim(Yii::$app->request->post('typeId',''));
        $districtIds = Yii::$app->request->post('districtIds','');
        $chargeObj   = Yii::$app->request->post('chargeObj','');
        //判断传值
        if ($levelTitle == '') {
            return self::error('请输入收费标准名称!',20001);
        }
        if ($brandId == '') {
            return self::error('请选择品牌',20002);
        }
        if (empty($classId)) {
            return self::error('请选择产品类目',20003);
        }
        if ($typeId == '') {
            return self::error('请选择服务类型',20004);
        }
        if (empty($districtIds)) {
            return self::error('请选择服务城市',20005);
        }
        if (empty($chargeObj)) {
            return self::error('请添加收费项目',20006);
        }
        //组合数据
        $serviceArea = AreaLogic::groupArea($districtIds);
        if (!$serviceArea) {
            return self::error('服务城市数据错误',20005);
        }
        $serviceArea = implode(',',$serviceArea);
        $DepartmentId = BaseLogic::getDepartmentId();
        $TopId = BaseLogic::getTopId();
        //判断是否有通用
        if ($brandId == -1) {
            $brandId = 0;
        }
        if ($classId == -1) {
            $classId = 0;
        }
        if ($typeId == -1) {
            $typeId = 0;
        }
        //定义时间
        $time = time();
        //组成数组
        //1 组合收费标准主表数据
        $costLevelData = [];
        $costLevelData['department_id']     = $DepartmentId;
        $costLevelData['department_top_id'] = $TopId;
        $costLevelData['level_title']  = $levelTitle;
        //$costLevelData['service_area'] = $serviceArea;
        $costLevelData['create_time']  = $time;
        $costLevelData['update_time']  = $time;
        $costLevelData['status']       = 3;
        //2 组合收费标准明细表数据
        $costLevelDetailData = [];
        foreach ($classId as $K=>$V){
            $costLevelDetailData[$K]['brand_id']     = $brandId;
            $costLevelDetailData[$K]['class_id']     = $V;
            $costLevelDetailData[$K]['type_id']      = $typeId;
            $costLevelDetailData[$K]['service_area'] = $serviceArea;
            $costLevelDetailData[$K]['create_time']  = $time;
            $costLevelDetailData[$K]['update_time']  = $time;
            $costLevelDetailData[$K]['status']       = 3;
            $costLevelDetailData[$K]['department_id']     = $DepartmentId;
            $costLevelDetailData[$K]['department_top_id'] = $TopId;

        }
        //3 组合收费标准收费项目表数据
        $costLevelItemData = [];
        $costLevelItemData['cost_item_id'] = $chargeObj[0];
        $costLevelItemData['cost_item_name'] = $chargeObj[1];
        $costLevelItemData['unit'] = $chargeObj[2];
        $costLevelItemData['price'] = $chargeObj[3];
        $costLevelItemData['create_time'] = $time;
        $costLevelItemData['update_time'] = $time;
        $costLevelItemData['status'] = 3;
        $costLevelItemData['department_id'] = $DepartmentId;
        $costLevelItemData['department_top_id'] = $TopId;
        //判断是否删除数据
        $detailData = ['department_top_id'=>$TopId,'brand_id'=>$brandId,'class_id'=>$classId,'type_id'=>$typeId];
        $costLevelDetail = CostLevelDetail::getList($detailData);
        if (!empty($costLevelDetail))
        {
            $newServiceArea = explode(',',$serviceArea);
            $oldServiceArea = array_column($costLevelDetail,'service_area','id');
            foreach ($oldServiceArea as $key => $val){
                $oldServiceArea[$key] = explode(',',$val);
            }
            $detailId = [];
            foreach ($oldServiceArea as $Key => $val) {
                foreach ($newServiceArea as $k => $v) {
                    if (in_array($v,$val)) {
                        $detailId[] = $key;
                    }
                }
            }
            if (!empty($detailId)) {
                //查询收费项目是否重复
                $res = CostLevelItem::getList(['department_top_id'=>$TopId,'level_detail_id'=>$detailId,'cost_item_id'=>$chargeObj[0]]);
                if($res){
                    return self::error('有重复的收费标准添加，请查找重复信息并编辑后再次提交',20001);
                }
            }

        }


        // 调用添加方法
        $res = CostLevel::add($costLevelData,$costLevelDetailData,$costLevelItemData);
        // 判断数据组合查询并返回数据
        if ($res) {
            $data = self::CallBackData($res);
            return self::success($data,'提交成功');
        }
        return self::error('保存失败',20007);

    }
    /**
     * 编辑
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $id
     * @return type
     */
    public static function edit($id)
    {
        //接收传值
        $levelTitle  = trim(Yii::$app->request->post('levelTitle',''));
        $brandId     = trim(Yii::$app->request->post('brandId',''));
        $classId     = Yii::$app->request->post('classId',[]);
        $typeId      = trim(Yii::$app->request->post('typeId',''));
        $districtIds = Yii::$app->request->post('districtIds','');
        $chargeObj   = Yii::$app->request->post('chargeObj','');
        $DepartmentId = BaseLogic::getDepartmentId();
        $TopId = BaseLogic::getTopId();
        //判断传值
        if ($levelTitle == '') {
            return self::error('请输入收费标准名称!',20001);
        }
        if ($brandId == '') {
            return self::error('请选择品牌',20002);
        }
        if (empty($classId)) {
            return self::error('请选择产品类目',20003);
        }
        if ($typeId == '') {
            return self::error('请选择服务类型',20004);
        }
        if (empty($districtIds)) {
            return self::error('请选择服务城市',20005);
        }
        if (empty($chargeObj)) {
            return self::error('请添加收费项目',20006);
        }
        $serviceArea = AreaLogic::groupArea($districtIds);
        if (!$serviceArea) {
            return self::error('服务城市数据错误',20005);
        }
        $serviceArea = implode(',',$serviceArea);
        // 判断是否有通用
        if ($brandId == -1) {
            $brandId = 0;
        }
        if ($classId == -1) {
            $classId = 0;
        }
        if ($typeId == -1) {
            $typeId = 0;
        }

        $detailData = ['department_top_id'=>$TopId,'brand_id'=>$brandId,'class_id'=>$classId,'type_id'=>$typeId];
        $costLevelDetail = CostLevelDetail::getList($detailData);
        if (!empty($costLevelDetail))
        {
            $newServiceArea = explode(',',$serviceArea);
            $oldServiceArea = array_column($costLevelDetail,'service_area','id');
            foreach ($oldServiceArea as $key => $val){
                $oldServiceArea[$key] = explode(',',$val);
            }
            $detailId = [];
            foreach ($oldServiceArea as $Key => $val) {
                foreach ($newServiceArea as $k => $v) {
                    if (in_array($v,$val)) {
                        $detailId[] = $key;
                    }
                }
            }
            if (!empty($detailId)) {
                //查询收费项目是否重复
                $res = CostLevelItem::getList(['department_top_id'=>$TopId,'level_detail_id'=>$detailId,'cost_item_id'=>$chargeObj[0],'status'=>[1,3]]);
                if($res){
                    return self::error('有重复的收费标准添加，请查找重复信息并编辑后再次提交',20001);
                }
            }

        }
        //判断是否删除数据
        $level_detail_ids_del = [];
        $costLevelDetail = CostLevelDetail::getList(['cost_level_id'=>$id]);
        if (!empty($costLevelDetail))
        {
            $oldClassId = array_column($costLevelDetail,'class_id','class_id');
            foreach ($classId as $k=>$v){
                if(in_array($v,$oldClassId)){
                    $flag[] = 1;
                }else{
                    $flag[] = 2;
                }
            }
            if(in_array(2,$flag) && !in_array(1,$flag)){
                $level_detail_ids_del = array_column($costLevelDetail,'id');
            }else{
                //查询收费项目是否重复
                /*$detailData = ['department_top_id'=>$TopId,'cost_level_id'=>$id,'brand_id'=>$brandId,'type_id'=>$typeId,'service_area'=>$serviceArea];
                $classIdArr = $classId;
                $is_rep = CostLevelDetail::checkRep($detailData,$classIdArr,$chargeObj);
                if(!$is_rep){
                    return self::error('收费项目重复添加',20001);
                }*/
            }
        }

        // 定义时间函数
        $time = time();
        //组成数组
        //1 组合收费标准主表数据
        $costLevelData = [];
        $costLevelData['id']           = $id;
        $costLevelData['level_title']  = $levelTitle;
        $costLevelData['update_time']  = $time;
        //$costLevelData['status']       = 3;

        //2 组合收费标准明细表数据
        $costLevelDetailData = [];
        foreach ($classId as $K=>$v){
            $costLevelDetailData[$K]['cost_level_id'] = $id;
            $costLevelDetailData[$K]['brand_id']     = $brandId;
            $costLevelDetailData[$K]['class_id']     = $v;
            $costLevelDetailData[$K]['type_id']      = $typeId;
            $costLevelDetailData[$K]['service_area'] = $serviceArea;
            $costLevelDetailData[$K]['create_time']  = $time;
            $costLevelDetailData[$K]['update_time']  = $time;
            $costLevelDetailData[$K]['status']       = 3;
            $costLevelDetailData[$K]['department_id']     = $DepartmentId;
            $costLevelDetailData[$K]['department_top_id'] = $TopId;

            $detail = CostLevelDetail::find()->where(['cost_level_id'=>$id,'brand_id'=>$brandId,'class_id'=>$v,'type_id'=>$typeId])->andWhere("find_in_set('$serviceArea','service_area')")->asArray()->one();
            if (!empty($detail)) {
                $costLevelDetailData[$K]['cost_level_id'] = $detail['id'];
            }
        }
        //3 组合收费标准收费项目表数据
        $costLevelItemData = [];
        if (!isset($chargeObj) && !isset($chargeObj[0])) {
            return self::error('请重新选择收费项目',20001);
        }

        $costLevelItemData['cost_level_id']  = $id;
        $costLevelItemData['cost_item_id']   = $chargeObj[0];
        $costLevelItemData['cost_item_name'] = $chargeObj[1];
        $costLevelItemData['unit']           = $chargeObj[2];
        $costLevelItemData['price']          = $chargeObj[3];
        $costLevelItemData['create_time']    = $time;
        $costLevelItemData['update_time']    = $time;
        $costLevelItemData['status']         = 3;
        $costLevelItemData['department_id']     = $DepartmentId;
        $costLevelItemData['department_top_id'] = $TopId;
        // 调用修改方法
        $res = CostLevel::edit($costLevelData,$costLevelDetailData,$costLevelItemData,$level_detail_ids_del);
        // 判断条件是否存在 存在查询数据并返回
        if ($res) {
            $data = self::CallBackData($res);
            //print_r($data);die;
            return self::success($data,'保存成功');
        }
        return self::error('保存失败',20007);
        
    }
    /**
     * 根据收费项目id修改收费项目为禁用状态
     * @param type $id
     * @return type
     */
    public static function del($idArr)
    {
        $res = CostLevel::del($idArr);
        if (!$res) {
            return self::error('删除失败',20001);
        }else{
            return self::error('删除成功','200');
        }
    }

    /**
     * 编辑页面数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function getEditList()
    {
        $id = Yii::$app->request->get('id',0);
        $serviceArea = [];
        $classIdsName = $classIds = '';
        if ($id == 0) {
            return self::error('数据错误');
        }
        $costLevel = CostLevel::getOne(['id'=>$id]);
        if (!$costLevel) {
            return self::error('暂无数据');
        }
        //查询已有数据
        $cost_level_detail = CostLevelDetail::getList(['cost_level_id'=>$costLevel['id'],'status'=>1]);
        $cost_level_item   = CostLevelItem::getList(['cost_level_id'=>$costLevel['id'],'status'=>1]);
        $cost_level_item_ids = implode(',',array_column($cost_level_item,'id'));
        if($cost_level_detail){
            //组织已添加的区域
            $area = explode(',',$cost_level_detail[0]['service_area']);
            $serviceArea = self::getServiceArea($area);
            //组织已添加的分类
            $classIdsArr = array_unique(array_column($cost_level_detail,'class_id'));
            $classIds = implode(',',$classIdsArr);
            $classIdsName = implode(',',ServiceClass::getName($classIdsArr));
        }
        $data['provinceId']     = $serviceArea['provinceId'];
        $data['cityId']         = $serviceArea['cityId'];
        $data['districtId']     = $serviceArea['districtId'];
        $data['level']          = $costLevel;
        $data['class_ids_name'] = $classIdsName;
        $data['class_ids']      = $classIds;
        $data['cost_level_item_ids'] = $cost_level_item_ids;
        $data['cost_level_id'] = $id;
        return self::success($data,'成功');
    }
    /**
     * 获取品牌
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function getBrands()
    {
        $brandQualification = BrandQualification::getSelfList();
        $brandIds = [];
        if ($brandQualification) {
            foreach ($brandQualification as $val) {
                $brandIds[] = $val['brand_id'];
            }
        }
        $brandId = implode(',', $brandIds);
        $brandId = explode(',',$brandId);
        array_unique(array_filter($brandId));
        return ServiceBrand::find()->where(['id'=>$brandId])->asArray()->all();
    }
    /**
     * 获取类目
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function getClasss()
    {
        //产品类目
        $classQualification = ServiceClassQualification::getList();
        $classIds = [];
        if ($classQualification) {
            foreach ($classQualification as $val) {
                $classIds[] =  $val['class_id'];
            }
        }
        $classId = implode(',', $classIds);
        $classId = explode(',',$classId);
        array_unique(array_filter($classId));
        return ServiceClass::find()->where(['id'=>$classId])->asArray()->all();
    }
    /**
     * 获取服务类型
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function getTypes()
    {
        $typeQualification = ServiceTypeQualification::getList();
        $typeIds = [];
        if ($typeQualification) {
            foreach ($typeQualification as $val) {
                $typeIds[] =  $val['type_id'];
            }
        }
        $typeId = implode(',', $typeIds);
        $typeId = explode(',',$typeId);
        array_unique(array_filter($typeId));
        return ServiceType::find()->where(['id'=>$typeId])->asArray()->all();
    }

    /**
     * 拆分地区数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $area
     * @return type
     */
    public static function getServiceArea($area)
    {
        $res = [];
        foreach ($area as $key => $val) {
            $res[$key] = explode('_', $val);
        }
        foreach ($res as $key => $val) {
            $provinceIds[] = $val[0];
            $cityIds[]     = $val[1];
            $districtIds[] = $val[2];
        }
        $data['provinceId'] = array_unique($provinceIds);
        $data['cityId']     = array_unique($cityIds);
        $data['districtId'] = array_unique($districtIds);
        return $data;
    }
    /**
     * 根据省份id获取市级和县级数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $provinceIds
     * @return type
     */
    public static function getCity($provinceIds)
    {
        $provinceIds = implode("','",$provinceIds);
        $city = Region::getCostAreaList($provinceIds);
        $data['city'] = $city;
        $cityIds = implode("','",array_column($city,'region_id'));
        $district = Region::getCostAreaList($cityIds);
        $data['district'] = $district;
        return $data;
    }
    /**
     * 根据市级数据获取县级数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $citysIds
     * @return type
     */
    public static function getDistrict($citysIds)
    {
        $citysIds = implode("','",$citysIds);
        $district = Region::getCostAreaList($citysIds);
        $data['district'] = $district;
        return $data;
    }

    /**
     * 根据收费详情id获取所有非禁用收费项目
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $id
     * @return type
     */
    public static function CallBackData($id)
    {
        $detial = CostLevelDetail::findOne(['id'=>$id]);
        $where = [
                    'cost_level_id'=>$detial['cost_level_id'],
                    'brand_id'=>$detial['brand_id'],
                    'type_id'=>$detial['type_id'],
                    'service_area'=>$detial['service_area']
                ];
        $detials = CostLevelDetail::getList($where);
        $detials_ids = array_column($detials,'id');
        //print_r($detials);die;
        $data = CostLevelItem::getList(['level_detail_id'=>$detials_ids]);
        foreach ($data as $key => $val) {
            if ($val['status'] == 2) {
                unset($data[$key]);
            }
        }
        $res = self::remove_duplicate($data,'cost_item_id');
        return $res;
    }
    /**
     * ajax品牌、产品类目、服务类型联动获取收费项目
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function linkAge()
    {
        $postData = Yii::$app->request->post();
        //接收传值
        $levelId = intval(trim(Yii::$app->request->post('levelId','')));
        $brandId = intval(trim(Yii::$app->request->post('brandId','')));
        $classId = Yii::$app->request->post('classId',[]);
        $typeId  = intval(trim(Yii::$app->request->post('typeId','')));
        //判断传值
        //print_r($levelId);die;
        if ($levelId == '') {
            return self::error('无收费标准标识',20001);
        }
        if ($brandId == '') {
            return self::error('请选择品牌',20002);
        }
        if (empty($classId)) {
            return self::error('请选择产品类目',20003);
        }
        if ($typeId == '') {
            return self::error('请选择服务类型',20004);
        }
        //判断是否有通用
        if ($brandId == -1) {
            $brandId = 0;
        }
        if ($classId == -1) {
            $classId = 0;
        }
        if ($typeId == -1) {
            $typeId = 0;
        }
        //查询数据
        $costLevelDetail = CostLevelDetail::getList(['cost_level_id'=>$levelId,'brand_id'=>$brandId,'class_id'=>$classId,'type_id'=>$typeId]);
        if($costLevelDetail){
            $costLevelDetailIds = array_column($costLevelDetail,'id');
            $data = CostLevelItem::getList(['level_detail_id'=>$costLevelDetailIds]);
            foreach ($data as $key => $val) {
                if ($val['status'] == 2) {
                    unset($data[$key]);
                }
            }
            $res = self::remove_duplicate($data,'cost_item_id');
            if (!empty($res)) {
                return self::success($res,'成功');
            }
        }
        return self::error('失败');
    }
    /**
     * 保存收费标准
     */
    public static function submit()
    {
        $postData = Yii::$app->request->post();
        //print_r($postData);die;
        $id = intval(trim(Yii::$app->request->post('levelId',0)));
        $levelTitle  = trim(Yii::$app->request->post('levelTitle',''));
        $districtIds = array_filter(Yii::$app->request->post('districtIds',''));

        $brandId     = trim(Yii::$app->request->post('brandId',''));
        $classId     = Yii::$app->request->post('classId',[]);
        $typeId      = trim(Yii::$app->request->post('typeId',''));
        $chargeObj   = Yii::$app->request->post('chargeObj','');
        
        //判断传值
        if ($id == 0) {
            return self::error('请添加收费项目及对应的收费标准!',20001);
        }
        if ($levelTitle == '') {
            return self::error('请输入收费标准名称!',20001);
        }
        if (empty($districtIds)) {
            return self::error('请选择服务城市',20002);
        }
        // 判断是否有通用
        if ($brandId == -1) {
            $brandId = 0;
        }
        if ($classId == -1) {
            $classId = 0;
        }
        if ($typeId == -1) {
            $typeId = 0;
        }
        //判断是否存在可用收费项目
        $res = CostLevelItem::getList(['cost_level_id'=>$id,'status'=>[1,3]]);

        if (empty($res)) {
            return self::error('请添加收费项目及对应的收费标准');
        }
        //组合数据
        $serviceArea = AreaLogic::groupArea($districtIds);
        if (!$serviceArea) {
            return self::error('服务城市数据错误',20002);
        }
        $serviceArea  = implode(',',$serviceArea);

        //查询提交的数据是否存在其他收费标准中
        /*$DepartmentId = BaseLogic::getDepartmentId();
        $TopId = BaseLogic::getTopId();
        //查询收费项目是否重复
        $detailData = ['department_id'=>$DepartmentId,'brand_id'=>$brandId,'type_id'=>$typeId,'service_area'=>$serviceArea];
        $classIdArr = $classId;
        $is_rep = CostLevelDetail::checkRep($detailData,$classIdArr,$chargeObj);
        if(!$is_rep){
            return self::error('有重复的收费标准添加，请查找重复信息并编辑后再次提交',20001);
        }*/

        $res = CostLevel::submit($id,$levelTitle,$serviceArea);
        if ($res) {
            return self::success('','提交成功');
        }
        return self::error('提交失败');

    }
    /**
     * 收费标准状态设置
     * @param type $id
     * @return type
     */
    public static function setStatus($id,$t,$status)
    {
        $where = ['id' => $id,'status' => $t];
        $model = CostLevel::findOne($where);
        if (!empty($model))
        {
            $model->status = $status;
            $model->update_time = time();
            if ($model->save(false)) {
                return self::success('','设置成功');
            }
            return self::error('设置失败',20001);

        }
        return self::error('设置失败',20001);
    }
    /**
     * 查看收费标准详情
     * @author sxz
     * @param type $id
     * @return type
     */
    public static function getCostDetail()
    {

        $id = intval(Yii::$app->request->get('id',0));
        if ($id == 0) {
            return self::error('数据错误');
        }
        //$costRuleRelation = CostRuleRelation::getOne($where);
        $departmentId = BaseLogic::getDepartmentId();
        $directTopId = BaseLogic::getTopId();
        if($departmentId != $directTopId){
            $departmentId = $directTopId;
        }
        $where = "id = $id "."and department_id = ".$departmentId." and (status <> 3)";
        $costLevel = CostLevel::getOne($where);
        if (!$costLevel) {
            return self::error('暂无数据');
        }
        if ($costLevel) {
            $data['id'] = $costLevel->id;
            $data['name'] = $costLevel->level_title;

        }
        return self::success($data);
    }
    /**
     * ajax品牌、产品类目、服务类型联动获收费标准
     * @author sxz
     * @return type
     */
    public static function ruleLinkAge()
    {
        $id = intval(Yii::$app->request->get('id',0));
        if ($id == 0) {
            return self::error('获取收费标准失败1',20001);
        }
        if ($id == -1) {
            return self::error('获取收费标准失败2',20002);
        }

        $costLevel = CostLevel::getOne(['id'=>$id,'status'=>[1,2]]);
        if (empty($costLevel)) {
            return self::error('暂无数据',20003);
        }
        $costLevelDetailData = CostLevelDetail::getList(['cost_level_id'=>$costLevel->id]);
        if (!$costLevelDetailData) {
            return self::error('获取收费标准数据失败',20004);
        }

        //获取地区
        $serviceArea = array_column($costLevelDetailData,'service_area');
        //处理地区
        $area = array_unique(explode(',',implode(',',$serviceArea)));
        if (empty($area)) {
            $data['province'] = $data['city'] = $data['district'] = [];
        }

        foreach ($area as $key => $val) {
            $res[$key] = explode('_', $val);
        }
        $provinceIds = [];
        $cityIds     = [];
        $districtIds = [];

        foreach ($res as $key => $val) {
            $provinceIds[] = $val[0];
            $cityIds[]     = $val[1];
            $districtIds[] = $val[2];
        }

        $data['province']   = Region::getCityName(['region_id'=>array_unique($provinceIds)]);
        $data['city']       = Region::getCityName(['region_id'=>array_unique($cityIds)]);
        $data['district']   = Region::getCityName(['region_id'=>array_unique($districtIds)]);
        //获取品牌名称
        $brandData = array_column($costLevelDetailData,'brand_id');
        $brandName = ServiceBrand::getName($brandData);
        //获取类目名称
        $classData = array_column($costLevelDetailData,'class_id');
        $class     = ServiceClass::getList(['id' => $classData]);
        $classPids = array_column($class, 'parent_ids', 'id');

        foreach ($classPids as $key => $val) {
            if (empty($val)) {
                $classPids[$key] = $key;
            }else{
                $classPids[$key] = $val.','.$key;
            }
        }

        $id = array_unique(explode(',', implode(',', $classPids)));

        $classNameData = ServiceClass::getName($id);

        foreach ($classPids as $key => $val) {
            if (!empty($val)) {
                $classPids[$key] = explode(',', $val);
            }

        }

        foreach ($classPids as $key => $val) {
            if (is_array($val)) {
                foreach ($val as $k => $v) {
                    if (isset($classNameData[$v])) {
                        $claName[$key][$v] = $classNameData[$v];
                    }
                }
            }
        }

        foreach ($claName as $key => $val) {
            if (!empty($val)) {
                $className[$key] = implode('-', $val);
            }
        }

        //获取类型名称
        $typeData = array_column($costLevelDetailData,'type_id');
        $typeName = ServiceType::getName($typeData);
        //print_r($typeName);die;
        //组合名称
        $costLevelDetail = [];
        foreach ($costLevelDetailData as $val) {
            $costLevelDetail[$val['id']]['id']   = $val['id'];
            if (isset($brandName[$val['brand_id']])) {
                $costLevelDetail[$val['id']]['brand_id']   = $val['brand_id'];
                $costLevelDetail[$val['id']]['brand_name'] = $brandName[$val['brand_id']];
            }else{
                $costLevelDetail[$val['id']]['brand_id']   = 0;
                $costLevelDetail[$val['id']]['brand_name'] = '通用';
            }
            if (isset($className[$val['class_id']])) {
                $costLevelDetail[$val['id']]['class_id']   = $val['class_id'];
                $costLevelDetail[$val['id']]['class_name'] = $className[$val['class_id']];
            }
            if (isset($typeName[$val['type_id']])) {
                $costLevelDetail[$val['id']]['type_id']   = $val['type_id'];
                $costLevelDetail[$val['id']]['type_name'] = $typeName[$val['type_id']];
            }else{
                $costLevelDetail[$val['id']]['type_id']   = 0;
                $costLevelDetail[$val['id']]['type_name'] = '通用';
            }
        }
        //print_r($costLevelDetail);die;
        //收费项目明细
        $costLevelItemData = CostLevelItem::getList(['cost_level_id'=>$costLevel->id,'status'=>1]);

        $costLevelItem = [];
        foreach ($costLevelItemData as $key => $val) {
            //1.拼接
            $costLevelItem[$val['level_detail_id']][] = $val['cost_item_name'].'：'.$val['price'].'元/'.$val['unit'];
        }

        //组合数组
        $cost = [];
        foreach ($costLevelDetail as $key => $val) {
            if ($val['brand_id'] == $val['brand_id']) {
                if (strstr($val['brand_name'],'/',true)) {
                    $cost[$val['brand_id']]['brand_name'] = strstr($val['brand_name'],'/',true);
                }else{
                    $cost[$val['brand_id']]['brand_name'] = $val['brand_name'];
                }
            }
        }


        foreach ($costLevelDetail as $key => $val) {
            foreach ($cost as $k => $v) {
                if ($val['brand_id'] == $k) {
                    if(isset($val['class_id'])){
                        $cost[$val['brand_id']]['classList'][$val['class_id']]['class_name'] = $val['class_name'];
                    }
                }
            }
        }
        foreach ($costLevelDetail as $key => $val) {
            foreach ($cost as $k => $v) {
                foreach ($v['classList'] as $i => $item) {

                    if (isset($val['class_id']) && $val['brand_id'] == $k && $val['class_id'] == $i) {
                        $cost[$val['brand_id']]['classList'][$val['class_id']]['typeList'][$val['type_id']]['type_name'] = $val['type_name'];
                        foreach ($costLevelItem as $c => $co) {
                            if ($val['id'] == $c) {
                                $cost[$val['brand_id']]['classList'][$val['class_id']]['typeList'][$val['type_id']]['itemList'][] = $co;
                            }
                        }

                    }
                }
            }
        }

        $data['rule'] = $cost;
        return self::success($data);
    }
    //二维数组去重
    public static function remove_duplicate($array,$keyid){
        //取出某项相同的id
        $main = [];
        foreach ($array as $val){
            $main[$val[$keyid]][] = $val['id'];
        }
        $newMain = [];
        foreach ($main as $K=>$v){
            $newMain[$K] = implode(',',$v);
        }
        $result = [];
        foreach ($array as $key => $value) {
            $has = false;
            foreach ($result as $val){
                if($val[$keyid] == $value[$keyid]){
                    $has = true;
                    break;
                }
            }
            if(!$has){
                $result[]=$value;
            }
        }
        if($result && $newMain){
            foreach ($result as $key=>$val){
                foreach ($newMain as $k1=>$v1){
                    if($val['cost_item_id'] == $k1){
                        $result[$key]['ids'] = $v1;
                    }
                }
            }
        }
        return $result;
        /*另外一种二维数组去重方法
         * $tmp_key[] = [];
        foreach ($array as $key => $item) {
            if ( is_array($item) && isset($item[$keyid]) ) {
                if ( in_array($item[$keyid], $tmp_key) ) {
                    unset($array[$key]);
                } else {
                    $tmp_key[] = $item[$keyid];
                }
            }
        }*/
    }
    public static function ResetItem()
    {
        $ids = Yii::$app->request->post('ids','');
        $id = Yii::$app->request->post('level_id','');
        //print_r(explode(',',$ids));
        if($ids){
            //查询cost_level下所有的items
            $cost_level_item   = CostLevelItem::getList(['cost_level_id'=>$id,'status'=>[1,3]]);
            $cost_level_item_ids = array_column($cost_level_item,'id');
            $idsArr = explode(',',$ids);
            $delIds = array_diff($cost_level_item_ids,$idsArr);
            //执行数据重置操作
            if($ids){
                $res = CostLevelItem::updateAll(['status'=>1,'update_time'=>time()],['id'=>$idsArr]);
            }
            if($delIds){
                $res = CostLevelItem::updateAll(['status'=>2,'update_time'=>time()],['id'=>$delIds]);
            }
        }

    }

}

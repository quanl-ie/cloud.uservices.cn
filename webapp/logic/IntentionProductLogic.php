<?php
/**
 * Created by PhpStorm.
 * User: xingq
 * Date: 2018/7/24
 * Time: 11:00
 */

namespace webapp\logic;

use webapp\models\SaleOrder;
use webapp\models\SaleOrderView;
use Yii;

class IntentionProductLogic extends BaseLogic
{
    /**
     * 添加客户意向产品
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/24
     * Time: 15:17
     * @return array
     */
    public static function add()
    {
        $accountId = intval(Yii::$app->request->post('account_id',0));
        $prodId    = intval(Yii::$app->request->post('prod_id',0));
        $brandId   = intval(Yii::$app->request->post('brand_id',0));
        $classId   = intval(Yii::$app->request->post('class_id',0));
        $info      = trim(Yii::$app->request->post('info',''));

        if ($accountId == 0) {
            return self::error('添加失败',20001);
        }
        if ($prodId == 0) {
            return self::error('添加失败',20002);
        }
        if ($brandId == 0) {
            return self::error('添加失败',20003);
        }
        if ($classId == 0) {
            return self::error('添加失败',20004);
        }
        
        $time = time();
        
        $data['department_id']     = self::getDepartmentId();
        $data['department_top_id'] = self::getTopId();
        $data['prod_id']           = $prodId;
        $data['account_id']        = $accountId;
        $data['status']            = 1;
        $data['brand_id']          = $brandId;
        $data['class_id']          = $classId;
        $data['info']              = $info;
        $data['intention_type']    = 2;
        $data['create_time']       = $time;
        $data['update_time']       = $time;
        
        $res = SaleOrder::addIntentionProduct($data);
        
        if ($res) {
            return self::success(['id'=>$res],'添加成功');
        }
        return self::error('添加失败',20005);
        
    }
    
    /**
     * 获取需要修改客户意向产品的数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/24
     * Time: 14:39
     * @return array
     */
    public static function beforeEdit()
    {
        $id        = intval(Yii::$app->request->get('id',0));
        $accountId = intval(Yii::$app->request->get('account_id',0));
        
        if ($id == 0) {
            return self::error('修改失败',20001);
        }
        if ($accountId == 0) {
            return self::error('修改失败',20002);
        }
        
        $res = SaleOrderView::find()->where(['id'=>$id,'account_id'=>$accountId,'intention_type'=>2])->asArray()->one();
        
        if (empty($res)) {
            return self::error('修改失败',20003);
        }
        
        return self::success($res);
    }

    /**
     * 修改客户意向产品
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/24
     * Time: 14:39
     * @return array
     */
    public static function edit()
    {
        $id   = intval(Yii::$app->request->post('id',0));
        $info = trim(Yii::$app->request->post('info',''));

        if ($id == 0) {
            return self::error('修改失败',20001);
        }
        
        $data['id'] = $id;
        if ($info != '') {
            $data['info'] = $info;
        }
        
        $data['update_time'] = time();
        
        $res = SaleOrder::editIntentionProduct($data);
        
        if ($res) {
            return self::success('','修改成功');
        }
        return self::error('修改失败',20002);
    }

    /**
     * 删除意向产品
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/7/24
     * Time: 14:50
     * @return array
     */
    public static function del()
    {
        $id   = intval(Yii::$app->request->post('id',0));
        
        if ($id == 0) {
            return self::error('删除失败',20001);
        }
        
        $data['id']          = $id;
        $data['status']      = 2;
        $data['update_time'] = time();

        $res = SaleOrder::editIntentionProduct($data);

        if ($res) {
            return self::success('','删除成功');
        }
        return self::error('删除失败',20002);
        
    }
}
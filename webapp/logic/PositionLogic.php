<?php
namespace webapp\logic;
use yii;
use webapp\models\BaseModel;
use common\helpers\Helper;
use webapp\models\SaleOrder;
use webapp\models\AccountAddress;
use webapp\models\Region;

class PositionLogic extends BaseLogic
{
    
    //订单
    public static function order()
    {
        $serviceTimeType = ['1'=>'立即上门','2'=>'预约上门'];
        $orderNo = yii::$app->request->get('order_no','');
        
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/detail";
        $postData = [
            'order_no' => $orderNo,
            'src_type' => BaseModel::SRC_FWS,
            'src_id'   => self::getManufactorId()
        ];
        
        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        
        if(isset($result['success']) && $result['success'] == 1 ){
            $detail = [];
            $data = $result['data'];
            if($data)
            {
                //服务产品 (服务内容)
                $serviceContent = '';
                $productIds = array_column($data['workDetail'], 'sale_order_id');
                $product = SaleOrder::getOne(['id'=>$productIds]);
                if ($product) {
                    $serviceContent = $product->brand_name.'-'.$product->class_name.'-'.$product->type_name;
                }
                //联系地址
                $addressDesc = '';
                $addressArr = AccountAddress::getOne(['id'=>$data['address_id']]);
                if ($addressArr) {
                    $addressDesc = Region::getCityName($addressArr->province_id).Region::getCityName($addressArr->city_id).Region::getCityName($addressArr->district_id);
                }
                $addressDesc = $addressDesc.$addressArr->address;
                
                if ($data['service_time_type'] == 1) {
                    $detail['service_time_type'] = '立即上门';
                }elseif ($data['service_time_type'] == 2) {
                    $detail['service_time_type'] = '预约上门';
                }
                $detail['order_no']        = $data['order_no'];
                $detail['service_content'] = $serviceContent;
                $detail['address']         = $addressDesc;
                $detail['plan_time']       = date('Y-m-d H:i:s',$data['plan_time']);
                $detail['lon']             = $addressArr->lon;
                $detail['lat']             = $addressArr->lat;
                return $detail;
            }
        } 
    }

}
<?php
    namespace webapp\logic;

    use common\models\Contract;
    use common\models\ContractDetail;
    use common\models\DepotProdStock;
    use common\models\ExchangeGoods;
    use common\models\ExchangeGoodsDetail;
    use common\models\PickUser;
    use common\models\Purchase;
    use common\models\PurchaseDetail;
    use common\models\ReturnGoods;
    use common\models\ReturnGoodsDetail;
    use common\models\ReturnPurchase;
    use common\models\ReturnPurchaseDetail;
    use common\models\StockDetail;
    use webapp\models\Account;
    use webapp\models\AccountAddress;
    use webapp\models\Apll;
    use webapp\models\ApllDetail;
    use webapp\models\PurchaseDetailView;
    use webapp\models\Region;
    use Yii;
    use common\models\SendOut;
    use webapp\models\StockDetailView;
    use common\models\DictEnum;
    use common\models\StockInfo;
    use common\helpers\CreateNoRule;
    use common\models\User;

    class SendOutLogic extends BaseLogic
    {
        /**
         * 搜索数据处理
         * User: quanl
         * Date: 2018/9/4
         * Time: 13:20
         * @return array
         */
        public static function search($dataArr)
        {
            $where = [];
            if (isset($dataArr['send_no']) && $dataArr['send_no'] != '') {
                $where[] = ['=','send_no',$dataArr['send_no']];
            }
            if (isset($dataArr['send_type']) && $dataArr['send_type'] != '') {
                $where[] = ['=','send_type',$dataArr['send_type']];
            }
            if (isset($dataArr['send_method']) && $dataArr['send_method'] != '') {
                $where[] = ['=','send_method',$dataArr['send_method']];
            }
            if (isset($dataArr['start_create_time']) && $dataArr['start_create_time'] != '') {
                $where[] = ['>','create_time',$dataArr['start_create_time']." 00:00:00"];
            }
            if (isset($dataArr['end_create_time']) && $dataArr['end_create_time'] != '') {
                $where[] = ['<','create_time',$dataArr['end_create_time']." 23:59:59"];
            }
            return $where;
        }
        
        /**
         * 列表数据
         * User: quanl
         * Date: 2018/9/4
         * Time: 13:24
         * @param string $type
         * @param int $status
         * @return array
         */
        public static function index()
        {
            $page     = intval(Yii::$app->request->post('page',1));
            $pageSize = intval(Yii::$app->request->post('pageSize',10));
            $status   = intval(Yii::$app->request->post('status',1));
            $source   = intval(Yii::$app->request->post('source',0));
            $search   = Yii::$app->request->post('postData',1);
            $map = [
                'direct_company_id'=>BaseLogic::getDirectCompanyId(),
                'send_status'=>$status,
                'send_source'=>$source
            ];
            $where = self::search($search);
            $data = SendOut::index($map,$where,$page,$pageSize);
            return self::success($data);
        }
        
        /**
         * 添加页面所需数据
         * User: quanl
         * Date: 2018/9/4
         * Time: 13:53
         * @param int $type
         * @return array
         */
        public static function beforeAdd()
        {
            $stockId  = intval(Yii::$app->request->post('stock_id',0));   //出库单id
            $source  = intval(Yii::$app->request->post('source',0));   //来源 出库 0  采购 1
            if($source == 1){
                //获取采购单信息
                if($stockId){
                    $purchase  = Purchase::findOneByAttributes(['id'=>$stockId],'subject,no');
                    $data['send_source']   = 1;
                    $data['send_no']   = CreateNoRule::getNo('SH');
                    $data['stock_id']  = $stockId;
                    $data['stock_no']  = $purchase['no'];
                    $data['subject']   = $purchase['subject'];
                    //提货人信息
                    $data['pick_list'] = PickUser::getList(['direct_company_id'=>self::getDirectCompanyId(),'status'=>1]);
                    $data['pick_default'] = 0;
                    //获取申请人姓名
                    $user =User::findOne(['id'=>self::getLoginUserId()]);
                    $data['apply_id'] = self::getLoginUserId();
                    $data['apply_name'] = $user->username;
                    $data['send_type']      = 8;
                    $data['send_type_name'] = '采购收货';
                    //发货方式
                    $data['send_method'] = DictEnum::findAllByAttributes(['dict_key'=>'send_method_type'],'dict_enum_id,dict_enum_value');
                    $data['default_send_method'] = "3";
                    $data['account'] = [];
                    $data['account_address'] = [];
                    //获取出库单产品信息
                    $data['product']     =  PurchaseDetailView::findAllByAttributes(['stock_id'=>$stockId],"prod_id,prod_name,stock_no,prod_no,brand_name,model,dict_enum_value,prod_num");
                    //获取开通省份
                    $data['province']    = Region::getOpenProvince();
                    return self::success($data);
                }
                return self::error('缺少采购单号');
            }
            if ($stockId)
            {
                //获取出库单信息
                $stockInfo         = StockInfo::findOneByAttributes(['id'=>$stockId],'no,stock_type,rel_id,rel_id,rel_type,subject,pick_user_id');
                $data['send_source']   = 0;
                $data['send_no']   = CreateNoRule::getNo('FH');
                $data['stock_id']  = $stockId;
                $data['stock_no']  = $stockInfo['no'];
                $data['subject']  = $stockInfo['subject'];
                //提货人信息
                $data['pick_list'] = PickUser::getList(['direct_company_id'=>self::getDirectCompanyId(),'status'=>1]);
                $data['pick_default'] = $stockInfo['pick_user_id'];
                //获取申请人姓名
                $user =User::findOne(['id'=>self::getLoginUserId()]);
                $data['apply_id'] = self::getLoginUserId();
                $data['apply_name'] = $user->username;
                //获取发货类型名称
                $dictType = DictEnum::findOneByAttributes(['dict_key'=>'send_out_type','dict_enum_id'=>$stockInfo['stock_type']]);
                $data['send_type']      = $stockInfo['stock_type'];
                $data['send_type_name'] = $dictType['dict_enum_value'];
                //发货方式
                $data['send_method'] = DictEnum::findAllByAttributes(['dict_key'=>'send_method_type'],'dict_enum_id,dict_enum_value');
                //获取合同信息
                $data['default_send_method'] = [];
                $data['account'] = [];
                $data['account_address'] = [];

                if($stockInfo['rel_type'] == 2){ //合同
                    $contraInfo = Contract::findOneByAttributes(['id'=>$stockInfo['rel_id']],"account_id,express_type,account_address_id");
                    $data['default_send_method'] = $contraInfo['express_type'];
                    $data['account'] = Account::findOneByAttributes(['id'=>$contraInfo['account_id']],"account_name,mobile");
                    $data['account_address'] = AccountAddress::findOneByAttributes(['id'=>$contraInfo['account_address_id']],"province_id,city_id,district_id,address");
                }
                if($stockInfo['rel_type'] == 4){ //换货
                    $exchangeInfo = ExchangeGoods::findOneByAttributes(['id'=>$stockInfo['rel_id']],"account_id,express_type,account_address_id");
                    $data['default_send_method'] = $exchangeInfo['express_type'];
                    $data['account'] = Account::findOneByAttributes(['id'=>$exchangeInfo['account_id']],"account_name,mobile");
                    $data['account_address'] = AccountAddress::findOneByAttributes(['id'=>$exchangeInfo['account_address_id']],"province_id,city_id,district_id,address");
                }
                //获取出库单产品信息
                $data['product']     =  StockDetailView::findAllByAttributes(['stock_id'=>$stockId],"prod_id,prod_name,stock_no,prod_no,brand_name,model,dict_enum_value,prod_num,serial_number,depot_name");
                //获取开通省份
                $data['province']    = Region::getOpenProvince();
                return self::success($data);
            }
            return self::error('缺少出库单号');
        }
        /**
         * 添加
         * User: quanl
         * Date: 2018/9/4
         * Time: 14:20
         * @param string $type
         * @return array
         * @throws \Exception
         */
        public static function add()
        {
            $obj = Yii::$app->request->post('postData','');
            $data = [];
            if(isset($obj['send_no']) && $obj['send_no'] != '') {
                $data['send_no'] = $obj['send_no'];
            }else{
                return self::error('缺少发货单号','1001');
            }
            if(isset($obj['stock_id']) && $obj['stock_id'] != '') {
                $data['stock_id'] = $obj['stock_id'];
            }else{
                return self::error('缺少出库单号','1002');
            }
            if(isset($obj['send_source']) && $obj['send_source'] !== '') {
                $data['send_source'] = $obj['send_source'];
            }else{
                return self::error('缺少发货来源字段','1006');
            }
            if(isset($obj['send_type']) && $obj['send_type'] != '' && $obj['send_type'] != 0) {
                $data['send_type'] = $obj['send_type'];
            }else{
                return self::error('缺少发货类型','1003');
            }
            if(isset($obj['send_method']) && $obj['send_method'] != '' && $obj['send_method'] != 0) {
                $data['send_method'] = $obj['send_method'];
            }else{
                return self::error('缺少发货方式','1004');
            }
            if(isset($obj['product_ids']) && !empty($obj['product_ids'])) {
                //$sn = array_column($obj['product_ids'],'sn','id');
                //$id = array_column($obj['product_ids'],'id');
                //$ids = implode(",",$id);
                //$data['product_ids'] = $ids;
                $data['product_ids'] = $obj['product_ids'];
            }else{
                return self::error('缺少发货产品','1005');
            }

            if(isset($obj['send_title']) && $obj['send_title'] != '') {
                $data['send_title'] = $obj['send_title'];
            }
            if(isset($obj['remark']) && $obj['remark'] != '') {
                $data['remark'] = $obj['remark'];
            }
            if(isset($obj['apll_id']) && $obj['apll_id'] != '') {
                $data['apll_id'] = $obj['apll_id'];
            }
            if(isset($obj['apll_no']) && $obj['apll_no'] != '') {
                $data['apll_no'] = $obj['apll_no'];
            }
            if(isset($obj['rec_name']) && $obj['rec_name'] != '') {
                $data['rec_name'] = $obj['rec_name'];
            }
            if(isset($obj['rec_mobile']) && $obj['rec_mobile'] != '') {
                $data['rec_mobile'] = $obj['rec_mobile'];
            }
            if(isset($obj['province_id']) && $obj['province_id'] != 0 && $obj['province_id'] != ''){
                $data['province_id'] = $obj['province_id'];
            }
            if(isset($obj['city_id']) && $obj['city_id'] != 0 && $obj['city_id'] != ''){
                $data['city_id'] = $obj['city_id'];
            }
            if(isset($obj['district_id']) && $obj['district_id'] != 0 && $obj['district_id'] != ''){
                $data['district_id'] = $obj['district_id'];
            }
            if(isset($obj['rec_address']) && $obj['rec_address'] != ''){
                $data['rec_address'] = $obj['rec_address'];
            }
            if(isset($obj['rec_date']) && $obj['rec_date'] != '') {
                $data['rec_date'] = $obj['rec_date'];
            }
            if(isset($obj['pick_name']) && $obj['pick_name'] != '') {
                $data['pick_name'] = $obj['pick_name'];
            }
            if(isset($obj['pick_mobile']) && $obj['pick_mobile'] != '') {
                $data['pick_mobile'] = $obj['pick_mobile'];
            }
            if(isset($obj['pick_card']) && $obj['pick_card'] != '') {
                $data['pick_card'] = $obj['pick_card'];
            }
            if(isset($obj['pick_license']) && $obj['pick_license'] != '') {
                $data['pick_license'] = $obj['pick_license'];
            }
            if(isset($obj['create_user_id']) && $obj['create_user_id'] != '') {
                $data['create_user_id'] = $obj['create_user_id'];
            }

            $data['send_status']        = 1;
            $data['direct_company_id']  = BaseLogic::getDirectCompanyId();
            $data['create_time']        =  date("Y-m-d H:i:s");
            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            try
            {

                $res = SendOut::add($data);
                if($data['send_source'] == 1){ //采购收货  进货
                    Purchase::updateAll(['isship'=>2],['id'=>$obj['stock_id']]);
                    $info = PurchaseDetail::findAllByAttributes(['parent_id'=>$obj['stock_id']],'id,prod_num');
                    foreach ($info as $k =>$v){
                        PurchaseDetail::updateAll(['send_num'=>$v['prod_num']],['id'=>$v['id']]);
                    }
                }else{
                    $stockInfo = StockDetail::findAllByAttributes(['parent_id'=>$obj['stock_id']]);
                    //修改出库信息
                    StockInfo::updateAll(['isship'=>2],['id'=>$obj['stock_id']]);
                    //修改仓储信息
                    foreach ($stockInfo as $key => $value) {
                        $res = DepotProdStock::updateNum($value,0);
                    }
                }
                $transaction->commit();
                return self::success(['send_id'=>$res],'成功');
            } catch(\Exception $e) {
                $transaction->rollBack();
                return self::error('失败');
            }
        }
        /**
         * 确认收货（后台）
         * quanl 2018-9-5
         */
        public static function confirm()
        {
            $sendId  = intval(Yii::$app->request->post('send_id',0));
            $recName  = Yii::$app->request->post('rec_name','');

            if($sendId)
            {
                $sendInfo  = SendOut::findOneByAttributes(['id'=>$sendId]);
                if($sendInfo['send_source'] == 1) //采购单
                {
                    $db = Yii::$app->db;
                    $transaction = $db->beginTransaction();
                    try
                    {
                        //修改发货单状态
                        SendOut::updateAll(['send_status'=>2,'rel_rec_name'=>$recName,'rel_rec_date'=>date("Y-m-d H:i:s")],['id'=>$sendId]);
                        //修改采购单信息
                        Purchase::updateAll(['isship'=>3],['id'=>$sendInfo['stock_id']]);
                        //增加物流信息 收货
                        $data['send_id'] = $sendId;
                        $data['content'] = "产品已签收 签收人:".$recName;
                        $data['update_time'] = date("Y-m-d H:i:s");
                        ApllDetail::addDetail($data);
                        $transaction->commit();
                        return self::success(['send_id'=>$sendId],'成功');
                    } catch(\Exception $e) {
                        $transaction->rollBack();
                        return self::error('失败');
                    }
                }else{  //出库单
                    $stockInfo = StockDetail::findAllByAttributes(['parent_id'=>$sendInfo['stock_id']]);
                    $db = Yii::$app->db;
                    $transaction = $db->beginTransaction();
                    try
                    {

                        //修改发货单状态
                        SendOut::updateAll(['send_status'=>2,'rel_rec_name'=>$recName,'rel_rec_date'=>date("Y-m-d H:i:s")],['id'=>$sendId]);

                        //修改出库信息
                        StockInfo::updateAll(['isship'=>3],['id'=>$sendInfo['stock_id']]);

                        //修改仓储信息
                        foreach ($stockInfo as $key => $value) {
                            DepotProdStock::updateNum($value,1);
                        }
                        //如果是采购退货单
                        $returnPurchaseInfo = StockInfo::findOneByAttributes(['id'=>$sendInfo['stock_id'],'type'=>2,"rel_type"=>7]);
                        if(!empty($returnPurchaseInfo)){
                            $prodNumArr = array_column($stockInfo,'prod_num','prod_id');
                            $detail = ReturnPurchaseDetail::findAllByAttributes(['parent_id'=>$returnPurchaseInfo['rel_id']]);
                            foreach ($detail as $v)
                            {
                                $detail = ReturnPurchaseDetail::findOne(['id'=>$v['id']]);
                                if(isset($prodNumArr[$v['prod_id']])){
                                    $detail->finish_num = $prodNumArr[$v['prod_id']];
                                    $detail->save(false);
                                }
                            }
                            ReturnPurchase::updateAll(['status'=>3],['id'=>$returnPurchaseInfo['rel_id']]);
                        }
                        //如果产品全部发货完成  修改换货单状态
                        $getExchangeInfo = StockInfo::findOneByAttributes(['id'=>$sendInfo['stock_id'],'type'=>2,"rel_type"=>4]);
                        if (!empty($getExchangeInfo))
                        {
                            //换货产品数量
                            $prodNumArr = array_column($stockInfo,'prod_num','prod_id');
                            $getExchangeGoodsDetail = ExchangeGoodsDetail::findAllByAttributes(['parent_id'=>$getExchangeInfo['rel_id'],'type'=>2]);
                            foreach ($getExchangeGoodsDetail as $v)
                            {
                                $Detail = ExchangeGoodsDetail::findOne(['id'=>$v['id']]);
                                if(isset($prodNumArr[$v['prod_id']])){
                                    $Detail->send_num = $v['send_num']+$prodNumArr[$v['prod_id']];
                                    $Detail->save(false);
                                }
                            }
                            $exchangeGoodsDetail = ExchangeGoodsDetail::getList(['parent_id'=>$getExchangeInfo['rel_id']]);
                            if ($exchangeGoodsDetail)
                            {
                                $num = [];
                                foreach ($exchangeGoodsDetail as $val) {
                                    if($val['type'] ==2){
                                        $num[] = $val['prod_num'] - $val['send_num'];
                                    }else{
                                        $num[] = $val['prod_num'] - $val['finish_num'];
                                    }
                                }
                                $sum = array_sum($num);
                                $exchangeGoods = ExchangeGoods::getOne(['id'=>$getExchangeInfo['rel_id']],1);
                                if ($sum == 0) {
                                    $exchangeGoods->status = 4;
                                }
                                $exchangeGoods->save(false);
                                ContractLogic::checkContract($exchangeGoods->contract_id);
                            }
                        }
                        //如果合同产品全部发货完成  修改合同状态
                        $getContraInfo = StockInfo::findOneByAttributes(['id'=>$sendInfo['stock_id'],'type'=>2,"rel_type"=>2]);

                        if (!empty($getContraInfo))
                        {
                            //修改合同已发货产品数量
                            $prodNumArr = array_column($stockInfo,'prod_num','prod_id');
                            $getContractDetail = ContractDetail::findAllByAttributes(['parent_id'=>$getContraInfo['rel_id']]);
                            foreach ($getContractDetail as $v)
                            {
                                $Detail = ContractDetail::findOne(['id'=>$v['id']]);
                                if(isset($prodNumArr[$v['prod_id']])){
                                    $Detail->send_num = $v['send_num']+$prodNumArr[$v['prod_id']];
                                    $Detail->save(false);
                                }
                            }
                            ContractLogic::checkContract($getContraInfo['rel_id']);
                        }
                        //增加物流信息 收货
                        $data['send_id'] = $sendId;
                        $data['content'] = "产品已签收 签收人:".$recName;
                        $data['update_time'] = date("Y-m-d H:i:s");
                        ApllDetail::addDetail($data);
                        $transaction->commit();
                        return self::success(['send_id'=>$sendId],'成功');
                    } catch(\Exception $e) {
                        $transaction->rollBack();
                        return self::error('失败');
                    }
                }
            }
            return self::error('没有找到此发货单','1011');
        }
        //详情
        public static function detail()
        {
            $sendId  = intval(Yii::$app->request->post('send_id',0));
            if($sendId){

                $sendInfo  = SendOut::getOne(['id'=>$sendId]);
                //物流信息
                $apll = Apll::findOneByAttributes(['id'=>$sendInfo['apll_id']],"name");
                $sendInfo['apll_name'] = isset($apll['name'])? $apll['name']: '';
                $apllDetail = ApllDetail::getDetail($sendInfo['id']);
                $sendInfo['apll_detail'] = $apllDetail;
                
                //获取申请人姓名
                $user = User::findOne(['id'=>$sendInfo['create_user_id']]);
                $sendInfo['apply_name'] = $user->username;
                //出库产品信息
                $sendInfo['stock_no'] = '';
                if($sendInfo['send_source'] == 1){
                    $stockInfo = PurchaseDetailView::findAllByAttributes(['stock_id'=>$sendInfo['stock_id']],"prod_name,stock_no,prod_no,brand_name,model,dict_enum_value,prod_num");
                }else{
                    $stockInfo = StockDetailView::findAllByAttributes(['stock_id'=>$sendInfo['stock_id']],"prod_name,stock_no,prod_no,brand_name,model,dict_enum_value,prod_num,serial_number,depot_name");
                }
                if(!empty($stockInfo)){
                    $sendInfo['stock_no'] =$stockInfo[0]['stock_no'];
                }
                $sendInfo['product']  = $stockInfo;

                //获取开通省份
                $sendInfo['province'] = Region::getOpenProvince();

                //删除接口不需要的字段
                unset($sendInfo['direct_company_id']);
                unset($sendInfo['send_type']);
                unset($sendInfo['product_ids']);
                unset($sendInfo['apll_id']);
                unset($sendInfo['create_user_id']);

                return self::success($sendInfo);
            }
            return self::error('没有找到此发货单','1011');
        }
        //更新物流信息
        public static function AddApllDetail(){
            $obj = Yii::$app->request->post('postData','');
            if(isset($obj['id']) && $obj['id'] != '' && $obj['id'] != 0){
                $data['send_id'] = $obj['id'];
            }else{
                return self::error('没有找到此发货单','1011');
            }
            if(isset($obj['update_time']) && $obj['update_time'] != ''){
                $data['update_time'] = $obj['update_time'];
            }else{
                return self::error('更新日期不能为空','1014');
            }
            if(isset($obj['content']) && $obj['content'] != ''){
                $data['content'] = $obj['content'];
            }else{
                return self::error('物流进度不能为空','1015');
            }
            $res = ApllDetail::addDetail($data);
            if($res){
                return self::success(['send_id'=>$obj['id']]);
            }
            return self::error('更新失败');
        }
        //编辑收货人信息
        public static function editRecInfo(){
            $obj = Yii::$app->request->post('postData','');
            $data = [];
            if(isset($obj['id']) && $obj['id'] != '' && $obj['id'] != 0){
                $data['id'] = $obj['id'];
            }else{
                return self::error('没有找到此发货单','1011');
            }
            if(isset($obj['rec_name'])){
                $data['rec_name'] = $obj['rec_name'];
            }
            if(isset($obj['rec_mobile'])){
                $data['rec_mobile'] = $obj['rec_mobile'];
            }
            if(isset($obj['province_id']) && $obj['province_id'] != 0 && $obj['province_id'] != ''){
                $data['province_id'] = $obj['province_id'];
            }
            if(isset($obj['city_id']) && $obj['city_id'] != 0 && $obj['city_id'] != ''){
                $data['city_id'] = $obj['city_id'];
            }
            if(isset($obj['district_id']) && $obj['district_id'] != 0 && $obj['district_id'] != ''){
                $data['district_id'] = $obj['district_id'];
            }
            if(isset($obj['rec_address']) && $obj['rec_address'] != ''){
                $data['rec_address'] = $obj['rec_address'];
            }
            $res = SendOut::add($data);
            if($res){
                return self::success(['send_id'=>$res]);
            }
            return self::error('编辑失败');
        }
        //编辑提货人信息
        public static function editPickInfo(){
            $obj = Yii::$app->request->post('postData','');
            $data = [];
            if(isset($obj['id']) && $obj['id'] != '' && $obj['id'] != 0){
                $data['id'] = $obj['id'];
            }else{
                return self::error('没有找到此发货单','1011');
            }
            if(isset($obj['pick_name'])){
                $data['pick_name'] = $obj['pick_name'];
            }
            if(isset($obj['pick_mobile'])){
                $data['pick_mobile'] = $obj['pick_mobile'];
            }
            if(isset($obj['pick_card'])){
                $data['pick_card'] = $obj['pick_card'];
            }
            if(isset($obj['pick_name'])){
                $data['pick_license'] = $obj['pick_license'];
            }
            $res = SendOut::add($data);
            if($res){
                return self::success(['send_id'=>$res]);
            }
            return self::error('编辑失败');
        }

    }
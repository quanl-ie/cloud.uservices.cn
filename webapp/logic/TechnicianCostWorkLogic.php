<?php
namespace webapp\logic;
use webapp\models\WorkCostDevide;
use Yii;
use webapp\models\Technician;
use webapp\models\BaseModel;
use common\helpers\Helper;
use webapp\models\WorkForm;



class TechnicianCostWorkLogic extends BaseLogic
{
    
    /**
     * 获取列表数据
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @return type
     */
    public static function getList()
    {
        $query['company']     = trim(Yii::$app->request->get('company'));
        $query['status']      = intval(Yii::$app->request->get('status'));

        $query['currentPage']  = Yii::$app->request->get('page',1);
        $query['pageSize']     = Yii::$app->request->get('pageSize',10);
        $query['a.src_type']     = BaseModel::SRC_FWS;
        $query['a.src_id']       = self::getManufactorId();
        //$query['a.src_id']       = 12;

        //确定结算单状态
        $action    = Yii::$app->controller->action->id;
        if($action == 'wait-cost'){
            $query['a.settlement_status'] = 1;
        }else{
            $query['a.settlement_status'] = 2;
        }
        $technicianName = trim(Yii::$app->request->get('technician_name'));
        if($technicianName!=''){
            $technician = Technician::getList(['like','name',$technicianName],[],1,1000);
            if($technician){
                $techIds = array_column($technician['list'],'id');
                $query['technician_id'] = $techIds;
            }


        }



        //$list = WorkCostDevide::getList($query);
        $list = WorkForm::getList($query);
     /*   echo '<pre>';
        print_R($list);exit;*/

        return $list;
    }
    /**
     * 获取添加编辑列表数据
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @return type
     */
    public static function addList()
    {
        $data = [];
        $costItemSetList = CostItemSet::getList();
        $data['list'] = [];
        if ($costItemSetList) {
            $data['list'] = $costItemSetList;
        }
        $costItemIds = [];
        $costItemList = CostItem::getList(['status'=>1]);
        foreach ($costItemList as $val) {
            $costItemIds[] = $val->cs_id;
        }
        $data['ids'] = [];
        if ($costItemIds) {
            $data['ids'] = $costItemIds;
        }
        return $data;
    }
    /**
     * 数据添加和编辑
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function add()
    {
        $postData = Yii::$app->request->post();
        $postData['cost_start_time'] = strtotime(Yii::$app->request->post('cost_start_time'));
        $postData['cost_end_time']   = strtotime(Yii::$app->request->post('cost_end_time'));
        $postData['create_time']     = $postData['update_time'] = time();
        $postData['src_type']        = BaseModel::SRC_FWS;
        $postData['src_id']          = BaseLogic::getManufactorId();
        //确定结算单状态
        $action    = Yii::$app->controller->action->id;
        if($action == 'wait-check'){
            $query['status'] = 1;
        }elseif($action == 'wait-cost'){
            $query['status'] = 2;
        }else{
            $query['status'] = 3;
        }


        $res = Cost::add($postData);

        if ($res) {
            return self::success($res,'成功');
        }
        return self::error('失败');
    }



    //获取详情
    public  static function getDetail(){
        $id = intval(Yii::$app->request->get('id'));
        $info = Cost::getOne(['id'=>$id]);
        return $info;
    }



    /**
     * 区分需要添加或编辑的数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $cs_id
     * @return type
     */
    public static function getSaveData($cs_id)
    {
        $data = [];
        //所有数据
        $res = CostItemSet::getList();
        //数据库id
        $oldIds = self::getCostItemIds();
        //获取需要删除的id
        $del = array_diff($oldIds, $cs_id);

        //获取需要添加的id
        $add = array_diff($cs_id, $oldIds);

        //获取需要修改的id
        $edit = array_intersect($cs_id, $oldIds);

        //组合数据
        if ($res) {
            foreach ($res as $key => $val) {
                if (in_array($val['id'], $add)) {
                    $data['add'][$key]['src_type']    = BaseModel::SRC_FWS;
                    $data['add'][$key]['src_id']      = self::getManufactorId();
                    $data['add'][$key]['cs_id']       = $val['id'];
                    $data['add'][$key]['cs_name']     = $val['item_name'];
                    $data['add'][$key]['create_time'] = $data['add'][$key]['update_time'] = time();
                    $data['add'][$key]['status']      = 1;
                    $data['add'][$key]['oprate_id']   = Yii::$app->user->getIdentity()->id;
                }
                if (in_array($val['id'], $edit)) {
                    $data['edit'][$key]['cs_id'] = $val['id'];
                    $data['edit'][$key]['cs_name'] = $val['item_name'];
                    $data['edit'][$key]['update_time'] = time();
                    $data['edit'][$key]['status']      = 1;
                    $data['edit'][$key]['oprate_id'] = Yii::$app->user->getIdentity()->id;
                }
                if (in_array($val['id'], $del)) {
                    $data['del'][$key]['cs_id'] = $val['id'];
                    $data['del'][$key]['update_time'] = time();
                    $data['del'][$key]['status']      = 2;
                    $data['del'][$key]['oprate_id'] = Yii::$app->user->getIdentity()->id;
                }
            }
        }
        return $data;
    }
    /**
     * 获取已存在的收费项目 cs_id
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function getCostItemIds()
    {
        $res = CostItem::getList();
        $ids = [];
        foreach ($res as $key => $val) {
            $ids[] = $val['cs_id'];
        }
        return $ids;
    }
}
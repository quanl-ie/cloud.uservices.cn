<?php
    /**
     * 仓储管理开关设置
     * User: xingq
     * Date: 2018/5/4
     * Time: 10:41
     */
    
    namespace webapp\logic;
    
    use Yii;
    use common\models\SysConfig;
    use webapp\models\BaseModel;
    
    class SysConfigLogic extends BaseLogic
    {
        /**
         * 列表显示数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/5/7
         * Time: 15:20
         * @return mixed
         */
        public static function BeforeList()
        {
            $directCompanyId = self::getDirectCompanyId();
            $departmentTopId = self::getTopId();

            $sys = SysConfig::getOne(['config_key'=>'storage_status','direct_company_id'=>$directCompanyId],1);

            if (!empty($sys)) {
                $data['sys'] = $sys;
            }else{
                $data['sys'] = [];
            }
            
            $option = self::getSysConfigOption($directCompanyId);

            if ($option['code'] == 200) {
                $data['option'] = $option['data'];
            }else{
                $data['option'] = [];
            }

            return $data;
        }
    
        /**
         * 修改
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/5/7
         * Time: 15:20
         * @return array
         * @throws \Exception
         */
        public static function edit()
        {
            $id = intval(Yii::$app->request->post('id',0));
            $configValues = intval(Yii::$app->request->post('config_values',0));
            
            if ($id == 0) {
                return self::error('保存失败',20001);
            }
            if ($configValues == 0) {
                return self::error('保存失败',20002);
            }
            
            $data['id'] = $id;
            $data['config_values'] = $configValues;
            
            if ($data['config_values'] == 1) {
                $data['status'] = 1;
            }else{
                $data['status'] = 0;
            }
            
            $res = SysConfig::edit($data);
            if ($res) {
                return self::success('','保存成功，下次登录时生效！');
            }else{
                return self::error('保存失败');
            }
            
        }
    }
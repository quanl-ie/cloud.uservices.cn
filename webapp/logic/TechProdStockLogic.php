<?php
namespace webapp\logic;
use common\models\DepotProdStock;
use Yii;
use webapp\models\BaseModel;
use common\helpers\Helper;
use webapp\models\Region;
use common\models\DictEnum;
use common\helpers\CreateNoRule;
use common\models\User;
use common\models\StockInfo;
use webapp\models\TechProdStock;
use webapp\models\TechProdStockDetail;
/**
 * 技师申请返还管理
 * @author sxz
 * @date 2018-03-29
 */
class TechProdStockLogic extends BaseLogic
{
    /**
     * 数据列表
     * @author sxz
     * @return string
     */
    public static function getList($stock_type)
    {
        $params = [];
        $where = [];
        $action    = Yii::$app->controller->action->id;
        $params['tech_name']    = $tech_name = trim(Yii::$app->request->get('tech_name','')); //技师名字
        $params['tech_mobile']  = $tech_mobile = trim(Yii::$app->request->get('tech_mobile','')); //技师名字

        $params['type']                 = $type = Yii::$app->request->get('type',''); //采购类型
        $params['status']               = $status = Yii::$app->request->get('status',''); //采购状态

        $params['page']                 = $page = intval(Yii::$app->request->get('page',1));
        $params['pageSize']             = $pageSize = intval(Yii::$app->request->get('pageSize',20));
        $maps = ['direct_company_id'=>BaseLogic::getDirectCompanyId(),'department_top_id'=>BaseLogic::getTopId()];
        $maps['stock_type'] = $stock_type;
        $maps['status'] = 1;
        //print_r($action);die;
        if($action == 'apply' || $action == 'refund'){  //待审批的
            $maps['audit_status'] = $audit_status = '1';
        }elseif($action == 'wait-check'){  //待出/入库的
            $maps['audit_status'] = $audit_status = '2';
        }elseif($action == 'finished'){   //已完成
            $maps['audit_status'] = $audit_status = '3';
        }elseif($action == 'not-pass'){ //未通过的
            $maps['audit_status'] = $audit_status = '4';
        }elseif($action == 'cancelled'){ //已取消的
            //$maps['audit_status'] = $audit_status = '5';
            $maps['status'] = $status = '2';   //$status  1 正常   2 取消
        }

        //搜索条件
        if (yii::$app->request->isGet) {
            if ($tech_name != '') {
                $where[] = ['like','tech_name',$tech_name];
            }
            if ($tech_mobile != '') {
                $maps['tech_mobile'] = $tech_mobile;
            }
        }
        //print_r($maps);die;
        $data = TechProdStock::getList($maps,$where,$page,$pageSize);
        if($data['list']) foreach ($data['list'] as $key=>$val){
            //获取每个单据详情表单的总数量
            $data['list'][$key]['total_num'] = self::getProdNum($val['id']);
        }
        $data['params'] = $params;
        return $data;
    }
    /**
     * 函数用途描述:获取技师备件申请返还详情
     * @date: 2018年3月29日 下午15:28:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function getDetail($id)
    {
        $data = [];
        if($id){
            $Prod_info = TechProdStock::getOne(['id'=>$id,'direct_company_id'=>BaseLogic::getDirectCompanyId(),'department_top_id'=>BaseLogic::getTopId()]);
            if(!$Prod_info){
                return $data;
            }
            if($Prod_info['status'] == 1){
                $Prod_info['status_name'] = self::getAuditStatus($Prod_info['audit_status'],$Prod_info['stock_type']);
            }else{
                $Prod_info['status_name'] = self::getAuditStatus($Prod_info['status'],$Prod_info['stock_type'],1);
            }
            //获取申请返还明细数据
            $detail = self::getProduct($Prod_info['id']);
            //print_r($detail);die;
            if ($Prod_info) {
                $data['info'] = $Prod_info;
            }else{
                $data['info'] = [];
            }
            if ($detail['success'] && $detail['success'] == 1) {
                $data['detail'] = $detail['data'];
            }else{
                $data['detail'] = [];
            }
            return $data;
        }
        return $data;
    }
    //获取审批状态数据
    public static function getAuditStatus($status,$stock_type,$flag=''){
        //$StatusList = DictEnum::getList(['dict_key'=>'enum_tech_stock_audit_status','org_id'=>BaseLogic::getManufactorId(),'org_type'=>BaseModel::SRC_FWS]);
        if($flag){
            if($stock_type == 1){
                $StatusList = DictEnum::getList(['dict_key'=>'enum_tech_stock_out_status']);
            }
            if($stock_type == 2){
                $StatusList = DictEnum::getList(['dict_key'=>'enum_tech_stock_in_status']);
            }
            $StatusList = array_column($StatusList,'dict_enum_value','dict_enum_id');
            $data = isset($StatusList[$status])?$StatusList[$status]:'';
            return $data;
        }
        $StatusList = DictEnum::getList(['dict_key'=>'enum_tech_stock_audit_status']);
        if($StatusList){
            $StatusList = array_column($StatusList,'dict_enum_value','dict_enum_id');
        }
        if($stock_type == 1){
            $data = isset($StatusList[$status])?$StatusList[$status]:'';
            if($status == 2){
                $data = '待出库';
            }
        }
        if($stock_type == 2){
            $data = isset($StatusList[$status])?$StatusList[$status]:'';
            if($status == 2){
                $data = '待入库';
            }
        }
        return $data;
    }
    /**
     * 获取产品明细数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/23
     * Time: 17:29
     * @param $id
     * @return array
     */
    public static function getProduct($id)
    {
        $where = [
            ['a.direct_company_id'=>BaseLogic::getDirectCompanyId()],
            ['a.department_top_id'=>BaseLogic::getTopId()],
            ['a.parent_id' => $id]
        ];
        $product = TechProdStockDetail::getProduct($where);
        //print_r($product);die;
        if (!$product) {
            return self::error('获取产品明细失败');
        }
        //调用产品逻辑层组合数据方法
        $productData = ProductLogic::changeData($product, 1);
        if (!$productData) {
            return self::error('获取产品明细失败');
        }
        //获取产品库存数量
        foreach ($productData as $key=>$val){
            $productData[$key]['total_num'] = self::getProdTotalNum($val['prod_id']);
        }
        //print_r($productData);die;
        return self::success($productData, '成功');
    }
    /**
     * 操作审核状态
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function SetAuditStatus($no,$status,$opinionContent)
    {
        $res = TechProdStock::findOne(['id'=>$no,'audit_status'=>'1']);
        if($res){
            $res->audit_suggest = $opinionContent;
            $res->audit_status = $status;
            $res->audit_date = date('Y-m-d H:i:s',time());
            $res->audit_user_id = BaseLogic::getLoginUserId();
            $save = $res->save(false);
            if($save){
                return self::success('','操作成功！');
            }
            return self::error('操作失败！',20004);
        }
        return self::error('操作失败!',20004);
    }
    /**
     * 获取申请单据的总量
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function getProdNum($id)
    {
        $total_num = 0;
        if($id > 0){
            $list = TechProdStockDetail::getList(['parent_id'=>$id,'direct_company_id'=>BaseLogic::getDirectCompanyId(),'department_top_id'=>BaseLogic::getTopId()]);
            if($list) foreach ($list as $key=>$val){
                $total_num += $val['prod_num'];
            }
            return $total_num;
        }
        return $total_num;
    }
    /**
     * 获取申请单据的产品的库存总量
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function getProdTotalNum($id)
    {
        $total_num = 0;
        if($id > 0){
            $list = DepotProdStock::getList(['prod_id'=>$id,'direct_company_id'=>BaseLogic::getDirectCompanyId(),'department_top_id'=>BaseLogic::getTopId()]);
            if($list) foreach ($list as $key=>$val){
                $total_num += $val['total_num'];
            }
            return $total_num;
        }
        return $total_num;
    }




}
<?php
namespace webapp\logic;

use yii;
use webapp\models\Account;
use webapp\models\BaseModel;
use common\helpers\CreateNoRule;
use webapp\models\AccountAddress;
use webapp\models\AdminUser;
use common\models\ReturnGoods;
use common\models\ReturnGoodsDetail;
use common\models\Contract;
use common\models\ContractDetail;
use common\helpers\Helper;
use common\models\StockInfo;
/***
 * @author chengjuanjuan<chengjuanjuan@c-ntek.com>
 * @created_at 2017-12-14 14:01
 * desc the logic of sale orders
 */


class ReturnGoodsLogic extends BaseLogic
{


    /**
     * 函数用途描述：
     * @date: 2017年12月14日 下午14:48:01
     * @author: chengjuanjuan<chengjuanjuan@c-ntek.com>
     * @param: $id
     * @return:$data
     */
    public static function getList(){

        $account_id     = intval(yii::$app->request->get('account_id'));  //此参数用于从客户详情中查看此客户的合同信息，不影响现有的搜索，不要删除 by sxz
        $accountName    = Helper::filterWord(Yii::$app->request->get('account_name'));
        $accountMobile  = trim(Yii::$app->request->get('account_mobile'));
        if($accountMobile)
            $accountName = $accountMobile;
        if($accountName){
            $accountIds = '';
            $accountList = Account::getAccountInfo($accountName,self::getDepartmentId(),2);
            foreach($accountList as $v){
                $accountIds[] = $v['id'];
            }
            $query['account_id'] = $accountIds;
        }
        //此参数用于从客户详情中查看此客户的合同信息，不影响现有的搜索，不要删除 by sxz
        if($account_id){
            $query['account_id'] = $account_id;
        }
        $query['startTime']    =  trim(Yii::$app->request->get('plan_start_time'));
        $query['endTime']      =  trim(Yii::$app->request->get('plan_end_time'));

        $getDataAuth           = self::getListDataAuth();
        if(empty($getDataAuth)){
            exit('无数据权限');
        }

        $departmentIds          = $getDataAuth['department_id'];
        $dataAuth               = $getDataAuth['dataAuth'];
        $createUserId           = $getDataAuth['create_user_id']?$getDataAuth['create_user_id']:'';
        $department_name        = $getDataAuth['department_name'];
        if($createUserId){
            $query['create_user_id'] = $createUserId;
        }
        $query['department_id'] = $departmentIds;

        //确定状态
        $action    = Yii::$app->controller->action->id;
        if($action == 'wait-check'){
            $query['status'] = 1;
            //审批通过的換貨單直接进入执行中
        }elseif($action == 'executing'){
            $query['status'] = 3;
        }elseif($action == 'execute-finished') {
            $query['status'] = 4;

        }elseif($action == 'unchecked') {
            $query['status'] = 5;

        }elseif($action == 'terminated') {
            $query['status'] = 6;

        }
        $query['no']           = trim(Yii::$app->request->get('no'));
        $query['subject']      = Helper::filterWord(Yii::$app->request->get('subject'));
        $query['currentPage']  = Yii::$app->request->get('page',1);
        $query['pageSize']     = Yii::$app->request->get('pageSize',10);

        $data = ReturnGoods::getList($query);
        if(!empty($data['list'])){
            foreach($data['list'] as &$v){
                $accountInfo          = Account::getOne(['id'=>$v['account_id']]);
                $accountInfo_name       = isset($accountInfo->account_name)?$accountInfo->account_name:'';
                $accountInfo_mobile     = isset($accountInfo->mobile)?$accountInfo->mobile:'';
                $v['account_name']    = $accountInfo_name;
                $v['account_mobile']  = $accountInfo_mobile;
                $v['option']          = isset($dataAuth[$v['department_id']]) ? $dataAuth[$v['department_id']]['option'] : '';
                $v['department_name'] = isset($dataAuth[$v['department_id']]) ? $dataAuth[$v['department_id']]['name'] : '';
            }
        }

        $data['department_name'] = $department_name;
        return $data;
    }



    /**
     * @desc 合同添加
     * @author:程娟娟
     * @createAt:20171215
     **/
    public static function add()
    {//处理数据
        $accountId                 = intval(yii::$app->request->post('account_id'));
        $subject                   = trim(yii::$app->request->post('subject'));
        if(!$accountId)
            return self::error('用户不存在');
        //退货的产品处理明细
        $contractDetail =   yii::$app->request->post('contractDetail');

        $topId            =   self::getTopId();
        $departmentId     =   self::getDepartmentId();
        $totalAmount = 0;
        foreach($contractDetail as $k => $v){
            if(isset($v['id'])){
                $productInfo = ContractDetail::getOne(['id'=>$k]);
                $productInfo['prod_num'] = $v['prod_num'];
                unset($productInfo['id']);
                unset($productInfo['parent_id']);
                unset($productInfo['finish_num']);
                unset($productInfo['total_num']);
                unset($productInfo['time_stamp']);
                unset($productInfo['org_type']);
                unset($productInfo['org_id']);
                unset($productInfo['warranty_date']);
                $products[$k] = $productInfo;
                $products[$k]['department_id']   = $departmentId;
                $products[$k]['department_top_id'] = $topId;
                $totalAmount += $productInfo['sale_price']*$productInfo['prod_num'];
            }
        }
        //基本信息
        $data['department_top_id']  =  $topId;
        $data['department_id']      =   $departmentId;
        $data['no']                 =   CreateNoRule::getNo('TH');
        $data['account_id']         =   $accountId;
        $data['subject']            =   $subject;
        $data['return_date']        =   trim(yii::$app->request->post('return_date',''));
        $data['return_reason']      =   intval(yii::$app->request->post('return_reason'));
        $data['contract_id']        =   intval(yii::$app->request->post('contract_id'));
        $data['need_stock_in']      =   intval(Yii::$app->request->post('need_stock_in'));
        //$data['total_amount']       =   trim(Yii::$app->request->post('total_amount'));
        $data['total_amount']       =   $totalAmount;

        //系统信息
        $data['create_time']       =   date("Y-m-d H:i:s");
        $data['create_user_id']    = Yii::$app->user->id;
        $data['status']            =  yii::$app->request->post('status','1');
        $res = ReturnGoods::add($data,$products);
        if ($res) {
            return self::success([
                'id' => $res
            ],'添加成功');
        }
        return self::error('添加失败');
    }






    /**
     * @desc 修改
     * @author chengjuanjuan <chengjuanjuan@c-ntek.com>
     * @createAt 2017-12-18 16:06
     **/
    public static function edit()
    {
        $id                        = intval(Yii::$app->request->post('id'));
        $accountId                 = intval(Yii::$app->request->post('account_id'));
        $subject                   = trim(Yii::$app->request->post('subject'));
        $totalAmount               = trim(Yii::$app->request->post('total_amount'));
        if(!$id)
            return self::error('该退货不存在');


        //退货产品处理明细
        $contractDetail            =   yii::$app->request->post('contractDetail');
        foreach($contractDetail as $k=>$v){
            if(isset($v['id'])&&!empty($v['id'])){
                $productInfo = ContractDetail::getOne(['id'=>$v['id']]);
                $productInfo['prod_num'] = $v['prod_num'];
                $haveInfo = [
                    'prod_id'=>$productInfo['prod_id'],
                    'prod_num'=>$v['prod_num'],
                    'sale_price'=>$productInfo['sale_price'],
                    'rate'=>$productInfo['rate'],
                    'amount'=>$v['amount'],
                    'total_amount'=>$v['total_amount'],
                    'warranty_date'=>$v['warranty_date'],
                ];
                $productList[] = $haveInfo;
            }
        }


        //基本信息
        $data['id']                 =  $id;
        $data['department_id']      =   self::getDepartmentId();
        $data['no']                 =   CreateNoRule::returnGoodsNo();
        $data['subject']            =   $subject;
        $data['return_date']        =   trim(yii::$app->request->post('return_date',''));
        $data['return_reason']      =   intval(yii::$app->request->post('return_reason'));
        $data['need_stock_in']      =   intval(Yii::$app->request->post('need_stock_in'));
        $data['total_amount']       =   $totalAmount;
        //系统信息
        $data['create_time']        =   date("Y-m-d H:i:s");
        $data['create_user_id']     =   Yii::$app->user->id;
        $data['status']             =   1;
        $data['audit_status']       = 0;

        $res = ReturnGoods::edit($data,$productList);
        if ($res) {
            return self::success([
                'id' => $res
            ],'编辑成功');
        }
        return self::error('编辑失败');
    }





    public static function  getDetail($id=''){
        if(!$id){
            $id = Yii::$app->request->get('id');
        }
        if(!$id){
            return self::error('该id不存在');
        }
        $getDataAuth = self::getListDataAuth();
        $demartmentId = $getDataAuth['department_id'];
        $info = ReturnGoods::getOne(['id'=>$id,'department_id'=>$demartmentId]);
        if(empty($info)){
            return self::error('该换货单不存在');
        }
        $contractId = $info['contract_id'];
        $contractInfo = Contract::getOne(['id'=>$contractId]);
        $info['contract_info'] = $contractInfo['subject'];
        $account = Account::getOne(['id'=>$contractInfo['account_id']]);
        $address = AccountAddress::getOne(['account_id'=>$contractInfo['account_id'],'is_default'=>1]);
        $info['account_name'] = isset($account)? $account->account_name.'/'.$account->mobile.'/'.$address->address : '';
        $info['account'] = isset($account) ? $account->account_name : '';
        $showStatus = ReturnGoods::showStatus();
        $info['show_status']  = $showStatus[$info['status']];
        $showReasonStatus = ReturnGoods::showReasonStatus();
        if($info['return_reason']){
            $info['show_reason_status']  = $showReasonStatus[$info['return_reason']];
        }
        $showExpressType = ReturnGoods::showExpressType();
        if($info['create_user_id']){
            $admin = AdminUser::getOne(['id'=>$info['create_user_id']]);
            $info['create_user'] = $admin['username'];
        }

        $detail = ReturnGoodsDetail::getList(['parent_id'=>$id]);
        foreach($detail as &$v){
            $productInfo = ProductLogic::getDetail($v['prod_id']);
            if(isset($productInfo['data'])){
                $product = ProductLogic::changeData(array($productInfo['data']),'');
                $v['prod_name']   = isset($product[0]['prod_name'])?$product[0]['prod_name']:'';
                $v['brand_name']  = isset($product[0]['brand_name'])?$product[0]['brand_name']:'';
                $v['class_name']  = isset($product[0]['class_name'])?$product[0]['class_name']:'';
                $v['model']       = isset($product[0]['model'])?$product[0]['model']:'';
            }else{
                $detail = [];
            }
        }
        //入库记录
        $stockInList = StockInfo::getList(['rel_id' => $info['id'], 'is_rel' => 1 ,'rel_type' => 3,'type'=>1]);
        $data = [
            'info'=>$info,
            'detail'=>$detail,
            'stockInList'=>isset($stockInList)?$stockInList:[]
            ];
        return self::success($data);
    }


    public  static function updateStatus($type='check'){
        $id = intval(Yii::$app->request->post('id'));
        if($type=='check'){
            $auditStatus = intval(Yii::$app->request->post('auditStatus'));
            $auditSuggest = trim(Yii::$app->request->post('auditSuggest'));
            if($auditStatus==1){  //审核通过
                $status = 3;
            }elseif($auditStatus==2){ //审核不通过
                $status=5;
            }
            $data = [
                'audit_status'=>$auditStatus,
                'audit_suggest'=>$auditSuggest,
                'status'=>$status,
                'audit_date'=>date("Y-m-d H:i:s"),
                'audit_user_id'=>Yii::$app->user->id,
            ];
        }elseif($type=='delete'){
            $data['status'] = 7;
        }elseif($type='terminate'){
            $data['status'] = 6;
        }

        $res = ReturnGoods::updateStatus($data,$id);
        if($res){
            return self::success($res);
        }else{
            return self::error('更新失败');
        }
    }


    //退货审核
    public  static function checkOption(){
        $id = intval(Yii::$app->request->post('id'));
        $auditStatus = intval(Yii::$app->request->post('auditStatus'));
        $auditSuggest = trim(Yii::$app->request->post('auditSuggest'));
        if($auditStatus==1){  //审核通过
            
            //查询是否需要入库
            $getDataAuth = self::getListDataAuth();
            $demartmentId = $getDataAuth['department_id'];
            $info = ReturnGoods::getOne(['id'=>$id,'department_id'=>$demartmentId]);
            //跟合同详情里的total_num比对，申请的数据大于当前数据直接返回错误
            $data = self::getDetail($id);
            $returnInfo  = $data['data']['info'];
            $returnDetail = $data['data']['detail'];
            $contrctId = $returnInfo['contract_id'];
            $contractList = ContractDetail::getList(['parent_id'=>$contrctId]);
            foreach($contractList as $v){
                $prod[$v['prod_id']] = $v['total_num'];
            }
            foreach($returnDetail as $v){
                if(isset($v['prod_id'])){
                    $lastNum = $prod[$v['prod_id']];
                    if(!$lastNum)
                        return self::error('合同可退货数量不能为0','20023');
                    if($v['prod_num']>$lastNum){
                        return self::error('申请退货数量不能超过合同数量','2002');
                    }else{
                        $needUpdate[] = [
                            'parent_id' => $contrctId,
                            'total_num' =>$lastNum-$v['prod_num'],
                            'prod_id'   =>$v['prod_id']
                        ];
                    }
                }
            }
            if ($info['need_stock_in'] == 1) {
                $status = 3;
            }else{
                $status = 4;
            }
            
        }elseif($auditStatus==2){ //审核不通过
            $status=5;
            $contrctId = '';
            $needUpdate = [];
        }
        $data = [
            'audit_status'=>$auditStatus,
            'audit_suggest'=>$auditSuggest,
            'status'=>$status,
            'audit_date'=>date("Y-m-d H:i:s"),
            'audit_user_id'=>Yii::$app->user->id,
        ];
        
        $res = ReturnGoods::checkOption($data,$id,$contrctId,$needUpdate);
        if($res){
            return self::success($res);
        }else{
            return self::error('审批操作失败');
        }
    }







}
<?php
namespace webapp\logic;


use common\models\User;
use Yii;
use webapp\models\BaseModel;
use common\helpers\Helper;
use common\helpers\CreateNoRule;
use common\models\PickUser;
/**
 * 提货人管理
 * @author sxz
 * @date 2018-09-25
 */
class PickUserLogic extends BaseLogic
{
    /**
     * 数据列表
     * @author sxz
     * @return string
     */
    public static function getList()
    {
        $directCompanyId = BaseLogic::getDirectCompanyId();
        $result = PickUser::getList(['direct_company_id'=>$directCompanyId,'status'=>1]);
        return $result;
    }
    /**
     * 数据添加
     */
    public static function add()
    {
        $data = Yii::$app->request->post();
        //print_r($data);die;
        $pickName       = trim(Yii::$app->request->post('pick_name','')); //提货人姓名
        $pickMobile     = trim(Yii::$app->request->post('pick_mobile','')); //提货人电话
        $pickCard       = trim(Yii::$app->request->post('pick_card','')); //提货人身份
        $pickLicense    = trim(Yii::$app->request->post('pick_license','')); //提货人车牌号

        $postData['pick_name']      = htmlspecialchars($pickName);
        $postData['pick_mobile']    = $pickMobile;
        $postData['pick_card']      = htmlspecialchars($pickCard);
        $postData['pick_license']   = htmlspecialchars($pickLicense);
        $postData['department_id']  = BaseLogic::getDepartmentId();
        $postData['direct_company_id']  = BaseLogic::getDirectCompanyId();
        $postData['create_user_id']     = BaseLogic::getLoginUserId();
        $postData['status']             = 1;  //默认状态
        $postData['create_time']        = $postData['update_time'] = time();
        //验证数据
        if ($pickName == '') {
            return self::error('提货人姓名不能为空!',20001);
        }
        if ($pickMobile == '') {
            return self::error('提货人电话不能为空!',20002);
        }
        if ($pickCard == '') {
            return self::error('提货人身份证号码不能为空!',20003);
        }
        if ($pickLicense == '') {
            //return self::error('提货人车牌号不能为空!',20004);
        }
        //执行数据插入
        $res = PickUser::add($postData);
        if($res){
            return self::success($res,'创建成功');
        }
        return self::error('创建失败');
    }
    /**
     * 函数用途描述:编辑提货人信息
     * @date: 2018年9月25日 10:27:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function edit(){
        $data = Yii::$app->request->post();
        //print_r($data);die;
        $pickId         = intval(Yii::$app->request->post('id',0)); //提货人id
        $pickName       = trim(Yii::$app->request->post('pick_name','')); //提货人姓名
        $pickMobile     = trim(Yii::$app->request->post('pick_mobile','')); //提货人电话
        $pickCard       = trim(Yii::$app->request->post('pick_card','')); //提货人身份
        $pickLicense    = trim(Yii::$app->request->post('pick_license','')); //提货人车牌号

        $postData['id']      = $pickId;
        $postData['pick_name']      = htmlspecialchars($pickName);
        $postData['pick_mobile']    = $pickMobile;
        $postData['pick_card']      = htmlspecialchars($pickCard);
        $postData['pick_license']   = htmlspecialchars($pickLicense);
        $postData['update_time']    = time();
        //验证数据
        if ($pickId == 0) {
            return self::error('提货人不存在',20005);
        }
        if ($pickName == '') {
            return self::error('提货人姓名不能为空!',20001);
        }
        if ($pickMobile == '') {
            return self::error('提货人电话不能为空!',20002);
        }
        if ($pickCard == '') {
            return self::error('提货人身份证号码不能为空!',20003);
        }
        if ($pickLicense == '') {
            //return self::error('提货人车牌号不能为空!',20004);
        }
        //执行数据更改插入
        $res = PickUser::edit($postData);
        if($res){
            return self::success($res,'编辑成功');
        }
        return self::error('编辑失败');
    }

    public static function del($ids)
    {
        $result = PickUser::upAll($ids);
        if($result){
            return self::success('','删除成功','200');
        }
        return self::error('删除失败');
    }



}
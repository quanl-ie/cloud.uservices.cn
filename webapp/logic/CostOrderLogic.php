<?php
namespace webapp\logic;

use webapp\models\ServiceBrandClass;
use webapp\models\WorkOrder;
use Yii;
use webapp\models\ServiceClassQualification;
use webapp\models\ServiceClass;
use webapp\models\CostOrder;
use webapp\models\BaseModel;
use common\helpers\Helper;
use webapp\models\ServiceTypeQualification;
use webapp\models\BrandQualification;
use webapp\models\SaleOrder;


/*
 *@desc:订单结算列表
 *@author:Eva<chengjuanjuan@uservices.cn>
 *@date:2018-02-22
*/
class CostOrderLogic extends BaseLogic
{

    /*
     * 获取列表数据
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @return type
     */
    public static function getList()
    {
        $query['a.order_no']     = trim(Yii::$app->request->get('order_no'));
        $query['work_type']      = intval(Yii::$app->request->get('type_id'));
        $query['start_time']   = strtotime(Yii::$app->request->get('start_time'));
        $query['end_time']     =  strtotime(Yii::$app->request->get('end_time'));
        $query['f.service_provider_id']      = BaseLogic::getManufactorId();
        $query['currentPage']  = Yii::$app->request->get('page',1);
        $query['pageSize']     = Yii::$app->request->get('pageSize',10);
        $brandId    = intval(Yii::$app->request->get('brand_id'));
        $classId     = intval(Yii::$app->request->get('class_id'));
        if($classId>0 || $brandId>0){
            $saleOrderId = SaleOrder::getIdsByClassId($classId,0,$brandId);
            if(empty($saleOrderId)){
                return false;
            }
            $query['sale_order_id'] = $saleOrderId;
        }
        //确定结算单状态
        $action    = Yii::$app->controller->action->id;
        if($action == 'wait-cost'){
            $query['a.settlement_status'] = 1;
        }elseif($action == 'have-costed'){
            $query['a.settlement_status'] = 2;
        }
        $list = WorkOrder::getList($query);
        return $list;
    }


    //结算订单列表搜索数据提供
    public static function getData()
    {
        //获取服务类型
        $typeList = ServiceTypeQualification::getServiceType();

        //获取二级分类
        $classIds = '';
        $class    = ServiceClassQualification::getList();
        if(!empty($class)){
            foreach($class as $v){
                $classIds .= $v['class_id'].',';
            }
            $classIds  = trim($classIds,',');
            $classList = ServiceClass::getClassName($classIds,'',1);
        }else{
            $classList = '';
        }


        //获取品牌
        $brandList = BrandQualification::getSelfList();


        return [
            'typeList'  => $typeList,
            'classList' => $classList,
            'brandList' => $brandList
        ];
    }




}
<?php
/**
 * Created by PhpStorm.
 * User: xingq
 * Date: 2018/5/14
 * Time: 16:10
 */

namespace webapp\logic;

use common\helpers\Helper;
use common\models\GroupingRelTechnician;
use common\models\ScheduleSet;
use common\models\ScheduleSetOptionDetail;
use Faker\Provider\Base;
use webapp\models\Character;
use webapp\models\BaseModel;
use webapp\models\Grouping;
use Yii;
use webapp\models\Technician;
use common\helpers\ExcelHelper;
use webapp\models\Department;
use webapp\models\Role;

class ScheduleManageLogic extends BaseLogic
{

    /**
     * 列表
     * @author li
     * @date   2018-06-15
     * @return array
     */
    public static function index($dataAuthId)
    {
        //导出标识，排班管理传入1 工作日历导出标识传入2
        $export     = Yii::$app->request->get('export','');

        //分页数据
        $page         = intval(Yii::$app->request->get('page',1));
        $pageSize     = intval(Yii::$app->request->get('pageSize',10));
        if($export){
            $pageSize = 1000;
        }

        //搜索数据
        $groupingId     = Yii::$app->request->get('grouping_id');
        $technicianId   = Yii::$app->request->get('tech_id');
        $date           = Yii::$app->request->get('date',date('Y-m-01'));

        $getDataAuth           = self::getListDataAuth(3,2);
        if(empty($getDataAuth)){
            exit('无数据权限');
        }
        $departmentIds          = $getDataAuth['department_id'];
        $dataAuth               = $getDataAuth['dataAuth'];
        $createUserId           = $getDataAuth['create_user_id']?$getDataAuth['create_user_id']:'';
        $department_name        = $getDataAuth['department_name'];
        //公司账号部门（及以下）权限时
        $selfAuth = self::getDatAuthId();
        if(self::getTopId() == self::getDepartmentId() && ($selfAuth == 4 ||$selfAuth == 5) ){
            $createUserId = self::getLoginUserId();
        }
        //查询所有技师分组
        if(!empty($groupingId) && $groupingId>-1){
            $groWhere = ['b.status'=>1,'b.grouping_id'=>$groupingId];
        }else{
            $groWhere = ['b.status'=>1];
        }

        $db = GroupingRelTechnician::find()->from(Grouping::tableName() . ' as a')
            ->leftJoin([GroupingRelTechnician::tableName() . ' as b'],' a.id = b.grouping_id')
            ->select("b.*,a.name")
            ->where($groWhere);

        //本人权限时，只能调取自己创建的分组或者本部门下的默认组
        //获取本部门没有分组的技师
        $noGrouping = Grouping::getNoGroupTechnician($departmentIds);
        $noGroupingTechnician = array_keys($noGrouping);

       if($createUserId) {
             $db->andWhere(" a.create_user_id = $createUserId");
             $noGroupingTechnician = [];
        }else{
             $db->andWhere(['a.department_id' => $departmentIds]);
        }
        $groupingRelTechnician = $db->asArray()->all();


        //根据分组提取技师
        $techId = array_column($groupingRelTechnician,'technician_id');

        $groupingData = [];
        if ($groupingId != -1) {
            foreach ($groupingRelTechnician as $key => $val) {
                $groupingData[$val['technician_id']]['grouping_id'] = $val['grouping_id'];
                $groupingData[$val['technician_id']]['job_title']   = $val['job_title'];
            }
        }

        $where = [];
        if($technicianId){
            $where[] = ['in','id',$technicianId];
        }else{
            //有分组，但是为默认分组时,排除分组查询出技师
            if (!empty($groupingId) && $groupingId == -1) {
                $where[] = ['not in','id',$techId];
            }elseif (!empty($groupingId) && !empty($techId)) {//当分组存在时
                $where[] = ['in','id',$techId];
            }elseif(!empty($groupingId) && empty($techId)) {
                $where[] = ['=','id',-1];
            }elseif(!$groupingId && empty($groupingRelTechnician)){ //只有默认组时
                $where[] = ['in','id',$noGroupingTechnician];
            }else{
                if($techId){
                    $techId  = array_merge($techId,$noGroupingTechnician);
                }else{
                    $techId = $noGroupingTechnician;
                }

                $where[] = ['in','id',$techId];
            }
            $where[] = ['in','store_id',$departmentIds ];

        }


        //查询出所有技师
        $maps = ['status'=>1,'audit_status'=>5];

        //关联查询分组
        $tech = Technician::getList($maps,$where,$page,$pageSize,$select = "id,name,mobile,store_id");
        $techList = [];
        if ($tech['totalCount'] > 0) {
            foreach ($tech['list'] as $key => $val) {
                $techList[$key]['id'] = $val['id'];
                $techList[$key]['name'] = $val['name'];
                $techList[$key]['mobile'] = $val['mobile'];
                $techList[$key]['store_name'] = Department::getDepartmentName($val['store_id']);
                if (isset($groupingData[$val['id']])) {
                    $techList[$key]['grouping_id'] = $groupingData[$val['id']]['grouping_id'];
                    $techList[$key]['job_title']   = $groupingData[$val['id']]['job_title'];
                }else{
                    $techList[$key]['grouping_id'] = -1;
                    $techList[$key]['job_title']   = 1;
                }
            }
        }

        //判断并组合数据
        if (!empty($tech['list'])) {
            $tech['list'] = self::changeData($techList,$departmentIds,$date);
        }

        //调用导出
        if(!empty($export) && !empty($tech['list'])){
            ScheduleManageLogic::setExport($tech['list'],$export,date('Y-m',strtotime($date)));
        }
        //查询所有分组
        if($createUserId){
            $departmentIds = '';
        }
        $groupData = Grouping::getGroupingAttr($departmentIds);

        //重新组合分组

       foreach ($groupData as $val) {
           if(self::getDatAuthId()!=1){
               $tech['group'][-1]         = '默认组';
           }
            $tech['group'][$val['id']] = $val['name'];
        }

        if (!isset($tech['group'])) {
            $tech['group'] = [];
        }
        $tech['departpment_name'] = $department_name;
        return $tech;
    }

    /**
     * 组合列表所需数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/5/16
     * Time: 11:05
     * @param $data
     * @param $srcId
     * @param $srcType
     * @param $date
     * @return mixed
     */
    public static function changeData($data,$department_id,$date)
    {
        //查询所有分组
        $groupData = Grouping::getGroupingAttr($department_id);
        $group = [];
        //重新组合分组
        foreach ($groupData as $val) {
            $group[-1]         = '默认组';
            $group[$val['id']] = $val['name'];
        }
        if($department_id) {
//            $where['department_id'] = $department_id;
            $where['direct_company_id'] = self::getDirectCompanyId();
        } else {
            $where['oprate_id'] = BaseLogic::getLoginUserId();
        }
        //获取班次
        $set = ScheduleSet::getList($where);
        $setId = '';
        $scheduleSet = [];
        foreach ($set as $val) {
            if ($val['is_delete'] == 1 && $val['name'] == '全天班') {
                $setId = $val['id'];
            }
            $scheduleSet[$val['id']] = $val;
        }
        //匹配分组
        foreach ($data as $key => $val) {
            //匹配分组名称
            if (isset($group[$val['grouping_id']])) {
                $data[$key]['grouping_name'] = $group[$val['grouping_id']];
            }else{
                $data[$key]['grouping_name'] = '默认组';
            }
            //匹配职位
            if ($val['job_title'] == 1) {
                $data[$key]['job_name'] = '职员';
            } elseif ($val['job_title'] == 2) {
                $data[$key]['job_name'] = '组长';
            }
        }

        //技师排班数据
        //获取当前月份天数
        $startTime = strtotime(date($date));
        $days = date("t",$startTime);
        //获取技师id
        $techId = array_column($data,'id');
        //获取排班数据
        $setOption = ScheduleSetOptionDetail::getSetOptionDetail($techId,$department_id,$startTime,$days);
        $week = [
            1 => '周一',
            2 => '周二',
            3 => '周三',
            4 => '周四',
            5 => '周五',
            6 => '周六',
            7 => '周日',
        ];

        //将班次修改为关联数组存储数据
        $scheduleSetData = [];

        foreach ($scheduleSet as $key => $val) {
            $scheduleSetData[$val['id']] = $val['name'];
        }

        //组合排班数据
        $result = [];
        foreach ($data as $key => $val) {
            $result[$val['id']] = $val;

            for($i=0;$i<$days;$i++){
                $day = date('Y-m-d',$startTime+$i*86400);
                $result[$val['id']]['child'][$i]['date']  = $day;
                $result[$val['id']]['child'][$i]['day']   = date('j',$startTime+$i*86400).'日';
                $result[$val['id']]['child'][$i]['week']  = $week[date('N',$startTime+$i*86400)];

                if (isset($setOption[$val['id']][$day]))
                {
                    $result[$val['id']]['child'][$i]['description']       = $setOption[$val['id']][$day]['description'];
                    $result[$val['id']]['child'][$i]['finsh_order']       = isset($setOption[$val['id']][$day]['finsh_order']) ? $setOption[$val['id']][$day]['finsh_order'] : 0;
                    $result[$val['id']]['child'][$i]['all_order']         = isset($setOption[$val['id']][$day]['all_order']) ? $setOption[$val['id']][$day]['all_order'] : 0;

                }else {
                    $result[$val['id']]['child'][$i]['description'] = '';
                    $result[$val['id']]['child'][$i]['finsh_order'] = 0;
                    $result[$val['id']]['child'][$i]['all_order'] = 0;
                }

                //判断班次是否存在
                if (isset($setOption[$val['id']][$day]['schedule_set_id']))
                {
                    $result[$val['id']]['child'][$i]['schedule_set_id']   = $setOption[$val['id']][$day]['schedule_set_id'];
                    $result[$val['id']]['child'][$i]['schedule_set_name'] = isset($scheduleSet[$setOption[$val['id']][$day]['schedule_set_id']]['name']) ? $scheduleSet[$setOption[$val['id']][$day]['schedule_set_id']]['name'] : "";
                    $result[$val['id']]['child'][$i]['color']             = isset($scheduleSet[$setOption[$val['id']][$day]['schedule_set_id']]['color']) ? $scheduleSet[$setOption[$val['id']][$day]['schedule_set_id']]['color'] : '';
                    $result[$val['id']]['child'][$i]['order_total_info']  = $result[$val['id']]['child'][$i]['schedule_set_name'].$result[$val['id']]['child'][$i]['finsh_order'].'/'.$result[$val['id']]['child'][$i]['all_order'];

                }else {
                    $result[$val['id']]['child'][$i]['schedule_set_id']   = $setId;
                    $result[$val['id']]['child'][$i]['schedule_set_name'] = isset($scheduleSet[$setId]['name']) ? $scheduleSet[$setId]['name'] : "全天班";
                    $result[$val['id']]['child'][$i]['color']             = isset($scheduleSet[$setId]['color']) ? $scheduleSet[$setId]['color'] : '' ;
                    $result[$val['id']]['child'][$i]['order_total_info']  = $result[$val['id']]['child'][$i]['schedule_set_name'].$result[$val['id']]['child'][$i]['finsh_order'].'/'.$result[$val['id']]['child'][$i]['all_order'];
                }

            }
        }

        //计算总单量
        foreach ($result as $key => $val) {
            $result[$key]['finsh_order'] = array_sum(array_column($val['child'],'finsh_order'));
            $result[$key]['all_order']   = array_sum(array_column($val['child'],'all_order'));
        }

        //数组重新排序
        return array_values($result);
    }

    //导出数据处理 2018-5-18 sxz
    public static function setExport($data,$export,$date){
        if($data){
            //组合表头数据开始
            $dateList = $data[0];
            foreach ($dateList as $key=>$val){
                if(!is_array($val)){
                    unset($dateList[$key]);
                }
            }
            $dateList = $dateList['child'];
            $dates = array_column($dateList,'date');
            $weeks = array_column($dateList,'week');
            $arr = [];
            foreach ($dates as $key => $val) {
                $arr[$val] = $val.$weeks[$key];
            }

            $titleList = array('name'=>'技师','grouping_name'=>'组别','job_name'=>'职位');
            $titleList = array_merge($titleList,$arr);
            //导出标识为2  工作日历
            if($export=='2'){
                $order_total = array('order_total'=>'工单合计');
                $titleList = array_merge($titleList,$order_total);
            }
            foreach ($titleList as $key=>$val){
                $cells[$key]['key']    = $key;
                $cells[$key]['value']  = $val;
                $cells[$key]['width']  = 30;
            }
            $cells = array_values($cells);
            //组合表内容数据开始
            $temp_key = array_column($data,'id');  //键值
            $new_arr_list = array_combine($temp_key,$data) ;
            foreach ($new_arr_list as $k=>$v){
                $v['a'] = [];
                $v['b'] = [];
                $v['c'] = [];
                $new_arr_list[$k] = '';
                $v['a'] = array_column($v['child'],'date');
                if($export=='1'){  //导出标识为1，排班管理
                    $v['b'] = array_column($v['child'],'schedule_set_name');
                }
                if($export=='2'){  //导出标识为2，工作日历
                    $v['b'] = array_column($v['child'],'order_total_info');
                }
                if(empty($v['a']) || empty($v['b'])){
                    return self::error('导出失败！');
                }
                $v['c'] = array_combine($v['a'],$v['b']);
                unset($v['a']);
                unset($v['b']);
                $new_arr_list[$k] = $v['c'];
                $new_arr_list[$k]['name'] = $v['name'];
                $new_arr_list[$k]['grouping_name'] = isset($v['grouping_name'])?$v['grouping_name']:'';
                $new_arr_list[$k]['job_name'] = $v['job_name'];
                if($export=='2'){  //导出标识为2，工作日历
                    $new_arr_list[$k]['order_total'] = $v['finsh_order'].'/'.$v['all_order'];
                }
            }
            $title = $sheet_title = $date.'月份班次管理报表';
            if($export=='2'){
                $title = $sheet_title = $date.'月份工作日历报表';
            }

            ExcelHelper::CreateExcel($cells,$new_arr_list,$title,$sheet_title);   //调用导出数据方法
        }
        return self::error('导出失败！');

    }
    
    
    /**
     * 获取机构分组
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/8/14
     * Time: 17:01
     * @return array
     */
    public static function getGroup()
    {
        $departmentIds = Yii::$app->request->get('department_ids','');
        
        if ($departmentIds == '') {
            return self::error('所选机构不能为空',20001);
        }
        
        $group = Grouping::find()->where(['in','department_id',$departmentIds])->asArray()->all();
    
        foreach ($group as $val) {
            if(self::getDatAuthId()!=1){
                $data[-1]         = '默认组';
            }
            $data[$val['id']] = $val['name'];
        }
        
        if (!isset($data)) {
            $data = [];
        }
        return self::success($data,'成功');
    }
}

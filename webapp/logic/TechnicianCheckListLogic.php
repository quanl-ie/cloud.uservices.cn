<?php
namespace webapp\logic;
use webapp\models\AdminUser;
use webapp\models\CheckListDetail;
use webapp\models\TechnicianCheckList;
use webapp\models\TechnicianCheckListCost;
use webapp\models\TechnicianCheckListDetail;
use Yii;
use webapp\models\Technician;
use webapp\models\BaseModel;
use common\helpers\Helper;
use yii\web\User;
use webapp\models\TechnicianBalance;


class TechnicianCheckListLogic extends BaseLogic
{
    
    /**
     * 获取列表数据
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @return type
     */
    public static function getList()
    {
        $query['company']     = trim(Yii::$app->request->get('company'));
        $query['status']      = intval(Yii::$app->request->get('status'));

        $query['currentPage']  = Yii::$app->request->get('page',1);
        $query['pageSize']     = Yii::$app->request->get('pageSize',10);
        $query['src_type']     = BaseModel::SRC_FWS;
        $query['src_id']       = self::getManufactorId();

        $action    = Yii::$app->controller->action->id;
        if($action == 'wait-check'){
            $query['a.status'] = [1];
        }elseif($action == 'wait-cost'){
            $query['a.status'] = 2;
        }else{
            $query['a.status'] = 3;
        }

        $list = TechnicianCheckList::getList($query);
        foreach($list['list'] as &$v){
            if($v['cost_oprate_id']){
                $admin = AdminUser::getOne(['id'=>$v['cost_oprate_id']]);
                $v['cost_oprate_id'] = $admin->username;
            }
            if($v['check_id']){
                $admin = AdminUser::getOne(['id'=>$v['check_id']]);
                $v['check_name'] = $admin->username;
            }
        }
        return $list;
    }
    /**
     * 获取添加编辑列表数据
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @return type
     */
    public static function addList()
    {
        $data = [];
        $costItemSetList = CostItemSet::getList();
        $data['list'] = [];
        if ($costItemSetList) {
            $data['list'] = $costItemSetList;
        }
        $costItemIds = [];
        $costItemList = CostItem::getList(['status'=>1]);
        foreach ($costItemList as $val) {
            $costItemIds[] = $val->cs_id;
        }
        $data['ids'] = [];
        if ($costItemIds) {
            $data['ids'] = $costItemIds;
        }
        return $data;
    }
    /**
     * 数据添加和编辑
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function updateStatus()
    {
        $checklistId = trim(Yii::$app->request->post('checklist_ids'),',');

        $checklistIds = explode(',',$checklistId);
        $res = TechnicianCheckList::updateStatus($checklistIds);
        if ($res) {
            return self::success($res,'成功');
        }
        return self::error('失败');
    }



    //获取详情
    public  static function getDetail(){
        $id          = intval(Yii::$app->request->get('id'));
        $info        = TechnicianCheckList::getOne(['id'=>$id]);
        $technician  = Technician::getOne(['id'=>$info['tech_id']]);
        $info['technician_name'] = $technician['name'];
        $info['mobile']          = $technician['mobile'];
        if(isset($info['report_id'])){
            $user = AdminUser::getOne(['id'=>$info['report_id']]);
            $info['report_name'] = $user->username;
        }
        if(isset($info['check_id'])){
            $check = AdminUser::getOne(['id'=>$info['check_id']]);
            $info['check_name'] = $check->username;
        }
        if(isset($info['cost_oprate_id'])){
            $check = AdminUser::getOne(['id'=>$info['cost_oprate_id']]);
            $info['cost_name'] = $check->username;
        }
        $costItemData   = TechnicianCheckListCost::getList(['checklist_id'=>$id]);
        $costItemList = [];
        foreach($costItemData as $v){
            if ($v['cost_id'] == $v['cost_id']) {
                $data[$v['cost_id']][] = $v;
            }
        }
        foreach($data as $k => $v){
            $amount = array_column($v,'amount');
            $costItemList[$k]['amount'] = array_sum($amount);
            $costItemList[$k]['cost_name'] = $v[0]['cost_name'];
        }

        $workList    = TechnicianCheckListDetail::getList(['checklist_id'=>$id]);
        return [
            'info'         => $info,
            'costItemList' => $costItemList,
            'workList'     => $workList
        ];
    }



    /**
     * 区分需要添加或编辑的数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $cs_id
     * @return type
     */
    public static function getSaveData($cs_id)
    {
        $data = [];
        //所有数据
        $res = CostItemSet::getList();
        //数据库id
        $oldIds = self::getCostItemIds();
        //获取需要删除的id
        $del = array_diff($oldIds, $cs_id);

        //获取需要添加的id
        $add = array_diff($cs_id, $oldIds);

        //获取需要修改的id
        $edit = array_intersect($cs_id, $oldIds);

        //组合数据
        if ($res) {
            foreach ($res as $key => $val) {
                if (in_array($val['id'], $add)) {
                    $data['add'][$key]['src_type']    = BaseModel::SRC_FWS;
                    $data['add'][$key]['src_id']      = self::getManufactorId();
                    $data['add'][$key]['cs_id']       = $val['id'];
                    $data['add'][$key]['cs_name']     = $val['item_name'];
                    $data['add'][$key]['create_time'] = $data['add'][$key]['update_time'] = time();
                    $data['add'][$key]['status']      = 1;
                    $data['add'][$key]['oprate_id']   = Yii::$app->user->getIdentity()->id;
                }
                if (in_array($val['id'], $edit)) {
                    $data['edit'][$key]['cs_id'] = $val['id'];
                    $data['edit'][$key]['cs_name'] = $val['item_name'];
                    $data['edit'][$key]['update_time'] = time();
                    $data['edit'][$key]['status']      = 1;
                    $data['edit'][$key]['oprate_id'] = Yii::$app->user->getIdentity()->id;
                }
                if (in_array($val['id'], $del)) {
                    $data['del'][$key]['cs_id'] = $val['id'];
                    $data['del'][$key]['update_time'] = time();
                    $data['del'][$key]['status']      = 2;
                    $data['del'][$key]['oprate_id'] = Yii::$app->user->getIdentity()->id;
                }
            }
        }
        return $data;
    }
    /**
     * 获取已存在的收费项目 cs_id
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function getCostItemIds()
    {
        $res = CostItem::getList();
        $ids = [];
        foreach ($res as $key => $val) {
            $ids[] = $val['cs_id'];
        }
        return $ids;
    }

    //查询按钮带出数据
    public static function getData(){
        $startTime = Yii::$app->request->post('startTime');
        $endTime = Yii::$app->request->post('endTime');

        $url = Yii::$app->params['order.uservices.cn'].'/v1/work/settlement-work';
        $postData = [
                    'begin_time' => $startTime,
                    'end_time'   => $endTime,
                    'src_id'     => BaseLogic::getManufactorId(),
                    //'src_id'=>12,
                    'src_type'   => BaseModel::SRC_FWS
                ];
        $jsonStr  = Helper::curlPostJson($url, $postData);
        $jsonData = json_decode($jsonStr,true);
        $data     = isset($jsonData['data']) ? $jsonData['data'] : [];
        $dataList = [];
        if(!empty($data)){
            $data['begin_time'] = strtotime($startTime);
            $data['end_time']   = strtotime($endTime);
            $dataList = TechnicianCheckList::add($data);
        }
        return json_encode($dataList);
    }


    public static function check($status=2){
        $detail       = Yii::$app->request->post('detail');
        $checkListId  = Yii::$app->request->post('checklist_id');
        $amountReal   =   Yii::$app->request->post('amount_real');
        $res          =  TechnicianCheckListDetail::saveDetail($checkListId,$status,$detail,$amountReal);
        if ($res) {
            $list = TechnicianCheckListDetail::getList(['in','checklist_id',$checkListId]);
            if($status==1){ //结算单申请成功时候，修改工单结算状态
                $workIds = array_column($list,'work_id');
                TechnicianCheckList::updateOrderSettlementStatus($workIds);
            }
            if($status==3){ //结算单完成时，balance表更新数据
                $techId = $list[0]['technician_id'];
                $checkLIstInfo = TechnicianCheckList::getOne(['id'=>$checkListId]);
                $amount = $checkLIstInfo['amount_real'];
                TechnicianBalance::updateData($techId,$amount,count($list));
            }
            return self::success($res,'成功');

        }
        return self::error('失败');

    }
}
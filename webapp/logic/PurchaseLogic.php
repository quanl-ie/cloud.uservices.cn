<?php
namespace webapp\logic;


use common\models\User;
use Yii;
use webapp\models\BaseModel;
use common\helpers\Helper;
use webapp\models\Region;
use common\models\Supplier;
use common\models\DictEnum;
use common\helpers\CreateNoRule;
use common\models\Purchase;
use common\models\PurchaseDetail;
use common\models\StockInfo;
use common\models\ReturnPurchase;
use common\models\ReturnPurchaseDetail;
use common\models\SendOut;
/**
 * 供货商管理
 * @author sxz
 * @date 2018-03-21
 */
class PurchaseLogic extends BaseLogic
{
    /**
     * 数据列表
     * @author sxz
     * @return string
     */
    public static function getList()
    {
        $params = [];
        $where = [];
        $params['audit_status']         = $audit_status = Yii::$app->request->get('audit_status',''); //审核状态
        $params['no']                   = $no = trim(Yii::$app->request->get('no','')); //采购编号
        $params['subject']              = $subject = trim(Yii::$app->request->get('subject','')); //采购主题
        $params['supplier_name']        = $supplier_name = trim(Yii::$app->request->get('supplier_name','')); //供货商名字
        $params['type']                 = $type = Yii::$app->request->get('type',''); //采购类型
        $params['status']               = $status = Yii::$app->request->get('status',''); //采购状态
        $params['isship']               = $isship = Yii::$app->request->get('isship',''); //收货状态
        $params['mix_status']           = $mix_status = Yii::$app->request->get('mix_status',''); //待入库时的采购状态
        $params['purchase_user_name']   = $purchase_user_name = trim(Yii::$app->request->get('purchase_user_name','')); //采购人员
        $params['start_time']           = $start_time   = trim(Yii::$app->request->get('start_time',''));
        $params['end_time']             = $end_time     = trim(Yii::$app->request->get('end_time',''));
        $params['page']                 = $page = intval(Yii::$app->request->get('page',1));
        $params['pageSize']             = $pageSize = intval(Yii::$app->request->get('pageSize',10));
        $maps = ['direct_company_id'=>BaseLogic::getDirectCompanyId()];
        $action    = Yii::$app->controller->action->id;
        if($action == 'index'){  //待审批的
            $audit_status = '1';
            $status = '3';
            $where[] = ['between','purchase_date',$start_time,$end_time];
        }elseif($action == 'wait-check-in'){  //待入库的
            $audit_status = '2';
            $status = [2,3];
            if($mix_status == '2'){ //部分入库
                $status = '2';
            }elseif($mix_status == '3'){  //待入库
                $status = '3';
            }
            $where[] = ['between','audit_date',$start_time,$end_time];
        }elseif($action == 'finished'){   //入库完毕的
            //$status = '3';
            $status = [1,6];  //状态1 全部入库，6部分入库终止
            $audit_status = '2';
            $where[] = ['between','time_stamp',$start_time,$end_time];
        }elseif($action == 'not-pass'){ //未通过的
            $audit_status = '3';
            $where[] = ['between','audit_date',$start_time,$end_time];
        }elseif($action == 'cancelled'){ //已取消的
            $status = '4';
            $where[] = ['between','time_stamp',$start_time,$end_time];
        }elseif($action == 'invalid'){ //已作废的
            $status = '5';
            $where[] = ['between','time_stamp',$start_time,$end_time];
        }
        //print_r($maps);die;
        //搜索条件
        if (yii::$app->request->isGet) {
            if ($subject != '') {
                $where[] = ['like','subject',$subject];
            }
            if ($supplier_name != '') {
                $where[] = ['like','supplier_name',$supplier_name];
            }
            if ($no != '') {
                $maps['no'] = $no;
            }
            if($type != ''){
                $maps['type'] = $type;
            }
            if($status != ''){
                $maps['status'] = $status;
            }
            if($isship != ''){
                $maps['isship'] = $isship;
            }
            if($audit_status != ''){
                $maps['audit_status'] = $params['audit_status'] = $audit_status;
            }
            if($purchase_user_name != ''){
                $maps['purchase_user_name'] = $purchase_user_name;
            }
        }
        $data = Purchase::getIndexList($maps,$where,$page,$pageSize);
        //已完成的采购单列表 去查询是否可以退货
        if($data['list'] && $action == 'finished')
        {
            $ids = array_column($data['list'],'id');   //采购单主表ids
            $detailsArr = PurchaseDetail::getList(['parent_id'=>$ids],1);  //获取采购单详情产品数量
            $detailsArrNum = array_column($detailsArr,'total_prod_num','parent_id');
            foreach ($data['list'] as $key=>$val) {
                $data['list'][$key]['total_prod_num'] = $detailsArrNum[$val['id']];
            }
            $returnData = ReturnPurchase::getAllList(['purchase_id'=>$ids,'status'=>[1,2,3],'del_status'=>2]);
            foreach($data['list'] as $k1=>$v1){
                $data['list'][$k1]['is_return'] = 1;
                if($returnData){
                    foreach($returnData as $k=>$v){
                        if($v1['id'] == $k){
                            if($v1['total_prod_num'] >$v['prod_num']){
                                $data['list'][$k1]['is_return'] = 1;
                            }else{
                                $data['list'][$k1]['is_return'] = 0;
                            }
                        }
                    }
                }
            }
        }
        $data['params'] = $params;
        //获取采购类型数据
        $data['purchaseTypeList'] = self::getPurchaseTypeList();
        //获取采购状态数据
        $data['purchaseStatusList'] = self::getPurchaseStatusList();
        $data['mixStatusList'] = ['2'=>'部分入库','3'=>'待入库',];  //待入库选项卡时需要区分的采购状态
        //获取审批状态数据
        $data['auditStatusList'] = self::getAuditStatusList();
        //print_r($data);die;
        return $data;
    }
    /**
     * 数据添加和编辑
     */
    public static function add()
    {
        $data = Yii::$app->request->post();
        //print_r($data);die;
        $postData['main']['subject']            = $subject = trim(Yii::$app->request->post('subject','')); //采购主题
        $postData['main']['no']                 = $no = trim(Yii::$app->request->post('no',''));  //采购编号
        $postData['main']['type']               = $type = intval(Yii::$app->request->post('type',0));  //采购类型
        $postData['main']['supplier_id']        = $supplier_id = intval(Yii::$app->request->post('supplier_id',''));  //供货商id
        $postData['main']['supplier_name']      = $supplier_name = trim(Yii::$app->request->post('supplier_name',''));  //供货商名称
        $postData['main']['purchase_user_id']   = $purchase_user_id = intval(Yii::$app->request->post('purchase_user_id',0));  //采购人id
        $postData['main']['purchase_user_name'] = trim(Yii::$app->request->post('purchase_user_name',''));  //采购人名称
        $postData['main']['description']        = htmlspecialchars(trim(Yii::$app->request->post('description','')));  //备注
        //采购产品数据
        $postData['addition']                   = Yii::$app->request->post('prod',[]);
        $postData['main']['direct_company_id']  = BaseLogic::getDirectCompanyId();
        $postData['main']['purchase_date']      = date('Y-m-d H:i:s',time()); //采购时间
        $postData['main']['status']             = 3;  //默认状态
        $postData['main']['audit_status']       = 1;  //默认待审批状态
        //$postData['main']['total_all_amount']   = trim(Yii::$app->request->post('total_all_amount',''));  //采购单总额
        //验证数据
        if ($subject == '') {
            return self::error('采购主题不能为空!',20001);
        }
        if ($supplier_id == 0) {
            return self::error('供货商不能为空!',20002);
        }
        if ($no == '') {
            return self::error('采购编号不能为空!',20002);
        }
        if ($type == 0) {
            return self::error('采购类型不能为空!',20002);
        }
        $num = array_sum(array_column($postData['addition'],'prod_num'));
        foreach ($postData['addition'] as $k=>$v){
            if(!isset($v['prod_num'])){
                unset($postData['addition'][$k]);
            }
            if(isset($v['prod_num']) && $v['prod_num'] == 0){
                unset($postData['addition'][$k]);
            }
        }
        if($num == 0){
            return self::error('产品数量不能为空',20003);
        }
        //执行数据插入
        $res = Purchase::add($postData);
        if($res){
            return self::success($res,'创建成功');
        }
        return self::error('创建失败');
    }
    /**
     * 函数用途描述:采购单编辑
     * @date: 2018年3月27日 10:27:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function edit(){
        $data = Yii::$app->request->post();
        //print_r($data);die;
        $postData['main']['id']                 = $id = intval(Yii::$app->request->post('id',0)); //采购主题
        $postData['main']['subject']            = $subject = trim(Yii::$app->request->post('subject','')); //采购主题
        $postData['main']['no']                 = $no = trim(Yii::$app->request->post('no',''));  //采购编号
        $postData['main']['type']               = $type = intval(Yii::$app->request->post('type',0));  //采购类型
        $postData['main']['supplier_id']        = $supplier_id = intval(Yii::$app->request->post('supplier_id',0));  //供货商id
        $postData['main']['supplier_name']      = $supplier_name = trim(Yii::$app->request->post('supplier_name',''));  //供货商名称
        $postData['main']['purchase_user_id']   = $purchase_user_id = intval(Yii::$app->request->post('purchase_user_id',0));  //采购人id
        $postData['main']['purchase_user_name'] = trim(Yii::$app->request->post('purchase_user_name',''));  //采购人名称
        $postData['main']['description']        = htmlspecialchars(trim(Yii::$app->request->post('description','')));  //备注
        //采购产品数据
        $postData['addition']                   = Yii::$app->request->post('prod',[]);
        $postData['main']['direct_company_id']             = $org_id   = BaseLogic::getDirectCompanyId();
        //验证数据
        if ($subject == '') {
            return self::error('采购主题不能为空!',20001);
        }
        if ($supplier_id == 0) {
            return self::error('供货商不能为空!',20002);
        }
        if ($no == '') {
            return self::error('采购编号不能为空!',20002);
        }
        if ($type == 0) {
            return self::error('采购类型不能为空!',20002);
        }
        $num = array_sum(array_column($postData['addition'],'prod_num'));
        foreach ($postData['addition'] as $k=>$v){
            if($v['prod_num'] == 0){
                unset($postData['addition'][$k]);
            }
        }
        if($num == 0){
            return self::error('产品数量不能为空',20003);
        }
        //执行数据更改插入
        $res = Purchase::edit($postData);
        if($res){
            return self::success($res,'编辑成功');
        }
        return self::error('编辑失败');
    }
    /**
     * 函数用途描述:获取采购单详情
     * @date: 2018年3月21日 下午16:07:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:$type 1 采购单id    2采购单号
     */
    public static function getDetail($no,$type)
    {
        $data = [];
        if($no){
            if($type == 1){
                $Purchase_info = Purchase::getOne(['id'=>$no,'direct_company_id'=>BaseLogic::getDirectCompanyId()]);
            }
            if($type == 2){
                $Purchase_info = Purchase::getOne(['no'=>$no,'direct_company_id'=>BaseLogic::getDirectCompanyId()]);
            }
            if(!$Purchase_info){
                return $data;
            }
            //获取采购单详情产品数量
            $detailsArr = PurchaseDetail::getList(['parent_id'=>$Purchase_info['id']],1);
            $total_prod_num = array_column($detailsArr,'total_prod_num','parent_id');
            $Purchase_info['total_prod_num'] = isset($total_prod_num[$Purchase_info['id']])?$total_prod_num[$Purchase_info['id']]:0;
            $returnData = ReturnPurchase::getAllList(['purchase_id'=>$Purchase_info['id'],'status'=>[1,2,3]]);
            $returnNum  = isset($returnData[$Purchase_info['id']]['prod_num'])?$returnData[$Purchase_info['id']]['prod_num']:0;
            if($Purchase_info['total_prod_num'] > $returnNum){
                $Purchase_info['is_return'] = 1;
            }else{
                $Purchase_info['is_return'] = 0;
            }
            //获取采购类型数据
            $purchaseTypeList = self::getPurchaseTypeList();
            //获取采购明细数据
            $detail = self::getProduct($Purchase_info['id']);
            /*if ($detail['code'] != 200) {
                return self::error('获取产品明细失败');
            }*/
            $where = [
                'direct_company_id' => BaseLogic::getDirectCompanyId(),
                'rel_id' => $Purchase_info['id'],
                'type'=>1,
                'is_rel'=>1,
                'rel_type'=>1,
                'audit_status'=>[1,2]
            ];
            //获取入库记录
            $store_record = StockInfo::getList($where);
            if ($Purchase_info) {
                $data['purchaseInfo'] = $Purchase_info;
                $data['purchaseInfo']['purchase_type_name'] = isset($purchaseTypeList[$Purchase_info['type']])?$purchaseTypeList[$Purchase_info['type']]:'';
                $data['purchaseInfo']['audit_user_name'] = self::getAuditUserInfo($Purchase_info['audit_user_id']);
            }else{
                $data['purchaseInfo'] = [];
            }
            if ($detail) {
                $data['purchaseDetail'] = $detail['data'];
            }else{
                $data['purchaseDetail'] = [];
            }
            if ($store_record) {
                $data['storeRecord'] = $store_record;
            }else{
                $data['storeRecord'] = [];
            }
            //获取收货记录
            $sendList = SendOut::findAllByAttributes(['stock_id'=>$Purchase_info['id'],'send_source'=>1],'id,stock_id,send_no,send_status,create_time,rel_rec_date','stock_id');
            if ($sendList) {
                $data['sendRecord'] = $sendList;
            }else{
                $data['sendRecord'] = [];
            }
            //获取退货记录
            $return_record = ReturnPurchase::getAllList(['purchase_id'=>$Purchase_info['id'],'status'=>[1,2,3]],1);
            if ($return_record) {
                $data['returnRecord'] = $return_record;
            }else{
                $data['returnRecord'] = [];
            }
            return $data;
        }
        return $data;
    }
    //获取采购类型数据
    public static function getPurchaseTypeList(){
        //$data = DictEnum::getList(['dict_key'=>'enum_purchase_type','direct_company_id'=>BaseLogic::getDirectCompanyId()]);
        $data = DictEnum::getList(['dict_key'=>'enum_purchase_type']);
        if($data){
            $data = array_column($data,'dict_enum_value','dict_enum_id');
        }
        return $data;
    }
    //获取采购状态数据
    public static function getPurchaseStatusList(){
        $data = DictEnum::getList(['dict_key'=>'enum_purchase_status']);
        return $data;
    }
    //获取采购审批状态数据,部分入库或待入库
    public static function getAuditStatusList(){
        $data = DictEnum::getList(['dict_key'=>'enum_audit_status']);
        return $data;
    }
    /**
     * 取消
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function cancel($no,$opinionContent)
    {
        $res = Purchase::findOne(['no'=>$no,'audit_status'=>'1']);
        if($res){
            $res->audit_suggest = htmlspecialchars($opinionContent);
            $res->status = 4;
            $save = $res->save(false);
            if($save){
                return self::success('','取消成功！');
            }
            return self::error('取消失败！',20004);
        }
        return self::error('取消失败!',20004);
    }
    /**
     * 获取产品明细数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/3/23
     * Time: 17:29
     * @param $id
     * @return array
     */
    public static function getProduct($id)
    {
        $where = [
            ['a.direct_company_id' => BaseLogic::getDirectCompanyId()],
            ['a.parent_id' => $id]
        ];
        $product = PurchaseDetail::getProduct($where);
        if (!$product) {
            return self::error('获取产品明细失败');
        }

        //调用产品逻辑层组合数据方法
        $productData = ProductLogic::changeData($product, 1);
        if (!$productData) {
            return self::error('获取产品明细失败');
        }
        //单位
        $unit = ProductLogic::getUnit();
        if ($unit) {
            foreach ($productData as $key => $val) {
                if (isset($unit[$val['unit_id']])) {
                    $productData[$key]['unit_name'] = $unit[$val['unit_id']];
                }else{
                    $productData[$key]['unit_name'] = '';
                }
            }
        }
        return self::success($productData, '成功');
    }

    /**
     * @desc: 操作审核状态
     * @param $no
     * @param $status
     * @param $opinionContent
     * @return array
     */
    public static function SetAuditStatus($no,$status,$opinionContent)
    {
        $res = Purchase::findOne(['no'=>$no,'audit_status'=>'1']);
        if($res){
            $res->audit_suggest = htmlspecialchars($opinionContent);
            $res->audit_status  = $status;
            $res->audit_user_id = BaseLogic::getLoginUserId();
            $res->audit_date    = date('Y-m-d H:i:s',time());
            $save = $res->save(false);
            if($save){
                return self::success('','审批成功！');
            }
            return self::error('审批失败！',20004);
        }
        return self::error('审批失败!',20004);
    }
    /**
     * @desc: 采购单终止/作废
     * @param $id
     * @param $status  // 6 采购终止 5 作废
     * @return array
     */
    public static function SetStop($id,$status)
    {
        $res = Purchase::findOne(['id'=>$id,'audit_status'=>'2']);
        //print_r($res);die;
        if($res){
            $res->status  = $status;
            $save = $res->save(false);
            if($save){
                //终止时，申请采购的数量需要减去已经发货完成的数量
                if($status == 5){
                    $updata = PurchaseDetail::updateAll(['activity_prod_num' => 0], ['parent_id' => $id]);
                }
                if($status == 6){
                    //更改库存已申请的采购数量
                    $detailList = PurchaseDetail::getList(['parent_id'=>$id]);
                    if($detailList){
                       foreach($detailList as $k=>$v){
                           $activity_prod_num = $v['prod_num'] - $v['finish_num'];
                           $model = PurchaseDetail::findOne(['id'=>$v['id']]);
                           $model->activity_prod_num = $activity_prod_num;
                           $model->save();
                       }
                    }
                }
                return self::success('','成功！');
            }
            return self::error('失败！',20004);
        }
        return self::error('失败!',20004);
    }
    /**
     * 获取审批人信息
     * @param $id
     * @return array
     */
    public static function getAuditUserInfo($id)
    {
        $data = '';
        if($id > 0 ){
            $data = User::findOne(['id'=>$id])->username;
        }
        return $data;
    }



}
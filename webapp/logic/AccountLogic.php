<?php
namespace webapp\logic;
use common\components\Upload;
use common\helpers\Gaode;
use common\models\Product;
use common\models\User;
use webapp\models\AccountAddress;
use webapp\models\ProductMaterials;
use webapp\models\Region;
use yii;
use webapp\models\Account;
use webapp\models\Department;
use common\helpers\Helper;
use common\models\ServiceType;
use webapp\models\SaleOrder;

class AccountLogic extends BaseLogic
{
    
    //列表
    public static function index()
    {
        
        $accountName   = trim(Yii::$app->request->get('account_name',''));
        $accountMobile = trim(Yii::$app->request->get('mobile',''));
        $department_id   = Yii::$app->request->get('department_ids','');
        $accountAddress   = Yii::$app->request->get('account_address','');
        $createStarTime   = trim(Yii::$app->request->get('create_start_time',''));
        $createEndTime = trim(Yii::$app->request->get('create_end_time',''));
        $page          = intval(Yii::$app->request->get('page',1));
        $pageSize      = intval(Yii::$app->request->get('pageSize',10));
        $auth = self::getTopDataAuth();
        $top_id = self::getDirectCompanyId();
        $type = self::getDepartmentType();
        $default_department_id = $department_id;
        //初始化搜索条件
        if($auth == 3){
            //获取上级及以下所有机构；
            $department_info = department::getDepartmentChildren($top_id,3,0);
            foreach ($department_info as $val){
                $department_ids[] = $val['id'];
            }
            $map['a.department_id']   = $department_ids;
        }else if($auth == 2){
            //获取上级及以下部门
            $department_info = department::getDepartmentChildren($top_id,0,1);
            foreach ($department_info as $val){
                $department_ids[] = $val['id'];
            }
            $map['a.department_id']   = $department_ids;
        }else if($auth == 5){
              if($type == 3){
                  //获取部门以下部门
                  if($department_id == ''){
                      $department_info = department::getDepartmentChildren(self::getDepartmentId(),2);
                  }else{
                      $department_info = department::getDepartmentChildren($department_id,2);
                  }

                  foreach ($department_info as $val){
                      $department_ids[] = $val['id'];
                  }
                  $map['a.department_id']   = $department_ids;
              }else{
                  $map = [];
                  $map['a.create_user'] = self::getLoginUserId();
              }
        }else if($auth == 4){
            if($type == 3){
                //获取部门
                if($department_id == ''){
                    $map['a.department_id']   = self::getDepartmentId();
                }else{
                    $map['a.department_id']   = $department_id;
                }
            }else{
                $map = [];
                $map['a.create_user'] = self::getLoginUserId();
            }

        }else{
            $map = [];
            $map['a.create_user'] = self::getLoginUserId();
        }

        //搜索条件
        $params = [];
        $where = [];

        $params['account_name'] = $accountName;
        $params['mobile'] = $accountMobile;
        $params['department_ids'] = $department_id;
        $params['account_address'] = $accountAddress;
        $params['create_start_time'] = $createStarTime;
        $params['create_end_time'] = $createEndTime;
        if (yii::$app->request->isGet) {
            if($accountAddress != ''){
                $params['account_address'] = $accountAddress;
                $accountId = AccountAddress::findAllByAttributes(['like','address',$accountAddress],'account_id');
                $accountIdArr = array_column($accountId,'account_id');
                $where[] = ['in','a.id',$accountIdArr];
            }
            if ($accountName != '') {
                $params['account_name'] = $accountName;
                $where[] = ['like','a.account_name',$accountName];
            }
            if ($accountMobile != 0) {
                $params['mobile'] = $accountMobile;
                $where[] = ['like','a.mobile',$accountMobile];
            }
            if($department_id != ''){
                $department_ids = explode(',',$department_id);
                if (!empty($department_ids)) {
                    foreach ($department_ids as $val){
                        if(Department::getDirectCompanyIdById($val) != $top_id){
                            $department_info = department::getDepartmentChildren($val,0,1);
                            foreach ($department_info as $v){
                                $department_ids[] = $v['id'];
                            }
                        }
                    }
                    $params['department_id'] = $default_department_id;
                    $map['a.department_id']   = $department_ids;
                }
            }

            if ($createStarTime != '') {
                $params['create_time'] = strtotime($createStarTime);
                $where[] = ['>=','a.create_time',$params['create_time']];
            }

            if ($createEndTime != '') {
                $params['create_time'] = strtotime($createEndTime);;
                $where[] = ['<=','a.create_time',$params['create_time']];
            }
        }

        $data = Account::getList($map,$where,$page,$pageSize);
        //获取订单数产品数
        $data = self::getNumber($data);
        $data['params'] = $params;
        return $data;
    }

    /**
     * 添加
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public static function add()
    {
        $name          = trim(yii::$app->request->post('account_name',''));
        $mobile        = trim(yii::$app->request->post('mobile',''));
        $sex           = intval(yii::$app->request->post('sex',0));
        $department_id = intval(yii::$app->request->post('department_id',0));
        $provice       = intval(yii::$app->request->post('province_id',0));
        $city          = intval(yii::$app->request->post('city_id',0));
        $district      = intval(yii::$app->request->post('district_id',0));
        $address       = trim(yii::$app->request->post('address',''));
        $lon           = trim(yii::$app->request->post('lng','')); //经度
        $lat           = trim(yii::$app->request->post('lat','')); //纬度
        $avatarUrl     = trim(yii::$app->request->post('avatar_url','')); //头像
        $clientType    = trim(yii::$app->request->post('client_type','')); //类型
    
        if ($name == '') {
            return self::error('请输入客户姓名!',20001);
        }
        if ($mobile == 0) {
            return self::error('请输入客户电话!',20002);
        }
        if (!preg_match('/^((0[0-9]{2,3}\-*)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?)|(1\d{10})$/',$mobile)) {
            return self::error('客户电话格式不正确',20002);
        }

        $map['department_id']   = self::getDepartmentId();
        $map['mobile'] = $mobile;
        $validateMobile = Account::validateMobile($map);
        if($validateMobile == false){
            return self::error('客户电话已存在!',20002);
        }
        if ($sex == 0) {
            return self::error('请选择称呼!',20003);
        }
        if ($provice == 0) {
            return self::error('请选择所在区域!',20004);
        }
        if ($city == 0) {
            return self::error('请选择所在区域!',20005);
        }
        if ($district == 0) {
            return self::error('请选择所在区域!',20006);
        }
        if ($address == '') {
            return self::error('请输入详细地址!',20007);
        }
        if ($department_id == 0) {
            return self::error('请选择所在机构!',20008);
        }

        if (!$lon || !$lat  ) {
            return self::error('详细地址输入有误!',20007);
        }

        //接收整理客户表数据
        $accountData['direct_company_id'] = self::getDirectCompanyId();
        $accountData['department_top_id'] = self::getDirectCompanyId();
        $accountData['account_name']   = $name;
        $accountData['sex']            = $sex;
        $accountData['department_id']  = $department_id;
        $accountData['mobile']         = $mobile;
        $accountData['status']         = intval(1);
        $accountData['source_type']    = intval(1);
        $accountData['operator_user']  = Yii::$app->user->getIdentity()->id;
        $accountData['create_user']    = Yii::$app->user->getIdentity()->id;
        $accountData['create_time']    = $accountData['update_time'] = intval(time());
        $accountData['avatar_url']     = $avatarUrl;
        if($avatarUrl &&  strpos($avatarUrl,'http')!==false){
            $avatarUrlHC = self::mergeAvatar($avatarUrl."?x-oss-process=image/resize,m_fixed,h_153,w_153,image/circle,r_100/format,png");
            $accountData['avatar_url_hc'] = $avatarUrlHC;
        }
        $accountData['client_type']    = $clientType;
    
        //接受整理客户联系地址数据
        $accountAddressData['lon']          = $lon;
        $accountAddressData['lat']          = $lat;
        $accountAddressData['province_id']   = $provice;  //省份id
        $accountAddressData['city_id']      = $city;  //市id
        $accountAddressData['district_id']  = $district; //区/县id
        $accountAddressData['address']      = $address; //详细地址
        $accountAddressData['is_default']   = intval(1); //默认为1
        $accountAddressData['create_time']  = $accountAddressData['update_time'] = intval(time());
        $accountAddressData['status']       = intval(1);  //默认为1
        $accountAddressData['conact_name']  = $name;
        $accountAddressData['mobile_phone'] = $mobile;
        //调用model执行添加
        $res = Account::add($accountData,$accountAddressData);
        if ($res) {
            return self::success($res,'添加成功');
        }
        return self::error('添加失败');
    }
    //修改
    public static function edit()
    {
        $id            = intval(Yii::$app->request->post('id',0));
        $name          = trim(Yii::$app->request->post('account_name',''));
        $department_id = intval(yii::$app->request->post('department_id',0));
        $mobile        = Yii::$app->request->post('mobile',0);
        $sex           = intval(Yii::$app->request->post('sex',0));
        $provice       = intval(Yii::$app->request->post('province_id',0));
        $city          = intval(Yii::$app->request->post('city_id',0));
        $district      = intval(Yii::$app->request->post('district_id',0));
        $address       = trim(Yii::$app->request->post('address',''));
        $lon           = trim(Yii::$app->request->post('lng','')); //经度
        $lat           = trim(Yii::$app->request->post('lat','')); //纬度
        $avatarUrl     = trim(yii::$app->request->post('avatar_url','')); //头像
        $clientType    = trim(yii::$app->request->post('client_type','')); //类型
    
    
        if ($id == 0) {
            return self::error('数据错误!',20008);
        }
        if ($name == '') {
            return self::error('请输入客户姓名!',20001);
        }
        if ($mobile == 0) {
            return self::error('请输入客户电话!',20002);
        }
        if (!preg_match('/^((0[0-9]{2,3}\-*)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?)|(1\d{10})$/',$mobile)) {
            return self::error('客户电话格式不正确',20002);
        }
        if ($department_id == 0) {
            return self::error('请选择所在机构!',20008);
        }
        $origin_info = Account::findOne(['id'=>$id]);
        $where = ['!=','id',$id];
        //$map['department_top_id']   = self::getDirectCompanyId();
        $map['department_id']   = isset($origin_info['department_id'])?$origin_info['department_id']:'';
        $map['mobile'] = $mobile;
        $validateMobile = Account::validateMobile($map,$where);
        if($validateMobile == false){
            return self::error('客户电话已存在!',20002);
        }
        
        if ($sex == 0) {
            return self::error('请选择称呼!',20003);
        }
        if ($provice == 0) {
            return self::error('请选择所在区域!',20004);
        }
        if ($city == 0) {
            return self::error('请选择所在区域!',20005);
        }
        if ($district == 0) {
            return self::error('请选择所在区域!',20006);
        }
        if ($address == '') {
            return self::error('请输入详细地址!',20007);
        }

        if (!$lon || !$lat) {
            return self::error('详细地址输入有误!',20007);
        }

        
        //接收整理客户表数据
        $accountData['id']           = $id;
        $accountData['account_name'] = $name;
        $accountData['sex']          = $sex;
        $accountData['mobile']       = $mobile;
        $accountData['department_id'] = $department_id;
        $accountData['department_top_id'] = self::getDirectCompanyId();
        $accountData['update_time']  = intval(time());
        $accountData['avatar_url']   = $avatarUrl;

        if($origin_info->avatar_url !=$avatarUrl  && $avatarUrl){
            $avatarUrlHC = self::mergeAvatar($avatarUrl."?x-oss-process=image/resize,m_fixed,h_153,w_153,image/circle,r_100/format,png");
            $accountData['avatar_url_hc'] = $avatarUrlHC;
        }
        $accountData['client_type']  = $clientType;
    
        //接受整理客户联系地址数据
        $accountAddressData['lon']          = $lon; //经度
        $accountAddressData['lat']          = $lat; //纬度
        $accountAddressData['province_id']  = $provice;  //省份id
        $accountAddressData['city_id']      = $city;  //市id
        $accountAddressData['district_id']  = $district; //区/县id
        $accountAddressData['address']      = $address; //详细地址
        $accountAddressData['update_time']  = intval(time());
        $accountAddressData['account_id']   = $id;
        $accountAddressData['conact_name']  = $name;
        $accountAddressData['mobile_phone'] = $mobile;

        //调用model执行添加
        $res = Account::edit($accountData,$accountAddressData);
        if ($res) {
            return self::success('','修改成功');
        }
        return json_encode($res);
    }    
    
    /**
     * 获取当前用户对应的客户信息
     * @author li
     * @date   2017-12-21
     * modify liquan 添加数据权限
     */
    public static function getAccountName($name)
    {
        $result = [];
        $adminId = array();
        $department_id = self::getDepartmentId();
        $adminId[] = $department_id;
        $auth_id = self::getDatAuthId();
        $top_id = self::getDirectCompanyId();
        $flag = 0;
        $department_type = Department::findOne(['id'=>$department_id]);
        if($department_type->type == 1 && ($auth_id == 4 || $auth_id == 5)){
              $auth_id = 1;
        }
        if($auth_id == 2){    //本公司
              $info = department::getDepartmentChildren($top_id,0,1);
              foreach($info as $val){
                    $adminId[] = $val['id'];
              }
        }
          if($auth_id == 3){    //本公司及以下
                $info = department::getDepartmentChildren($top_id);
                foreach($info as $val){
                      $adminId[] = $val['id'];
                }
          }
          if($auth_id == 5){    //本部门及以下
                $info = department::getDepartmentChildren($department_id,2);
                foreach($info as $val){
                      $adminId[] = $val['id'];
                }
          }
          if($auth_id == 1){    //本人
                $adminId = self::getLoginUserId();
                $flag = 1;
          }
        $data = Account::getAccountInfo($name, $adminId,$flag);
        if ($data) {
            foreach ($data as $v) {
                $depart   = Department::findOne(['id'=>$v['department_id']]);
                $result[] = [
                    'id' => $v['id'],
                    'name' => $v['account_name']."/".$v['mobile']."/".$depart->name,
                ];
            }
        }

        //未找到相应客户
        if(!$result){
            $result[] = [
                'id' => 0,
                'name' => '未找到相应客户'
            ];
        }
        return $result;
    }

    /**
     * 获取客户产品数据
     * @param $intentionType
     * @return bool|mixed
     */
    public static function getSelaOrder($intentionType)
    {
        $saleOrder = SaleOrderLogic::index($intentionType);
        $prodIds = [];
        foreach ($saleOrder['list'] as $key => $val) {
            $prodIds[] = $val['id'];            
        }
        $accountId = Yii::$app->request->get('account_id');
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/account-product";
        $postData = [
            'account_id'     => $accountId,
            'sale_order_id'  => $prodIds,
            'department_id'  => self::getDepartmentId(),
            'intention_type' => 1,
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        $data = [];
        //判断是否有数据
        if ($jsonArr['success'] == TRUE) {
            //处理数据
            foreach ($jsonArr['data'] as $key => $val) {
                $data[$key]['plan_time'] = date('Y-m-d',$val['plan_time']);
                $data[$key]['work_name'] = ServiceType::getTitle($val['work_type']);
            }
            //根据产品id组合数据
            foreach ($saleOrder['list'] as $key => $val) {
                if (isset($data[$val['id']])) {
                    $saleOrder['list'][$key]['type_name'] = $data[$val['id']]['work_name'];
                    $saleOrder['list'][$key]['update_time'] = $data[$val['id']]['plan_time'];
                }else{
                    $saleOrder['list'][$key]['type_name'] = '无';
                    $saleOrder['list'][$key]['update_time'] = '无';
                }
            }
            return $saleOrder;
        }else{
            return false;
        } 
    }
    
    
    //获取订单数量和产品数量
    public static function getNumber($data)
    {
        
        //获取订单数和产品数
        //获取客户id
        $accountIds = [];
        foreach ($data['list'] as $key=>$val) {
            $accountIds[] = $val['id'];
        }
        //获取产品数
        $productNumber = SaleOrder::getSaleOrderNum($accountIds);
        //调用订单接口
        $url = Yii::$app->params['order.uservices.cn']."/v1/order/user-orders";
        $postData = [
            'account_ids'     => $accountIds,
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        $orderNumber = [];
        if ($jsonArr['success'] == TRUE) {
            $orderNumber = $jsonArr['data'];
        }
        foreach ($data['list'] as $key => $val) {
            if (isset($productNumber[$val['id']])) {
                $data['list'][$key]['product_number'] = $productNumber[$val['id']];
            }
            else{
                $data['list'][$key]['product_number'] = 0;
            }
            if (isset($orderNumber[$val['id']])) {
                $data['list'][$key]['order_number'] = $orderNumber[$val['id']];
            }
            else {
                $data['list'][$key]['order_number'] = 0;
            }
        }
        return $data;
    }
    public static function zbLogin(){
        //获取用户登录标识
        $cache = 'user_session';
        if (!Yii::$app->cache->exists($cache)) {
            $json = '{session:"" , datas:[';
            $json .= '  {id:"user", val:"txt:shenheng"},';        /*用户名*/
            $json .= '  {id:"password", val:"txt:Qwer1234."},';        /*用户名密码*/
            $json .= '  {id:"serialnum", val:"txt:865441030774699"},';        /*设备串号*/
            $json .= '  {id:"rndcode", val:""}';        /*随机验证码*/
            $json .= ']}';
            $url = "mobilephone/login.asp";
            $info = self::PostCurl($json, $url);
            Yii::$app->cache->set($cache, $info['header']['session'], 3600 * 5);
        }
        $token = Yii::$app->cache->get($cache);
        return $token;
    }
    /**
     *  获取智邦客户信息
     *  liquan 2018-8-16
     *  TODO  各接口参数请参考智邦接口文档
     */
    public static function ZbData($zb_mobile,$selectId='',$flag=0)
    {
        //获取用户登录标识
        $cache = 'user_session';
        if (Yii::$app->cache->exists($cache)) {
            $token = Yii::$app->cache->get($cache);
        }else{
            $token = self::zbLogin();
        }
        $account = [];
        //通过搜索获得客户信息
        $json = '{session:"' . $token . '",cmdkey:"refresh", datas:[';
        if($flag == 1){
            $json .= '  {id:"person_name", val:"' . $zb_mobile . '"},';        /* 姓  名 */
        }else{
            $json .= '  {id:"mobile", val:"' . $zb_mobile . '"},';        /* 手  机 */
        }
        $json .= '  {id:"pagesize", val:"10"},';        /*每页记录数*/
        $json .= '  {id:"pageindex", val:"1"},';        /*数据页标*/
        $json .= '  {id:"_rpt_sort", val:""}';        /*排序字段*/
        $json .= ']}';
        $url = "mobilephone/salesmanage/person/list.asp";
        $res_arr = self::PostCurl($json, $url);
        $user = [];

        if(isset($res_arr['code']) && $res_arr['code'] == 20001){ //登录异常抛出
            return $res_arr;
        }
        foreach ($res_arr['body']['source']['table']['rows'] as $key => $val) {
            foreach ($val as $k => $v) {
                $user[$key][$res_arr['body']['source']['table']['cols'][$k]['id']] = $v;
            }

        }
        if(empty($user)){ //无符合条件
            return [];
        }
        if(count($user) > 1)
        {
            if($selectId == ''){
                return $user;
            }else{
                foreach ($user as $k=>$v)
                {
                    if($v['company'] == $selectId)
                    {
                        $custom = $v;
                        break;
                    }
                }
            }
        }else{
            $custom = $user[0];
        }
        $account['search_user'] = $custom;
        //获取客户基本信息
        $json = '{session:"' . $token . '", datas:[';
        $json .= '  {id:"edit", val:""},';        /*修改模式*/
        $json .= '  {id:"intsort", val:""},';        /*客户类型*/
        $json .= '  {id:"ord", val:"' . $custom["company"] . '"},';        /*数据唯一标识*/
        $json .= '  {id:"_insert_rowindex", val:""},';
        $json .= '  {id:"debug", val:""}';
        $json .= ']}';
        $url = "mobilephone/salesmanage/custom/add.asp?edit=1";
        $res_arr = self::PostCurl($json, $url);
        if(isset($res_arr['code']) && $res_arr['code'] == 20001){ //登录异常抛出
            return $res_arr;
        }
        foreach ($res_arr['body']['bill']['groups'] as $val)
        {
            //排除合同，售后
            if (trim($val['id']) != 'contract' && trim($val['id']) != 'service')
            {
                foreach ($val['fields'] as $k => $v)
                {
                    if (isset($v['text'])) {
                        $account[$val['id']][$v['id']] = $v['text'];
                    } else {
                        if (isset($v['source']['table']['rows']) && !empty($v['source']['table']['rows']))
                        {
                            foreach ($v['source']['table']['rows'] as $z => $x) {
                                foreach ($x as $m => $n) {
                                    $account[$val['id']][$z][$v['source']['table']['cols'][$m]['id']] = $n;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $account;
    }
    public static function getAccount($user){
        //获取用户登录标识
        $cache = 'user_session';
        if (Yii::$app->cache->exists($cache)) {
            $token = Yii::$app->cache->get($cache);
        }else{
            $token = self::zbLogin();
        }

    }
    //合同列表
    public static function getContractList($user_id,$account_page = 1){
        //获取用户登录标识
        $cache = 'user_session';
        if (Yii::$app->cache->exists($cache)) {
            $token = Yii::$app->cache->get($cache);
        }else{
            $token = self::zbLogin();
        }
        //合同列表
        $json = '{session:"'.$token.'",cmdkey:"refresh", datas:[';
        $json .= '  {id:"stype", val:""},';		/*列表模式*/
        $json .= '  {id:"datatype", val:"tel"},';		/*列表模式*/
        $json .= '  {id:"ord", val:"'.$user_id.'"},';		/*列表数据检索条件*/
        $json .= '  {id:"pagesize", val:"20"},';		/*每页记录数*/
        $json .= '  {id:"pageindex", val:"'.$account_page.'"}';		/*数据页标*/
        $json .= ']}';
        $url = "mobilephone/salesmanage/contract/billlist.asp";
        $res_arr = self::PostCurl($json,$url);
        if(isset($res_arr['code']) && $res_arr['code'] == 20001){ //登录异常抛出
            return $res_arr;
        }
        if($res_arr['body']['source']['table']['rows'])
        {
            $contract['page'] = $res_arr['body']['source']['table']['page'];
            foreach($res_arr['body']['source']['table']['rows'] as $key=>$val)
            {
                foreach($val as $k=>$v){
                    $contract['data'][$key][$res_arr['body']['source']['table']['cols'][$k]['id']] = $v;
                }
            }
            return $contract;
        }
        return [];
    }

    //合同详情
    public static function getContractDetail($contract_id)
    {
        if ($contract_id) {
            //获取用户登录标识
            $cache = 'user_session';
            if (Yii::$app->cache->exists($cache)) {
                $token = Yii::$app->cache->get($cache);
            } else {
                $token = self::zbLogin();
            }
            //合同详情
            $json = '{session:"' . $token . '" , datas:[';
            $json .= '  {id:"ord", val:"' . $contract_id . '"},';        /*数据唯一标识*/
            $json .= '  {id:"_insert_rowindex", val:""},';        /**/
            $json .= '  {id:"debug", val:""}';        /**/
            $json .= ']}';
            $url = "mobilephone/salesmanage/contract/billservice.asp";
            $res_arr = self::PostCurl($json, $url);
            if(isset($res_arr['code']) && $res_arr['code'] == 20001){ //登录异常抛出
                return $res_arr;
            }
            foreach ($res_arr['body']['bill']['groups'] as $val)
            {
                foreach ($val['fields'] as $k => $v)
                {
                    if (isset($v['text'])) {
                        $cap = str_replace("@","",$v['id']);
                        $contractDetail[$val['id']][$cap] = $v['text'];
                    } else {
                        if (isset($v['source']['table']['rows']))
                        {
                            foreach ($v['source']['table']['rows'] as $z => $x)
                            {
                                foreach ($x as $m => $n) {
                                    $contractDetail[$val['id']][$z][$v['source']['table']['cols'][$m]['id']] = $n;
                                }
                            }
                        }
                    }
                }
            }

            //获取出库详情
            if (isset($contractDetail['kuout']))
            {
                foreach ($contractDetail['kuout'] as $m2 => $n2)
                {
                    $json = '{session:"' . $token . '" , datas:[';
                    $json .= '{id:"ord", val:"' . $n2['ord'] . '"},';/*数据唯一标识*/
                    $json .= '{id:"_insert_rowindex", val:""},';
                    $json .= '{id:"debug", val:""}';
                    $json .= ']}';
                    $url = 'mobilephone/storemanage/kuout/detail.asp';
                    $res_arr2 = self::PostCurl($json, $url);
                    if(isset($res_arr['code']) && $res_arr['code'] == 20001){ //登录异常抛出
                        return $res_arr;
                    }
                    foreach ($res_arr2['body']['bill']['groups'] as $val2)
                    {
                        foreach ($val2['fields'] as $k2 => $v2)
                        {
                            if (isset($v2['source']['table']['rows']))
                            {
                                foreach ($v2['source']['table']['rows'] as $z => $x)
                                {
                                    foreach ($x as $m => $n) {
                                        $contractDetail['kuout'][$m2][$val2['id']][$z][$v2['source']['table']['cols'][$m]['id']] = $n;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //获取发货详情
            if (isset($contractDetail['send'])) {
                foreach ($contractDetail['send'] as $m2 => $n2)
                {
                    $json = '{session:"' . $token . '" , datas:[';
                    $json .= '{id:"ord", val:"' . $n2['ord'] . '"},';/*数据唯一标识*/
                    $json .= '{id:"_insert_rowindex", val:""},';
                    $json .= '{id:"debug", val:""}';
                    $json .= ']}';
                    $url = 'mobilephone/storemanage/sent/detail.asp';
                    $res_arr2 = self::PostCurl($json, $url);
                    if(isset($res_arr['code']) && $res_arr['code'] == 20001){ //登录异常抛出
                        return $res_arr;
                    }
                    foreach ($res_arr2['body']['bill']['groups'] as $val2)
                    {
                        foreach ($val2['fields'] as $k2 => $v2)
                        {
                            if (isset($v2['source']['table']['rows']))
                            {
                                foreach ($v2['source']['table']['rows'] as $z => $x)
                                {
                                    foreach ($x as $m => $n) {
                                        $contractDetail['send'][$m2][$val2['id']][$z][$v2['source']['table']['cols'][$m]['id']] = $n;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $contractDetail;
        }
        return [];
    }
//售后列表
public static function getServiceList($user_id,$service_page = 1)
{
    //获取用户登录标识
    $cache = 'user_session';
    if (Yii::$app->cache->exists($cache)) {
        $token = Yii::$app->cache->get($cache);
    } else {
        $token = self::zbLogin();
    }
    //售后列表
    $json = '{session:"' . $token . '",cmdkey:"refresh", datas:[';
    $json .= '  {id:"datatype", val:"tel"},';        /*列表模式*/
    $json .= '  {id:"ord", val:"' . $user_id . '"},';        /*列表数据检索条件*/
    $json .= '  {id:"pagesize", val:"20"},';        /*每页记录数*/
    $json .= '  {id:"pageindex", val:"' . $service_page . '"},';        /*数据页标*/
    $json .= '  {id:"_rpt_sort", val:""}';        /*排序字段*/
    $json .= ']}';
    $url = "mobilephone/salesmanage/service/list.asp";
    $res_arr = self::PostCurl($json, $url);
    if(isset($res_arr['code']) && $res_arr['code'] == 20001){ //登录异常抛出
        return $res_arr;
    }
    if ($res_arr['body']['source']['table']['rows'])
    {
        $serviceList['page'] = $res_arr['body']['source']['table']['page'];
        foreach ($res_arr['body']['source']['table']['rows'] as $key => $val)
        {
            foreach ($val as $k => $v) {
                $serviceList['data'][$key][$res_arr['body']['source']['table']['cols'][$k]['id']] = $v;
            }
        }
        return $serviceList;
    }
    return [];
}
        //获取售后详情
public static function getServiceDetail($service_id)
{
        if($service_id){
            //获取用户登录标识
            $cache = 'user_session';
            if (Yii::$app->cache->exists($cache)) {
                $token = Yii::$app->cache->get($cache);
            } else {
                $token = self::zbLogin();
            }
            $json = '{session:"'.$token.'" , datas:[';
            $json .= '  {id:"ord", val:"'.$service_id.'"},';		/*数据唯一标识*/
            $json .= '  {id:"_insert_rowindex", val:""},';
            $json .= '  {id:"debug", val:""}';
            $json .= ']}';
            $url = 'mobilephone/salesmanage/service/add.asp';
            $res_arr = self::PostCurl($json,$url);
            if(isset($res_arr['code']) && $res_arr['code'] == 20001){ //登录异常抛出
                return $res_arr;
            }
            foreach($res_arr['body']['bill']['groups'] as $val)
            {
                foreach($val['fields'] as $k=>$v)
                {
                    if(isset($v['text']))
                    {
                        $serviceDetail[$val['id']][$v['id']] = $v['text'];
                    }else{
                        if(isset($v['source']['table']['rows']))
                        {
                            foreach($v['source']['table']['rows'] as $z=>$x)
                            {
                                foreach($x as $m=>$n){
                                    $serviceDetail[$val['id']][$z][$v['source']['table']['cols'][$m]['id']] = $n;
                                }
                            }
                        }
                    }
                }
            }
            return $serviceDetail;
        }
        return [];
    }
    public static function PostCurl($json,$url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, 'http://www.bfznkj.com:84/SYSA/'.$url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/zsml; charset=utf-8',	 /*接口规定content-type值必须为application/zsml。*/
                'Content-Length: '.strlen($json))
        );
        ob_start();
        curl_exec($ch);
        $b=ob_get_contents();
        ob_end_clean();
        //替换特殊字符
        $result =  preg_replace('/[\x00-\x1F\x80-\x9F]/u', '', trim($b));
        $res_arr = json_decode($result,true);
        //登录状态异常处理
        if($res_arr['header']['status'] != 0){
            Yii::$app->cache->delete('user_session');
            return self::error("状态异常！请点击按钮，重新搜索！",20001);
        }
        return $res_arr;
    }
    /**
     *  添加智邦客户到U服务
     *  liquan 2018-8-16
     */
    public static function AddZbAccount($data){
          if(!empty($data))
          {
              //解析客户地址
             $area = self::getArea($data['custom_Info']['address']);
             //如解析不到
             if(empty($area) || $area['pro'] == '' || $area['cit'] == '' || $area['dis'] == ''){
                 return '-201';
             }
             $preson_mobile = array_column($data['preson'],"mobile");
             $res = Account::findAllByAttributes(['mobile'=>$preson_mobile]);

             if(!empty($res)){  //客户已存在
                 //判断联系人信息列表  无责添加 有责跳过
                 $account = Account::findoneByAttributes(['mobile'=>$preson_mobile[0]]);
                 $address = AccountAddress::findAllByAttributes(['account_id'=>$account['id']]);
                 $fws = [];
                 foreach ($address as $k=>$v) {
                    $fws[$k] = $v['conact_name'].$v['mobile_phone'].$v['address'];
                 }
                 foreach ($data['preson'] as $key=>$val)
                 {
                     //判断手机号 姓名 地址
                     $str = $val['name'].$val['mobile'].$data['custom_Info']['address'];
                     $add_data = [];
                     if(!in_array($str,$fws))
                     {
                         //$lacation = Gaode::getLocation($data['custom_Info']['address']);
                         $add_data[$key]['department_id'] = $account['department_id'];
                         $add_data[$key]['lon'] = 0;
                         $add_data[$key]['lat'] = 0;
                         $add_data[$key]['province_id'] = $area['pro'];
                         $add_data[$key]['city_id'] = $area['cit'];
                         $add_data[$key]['district_id'] = $area['dis'];
                         $add_data[$key]['address'] = $data['custom_Info']['address'];
                         $add_data[$key]['is_default'] = 2;
                         $add_data[$key]['create_time'] = time();
                         $add_data[$key]['update_time'] = time();
                         $add_data[$key]['status'] = 1;
                         $add_data[$key]['account_id'] = $account['id'];
                         $add_data[$key]['conact_name'] = $val['name'];
                         $add_data[$key]['mobile_phone'] = $val['mobile'];
                     }
                 }
                 if(!empty($add_data)){
                     //新增联系地址
                     AccountAddress::batchInsert($add_data);
                 }
                 $account_id =  $account['id'];
             }else  //客户不存在
             {
                //添加客户
                 $account_data = [];
                 $saleUser = User::getOne(['username'=>$data['custom_Info']['cateid'],"status"=>1,"del_status"=>1]);
                 if (empty($saleUser)){
                     return '-202';
                 }
                 $account_data['create_user'] = $saleUser['id'];
                 $account_data['department_id'] = $saleUser['department_id'];
                 $account_data['direct_company_id'] = Department::getDirectCompanyIdById($saleUser['department_id']);
                 $account_data['department_top_id'] = Department::getTopIdById($saleUser['department_id']);
                 $client_type = 4;
                 if(isset($data['custom_Info']['sort1'])){
                     $sort1 = explode("-",$data['custom_Info']['sort1']);
                     if(trim($sort1[0]) == "零售门店"){
                         $client_type = 3;
                     }
                     if(trim($sort1[0]) == "批发"){
                         $client_type = 5;
                     }
                     if(trim($sort1[0]) == "工程项目"){
                         $client_type = 7;
                     }
                     if(trim($sort1[0]) == "小区推广"){
                         $client_type = 8;
                     }
                     if(trim($sort1[0]) == "公司"){
                         $client_type = 2;
                     }
                 }
                 $account_data['client_type'] = $client_type;
                 $account_data['mobile'] = $data['preson'][0]['mobile'];
                 $account_data['account_name'] = $data['custom_Info']['name'];

                 $account_data['operator_user'] = Yii::$app->user->id;
                 $account_data['create_time'] = time();
                 $account_data['update_time'] = time();
                 $account_data['status'] = 1;
                 $account_data['source_type'] = 1;
                 //客户联系地址
                 $account_address = [];
                 $account_address['department_id'] = $saleUser['department_id'];
                 $account_address['lon'] = 0;
                 $account_address['lat'] = 0;
                 $account_address['province_id'] = $area['pro'];
                 $account_address['city_id'] = $area['cit'];
                 $account_address['district_id'] = $area['dis'];
                 $account_address['address'] = $data['custom_Info']['address'];
                 $account_address['is_default'] = 1;
                 $account_address['create_time'] = time();
                 $account_address['update_time'] = time();
                 $account_address['status'] = 1;
                 $account_address['conact_name'] = $data['custom_Info']['name'];
                 $account_address['mobile_phone'] = $data['preson'][0]['mobile'];
                 $result = Account::add($account_data,$account_address);
                 //添加联系人
                 foreach ($data['preson'] as $key=>$val)
                 {
                     if(($val['name'] != $data['custom_Info']['name']) || ($val['mobile'] !=$data['preson'][0]['mobile'])){
                         $add_data = [];
                         $add_data[$key]['department_id'] = $saleUser['department_id'];
                         $add_data[$key]['lon'] = 0;
                         $add_data[$key]['lat'] = 0;
                         $add_data[$key]['province_id'] = $area['pro'];
                         $add_data[$key]['city_id'] = $area['cit'];
                         $add_data[$key]['district_id'] = $area['dis'];
                         $add_data[$key]['address'] = $data['custom_Info']['address'];
                         $add_data[$key]['is_default'] = 2;
                         $add_data[$key]['create_time'] = time();
                         $add_data[$key]['update_time'] = time();
                         $add_data[$key]['status'] = 1;
                         $add_data[$key]['account_id'] = $result['id'];
                         $add_data[$key]['conact_name'] = $val['name'];
                         $add_data[$key]['mobile_phone'] = $val['mobile'];
                     }
                 }
                 $account_id =  $result['id'];
                 if(!empty($add_data)){
                     //新增联系地址
                     AccountAddress::batchInsert($add_data);
                 }
             }
             return $account_id;
          }else{
             return 0;
          }
    }

    /**
     * @param $address
     * 根据地址获取省市区ID
     */
    public static function getArea($address){
           $area = Gaode::getProvinceName($address,'北京');
           if(empty($area)){
               $area = Gaode::getProvinceName($address);
           }
           if(empty($area)){
               return [];
           }
           $pro = '';
           $cit = '';
           $dis = '';
           if($area['province'] != ''){
               $province = mb_substr($area['province'],0,2);
               $info = Region::findOneByAttributes(['region_name'=>$province,'region_type'=>1]);
               if($info){
                   $pro = $info['region_id'];
               }
           }
            if($area['city'] != ''){
                $info = Region::findOneByAttributes(['region_name'=>$area['city'],'region_type'=>2]);
                if($info){
                    $cit = $info['region_id'];
                }
            }
            if($area['district'] != '' && !empty($area['district'])){
                preg_match_all('/./u', $area['district'], $arrName);    //使用正则匹配将汉字转成数组
                if(count($arrName[0])==2){  //如果是两个字，则加全角空格
                    $district = $arrName[0][0].'　'.$arrName[0][1];
                }else{
                    $district = $area['district'];
                }
                $info = Region::findOneByAttributes(['region_name'=>$district,'region_type'=>3]);
                if($info){
                    $dis = $info['region_id'];
                }
            }
            $data['pro'] = $pro;
            $data['cit'] = $cit;
            $data['dis'] = $dis;
            $data['lon'] = $area['lon'];
            $data['lat'] = $area['lat'];
            return $data;
    }
    /**
     * 将选中智邦产品和产品库匹配的产品 加至客户售后产品表
     * liquan 2018-8-16
     * @return array
     */
    public static function AddZbProduct(){

        $prod_arr = Yii::$app->request->post('prod_arr', '');
        $account_id = Yii::$app->request->post('account_id', '');
        $contract_id = Yii::$app->request->post('contract_id', '');
        $prod_arr = json_decode($prod_arr,true);
        if(empty($prod_arr)){
            return 20007;
        }
        if($account_id == ''|| $contract_id == ''){
            return 20008;
        }
        foreach ($prod_arr as $k=>$v){
            $search[$v['id']] = $v['name'];
        }
        $prod_no_arr = array_column($prod_arr,'id');
        $list = Product::findAllByAttributes(['prod_no'=>$prod_no_arr,'status'=>1,'direct_company_id'=>self::getDirectCompanyId()]);
        $prod_ids = array_column($list,'id');
        //已存在的产品
        if(!empty($list)) {
            //在所有搜索产品里 删除已存在的产品 不存在的返回输出到页面
            foreach ($list as $k1 => $v1) {
                if (isset($search[$v1['prod_no']])) {
                    unset($search[$v1['prod_no']]);
                }
            }
            //判断是否已存在客户产品
            $sale_info = SaleOrder::findAllByAttributes(['prod_id' => $prod_ids, 'contract_id' => $contract_id,'direct_company_id'=>self::getDirectCompanyId(), 'source' => 3, 'account_id' => $account_id]);
            $jy = [];
            if (!empty($sale_info)) {
                $is_exsit = array_column($sale_info, 'prod_id');
                foreach ($list as $k => $v) {
                    if (in_array($v['id'], $is_exsit)) {
                        unset($list[$k]);
                    }
                }
                //已存在产品 但已被禁用
                foreach ($sale_info as $k2 => $v2) {
                    if($v2['status'] == 0){
                        $jy[] = $v2['id'];
                        unset($sale_info[$k2]);
                    }
                }
                //修改成启用
                SaleOrder::updateAll(['status'=>1],['id'=>$jy]);
            }

        }
        if(!empty($list)) {
            //查询关联的物料包
            $productIds = array_column($list,'id');
            $mate = ProductMaterials::findAllByAttributes(['prod_id'=>$productIds,'status'=>1,'is_directly'=>2],"parent_id,prod_id","prod_id");
            foreach ($productIds as $key=>$val)
            {
                if(isset($mate[$val])){
                    $productIds[] = $mate[$val]['parent_id'];
                }
            }
            $saleList = Product::findAllByAttributes(['id'=>$productIds]);
            //添加至售后产品表
            $add_sale_data = [];
            foreach ($saleList as $key=>$val)
            {
                 $add_sale_data[$key]['department_id'] =  self::getDepartmentId();
                 $add_sale_data[$key]['direct_company_id'] =  self::getDirectCompanyId();
                 $add_sale_data[$key]['department_top_id'] =  self::getTopId();;
                 $add_sale_data[$key]['prod_id'] = $val['id'];
                 $add_sale_data[$key]['account_id'] = $account_id;
                 $add_sale_data[$key]['create_time'] = time();
                 $add_sale_data[$key]['update_time'] = time();
                 $add_sale_data[$key]['status'] = 1;
                 $add_sale_data[$key]['type_id'] = $val['type_id'];
                 $add_sale_data[$key]['prod_series'] = $val['model'];
                 $add_sale_data[$key]['brand_id'] = $val['brand_id'];
                 $add_sale_data[$key]['class_id'] = $val['class_id'];
                 $add_sale_data[$key]['intention_type'] = 1;
                 $add_sale_data[$key]['contract_id'] = $contract_id;
                 $add_sale_data[$key]['source'] = 3; //
            }
            //add saleorder
            if(!empty($add_sale_data))
            {
                SaleOrder::batchSale($add_sale_data);
            }
        }
        //返回已存在的产品名称
        if(!empty($search)){
            foreach ($search as $v2){
                $return[]['name'] = $v2;
            }
            return $return;
        }
        return [];
    }

    /**
     * 合并图片
     * @param $avatarUrl
     */
    public static function mergeAvatar($avatarUrl)
    {
        $dstPath = Yii::getAlias('@webapp') . '/web/img/avatarKK.png';

        $dst = imagecreatefromstring(file_get_contents($dstPath));
        $src = imagecreatefromstring(file_get_contents($avatarUrl));

        imagesavealpha($dst,true);
        imagesavealpha($src,true);

        list($src_w, $src_h) = getimagesize($avatarUrl);
        imagecopy($dst, $src, 25, 25, 0, 0, $src_w, $src_h);

        $file = Yii::getAlias('@runtime') . '/'.time().'.png';
        imagepng($dst,$file );
        imagedestroy($dst);
        imagedestroy($src);

        $res = Upload::uploadLocalFile($file, 'account/avatar');
        if($res['code'] == 0){
            unlink($file);
            return $res['url'];
        }

        return '';
    }

}
<?php
namespace webapp\logic;

use common\models\Common;
use common\models\Product;
use common\models\ServiceType;
use console\models\WorkOrder;
use webapp\models\BaseModel;
use webapp\models\CostItem;
use webapp\models\Department;
use webapp\models\Region;
use webapp\models\SaleOrder;
use webapp\models\SaleOrderView;
use common\models\ServiceFlow;
use webapp\models\Technician;
use Yii;
use common\helpers\Helper;
use webapp\models\Account;
use webapp\models\Stores;
use webapp\models\AccountAddress;
use common\models\User;
use webapp\logic\MessageLogic;
use common\lib\Push;
use webapp\models\CostLevel;
use common\models\Work;
use common\models\CostPayType;
use webapp\models\WorkCostForm;
use webapp\models\CostLevelItem;
use webapp\models\CostLevelDetail;
use yii\helpers\ArrayHelper;
use common\models\Order;
use common\models\CostItemSet;


class WorkLogic extends BaseLogic
{

    /**
     * 订单列表
     * @return array
     * @author xi
     * @date 2017-12-18
     */
    public static function getList()
    {
        $workNo         = trim(Yii::$app->request->get('work_no',''));
        $accountName     = trim(Yii::$app->request->get('account_name',''));
        $accountMobile   = trim(Yii::$app->request->get('account_mobile',''));
        $workType        = intval(Yii::$app->request->get('work_type',0));
        $planStartTime   = Yii::$app->request->get('plan_start_time','');
        $planEndTime     = Yii::$app->request->get('plan_end_time','');
        $accountId       = intval(Yii::$app->request->get('account_id',0));
        $status          = Yii::$app->request->get('status',0);
        $cancelStatus    = intval(Yii::$app->request->get('cancel_status',1));

        $visitStatus     = intval(Yii::$app->request->get('visit_status',0));
        $isFirst         = Yii::$app->request->get('is_first',-1);
        $confirmStatus = intval(Yii::$app->request->get('confirm_status',0));
        $settlementStatus = intval(Yii::$app->request->get('settlement_status',0));

        $firstClassId     = intval(Yii::$app->request->get('first_class_id',0));
        $secondClassId     = intval(Yii::$app->request->get('second_class_id',0));

        //技师信息
        $tecMember       = trim(Yii::$app->request->get('tec_member',''));
        $tecMobile       = intval(trim(Yii::$app->request->get('tec_mobile',0)));
        $page            = intval(Yii::$app->request->get('page',1));
        $pageSize        = intval(Yii::$app->request->get('pageSize',10));
        
        $accountIds = [];
        $saleOrderId = [];
        
        //搜索
        $flag = true;
        if($accountName!=''){
            $res = Account::find()->andWhere(['like','account_name',$accountName])->select('id')->asArray()->all();
            $accountIds = array_column($res, 'id');
            if(!$accountIds){
                $accountIds = [-1];
                $flag = false;
            }
            else {
                $flag = true;
            }
        }
        if($accountMobile!='' && $flag == true){
            $res = Account::find()->andWhere(['like','mobile',$accountMobile])->select('id')->asArray()->all();
            if($res){
                $accountIds = array_merge($accountIds,array_column($res, 'id'));
            }
            else {
                $accountIds = [-1];
            }
        }
        //技师信息
        $technicianIds = [];
        if($tecMember!=''){
            $res = Technician::getTechnicianId(['like','name',$tecMember]);
            $technicianIds = array_column($res, 'id');
            if(!$technicianIds){
                $technicianIds = [-1];
                $flag = false;
            }
            else {
                $flag = true;
            }
        }
        if($tecMobile!='' && $flag == true){
            $res = Technician::getTechnicianId(['like','mobile',$tecMobile]);
            if($res){
                $technicianIds = array_merge($technicianIds,array_column($res, 'id'));
            }
            else {
                $technicianIds = [-1];
            }
        }

        if($accountId>0){
            $accountIds[] = $accountId;
        }

        if(trim($planStartTime) !=''){
            $planStartTime = strtotime($planStartTime);
        }
        if(trim($planEndTime) !=''){
            $planEndTime = strtotime($planEndTime);
        }
        //产品类目
        if($secondClassId>0){
            $saleOrderId = SaleOrderView::getIdsByClassId($secondClassId);
            if(!$saleOrderId){
                $saleOrderId = -1;
            }
        }
        else if($firstClassId>0){
            $saleOrderId = SaleOrderView::getIdsByClassId($firstClassId);
            if(!$saleOrderId){
                $saleOrderId = -1;
            }
        }

        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/search";
        $postData = [
            'department_id'     => self::getDepartmentId(),
            'work_no'           => $workNo,
            'account_ids'       => $accountIds,
            'work_type'         => $workType,
            'sale_order_id'     => $saleOrderId,
            'plan_start_time'   => $planStartTime,
            'plan_end_time'     => $planEndTime,
            'technician_id'     => $technicianIds,
            'status'            => $status,
            'cancel_status'     => $cancelStatus,
            'visit_status'      => $visitStatus,
            'is_first'          => $isFirst,
            'confirm_status'    => $confirmStatus,
            'settlement_status' => $settlementStatus,
            'page'              => $page,
            'pageSize'          => $pageSize,
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);

        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            
            $list = $jsonArr['data']['list'];
            if( $list )
            {
                //客户
                $accountArr = [];
                $accountIds = array_column($list, 'account_id');
                if($accountIds){
                    $accountArr = Account::findAllByAttributes(['id'=>$accountIds,'status'=>1],'id,account_name,mobile','id');
                }
                //客户地址
                $accountAddressArr = [];
                $accountAddressIds = array_column($list, 'address_id');
                
                if($accountAddressIds){
                    $accountAddressArr = Region::getAddress($accountAddressIds, $type = 2);
                }
                //服务类型
                $workTypeArr = self::getTypeList();
                $workTypeArr = $workTypeArr['data'];
                
                //服务产品
                $saleOrderIds = array_column($list, 'sale_order_id');
                $saleOrderArr = SaleOrderView::findAllByAttributes(['id'=>$saleOrderIds,'status'=>1],'id,prod_name,brand_name,class_name,type_name','id');

                //技师
                $technicianArr = [];
                $technicianIds = array_filter(array_column($list,'technician_id'));
                if($technicianIds){
                    $technicianArr = Technician::findAllByAttributes(['id'=>$technicianIds],'id,name,mobile','id');
                }

                //合并数据 
                foreach ($jsonArr['data']['list'] as $key=>$val)
                {
                    $accountName   = '';
                    $accountMobile = '';
                    $workTypeDesc  = '';
                    $prodDesc      = '';
                    $technicianName = '--';
                    $technicianMobile = '--';
                    $technicianTitle  = '';
                    $address = '';
                    $serviceContent = '无';
                    $brandName = '';
                    
                    //客户
                    if( isset($accountArr[$val['account_id']]) ){
                        $accountName  = $accountArr[$val['account_id']]['account_name'];
                        $accountMobile = $accountArr[$val['account_id']]['mobile'];
                    }
                    //客户地址
                    if( isset($accountAddressArr[$val['address_id']]) ){
                        $address = $accountAddressArr[$val['address_id']]['address'];
                    }
                    
                    //服务类型
                    if(isset($workTypeArr[$val['work_type']])){
                        $workTypeDesc = $workTypeArr[$val['work_type']];
                    }
                    //服务产品 
                    if(isset($saleOrderArr[$val['sale_order_id']])){
                        $prodDesc = $saleOrderArr[$val['sale_order_id']]['prod_name'];
                    }
                    //服务内容
                    if(isset($saleOrderArr[$val['sale_order_id']])){
                        $serviceContent = $saleOrderArr[$val['sale_order_id']]['brand_name'].'-'.$saleOrderArr[$val['sale_order_id']]['class_name'].'-'.$workTypeDesc;
                        $brandName = $saleOrderArr[$val['sale_order_id']]['brand_name'];
                    }
                    //技师
                    if($val['technician_id']>0 && isset($technicianArr[$val['technician_id']])){
                        $technicianName   = $technicianArr[$val['technician_id']]['name'];
                        $technicianMobile = $technicianArr[$val['technician_id']]['mobile'];
                        $technicianTitle  = $val['technician_arr']['job_title'];
                    }
                    $jsonArr['data']['list'][$key]['account_name']   = $accountName;
                    $jsonArr['data']['list'][$key]['account_mobile'] = $accountMobile;
                    $jsonArr['data']['list'][$key]['work_type_desc'] = $workTypeDesc;
                    $jsonArr['data']['list'][$key]['plan_time']      = $val['plan_time']?Order::getPlantimeType($val['plan_time_type'],date('Y-m-d H:i',$val['plan_time'])):'待定';
                    $jsonArr['data']['list'][$key]['plan_sort_time'] = $val['plan_time'];

                    $jsonArr['data']['list'][$key]['prod_desc']      = $prodDesc;
                    $jsonArr['data']['list'][$key]['technician_name']   = $technicianName;
                    $jsonArr['data']['list'][$key]['technician_mobile'] = $technicianMobile;
                    $jsonArr['data']['list'][$key]['technician_title']  = $technicianTitle!=''?'('.$technicianTitle.')':'';
                    $jsonArr['data']['list'][$key]['address'] = $address;
                    $jsonArr['data']['list'][$key]['service_content'] = $serviceContent;

                    $jsonArr['data']['list'][$key]['begin_service_time'] = $val['begin_service_time']>0?date('Y-m-d H:i',$val['begin_service_time']):'';
                    $jsonArr['data']['list'][$key]['finsh_service_time'] = $val['finsh_service_time']>0?date('Y-m-d H:i',$val['finsh_service_time']):'';
                    $jsonArr['data']['list'][$key]['brand_name'] = $brandName;
                }
            }

            return self::success($jsonArr['data']);
        }
        else {
            return self::error("接口：$url 没有返回数据");
        }
    }

    /**
     * 关闭工单
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function close($workNo,$reason)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/close";

        $postData = [
            'department_id'   => self::getDepartmentId(),
            'work_no'         => $workNo,
            'process_user_id' => self::getLoginUserId(),
            'reason'          => $reason
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);

        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            return self::success(['work_no'=>$workNo]);
        }
        return self::error($jsonArr['message'],$jsonArr['code']);
    }

    /**
     * 修改工单服务时间
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function ChangeTime($workNo,$planTime,$planTimeType){
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/change-plantime";
        $postData = [
            'src_type'        => BaseModel::SRC_FWS,
            'src_id'          => self::getManufactorId(),
            'work_no'         => $workNo,
            'process_user_id' => self::getLoginUserId(),
            'plantime'        => $planTime,
            'plan_time_type'  => $planTimeType,
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            return self::success(['work_no'=>$workNo],'提交成功');
        }
        return self::error($jsonArr['message'],20005);
    }

    /**
     * 确认回访工单
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function visit($workNo,$orderNo,$content)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/visit";

        $postData = [
            'src_type'        => BaseModel::SRC_FWS,
            'src_id'          => self::getManufactorId(),
            'work_no'         => $workNo,
            'order_no'        => $orderNo,
            'process_user_id' => self::getLoginUserId(),
            'content'          => $content
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);

        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            return self::success(['work_no'=>$workNo]);
        }
        return self::error($jsonArr['message'],20004);
    }

    /**
     * 查看已回访的工单详情
     * @param $workNo
     * @return array
     */
    public static function viewVisit($workNo)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/view-visit";

        $postData = [
            'src_type'        => BaseModel::SRC_FWS,
            'src_id'          => self::getManufactorId(),
            'work_no'         => $workNo,
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);

        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            return self::success($jsonArr['data']);
        }
        return self::error($jsonArr['message'],20004);
    }

    /**
     * 对服务中的工单进行完成服务操作
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function setFinish($type,$workNo,$workImg,$sceneImg,$serviceRecord,$flag=1)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/finsh-service";
        $child = Department::getDepartmentChildren(self::getDirectCompanyId(),0,1);
        $child_ids = array_column($child,'id');
        $postData = [
            'department_id'   => self::getDepartmentId(),
            'department_ids'   => $child_ids,
            'type'            => $type,
            'work_no'         => $workNo,
            'process_user_id' => self::getLoginUserId(),
            'work_img'        => $workImg,
            'scene_img'       => $sceneImg,
            'service_record'  => $serviceRecord,
            'flag'             =>$flag
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);

        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            return ['work_no'=>$workNo];
        }
        return $jsonArr;
    }
    /**
     * 获取工单详情
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function getDetail(){
        $order_no= trim(Yii::$app->request->get('order_no',''));
        $work_no= trim(Yii::$app->request->get('work_no',''));
        //print_r($work_no);die;
        if($work_no){
            //获取订单号
            $orderInfo = Work::findOne(['work_no'=>$work_no]);
            $order_no = isset($orderInfo->order_no)?$orderInfo->order_no:'';
        }
        if(!$order_no){
            return self::error('订单号不能为空！',20004);
        }
        $data = [];
        $data = OrderLogic::getDetail($order_no);
        if($data){
            $accountName   = '';
            $accountMobile = '';
            $workTypeDesc  = '';
            //服务类型
            $workTypeArr = self::getTypeList();
            if(isset($workTypeArr['data'][$data['work_type']])){
                $workTypeDesc = $workTypeArr['data'][$data['work_type']];
            }
            //服务产品
            $saleOrderIds = array_column($data['workDetail'], 'sale_order_id');
            $saleOrderArr = SaleOrderView::findAllByAttributes(['id'=>$saleOrderIds,'status'=>1],'id,prod_name,brand_name,class_name,type_name','id');
            //服务内容
            $serviceContent = '';
            if(isset($saleOrderArr[$saleOrderIds[0]])){
                $serviceContent = $saleOrderArr[$saleOrderIds[0]]['brand_name'].'-'.$saleOrderArr[$saleOrderIds[0]]['class_name'].'-'.$workTypeDesc;
            }
            //联系地址
            $addressDesc = Region::getAddress($data['address_id'], $type = 1);
            $data['account_name']   = $accountName;
            $data['account_mobile'] = $accountMobile;
            $data['plan_time']      = date('Y-m-d H:i',$data['plan_time']);
            $data['create_time']    = date('Y-m-d H:i',$data['create_time']);
            $data['word_type_desc'] = $workTypeDesc;
            $data['word_type']      = $data['work_type'];
            $data['address_desc']   = $addressDesc['address'];
            $data['conact_name']    = AccountAddress::getConactName($data['address_id']);
            $data['amount']         = $data['amount']/100;
            $data['create_user']    = User::getCreateName($data['create_user_id']);
            $data['service_content']= $serviceContent;
            $data['brand_name'] = isset($saleOrderArr[$saleOrderIds[0]]['brand_name'])?$saleOrderArr[$saleOrderIds[0]]['brand_name']:'';
            $data['class_name'] = isset($saleOrderArr[$saleOrderIds[0]]['class_name'])?$saleOrderArr[$saleOrderIds[0]]['class_name']:'';
            foreach ($data['workDetail'] as $key=>$val){
                $data['workDetail'][$key]['productDesc'] = $serviceContent;
            }
            return self::success($data);
        }else{
            return self::error('');
        }
    }
    /**
     * 工单详情
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function getWorkDetail()
    {
        $work_no= trim(Yii::$app->request->get('work_no',''));
        if(!$work_no){
            return self::error('工单号不能为空！',20004);
        }
        //添加工单
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/detail";
        $postData = [
            'department_id' => self::getDepartmentId(),
            'work_no'       => $work_no
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        if(isset($result['success']) && $result['success'] == 1 )
        {
            $data = $result['data'];
            if($data){
                $accountName   = '';
                $accountMobile = '';
                $workTypeDesc  = '';

                $accountArr = Account::findOneByAttributes(['id'=>$data['order_info']['account_id']],'id,account_name,mobile');
                if($accountArr){
                    $accountName   = $accountArr['account_name'];
                    $accountMobile = $accountArr['mobile'];
                }

                //服务类型
                $workTypeArr = self::getTypeList();
                if(isset($workTypeArr['data'][$data['order_info']['work_type']])){
                    $workTypeDesc = $workTypeArr['data'][$data['order_info']['work_type']];
                }
                //服务产品
                $saleOrderIds = $data['order_info']['sale_order_id'];
                $saleOrderArr = SaleOrderView::findAllByAttributes(['id'=>$saleOrderIds,'status'=>1],'id,prod_name,brand_name,class_name,type_name','id');

                //服务内容
                $serviceContent = '';
                if(isset($saleOrderArr[$saleOrderIds])){
                    $serviceContent = $saleOrderArr[$saleOrderIds]['brand_name'].'-'.$saleOrderArr[$saleOrderIds]['class_name'].'-'.$workTypeDesc;
                }
                //收费项目
                $data['work_cost_list'] = [];

                $unitArr = self::getUnit(self::getDepartmentId());

                if($data['work_cost']) foreach ($data['work_cost'] as $key=>$val)
                {
                    $data['work_cost'][$key]['work_cost_name'] = '';
                    $data['work_cost'][$key]['cost_payer']     = '';
                    $data['work_cost'][$key]['unit']     = '';
                    $cost_item_info = CostLevelItem::getOnes(['cost_item_id'=>$val['cost_id']]);
                    $cost_pay_info  = CostPayType::getOne(['id'=>$val['payer_id']]);
                    $data['work_cost'][$key]['work_cost_name'] = isset($cost_item_info['cost_item_name'])?$cost_item_info['cost_item_name']:'';
                    $data['work_cost'][$key]['cost_payer']     = isset($cost_pay_info['type_name'])?$cost_pay_info['type_name']:'';
                    $data['work_cost'][$key]['unit']     = isset($cost_item_info['unit'])?$cost_item_info['unit']:'';

                    //判断费用类型，1：服务费，2：配件费
                    $cost_type =1;
                    if(key_exists('cost_type',$val))
                    {
                        $cost_type = $val['cost_type'];
                    }
                    if($cost_type == 2)
                    {
                        $prod_info = Product::getOne(['id'=>$val['cost_id']]);
                        if($prod_info)
                        {
                            //单位从dict_enum中获取,名称从product中获取
                            $unit_id = isset($prod_info['unit_id'])?$prod_info['unit_id']:1;
                            $data['work_cost'][$key]['unit']     = isset($unitArr)?$unitArr[$unit_id]:'';
                            $data['work_cost'][$key]['work_cost_name'] = isset($prod_info['prod_name'])?$prod_info['prod_name']:'';
                        }

                    }



                }

                //技师信息
                $technician_info = [];
                $jsIds = [];
                if(isset($data['technicianArr']) && $data['technicianArr']){
                    foreach ($data['technicianArr'] as $val){
                        $jsIds[] = $val['technician_id'];
                    }
                }

                if($jsIds)
                {
                    $technician_info = Common::getTechniciansInfo($jsIds,'id,name,mobile,icon');
                    if($technician_info)
                    {
                        foreach ($data['technicianArr'] as $key=>$val)
                        {
                            $avatar = $name = $mobile = '';
                            if(isset($technician_info[$val['technician_id']]))
                            {
                                $name   = $technician_info[$val['technician_id']]['name'];
                                $mobile = $technician_info[$val['technician_id']]['mobile'];
                                $avatar = $technician_info[$val['technician_id']]['icon'];
                            }
                            $title = '协助';
                            if($val['is_self'] == 1){
                                $title = '负责人';
                            }
                            else if($val['is_self'] == 2){
                                $title = '组长';
                            }

                            $data['technicianArr'][$key]['name']   = $name;
                            $data['technicianArr'][$key]['mobile'] = $mobile;
                            $data['technicianArr'][$key]['avatar'] = trim($avatar)==''?'/images/morentouxiang.png':$avatar;
                            $data['technicianArr'][$key]['title']  = $title;
                        }
                    }
                    else
                    {
                        foreach ($data['technicianArr'] as $key=>$val)
                        {
                            $data['technicianArr'][$key]['name']   = '';
                            $data['technicianArr'][$key]['mobile'] = '';
                            $data['technicianArr'][$key]['avatar'] = '/images/morentouxiang.png';
                            $data['technicianArr'][$key]['title']  = '';
                        }
                    }
                }

                //联系地址
                $addressDesc = Region::getAddress($data['order_info']['address_id'], $type = 1);
                $data['account_name']   = $accountName;
                $data['account_mobile'] = $accountMobile;
                if (isset($data['plan_time']) && !empty($data['plan_time'])) {
                    //工单预约时间
                    $data['work_plan_time'] = Order::getPlantimeType($data['plan_time_type'],date('Y-m-d H:i',$data['plan_time']));
                }else{
                    $data['work_plan_time'] = '待定';  //工单预约时间
                }

                $accountAddressArr = AccountAddress::findOneByAttributes(['id'=>$data['order_info']['address_id']]);

                //$data['plan_time']      = date('Y-m-d H:i',$data['order_info']['plan_time']); //订单预约时间
                $data['plan_time']      = $data['plan_time']?Order::getPlantimeType($data['plan_time_type'],date('Y-m-d H:i',$data['plan_time'])):'待定'; //订单预约时间
                $data['is_scope']       = $data['order_info']['is_scope'];
                $data['create_time']    = date('Y-m-d H:i',$data['order_info']['create_time']);
                $data['word_type_desc'] = $workTypeDesc;
                $data['address_desc']   = $addressDesc['address'];
                $data['conact_name']    = $accountAddressArr?$accountAddressArr['conact_name']:'';
                $data['conact_phone']   = $accountAddressArr?$accountAddressArr['mobile_phone']:'';
                $data['amount']         = $data['order_info']['amount']/100;
                $data['create_user']    = User::getCreateName($data['order_info']['create_user_id']);
                $data['service_content']= $serviceContent;
                $data['scope_img'] = isset($data['order_info']['scope_img'])?explode(',',$data['order_info']['scope_img']):'';  //质保凭证
                $data['fault_img'] = isset($data['order_info']['fault_img'])?explode(',',$data['order_info']['fault_img']):'';  //故障图片
                $data['description']= $data['order_info']['description'];
            }
            return self::success($data);
        }else {
            return self::error($result['message'],$result['code']);
        }
    }
    /**
     * 获取服务流程
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function getServiceFlow($type_id){

        $res = ServiceType::findOne(['id'=>$type_id,'status'=>1]);
        if($res){
            //$data = ServiceFlow::getList(['type_id' => $type_id,'department_id'=>BaseLogic::getDirectCompanyId()]);
            $data = ServiceFlow::getList(['type_id' => $type_id,'department_id'=>BaseLogic::getTopId()]);
            if($data){
                $data = array_column($data,'title','id');
            }
        }
        //print_r($data);die;
        //$data = ServiceFlow::getList(['type_id' => $type_id,'department_id'=>'']);
        return $data;
    }
    /**
     * 添加工单
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function add()
    {
        $order_no      = trim(yii::$app->request->post('order_no',''));    //订单号
        $work_stage    = trim(yii::$app->request->post('work_stage',''));  //服务流程id
        
        $selectDate   = Yii::$app->request->post('selectDate','');  //上门时间
        $planTimeType = Yii::$app->request->post('plan_time_type','');  //上门时间
        $setTime      = Yii::$app->request->post('setTime','');  //上门时间
        $planTime = Helper::changeTime($planTimeType,$selectDate,$setTime);//服务时间
        $reason        = yii::$app->request->post('reason','');      //原因
        $reason_detail = yii::$app->request->post('reason_detail','');  //原因详情

        if ($order_no == '') {
            return self::error('订单号不能为空!',20003);
        }
        if ($work_stage == '') {
            return self::error('服务流程不能为空!',20004);
        }
        if ($planTime == '') {
            return self::error('服务时间不能为空!',20005);
        }
        if ($reason == '') {
            return self::error('原因不能为空!',20006);
        }
        switch ($reason)
        {
            case 1:
                $reason = "技师原因";
                break;
            case 2:
                $reason = "客户原因";
                break;
            case 3:
                $reason = "其他";
                break;
            default:
                $reason = "其他";
        }

        //添加工单
        $url = Yii::$app->params['order.uservices.cn']."/v1/work/add";
        $postData = [
            'department_id'         => self::getDepartmentId(),
            'process_user_id' => self::getLoginUserId(),
            'order_no'       => $order_no,
            'work_stage'     => $work_stage,
            'plan_time'      => $planTime,
            'plan_time_type' => $planTimeType,
            'reason'         => $reason,
            'reason_detail'  => $reason_detail,
            'assign_technician_type' =>1,
            'assign_plantime_type' =>1
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        if(isset($result['success']) && $result['success'] == 1 )
        {
            return self::success($result['data']);
        }
        else {
            return self::error($result['message'],$result['code']);
        }
    }
    /**
     * 获取收费项目详情
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function getCostDetail($id='',$department_id=''){
        //$id     = Yii::$app->request->get('id','');
        if ($id == '') {
            return self::error('工单收费项目id不能为空!',20003);
        }
        //查询收费项目详情
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-cost/detail";
        $postData = [
            'id'       => $id
        ];
        //print_r($postData);die;
        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        //print_r($result);die;
        if(isset($result['success']) && $result['success'] == 1 ){
            $data = $result['data'];
            if($data){
                //获取下单机构的收费项目
                $costItemData = [];
                if($department_id){
                    //根据部门id获取直属公司id
                    $depart = Department::find()->where(['id'=>$department_id])->asArray()->one();
                    $costItemList = CostItem::getList(['status'=>1,'direct_company_id'=>$depart['direct_company_id']]);
                    $costItemData = CostItemSet::find()
                        ->where(['status'=>1,'del_status'=>1,'id'=>array_column($costItemList,'cost_item_set_id')])
                        ->asArray()->all();
                    $costItemData = array_column($costItemData,'name','id');
                }
                //print_r($costItemData);die;
                $data['work_cost_name'] = '';
                $data['cost_payer']     = '';
                //$cost_item_info = CostItem::getOne(['cs_id'=>$data['cost_id']]);
                //查询已经添加的收费项目详情
                $cost_item_info = CostItemSet::find()->where(['id'=>$data['cost_id']])->select('name')->asArray()->one();
                if($data['cost_type']==1){
                    $cost_item_info = CostLevelItem::getOnes(['cost_item_id'=>$data['cost_id']]);
                    $cost_pay_info  = CostPayType::getOne(['id'=>$data['payer_id']]);
                    //$data['work_cost_name'] = isset($cost_item_info['cost_item_name'])?$cost_item_info['cost_item_name']:'';
                    //$data['work_cost_name'] = isset($costItemData[$data['cost_id']])?$costItemData[$data['cost_id']]:'';
                    $cost_item_info = CostItemSet::find()->where(['id'=>$data['cost_id']])->select('name')->asArray()->one();
                    $data['work_cost_name'] = isset($cost_item_info['name'])?$cost_item_info['name']:'';
                    $data['cost_payer']     = isset($cost_pay_info['type_name'])?$cost_pay_info['type_name']:'';
                    $data['guide_price'] = isset($cost_item_info['price'])?$cost_item_info['price']:'';
                    $data['unit'] = isset($cost_item_info['unit'])?$cost_item_info['unit']:'';
                }else{
                    $data['work_cost_name'] = "配件费";
                    $data['cost_id'] = 48;
                    $cost_pay_info  = CostPayType::getOne(['id'=>$data['payer_id']]);
                    $data['cost_payer']     = isset($cost_pay_info['type_name'])?$cost_pay_info['type_name']:'';
                    $data['guide_price'] = isset($cost_item_info['price'])?$cost_item_info['price']:'';
                    $prodId = $data['cost_id'];
                    $data['product'] = Product::getOne(['id'=>$prodId],1);
                }
            }
            return self::success($data);
        }else {
            return self::error($result['message'],$result['code']);
        }
    }
    /**
     * 添加工单收费项目
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function addWorkCost()
    {
        if(Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $work_no = trim(yii::$app->request->post('work_no', ''));       //工单号
            $payer_id = trim(yii::$app->request->post('payer_id', ''));      //付费方id
            $cost_id = trim(yii::$app->request->post('cost_id', ''));      //收费项目id
            $cost_price = trim(yii::$app->request->post('cost_price', 0));    //收费价格
            $cost_number = yii::$app->request->post('cost_number', '');        //收费数量
            $comment = yii::$app->request->post('comment', '');            //备注
            $prod_id  = Yii::$app->request->post('prod_id','0');
            $cost_type = Yii::$app->request->post('cost_type','0');
            $storageSwitch = Yii::$app->request->post('storageSwitch','0');//仓储开关,1开，2关
            if($prod_id && $cost_type==2){
                $cost_id = $prod_id;
            }
            $cost_amount = $cost_price*$cost_number;
            //计算实际总额
            $cost_real_amount = $cost_price*$cost_number;
            //凭证图片
            $voucher_img = '';
            $file = Yii::$app->request->post("WorkCostForm", '');
            if ($file['voucher_img']) {
                $voucher_img = implode(',', $file['voucher_img']);
            }
            //添加收费项目
            $url = Yii::$app->params['order.uservices.cn'] . "/v1/work-cost/add";
            $postData = [
                'src_type' => '13',
                //'src_id' => self::getManufactorId(),
                'work_no' => $work_no,
                'process_user_id' => self::getLoginUserId(),
                'payer_id' => $payer_id,
                'cost_id' => $cost_id,
                'cost_price' => $cost_price,
                'cost_number' => $cost_number,
                'cost_amount' => $cost_amount,
                'cost_real_amount' => $cost_real_amount,
                'comment' => $comment,
                'voucher_img' => $voucher_img,
                'cost_type' =>$cost_type
            ];
            $jsonStr = Helper::curlPostJson($url, $postData);
            $jsonArr = json_decode($jsonStr, true);
            if (isset($jsonArr['success']) && $jsonArr['success'] == 1) {
                if($storageSwitch==1){//仓储开关打开时，技师库存减掉当前使用数量
                    //TechStorageLogic::updateStorage($technicianId,$prod_id,$cost_number);
                }
                return self::success(['id' => $jsonArr['data']['id'],'work_no'=>$work_no]);
            }
            return self::error($jsonArr['message'], 20004);
        }
    }


    public static function editWorkCost(){
        if(Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $id = trim(yii::$app->request->post('work_cost_id', ''));       //当前编辑的收费项id
            $work_no = trim(yii::$app->request->post('work_no', ''));       //工单号
            $payer_id = trim(yii::$app->request->post('payer_id', ''));      //付费方id
            $cost_id = trim(yii::$app->request->post('cost_id', ''));      //收费项目id
            $cost_price = trim(yii::$app->request->post('cost_price', 0));    //收费价格
            $guide_price = trim(yii::$app->request->post('guide_price', 0));    //建议价格
            $cost_number = yii::$app->request->post('cost_number', '');        //收费数量
            $cost_amount = yii::$app->request->post('cost_amount', '');        //费用总金额
            $cost_real_amount = yii::$app->request->post('cost_real_amount', '');   //费用实际总金额
            $comment = yii::$app->request->post('comment', '');            //备注
            $prod_id  = Yii::$app->request->post('prod_id','0');
            $cost_type = Yii::$app->request->post('cost_type','0');
            $storageSwitch = Yii::$app->request->post('storageSwitch','0');//仓储开关,1开，2关
            $technicianId  = Yii::$app->request->post('technician_id','0');
            $lasCostNum    = Yii::$app->request->post('last_cost_num','0');
            if($prod_id && $cost_type==2){
                $cost_id = $prod_id;
            }
            //计算总额
            //$cost_item_info= CostLevelItem::getOnes(['id'=>$cost_id]);
            //$guide_price = isset($cost_item_info['price'])?$cost_item_info['price']:'0';
            $cost_amount = $guide_price*$cost_number;
            //计算实际总额
            $cost_real_amount = $cost_price*$cost_number;
            //凭证图片
            $voucher_img = '';
            $file = Yii::$app->request->post("WorkCostForm", '');
            if ($file['voucher_img']) {
                $voucher_img = implode(',', $file['voucher_img']);
            }
            $workCostInfo = WorkCostForm::findOne(['id'=>$id]);

            //添加收费项目
            $url = Yii::$app->params['order.uservices.cn'] . "/v1/work-cost/edit";
            $postData = [
                'id'    =>$id,
                'src_type' => '13',
                //'src_id' => self::getManufactorId(),
                'work_no' => $work_no,
                'process_user_id' => self::getLoginUserId(),
                'payer_id' => $payer_id,
                'cost_id' => $cost_id,
                'cost_price' => $cost_price,
                'cost_number' => $cost_number,
                'cost_amount' => $cost_amount,
                'cost_real_amount' => $cost_real_amount,
                'comment' => $comment,
                'voucher_img' => $voucher_img,
                'cost_type' =>$cost_type
            ];
            $jsonStr = Helper::curlPostJson($url, $postData);
            $jsonArr = json_decode($jsonStr, true);
            if (isset($jsonArr['success']) && $jsonArr['success'] == 1) {
                /*if($storageSwitch==1){//仓储开关打开时，技师库存减掉当前使用数量
                    if($cost_type==2){
                        if($workCostInfo->cost_id!=$prod_id){
                            $lastNum = -$workCostInfo->cost_number;//当本次产品不是上次产品时，库存还原
                            TechStorageLogic::updateStorage($technicianId,$workCostInfo->cost_id,$lastNum);
                        }else{
                            if($lasCostNum>$cost_number){
                                $cost_number = -($lasCostNum-$cost_number);
                            }else{
                                $cost_number = $cost_number-$lasCostNum;
                            }
                        }

                    }else{
                        $cost_number = -$cost_number;//当仓储打开不是备件费时，库存还原
                    }

                    TechStorageLogic::updateStorage($technicianId,$prod_id,$cost_number);
                }*/
                return self::success(['id' => $jsonArr['data']['id'],'work_no'=>$work_no]);
            }
            return self::error($jsonArr['message'], 20004);
        }
    }




    /**
     * 添加编辑工单收费项目时，获取详情
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function getCostTerms($department_id='',$work_no='',$work_cost_id='',$type='',$type_id='',$standard_id=''){
        $data = [];
        $model = new WorkCostForm();
        //获取下单机构的收费项目
        if($department_id){
            //根据部门id获取直属公司id
            $depart = Department::find()->where(['id'=>$department_id])->asArray()->one();
            $costItemList = CostItem::getList(['status'=>1,'direct_company_id'=>$depart['direct_company_id']]);
            $costItemData = CostItemSet::find()
                ->where(['status'=>1,'del_status'=>1,'id'=>array_column($costItemList,'cost_item_set_id')])
                ->asArray()->all();
            $costItemData = array_column($costItemData,'name','id');
        }
        //print_r($costItemData);die;
        $cost_pay_list  = CostPayType::getList();
        $model = new WorkCostForm();
        $data['storageSwitch']    = [];
        $data['cost_item_list']    = $costItemData;
        $data['cost_pay_list']    = $cost_pay_list;
        $data['model'] = $model;
        if($work_no){
            //$work_info = self::getWorkDetails($work_no);
            //$order['brand_id'] = $work_info['data']['brand_id'];
            //$order['class_id'] = $work_info['data']['class_id'];
            //$order['type_id']  = $work_info['data']['order_info']['work_type'];
            //获取收费项目列表
            //$cost_item_list = CostItem::getCost($type,$type_id,$standard_id,'',$order);
        }
        if($work_cost_id){
            $work_info = self::getCostDetail($work_cost_id);
        }
        $data['work_info'] = isset($work_info['data'])?$work_info['data']:[];
        return $data;

        //获取付费方列表
        $cost_pay_list  = CostPayType::getList();
        $cost_item_list = [];
        $work_info = self::getWorkDetails($work_no);
        $order['brand_id'] = $work_info['data']['brand_id'];
        $order['class_id'] = $work_info['data']['class_id'];
        $order['type_id']  = $work_info['data']['order_info']['work_type'];
        $store_id = isset($work_info['data']['order_standard']['type_id'])?$work_info['data']['order_standard']['type_id']:'';
        //获取收费项目列表
        $cost_item_list = CostItem::getCost($type,$type_id,$standard_id,'',$order,$store_id);
        if($work_cost_id){
            $work_info = self::getCostDetail($work_cost_id);
        }
        $data['work_info'] = isset($work_info['data'])?$work_info['data']:[];
        $data['work_no'] = $work_no;
        $data['cost_pay_list'] = $cost_pay_list;
        $data['cost_item_list'] = $cost_item_list;
        $data['model'] = $model;
        print_r($cost_item_list);die;
        return $data;
    }






    //修改技师分成
    public static function EditDivide($id,$divideAmount,$comment)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/cost-divide-edit";

        $postData = [
            'id'            => $id,
            'divide_amount' => $divideAmount*10000,
            'comment'       => $comment
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        //print_r($jsonArr);die;
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            //print_r(self::success(['id'=>$id]));die;
            return self::success(['id'=>$id]);
        }
        return self::error($jsonArr['message'],20004);
    }






    public static function getWorkDetails($work_no){
        //$work_no= trim(Yii::$app->request->get('work_no',''));
        if(!$work_no){
            return self::error('工单号不能为空！',20004);
        }
        //添加工单
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/detail";
        $postData = [
            'department_id' => self::getDepartmentId(),
            'work_no'       => $work_no
        ];
        //print_r($postData);die;
        $jsonStr = Helper::curlPostJson($url, $postData);
        $result  = json_decode($jsonStr,true);
        if(isset($result['success']) && $result['success'] == 1 ){
            $data = $result['data'];
            //print_r($data);die;
            if($data){
                $accountName   = '';
                $accountMobile = '';
                $workTypeDesc  = '';

                $accountArr = Account::findOneByAttributes(['id'=>$data['order_info']['account_id']],'id,account_name,mobile');
                if($accountArr){
                    $accountName   = $accountArr['account_name'];
                    $accountMobile = $accountArr['mobile'];
                }

                //服务类型
                $workTypeArr = self::getTypeList();
                if(isset($workTypeArr['data'][$data['order_info']['work_type']])){
                    $workTypeDesc = $workTypeArr['data'][$data['order_info']['work_type']];
                }
                //服务产品
                $saleOrderIds = $data['order_info']['sale_order_id'];
                $saleOrderArr = SaleOrderView::findAllByAttributes(['id'=>$saleOrderIds,'status'=>1],'id,prod_name,brand_name,brand_id,class_name,class_id,type_name','id');
                //服务内容
                $serviceContent = '';
                if(isset($saleOrderArr[$saleOrderIds])){
                    $serviceContent = $saleOrderArr[$saleOrderIds]['brand_name'].'-'.$saleOrderArr[$saleOrderIds]['class_name'].'-'.$workTypeDesc;
                }
                //收费项目
                $data['work_cost_list'] = [];
                if($data['work_cost']) foreach ($data['work_cost'] as $key=>$val){
                    $data['work_cost'][$key]['work_cost_name'] = '';
                    $data['work_cost'][$key]['cost_payer']     = '';
                    $data['work_cost'][$key]['unit']     = '';
                    //$cost_item_info = CostItem::getOne(['cs_id'=>$val['cost_id']]);
                    $cost_item_info = CostLevelItem::getOnes(['cost_item_id'=>$val['cost_id']]);
                    $cost_pay_info  = CostPayType::getOne(['id'=>$val['payer_id']]);
                    //$data['work_cost'][$key]['work_cost_name'] = isset($cost_item_info['cs_name'])?$cost_item_info['cs_name']:'';
                    $data['work_cost'][$key]['work_cost_name'] = isset($cost_item_info['cost_item_name'])?$cost_item_info['cost_item_name']:'';
                    $data['work_cost'][$key]['cost_payer']     = isset($cost_pay_info['type_name'])?$cost_pay_info['type_name']:'';
                    $data['work_cost'][$key]['unit']     = isset($cost_item_info['unit'])?$cost_item_info['unit']:'';
                }
                //技师信息
                $technician = $technician_info = [];
                $technician_id = isset($data['technician_id'])?$data['technician_id']:'';
                $technician = Technician::getOne(['id'=>$technician_id]);
                //获取技师所属服务商信息
                $technician_store = ManufactorLogic::getOne($technician['store_id']);
                $technician_info['id'] = isset($data['technician_id'])?$data['technician_id']:'';
                $technician_info['technician_number'] = isset($technician['technician_number'])?$technician['technician_number']:'';
                $technician_info['name']       = isset($technician['name'])?$technician['name']:'';
                $technician_info['sex']        = isset($technician['sex'])?$technician['sex']:'';
                $technician_info['mobile']     = isset($technician['mobile'])?$technician['mobile']:'';
                $technician_info['icon']       = isset($technician['icon'])?$technician['icon']:'';
                $technician_info['store_name'] = isset($technician_store['model']['company'])?$technician_store['model']['company']:'';
                //print_r($technician_info);die;
                //联系地址
                $addressDesc = Region::getAddress($data['order_info']['address_id'], $type = 1);
                $data['account_name']   = $accountName;
                $data['account_mobile'] = $accountMobile;
                $data['work_plan_time'] = date('Y-m-d H:i',$data['plan_time']);  //工单预约时间
                $data['plan_time']      = date('Y-m-d H:i',$data['order_info']['plan_time']); //订单预约时间
                $data['is_scope']       = $data['order_info']['is_scope'];
                $data['create_time']    = date('Y-m-d H:i',$data['order_info']['create_time']);
                $data['word_type_desc'] = $workTypeDesc;
                $data['address_desc']   = $addressDesc['address'];
                $data['conact_name']    = AccountAddress::getConactName($data['order_info']['address_id']);
                $data['amount']         = $data['order_info']['amount']/100;
                $data['create_user']    = User::getCreateName($data['order_info']['create_user_id']);
                $data['service_content']= $serviceContent;
                $data['scope_img'] = isset($data['order_info']['scope_img'])?explode(',',$data['order_info']['scope_img']):'';  //质保凭证
                $data['fault_img'] = isset($data['order_info']['fault_img'])?explode(',',$data['order_info']['fault_img']):'';  //故障图片
                $data['description']= $data['order_info']['description'];
                $data['className']= $saleOrderArr[$saleOrderIds]['class_name'];
                $data['typeName'] = $workTypeDesc;
                $data['class_id'] = $saleOrderArr[$saleOrderIds]['class_id'];
                $data['brand_id'] = $saleOrderArr[$saleOrderIds]['brand_id'];
                    //技师信息
                $data['technician']= $technician_info;
            }
            return self::success($data);
        }else {
            return self::error($result['message'],$result['code']);
        }
    }

    /**
     * 开始服务
     * @param $workNo
     * @return array
     */
    public static function startService($workNo,$type)
    {
        $url = Yii::$app->params['order.uservices.cn']."/v1/work-pc/begin-service";
        $postData = [
            'type'             => $type,
            'work_no'          => $workNo,
            'department_id'    => self::getDepartmentId(),
            'operator_user_id' => self::getLoginUserId()
        ];

        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);

        if( isset($jsonArr['success']) && $jsonArr['success']== 1 )
        {
            return self::success([
                'work_no' => $workNo
            ]);
        }
        return self::error($jsonArr['message'],$jsonArr['code']);
    }


}
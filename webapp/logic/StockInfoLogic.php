<?php
    /**
     * Created by PhpStorm.
     * User: xingq
     * Date: 2018/3/22
     * Time: 10:09
     */
    
    namespace webapp\logic;
    
    
    use common\models\ReturnPurchase;
    use common\models\ReturnPurchaseDetail;
    use webapp\models\TechProdStockDetail;
    use webapp\models\TechStorage;
    use Yii;
    use webapp\models\BaseModel;
    use webapp\models\AdminUser;
    use common\models\Depot;
    use common\models\Purchase;
    use common\models\DictEnum;
    use common\models\StockInfo;
    use common\models\StockDetail;
    use common\helpers\CreateNoRule;
    use common\models\DepotProdStock;
    use common\models\PurchaseDetail;
    use webapp\models\TechProdStock;
    use common\models\Contract;
    use common\models\ContractDetail;
    use common\models\ExchangeGoods;
    use common\models\ExchangeGoodsDetail;
    use common\models\ReturnGoods;
    use common\models\ReturnGoodsDetail;
    use common\models\SendOut;
    use common\models\PickUser;
    use webapp\models\Department;
    
    class StockInfoLogic extends BaseLogic
    {
        /**
         * 搜索数据处理
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/23
         * Time: 13:24
         * @return array
         */
        public static function search()
        {
            
            $no              = trim(Yii::$app->request->get('no',''));  //入库单号
            $subject         = trim(Yii::$app->request->get('subject',''));  //入库主题
            $stockType       = intval(Yii::$app->request->get('stock_type',0)); //入库类型
            $startCreateTime = trim(Yii::$app->request->get('start_create_time',0)); //申请开始时间
            $endCreateTime   = trim(Yii::$app->request->get('end_create_time',0));   //申请结束时间
            $startAuditDate  = trim(Yii::$app->request->get('start_audit_date',0));  //审批开始时间
            $endAuditDate    = trim(Yii::$app->request->get('end_audit_date',0));   //审批结束时间
            $isship          = intval(Yii::$app->request->get('isship',0)); //发货状态   出库搜索使用
            $id              = trim(Yii::$app->request->get('id',''));  //出入库id
            
            $where = [];
            if ($id != '') {
                $where[] = ['=','rel_id',$id];
            }
            if ($no != '') {
                $where[] = ['like','no',$no];
            }
            if ($subject != '') {
                $where[] = ['like','subject',$subject];
            }
            if ($stockType != 0) {
                $where[] = ['=','stock_type',$stockType];
            }
            if ($isship != 0) {
                $where[] = ['=','isship',$isship];
            }
            if ($startCreateTime != 0) {
                $where[] = ['>','create_time',$startCreateTime];
            }
            if ($endCreateTime != 0) {
                $where[] = ['<','create_time',$endCreateTime];
            }
            if ($startAuditDate != 0) {
                $where[] = ['>','audit_date',$startAuditDate];
            }
            if ($endAuditDate != 0) {
                $where[] = ['<','audit_date',$endAuditDate];
            }
            
            return $where;
        }
        
        /**
         * 列表数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/23
         * Time: 13:24
         * @param string $type
         * @param int $status
         * @return array
         */
        public static function index($type= '',$status = '')
        {
            $page     = intval(Yii::$app->request->get('page',1));
            $pageSize = intval(Yii::$app->request->get('pageSize',10));
            if($status){
                $map = [
                    'direct_company_id'=>BaseLogic::getDirectCompanyId(),
                    'type'=>$type,
                    'status'=>$status
                ];
            }else{
                $map = [
                    'direct_company_id'=>BaseLogic::getDirectCompanyId(),
                    'type'=>$type
                ];
            }

            $where = self::search();
            $data = StockInfo::index($map,$where,$page,$pageSize);
            if (!empty($data['list'])) {
                $res = self::changeData($data['list']);
                $data['list'] = $res;
            }

            //获取出入库类型列表
            $stockTypeList = self::getStockType($type);
            if ($stockTypeList['code'] == 200) {
                $data['stockTypeList'] = $stockTypeList['data'];
            }
            //print_r($data['list']);die;
            return $data;
        }
        
        /**
         * 添加页面所需数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/23
         * Time: 13:23
         * @param int $type
         * @return array
         */
        public static function beforeList($type = 1)
        {
            $directCompanyId   = BaseLogic::getDirectCompanyId();

            $data = [];
            $data['model']['type'] = $type;
            
            $relId   = intval(Yii::$app->request->get('rel_id',0));   //关联单id
            $relType = intval(Yii::$app->request->get('rel_type',0));   //关联单类型
            
            if ($relType && $relId) {
                $relName = self::getRelName($directCompanyId,$relType,$relId);
                $relInfo = StockInfo::getOne(['direct_company_id'=>$directCompanyId,'rel_type'=>$relType,'rel_id'=>$relId]);
                if ($relName['code'] != 200) {
                    return $relName;
                }
                $data['model']['rel_type'] = $relType;
                $data['model']['rel_id']   = $relId;
                $data['model']['rel_name'] = $relName['data']['relName'];
                $data['model']['rel_no'] = isset($relName['data']['relNo'])?$relName['data']['relNo']:'';
                $data['model']['stock_type'] = isset($relInfo['stock_type'])?$relInfo['stock_type']:'';
                if (isset($relName['data']['product'])) {
                    $data['product'] = $relName['data']['product'];
                }
                
            }
            $data['model']['apply_user_id']   = Yii::$app->user->identity->id;
            $data['model']['apply_user_name'] = Yii::$app->user->identity->username;
            //根据关联单 判断历史数量 是否还可创建出入库
            //查询历史
            $old        = StockInfo::getList(['direct_company_id'=>$directCompanyId,'rel_type'=>$relType,'rel_id'=>$relId,'stock_type'=>[1,2,3,4,5,7,8]]);
            $parent_ids = array_column($old,'id');
            $detail     = StockDetail::findOneByAttributes(['parent_id'=>$parent_ids],"sum(prod_num) as prod_num");
            //查询关联单 产品总数
            switch ($relType) {
                //1 采购单表： purchas   subject
                case 1:
                   $purchase = Purchase::findOneByAttributes(['id'=>$relId],"id");
                   $purchaseDetail = PurchaseDetail::findOneByAttributes(['parent_id'=>$purchase['id']],"sum(prod_num) as total_num");
                   if(intval($detail['prod_num']) !=0 && intval($detail['prod_num']) >= $purchaseDetail['total_num']){
                       return self::error('剩余产品不足',20003);
                   }
                   break;
                //2 合同： contract      subject
                case 2:
                    $contract = Contract::findOneByAttributes(['id'=>$relId],"id");
                    $contractDetail = ContractDetail::findOneByAttributes(['parent_id'=>$contract['id']],"sum(prod_num) as total_num");
                    //查询退换货
                    $return       = ReturnGoods::findAllByAttributes(['contract_id'=>$relId]);
                    $returnIds    = array_column($return,'id');
                    $returnDetail = ReturnGoodsDetail::findOneByAttributes(['parent_id'=>$returnIds],"sum(finish_num) as return_num");

                    $exchang  = ExchangeGoods::findAllByAttributes(['contract_id'=>$relId]);
                    $exchangIds    = array_column($exchang,'id');
                    $exchangOldDetail = ExchangeGoodsDetail::findOneByAttributes(['parent_id'=>$exchangIds,'type'=>1],"sum(finish_num) as ex_old_sum");
                    $exchangNewDetail = ExchangeGoodsDetail::findOneByAttributes(['parent_id'=>$exchangIds,'type'=>2],"sum(finish_num) as ex_new_sum");

                    $toUse = intval($contractDetail['total_num']) - intval($returnDetail['return_num']) + intval($exchangNewDetail['ex_new_sum'])-intval($exchangOldDetail['ex_old_sum']);
                    if(intval($contractDetail['total_num']) !=0 && intval($contractDetail['total_num']) >= $toUse){
                        return self::error('剩余产品不足',20003);
                    }
                    break;

                //3 退货单表： return    subject
                case 3:

                    break;

                //4 换货表： exchange    subject
                case 4:

                    break;

                // stock_type   类型，1：申请，2：归还
                //5 技师申请： tech_prod_stock       subject
                case 5:

                    break;
                //6 退还备件表： tech_prod_stock      subject
                case 6:

                    break;
                //7 采购退货单表： return    subject
                case 7:

                    break;
                default:

                    break;
            }
            //入库
            if ($type == 1) {
                //入库单编号
                $data['model']['no'] = CreateNoRule::getNo('RK');
                
                //入库类型
                $where = ['dict_key'=>'enum_fws_stock_in_type'];
                $dict = DictEnum::getList($where);
                if (!$dict) {
                    return self::error('入库类型获取失败',20001);
                }
            }
            
            //出库
            if ($type == 2) {
                //出库单编号
                $data['model']['no'] = CreateNoRule::getNo('CK');
                
                $data['model']['serial_number'] = 0;
                //出库类型
                if($relType == 7){ //采购退货
                    $where = ['dict_key'=>'enum_fws_stock_out_type','dict_enum_id'=>9];
                }else{
                    $where = ['dict_key'=>'enum_fws_stock_out_type'];
                }
                $dict = DictEnum::getList($where);
                if (!$dict) {
                    return self::error('出库类型获取失败',20001);
                }
            }
            //出入库类型列表
            foreach ($dict as $val) {
                $data['stock_type'][$val['dict_enum_id']] = $val['dict_enum_value'];
            }
    
            //根据关联单类型获取出入库类型
            $stockType = self::getStockTypeFromRelType($relType,$type);
            if ($stockType) {
                $data['model']['stock_type'] = $stockType;
            }
            
            //库房
            $data['depot'] = self::getDepot($directCompanyId);
            return self::success($data,'成功');
            
        }
        
        /**
         * 添加页面传值处理
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/23
         * Time: 13:23
         * @return array
         */
        public static function receivePost()
        {
            //$postData = Yii::$app->request->post();
            //print_r($postData);die;
            $no          = trim(Yii::$app->request->post('no',0));  //编号
            $stockType   = intval(Yii::$app->request->post('stock_type',0));  //类型
            $relId       = intval(Yii::$app->request->post('rel_id',0));    //关联单id
            $relType     = intval(Yii::$app->request->post('rel_type',0));    //关联单id
            $applyUserId = intval(Yii::$app->request->post('apply_user_id',0)); //创建人id
            $description = trim(Yii::$app->request->post('description',''));    //备注
            $prod        = Yii::$app->request->post('prod','');   //产品数据
            $subject     = trim(Yii::$app->request->post('subject',''));   //入库主题

            $pickUserId = intval(Yii::$app->request->post('pick_user_id',0));   //提货人id
            $pickUserInfo = trim(Yii::$app->request->post('pick_user_info',''));   //提货人信息
            $departmentId = intval(Yii::$app->request->post('department_id',0));   //提货归属部门
            $data = [];
            //验证返回
            if ($stockType == 0) {
                return self::error('类型不能为空',20001);
            }else{
                $data['stockInfo']['stock_type'] = $stockType;
            }
//            if($subject == ''){
//                return self::error('主题不能为空',20004);
//            }else{
//                $data['stockInfo']['subject'] = $subject;
//            }
            $data['stockInfo']['subject'] = $subject; // 修改为可不填
            if (empty($prod)) {
                return self::error('产品不能为空',20002);
            }
            
            $num = array_sum(array_column($prod,'prod_num'));
            if ($num == 0) {
                return self::error('产品数量不能为空',20003);
            }
            
            foreach ($prod as $key => $val) {
                if ($val['prod_num'] == 0) {
                    unset($prod[$key]);
                }
            }
            $data['stockDetail'] = $prod;
            
            if($no != '') {
                $data['stockInfo']['no'] = $no;
            }
            if($relId != '') {
                $data['stockInfo']['is_rel']   = 1;
                $data['stockInfo']['rel_id']   = $relId;
                $data['stockInfo']['rel_type'] = $relType;
            }else{
                $data['stockInfo']['is_rel'] = 0;
            }
            if($applyUserId != '') {
                $data['stockInfo']['apply_user_id'] = $applyUserId;
            }
            if($description != '') {
                $data['stockInfo']['description'] = $description;
            }
            $data['stockInfo']['pick_user_id'] = $pickUserId;
            //$data['stockInfo']['pick_user_info'] = $pickUserInfo;
            $data['stockInfo']['department_id'] = $departmentId;
            
            return self::success($data,'成功');
            
        }
        
        /**
         * 添加
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/23
         * Time: 13:23
         * @param string $type
         * @return array
         * @throws \Exception
         */
        public static function add($type = '')
        {
            $data = self::receivePost();
            if ($data['code'] != 200) {
                return $data;
            }
            $stockInfo = [];
            if (isset($data['data']['stockInfo'])) {
                $stockInfo = $data['data']['stockInfo'];
            }
            $stockDetail = [];
            if (isset($data['data']['stockDetail'])) {
                $stockDetail = $data['data']['stockDetail'];
            }
            
            $directCompanyId = BaseLogic::getDirectCompanyId();
            $time = date("Y-m-d H:i:s");
            
            //组合数据
            //出入库单数据
            $stockInfo['direct_company_id'] = $directCompanyId;
            $stockInfo['type']              = $type;
            $stockInfo['create_user_id']    = Yii::$app->user->id;
            $stockInfo['status']            = 1;
            $stockInfo['audit_status']      = 1;
            $stockInfo['create_time']       = $time;
            //出入库产品数据
            foreach ($stockDetail as  $key => $val) {
                $stockDetail[$key]['direct_company_id']    = $directCompanyId;
            }
            $res = StockInfo::add($stockInfo,$stockDetail,$type);
            if ($res)
            {
                //直接出/入库 不需要手动审批
                if($stockInfo['type'] == 1 && $stockInfo['stock_type'] == 9)
                {
                    self::autoExamine($res,$stockInfo['type']);
                }
                if($stockInfo['type'] == 2 && $stockInfo['stock_type'] == 11)
                {
                    self::autoExamine($res,$stockInfo['type']);
                }
                return self::success('','成功');
            }
            return self::error('失败');
        }
        
        /**
         * 获取服务商关联单主题及相关信息
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/23
         * Time: 13:22
         * @param $directCompanyId 直属公司ID
         * @param $relType
         * @param $relId
         * @return array
         */
        public static function getRelName($directCompanyId,$relType,$relId)
        {
            switch ($relType) {
                //1 采购单表： purchas   subject
                case 1:
                    $model  = new Purchase();
                    $detail = new PurchaseDetail();
                    $res    = self::getRelationList($directCompanyId,$relId,$model,$detail);
                    if (isset($res['code']) && $res['code'] != 200) {
                        return $res;
                    }
                    foreach ($res['product'] as $key => $val) {
                        $res['product'][$key]['prod_num'] = $val['prod_num'] - $val['finish_num'];
                        if ($res['product'][$key]['prod_num'] == 0) {
                            unset($res['product'][$key]);
                        }
                    }
                    break;


                //2 合同： contract      subject
                case 2:
                    $model = new Contract();
                    $detail= new ContractDetail();
                    $res   = self::getRelationList($directCompanyId, $relId, $model, $detail,$relType);
                    if (isset($res['code']) && $res['code'] != 200) {
                        return $res;
                    }
                    foreach ($res['product'] as $key => $val) {
                        $res['product'][$key]['prod_num'] = $val['total_num'] - $val['finish_num'];
                        if ($res['product'][$key]['prod_num'] == 0) {
                            unset($res['product'][$key]);
                        }
                    }

                    break;

                //3 退货单表： return    subject
                case 3:
                    $model = new ReturnGoods();
                    $detail = new ReturnGoodsDetail();
                    $res = self::getRelationList($directCompanyId,$relId,$model,$detail,$relType);

                    if (isset($res['code']) && $res['code'] != 200) {
                        return $res;
                    }

                    foreach ($res['product'] as $key => $val) {
                        $res['product'][$key]['prod_num'] = $val['prod_num'] - $val['finish_num'];
                        if ($res['product'][$key]['prod_num'] == 0) {
                            unset($res['product'][$key]);
                        }
                    }
                    break;

                //4 换货表： exchange    subject
                case 4:
                    $model = new ExchangeGoods();
                    $detail = new ExchangeGoodsDetail();
                    $res = self::getRelationList($directCompanyId,$relId,$model,$detail,$relType);

                    if (isset($res['code']) && $res['code'] != 200) {
                        return $res;
                    }

                    foreach ($res['product'] as $key => $val) {
                        $res['product'][$key]['prod_num'] = $val['prod_num'] - $val['finish_num'];
                        if ($res['product'][$key]['prod_num'] == 0) {
                            unset($res['product'][$key]);
                        }
                    }
                    break;
                
                // stock_type   类型，1：申请，2：归还
                //5 技师申请： tech_prod_stock       subject
                case 5:
                    $stockType = 1;
                    $model  = new TechProdStock();
                    $detail = new TechProdStockDetail();
                    $res    = self::getRelationList($directCompanyId,$relId,$model,$detail,$stockType);
                    if (isset($res['code']) && $res['code'] != 200) {
                        return $res;
                    }
                    foreach ($res['product'] as $key => $val) {
                        $res['product'][$key]['prod_num'] = $val['prod_num'] - $val['finish_num'];
                        if ($res['product'][$key]['prod_num'] == 0) {
                            unset($res['product'][$key]);
                        }
                    }
                    
                    break;
                //6 退还备件表： tech_prod_stock      subject
                case 6:
                    $stockType = 2;
                    $model  = new TechProdStock();
                    $detail = new TechProdStockDetail();
                    $res    = self::getRelationList($directCompanyId,$relId,$model,$detail,$stockType);
                    if (isset($res['code']) && $res['code'] != 200) {
                        return $res;
                    }
                    foreach ($res['product'] as $key => $val) {
                        $res['product'][$key]['prod_num'] = $val['prod_num'] - $val['finish_num'];
                        if ($res['product'][$key]['prod_num'] == 0) {
                            unset($res['product'][$key]);
                        }
                    }
                    
                    break;
                //7 采购退货单表： return    subject
                case 7:
                    $model = new ReturnPurchase();
                    $detail = new ReturnPurchaseDetail();
                    $res = self::getRelationList($directCompanyId,$relId,$model,$detail,$relType);
                    if (isset($res['code']) && $res['code'] != 200) {
                        return $res;
                    }
                    foreach ($res['product'] as $key => $val) {
                        $res['product'][$key]['prod_num'] = $val['prod_num'] - $val['finish_num'];
                        if ($res['product'][$key]['prod_num'] == 0) {
                            unset($res['product'][$key]);
                        }
                    }
                    break;

                default:
                    $res = [];
            }
            return self::success($res,'成功');
        }
        
        /**
         * 组合列表数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/23
         * Time: 13:24
         * @param $data
         * @return mixed
         */
        public static function changeData($data)
        {
            
            foreach ($data as $key => $val) {
                
                //出入库类型
                if (!empty($val['stock_type'])) {
                    $stockType = self::getStockType($val['type'],$val['stock_type']);
                    $data[$key]['stock_type_desc'] = $stockType['data'];
                }
                
                //出入库状态
                if (!empty($val['status'])) {
                    $auditStatus = self::getStatus($val['type'],$val['status']);
                    $data[$key]['status_desc'] = $auditStatus['data'];
                }
                //发货状态
                if (!empty($val['isship'])) {
                    $isship_status = ['1'=>'未发货','2'=>'已发货','3'=>'已收货'];
                    $data[$key]['isship_desc'] = isset($isship_status[$val['isship']])?$isship_status[$val['isship']]:'';
                }
                //产品数量
                if (!empty($val['id'])) {
                    $prodSum = self::getProdSum($val['id']);
                    $data[$key]['prod_sum'] = $prodSum['data'];
                }
            }
            
            return $data;
        }
        
        /**
         * 获取出入库类型
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/23
         * Time: 11:21
         * @param int $type
         * @param int $id
         * @return array|mixed|string
         */
        public static function getStockType ($type=1,$id = 0)
        {
            $data = [];

            //入库
            if ($type == 1) {
                //入库类型
                $where = ['dict_key'=>'enum_fws_stock_in_type'];
                $dict = DictEnum::getList($where);
                if (!$dict) {
                    return self::error('入库类型获取失败',20001);
                }
                foreach ($dict as $val) {
                    $data[$val['dict_enum_id']] = $val['dict_enum_value'];
                }
            }
            
            //出库
            if ($type == 2) {
                //出库类型
                $where = ['dict_key'=>'enum_fws_stock_out_type'];
                $dict = DictEnum::getList($where);
                if (!$dict) {
                    return self::error('出库类型获取失败',20001);
                }
                foreach ($dict as $val) {
                    $data[$val['dict_enum_id']] = $val['dict_enum_value'];
                }
            }
            
            //判断是否存在id  存在返回对应的类型
            if ($id != 0) {
                $stockType = '';
                foreach ($data as $key => $val) {
                    if ($id == $key) {
                        $stockType = $val;
                    }
                }
                return self::success($stockType,'成功');
            }
            return self::success($data,'成功');
            
        }
        
        /**
         * 审批状态
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/23
         * Time: 13:07
         * @param int $type
         * @param int $id
         * @return array
         */
        public static function getAuditStatus($type=1,$id = 0)
        {
            
            $data = [];
            $where = ['dict_key'=>'enum_audit_status'];
            $dict = DictEnum::getList($where);
            if (!$dict) {
                return self::error('审核状态获取失败',20001);
            }
            foreach ($dict as $val) {
                $data[$val['dict_enum_id']] = $val['dict_enum_value'];
            }
            //判断是否存在id  存在返回对应的类型
            if ($id != 0) {
                $auditStatus = '';
                foreach ($data as $key => $val) {
                    if ($id == $key) {
                        $auditStatus = $val;
                    }
                }
                return self::success($auditStatus,'成功');
            }
            return self::success($data,'成功');
            
        }
        
        /**
         * 获取状态
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/28
         * Time: 16:28
         * @param int $type
         * @param int $id
         * @return array
         */
        public static function getStatus($type=1,$id = 0)
        {
            
            $data = [];
            $orgType = BaseModel::SRC_FWS;
            //入库
            if ($type == 1) {
                //入库类型
                $where = ['dict_key'=>'enum_fws_stock_in_status'];
                $dict = DictEnum::getList($where);
                if (!$dict) {
                    return self::error('状态获取失败',20001);
                }
                foreach ($dict as $val) {
                    $data[$val['dict_enum_id']] = $val['dict_enum_value'];
                }
            }
            
            //出库
            if ($type == 2) {
                //出库类型
                $where = ['dict_key'=>'enum_fws_stock_out_status'];
                $dict = DictEnum::getList($where);
                if (!$dict) {
                    return self::error('状态获取失败',20001);
                }
                foreach ($dict as $val) {
                    $data[$val['dict_enum_id']] = $val['dict_enum_value'];
                }
            }
            
            //判断是否存在id  存在返回对应的类型
            if ($id != 0) {
                $status = '';
                foreach ($data as $key => $val) {
                    if ($id == $key) {
                        $status = $val;
                    }
                }
                return self::success($status,'成功');
            }
            return self::success($data,'成功');
            
        }
        
        /**
         * 获取产品数量总和
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/23
         * Time: 14:12
         * @param $id
         * @return array|float|int|string
         */
        public static function getProdSum($id) {
            if (!$id) {
                return self::error('数据获取失败',20001);
            }
            $sum = '';
            $where = ['direct_company_id'=>BaseLogic::getDirectCompanyId(),'parent_id'=>$id];
            
            $stockDetail = StockDetail::getList($where);
            
            if ($stockDetail) {
                $sum = array_sum(array_column($stockDetail,'prod_num'));
            }
            return self::success($sum,'成功');
            
        }
        
        /**
         * 查看详情
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/23
         * Time: 17:25
         * @return array
         */
        public static function view($flag = '')
        {
            $id = intval(Yii::$app->request->get('id',0));
            
            if (!$id) {
                return self::error('获取详情失败',20001);
            }
            
            $directCompanyId = BaseLogic::getDirectCompanyId();

            $map = [
                'direct_company_id'=>$directCompanyId,
                'id'=>$id,
            ];
            
            $stockInfo = StockInfo::getOne($map,1);
            
            if (!$stockInfo) {
                return self::error('获取详情失败',20002);
            }
            //获取出库单发货详情
            if($stockInfo['type'] == 2){
                $shipInfo = SendOut::getOne(['direct_company_id'=>$directCompanyId,'stock_id'=>$id],1);
            }
            if ($flag) {
                $stock = StockInfo::getOne(['id'=>$id,'audit_status'=>[2,3]]);
                if (!empty($stock)) {
                    return self::error('此单据已审批',20004);
                }
            }
            
            //出入库类型
            if (!empty($stockInfo['stock_type'])) {
                $stockType = self::getStockType($stockInfo['type'],$stockInfo['stock_type']);
                $stockInfo['stock_type_desc'] = $stockType['data'];
            }
            
            //出入库审批状态
            if (!empty($stockInfo['audit_status'])) {
                $auditStatus = self::getAuditStatus($stockInfo['type'],$stockInfo['audit_status']);
                if ($auditStatus['code'] == 200) {
                    $stockInfo['audit_status_desc'] = $auditStatus['data'];
                }else{
                    return $auditStatus;
                }
            }
            
            //申请人
            if (!empty($stockInfo['apply_user_id'])) {
                
                $user = AdminUser::getOne(['id'=>$stockInfo['create_user_id']]);
                if ($user) {
                    $stockInfo['apply_user_name'] = $user->username;
                }
                
            }
            
            //审批人
            if (!empty($stockInfo['audit_user_id'])) {
                
                $user = AdminUser::getOne(['id'=>$stockInfo['audit_user_id']]);
                if ($user) {
                    $stockInfo['audit_user_name'] = $user->username;
                }
                
            }
            
            //关联单
            if (!empty($stockInfo['rel_type']) && !empty($stockInfo['rel_id'])) {
                $relName = self::getRelName($directCompanyId,$stockInfo['rel_type'],$stockInfo['rel_id']);
                if ($relName['code'] == 200) {
                    $stockInfo['rel_name'] = $relName['data']['relName'];
                    $stockInfo['rel_no'] = isset($relName['data']['relNo'])?$relName['data']['relNo']:'';
                    if ($flag == 1) {
                        $sum = array_sum(array_column($relName['data']['product'],'prod_num'));
                        if ($sum == 0) {
                            return self::error('当前关联入库单已入库完毕');
                        }
                    }
                }
            }
            
            //入库产品明细
            $productData = self::getProduct($id);
            
            if ($productData['code'] != 200) {
                return self::error('获取产品明细失败',20003);
            }
            $data = [];
            if ($stockInfo) {
                //提货人信息
                $pickInfo = PickUser::getOne(['id'=>$stockInfo['pick_user_id']]);
                $stockInfo['pick_name']     = isset($pickInfo['pick_name'])?$pickInfo['pick_name']:'';
                $stockInfo['pick_mobile']   = isset($pickInfo['pick_mobile'])?$pickInfo['pick_mobile']:'';
                $stockInfo['pick_card']     = isset($pickInfo['pick_card'])?$pickInfo['pick_card']:'';
                $stockInfo['pick_license']  = isset($pickInfo['pick_license'])?$pickInfo['pick_license']:'';
                //提货归属部门
                $departmentInfo = Department::getDepartmentName($pickInfo['department_id']);
                $stockInfo['pick_department_name']  = isset($departmentInfo)?$departmentInfo:'';
                $data['stockInfo'] = $stockInfo;
                }else{
                $data['stockInfo'] = [];
                }
            foreach ($productData['data'] as $key => $val) {
                $depot = DepotProdStock::getList(['depot_id'=>$val['depot_id'],'prod_id'=>$val['prod_id']]);
                if($stockInfo['stock_type'] == 10){
                    $productData['data'][$key]['depot_num'] = array_sum(array_column($depot,'bad_num'));
                }else{
                    $productData['data'][$key]['depot_num'] = array_sum(array_column($depot,'num'));
                }

            }
            $prod_send_num  = array_sum(array_column($productData['data'],'prod_num'));

            if ($productData) {
                $data['stockDetail'] = $productData['data'];
            }else{
                $data['stockDetail'] = [];
            }
            
            //库房
            $data['depot'] = self::getDepot($directCompanyId);
            //出库单发货详情
            if($stockInfo['type'] == 2 && $shipInfo && in_array($stockInfo['isship'],[2,3]) ){
                $send_status_lit = ['0'=>'','1'=>'运输中','2'=>'已签收'];
                $data['shipInfo']['id'] = isset($shipInfo['id'])?$shipInfo['id']:'';
                $data['shipInfo']['send_no'] = isset($shipInfo['send_no'])?$shipInfo['send_no']:'';
                $data['shipInfo']['send_status'] = isset($shipInfo['send_status'])?$shipInfo['send_status']:'';
                $data['shipInfo']['send_status_desc'] = isset($send_status_lit[$shipInfo['send_status']])?$send_status_lit[$shipInfo['send_status']]:'';
                $data['shipInfo']['send_prod_num'] = isset($prod_send_num)?$prod_send_num:'';  //发货产品数量
                $data['shipInfo']['create_time'] = isset($shipInfo['create_time'])?$shipInfo['create_time']:'';
            }
            return self::success($data,'成功');
            
        }
        
        /**
         * 获取入库单产品明细数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/23
         * Time: 17:29
         * @param $id
         * @return array
         */
        public static function getProduct($id)
        {
            $directCompanyId = BaseLogic::getDirectCompanyId();
            $where = [
                ['a.direct_company_id'=>$directCompanyId],
                ['a.parent_id'=>$id]
            
            ];
            $product = StockDetail::getProduct($where);
            
            if (!$product) {
                return self::error('获取产品明细失败');
            }
            
            //调用产品逻辑层组合数据方法
            $productData = ProductLogic::changeData($product,1);
            
            if (!$productData) {
                return self::error('获取产品明细失败');
            }
            
            //单位
            $unit = ProductLogic::getUnit();
            if ($unit) {
                foreach ($productData as $key => $val) {
                    if (isset($unit[$val['unit_id']])) {
                        $productData[$key]['unit_name'] = $unit[$val['unit_id']];
                    }else{
                        $productData[$key]['unit_name'] = '';
                    }
                }
            }
            
            //库房
            foreach ($productData as $key => $val) {
                $depot = Depot::getOne(['id'=>$val['depot_id']]);
                if (isset($depot['name'])) {
                    $productData[$key]['depot_name'] = $depot['name'];
                }else{
                    $productData[$key]['depot_name'] = '';
                }
            }
            
            
            return self::success($productData,'成功');
            
        }
        
        /**
         * 校验生成单号是否重复
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/26
         * Time: 14:46
         * @param $no
         * @return array
         */
        public static function validateNo($no)
        {
            
            if (!$no) {
                return self::error('单号生成失败');
            }
            $info = StockInfo::getOne(['direct_company_id'=>BaseLogic::getDirectCompanyId(),'no'=>$no]);
            
            if ($info) {
                return self::error('单号已存在',20001);
            }
            return self::success($no,'成功');
        }
        /**
         * 审批入库单
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/27
         * Time: 15:55
         * @return array
         * @throws \Exception
         */
        public static function autoExamine($id,$type)
        {
            //系统是否开启发货功能状态
            $shipSys = '1'; //1 已开启 0 未开启
            $stock = StockInfo::getOne(['id'=>$id,'audit_status'=>[2,3]]);
            if (!empty($stock)) {
                return self::error('此单据已审批',20004);
            }

            $data = [];
            $data['stock']['id']  = $id;
            $data['stock']['status']  = 2;
            $data['stock']['audit_status']  = 2;
            $data['stock']['audit_user_id'] = Yii::$app->user->id;
            $data['stock']['audit_suggest'] = '';
            $data['stock']['audit_date']    = date('Y-m-d H:i:s',time());
            //查询审批单据主表信息
            $directCompanyId = BaseLogic::getDirectCompanyId();
            $stockIn = StockInfo::getOne(['id'=>$id,'direct_company_id'=>$directCompanyId,'is_rel'=>1],1);
            //组合所有入库所需修改的数据
            if (!empty($stockIn)) {
                $purchase = self::examineData($id,$directCompanyId,$stockIn,$type);
                if ($purchase['code'] != 200) {
                    return $purchase;
                }
                //爆溢入库
                if (isset($purchase['data']['burst'])) {
                    $data['burst'] = $purchase['data']['burst'];
                }

                //入库单
                if (isset($purchase['data']['stockDetail'])) {
                    $data['stockDetail'] = $purchase['data']['stockDetail'];
                }

                //采购单详情
                if (isset($purchase['data']['purchase'])) {
                    $data['purchase'] = $purchase['data']['purchase'];
                }

                //技师申请
                if (isset($purchase['data']['tech'])) {
                    $data['tech'] = $purchase['data']['tech'];
                }
                //合同出库
                if (isset($purchase['data']['contract'])) {
                    $data['contract'] = $purchase['data']['contract'];
                }

                //退货入库
                if (isset($purchase['data']['return'])) {
                    $data['return'] = $purchase['data']['return'];
                }

                //换货出入库
                if (isset($purchase['data']['exchange'])) {
                    $data['exchange'] = $purchase['data']['exchange'];
                }

                //技师库存
                if (isset($purchase['data']['techStorage'])) {
                    $data['techStorage'] = $purchase['data']['techStorage'];
                }
                //采购退货出库
                if (isset($purchase['data']['returnPurchase'])) {
                    $data['returnPurchase'] = $purchase['data']['returnPurchase'];
                }
            }

            //审核通过操作库存数据
            $depot = self::stockInData($id,$directCompanyId,$type);
            if ($depot['code'] == 200) {
                $data['depot'] = $depot['data'];
            }
            $res = StockInfo::examine($data,$shipSys);
            if ($res) {
                return self::success('','成功');
            }
            return self::error('失败',20004);

        }
        /**
         * 审批入库单
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/27
         * Time: 15:55
         * @return array
         * @throws \Exception
         */
        public static function ajaxExamine($type)
        {
            //系统是否开启发货功能状态
            $shipSys = '1'; //1 已开启 0 未开启
            $id           = intval(Yii::$app->request->post('id',0));
            $auditStatus  = intval(Yii::$app->request->post('auditStatus',0));
            $auditSuggest = trim(Yii::$app->request->post('auditSuggest',''));
            
            if ($id == 0) {
                return self::error('单号获取失败',20001);
            }
            if ($auditStatus == 0) {
                return self::error('审批状态获取失败',20002);
            }
            if ($auditStatus == 3 && $auditSuggest == '') {
                return self::error('审批意见获取失败',20003);
            }
            
            $stock = StockInfo::getOne(['id'=>$id,'audit_status'=>[2,3]]);
            if (!empty($stock)) {
                return self::error('此单据已审批',20004);
            }
            
            $data = [];
            $data['stock']['id']  = $id;
            $data['stock']['status']  = $auditStatus;
            $data['stock']['audit_status']  = $auditStatus;
            $data['stock']['audit_user_id'] = Yii::$app->user->id;
            $data['stock']['audit_suggest'] = $auditSuggest;
            $data['stock']['audit_date']    = date('Y-m-d H:i:s',time());
            
            //查询审批单据主表信息
            $directCompanyId = BaseLogic::getDirectCompanyId();
            $stockIn = StockInfo::getOne(['id'=>$id,'direct_company_id'=>$directCompanyId,'is_rel'=>1],1);
            //组合所有入库所需修改的数据
            if ($auditStatus == 2) {
                if (!empty($stockIn)) {
                    $purchase = self::examineData($id,$directCompanyId,$stockIn,$type);
                    if ($purchase['code'] != 200) {
                        return $purchase;
                    }
    
                    //爆溢入库
                    if (isset($purchase['data']['burst'])) {
                        $data['burst'] = $purchase['data']['burst'];
                    }
    
                    //入库单
                    if (isset($purchase['data']['stockDetail'])) {
                        $data['stockDetail'] = $purchase['data']['stockDetail'];
                    }

                    //采购单详情
                    if (isset($purchase['data']['purchase'])) {
                        $data['purchase'] = $purchase['data']['purchase'];
                    }

                    //技师申请
                    if (isset($purchase['data']['tech'])) {
                        $data['tech'] = $purchase['data']['tech'];
                    }
                    //合同出库
                    if (isset($purchase['data']['contract'])) {
                        $data['contract'] = $purchase['data']['contract'];
                    }

                    //退货入库
                    if (isset($purchase['data']['return'])) {
                        $data['return'] = $purchase['data']['return'];
                    }

                    //换货出入库
                    if (isset($purchase['data']['exchange'])) {
                        $data['exchange'] = $purchase['data']['exchange'];
                    }
    
                    //技师库存
                    if (isset($purchase['data']['techStorage'])) {
                        $data['techStorage'] = $purchase['data']['techStorage'];
                    }
                    //采购退货出库
                    if (isset($purchase['data']['returnPurchase'])) {
                        $data['returnPurchase'] = $purchase['data']['returnPurchase'];
                    }
                }
    
                //审核通过操作库存数据
                $depot = self::stockInData($id,$directCompanyId,$type);
                if ($depot['code'] == 200) {
                    $data['depot'] = $depot['data'];
                }
                
            }
            $res = StockInfo::examine($data,$shipSys);
            if ($res) {
                return self::success('','成功');
            }
            return self::error('失败',20004);
            
        }
        
        /**
         * 组合审批出入库需修改数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/27
         * Time: 10:42
         * @param $id
         * @param $directCompanyId
         * @param $orgType
         * @param $stockIn
         * @param $type
         * @return array
         */
        public static function examineData($id,$directCompanyId,$stockIn,$type)
        {
            
            $data = [];
            
            //获取关联单数据
            $purchase = self::getRelName($directCompanyId,$stockIn['rel_type'],$stockIn['rel_id']);
            
            if ($purchase['code'] != 200) {
                return self::error('审核失败',20002);
            }
            $relation = $purchase['data']['product'];

            //获取审批单据详情产品数量
            $where = [
                ['a.direct_company_id'=>$directCompanyId],
                ['a.parent_id'=>$id]
            ];
            $stockDetail = StockDetail::getProduct($where);
            if (empty($stockDetail)) {
                return self::error('审核失败',20003);
            }
            
            $time = date('Y-m-d H:i:s',time());
            
            if (empty($stockIn['rel_type'])) {
                return self::error('审核失败',20004);
            }
            //根据关联单类型组合需要修改的数据
            switch ($stockIn['rel_type']) {
                
                //采购
                case 1:
                    foreach ($relation as $key => $val) {
                        foreach ($stockDetail as $k => $v) {
                            if ($val['prod_id'] == $v['prod_id']) {
                                //正常采购数据修改
                                if ($val['prod_num'] > 0 && $val['prod_num'] >= $v['prod_num']) {
                                    $data['purchase'][$key]['id']         = $val['id'];
                                    $data['purchase'][$key]['parent_id']  = $stockIn['rel_id'];
                                    $data['purchase'][$key]['finish_num'] = $val['finish_num'] + $v['prod_num'];
                                }
                                //入库数量大于申请采购数量时 存在爆溢入库
                                if ($val['prod_num'] > 0 && $val['prod_num'] < $v['prod_num']) {
                                    
                                    //采购单数据更改
                                    $data['purchase'][$key]['id'] = $val['id'];
                                    $data['purchase'][$key]['parent_id']  = $stockIn['rel_id'];
                                    $data['purchase'][$key]['finish_num'] = $val['finish_num'] + $val['prod_num'];
                                    
                                    //存在爆溢入库时修改当前审批单数量
                                    $data['stockDetail'][$k]['id']       = $v['id'];
                                    $data['stockDetail'][$k]['prod_num'] = $val['prod_num'];
                                    
                                    //生成爆溢入库单数据
                                    $data['burst']['stockInfo']['direct_company_id'] = $directCompanyId;
                                    $data['burst']['stockInfo']['no']             = CreateNoRule::getNo('RK');
                                    $data['burst']['stockInfo']['type']           = 1;
                                    $data['burst']['stockInfo']['is_rel']         = 0;
                                    $data['burst']['stockInfo']['apply_user_id']  = Yii::$app->user->id;
                                    $data['burst']['stockInfo']['create_user_id'] = Yii::$app->user->id;
                                    $data['burst']['stockInfo']['create_time']    = $time;
                                    $data['burst']['stockInfo']['status']         = 2;
                                    $data['burst']['stockInfo']['stock_type']     = 6;
                                    $data['burst']['stockInfo']['audit_status']   = 2;
                                    $data['burst']['stockInfo']['audit_user_id']  = Yii::$app->user->id;
                                    $data['burst']['stockInfo']['audit_date']     = $time;
                                    $data['burst']['stockInfo']['time_stamp']     = $time;
                                    
                                    $data['burst']['stockDetail'][$k]['direct_company_id']      = $directCompanyId;
                                    $data['burst']['stockDetail'][$k]['prod_id']     = $v['prod_id'];
                                    $data['burst']['stockDetail'][$k]['prod_num']    = $v['prod_num']-$val['prod_num'];
                                    $data['burst']['stockDetail'][$k]['depot_id']    = $v['depot_id'];
                                    $data['burst']['stockDetail'][$k]['prod_batch']  = $v['prod_batch'];
                                    $data['burst']['stockDetail'][$k]['prod_date']   = $v['prod_date'];
                                    $data['burst']['stockDetail'][$k]['create_date'] = $time;
                                    $data['burst']['stockDetail'][$k]['time_stamp'] = $time;
                                    
                                }
                            }
                        }
                    }
                    
                    break;

                //合同
                case 2:
                    foreach ($relation as $key => $val) {
                        foreach ($stockDetail as $k => $v) {
                            if ($val['prod_id'] == $v['prod_id']) {
                                //正常合同数据修改
                                if ($val['prod_num'] >= 0 && $val['prod_num'] >= $v['prod_num']) {
                                    $data['contract'][$key]['id']         = $val['id'];
                                    $data['contract'][$key]['parent_id']  = $stockIn['rel_id'];
                                    $data['contract'][$key]['finish_num'] = $val['finish_num'] + $v['prod_num'];

                                }
                                if ($val['prod_num'] > 0  && $val['prod_num'] < $v['prod_num']) {
                                    if($v['type_id'] == 1){
                                        return self::error('入库数量大于申请采购数量',20005);
                                    }else if($v['type_id'] == 2){
                                        return self::error('出库数量大于合同剩余数量',20005);
                                    }else{
                                        return self::error('审核失败',20005);
                                    }
                                }
                            }
                        }
                    }

                    break;

                //退货
                case 3:

                    foreach ($relation as $key => $val) {
                        foreach ($stockDetail as $k => $v) {
                            if ($val['prod_id'] == $v['prod_id']) {
                                //正常合同数据修改
                                if ($val['prod_num'] > 0 && $val['prod_num'] >= $v['prod_num']) {
                                    $data['return'][$key]['id']         = $val['id'];
                                    $data['return'][$key]['parent_id']  = $stockIn['rel_id'];
                                    $data['return'][$key]['finish_num'] = $val['finish_num'] + $v['prod_num'];

                                }
                                //入库数量大于申请采购数量时 存在爆溢入库
                                if ($val['prod_num'] > 0 && $val['prod_num'] < $v['prod_num']) {
                                    return self::error('审核失败',20005);
                                }
                            }
                        }
                    }
                    break;

                //换货
                case 4:

                    foreach ($relation as $key => $val) {
                        foreach ($stockDetail as $k => $v) {
                            if ($val['prod_id'] == $v['prod_id'] && $val['type'] == $type) {
                                //正常合同数据修改
                                if ($val['prod_num'] > 0 && $val['prod_num'] >= $v['prod_num']) {
                                    $data['exchange'][$key]['id']         = $val['id'];
                                    $data['exchange'][$key]['parent_id']  = $stockIn['rel_id'];
                                    $data['exchange'][$key]['finish_num'] = $val['finish_num'] + $v['prod_num'];

                                }
                                //入库数量大于申请采购数量时 存在爆溢入库
                                if ($val['prod_num'] > 0 && $val['prod_num'] < $v['prod_num']) {
                                    return self::error('审核失败',20005);
                                }
                            }
                        }
                    }
                    break;

                //配件申请
                case 5:
                    
                    //技师备件组合
                    //获取申请人信息
                    $techProdStock = TechProdStock::getOne(['id'=>$stockIn['rel_id'],'direct_company_id'=>$directCompanyId,'stock_type'=>1]);
                    if (empty($techProdStock)) {
                        return self::error('审核失败',20005);
                    }
                    //配件申请表数据组合
                    foreach ($relation as $key => $val) {
                        foreach ($stockDetail as $k => $v) {
                            if ($val['prod_id'] == $v['prod_id']) {
                                //正常合同数据修改
                                if ($val['prod_num'] > 0 && $val['prod_num'] >= $v['prod_num']) {
                                    $data['tech'][$key]['id']         = $val['id'];
                                    $data['tech'][$key]['parent_id']  = $stockIn['rel_id'];
                                    $data['tech'][$key]['finish_num'] = $val['finish_num'] + $v['prod_num'];
                                    
                                }
                                //入库数量大于申请采购数量时 存在爆溢入库
                                if ($val['prod_num'] > 0 && $val['prod_num'] < $v['prod_num']) {
                                    return self::error('审核失败',20006);
                                }
    
                                //根据申请人信息查询是否存在库存
                                $techStorage = TechStorage::getOne(['direct_company_id'=>$directCompanyId,'tech_id'=>$techProdStock['tech_id'],'prod_id'=>$v['prod_id'],'prod_batch'=>$v['prod_batch']]);
                                //存在为修改  不存在为添加
                                if (empty($techStorage)) {
                                    
                                    $data['techStorage']['add'][$key]['direct_company_id'] = $directCompanyId;
                                    $data['techStorage']['add'][$key]['tech_id']    = $techProdStock['tech_id'];
                                    $data['techStorage']['add'][$key]['prod_id']    = $v['prod_id'];
                                    $data['techStorage']['add'][$key]['prod_batch'] = $v['prod_batch'];
                                    $data['techStorage']['add'][$key]['total_num']  = $v['prod_num'];
                                    $data['techStorage']['add'][$key]['num']        = $v['prod_num'];
                                    $data['techStorage']['add'][$key]['status']     = 0;
                                    $data['techStorage']['add'][$key]['time_stamp'] = $time;
                                }else{
                                    
                                    $data['techStorage']['update'][$key]['id']         = $techStorage['id'];
                                    $data['techStorage']['update'][$key]['total_num']  = $techStorage['total_num'] + $v['prod_num'];
                                    $data['techStorage']['update'][$key]['num']        = $techStorage['num'] + $v['prod_num'];
                                    $data['techStorage']['update'][$key]['time_stamp'] = $time;
                                }
                                
                            }
                            
                        }
                    }
                    
                    break;
                
                //配件返还
                case 6:
    
                    //技师备件组合
                    //获取申请人信息
                    $techProdStock = TechProdStock::getOne(['id'=>$stockIn['rel_id'],'direct_company_id'=>$directCompanyId,'stock_type'=>2]);
                    if (empty($techProdStock)) {
                        return self::error('审核失败',20007);
                    }
                    foreach ($relation as $key => $val) {
                        foreach ($stockDetail as $k => $v) {
                            if ($val['prod_id'] == $v['prod_id']) {
                                //正常合同数据修改
                                if ($val['prod_num'] > 0 && $val['prod_num'] >= $v['prod_num']) {
                                    $data['tech'][$key]['id']         = $val['id'];
                                    $data['tech'][$key]['parent_id']  = $stockIn['rel_id'];
                                    $data['tech'][$key]['finish_num'] = $val['finish_num'] + $v['prod_num'];
                                }
                                //入库数量大于申请采购数量时 存在爆溢入库
                                if ($val['prod_num'] > 0 && $val['prod_num'] < $v['prod_num']) {
                                    return self::error('审核失败',20008);
                                }
                                //根据申请人信息查询是否存在库存
                                $returnType = 1;
                                if($techProdStock['return_type'] != 1){
                                    $returnType = 0;
                                }
                                $techStorage = TechStorage::getOne(['direct_company_id'=>$directCompanyId,'tech_id'=>$techProdStock['tech_id'],'prod_id'=>$v['prod_id'],'type'=>$returnType]);
                                //存在为修改  不存在为添加
                                if (!empty($techStorage)) {
                                    if ($techStorage['total_num'] - $v['prod_num'] < 0 || $techStorage['num'] - $v['prod_num'] < 0) {
                                        return self::error('审核失败',20009);
                                    }
                                    $data['techStorage']['update'][$key]['id']         = $techStorage['id'];
                                    $data['techStorage']['update'][$key]['total_num']  = $techStorage['total_num'] - $v['prod_num'];
                                    $data['techStorage']['update'][$key]['num']        = $techStorage['num'] - $v['prod_num'];
                                    $data['techStorage']['update'][$key]['time_stamp'] = $time;
                                }else{
                                    return self::error('审核失败',20010);
                                }
                            }
                        }
                    }
                    break;
                case 7:
                  //采购退货出库

                    foreach ($relation as $key => $val) {
                        foreach ($stockDetail as $k => $v) {
                            if ($val['prod_id'] == $v['prod_id']) {
                                //正常合同数据修改
                                if ($val['prod_num'] >= 0 && $val['prod_num'] >= $v['prod_num']) {
                                    $data['returnPurchase'][$key]['id']         = $val['id'];
                                    $data['returnPurchase'][$key]['parent_id']  = $stockIn['rel_id'];
                                    $data['returnPurchase'][$key]['finish_num'] = $v['prod_num'];
                                }
                                if ($val['prod_num'] > 0  && $val['prod_num'] < $v['prod_num']) {
                                    return self::error('出库数量大于采购数量',20005);
                                }
                            }
                        }
                    }
                    break;
                default:
                    return self::error('审核失败',20006);
                    break;
                
            }
            return self::success($data);
        }
        
        /**
         * 组合库存产品数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/27
         * Time: 15:55
         * @param string $id
         * @return array
         */
        public static function stockInData($id,$directCompanyId,$type)
        {
            
            if (!$id) {
                return self::error('获取数据失败',20001);
            }
            $stockIn = StockDetail::getList(['direct_company_id'=>$directCompanyId,'parent_id'=>$id]);
            if (!$stockIn) {
                return self::error('获取数据失败',20002);
            }
            
            $time = date('Y-m-d H:i:s',time());
            //入库数据组合

            //2018-11-20 新增非良品类型
            $depot = StockInfo::findOneByAttributes(['id'=>$id]);
            $stockType = TechProdStock::findOneByAttributes(['id'=>$depot['rel_id']],"return_type");
            if ($type == 1)
            {
                foreach ($stockIn as $key => $val)
                {
                    $where = ['direct_company_id'=>$directCompanyId,'prod_id'=>$val['prod_id'],'prod_batch'=>$val['prod_batch'],'depot_id'=>$val['depot_id']];
                    $depotProdStock = DepotProdStock::getOne($where);
                    if (!empty($depotProdStock))
                    {
                        //非良品
                        if(isset($stockType['return_type']) && $stockType['return_type'] == 0)
                        {
                            $data['update'][$key]['id']          = $depotProdStock['id'];
                            $data['update'][$key]['total_num']  = $depotProdStock['total_num'];
                            $data['update'][$key]['bad_num']    = $depotProdStock['bad_num']+$val['prod_num'];
                            $data['update'][$key]['num']         = $depotProdStock['num'];
                        }else{
                            $sum = $depotProdStock['num']+$val['prod_num'];
                            $data['update'][$key]['id']          = $depotProdStock['id'];
                            $data['update'][$key]['num']         = $sum;
                            $data['update'][$key]['total_num']   = $sum;
                        }
                    }else{
                        //库存不存在添加库存
                        $data['add'][$key]['direct_company_id']    = $val['direct_company_id'];
                        $data['add'][$key]['prod_id']     = $val['prod_id'];
                        $data['add'][$key]['prod_batch']  = $val['prod_batch'];
                        if(isset($stockType['return_type']) && $stockType['return_type'] == 0){
                            $data['add'][$key]['bad_num']   =  $val['prod_num'];
                            $data['add'][$key]['num']   =  0;
                            $data['add'][$key]['total_num']   = 0;
                        }else{
                            $data['add'][$key]['bad_num']   =  0;
                            $data['add'][$key]['num']   = $val['prod_num'];
                            $data['add'][$key]['total_num']   = $val['prod_num'];
                        }
                        $data['add'][$key]['freeze_num']  = 0;
                        $data['add'][$key]['depot_id']    = $val['depot_id'];
                        $data['add'][$key]['create_time'] = $time;
                        $data['add'][$key]['time_stamp']  = $time;
                    }
                }
            }
            //出库数据组合
            if ($type == 2) {
                foreach ($stockIn as $key => $val) {
                    $where = ['direct_company_id' => $directCompanyId, 'prod_id' => $val['prod_id'], 'depot_id' => $val['depot_id']];
                    $depotProdStock = DepotProdStock::getList($where);
                    if (empty($depotProdStock)) {
                        return self::error('库存不足，不允许出库', 20003);
                    }
                    //按照入库时间排序
                    array_multisort(array_column($depotProdStock, 'create_time'), SORT_ASC, $depotProdStock);
                    if($depot['stock_type'] == 10){ //非良品出库
                        $sum = array_sum(array_column($depotProdStock,'bad_num'));
                    }else{
                        $sum = array_sum(array_column($depotProdStock,'num'));
                    }
                    if ($sum < $val['prod_num']) {
                        return self::error('库存不足，不允许出库', 20004);
                    }
                    //依次减去出库数量
                    $prodNum = $val['prod_num'];//待出库数量
                    foreach ($depotProdStock as $k => $v) {
                        if($depot['stock_type'] == 10) { //非良品出库
                            $num = $v['bad_num'] - $prodNum;
                            if ($num >= 0) {
                                $data['update'][$v['id'].$key.$k]['id'] = $v['id'];
                                $data['update'][$v['id'].$key.$k]['bad_num'] = $num;
                                break;
                            }
                            $prodNum = abs($num);
                            $data['update'][$v['id'].$key.$k]['id'] = $v['id'];
                            $data['update'][$v['id'].$key.$k]['bad_num'] = 0;
                        }else{
                            $num = $v['num'] - $prodNum;
                            if ($num >= 0) {
                                $data['update'][$v['id'].$key.$k]['id'] = $v['id'];
                                $data['update'][$v['id'].$key.$k]['num'] = $num;
                                $data['update'][$v['id'].$key.$k]['total_num'] = $num;
                                break;
                            }

                            $prodNum = abs($num);
                            $data['update'][$v['id'].$key.$k]['id'] = $v['id'];
                            $data['update'][$v['id'].$key.$k]['num'] = 0;
                            $data['update'][$v['id'].$key.$k]['total_num'] = 0;
                        }

                    }
                }
                
            }
            return self::success($data);
            
        }
        
        /**
         * 获取库房产品明细
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/3/27
         * Time: 15:55
         * @param $directCompanyId
         * @param $orgType
         * @param $prodId
         * @param $prodBatch
         * @param $depotId
         * @return array|bool|null|\yii\db\ActiveRecord
         */
        public static function getDepotProdStock($directCompanyId,$prodId,$prodBatch,$depotId)
        {
            $where = ['direct_company_id'=>$directCompanyId,'prod_id'=>$prodId,'prod_batch'=>$prodBatch,'depot_id'=>$depotId];
            $depot = DepotProdStock::getOne($where);
            if (!$depot) {
                return false;
            }
            return $depot;
        }
        
        /**
         * 获取关联单数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/2
         * Time: 10:04
         * @param $directCompanyId
         * @param $orgType
         * @param $relId
         * @param $model
         * @param $detail
         * @return array
         */
        public static function getRelationList($directCompanyId,$relId,$model,$detail,$stockType = '')
        {
            if ($stockType) {
                //type为合同类型时直接查找合同详情
                if($stockType ==2 ){
                    $relation = $model::getOne(['id'=>$relId]);
                }else{
                    $relation = $model::getOne(['id'=>$relId]);
                    //$relation = $model::getOne(['id'=>$relId,'stock_type'=>$stockType]);
                }
            }else{
                $relation = $model::getOne(['id'=>$relId]);
            }
            if (!$relation) {
                return self::error('获取关联单失败',20001);
            }
            //type为合同类型时直接查找合同详情
            if($stockType ==2 ) {
                $where = [
                    ['a.parent_id'=>$relId]
                ];
            }else{
                $where = [
                    //['a.direct_company_id'=>$directCompanyId],
                    ['a.parent_id'=>$relId]
                ];
            }



            //获取采购单产品详情并计算出可采购数量

            $product = $detail::getProduct($where);
            if (!$product) {
                return self::error('获取关联单失败',20002);
            }
            
            //调用产品逻辑层组合数据方法
            $product = ProductLogic::changeData($product,1);
            if (!$product) {
                return self::error('获取关联单失败',20003);
            }
            
            //单位
            $unit = self::getUnit();
            if ($unit) {
                foreach ($product as $k => $v) {
                    if (isset($unit[$v['unit_id']])) {
                        $product[$k]['unit_name'] = $unit[$v['unit_id']];
                    }
                }
            }
            //返回采购单主题及可采购产品数量
            $data = [];
            $data['relName'] = $relation['subject'];
            $data['relNo'] = isset($relation['no'])?$relation['no']:'';
            $data['product'] = $product;
            return $data;
        }
        
        /**
         * 获取库存数量
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/19
         * Time: 10:56
         * @return array
         */
        public static function getDepotNum()
        {
            $stockType = intval(Yii::$app->request->get('stockType',0));
            $depotId = intval(Yii::$app->request->get('depotId',0));
            $prodId  = intval(Yii::$app->request->get('prodId',0));
            if ($depotId == 0) {
                return self::error('获取数据失败',20001);
            }
            if ($prodId == 0) {
                return self::error('获取数据失败',20002);
            }
            $where[] = ['=','depot_id',$depotId];
            $where[] = ['=','prod_id',$prodId];
            if($stockType == 10){
                $where[] = ['>','bad_num',0];
            }
            $depot = DepotProdStock::getList($where,1);
            if($stockType == 10){
                $num = array_column($depot,'bad_num');
            }else{
                $num = array_column($depot,'num');
            }
            $sum = array_sum($num);
            return self::success($sum);
        }
        
        /**
         * 审批前修改产品明细数据
         * User: xingq
         * Email: liuxingqi@services.cn
         * Date: 2018/4/24
         * Time: 19:05
         * @param $flag
         * @return array
         * @throws \Exception
         */
        public static function edit($flag)
        {
            $id = intval(Yii::$app->request->post('id',0));
            //需要修改的数据
            $depot = Yii::$app->request->post('depot','');
            
            $where = [
                'direct_company_id'=>BaseLogic::getDirectCompanyId(),
                'parent_id'=>$id
            ];
            //查询出需要的详情
            $detail = StockDetail::getList($where);
            //替换需要修改的数据
            foreach ($detail as $key => $val) {
                foreach ($depot as $k => $v) {
                    if ($val['id'] == $v['detail_id']){
                        $detail[$key]['depot_id']          = $v['depot_id'];
                        if ($flag == 1) {
                            $detail[$key]['prod_batch']    = $v['prod_batch'];
                        }elseif ($flag == 2){
                            $detail[$key]['serial_number'] = $v['serial_number'];
                        }
                    }
                }
            }
            $res = StockInfo::edit($detail);
            if ($res) {
                return self::success('','成功');
            }
            return self::error('失败',20001);
        }


        /**
         * @desc: 根据关联单类型返回出入库类型
         * @param $type 1为入库，2为出库
         * @param $relType 关联单类型 1：采购入库，2：合同出库，3：退货入库，4：换货入库出库，5：配件申请出库，6：配件归还入库 7 采购退货出库
         * @return int|string
         */
        public static function getStockTypeFromRelType($relType, $type = 0)
        {
            switch ($relType) {
                case 1:
                    $stockType = 1;
                    break;

                case 2:
                    $stockType = 6;
                    break;

                case 3:
                    $stockType = 8;
                    break;

                case 4:
                    if($type == 1){
                        $stockType = 7;
                    }else{
                        $stockType = 7;
                    }
                    break;

                case 5:
                    $stockType = 2;
                    break;
                case 6:
                    $stockType = 3;
                    break;
                case 7:
                    $stockType = 9;
                    break;
                default:
                    $stockType = '';
            }

            return $stockType;
        }

        /** 获取发货状态
         * @param $id 关系id
         * @param $num 合同中产品总数量
         * @param $rel_type 关联单据类型：1：采购，2：合同，3：退货，4：换货，5：配件申请，6：配件归还,enum_stock_rel_type
         * @return array
         */
        public static function getRelShipStatus($id,$num,$rel_type='')
        {
            $stockInfoList = StockInfo::getList(['rel_id'=>$id,'rel_type'=>$rel_type,'audit_status'=>2]);
            //print_r($stockInfoList);die;
            if(!$stockInfoList){
                return ['status'=>1,'msg'=>' 待发货'];
            }
            $prodTotalSum = 0;
            foreach ($stockInfoList as $key=>$val){
                $prodSum = self::getProdSum($val['id']);
                $prodTotalSum += $prodSum['data'];
            }
            //出库单产品数量小于订单总数量的时候
            if($num > $prodTotalSum){
                foreach ($stockInfoList as $k=>$v){
                    if(in_array($v['isship'],[2,3])){
                        return ['status'=>2,'msg'=>'部分发货'];
                    }
                    if($v['isship'] == 2){
                        //return ['status'=>2,'msg'=>'部分发货'];
                    }
                }
            }else{
                //合同产品已全部创建完出库单
                $finishd = implode(',',array_unique(array_column($stockInfoList,'isship')));
                if($finishd === '3'){
                    return ['status'=>3,'msg'=>'全部发货'];
                }
                foreach ($stockInfoList as $k=>$v){
                    if(in_array($v['isship'],[2,3])){
                        return ['status'=>2,'msg'=>'部分发货'];
                    }
                }
            }
            return ['status'=>1,'msg'=>' 待发货'];
        }
        
        
    }
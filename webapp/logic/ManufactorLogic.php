<?php
namespace webapp\logic;
use webapp\models\BaseModel;
use webapp\models\StoresSkill;
use Yii;
use common\models\Region;
use webapp\models\Manufactor;
use webapp\models\Brand;
use common\models\user;
use webapp\models\Department;

class ManufactorLogic extends BaseLogic
{
    //显示
    public static function getOne($id='')
    {
        $model = Manufactor::findOne(['id' => $id]);
        if (!$model) {
            return false;
        }
        //获取开通省份
        $data['province'] = Region::getList(['parent_id'=>1]);
        $data['model'] = ManufactorLogic::getAddress($model);

        return $data;
    }
    public static function addList()
    {
        $data['province'] = Region::getList(['parent_id'=>1]);
        //查询资料
        $manu = Manufactor::getOne(['user_id'=>Yii::$app->user->id]);
        $haveSkill = '';
        $model = $manu;
        //判断是否存在
        /*if ($manu) {
            $model = $manu;
            //获取已经存在的技能
            $haveSkillArr     = StoresSkill::getList(self::getManufactorId());
            foreach($haveSkillArr as $v){
                $haveSkill[] = $v['service_id'].'_'.$v['class_id'];
            }
            $data['haveSkill'] = $haveSkill;
            $classList = self::getClassList();
            $typeList = self::getTypeList();
            $skill = [];
            foreach($typeList['data'] as $k=>$v){
                $skill[$k]= ['type_id'=>$k,'type_name'=>$v];
                foreach($classList['data'] as $key=>$val){
                    $skill[$k]['class'][] = ['class_id'=>$key,'class_name'=>$val];
                }
            }
        }else{
            $model = new Manufactor();
            $classList = self::getClassList();
            $typeList = self::getTypeList();
            $skill = [];
            foreach($typeList['data'] as $k=>$v){
                $skill[$k]= ['type_id'=>$k,'type_name'=>$v];
                foreach($classList['data'] as $key=>$val){
                    $skill[$k]['class'][] = ['class_id'=>$key,'class_name'=>$val];
                }
            }
            $haveSkill = [];
        }
        $data['haveSkill'] = $haveSkill;
        $data['skill'] = $skill;*/
        //获取省市区中文
        $data['model'] = ManufactorLogic::getAddress($model);
        return $data;
    }
    /**
     * //添加资料
     * @param
     * @param
     * @return array
     * @author sxz
     * @date   2018-06-7
     */
    public static function add()
    {
        //获取企业资料
        $manufactor = [];
        $manufactor['id']                   = intval(Yii::$app->request->post('id',0));
        $manufactor['user_type']            = intval(Yii::$app->request->post('user_type',0));
        $manufactor['document_type']        = intval(Yii::$app->request->post('document_type',0));
        $manufactor['company']              = trim(Yii::$app->request->post('company',''));
        $manufactor['business_licence_num'] = trim(Yii::$app->request->post('business_licence_num',''));
        $manufactor['province_id']          = intval(Yii::$app->request->post('province_id',0));
        $manufactor['city_id']              = intval(Yii::$app->request->post('city_id',0));
        $manufactor['district_id']          = intval(Yii::$app->request->post('district_id',0));
        $manufactor['address']              = trim(Yii::$app->request->post('address',''));
        $manufactor['scope_operation']      = trim(Yii::$app->request->post('scope_operation',''));
        $manufactor['contact']              = trim(Yii::$app->request->post('contact',''));
        $manufactor['mobile']               = trim(Yii::$app->request->post('mobile',0));

        $manufactor['user_id'] = Yii::$app->user->id;
        
        if ($manufactor['company'] == '') {
            return self::error('请填写公司名称',20001);
        }
        if ($manufactor['business_licence_num'] == '') {
            return self::error('请填写营业执照上的注册号',20002);
        }
        if ($manufactor['province_id'] == 0) {
            return self::error('请选择所在区域',20003);
        }
        if ($manufactor['city_id'] == 0) {
            return self::error('请选择所在区域',20003);
        }
        if ($manufactor['district_id'] == 0) {
            return self::error('请选择所在区域',20003);
        }
        if ($manufactor['address'] == '') {
            return self::error('请输入详细地址',20004);
        }
        if ($manufactor['scope_operation'] == '') {
            return self::error('请输入经营范围',20005);
        }
        if ($manufactor['contact'] == '') {
            return self::error('请输入联系人姓名',20006);
        }
        if ($manufactor['mobile'] == '') {
            return self::error('请输入联系人电话',20007);
        }
        $pre = '/^1(3[0-9]|47|5[0,1,2,3,5,6,7,8,9]|66|7[0,6,7]|8[0,2,3,5,6,7,8,9]|9[8,9])[0-9]{8}$/';
        if (!preg_match($pre,$manufactor['mobile'])) {
            return self::error('手机号格式不正确',20003);
        }
        
        if ($manufactor['id'] == 0) {
            $res = Manufactor::add($manufactor);
        }else{
            $res = Manufactor::edit($manufactor);
        }

        if ($res){
            return self::success('', '提交成功');
        }
        return self::error('失败');

    }
    

    /**
     * //修改资料
     * @param
     * @param
     * @return array
     * @author sxz
     * @date   2018-06-7
     */
    public static function editInfo()
    {
        $id                         = intval(Yii::$app->request->post('id',0));
        $manufactor['email']        = trim(Yii::$app->request->post('email',''));
        $manufactor['mobile']       = trim(Yii::$app->request->post('mobile',''));
        $manufactor['contact']      = trim(Yii::$app->request->post('contact',''));
        $manufactor['province_id']  = intval(Yii::$app->request->post('province_id',0));
        $manufactor['city_id']      = intval(Yii::$app->request->post('city_id',0));
        $manufactor['district_id']  = intval(Yii::$app->request->post('district_id',0));
        $manufactor['address']      = trim(Yii::$app->request->post('address',''));
        $manufactor['company_desc'] = trim(Yii::$app->request->post('company_desc',''));
        
        if ($id == 0) {
            return self::error('数据错误',20005);
        }
        if ($manufactor['province_id'] == 0) {
            return self::error('请选择所在区域',20001);
        }
        if ($manufactor['city_id'] == 0) {
            return self::error('请选择所在区域',20002);
        }
        if ($manufactor['district_id'] == 0) {
            return self::error('请选择所在区域',20003);
        }
        if ($manufactor['address'] == '') {
            return self::error('请输入详细地址',20004);
        }
        $pre = '/^1(3[0-9]|47|5[0,1,2,3,5,6,7,8,9]|66|7[0,6,7]|8[0,2,3,5,6,7,8,9]|9[8,9])[0-9]{8}$/';
        if ($manufactor['mobile']!= '' && !preg_match($pre,$manufactor['mobile'])) {
            return self::error('手机号格式不正确',20005);
        }
        if ($manufactor['email']!='' && !preg_match("/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/", $manufactor['email'])) {
            return self::error('邮箱格式不正确',20006);
        }
        $res =Manufactor::editInfo($id,$manufactor);
        if ($res ==1)
        {
            $where = [
                'manufactor_id' => $id,
                'status'        => 1,
                'del_status'    => 1,
                'parent_id'     => 0
            ];
            $departmentInfo = Department::find()->where($where)->one();
            if($departmentInfo)
            {
                $departmentInfo->remark = $manufactor['company_desc'];
                $departmentInfo->save(false);
            }

            return self::success('','保存成功');
        }
        if ($res ==2) {
            return self::error('保存失败');
        }
        if ($res ==3) {
            return self::error('信息未通过审核，保存失败');
        }
    }
    //获取省市区名称
    public static function getAddress($model)
    {
        //判断是否存在数据
        if ($model) {
            $model->province_name = self::getCityName($model->province_id);
            $model->city_name     = self::getCityName($model->city_id);
            $model->district_name = self::getCityName($model->district_id);
        }
        return $model;
    }
    //获取商家所属品牌
//    public static function getBrand()
//    {
//        $res = Brand::getAll();
//        $brand = [];
//        foreach ($res as $key => $val) {
//            $brand[$val['brand_id']] = $val['brand_name'];
//        }
//        return $brand;
//    }
    
    public static function getCityName($id)
    {
        $res = Region::getCityName(['region_id'=>$id]);
        $cityName = '';
        if ($res) {
            foreach ($res as $val) {
                $cityName = $val['region_name'];
            }
        }
        return $cityName;
    }

    /**
     * //根据department_id获取顶级公司信息
     * @param
     * @param
     * @return array
     * @author sxz
     * @date   2018-06-7
     */
    public static function getInfo($department_id='')
    {
        $info = Department::getOne(['id'=>$department_id]);

        $data = Manufactor::findOne(['id' => $info['manufactor_id']]);
        if (!$data) {
            return false;
        }
        return $data;
    }
}
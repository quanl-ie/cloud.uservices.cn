<?php
namespace webapp\logic;
use common\helpers\Gaode;
use webapp\models\Account;
use webapp\models\User;
use yii;
use webapp\models\AccountAddress;
use webapp\models\Region;
use webapp\models\BaseModel;

class AccountAddressLogic extends BaseLogic
{
    //显示
    public static function getList()
    {
        $id = intval(yii::$app->request->get('id',0));
        $page = intval(Yii::$app->request->get('page',1));
        $params = ['account_id'=>$id/*,'is_default'=>2*/];
        $data = AccountAddress::getList($params,$page);
        foreach ( $data['list'] as $key => $val) {
            $data['list'][$key]['area'] = 
                Region::getCityName($val['province_id'])."-".
                Region::getCityName($val['city_id'])."-".
                Region::getCityName($val['district_id']);
        }
        return $data;
    }
    //添加
    public static function add()
    {
        $id       = intval(yii::$app->request->post('account_id',0));
        $name     = trim(yii::$app->request->post('conact_name',''));
        $mobile   = Yii::$app->request->post('mobile_phone',0);
        $province  = intval(yii::$app->request->post('province_id',0));
        $city     = intval(yii::$app->request->post('city_id',0));
        $district = intval(yii::$app->request->post('district_id',0));
        $address  = trim(yii::$app->request->post('address',''));
        $lon      = yii::$app->request->post('lon',''); //经度
        $lat      = yii::$app->request->post('lat',''); //纬度
        
        if ($id == 0) {
            return BaseLogic::error('数据错误!',20008);
        }
        if ($name == '') {
            return BaseLogic::error('请输入联系人姓名!',20001);
        }
        if ($mobile == 0) {
            return BaseLogic::error('请输入联系人电话!',20002);
        }
        /*$pre = '/^1(3[0-9]|47|5[0,1,2,3,5,6,7,8,9]|66|7[0,6,7]|8[0,2,3,5,6,7,8,9]|9[8,9])[0-9]{8}$/';
        if (!preg_match($pre,$mobile)) {
            return self::error('手机号格式不正确',20003);
        }*/
        if (!preg_match('/^((0[0-9]{2,3}\-*)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?)|(1\d{10})$/',$mobile)) {
            return self::error('客户电话格式不正确',20002);
        }
        if ($province == 0) {
            return BaseLogic::error('请选择所在区域!',20004);
        }
        if ($city == 0) {
            return BaseLogic::error('请选择所在区域!',20005);
        }
        if ($district == 0) {
            return BaseLogic::error('请选择所在区域!',20006);
        }
        if ($address == '') {
            return BaseLogic::error('请输入详细地址!',20007);
        }
        $cityDesc = Region::getCityName($city);
        $res = Gaode::getLocation(str_replace(' ','',$address),$cityDesc);
        $lon = $res['lon'];
        $lat = $res['lat'];
        if ($lon == '' || $lon == 0) {
            return self::error('详细地址输入有误!',20007);
        }
        if ($lat == '' || $lat == 0) {
            return self::error('详细地址输入有误!',20007);
        }
        //接受整理客户联系地址数据
        $account = Account::findOneByAttributes(['id'=>$id],"department_id");
        $data['lon']          = $lon;
        $data['lat']          = $lat;
        $data['department_id']= $account['department_id'];  //来源id
        $data['province_id']   = $province;  //省份id
        $data['city_id']      = $city;  //市id
        $data['district_id']  = $district; //区/县id
        $data['address']      = $address; //详细地址
        $data['is_default']   = intval(2); //默认为1
        $data['create_time']  = $data['update_time'] = intval(time());
        $data['status']       = intval(1);  //默认为1
        $data['account_id']   = $id;
        $data['conact_name']  = $name;
        $data['mobile_phone'] = $mobile;

        //调用model执行添加
        $res = AccountAddress::add($data);
        if ($res) {
            return BaseLogic::success(['id'=>$res],'添加成功');
        }
        return BaseLogic::error('添加失败');
    }
    
    //查询客户联系地址
    public static function getAddress($id)
    {
        $accountAddress = AccountAddress::getOne(['account_id'=>$id,'is_default'=>1,'status'=>1]);
        
        //判断是否存在数据
        if ($accountAddress) {
            $accountAddress->province_name = Region::getCityName($accountAddress->province_id);
            $accountAddress->city_name     = Region::getCityName($accountAddress->city_id);
            $accountAddress->district_name = Region::getCityName($accountAddress->district_id);

            $accountAddress->cityLabel = $accountAddress->province_name.'，'.$accountAddress->city_name.'，'.$accountAddress->district_name;
        }
        return $accountAddress;
    }
    
}
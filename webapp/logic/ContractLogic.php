<?php
namespace webapp\logic;

use common\models\ExchangeGoodsDetail;
use common\models\ReturnGoodsDetail;
use common\models\SendOut;
use webapp\models\Brand;
use yii;
use common\helpers\CreateNoRule;
use common\helpers\Helper;

use common\models\Contract;
use common\models\ContractDetail;
use common\models\StockInfo;
use common\models\ReturnGoods;
use common\models\ExchangeGoods;
use common\models\Product;
use common\models\ServiceBrand;

use webapp\models\BaseModel;
use webapp\models\AdminUser;
use webapp\models\Department;
use webapp\models\ServiceClass;
use webapp\models\Account;
use webapp\models\AccountAddress;
use webapp\models\User;
use common\models\ContractAttachment;
use common\models\DictEnum;


/**
 * @author chengjuanjuan<chengjuanjuan@c-ntek.com>
 * @created_at 2017-12-14 14:01
 * desc the logic of sale orders
 */


class ContractLogic extends BaseLogic
{


    /**
     * 函数用途描述：
     * @date: 2017年12月14日 下午14:48:01
     * @author: chengjuanjuan<chengjuanjuan@c-ntek.com>
     * @param: $id
     * @return:$data
     */
    public static function getList($flag = '',$shipSys='')
    {
        $subject               = Yii::$app->request->get('subject');
        $account_id            = intval(yii::$app->request->get('account_id'));  //此参数用于从客户详情中查看此客户的合同信息，不影响现有的搜索，不要删除 by sxz
        $accountName           = trim(Yii::$app->request->get('account_name'));
        $accountMobile         = trim(Yii::$app->request->get('account_mobile'));
        $subject               = Helper::filterWord($subject);
        $accountName           = Helper::filterWord($accountName);
        $getDataAuth           = self::getListDataAuth();
    
        if(empty($getDataAuth)){
            exit('无数据权限');
        }

       $departmentIds          = $getDataAuth['department_id'];
       $dataAuth               = $getDataAuth['dataAuth'];
       $createUserId           = $getDataAuth['create_user_id']?$getDataAuth['create_user_id']:'';
       $department_name        = $getDataAuth['department_name'];
       if($createUserId){
           $query['create_user_id'] = $createUserId;
       }
       $query['department_id'] = $departmentIds;
       $query['currentPage']   = Yii::$app->request->get('page',1);
       $query['pageSize']      = Yii::$app->request->get('pageSize',10);
       $query['startTime']     = trim(Yii::$app->request->get('plan_start_time'));
       $query['endTime']       = trim(Yii::$app->request->get('plan_end_time'));
       $query['no']            = trim(Yii::$app->request->get('no'));

        if($accountMobile){
            $accountName = $accountMobile;
        }
        if($accountName){
            $accountIds = '';
            $accountList = Account::getAccountInfo($accountName,self::getDepartmentId(),2);
            foreach($accountList as $v){
                $accountIds[] = $v['id'];
            }
            if($accountIds){
                $query['account_id'] = $accountIds;
            }else{
                $query['account_id'] = '-1';
            }
        }

        //此参数用于从客户详情中查看此客户的合同信息，不影响现有的搜索，不要删除 by sxz
        if($account_id){
            $query['account_id'] = $account_id;
        }

        //确定合同状态
        $action    = Yii::$app->controller->action->id;
        
        if ($flag)
        {
            $query['pageSize'] = 10000;
            $action = 'get-all-contract';
        }
        
        if($action == 'wait-check'){
            $query['status'] = 1;
            //审批通过的合同直接进入执行中
        }elseif($action == 'executing'){
            $query['status'] = 3;
        }elseif($action == 'execute-finished') {
            $query['status'] = 4;

        }elseif($action == 'unchecked') {
            $query['status'] = 5;

        }elseif($action == 'terminated') {
            $query['status'] = 6;
        }elseif ($action == 'get-contract') {
            $query['status'] = [1,3];
        }elseif ($action == 'get-all-contract') {
            $query['status'] = [1,3,4,5,6];
        }
        $query['subject']          = $subject;
        $data = Contract::getList($query);
        $showStoreOutStatus = Contract::showStoreOutStatus();
        if(!empty($data['list'])){
            $contractNewNumArr = [];
            //销售开关开启时候
            if($shipSys == 1){
                $contIds = array_column($data['list'],'id');
                //查询合同详情的数量
                $contractNumArr = ContractDetail::findAllByAttributes(['parent_id'=>$contIds,'status'=>1],'id,parent_id,prod_id,prod_num');
                $result = [];
                foreach ($contractNumArr as $key=>$val) {
                    $result[$val['parent_id']][] = $val['prod_num'];
                }
                foreach ($result as $m =>$n){
                    $val = array_values($n);
                    $contractNewNumArr[$m] = array_sum($val);
                }
            }
            $rel_type = 2; //出入库关联类型
            foreach($data['list'] as &$v){
                $accountInfo               = Account::getOne(['id'=>$v['account_id']]);
                $v['account_name']         = isset($accountInfo) ? $accountInfo->account_name:'';
                $v['account_mobile']       = isset($accountInfo) ? $accountInfo->mobile:'';
                $v['option']               = $dataAuth[$v['department_id']]['option'];
                $v['department_name']      = $dataAuth[$v['department_id']]['name'];
                if(isset($query['status']) &&$query['status'] ==3){
                    $v['showStoreOutStatus'] = $showStoreOutStatus[$v['store_out_status']];
                }
                //销售开关开启时候
                if($shipSys == 1 && !empty($contractNewNumArr)){
                    $v['prod_total_num'] = $contractNewNumArr[$v['id']];
                    //获取当前合同的发货状态
                    $shipStatusInfo         = StockInfoLogic::getRelShipStatus($v['id'],$v['prod_total_num'],$rel_type);
                    $v['ship_status']       = $shipStatusInfo['status'];
                    $v['ship_status_desc']  = $shipStatusInfo['msg'];
                }
            }
        }
        $departmentList          = array_column($dataAuth,'name','id');
        $data['department_name'] = $department_name;
        return $data;
    }
    /**
     * @desc 合同添加
     * @author:程娟娟
     * @createAt:20171215
     **/
    public static function add()
    {//处理数据
        //$postData = yii::$app->request->post();
        //print_r($postData);die;
        $accountId                 = intval(yii::$app->request->post('account_id'));
        $subject                   = trim(yii::$app->request->post('subject'));
        if(!$accountId)
            return self::error('用户不存在');
        //产品处理明细
        $products                   =   yii::$app->request->post('prod');
        //基本信息
        $data['department_top_id']  =   self::getTopId();
        $data['department_id']      =   self::getDepartmentId();
        $data['no']                 =   CreateNoRule::getNo('HT');
        $data['account_id']         =   $accountId;
        $data['subject']            =   $subject;
        $data['signed_date']        =   trim(yii::$app->request->post('signed_date',date('Y-m-d H:i:s')));
        $data['start_date']         =    trim(yii::$app->request->post('start_date',date('Y-m-d H:i:s')));
        $data['collection_date']    =   trim(yii::$app->request->post('collection_date'));
        $data['collection_status']  =   trim(yii::$app->request->post('collection_status'));    //付款状态
        $data['type']               =   trim(yii::$app->request->post('contract_type'));        //合同类型
        $data['classify']           =   trim(yii::$app->request->post('contract_classify'));    //合同分类
        $data['brand']              =   trim(yii::$app->request->post('contract_brand'));       //合同品牌
        $data['payment_type']       =   trim(yii::$app->request->post('payment_type'));         //支付类型
        $data['estimate_date']      =   trim(yii::$app->request->post('estimate_date'));        //预计安装时间
        $data['sale_shop']          =   trim(yii::$app->request->post('department_id'));        //销售店面id
        $data['sale_user_id']       =   trim(yii::$app->request->post('sale_user_id'));         //销售人员id
        //$data['sale_user_name']     =   trim(yii::$app->request->post('sale_user_name'));
        //$data['sale_phone']         =   Yii::$app->request->post('sale_phone');
        $data['another_cost_amount']=   Yii::$app->request->post('another_cost_amount');
        $data['total_amount']       =   trim(Yii::$app->request->post('total_amount'));
        //发票信息
        $data['invoice_type']       =   intval(Yii::$app->request->post('invoice_type'));
        $data['invoice_title']      =   trim(Yii::$app->request->post('invoice_title'));
        $data['invoice_content']    =   trim(Yii::$app->request->post('invoice_content'));
        $data['invoice_tax_no']     =   trim(yii::$app->request->post('invoice_tax_no'));
        $data['express_type']       =   trim(yii::$app->request->post('express_type'));
        $data['account_address_id'] =   intval(yii::$app->request->post('address_id'));
        $data['delivery_address']   =   trim(yii::$app->request->post('delivery_address'));
        $data['express_description']=   Helper::filterWord(yii::$app->request->post('express_description'));
        //查询销售人员信息
        $account = User::getOne(['id'=>$data['sale_user_id']]);
        $data['sale_user_name'] = isset($account['username'])?$account['username']:'';
        $data['sale_phone']     = isset($account['mobile'])?$account['mobile']:'';
        //附件
        $attachmentUrl = '';
        $ContractForm        = Yii::$app->request->post("ContractForm",[]);
        $fileArr = [];
        if ($ContractForm['attachment_url'])
        {
            //循环分割附件名称和地址
            foreach ($ContractForm['attachment_url'] as $key=>$val)
            {
                $fullInfo = explode('*',$val);
                $fileArr[$key]['name']              = isset($fullInfo[0])?$fullInfo[0]:'';
                $fileArr[$key]['attachment_url']    =isset($fullInfo[1])?$fullInfo[1]:'';
            }
        }
        //系统信息
        $data['create_time']        =   date("Y-m-d H:i:s");
        $data['create_user_id']     =  Yii::$app->user->id;
        $data['status']             =  yii::$app->request->post('status','1');
        $res = Contract::add($data,$products,$fileArr);
        if ($res) {
            return self::success([
                'id' => $res
            ],'添加成功');
        }
        return self::error('添加失败');
    }



    /**
     * @desc 修改
     * @author chengjuanjuan <chengjuanjuan@c-ntek.com>
     * @createAt 2017-12-18 16:06
     **/
    public static function edit()
    {
        $postData = yii::$app->request->post();
        //print_r($postData);die;
        $id                        = intval(Yii::$app->request->post('id'));
        $accountId                 = intval(yii::$app->request->post('account_id'));
        $subject                   = trim(yii::$app->request->post('subject'));
        if(!$id)
            return self::error('该合同不存在');
        //产品处理明细
        $products                   =   yii::$app->request->post('prod');
        //基本信息
        //基本信息
        $data['id']                 =   $id;
        $data['department_top_id']  =   self::getTopId();
        $data['department_id']      =   self::getDepartmentId();
        $data['no']                 =   CreateNoRule::contractNo();
        $data['account_id']         =   $accountId;
        $data['subject']            =   $subject;
        $data['signed_date']        =   trim(yii::$app->request->post('signed_date',date('Y-m-d H:i:s')));
        $data['start_date']         =    trim(yii::$app->request->post('start_date',date('Y-m-d H:i:s')));
        $data['collection_date']    =   trim(yii::$app->request->post('collection_date'));
        $data['collection_status']  =   trim(yii::$app->request->post('collection_status'));    //付款状态
        $data['type']               =   trim(yii::$app->request->post('contract_type'));        //合同类型
        $data['classify']           =   trim(yii::$app->request->post('contract_classify'));    //合同分类
        $data['brand']              =   trim(yii::$app->request->post('contract_brand'));       //合同品牌
        $data['payment_type']       =   trim(yii::$app->request->post('payment_type'));         //支付类型
        $data['estimate_date']      =   trim(yii::$app->request->post('estimate_date'));        //预计安装时间
        $data['sale_shop']          =   trim(yii::$app->request->post('department_id'));        //销售店面id
        $data['sale_user_id']       =   trim(yii::$app->request->post('sale_user_id'));         //销售人员id
        //$data['sale_user_name']     =   trim(yii::$app->request->post('sale_user_name'));
        //$data['sale_phone']         =   Yii::$app->request->post('sale_phone');
        $data['another_cost_amount']= Yii::$app->request->post('another_cost_amount');
        $data['total_amount']       =   trim(Yii::$app->request->post('total_amount'));
        $data['status'] = 1;
        $data['audit_status'] = 0;
        //发票信息
        $data['invoice_type']       =   intval(Yii::$app->request->post('invoice_type'));
        $data['invoice_title']      =   trim(Yii::$app->request->post('invoice_title'));
        $data['invoice_content']    =   trim(Yii::$app->request->post('invoice_content'));
        $data['invoice_tax_no']     =   trim(yii::$app->request->post('invoice_tax_no'));
        $data['express_type']       =   trim(yii::$app->request->post('express_type'));
        $data['delivery_address']   =   trim(yii::$app->request->post('delivery_address',''));
        $data['express_description']=   Helper::filterWord(yii::$app->request->post('express_description'));
        //查询销售人员信息
        $account = User::getOne(['id'=>$data['sale_user_id']]);
        $data['sale_user_name'] = isset($account['username'])?$account['username']:'';
        $data['sale_phone']     = isset($account['mobile'])?$account['mobile']:'';
        //附件
        $attachmentUrl = '';
        $ContractForm        = Yii::$app->request->post("ContractForm",[]);
        $fileArr = [];
        if ($ContractForm['attachment_url'])
        {
            //循环分割附件名称和地址
            foreach ($ContractForm['attachment_url'] as $key=>$val)
            {
                $fullInfo = explode('*',$val);
                $fileArr[$key]['name']              = isset($fullInfo[0])?$fullInfo[0]:'';
                $fileArr[$key]['attachment_url']    =isset($fullInfo[1])?$fullInfo[1]:'';
            }
        }
        //$data['attachment_url']    =   $attachmentUrl;
        //系统信息
        $data['create_time']       =   date("Y-m-d H:i:s");
        $data['create_user_id']    = Yii::$app->user->id;
        $data['status']            =  yii::$app->request->post('status','1');
        $res = Contract::edit($data,$products,$fileArr);
        if ($res) {
            return self::success('','编辑成功');
        }
        return self::error('编辑失败');
    }


    public static function  getDetail($id='',$contractNo='',$type=''){
        if(!$contractNo){
            $contractNo = trim(Yii::$app->request->get('cno',''));
            if($contractNo){
                $noInfo = Contract::getOne(['no'=>$contractNo]);
                $id     = $noInfo['id'];
            }
        }
        if(!$id){
            $id = Yii::$app->request->get('id');
        }
        if(!$id){
            return self::error('该合同id不存在');
        }

        $getDataAuth = self::getListDataAuth();
        $demartmentId = $getDataAuth['department_id'];
        $info = Contract::getOne(['id'=>$id,'department_id'=>$demartmentId]);
        if(empty($info)){
            return self::error('该合同不存在');
        }
        $account = Account::getOne(['id'=>$info['account_id']]);
        $account_name       = isset($account->account_name)?$account->account_name:'';
        $account_mobile     = isset($account->mobile)?$account->mobile:'';
        $account_address    = isset($account->address)?$account->address:'';
        $address = AccountAddress::getOne(['account_id'=>$info['account_id'],'is_default'=>1]);
        $info['account_name']            = $account_name.'/'.$account_mobile.'/'.$account_address;
        $info['account']                 = $account_name;
        $info['mobile']                  = $account_mobile;
        $showStatus                      = Contract::showStatus();
        $info['show_status']             = isset($showStatus[$info['status']])?$showStatus[$info['status']]:'未知';
        $showCollectionStatus            = Contract::showCollectionStatus();
        $info['show_collection_status']  = $showCollectionStatus[$info['collection_status']];
        $showInvoiceType                 = Contract::showInvoiceType();
        $info['show_invoice_type']       = $showInvoiceType[$info['invoice_type']];
        $showExpressType                 = Contract::showExpressType();
        $info['show_express_type']       = $showExpressType[$info['express_type']];
        $contractType = Contract::showContractType();
        $info['contract_type']           = isset($contractType[$info['type']])?$contractType[$info['type']]:'';
        //合同分类 需要获取后台设置信息
        $contractClassifyList = ContractTypeLogic::getTypeList();
        $info['contract_classify']       = isset($contractClassifyList[$info['classify']])?$contractClassifyList[$info['classify']]:'';
        //合同品牌
        $contract_brand = ServiceBrand::findOne($info['brand']);
        $info['contract_brand']          = isset($contract_brand['title'])?$contract_brand['title']:'';
        //合同付款方式
        $paymentType = Contract::showPaymentType();
        $info['show_payment_type']           = isset($paymentType[$info['payment_type']])?$paymentType[$info['payment_type']]:'';
        //获取销售店面信息
        $sale_shop_info = Department::getOne(['id'=>$info['sale_shop']]);
        $info['sale_shop_name'] = isset($sale_shop_info['name'])?$sale_shop_info['name']:'';
        if($info['create_user_id']){
            $admin = AdminUser::getOne(['id'=>$info['create_user_id']]);
            $info['create_user'] = $admin['username'];
        }
        //审核人信息
        $info['audit_user_name'] = '';
        if($info['audit_user_id']){
            $admin = AdminUser::getOne(['id'=>$info['audit_user_id']]);
            $info['audit_user_name'] = $admin['username'];
        }
        //获取附件信息
        $fileData = ContractAttachment::getList(['parent_id'=>$id,'status'=>1]);
        $info['attachment_data'] = isset($fileData)?$fileData:[];
        //获取当前合同的订单已完成的服务记录
        $order_data = [];
        if($info['status']>2 && $info['audit_status']==1){
            $data = OrderLogic::getContractServiceRecord();
            $order_data = isset($data['data']['list'])?$data['data']['list']:[];
        }
        $detail = ContractDetail::getList(['parent_id'=>$id,'status'=>1]);
        foreach($detail as &$v){
            $productInfo      = ProductLogic::getDetail($v['prod_id']);
            if(isset($productInfo['data'])) {
                $product = ProductLogic::changeData(array($productInfo['data']), '');
                if($type == 9){ //合同派单 获取详情
                    $v['brand_id'] = isset($product[0]['brand_id']) ? $product[0]['brand_id'] : '';
                    $v['class_id'] = isset($product[0]['class_id']) ? $product[0]['class_id'] : '';
                }
                $v['prod_name']     = isset($product[0]['prod_name']) ? $product[0]['prod_name'] : '';
                $v['brand_name']    = isset($product[0]['brand_name']) ? $product[0]['brand_name'] : '';
                $v['class_name']    = isset($product[0]['class_name']) ? $product[0]['class_name'] : '';
                $v['model']         = isset($product[0]['model']) ? $product[0]['model'] : '';
                $v['prod_no']       = isset($product[0]['prod_no']) ? $product[0]['prod_no'] : '';
                $v['type_name']     = isset($product[0]['type_name']) ? $product[0]['type_name'] : '';
                $v['unit_name']     = isset($product[0]['unit_name']) ? $product[0]['unit_name'] : '';
                $v['prod_type']     = isset($product[0]['type_id']) ? $product[0]['type_id'] : '';
            }else{
                $detail = [];
            }
        }
        if($type=='all'){
            //获取出库记录
            //这里获取当前账号的直属公司id是否正确？待定
            $stockInfo = StockInfo::getList(['direct_company_id' => self::getDirectCompanyId(),'rel_id' => $info['id'], 'is_rel' => 1 ,'rel_type' => 2,'status'=>2]);
            if($stockInfo){
                //获取发货类型名称
                $sendTypeList = array_column(DictEnum::getList(['dict_key'=>'send_out_type']),'dict_enum_value','dict_enum_id');
                //获取发货方式名称
                $sendMethodList = array_column(DictEnum::getList(['dict_key'=>'send_method_type']),'dict_enum_value','dict_enum_id');
                //发货状态
                $sendStatuslist = ['0'=>'待发货','1'=>'运输中',2=>'已签收'];
                //发货记录
                $stockIds = array_column($stockInfo,'id');
                $sendList = SendOut::findAllByAttributes(['stock_id'=>$stockIds],'id,stock_id,send_no,send_status,send_type,send_method,create_time','stock_id');
                foreach ($stockInfo as $key=>$val){
                    //产品数量
                    if (!empty($val['id'])) {
                        $prodSum = StockInfoLogic::getProdSum($val['id']);
                        $stockInfo[$key]['prod_sum'] = $prodSum['data'];
                    }
                    //出入库状态
                    if (!empty($val['status'])) {
                        $auditStatus = StockInfoLogic::getStatus($val['type'],$val['status']);
                        $stockInfo[$key]['status_desc'] = $auditStatus['data'];
                    }
                    if($sendList){
                        foreach ($sendList as $k1=>$v1){
                            $sendList[$k1]['send_status_desc'] = isset($sendStatuslist[$v1['send_status']])?$sendStatuslist[$v1['send_status']]:'';
                            $sendList[$k1]['send_type_desc']   = isset($sendTypeList[$v1['send_type']])?$sendTypeList[$v1['send_type']]:'';
                            $sendList[$k1]['send_method_desc'] = isset($sendMethodList[$v1['send_method']])?$sendMethodList[$v1['send_method']]:'';

                            if($k1 == $val['id']){
                                $sendList[$k1]['prod_sum'] = $stockInfo[$key]['prod_sum'];
                            }
                        }
                    }
                }
            }
            if(!empty($stockInfo)){
                $stockList = $stockInfo;
            }else{
                $stockList = '';
            }
            $sendListInfo = isset($sendList)?$sendList:[];
            //获取退货记录
            $returnInfo = ReturnGoods::getList(['department_id' => $info['department_id'], 'contract_id' => $info['id'],'status'=>['3','4']]);
            if(!empty($returnInfo['list'])){
                $returnList = $returnInfo['list'];
            }else{
                $returnList = '';
            }
            //获取换货记录
            $exchangeInfo = ExchangeGoods::getList(['department_id' => $info['department_id'], 'contract_id' => $info['id'],'status'=>['3','4']]);
            if(!empty($exchangeInfo['list'])){
                $exchangeList = $exchangeInfo['list'];
            }else{
                $exchangeList = '';
            }
            $showStoreOutStatus = Contract::showStoreOutStatus();
            //if($info['status'] == 3){
                $info['showStoreOutStatus'] = isset($showStoreOutStatus[$info['store_out_status']])?$showStoreOutStatus[$info['store_out_status']]:'';
            //}
            //获取发货进展
            //查询合同详情的数量
            $contractNumArr = ContractDetail::findAllByAttributes(['parent_id'=>$info['id'],'status'=>1],'id,parent_id,prod_id,prod_num');
            $prodTotalSum = 0;
            foreach ($contractNumArr as $key=>$val){
                $prodTotalSum += $val['prod_num'];
            }
            $rel_type = 2; //出入库关联类型
            $shipStatusInfo             = StockInfoLogic::getRelShipStatus($info['id'],$prodTotalSum,$rel_type);
            $info['ship_status']        = isset($shipStatusInfo['status'])?$shipStatusInfo['status']:'';
            $info['ship_status_desc']   = isset($shipStatusInfo['msg'])?$shipStatusInfo['msg']:'';

            $data = [
                'info'	        => $info,
                'detail'	    => $detail,
                'stock_info'    => $stockList,
                'send_list_info'=>$sendListInfo,
                'return_info'   => $returnList,
                'exchange_info' => $exchangeList,
                'order_data_info'=>$order_data
            ];
        }else{
            $data = [
                'info'	        => $info,
                'detail'	    => $detail,
                'order_data_info'=>$order_data
            ];
        }
        return self::success($data);
    }


    public  static function updateStatus($type='check'){
        $id = intval(Yii::$app->request->post('id'));
        if($type=='check'){
            $auditStatus = intval(Yii::$app->request->post('auditStatus'));
            $auditSuggest = trim(Yii::$app->request->post('auditSuggest'));
            if($auditStatus==1){  //审核通过
                $status = 3;
            }elseif($auditStatus==2){ //审核不通过
                $status=5;
            }
            $data = [
                'audit_status'  => $auditStatus,
                'audit_suggest' => $auditSuggest,
                'status       ' => $status,
                'audit_date'    => date("Y-m-d H:i:s"),
                'audit_user_id' => Yii::$app->user->id,
            ];
        }elseif($type=='delete'){
            $data['status'] = 7;
        }elseif($type='terminate'){
            $data['status'] = 6;
        }

        $res = Contract::updateStatus($data,$id);
        if($res){
            return self::success($res);
        }else{
            return self::error('更新失败');
        }
    }


    /**
     * @return array
     * 合同审核操作
     */
    public  static function checkOption(){
        $id           = intval(Yii::$app->request->post('id'));
        $auditStatus  = intval(Yii::$app->request->post('auditStatus'));
        $auditSuggest = Helper::filterWord(Yii::$app->request->post('auditSuggest'));
        // $info = Contract::getOne(['id'=>$id]);
        $data = self::getDetail($id);
        $info = $data['data']['info'];
        $detail = $data['data']['detail'];
        if(!empty($info)){
            self::error('该合同不存在');
        }
        if($auditStatus==1){  //审核通过
            $status = 3;
            //合同里的产品同步到售后产品开始
            if(!empty($detail)){
                $prodIds = array_column($detail,'prod_id');
            }
            $getList = Product::getList(['id'=>$prodIds]);
            $productList = '';
            if(!empty($detail)){
                foreach ($detail as $key=>$val){
                    foreach($getList as $v){
                        $brandName =  self::getBrand($v['brand_id']);

                        $list = [
                                    'department_top_id' => $info['department_top_id'],
                                    'department_id'     => $info['department_id'],
                                    'account_id'        => $info['account_id'],
                                    //'account_name'      => $info['account'],
                                    'prod_id'           => $v['id'],
                                    //'prod_name'         => $v['prod_name'],
                                    'type_id'           => $v['type_id'],
                                    'brand_id'          => $v['brand_id'],
                                    //'first_class_id'    => $v['class_parent_ids'],
                                    'class_id'          => $v['class_id'],
                                    //'brand_name'        => isset($brandName[$v['brand_id']])  ? $brandName[$v['brand_id']]:'',
                                    //'class_name'        => isset($brandName[$v['brand_id']])  ? $brandName[$v['brand_id']]:'',
                                    //'class_name'        => isset($v['class2_id'])?ServiceClass::getName($v['class2_id']):'',
                                    'status'            => 1,
                                    'buy_time'          => strtotime($info['signed_date']),
                                    'warranty_num'      => $v['warranty_num'],
                                    'warranty_type'     => $v['warranty_type'],
                                    'prod_series'       => $v['model'],
                                    'create_time'       => time(),
                                    'update_time'       => time()

                            ];

                        if($val['prod_id'] == $v['id'] && $val['prod_num'] >0){
                            for ($num=1; $num<=$val['prod_num']; $num++) {
                                $productList[] = $list;
                            }
                        }
                    }
                }

            }
            //同步到售后产品结束
        }elseif($auditStatus==2){ //审核不通过
            $status=5;
            $productList = '';
        }
        $data = [
            'audit_status'=>$auditStatus,
            'audit_suggest'=>$auditSuggest,
            'status'=>$status,
            'audit_date'=>date("Y-m-d H:i:s"),
            'audit_user_id'=>Yii::$app->user->id,
        ];
        $res = Contract::checkOption($data,$id,$productList);
        if($res){
            return self::success($res);
        }else{
            return self::error('失败');
        }
    }



    /**
     * 获取用户地址信息
     * @author liwenyong
     * @data    2017-12-20
     */
    public static function getAddress($accountId,$detailAddress='') {
        $address = AccountAddress::getAddress($accountId);
        $str = $check = '';
        $detailAddress = explode('/',$detailAddress);

        if ($address) {
            $str = "<option value=''>请选择客户地址</option>";
            foreach ($address as $k => $v) {
                if(isset($detailAddress[2]) && $v==$detailAddress[2] && $detailAddress!=''){
                  $check = 'selected=true';
                }
                $str.="<option value='".$k."' $check >".$v."</option>";
            }
        }else{
            $str = "<option value=''>该客户暂无联系地址</option>";
        }

        return $str;
    }
    
    /**
     * 获取选取合同信息
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/8/1
     * Time: 16:39
     * @return array
     */
    public static function getContractDetail()
    {
        $id = intval(Yii::$app->request->get('id',0));
    
        if ($id == 0) {
            return self::error('获取合同信息失败', 20001);
        }
        
        $data = Contract::getOne(['id'=>$id]);
        
        if (empty($data)) {
            return self::error('获取合同信息失败', 20002);
        }
        
        return self::success($data,'成功');
        
    }
    //检测合同 出入库详情
    public static function checkContract($contract_id,$type=1){
        $contractDetail = ContractDetail::getList(['parent_id'=>$contract_id]);
        if ($contractDetail) {
            //判断是否有换货
            $flagExchange = 0;
            $exchangeId = [];
            $returnId = [];
            $getExchangeInfo = ExchangeGoods::findAllByAttributes(['contract_id'=>$contract_id],"id,status");
            if($getExchangeInfo)
            {
                //$exIds = array_column($getExchangeInfo,'id');
                //$exchangeId = ExchangeGoodsDetail::findAllByAttributes(['parent_id'=>$exIds],"prod_id");
                foreach ($getExchangeInfo as $val)
                {
                    if($val['status'] != 4){
                        $flagExchange = 1;
                    }
                }
            }
            //判断是否有退货
            $returnInfo = ReturnGoods::findAllByAttributes(['contract_id'=>$contract_id],"id,status");
            foreach ($returnInfo as $val)
            {
                //$reIds = array_column($returnInfo,'id');
                //$returnId = ReturnGoodsDetail::findAllByAttributes(['parent_id'=>$reIds],"prod_id");
                if($val['status'] != 4){
                    $flagExchange = 1;
                }
            }
           // $notId = array_column(array_merge($exchangeId,$returnId),'prod_id');
            $num = [];
            foreach ($contractDetail as $val)
            {
                /*if($type == 1)
                {
                    if(!in_array($val['prod_id'],$notId)){
                        $num[] = $val['total_num'] - $val['send_num'];
                    }
                }else{
                    $num[] = $val['total_num'] - $val['send_num'];
                }*/
                $num[] = $val['total_num'] - $val['send_num'];
            }
            $sum = array_sum($num);
            $contract = Contract::getOne(['id'=>$contract_id],1);
            if ($sum == 0 && $flagExchange == 0) {
                $contract->status = 4;
            }
            $contract->save(false);
        }
    }
    
}

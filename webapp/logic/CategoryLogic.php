<?php
namespace webapp\logic;

use Yii;
use common\models\ServiceClass;
use webapp\models\ManufactorClass;

class CategoryLogic extends BaseLogic
{
    /**
     * 获取类目列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 13:33
     * @return array
     */
    public static function getIndex()
    {
    
        $dcId     = self::getDirectCompanyId();
        $topId    = self::getTopId();
        
        $where = [
            ['=','a.direct_company_id',$dcId],
            ['=','a.class_pid',0],
            ['=','b.department_id',$topId]
            
        ];
        
        //获取列表数据

        if ($dcId == $topId) {
            $flag = 1;
        } else {
            $flag = 2;
        }
        
        //获取列表数据
        $res = ManufactorClass::getPageList($where);
        if (isset($flag)) {
            $res['flag'] = $flag;
        }
        
        if (empty($res['list'])) {
            return $res;
        }
        
        //获取类目Id
        $classIds = array_column($res['list'],'class_id');

        //查询是否存在下级
        $exist = self::existNext($classIds,$dcId);
        foreach ($res['list'] as $key => $val) {
            
            if ($val['status'] == 1) {
                $res['list'][$key]['status_desc'] = '已启用';
            }else{
                $res['list'][$key]['status_desc'] = '已禁用';
            }
            
            if (isset($exist[$val['class_id']])) {
                $res['list'][$key]['exist'] = $exist[$val['class_id']];
            }
        
        }
    
        return $res;
    }
    
    /**
     * 添加数据列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 16:07
     * @return mixed
     */
    public static function beforeAdd()
    {
        $topId = self::getTopId();
        $dcId  = self::getDirectCompanyId();
        //获取已添加的类目
        $manufactorClass = ManufactorClass::getPageList([['a.direct_company_id' => $dcId,'b.department_id'=>$topId, 'b.parent_id' => 0,'b.status'=>1]]);
        
        //获取id
        $classIds = [];
        if (!empty($manufactorClass['list'])) {
            $classIds = array_column($manufactorClass['list'],'class_id');
        }
        
        //查询是否存在下级
        $exist = self::existNext($classIds,self::getDirectCompanyId(),1);

        $data = [];
        if (isset($manufactorClass['list']) && !empty($manufactorClass['list']))
        {
            foreach ($manufactorClass['list'] as $key => $val) {
                $data[$key]['id']         = $val['class_id'];
                $data[$key]['class_name'] = $val['title'];
                
                if (isset($exist[$val['class_id']])) {
                    $data[$key]['exist'] = $exist[$val['class_id']];
                }
            }
        }
        return $data;
    }
    
    /**
     * 添加数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/5
     * Time: 17:56
     * @return array
     * @throws \Exception
     */
    public static function add()
    {
        $departmentId = self::getDepartmentId();
        $topId        = self::getTopId();
        $dcId         = self::getDirectCompanyId();
        
        //接收参数
        $classId   = intval(Yii::$app->request->post('class_pid',0));
        $className = trim(Yii::$app->request->post('class_name',''));
        $sort      = intval(Yii::$app->request->post('sort',0));

        if ($classId == 0) {
            return self::error('保存失败',20001);
        }
    
        if ($className == '') {
            return self::error('保存失败',20002);
        }
    
        if ($classId == -1) {
            $classId = 0;
        }
    
        $exist = ServiceClass::getList(['title'=>$className,'parent_id'=>$classId,'department_id'=>$topId]);
        if (!empty($exist)) {
            return self::error('类目已存在，请勿重复添加',20003);
        }
        
        $time   = time();
        $userId = Yii::$app->user->id;
    
        if ($classId == -1) {
            $classId = 0;
        }
        //获取所有父级id
        $class = ServiceClass::findOne(['id'=>$classId]);
        
        if (!empty($class) && $classId != 0) {
            $parentIds = $class->parent_ids != '' ? $class->parent_ids.','.$classId : $classId;
        }else{
            $parentIds = $classId > 0 ? $classId : 0;
        }
        
        $data['class']['title']           = $className;
        $data['class']['parent_id']       = $classId;
        $data['class']['parent_ids']      = $parentIds;
        $data['class']['department_id']   = $dcId;
        $data['class']['sort']            = $sort;
        $data['class']['status']          = 1;
        $data['class']['del_status']      = 1;
        $data['class']['create_id']       = $userId;
        $data['class']['create_time']     = $time;
        $data['class']['update_id']       = $userId;
        $data['class']['update_time']     = $time;
    
        $data['mClass']['department_id']     = $departmentId;
        $data['mClass']['direct_company_id'] = $dcId;
        $data['mClass']['class_id']          = '';
        $data['mClass']['class_pid']         = $classId;
        $data['mClass']['status']            = 1;
        $data['mClass']['del_status']        = 1;
        $data['mClass']['create_time']       = $time;
        $data['mClass']['update_time']       = $time;
        
        $res = ServiceClass::add($data);
        
        if ($res) {
            return self::success('','保存成功');
        }
        return self::error('保存失败',20002);

    }
    
    /**
     * 修改数据列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/11
     * Time: 17:49
     * @return array
     */
    public static function beforeEdit()
    {
        $classId  = intval(Yii::$app->request->get('id',0));
        $classPid = intval(Yii::$app->request->get('pid',-1));
        $type     = intval(Yii::$app->request->get('type',1));
        
        if ($classId == 0) {
            return self::error('修改失败',20001);
        }
        if ($type == 1 && $classPid == -1) {
            return self::error('修改失败',20002);
        }
        
        $class = ServiceClass::getList(['id'=>[$classId,$classPid]]);
        
        $res = [];
        foreach ($class as $key => $val) {
            if ($classId == $val['id'])
            {
                $res['data']['id']         = $val['id'];
                $res['data']['class_name'] = $val['title'];
                $res['data']['sort']       = $val['sort'];
            }
            
            if($classPid == $val['id'])
            {
                $res['data']['pid']      = $val['id'];
                $res['data']['pid_name'] = $val['title'];
            }
            
            if ($classPid == 0) {
                $res['data']['pid'] = -1;
            }
            
        }
        
        $res['list'] = [];
        if (self::beforeAdd()){
            $res['list'] = self::beforeAdd();
            
            foreach ($res['list'] as $key => $val) {
                if (isset($res['data']['pid']) && $res['data']['pid'] == $val['id']) {
                    $res['list'][$key]['exist'] = 2;
                }

                if ($classPid == 0 && $val['id'] == $classId) {
                    unset($res['list'][$key]);
                }
            }
        }
        
        return $res;
    }
    
    /**
     * 修改
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/11
     * Time: 17:49
     * @return array
     * @throws \Exception
     */
    public static function edit()
    {
        $dcId         = self::getDirectCompanyId();
        $departmentId = self::getDepartmentId();
    
        //接收参数
        $classId   = intval(Yii::$app->request->post('class_id',0));
        $classOid  = intval(Yii::$app->request->post('class_oid',-1));
        $classPid  = intval(Yii::$app->request->post('class_pid',0));
        $className = trim(Yii::$app->request->post('class_name',''));
        $sort      = intval(Yii::$app->request->post('sort',0));
    
        if ($classId == 0) {
            return self::error('保存失败',20001);
        }
        if ($classOid == 0) {
            return self::error('保存失败',20002);
        }
    
        if ($className == '') {
            return self::error('保存失败',20003);
        }
    
        $where = ['title'=>$className,'parent_id'=>$classPid,'department_id'=>$departmentId];
        
        if ($classPid == 0) {
            $where = ['title'=>$className,'parent_id'=>$classOid,'department_id'=>$departmentId];
        }
        
        if ($classPid == $classOid) {
            $where = ['title'=>$className,'parent_id'=>-1,'department_id'=>$departmentId];
        }

        $exist = ServiceClass::getList($where);
        
        if (!empty($exist)) {
            return self::error('类目已存在，请勿重复添加',20004);
        }
    
        $time   = time();
        $userId = Yii::$app->user->id;
        
        if ($classPid == 0) {
            $classPid = $classOid;
        }
        
        if ($classPid == -1) {
            $classPid = 0;
        }
        
        //获取所有父级id
        $class = ServiceClass::findOne(['id'=>$classPid]);

        if (!empty($class)) {
            $parentIds = $class->parent_ids != '' ? $class->parent_ids.','.$classPid : $classPid;
        }else{
            $parentIds = $classPid > 0 ? $classPid : 0;
        }
        
        //获取所有需要更改的类目id
        $changeClass = self::getChangeClass($classId,$parentIds);
        
        if (!empty($changeClass))
        {
            foreach ($changeClass as $key => $val) {
                $data['changePids'][$key]['parent_ids']  = $val;
                $data['changePids'][$key]['update_id']   = $userId;
                $data['changePids'][$key]['update_time'] = $time;
            }

        }
        
        $data['class']['id']          = $classId;
        $data['class']['title']       = $className;
        $data['class']['parent_id']   = $classPid;
        $data['class']['parent_ids']  = $parentIds;
        $data['class']['sort']        = $sort;
        $data['class']['update_id']   = $userId;
        $data['class']['update_time'] = $time;
    
        $data['mClass']['direct_company_id'] = $dcId;
        $data['mClass']['class_id']          = $classId;
        $data['mClass']['class_pid']         = $classPid;
        $data['mClass']['update_time']       = $time;
        
        $res = ServiceClass::edit($data);
    
        if ($res) {
            return self::success('','保存成功');
        }
        return self::error('保存失败',20005);
    }
    
    /**
     * 修改类目状态（禁用、启用）
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/11
     * Time: 13:20
     * @return array
     * @throws \Exception
     */
    public static function changeStatus()
    {
        $id     = intval(Yii::$app->request->post('id',0));
        $status = intval(Yii::$app->request->post('status',0));
        
        if ($id == 0) {
            return self::error('操作失败',20001);
        }
        
        if ($status == 0) {
            return self::error('操作失败',20002);
        }
        $data['id']     = $id;
        $data['status'] = $status;
        $data['user']   = Yii::$app->user->id;
        $data['time']   = time();
        $data['top_id'] = self::getTopId();
        $res = ServiceClass::changeStatus($data);
        
        if ($res) {
            return self::success('','操作成功');
        }
        return self::error('操作失败',20003);
    }
    
    /**
     * 查询是否还有下级
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/8
     * Time: 14:37
     * @param $classId
     * @param $departId
     * @return array
     */
    public static function existNext($classId,$dcId,$status = [1,2])
    {
        $nextClassList = ManufactorClass::getPageList([['a.class_pid'=>$classId,'a.direct_company_id'=>$dcId,'b.status'=>$status]]);
        
        $data = [];
        
        if (empty($nextClassList['list']))
        {
            foreach ($classId as $val) {
                $data[$val] = 2;
            }
            return $data;
        }

        $nextClassId = array_column($nextClassList['list'],'parent_id','parent_id');
        
        foreach ($classId as $val) {
            if (isset($nextClassId[$val])) {
                $data[$val] = 1;
            }else{
                $data[$val] = 2;
            }
        }
        
        return $data;
    }
    
    /**
     * 获取需要更改父级id的数据
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/11
     * Time: 17:49
     * @param $classId
     * @param $parentIds
     * @return array
     */
    public static function getChangeClass($classId,$parentIds)
    {
        $data = [];
        //获取子级id
        $childClass = ServiceClass::getChilds($classId);
        
        //根据子级id查询数据
        if (empty($childClass)) {
            return $data;
        }
        $res = ServiceClass::getList(['id'=>$childClass]);
        
        //获取父级id
        $parentClass   = ServiceClass::findOne(['id'=>$classId]);
        $parentClassId = explode(',',$parentClass->parent_ids);
        
        //提取所有子级父级id组合
        $parentList = array_column($res,'parent_ids','id');
        
        foreach ($parentList as $key => $val) {
            $id = explode(',',$val);
            $pid = array_diff($id,$parentClassId);
            if ($pid) {
                $data[$key] = $parentIds.','.implode(',',$pid);
            }else{
                $data[$key] = $parentIds;
            }
            
        }
        
        return $data;
    }
}
<?php
namespace webapp\logic;
use common\models\Work;
use webapp\models\Department;
use webapp\models\SaleOrderView;
use webapp\models\Technician;
use webapp\models\WorkCostForm;
use webapp\models\WorkOrderTrade;
use Yii;
use webapp\models\CheckList;
use webapp\models\CheckListCost;
use webapp\models\CheckListDetail;
use webapp\models\WorkForm;
use webapp\models\BaseModel;
use common\helpers\Helper;
use webapp\models\TechnicianCheckList;
use webapp\models\WorkCostDevide;
use common\helpers\ExcelHelper;
use common\components\Upload;
use webapp\models\AdminUser;


class CostLogic extends BaseLogic
{
    
    //财务中心首页  结算管理
    public static function getIndex()
    {
        $data = [];
        //商家结算单数据
        $checkList = self::getCheckList();
        $data['checkList'] = $checkList;
        //商家结算订单数据
        $checkOrderList = self::getCheckOrderList();
        $data['checkOrderList'] = $checkOrderList;

        
        //技师结算单数据
        $techCheckList = self::getTechCheckList();
        $data['techCheckList'] = $techCheckList;
        
        //技师工单结算数据
        $techCheckOrderList = self::getTechCheckOrderList();
        $data['techCheckOrderList'] = $techCheckOrderList;



        return $data;
    }
    
    /**
     * 查询商家结算单
     * @author liuxingqi <lxq@c-ntek.com>
     * @return int
     */
    public static function getCheckList()
    {
        $unCheck  = CheckList::getAll(2);
        $unCost   = CheckList::getAll(3);
        $haveCost = CheckList::getAll(4);
        return ['unCheck'=>$unCheck['count'],'unCost'=>$unCost['count'],'haveCost'=>$haveCost['count'],'haveCostMoney'=>$haveCost['sum']];
    }
    
    /**
     * 获取商家订单数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @return int
     */
    public static function getCheckOrderList()
    {
        $unCost   = WorkCostForm::getOrderTotalSum(1);
        $haveCost = WorkCostForm::getOrderSum(2);

        //查出待结算金额

        return ['unCost'=>$unCost['count'],'haveCost'=>$haveCost['count'],'unCostMoney'=>$unCost['sum']];
    }
    
    /**
     * 技师结算单数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @return int
     */
    public static function getTechCheckList()
    {

        $unCheck = TechnicianCheckList::getAll(1);
        $unCost = TechnicianCheckList::getAll(2);
        $haveCost = TechnicianCheckList::getAll(3);
        return ['unCheck'=>$unCheck['count'],'unCost'=>$unCost['count'],'haveCost'=>$haveCost['count'],'haveCostMoney'=>$haveCost['sum']];
    }
    
    public static function getTechCheckOrderList()
    {

        $unCost = TechnicianCheckList::getTotalCostAmount();
        $haveCost = WorkCostForm::getTechnicianSum(2);
        return ['unCost'=>$unCost['count'],'haveCost'=>$haveCost['count'],'unCostMoney'=>$unCost['sum']];

    }
    
    /**
     * 通过接口获取数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $status
     * @return type
     */
    public static function getPostList($url,$status = 0)
    {
        $postData = [
            'src_id'              => self::getManufactorId(),
            'src_type'            => BaseModel::SRC_FWS,
            'settlement_status'   => $status,   //1 待结算  2已结算
        ];
        $jsonStr  = Helper::curlPostJson($url, $postData);
        $jsonData = json_decode($jsonStr,true);
        return $jsonData;
    }

    
    /**
     * 获取列表数据
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @return type
     */
    public static function getList()
    {
        $query['company']     = trim(Yii::$app->request->get('company'));
        $query['status']      = intval(Yii::$app->request->get('status'));

        $query['currentPage']  = Yii::$app->request->get('page',1);
        $query['pageSize']     = Yii::$app->request->get('pageSize',10);
        $query['src_type']     = BaseModel::SRC_FWS;
        $query['src_id']       = self::getManufactorId();
        $action    = Yii::$app->controller->action->id;
        if($action == 'wait-check'){
            $query['status'] = [1,2];
        }elseif($action == 'wait-cost'){
            $query['status'] = 3;
        }else{
            $query['status'] = 4;
        }


        $list = CheckList::getList($query);
        foreach($list['list'] as &$v){
            if($v['cost_oprate_id']){
                $admin = AdminUser::getOne(['id'=>$v['cost_oprate_id']]);
                if($admin){
                    if(isset($admin->username)){
                        $v['cost_oprate_id'] = $admin->username;
                    }
                }else{
                    $v['cost_oprate_id'] = '';
                }
            }
            if($v['check_id']){
                $admin = AdminUser::getOne(['id'=>$v['check_id']]);
                if($admin){
                    if(isset($admin->username)){
                        $v['check_name'] = $admin->username;
                    }
                }else{
                    $v['check_name'] = '';
                }
            }
        }

        return $list;
    }
    
    /**
     * 获取添加编辑列表数据
     * @author Eva <chengjuanjuan@c-ntek.com>
     * @return type
     */
    public static function addList()
    {
        $data = [];
        $costItemSetList = CostItemSet::getList();
        $data['list'] = [];
        if ($costItemSetList) {
            $data['list'] = $costItemSetList;
        }
        $costItemIds = [];
        $costItemList = CostItem::getList(['status'=>1]);
        foreach ($costItemList as $val) {
            $costItemIds[] = $val->cs_id;
        }
        $data['ids'] = [];
        if ($costItemIds) {
            $data['ids'] = $costItemIds;
        }
        return $data;
    }

    //获取详情
    public  static function getDetail(){
        $id = intval(Yii::$app->request->get('id'));
        $info = CheckList::getOne(['id'=>$id]);
        if(!$info){
          $info = '';
          $costItem = '';
        }else{
            if(isset($info['report_id'])){
                $user = AdminUser::getOne(['id'=>$info['report_id']]);
                $info['report_name'] = $user->username;
            }
            if(isset($info['check_id'])){
                $check = AdminUser::getOne(['id'=>$info['check_id']]);
                $info['check_name'] = $check->username;
            }
            if(isset($info['cost_oprate_id'])){
                $check = AdminUser::getOne(['id'=>$info['cost_oprate_id']]);
                $info['cost_name'] = $check->username;
            }
            $costItem = CheckListCost::getList(['check_list_id'=>$id]);
        }

        return ['info'=>$info,'costItem'=>$costItem];
    }
    
    //查询按钮带出数据
    public static function getData(){
        $startTime    = Yii::$app->request->post('startTime');
        $endTime      = Yii::$app->request->post('endTime');
        $sjId         = Yii::$app->request->post('sj_id');
        $company      =  Yii::$app->request->post('company');


        $url = Yii::$app->params['order.uservices.cn'].'/v1/order/settlement-order';

 /*       $postData = [
            'begin_time' => '2018-01-01 00:00:00',
            'end_time'   => '2018-03-01 00:00:00',
            'sj_id'     => 19,
            'src_id'=>      64,
            'src_type'   => 14
        ];*/
          $postData = [
          'begin_time' => $startTime,
          'end_time'   => $endTime,
          'sj_id'     => $sjId,
          'src_id'=>      BaseLogic::getManufactorId(),
          'src_type'   => 14 //这里取商家标识？
         ];

        $jsonStr  = Helper::curlPostJson($url, $postData);
        $jsonData = json_decode($jsonStr,true);
        $data = $jsonData['data'];

        if(isset($data['order_number']))
        {
            $orderSum = ['number'=>$data['order_number'],'amount'=>$data['order_total_amount'],'amount_expect'=>$data['total_amount']];
            $returnData = ['orderSum'=>$orderSum,'CostItem'=>$data['cost_info']];
            return json_encode($returnData);

        }



        return json_encode($jsonData);
    }
    
    public static function updateStatus()
    {
        $checklistId = intval(Yii::$app->request->post('checklist_ids'));
        $res = CheckList::updateStatus($checklistId);
        if ($res) {
            return self::success($res,'成功');
        }
        return self::error('失败');
    }
    
    public static function getExport()
    {
        $param['check_list_id'] = intval(Yii::$app->request->get('id'));
        $result = CheckListDetail::getList($param);
        if($result){
            $title = $sheet_title = '订单对账明细';
            foreach($result['list'] as &$v){
                $v['finish_time'] = date('Y-m-d H:i:s');
                $v['amount'] = sprintf('%0.2f',$v['amount']);
            }
            $dataList = $result;
            self::setExport($dataList,$title,$sheet_title);
        }
    }

    public static function setExport($dataList,$title,$sheet_title)
    {
        $status = [
            'order_no'    =>'订单编号',
            'accountName' =>'客户姓名',
            'accountMobile'=>'客户电话',
            'address'     =>'服务地址',
            'type_name'   =>'服务类型',
            'class_name'  =>'产品名称',
            'brand_name'  =>'品牌',
            'sn'          => '序列号',
            'amount'      =>'金额',
            'finish_time' =>'完成服务时间'
        ];

        if(!empty($status)){
            foreach ($status as $key=>$val){
                $newArr[$key]['key']    = $key;
                $newArr[$key]['value']  = $val;
                $newArr[$key]['width']  = 30;
            }
            $cells = array_values($newArr);
        }

        ExcelHelper::CreateExcel($cells,$dataList['list'],$title,$sheet_title);
    }
    
    //提交结算单
    public static function checkListSubmit(){
        $startTime    = Yii::$app->request->post('cost_start_time');
        $endTime      = Yii::$app->request->post('cost_end_time');
        $sjId         = Yii::$app->request->post('sj_id');
        $company      =  Yii::$app->request->post('company');

        $url = Yii::$app->params['order.uservices.cn'].'/v1/order/settlement-order';

/*        $postData = [
            'begin_time' => '2018-01-01 00:00:00',
            'end_time'   => '2018-03-01 00:00:00',
            'sj_id'     => 19,
            'src_id'=>      64,
            'src_type'   => 14
        ];*/
       $postData = [
            'begin_time' => $startTime,
            'end_time'   => $endTime,
            'sj_id'     => $sjId,
            'src_id'=>      BaseLogic::getManufactorId(),
            'src_type'   => 14 //这里取商家标识？
        ];

        $jsonStr  = Helper::curlPostJson($url, $postData);
        $jsonData = json_decode($jsonStr,true);
        $data = $jsonData['data'];

        if(isset($data['order_info']))
        {
           $checkList = ['order_number'=>$data['order_number'],'amount'=>$data['order_total_amount'],'amount_expect'=>$data['total_amount']];
           $orderList = $data['order_info'];
           $result =  CheckList::add(['check_list'=>$checkList,'order_list'=>$orderList,'data'=>['begin_time'=>$startTime,'end_time'=>$endTime,'sj_id'=>$sjId,'company'=>$company]]);

            //修改订单的结算状态
            $orderIds = isset($data['order_info'])?array_keys($data['order_info']):[];

            if($orderIds)
           {
               return CheckList::updateOrderSettlementStatus($orderIds);
           }

        }

        return [];
    }
    
    public static function searchWaitCostOrder()
    {
        
        $orderNo      = trim(Yii::$app->request->get('order_no', ''));
        $typeId       = intval(Yii::$app->request->get('type_id', -1));
        $startTime    = trim(Yii::$app->request->get('start_time', ''));
        $endTime      = trim(Yii::$app->request->get('end_time', ''));
        
        $where = [];
        
        if ($orderNo != '') {
            $where[] = ['=', 'a.order_no', $orderNo];
        }
        
        if ($typeId != -1) {
            
            $saleOrder   = SaleOrderView::getList(['type_id'=>$typeId]);
            $saleOrderId = array_column($saleOrder,'id');
            
            if (!empty($saleOrderId)) {
                $where[] = ['in', 'b.sale_order_id', $saleOrderId];
            }else{
                $where[] = ['=', 'b.sale_order_id', -1];
            }
        }
        
        if ($startTime != '' && $endTime != '')
        {
            $where[] = ['>', 'c.create_time', strtotime($startTime)];
            $where[] = ['<', 'c.create_time', strtotime($endTime)];
        }
        
        return $where;
    }
    
    /**
     * 待结算订单列表
     * User: xingq
     * Email: liuxingqi@services.cn
     * Date: 2018/6/22
     * Time: 14:10
     * @return array
     */
    public static function getWaitCostOrder()
    {
    
        $listNull = [
            'page'       => 0,
            'totalCount' => 0,
            'totalPage'  => 0,
            'list'       => []
        ];
        
        $page          = intval(Yii::$app->request->get('page',1));
        $pageSize      = intval(Yii::$app->request->get('pageSize',10));
        
        //获取本公司所有部门id
        $department = Department::getDepartmentChildren(self::getDirectCompanyId(),0,1);
        $departIds  = array_unique(array_column($department,'id'));
        
        //获取本公司所有技师id
        $tech    = Technician::find()->where(['store_id'=>$departIds,'audit_status'=>5])->asArray()->all();

        if (empty($tech))
        {
            return $listNull;
        }
        
        $techIds = array_column($tech,'id');
    
        //获取当前公司所有线上已支付工单
        $work = WorkOrderTrade::getCostWork($techIds);
        
        if (empty($work))
        {
            return $listNull;
        }
        
        $orderNo = array_unique(array_column($work,'order_no','order_no'));
        
        $where = self::searchWaitCostOrder();
        
        $where[] = ['in','a.order_no',$orderNo];
        
        //获取订单信息
        $res = WorkOrderTrade::waitCostOrderList($page, $pageSize, $where);
    
        //获取服务类型
        $type = self::getType();
        if (!empty($type)) {
            $res['type'] = $type;
        }else{
            $res['type'] = [];
        }
        
        if (empty($res['list'])) {
            return $res;
        }
        
        //提取售后产品id
        $saleOrderId = array_unique(array_column($res['list'],'sale_order_id','id'));
        $saleOrder   = SaleOrderView::getList(['id'=>$saleOrderId]);
        
        //组合产品名称
        if (!empty($saleOrder))
        {
            foreach ($res['list'] as $key => $val) {
                $res['list'][$key]['type_name']  = $type[$val['work_type']];
                foreach ($saleOrder as $k => $v) {
                    if ($val['sale_order_id'] == $v['id'])
                    {
                        $res['list'][$key]['prod_name']  = $v['prod_name'];
                        $res['list'][$key]['brand_name'] = $v['brand_name'];
                        $res['list'][$key]['class_name'] = $v['class_name'];
                    }
                }
            }
        }
        
        $workPriceList = [];
        //收费金额分组
        if (!empty($orderNo))
        {
            foreach ($work as $key => $val) {
                if (isset($orderNo[$val['order_no']]))
                {
                    $workPriceList[$orderNo[$val['order_no']]][$val['work_no']] = $val['pay_amount'];
                }
            }
        }
        
        //统计订单收费金额
        $orderPriceList = [];
        if (!empty($workPriceList))
        {
            foreach ($workPriceList as $key => $val) {
                if (is_array($val)) {
                    $orderPriceList[$key] = number_format(array_sum($val),2);
                }else{
                    $orderPriceList[$key] = number_format($val,2);
                }
            }
        }
        
        //合并列表
        if (!empty($orderPriceList))
        {
            foreach ($res['list'] as $key => $val) {
                if (isset($orderPriceList[$val['order_no']])) {
                    $res['list'][$key]['pay_amount'] = $orderPriceList[$val['order_no']];
                }
                $res['list'][$key]['create_time'] = date('Y-m-d H:i',$val['create_time']);
            }
            
        }
        
        //统计订单收费金额
        $res['sumPrice'] = number_format(array_sum(array_column($res['list'],'pay_amount')),2);
        
        return $res;
        
    }
    
}
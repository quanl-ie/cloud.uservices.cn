<?php
namespace webapp\logic;


use webapp\models\ServiceBrand;

/***
 * @author chengjuanjuan<chengjuanjuan@c-ntek.com>
 * @created_at 2017-12-14 14:01
 * desc the logic of customer ma
 */


class CustomerManageLogic extends BaseLogic
{


        /**
         * 函数用途描述：获取单个品牌名称
         * @date: 2017年12月14日 下午14:48:01
         * @author: chengjuanjuan<chengjuanjuan@c-ntek.com>
         * @param: $id
         * @return:$data
         */
    public static function getOneBrand(){
        $data = ServiceBrand::getTitle((1));
        return self::success(['data'=>$data]);
    }

}
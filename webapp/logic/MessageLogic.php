<?php
namespace webapp\logic;

use Yii;
use common\helpers\Helper;
use webapp\models\BaseModel;
class MessageLogic extends BaseLogic
{

    public static function getList($user_id='',$src_id='',$msg_type='',$src_type='',$page='',$page_size='',$read_status)
    {
        //获取消息列表
        $url = Yii::$app->params['msg.uservices.cn']."/v1/msg/get-user-msg-list";
        $postData = [
                'user_id'        => $user_id,
                'src_type'       => $src_type,        //来源：14 厂商 13 服务商 12 门店 11 技师 20 c端用户
                'src_id'         => $src_id,          //来源id
                'msg_type'       =>$msg_type,
                'page'           =>$page,
                'page_size'      =>$page_size,
                'read_status'    =>$read_status
        ];
        //print_r($postData);exit;
        $data = ['page'=>0,'total_count'=>0,'total_page'=>0,'list'=>[]];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 ){
            $data = $jsonArr['data']['data'];

            if($data){
                foreach($data['list'] as &$v){
                    $content = json_decode($v['content'],true);
                    $orderNo = isset($content['order_no'])?$content['order_no']:'';
                    $status  = isset($content['status'])?$content['status']:'';
                    //1待接单 2待指派 3待服务 4服务中 5 已完成 6已取消
                    if($status>0){
                        switch($status)
                        {
                            case 1:
                                $v['link_info'] = '/order/wait';
                                break;
                            case 2:
                                $v['link_info'] = '/order/to-assign';
                                break;
                            case 3:
                                $v['link_info'] = '/order/wait-service';
                                break;
                            case 4:
                                $v['link_info'] = '/order/in-service';
                                break;
                            case 5:
                                $v['link_info'] = '/order/finish';
                                break;
                            case 6:
                                $v['link_info'] = '/order/canc';
                                break;
                            default:
                                $v['link_info'] = '/order/index';
                                break;
                        }
                    }


                }
            }
        }
        return $data;
    }
    /**
    * 函数用途描述:获取弹窗最新消息
    * @date: 2017年12月20日 下午6:11:23
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: $GLOBALS
    * @return:
    */
    public static function getNewMsg($user_id='',$src_id='',$msg_type='',$src_type='',$page='',$page_size='') {
        return [1];
        $url = Yii::$app->params['msg.uservices.cn']."/v1/msg/get-user-notice-list";
        $postData = [
                'user_id'        => $user_id,
                'src_type'       => $src_type,        //来源：14 厂商 13 服务商 12 门店 11 技师 20 c端用户
                'src_id'         => $src_id,          //来源id
                'msg_type'       =>$msg_type,
                'page'           =>$page,
                'page_size'      =>$page_size
        ];
        $data = ['page'=>0,'total_count'=>0,'total_page'=>0,'list'=>[]];
        $jsonStr = Helper::curlPostJson($url, $postData);
        $jsonArr = json_decode($jsonStr,true);
        if( isset($jsonArr['success']) && $jsonArr['success']== 1 ){
            $data = $jsonArr['data']['data'];
        }
        return $data;
    }
    /**
    * 函数用途描述：设置消息已读状态
    * @date: 2017年12月20日 下午6:11:46
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function setMsgReadStatus($msg_id){
        $url = Yii::$app->params['msg.uservices.cn']."/v1/msg/set-msg-read-status";
        $postData = [
                'message_id'        => $msg_id,
                'status'            =>1,
                'member_id'         =>Yii::$app->user->getIdentity()->id
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        return $jsonStr;
    }
    /**
    * 函数用途描述:消息设置为忽略状态
    * @date: 2017年12月21日 上午10:28:42
    * @author: sxz<shixiaozheng@c-ntek.com>
    * @param: variable
    * @return:
    */
    public static function setMsgNoticeStatus($msg_id){
        $url = Yii::$app->params['msg.uservices.cn']."/v1/msg/set-msg-notice-status";
        $postData = [
                'message_id'        => $msg_id,
                'status'            =>2
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);
        return $jsonStr;
    }


    /**
    * @desc 消息入库
     * @date 2018-01-03
     * $param:见文档：http://192.168.0.206:8090/pages/viewpage.action?pageId=1605893
     * @author chengjuanjuan
     * $data 商家申请合作服务商时候和需要的数据
     **/

    public static function sendMsg($title,$content,$msgClass=1,$msgType=2,$publishType=1,$subscribeId=2,$linkInfo='',$noticeType=2,$noticeUrl='',$data=[]){
        $url = Yii::$app->params['msg.uservices.cn']."/v1/msg/send-msg";
        $mainId = self::getMainId();
        $src_type = BaseModel::SRC_FWS;
        $src_id = $mainId['data'];
        if($data){
            $src_type = $data['src_type'];
            $src_id = $data['src_id'];
        }
        $postData = [
            'src_type'        =>$src_type,
            'src_id'          =>$src_id,
            'msg_class'       =>$msgClass,
            'msg_type'        =>$msgType,
            'title'           =>$title,
            'content'         =>$content,
            'publish_id'      =>self::getManufactorId(),
            'subscribe_id'    =>$subscribeId,
            'publish_type'    =>$publishType,
            'link_info'       =>$linkInfo,
            'notice_type'     =>$noticeType,
            'notice_url'      =>$noticeUrl,
        ];
        $jsonStr = Helper::curlPostJson($url, $postData);        
        return $jsonStr;
    }
}
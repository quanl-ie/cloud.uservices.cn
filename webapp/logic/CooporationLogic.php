<?php
namespace webapp\logic;

use webapp\models\ServiceClassQualification;
use webapp\models\ServiceTypeQualification;
use webapp\models\Manufactor;
use Yii;
use webapp\models\Cooporation;
use common\models\Region;
use webapp\models\BaseModel;
use webapp\models\AdminUser;
use webapp\models\BrandQualification;
use common\models\ServiceClass;
use common\models\ServiceBrand;
use webapp\models\CostLevel;
use webapp\models\CostLevelDetail;
use webapp\models\CostLevelItem;
use webapp\logic\ServiceAreaLogic;
use common\models\CostRule;
use common\models\CostRuleDetail;
use common\models\CostRuleRelation;
use common\models\CostRuleItem;
use common\models\ServiceType;

/***
 * @author chengjuanjuan<chengjuanjuan@c-ntek.com>
 * @created_at 2017-12-14 14:01
 * desc the logic of sale orders
 */


class CooporationLogic extends BaseLogic
{

    /**
     * 函数用途描述：
     * @date: 2017年12月14日 下午14:48:01
     * @author: chengjuanjuan<chengjuanjuan@c-ntek.com>
     * @param: $id
     * @return:$data
     */
    public static function index()
    {
        $page     = intval(Yii::$app->request->get('page',1));
        $pageSize = intval(Yii::$app->request->get('pageSize',10));
        //搜索数据
        $name = trim(Yii::$app->request->get('name',''));
        $brand = trim(Yii::$app->request->get('brand',''));
        $status = trim(Yii::$app->request->get('status',''));
        $firstClassId     = intval(Yii::$app->request->get('first_class_id',0));
        $secondClassId    = intval(Yii::$app->request->get('second_class_id',0));
        $srcId = [];
        if ($name != '' || $brand != '' || $secondClassId != 0 || $status != '') {
            $srcId = [-1];
        }
        if ($name != '') {
            $manufactor = Manufactor::getList(['like','company',$name]);
            if ($manufactor) {
                foreach ($manufactor as $key => $val) {
                    if ($val['src_type'] == BaseModel::SRC_SJ) {
                        $srcId[$key] = $val['id'];
                    }
                }
            }
        }
        if ($brand != '') {
            $where = ['like','brand_name',$brand];
            $andWhere = ['src_type'=> BaseModel::SRC_SJ,'src_id'=>$srcId];
            $brandQualification = BrandQualification::find()->where($where)->andFilterWhere($andWhere)->all();
            if ($brandQualification) {
                unset($srcId);
                foreach ($brandQualification as $key => $val) {
                    $srcId[$key] = $val['src_id'];
                }
            }
        }
        if ($secondClassId != 0) {
            $where = ['like','class_id',$secondClassId];
            $andWhere = ['src_type'=> BaseModel::SRC_SJ,'src_id'=>$srcId];
            $serviceClassQualification = ServiceClassQualification::find()->where($where)->andFilterWhere($andWhere)->all();
            if ($serviceClassQualification) {
                unset($srcId);
                foreach ($serviceClassQualification as $key => $val) {
                    $srcId[$key] = $val['src_id'];
                }
            }
        }
        //初始数据
        $firstClass = BaseLogic::getClassList(0);
        $secondClass = [];
        if($firstClassId > 0){
            $secondClass = BaseLogic::getClassList($firstClassId);
            $secondClass = $secondClass['data'];
        }
        $data['firstClass'] = $firstClass['data'];
        $data['secondClass'] = $secondClass;
        $cooporation = Cooporation::getList($srcId,$page,$pageSize);

        $data['page']       = $cooporation['page'];
        $data['totalCount'] = $cooporation['totalCount'];
        $data['totalPage']  = $cooporation['totalPage'];
        $data['list']       = [];
        if (!empty($cooporation['list'])) {
            //状态
            $statusData = [];
            $cooporationData = [];
            foreach ($cooporation['list'] as $key => $val) {
                if ($status!= '' && $status != $val['status']) {
                    unset($cooporation['list'][$key]);
                }else{
                    $statusData[$val['src_id']] = $val['status'];
                    $cooporationData[$val['src_id']] = $val['id'];
                }
            }
            //提取商家id

            $manufactorId = [];
            foreach ($cooporation['list'] as $val) {
                $manufactorId[$val['src_id']] = $val['src_id'];
            }            
            //获取商家数据
            $manufactor = self::getManufactorInfo($manufactorId);

            foreach ($manufactor as $key => $val) {
                if (isset($statusData[$val['src_id']])) {
                    $manufactor[$key]['id']         = $cooporationData[$val['src_id']];
                    $manufactor[$key]['status']     = $statusData[$val['src_id']];
                    $manufactor[$key]['statusDesc'] = Cooporation::STRTUS_ARRAY[$statusData[$val['src_id']]];
                }
            }
            $data['totalCount'] = count($manufactor);
            $data['list']       = $manufactor;
        }
        $data['statusList'] =  Cooporation::STRTUS_ARRAY;
        return $data;
    }
    
    /**
     * 获取合作商家信息
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $manufactorId
     * @return type
     */
    public static function getManufactorInfo($manufactorId)
    {
        $manufactor = Manufactor::getList(['id'=>$manufactorId]);
        $data = [];
        foreach ($manufactor as $key => $val) {
            if (isset($manufactorId[$val['id']])) {
                $data[$val['id']]['src_id']  = $val['id'];
                $data[$val['id']]['company'] = $val['company'];
                $data[$val['id']]['contact'] = $val['contact'];
                $data[$val['id']]['mobile']  = $val['mobile'];
                unset($manufactorId[$val['id']]);
            }
        }
        return $data;
    }
    /**
     * 查看商家经营品牌
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $id
     * @return type
     */
    public static function getSelfBrand($id)
    {
        $data = BrandQualification::getSelfList($id, BaseModel::SRC_SJ);
        $brand = [];
        if ($data) {
            foreach ($data as $key => $val) {
                $brand[] = explode('/', $val['brand_name']);
            }
            $brand = array_unique(array_column($brand,0));
            $brand = implode('、', $brand);
        }
        return $brand;
    }
    /**
     * 查看商家经营类目
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $id
     * @return type
     */
    public static function getSelfClass($id)
    {
        $data = ServiceClassQualification::getList($id, BaseModel::SRC_SJ);
        if ($data) {
            foreach ($data as &$val){
                $val['s_class'] = '';
                $val['title'] = ServiceClass::getClassName($val['class_pid']);
                if($val['class_id']){
                    $val['secondClass'] = ServiceClass::getClassName($val['class_id'],'、');
                }
            }
        }
        return $data;
    }

    /**
     * 查看收费标准
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $id
     * @return type
     */
    public static function getCostLevel()
    {
        $id = intval(Yii::$app->request->get('id',0));
        $costLevel = CostLevel::getManufactorList(['src_id'=>$id,'status'=>1]);
        if (!$costLevel) {
            return self::error('该商家还未配置收费标准',20002);
        }
        //服务区域
        $area = array_column($costLevel,'service_area');
        $data = self::getServiceArea($area);
        
        //品牌
        $brandData = self::getBrand($id,BaseModel::SRC_SJ);
        
        if ($brandData) {
            foreach ($brandData as $key => $val) {
                if (strstr($val,'/',true)){
                    $data['brand'][$key] = strstr($val,'/',true);
                }else{
                    $data['brand'][$key] = $val;
                }
            }
        }
        //产品类目
//        $serviceClass = self::getClass($id, BaseModel::SRC_SJ);
        $serviceClass = ServiceClassQualification::find()->where(['src_id'=>$id,'src_type'=>BaseModel::SRC_SJ])->asArray()->all();
        $classId = array_column($serviceClass,'class_id');
        $classId = explode(',',implode(',',$classId));
        $className = ServiceClass::getName($classId);

        if ($className) {
            foreach ($className as $key => $val) {
                $data['class'][$key] = $val;
            }
        }
        
        //服务类型
        $typeQualification = self::getType($id,BaseModel::SRC_SJ);
        if ($typeQualification) {
            $data['type'] = $typeQualification;
        }
        //商家名称
        $manufactor = Manufactor::getOne(['id'=>$id]);
        if ($manufactor) {
            $data['manufactor'] = $manufactor;
        }
        return self::success($data);
    }
    /**
     * 查看结算规则
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $id
     * @return type
     */
    public static function getCostRule()
    {
        $id = intval(Yii::$app->request->get('id',0));
        $where = ['src_id'=>$id,'src_type'=>BaseModel::SRC_SJ,'service_provider_id'=>self::getManufactorId()];
        $costRuleRelation = CostRuleRelation::getOne($where);
        if (!$costRuleRelation) {
            return self::error('该商家还未配置结算规则',20001);
        }
        
        //商家名称
        $manufactor = Manufactor::getOne(['id'=>$id]);
        if ($manufactor) {
            $data['manufactor']['rule_id'] = $costRuleRelation->cost_rule_id;
            $data['manufactor']['id'] = $id;
            $data['manufactor']['name'] = $manufactor->company;
            
        }
        return self::success($data);
    }
    
    /**
     * 根据省份id获取市级和县级数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $provinceIds
     * @return type
     */
    public static function getCity($id,$provinceId,$type)
    {
        //服务区域
        $serviceArea = [];
        if ($type == 1) {
            $costLevel = CostLevel::getManufactorList(['src_id'=>$id]);
            $serviceArea = array_column($costLevel,'service_area');
        }else{
            $costLevel = CostRule::getManufactorList(['src_id'=>$id]);
            $serviceArea = array_column($costLevel,'service_area');
        }

        $city = [];
        $area = self::getServiceArea($serviceArea);
        if($area)
        {
            unset($area['province']);
            unset($area['district']);
            foreach ($area['city'] as $key => $val) {
                if ($provinceId == $val['parent_id']) {
                    $city[$key] = $val;
                }
            }
        }
        return $city;
    }
    
   /**
     * 根据市级数据获取县级数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $citysIds
     * @return type
     */
    public static function getDistrict($id,$cityId,$type)
    {
        //服务区域
        $serviceArea = [];
        if ($type == 1) {
            $costLevel = CostLevel::getManufactorList(['src_id'=>$id]);
            $serviceArea = array_column($costLevel,'service_area');
        }else{
            $costLevel = CostRule::getManufactorList(['src_id'=>$id]);
            $serviceArea = array_column($costLevel,'service_area');
        }
        
        $district = [];
        $area = self::getServiceArea($serviceArea);
        if($area)
        {
            unset($area['province']);
            unset($area['city']);
            foreach ($area['district'] as $key => $val) {
                if ($cityId == $val['parent_id']) {
                    $district[$key] = $val;
                }
            }
        }
        return $district;
    }
    
    /**
     * ajax品牌、产品类目、服务类型联动获取收费项目
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function linkAge()
    {
        //接收传值
        $srcId = intval(trim(Yii::$app->request->post('levelId','')));
        $brandId = intval(trim(Yii::$app->request->post('brandId','')));
        $classId = intval(trim(Yii::$app->request->post('classId','')));
        $typeId  = intval(trim(Yii::$app->request->post('typeId','')));
        $cityId  = intval(trim(Yii::$app->request->post('cityId','')));
        
        //判断传值
        if ($srcId == '') {
            return self::error('无商家标识',20001);
        }
        if ($brandId == '') {
            return self::error('请选择品牌',20002);
        }
        if ($classId == '') {
            return self::error('请选择产品类目',20003);
        }
        if ($typeId == '') {
            return self::error('请选择服务类型',20004);
        }
//        if ($cityId == '') {
//            return self::error('请选择服务类型',20005);
//        }
        //组合数据
//        $serviceArea = ServiceAreaLogic::getSaveData($cityId);
//        if (!$serviceArea) {
//            return self::error('服务城市数据错误',20005);
//        }
        //判断是否有通用
        if ($brandId == -1) {
            $brandId = 0;
        }
        if ($classId == -1) {
            $classId = 0;
        }
        if ($typeId == -1) {
            $typeId = 0;
        }

        $where = ['src_type'=> BaseModel::SRC_SJ,'src_id'=>$srcId,'status'=>1];
        $andWhere = [];
        $costLevel = Costlevel::getCostIdList($where,$andWhere);
        $costLevelId = array_column($costLevel,'id');
        //查询数据
        $costLevelDetail = CostLevelDetail::getOne(['cost_level_id'=>$costLevelId,'brand_id'=>$brandId,'class_id'=>$classId,'type_id'=>$typeId,'status'=>1]);
        if (isset($costLevelDetail->id)) {
            $data = CostLevelItem::getList(['level_detail_id'=>$costLevelDetail->id,'status'=>1]);
            return self::success($data,'成功');
        }
        return self::error('失败');
    }
    
/**
     * ajax品牌、产品类目、服务类型联动获取结算规则
     * @author liuxingqi <lxq@c-ntek.com>
     * @return type
     */
    public static function ruleLinkAge()
    {
//        //接收传值
//        $srcId = intval(trim(Yii::$app->request->post('ruleId','')));
//        $brandId = intval(trim(Yii::$app->request->post('brandId','')));
//        $classId = intval(trim(Yii::$app->request->post('classId','')));
//        $typeId  = intval(trim(Yii::$app->request->post('typeId','')));
//
//        //判断传值
//        if ($srcId == '') {
//            return self::error('无商家标识',20001);
//        }
//        if ($brandId == '') {
//            return self::error('请选择品牌',20002);
//        }
//        if ($classId == '') {
//            return self::error('请选择产品类目',20003);
//        }
//        if ($typeId == '') {
//            return self::error('请选择服务类型',20004);
//        }
//
//        $costRuleRelation = CostRuleRelation::getOne(['src_id'=>$srcId,'status'=>1]);
//        if (!$costRuleRelation) {
//            return self::error('商家标识错误',20001);
//        }
//        //查询数据
//        $costRuleDetail = CostRuleDetail::getOne(['cost_rule_id'=>$costRuleRelation->cost_rule_id,'brand_id'=>$brandId,'class_id'=>$classId,'type_id'=>$typeId]);
//        if (!$costRuleDetail) {
//            return self::error('失败');
//        }
//        $data = CostRuleItem::getList(['rule_detail_id'=>$costRuleDetail->id]);
//        foreach ($data as $key => $val) {
//            if (in_array($val['status'], [2,3])) {
//                unset($data[$key]);
//            }
//        }
    
        $id = intval(Yii::$app->request->get('id',0));
        $ruleId = intval(Yii::$app->request->get('ruleId',0));
        if ($id == 0) {
            return self::error('获取结算规则失败',20001);
        }
        if ($id == -1) {
            return self::error('当前商家还没有匹配结算规则',20002);
        }
    
        $costRuleRelation = CostRuleRelation::getOne(['src_id'=>$id,'status'=>1]);
        if (!$costRuleRelation) {
            return self::error('该商家还未配置结算规则',20001);
        }
        $costRule = CostRule::getOne(['id'=>$ruleId]);
        if (!$costRule) {
            return self::error('该商家还未配置结算规则',20002);
        }
        //服务区域
        $data['costRule']['real_cost_date'] = "每个售后订单服务完成".$costRule['real_cost_date']."天后进行结算";
        
        //地区
        $area = explode(',',$costRule['service_area']);
        
        $res = [];
        foreach ($area as $key => $val) {
            $res[$key] = explode('_', $val);
        }
        foreach ($res as $key => $val) {
            $provinceIds[] = $val[0];
            $cityIds[]     = $val[1];
            $districtIds[] = $val[2];
        }
        $serviceArea['provinceId'] = array_unique($provinceIds);
        $serviceArea['cityId']     = array_unique($cityIds);
        $serviceArea['districtId'] = array_unique($districtIds);
        

        $data['province']   = Region::getCityName(['region_id'=>$serviceArea['provinceId']]);
        $data['city']       = Region::getCityName(['region_id'=>$serviceArea['cityId']]);
        $data['district']   = Region::getCityName(['region_id'=>$serviceArea['districtId']]);
    
        //结算规则详情
        $costRuleDetailData = CostRuleDetail::getList(['cost_rule_id'=>$ruleId,'status'=>1]);
        
        //获取品牌名称
        $brandData = array_column($costRuleDetailData,'brand_id');
        $brandName = ServiceBrand::getName($brandData);
    
        //获取类目名称
        $classData = array_column($costRuleDetailData,'class_id');
        $className = ServiceClass::getName($classData);
    
    
        //获取类型名称
        $typeData = array_column($costRuleDetailData,'type_id');
        $typeName = ServiceType::getName($typeData);
    
        //组合名称
        $costRuleDetail = [];
        foreach ($costRuleDetailData as $val) {
            $costRuleDetail[$val['id']]['id']   = $val['id'];
            if (isset($brandName[$val['brand_id']])) {
                $costRuleDetail[$val['id']]['brand_id']   = $val['brand_id'];
                $costRuleDetail[$val['id']]['brand_name'] = $brandName[$val['brand_id']];
            }
            if (isset($className[$val['class_id']])) {
                $costRuleDetail[$val['id']]['class_id']   = $val['class_id'];
                $costRuleDetail[$val['id']]['class_name'] = $className[$val['class_id']];
            }
            if (isset($typeName[$val['type_id']])) {
                $costRuleDetail[$val['id']]['type_id']   = $val['type_id'];
                $costRuleDetail[$val['id']]['type_name'] = $typeName[$val['type_id']];
            }
        }
    
    
        //结算项目明细
        $costRuleItemData = CostRuleItem::getList(['cost_rule_id'=>$ruleId,'status'=>1]);
        
        $costRuleItem = [];
        foreach ($costRuleItemData as $key => $val) {
            //1.定额结算
            if ($val['cost_type'] == 1) {
                $costRuleItem[$val['rule_detail_id']][] = $val['cost_item_name'].'：定额结算('.$val['fixed_amount'].'元)';
            }
            //2.百分比结算
            if ($val['cost_type'] == 2) {
                $costRuleItem[$val['rule_detail_id']][] = $val['cost_item_name'].'：百分比结算('.$val['pecent'].'%)';
            }
        }
    
        //组合数组
        $cost = [];
        foreach ($costRuleDetail as $key => $val) {
            if ($val['brand_id'] == $val['brand_id']) {
    
                if (strstr($val['brand_name'],'/',true)) {
                    $cost[$val['brand_id']]['brand_name'] = strstr($val['brand_name'],'/',true);
                }else{
                    $cost[$val['brand_id']]['brand_name'] = $val['brand_name'];
                }
            }
        }
    
        foreach ($costRuleDetail as $key => $val) {
            foreach ($cost as $k => $v) {
                if ($val['brand_id'] == $k) {
                    $cost[$val['brand_id']]['classList'][$val['class_id']]['class_name'] = $val['class_name'];
                }
            }
        }
    
        foreach ($costRuleDetail as $key => $val) {
            foreach ($cost as $k => $v) {
                foreach ($v['classList'] as $i => $item) {
                    if ($val['brand_id'] == $k && $val['class_id'] == $i) {
                        $cost[$val['brand_id']]['classList'][$val['class_id']]['typeList'][$val['type_id']]['type_name'] = $val['type_name'];
                        foreach ($costRuleItem as $c => $co) {
                            if ($val['id'] == $c) {
                                $cost[$val['brand_id']]['classList'][$val['class_id']]['typeList'][$val['type_id']]['itemList'] = $co;
                            }
                        }
                    
                    }
                }
            }
        }
    
        $data['rule'] = $cost;
        return self::success($data);
        
    }

    /**
     * 拆分地区数据
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $area
     * @return type
     */
    public static function getServiceArea($serviceArea)
    {
        if (is_array($serviceArea)) {
            $area = implode(',', $serviceArea);
        }else{
            $area = $serviceArea;
        }
        $area = explode(',', $area);
        $area = array_filter(array_unique($area));
        $data = [];
        if (empty($area)) {
            return $data;
        }
        $res = [];
        foreach ($area as $key => $val) {
            $res[$key] = explode('_', $val);
        }
        foreach ($res as $key => $val) {
            $provinceIds[] = $val[0];
            $cityIds[]     = $val[1];
            $districtIds[] = $val[2];
        }
        $data['province'] = self::getCityName(array_unique($provinceIds));
        $data['city']     = self::getCityName(array_unique($cityIds));
        $data['district'] = self::getCityName(array_unique($districtIds));
        return $data;
    }
    /**
     * 根据id返回城市名称
     * @author liuxingqi <lxq@c-ntek.com>
     * @param type $cityIds
     * @return type
     */
    public static function getCityName($cityIds)
    {
        $data = Region::getCityName(['region_id'=>$cityIds]);
        return $data;
    }

    
    /**
     * 函数用途描述：
     * @date: 2017年12月14日 下午14:48:01
     * @author: chengjuanjuan<chengjuanjuan@c-ntek.com>
     * @param: $id
     * @return:$data
     */
    public static function Md(){
        /*   $query['account_name'] = Yii::$app->request->post('account_name','chengjuanjuan');
           $query['brand_name']   = Yii::$app->request->post('brand_name','海尔');
           $query['start_time']   = Yii::$app->request->post('start_time',time()-86400);
           $query['end_time']     = Yii::$app->request->post('end_time',time());*/
        $query = '';
        $query['src_type']     = BaseModel::SRC_MD;
        $query['service_provider_id']       = BaseLogic::getManufactorId();
        $query['currentPage']  = Yii::$app->request->get('page','1');
        $query['pageSize']     = Yii::$app->request->get('pageSize',10);
        $data = Cooporation::getList($query);
        foreach($data as $v){}
        return $data;
    }


    /**
    * @desc 服务商添加
     * @author:chengjuanjuan <chengjuanjuan@c-ntek.com>
     * @createAt:20171215
     **/
    public static function add()
    {//处理数据
            $main         = self::getMainId();
            $providerId   = intval(Yii::$app->request->post('service_provider_id'));
            $providerName = trim(Yii::$app->request->post('service_provider_name'));
            $isCount      = Cooporation::findOne(['src_id'=>BaseModel::SRC_JS,'src_id'=>$main['data'],'service_provider_id'=>$providerId]);
            if(!empty($isCount))
                return self::error('您申请的服务商已存在');
            if(!$providerId){
                return self::error('您申请的服务商不存在');
            }
            $info                          = AdminUser::findOne(['id'=>$providerId]);
            $minfo                         = Manufactor::findOne(['user_id'=>$providerId]);
            $data['service_provider_id']   = $providerId;   //暂时取
            $data['service_provider_name'] = $providerName;
            $data['info']                  = Yii::$app->request->post('info');
            $data['src_type']              = BaseModel::SRC_FWS; //类型来源
            $data['src_id']                = $main['data'];//对应的id
            $data['status']                = 1;//待确认
            $data['create_time']           = $data['update_time'] = time();
            $data['conact_name']           = $minfo['contact'];
            $data['conact_mobile']         = $minfo['mobile'];
            $data['service_area']          = isset( $minfo)?$minfo['province_id'].','.$minfo['city_id'].','.$minfo['district_id']:'';
            $data['service_class']         =  isset( $minfo)?$minfo['class_id']:'';
            $res = Cooporation::add($data);
            if ($res) {
                return self::success('','新增成功');
            }
            return self::error('数据新增失败');;
    }



    public static function changeStatus()
    {
        $id     = intval(Yii::$app->request->post('id'));

        $status = intval(Yii::$app->request->post('status'));  //2 同意 3 拒绝

        $info = Cooporation::getOne(['id'=>$id]);

        $provider_id = $info->service_provider_id;

        //获取商家的公司名字
        $companyInfo = Manufactor::getOne(['id'=>$provider_id]);
        $companyName = $companyInfo['company'];

        //需要通知的商家id
        $merchantId = $info->src_id;
        $subscribeId = Manufactor::getOne(['id'=>$merchantId]);
        if($info['status'] == $status){
            return self::error('该状态已存在，无需修改');
        }else{
            $res = Cooporation::changeData($id,$status);
            if($res){
                if($status == 2){
                    $statusInfo = '已同意';
                }
                if($status == 3){
                    $statusInfo = '被拒绝';
                }
                //发送消息通知商家
                $title = '您向'.$companyName.'提交的合作申请'.$statusInfo;
                $content = '您向'.$companyName.'提交的合作申请'.$statusInfo;
                $msgClass = 2;
                $msgType = 2;
                $publishType = 1;
                $subscribeId = $subscribeId['user_id'];
                $linkInfo = '/cooporation/index';
                $noticeType = 1;
                $noticeUrl='';
                $data = ['src_id'=>$merchantId,'src_type'=>14];//接收者是id和type
                $send = MessageLogic::sendMsg($title,$content,$msgClass,$msgType,$publishType,$subscribeId,$linkInfo,$noticeType,$noticeUrl,$data);
            }
            return self::success($res);
        }

    }


    public static function getAccount()
    {
        $accountName = trim(Yii::$app->request->get('q'));
        $main = self::getMainId();
        $res  =  AdminUser::getListInfo($accountName);
        exit(json_encode($res));
    }

    //获取服务商对应的合作商家信息
    public static function getCooporationMerchant()
    {
        $data = Cooporation::getCooporationList();
        return $data;

    }

}
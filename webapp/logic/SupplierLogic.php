<?php
namespace webapp\logic;
use Yii;
use webapp\models\BaseModel;
use common\helpers\Helper;
use webapp\models\Region;
use common\models\Supplier;
use common\models\DictEnum;
/**
 * 供货商管理
 * @author sxz
 * @date 2018-03-21
 */
class SupplierLogic extends BaseLogic
{
    /**
     * 数据列表
     * @author sxz
     * @return string
     */
    public static function getList($flag='')
    {
        $params = [];
        $params['no'] = $no = trim(Yii::$app->request->get('no','')); //供货商编号
        $params['name'] = $name = trim(Yii::$app->request->get('name','')); //供货商名称
        $params['level'] = $level = Yii::$app->request->get('level',''); //等级
        $params['status'] = $status = Yii::$app->request->get('status',''); //状态
        $params['page'] = $page = intval(Yii::$app->request->get('page',1));
        $params['pageSize'] = $pageSize = intval(Yii::$app->request->get('pageSize',20));
        $maps = ['direct_company_id'=>BaseLogic::getDirectCompanyId()];
        if($flag){
            $maps = ['direct_company_id'=>BaseLogic::getDirectCompanyId(),'status'=>0];
        }
        $where = [];
        //搜索条件
        if (yii::$app->request->isGet) {
            if ($no != '') {
                $where[] = ['like','no',$no];
            }
            if ($name != '') {
                $maps['name'] = $name;
            }
            if($level != ''){
                $maps['level'] = $level;
            }
            if($status != ''){
                $maps['status'] = $status;
            }
        }
        $data = Supplier::getIndexList($maps,$where,$page,$pageSize);
        $province_name = $city_name = '';
        if($data['list']) foreach ($data['list'] as $key=>$val ){
            $province_name = Region::getCityName(isset($val['province_id'])?$val['province_id']:'');
            $city_name = Region::getCityName(isset($val['city_id'])?$val['city_id']:'');
            $data['list'][$key]['full_address'] = $province_name.$city_name.$val['address'];
        }
        $data['params'] = $params;
        //获取等级数据
        $data['levelSelect'] = self::getLevelSelect();
        return $data;
    }
    /**
     * 数据添加和编辑
     */
    public static function add()
    {
        $data = Yii::$app->request->post();
        //print_r($data);die;
        $postData['no'] = trim(Yii::$app->request->post('no',''));
        $postData['name'] = htmlspecialchars(trim(Yii::$app->request->post('name','')));
        $postData['contact_name'] = htmlspecialchars(trim(Yii::$app->request->post('contact_name','')));
        $postData['contact_mobile'] = trim(Yii::$app->request->post('contact_mobile',''));
        $postData['province_id'] = intval(Yii::$app->request->post('province',0));
        $postData['city_id'] = intval(Yii::$app->request->post('city',0));
        $postData['district_id'] = intval(Yii::$app->request->post('district',0));
        $postData['address'] = htmlspecialchars(trim(Yii::$app->request->post('address','')));
        $postData['status'] = intval(Yii::$app->request->post('status'));
        $postData['level'] = intval(Yii::$app->request->post('level'));
        $postData['level_name'] = trim(Yii::$app->request->post('level_name'));
        $postData['direct_company_id'] = BaseLogic::getDirectCompanyId();
        $postData['create_time'] = date('Y-m-d H:i:s',time());
        $postData['create_user_id'] = BaseLogic::getLoginUserId();

        if($postData['no']){
            if(!preg_match('/^[A-Za-z0-9]*$/',$postData['no'])){
                return self::error('供货商编号只能是字母或数字！');
            }
            //验证数据
            $res = Supplier::getOne(['no'=>$postData['no'],'direct_company_id'=>BaseLogic::getDirectCompanyId()]);
            if($res){
                return self::error('供货商编号已存在！');
            }
        }
        if(!$postData['name']){
            return self::error('供货商名称不能为空！');
        }
        if(!$postData['contact_name']){
            return self::error('联系人姓名不能为空！');
        }
        if(!$postData['contact_mobile']){
            return self::error('联系人电话不能为空！');
        }
        if(!$postData['province_id']){
            return self::error('省份不能为空！');
        }
        if(!$postData['city_id']){
            return self::error('城市不能为空！');
        }
        if(!$postData['district_id']){
            return self::error('区域不能为空！');
        }
        if(!$postData['address']){
            return self::error('详细地址不能为空！');
        }
        if(!$postData['level_name']){
            $postData['level_name'] = self::getLevelName($postData['level']);
        }
        //print_r($postData);die;
        $res = Supplier::add($postData);
        if ($res) {
            return self::success($res,'成功');
        }
        return self::error('失败');
    }
    /**
     * 函数用途描述:库房编辑
     * @date: 2018年3月20日 下午6:27:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function edit(){
        $postData['id'] = intval(Yii::$app->request->post('id',0));
        $postData['no'] = trim(Yii::$app->request->post('no',''));
        $postData['name'] = htmlspecialchars(trim(Yii::$app->request->post('name','')));
        $postData['contact_name'] = htmlspecialchars(trim(Yii::$app->request->post('contact_name','')));
        $postData['contact_mobile'] = trim(Yii::$app->request->post('contact_mobile',''));
        $postData['province_id'] = intval(Yii::$app->request->post('province',0));
        $postData['city_id'] = intval(Yii::$app->request->post('city',0));
        $postData['district_id'] = intval(Yii::$app->request->post('district',0));
        $postData['address'] = htmlspecialchars(trim(Yii::$app->request->post('address','')));
        $postData['status'] = intval(Yii::$app->request->post('status'));
        $postData['level'] = intval(Yii::$app->request->post('level'));
        $postData['level_name'] = trim(Yii::$app->request->post('level_name'));
        $postData['direct_company_id'] = BaseLogic::getDirectCompanyId();
        $postData['create_time'] = date('Y-m-d H:i:s',time());
        $postData['create_user_id'] = BaseLogic::getLoginUserId();
        if($postData['no']){
            if(!preg_match('/^[A-Za-z0-9]*$/',$postData['no'])){
                return self::error('库房编号只能是字母或数字！');
            }
            //验证数据
            $res = Supplier::getOne(['no'=>$postData['no'],'direct_company_id'=>BaseLogic::getDirectCompanyId()]);
            if($res && $postData['id'] != $res['id']){
                return self::error('供货商编号已存在！');
            }
        }
        $res = Supplier::edit($postData);
        if ($res) {
            return BaseLogic::success('','修改成功');
        }
        return BaseLogic::error('修改失败');
    }
    /**
     * 函数用途描述:获取供货商信息
     * @date: 2018年3月21日 下午16:07:23
     * @author: sxz<shixiaozheng@c-ntek.com>
     * @param: variable
     * @return:
     */
    public static function getSupplierInfo($id)
    {
        $data = [];
        if($id){
            $data = Supplier::getOne(['id'=>$id,'direct_company_id'=>BaseLogic::getDirectCompanyId()]);
            if($data){
                $data['province_name'] = Region::getCityName($data['province_id']);
                $data['city_name'] = Region::getCityName($data['city_id']);
                $data['district_name'] = Region::getCityName($data['district_id']);
            }
            return $data;
        }
        return $data;
    }
    //获取等级
    public static function getLevelSelect(){
        //$data = DictEnum::getList(['dict_key'=>'enum_supplier_level','org_id'=>BaseLogic::getManufactorId()]);
        $data = DictEnum::getList(['dict_key'=>'enum_supplier_level']);
        return $data;
    }
    //获取等级名称
    public static function getLevelName($level_id=''){
        $list = self::getLevelSelect();
        if($list){
            $list = array_column($list,'dict_enum_value','dict_enum_id');
        }
        $data = isset($list[$level_id])?$list[$level_id]:'';
        return $data;
    }




}
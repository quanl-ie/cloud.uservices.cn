<?php
namespace webapp\logic;

use common\models\User;
use webapp\models\Role;
class RoleLogic extends BaseLogic
{

    public static function getList()
    {
        $data = array(
            'name'        => '角色名称',
            'description' => '备注',
        );

        return self::success($data);
    }

    /**
     * 获取创建人信息
     * @param $workNo
     * @param $reason
     * @return array
     */
    public static function getUserInfo($id)
    {
        $data = '';
        if($id > 0 ){
            $data = isset(User::findOne(['id'=>$id])->username)?User::findOne(['id'=>$id])->username:'';
        }
        return $data;
    }

    public static function getPageList($page = 1,$pageSize = 10,$where = [])
    {
        $db = Role::find();
        //判断where
        if (!empty($where)) {
            foreach ($where as $val) {
                $db->andFilterWhere($val);
            }
        }

        //总数
        $totalNum = $db->count();
        //当有结果时进行组合数据
        if ($totalNum>0) {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
            $db->offset(($page-1)*$pageSize);
            $db->orderBy(['created_at'=> SORT_DESC]);
            $db->limit($pageSize);
            /*$db->asArray();
            $list = $db->all();*/
            $list = $db->createCommand()->getRawSql();
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => $totalPage,
                'list'       => $list
            ];
        }
        else
        {
            return [
                'page'       => $page,
                'totalCount' => $totalNum,
                'totalPage'  => 0,
                'list'       => []
            ];
        }
    }
    
}
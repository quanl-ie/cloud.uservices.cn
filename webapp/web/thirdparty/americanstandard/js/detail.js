var me={identify:'ITEM'}
var url = "";
var token ="";
var detailInfo ={};//详情信息
var updateInfo={};//更新信息
var mapP ={
    lng:undefined,
    lat:undefined
};
var orderNo = "";
$(function () {
    $(".detail_div .orderDetail>li").click(function () {
        $(this).addClass("isActive").siblings().removeClass("isActive");
        $(".orderDetailContent .main").eq($(this).index()).show().siblings().hide();
        initDetailFunc($(this).index()+1)
        if($(this).index()==1){
            $(".orderDetailContent").css("background","#f7f8f9");
        }else{
            $(".orderDetailContent").css("background","#ffffff");

        }
    })
    window.$$map("address-map").onLocation(function (lng,lat) {
        mapP.lng = lng;
        mapP.lat = lat;
    })

    $("#time").datetimepicker({
        timeFormat: "HH:mm:ss",
        dateFormat: "yy/mm/dd"
    })
    $(document.body).bind("copy",function(e){
        var cpTxt;
        if(e.target.innerHTML =="基本信息"){
            cpTxt = document.getElementById('basicInfo').innerText;
            if(cpTxt.indexOf("派工")>0){
                cpTxt = cpTxt.replace("派工"," ")
            }
        }else if(e.target.innerHTML =="产品信息"){
            cpTxt = document.getElementById('productInfo').innerText;

        }else if(e.target.innerHTML =="服务信息"){
            cpTxt = document.getElementById('serviceInfo').innerText;

        }
        cpTxt = cpTxt.replace("自动回单"," ")
        var clipboardData = window.clipboardData; //for IE
        if (!clipboardData) { // for chrome
            clipboardData = e.originalEvent.clipboardData;
        }
        clipboardData.setData('Text', cpTxt);
        alert("已复制到剪贴板");
        return false;//否则设不生效
    })

    //基本信息自动回单
    $("#basicInfo").on("click","#comeOrder",function(e){
        document.designMode = "on";
        document.execCommand('copy', true);
        document.designMode = "off";
    });

    //产品信息自动回单
    $("#productInfo").on("click",".prodComeOrder",function (e) {
        document.designMode = "on";
        document.execCommand('copy', true);
        document.designMode = "off";
    })

    //服务信息自动回单
    $("#serviceInfo").on("click",".servComeOrder",function (e) {
        document.designMode = "on";
        document.execCommand('copy', true);
        document.designMode = "off";
    })
})
//初始化
function initDetailFunc(type){
    var that = this;
    $.ajax({
        url:"http://www.bfznkj.com:10028/v1/order/view",
        type:"post",
        headers:{
            // "Authorization":"Bearer ef61c968d4e3c29f7f2e94d40d347dff154144f2"
            "Authorization":"Bearer "+token
        },
        data:{
            external_order_no:url,
            type:type,
        },
        success(res){
            if(res.code==200){
                if(type==1){
                    $("#basicInfo>div").empty();
                    var data =res.data;
                    if(data){
                        orderNo = data.orderNo;
                        var imgArr = data.faultImgs;
                        var htmItem = "";
                        if(imgArr.length>0){
                            imgArr.forEach(function(item,index){
                                htmItem+="<img alt='' style='vertical-align: top;cursor: pointer;' title='点我查看大图！' class='preview-img' src='"+item+"'/>"
                            })
                        }
                        $("#basicInfo>div").append("<p><span>主单状态</span><span>"+data.status+"<a href='javascript:void (0)' id='assign'>派工</a><a href='javascript:void (0)' id='comeOrder' style='display: none;'>自动回单</a></span></p><p><span>主单编号</span><span>"+data.orderNo+"</span></p><p><span>客户电话</span><span>"+data.customerPhone+"</span></p><p><span>服务地址</span><span>"+data.address+"</span></p><p><span>服务类型</span><span>"+data.workType+"</span></p><p><span>服务时间</span><span>"+data.planTime+"</span></p><p><span>故障图片</span><span>" +htmItem+"</span></p><p><span>下单时间</span><span>"+data.createTime+"</span></p><p><span>备注</span><span>"+data.remark+"</span></p>")
                        $("img.preview-img").click(function(){
                            $(".img").remove();
                            let img = new Image();
                            img.src = $(this)[0].currentSrc;
                            window.open(img.src)
                        })
                        if(data.status=="待指派"){
                            $("#assign").show();
                            $("#comeOrder").css("margin-left","0")
                        }else{
                            $("#assign").hide();
                            $("#comeOrder").css("margin-left","260px")

                        }
                    }
                }else if(type==2){
                    $("#productInfo").empty();
                    var data = res.data;
                    if(data.length>0){
                        data.forEach(function(item){
                            var prodItem = "";
                            var imgUrl = "";
                            var imgArr = item.scopeImgs;
                            if(imgArr.length>0){
                                imgArr.forEach(function(itemSon){
                                    imgUrl+="<img alt='' style='vertical-align: top;' class='preview-img' src='"+itemSon+"'/>"
                                })
                            }
                            var errorTxt = "";//故障描述
                            var errorArr = item.ext;
                            if(errorArr.length>0){
                                errorArr.forEach(function(errorItem,index){
                                    errorItem.forEach(function(errorDetailItem,index){
                                           errorTxt += "<p><span>"+errorDetailItem.label+"</span><span>"+errorDetailItem.value+"</span></p>"
                                    })
                                })
                            }
                            prodItem+="<p><span>产品名称</span><span style='width: calc(100% - 135px);'>"+item.name+"</span><a style='float: right;' href='javascript:void (0)' style='display: none;' class='prodComeOrder'>自动回单</a></p><p><span>产品编号</span><span>"+item.prodNo+"</span></p><p><span>品牌</span><span>"+item.brandName+"</span></p><p><span>产品型号</span><span>"+item.prodModel+"</span></p><p><span>序列号</span><span>"+item.prodSN+"</span></p><p><span>购买日期</span><span>"+item.prodBuyDate+"</span></p><p><span>出厂日期</span><span>"+item.prodProduceDate+"</span></p><p><span>质保状态</span><span>"+item.scopeStatus+"</span></p><p><span>质保期</span><span>"+item.scope+"</span></p><p><span>质保凭证</span><span>"+imgUrl+"</span></p><p><span>备注</span><span>"+item.remark+"</span></p><p><span>故障描述</span><span style='width: calc(100% - 123px);'>"+item.subject+"</span></p>"+errorTxt;
                            var divItem = $("<div>"+prodItem+"</div>");
                            $("#productInfo").append(divItem)
                        })

                    }
                }else if(type==3){
                    $("#serviceInfo>div.serviceTop").empty();
                    $("#payee").empty();
                    var data = res.data;
                    var serviceItem = "";//服务信息
                    var payService = "";//收费项目
                    if(data.length>0){
                        data.forEach(function(item){
                            var serviceProd = "<span>";//服务产品
                            var serviceWorkImg = "";//服务工单
                            var scenePhoto = "";//现场拍照

                            //服务产品开始
                            var arr = item.productArr;
                            if(arr.length>1){
                                serviceProd+=arr[0].title+'x'+arr[0].num+arr[0].unit+"等多个产品</span>"
                            }else{
                                serviceProd+=arr[0].title+'x'+arr[0].num+arr[0].unit+"</span>"
                            }
                            //服务产品结束

                            //服务工单开始
                            var serviceWorkImgArr = item.serviceWorkPicArr;
                            if(serviceWorkImgArr.length>0){
                                serviceWorkImgArr.forEach(function(img){
                                    serviceWorkImg+="<img alt='' style='vertical-align: top;cursor: pointer;' title='点我查看大图！' class='preview-img' src='"+img+"'/>"
                                })
                            }
                            //服务工单结束

                            //现场拍照开始
                            var scenePhotoArr = item.scenePhotoArr;
                            if(scenePhotoArr.length>0){
                                scenePhotoArr.forEach(function(img){
                                    scenePhoto+="<img alt='' style='vertical-align: top;cursor: pointer;' title='点我查看大图！' class='preview-img' src='"+img+"'/>"
                                })
                            }
                            //现场拍照结束
                            serviceItem+= "<p><span>订单状态</span><span>"+item.status+"<a href='javascript:void (0)' style='display: none;' class='servComeOrder'>自动回单</a></span></p><p><span>子单编号</span><span>"+item.workNo+"</span></p><p><span>服务流程</span><span>"+item.workStage+"</span></p><p><span>服务产品</span>"+serviceProd+"</p><p><span>预约时间</span><span>"+item.plantime+"</span></p><p><span>服务时间</span><span>"+item.startTime+"</span></p><p><span>技师信息</span><span>"+item.technician+"</span></p><p><span>服务工单</span><span>"+serviceWorkImg+"</span></p><p><span>现场拍照</span><span>"+scenePhoto+"</span></p><p><span>服务记录</span><span>"+item.serviceRecord+"</span></p><p><span>客户签名</span><span><img class='preview-img' src='"+item.customerSignature+"' alt=''></span></p>"

                            //收费项目开始
                            var payServiceArr = item.payServiceArr;
                            var firstPayeeProject = "";
                            if(payServiceArr.length>0){
                                payServiceArr.forEach(function(payItem,index){

                                    var certificate = "";//凭证
                                    var certificateArr = payItem.certificateArr;//凭证
                                    if(certificateArr.length>0){
                                        certificateArr.forEach(function(img){
                                            certificate+="<img alt='' style='vertical-align: top;cursor: pointer;' title='点我查看大图！' class='preview-img' src='"+img+"'/>"
                                        })
                                    }
                                    if(payItem.type==1){ //其他费用
                                        if(index==0){
                                            payService+="<div class='floatRight floatRight70P'><div class='right' style='margin-top: 0;'><div>"+payItem.serviceType+"</div><div>"+payItem.payType+"</div></div><div class='right rightWidth100'><div>价格</div><div>"+payItem.price+"</div></div><div class='right rightWidth100'><div>凭证</div><div>"+certificate+"</div></div><div class='right rightWidth100' style='margin-bottom: 10px;'><div>备注</div><div>"+payItem.remark+"</div></div></div>"
                                        }else{
                                            payService+="<div class='floatRight floatRightPayee'><div class='right' style='margin-top: 15px;'><div>"+payItem.serviceType+"</div><div>"+payItem.payType+"</div></div><div class='right rightWidth100'><div>价格</div><div>"+payItem.price+"</div></div><div class='right rightWidth100'><div>凭证</div><div>"+certificate+"</div></div><div class='right rightWidth100' style='margin-bottom: 10px;'><div>备注</div><div>"+payItem.remark+"</div></div></div>"
                                        }
                                    }else{ //配件费
                                        //配件名称开始
                                        var partsArr = payItem.partsArr;
                                        var partsData = "";
                                        if(partsArr.length>0){
                                            partsArr.forEach(function(partsItem,index){
                                                if(index==0){
                                                    partsData+= "<div class='right rightWidth100 marginTop20'><div>配件名</div><div>"+partsItem.name+"</div></div><div class='right rightWidth100'><div>价格</div><div>"+partsItem.price+"</div></div><div class='right rightWidth100'><div>回收</div><div>"+partsItem.recovery+"</div></div>"
                                                }else{
                                                    partsData+= "<div class='right rightWidth100 marginTop20'><div>配件名</div><div>"+partsItem.name+"</div></div><div class='right rightWidth100'><div>价格</div><div>"+partsItem.price+"</div></div><div class='right rightWidth100 marginBottom20'><div>回收</div><div>"+partsItem.recovery+"</div></div>"
                                                }
                                            })
                                        }else{
                                            partsArr.forEach(function(partsItem,index){
                                                partsData+= "<div class='right rightWidth100 marginTop20'><div>配件名</div><div>"+partsItem.name+"</div><div class='right rightWidth100'><div>价格</div><div>"+partsItem.price+"</div></div><div class='right rightWidth100 marginBottom20'><div>回收</div><div>"+partsItem.recovery+"</div></div>"
                                            })
                                        }

                                        //配件费开始
                                        if(index==0){
                                            payService+="<div class='floatRight floatRight70P'><div class='right' style='margin-top: 0;'><div>"+payItem.serviceType+"</div><div>"+payItem.payType+"</div></div>"+partsData+"<div class='right rightWidth100'><div>实际收费</div><div>"+payItem.realPrice+"</div></div><div class='right rightWidth100'><div>凭证</div><div>"+certificate+"</div></div><div class='right rightWidth100'><div>备注</div><div>"+payItem.remark+"</div></div><div class='right rightWidth100' style='margin-bottom: 10px;'><div>总计金额</div><div>"+payItem.totalPrice+"</div></div></div>"
                                        }else{
                                            payService+="<div class='floatRight floatRightPayee'><div class='right' style='margin-top: 15px;'><div>"+payItem.serviceType+"</div><div>"+payItem.payType+"</div></div>"+partsData+"<div class='right rightWidth100'><div>实际收费</div><div>"+payItem.realPrice+"</div></div><div class='right rightWidth100'><div>凭证</div><div>"+certificate+"</div></div><div class='right rightWidth100'><div>备注</div><div>"+payItem.remark+"</div></div><div class='right rightWidth100' style='margin-bottom: 10px;'><div>总计金额</div><div>"+payItem.totalPrice+"</div></div></div>"
                                        }
                                    }

                                })
                            }else{
                                $("#payee").hide();
                            }

                        })
                    }
                    $("#serviceInfo>div.serviceTop").append(serviceItem)
                    $("#payee").append("<div class='left'>收费项目</div>"+payService)
                    $("img.preview-img").click(function(){
                        $(".img").remove();
                        let img = new Image();
                        img.src = $(this)[0].currentSrc;
                        window.open(img.src)
                    })
                }else{
                    $("#orderStateInfo").empty();
                    var data = res.data;
                    var orderStateInfo = "";
                    if(data.length>0){
                        data.forEach(function(item,index){
                            if(index==0){
                                orderStateInfo+="<div class='orderStateProgress'><div class='orderStatus'><p><i style='display: inline-block;'>"+item.date+"</i><i style='font-size: 15px;display: inline-block;'>"+item.time+"</i></p><div class='progress'><p class='circle circleActive'></p><p class='bottom' style='height: 65px;'></p></div><p><strong style='font-size:14px;font-weight: 400;color: #333;'>"+item.type+"</strong><span style='text-indent: 0;'>"+item.content+"</span></p></div></div>"
                            }else{
                                orderStateInfo+="<div class='orderStateProgress'><div class='orderStatus'><p><i style='display: inline-block;'>"+item.date+"</i><i style='font-size: 15px;display: inline-block;'>"+item.time+"</i></p><div class='progress'><p class='circle circleGary'></p><p class='bottom' style='height: 65px;'></p></div><p><strong style='font-size:14px;font-weight: 400;color: #333;'>"+item.type+"</strong><span style='text-indent: 0;'>"+item.content+"</span></p></div></div>"
                            }
                        })
                        $("#orderStateInfo").append(orderStateInfo)
                    }
                }
            }else{
                alert(res.message)
            }
        },
        fail(error){
            alert(error)
        }
    })
};

//服务类型
function getServiceTypeFunc(){
    $.ajax({
        url:"http://www.bfznkj.com:10028/v1/order/get-work-type",
        type:"post",
        headers:{
            // "Authorization":"Bearer ef61c968d4e3c29f7f2e94d40d347dff154144f2"
            "Authorization":"Bearer "+token
        },
        data:{},
        success(res){
            if(res.code==200){
                var data = res.data;
                var serviceType = "";
                $("#radioLable").empty();
                data.forEach(function(item,index){
                    serviceType+="<input type='radio' value='"+item.id+"' name='service_type' id='servertype"+item.id+"'/><label for='servertype"+item.id+"'>"+item.title+"</label>"
                })
                $("#radioLable").append(serviceType)
            }else{
                alert(res.message)
            }
        },
        fail(error){
            alert(error)
        }
    })
}


$(document).ready(function (){
    top.postMessage({ sender:me.identify,cmd:'parentUrl',data: "请给我页面路径" }, '*')
})
window.addEventListener("message", function (e) {
    //if(e && e.sender!=me.identify) procMsg(e);
    getUrlMsg(e.data);
}, false);


function getUrlMsg(msg){
    //e:{sender:'',cmd:'',data:any}
    if(msg.cmd=="export"){
        url = msg.data.url.split("&")[1].split("=")[1];
        token = msg.data.token;
        checkOrder();
        getServiceTypeFunc();
    }else if(msg.cmd=="exportData"){
        detailInfo = msg.data;
        if(detailInfo){
            if(detailInfo.plantime!=''){
                $("#time").val(detailInfo.plantime)
            }
            $(".input-pua").val(detailInfo.customer_data.province_name +","+detailInfo.customer_data.city_name+","+detailInfo.customer_data.district_name)
            $(".input-desc").val(detailInfo.customer_data.address)
        }
    }else if(msg.cmd=="updateexportData"){
        updateInfo = msg.data;
        if(updateInfo){
            $.ajax({
                url:"http://www.bfznkj.com:10028/v1/order/update",
                type:"post",
                headers:{
                    "Authorization":"Bearer "+token
                },
                data:updateInfo,
                success:function (res) {
                    if(res.code==200){
                        alert("更新信息成功！")
                        initDetailFunc(1);
                    }else{
                        alert(res.message)
                    }
                },
                fail:function (error) {
                    console.log(error)
                }
            })
        }
    }
}

$(".confirmBtn").click(function () {
    if($("input[name='service_type']:checked").val()==''||$("input[name='service_type']:checked").val()==null){
        alert("请选择服务类型！");
        return false;
    }
    // if(this.date){
    //     if(this.date.split(' ')[1].split(':')[1]!='00'&&this.date.split(' ')[1].split(':')[1]!='30'){
    //         alert("时间只能是整点或者半点！");
    //         return false;
    //     }
    // }
    detailInfo.work_type = $("input[type='radio']:checked").val();
    detailInfo.plantime = $("#time").val();
    if(mapP.lng!=undefined&&mapP.lat!=undefined){
        detailInfo.customer_data.lng = mapP.lng;
        detailInfo.customer_data.lat = mapP.lat;
    }else{
        alert("请点击定位！")
        return false;
    }
    detailInfo.customer_data.address =$(".input-pua").val().replace(/,/g,"")+$(".input-desc").val();
    delete detailInfo.customer_data.province_name;
    delete detailInfo.customer_data.city_name;
    delete detailInfo.customer_data.district_name;
    $.ajax({
        url:"http://www.bfznkj.com:10028/v1/order/add",
        data:detailInfo,
        type:"post",
        headers:{
            // "Authorization":"Bearer ef61c968d4e3c29f7f2e94d40d347dff154144f2"
            "Authorization":"Bearer "+token
        },
        success:function (res) {
            if(res.code==200){
                alert("添加成功！")
                $(".detail_div .orderDetail>li").eq(0).addClass("isActive").siblings().removeClass("isActive");
                $(".orderDetailContent .main").eq(0).show().siblings().hide();
                initDetailFunc(1);
                $("#serviceInfo").show();
                $(".popupDiv").hide();
                $("#updateBtn").show();
                console.log("lng"+detailInfo.customer_data.lng )
                console.log("lat"+detailInfo.customer_data.lat )
                console.log("work_type"+detailInfo.work_type)
                console.log("plantime"+detailInfo.plantime )
                console.log("address"+detailInfo.customer_data.address )
                top.postMessage({ sender:me.identify,cmd:'lngLatData',data: {lng:detailInfo.customer_data.lng,lat:detailInfo.customer_data.lat,work_type:detailInfo.work_type,plantime:detailInfo.plantime,address:detailInfo.customer_data.address} }, '*')
            }else{
                alert(res.message)
            }
        },
        fail:function (error) {

        }
    })

})

$("#updateBtn").click(function () {
    top.postMessage({sender:me.identify,cmd:'updateInfo',data: "更新信息指令发送"},'*')
})

$("#importOrderBtn").click(function () {
    $("#importBtn").hide();
    $("#contentPanel").show();
    $(this).parent().hide();
    $(".detail_div .orderDetail>li").eq(2).addClass("isActive").siblings().removeClass("isActive");
    $(".orderDetailContent .main").eq(2).show().siblings().hide();
    $("#serviceInfo").hide();
    $(".popupDiv").show();
    $("#updateBtn").hide();
    setTimeout(function () {
        top.postMessage({sender:me.identify,cmd:'importInfo',data: "导出指令发送"},'*')
    },2000);
})

function checkOrder(){
    $.ajax({
        url:"http://www.bfznkj.com:10028/v1/order/has-exist",
        type:"post",
        headers:{
            "Authorization":"Bearer "+token
        },
        data:{
            external_order_no:url,
        },
        success:function (res) {
            if(res.code==200){
                if(res.data.exist){
                    $("#contentPanel").show();
                    $(".logoDiv").hide();
                    initDetailFunc(1);
                }else{
                    $("#contentPanel").hide();
                    $(".logoDiv").show();
                }
            }else{
                alert(res.message);
            }
        },
        fail:function (error) {
            alert(error)
        }
    })
}

$("#basicInfo").on("click","#assign",function (e) {
    var assignUrl = "http://dev.fws.uservices.cn/order/assign?order_no="+orderNo+"&token="+token;
    window.open(assignUrl)
})
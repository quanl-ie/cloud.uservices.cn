<?php 

$action = isset($_GET['t'])?$_GET['t']:'';

if($action == 'mysql'){
	echo testMysql();
}
else if($action == 'redis'){
	echo testRedis();
}
else if($action == 'order'){
	echo testOrder();
}
else if($action == 'system') {
	echo testLocalSystem();
}

//测试本系统
function testLocalSystem()
{
	$loginUrl = $_SERVER['REQUEST_SCHEME'] . '://'.$_SERVER['SERVER_NAME'].'/site/login';
	$str = @file_get_contents($loginUrl);
	if(!$str){
		return 0;
	}
	return 1;
}


//订单系统检查
function testOrder()
{
	$paramsArr = require_once('../../common/config/params.php');

	$jsonStr = @file_get_contents($paramsArr['order.uservices.cn']);
	if($jsonStr){
		$jsonArr = json_decode($jsonStr,true);
		if(isset($jsonArr['success'])){
			return 1;
		}
	}
	return 0;
}
//mysql 测试
function testMysql()
{
	$mainArr = require_once('../../common/config/main-local.php');

	$dsn = $mainArr['components']['db']['dsn'];
	$dsnArr = explode(';',$dsn);

	//host
	$mysqlHost = explode('=',$dsnArr[0])[1];
	$database  = explode('=',$dsnArr[1])[1];
	$username  = $mainArr['components']['db']['username'];
	$password  = $mainArr['components']['db']['password'];



	$con=@mysqli_connect($mysqlHost,$username,$password,$database); 
	if($con) 
	{ 
		return 1;
	}
	return 0;
} 
//redis 测试
function testRedis()
{
	try
	{
		if(class_exists('\Redis') == false){
			return 0;
		}
		
		$mainArr = require_once('../../common/config/main-local.php');
		
		$redis = new \Redis();
		$redis->connect($mainArr['components']['redis']['hostname'], $mainArr['components']['redis']['port']);
		$redis->auth($mainArr['components']['redis']['password']);
		$redis->set('envtesting', 111);

		if($redis->get('envtesting')){
			return 1;
		}
		else {
		   return  0;
		}
	}
	catch(\Exception $e){
		return 0;
	}
}

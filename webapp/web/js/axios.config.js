// axios异步请求、响应统一处理
(function () {
    // 添加请求拦截器
    axios.interceptors.request.use( function ( config ) {
        // 在发送请求之前做些什么
        return config;
    }, function ( error ) {
        // 对请求错误做些什么
        return Promise.reject( error );
    } );

    // 添加响应拦截器
    axios.interceptors.response.use( function ( response ) {
        var data;
        // 入股axios响应成功，提取服务返回的数据
        if ( data = response.data ) {
            // 如果业务逻辑响应成功
            if ( data.success || data.data ) {
                return data.data;
            } else if ( data.message ) {
                return Promise.reject( data );
            }
        }
        return Promise.reject( response );
    }, function ( error ) {
        // 对响应错误做点什么
        return Promise.reject( error );
    } );

})();
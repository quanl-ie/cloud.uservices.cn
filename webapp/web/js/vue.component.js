;(function ( factory ) {
    if ( Vue ) factory();
})( function ( un ) {
    var O = {};
    var N = "";
    var ts = O.toString;
    var RCT = /\[object\s(\w+)]/;
    
    function ct( o ) {
        return (ts.call( o ).replace( RCT, "$1" )).toLocaleLowerCase()
    }
    
    function isD( i ) {
        return i !== null && i !== un && i !== N
    }
    
    function to( v ) {
        return !isD( v ) ? N : typeof v === "object" ? JSON.stringify( v, null, 2 ) : String( v );
    }
    
    Vue.$utils = {isDefined: isD, toString: to, classType: ct};
    
    function CR( r ) {
        var fn, s, rl, m, t = ct( r );
        if ( t === "function" ) {
            fn = function ( v, c ) {
                return r( v, c )
            }
        } else if ( t === 'string' ) {
            s = r.split( "::" ), rl = s[0], m = s[1];
            fn = function ( v, c ) {
                return (Vue.$rules[rl])( v, c )
            };
        } else if ( t === 'object' ) {
            fn = CR( r.rule ), m = r.msg;
        } else if ( t === "regexp" ) {
            fn = function ( v, c ) {
                return c( r.test( v ) )
            }
        }
        fn.msg = m;
        return fn;
    }
    
    // 验证
    function check( v ) {
        var vm = this, r = this._rule, flag;
        if ( typeof r === "function" ) r( v, function ( f, m ) {
            vm.message = (vm._flag = f) ? false : (m || r.msg);
            if ( !f ) vm.$emit( "input-error", vm.message );
            return f;
        } );
        return vm._flag;
    }
    
    var inputMixin = {
        props    : {
            rule   : null,
            width  : {type: String, default: "100%"},
            height : {type: String, default: "32px"},
            padding: {type: String, default: ""},
            margin : {type: String, default: ""}
        },
        data     : function () {
            return {message: false}
        },
        created  : function () {
            if ( isD( this.rule ) ) {
                this._rule = CR( this.rule );
                var form = this._form = this.$$parent( "u-form" );
                if ( form ) form.append( this );
                this._check = function ( v ) {
                    return check.call( this, v || this.modelValue )
                }
            }
        },
        destroyed: function () {
            if ( this._form ) this._form.remove( this );
        }
    };
    
    /**
     * textarea组件
     */
    Vue.component( "u-textarea", {
        props   : {
            value      : null,
            placeholder: String,
            length     : {type: Number, default: 200},
        },
        mixins  : [inputMixin],
        template: (
            " <div data-v-24ad2701 class=\"input-warp\"  :style='{ width : width, margin : margin, padding : padding }'>\n" +
            "    <textarea data-v-24ad2701 class=\"ipt\" :style='{height : height}' v-model=\"modelValue\" :placeholder=\"placeholder\"></textarea>\n" +
            "    <span data-v-24ad2701 class=\"compute-font\">{{fontCount}}</span>\n" +
            "    <div v-if='!!message' class='ipt-explain'>{{message}}</div>" +
            "  </div>"
        ),
        computed: {
            modelValue  : {
                get: function () {
                    return this.value
                },
                set: function ( t ) {
                    this.$emit( "input", t )
                }
            }, fontCount: function () {
                var t = this.modelValue, e = t ? t.length : 0;
                return e > this.length && this.$emit( "overflow" ), e + "/" + this.length;
            }
        }
    } );
    
    /**
     * 图片选择组件
     */
    Vue.component( "select-image", {
        template: (
            "  <div data-v-895fa160 class=\"select-image-warp\">\n" +
            "    <ul data-v-895fa160 class=\"ul\" :class=\"[vertical ? 'vertical' : 'level']\">\n" +
            "      <li data-v-895fa160 v-for=\"img in getImages\">\n" +
            "        <div data-v-895fa160 class=\"image-content flex\">\n" +
            "          <img data-v-895fa160 ref=\"image\" class=\"flex-item-orig\" :src=\"img.src\"/>\n" +
            "          <div data-v-895fa160 v-if=\"vertical\" class=\"flex-item slot flex flex-jcz-center\">\n" +
            "            <div data-v-895fa160 v-if=\"readonly\">{{img.desc}}</div>\n" +
            "            <u-textarea v-else width='270px' height='82px' v-model=\"img.desc\" placeholder=\"请输入图片描述\"></u-textarea>\n" +
            "          </div>\n" +
            "          <div v-if=\"!readonly\" data-v-895fa160 class=\"image-content-mask\">\n" +
            "            <form method='post' enctype='multipart/form-data' target=\"uploadIFrame\" ref='editIFrame' :action='uploadUrl'>\n" +
            "              <label data-v-895fa160 class=\"image-content-mask-btn image-content-mask-btn-left\">\n" +
            "                修改<input data-v-895fa160 type=\"file\" @change=\"edit(img, $event)\">\n" +
            "              </label>\n" +
            "            </form>\n" +
            "            <label data-v-895fa160 @click.stop=\"del(img)\"\n" +
            "                   class=\"image-content-mask-btn image-content-mask-btn-right\"></label>\n" +
            "          </div>\n" +
            "        </div>\n" +
            "      </li>\n" +
            "      <li v-if=\"!readonly && !isMax\" data-v-895fa160 class=\"select-image-add\">\n" +
            "        <iframe style='display: none' name='uploadIFrame' ref='uploadIFrame'></iframe>\n" +
            "        <form method=\"post\" enctype=\"multipart/form-data\" ref='addForm' target='uploadIFrame' :action='uploadUrl'>\n" +
            "          <label data-v-895fa160 class=\"image-content flex flex-zz-center flex-jcz-center flex flex-top\">\n" +
            "            <input data-v-895fa160 :multiple=\"multiple\" name='file' type=\"file\" @change=\"add\"/>\n" +
            "            <div data-v-895fa160 class=\"flex-item-orig select-image-add-icon\">+</div>\n" +
            "            <div data-v-895fa160 class=\"flex-item-orig select-image-add-text\">添加图片</div>\n" +
            "          </label>\n" +
            "        </form>\n" +
            "      </li>\n" +
            "    </ul>\n" +
            "  </div>"
        ),
        data    : function () {
            return {array: [], files: []}
        },
        props   : {
            uploadUrl: {type: String, default: "/upload/upload?source=goods"},
            images   : {
                type: Array, default: function () {
                    return []
                }
            },
            multiple : {type: Boolean, default: !1},
            imageType: {type: String, default: "jpg,jpeg,png,bmp"},
            length   : {type: Number, default: 5},
            vertical : {type: Boolean, default: !1},
            readonly : {type: Boolean, default: !1},
            imageSize: {type: Number, default: 3145728},
        },
        computed: {
            getRegExp: function () {
                return new RegExp( "(?:" + this.imageType.split( "," ).join( "|" ) + ")" )
            },
            getImages: function () {
                var t = this, e = this.images || [];
                return e.forEach( function ( e, n ) {
                    e.index = n, t.$nextTick( function () {
                        return t.getPath( e.file, function ( e ) {
                            return t.$refs.image[n].src = e
                        } )
                    } )
                } ), e
            },
            isMax    : function () {
                return this.length && this.images.length >= this.length
            },
        },
        mounted : function () {
            var iframe = this.$refs.uploadIFrame;
            this.uploadedCall = function ( fn ) {
                iframe.onload = function () {
                    fn( JSON.parse( iframe.contentWindow.document.body.innerText ) );
                };
            }
        },
        methods : {
            createdIterator: function ( a ) {
                var i = -1, l = a && a.length;
                return function ( call ) {
                    i = +1;
                    if ( l && (i < l) ) call( a[i] );
                };
            },
            toArray        : function ( t ) {
                return this.array.slice.call( t )
            },
            filter         : function ( t ) {
                var e = this, n = this.imageSize, i = !0,
                    a = t.filter( function ( t ) {
                        return e.getRegExp.test( t.type ) ? !(n && t.size > n) || (e.$emit( "file-size-error", t ), i = !1) : (e.$emit( "file-type-error", t ), i = !1)
                    } );
                return i ? a : [];
            },
            getPath        : function ( t, e ) {
                if ( t ) {
                    var n = new FileReader;
                    n.addEventListener( "load", function t( i ) {
                        e( i.target.result ), n.removeEventListener( "load", t )
                    } ), n.readAsDataURL( t )
                }
            },
            add            : function ( t ) {
                var s = this, e, n = this.filter( this.toArray( t.target.files ) ), i = this.images.length,
                    a = n.map( function ( t ) {
                        return {file: t, index: i, desc: null}
                    } );
                var iterator = this.createdIterator( a );
                iterator( function call( obj ) {
                    s.uploadedCall( function ( res ) {
                        if ( !res.url ) return alert( "图片上传失败，请重试上传！" );
                        obj.url = res.url;
                        (e = s.images).push( obj );
                        iterator( call );
                    } );
                    this.$refs.addForm.submit();
                } );
            },
            edit           : function ( t, e ) {
                var n = t.index, i = this.filter( this.toArray( e.target.files ) )[0];
                i && this.images && this.images.splice( n, 1, {file: i, index: n, desc: null} )
            },
            del            : function ( t ) {
                var e = t.index;
                this.images.splice( e, 1 )
            }
        }
    } );
    
    /**
     * form表单
     */
    Vue.component( "u-form", {
        template: (
            "  <form data-v-f4ca5dcc class=\"form\" :style=\"{ width }\">\n" +
            "    <slot></slot>\n" +
            "  </form>"
        ),
        props   : {width: String, labelWidth: String},
        created : function () {
            var c = [];
            this.append = function ( v ) {
                (c.indexOf( v ) === -1) && c.push( v )
            };
            this.remove = function ( v ) {
                c.splice( c.indexOf( v ), 1 )
            };
            this.checkAll = function () {
                return !c.filter( function ( vm ) {
                    return !vm._check()
                } ).length;
            }
        }
    } );
    
    /**
     * form-item
     */
    Vue.component( "u-form-item", {
        template: (
            "  <div data-v-5b242029 class=\"row form-item-row flex\">\n" +
            "    <div data-v-5b242029 class=\"col form-item-label flex-item-orig\">\n" +
            "      <span data-v-5b242029 v-if=\"required\" class=\"form-item-label-imp\">*</span>\n" +
            "      <slot name=\"label\"><label data-v-5b242029>{{label}}</label></slot>\n" +
            "    </div>\n" +
            "    <div data-v-5b242029 class=\"col form-item-content flex-item\">\n" +
            "      <slot></slot>\n" +
            "    </div>\n" +
            "  </div>"
        ),
        props   : {
            label      : String,
            required   : {type: Boolean, default: !1},
            labelColumn: {type: Number, default: 6}
        }
    } );
    
    /**
     * 按钮
     */
    Vue.component( "u-button", {
        template: (
            "  <button data-v-6b64a23e " +
            ":style='{ width :width, height: height, padding: padding, margin: margin }'" +
            " :type=\"nativeType\"" +
            " @click.stop='clickHandler'" +
            " class=\"btn\"" +
            " :disabled=\"disabled\" :class=\"'btn-'+type\">\n" +
            "    <slot></slot>\n" +
            "  </button>"
        ),
        props   : {
            width     : {type: String, default: ""},
            height    : {type: String, default: ""},
            padding   : {type: String, default: ""},
            margin    : {type: String, default: ""},
            type      : {type: String, default: "default"},
            nativeType: {type: String, default: "button"},
            disabled  : {type: Boolean, default: !1},
            check     : {type: Boolean, default: !1}
        },
        created : function () {
            if ( this.check ) {
                var f = this.$$parent( "u-form" );
                if ( f ) this.$check = function () {
                    return f.checkAll();
                }
            }
        },
        methods : {
            clickHandler: function () {
                this.$emit( "click", this.$check && this.$check() )
            }
        }
    } );
    
    /**
     * text输入框
     */
    Vue.component( "u-input", {
        template: (
            "  <div class='ipt-warp'  :style='{ width : width, margin : margin, padding : padding }'><input :type=\"password ? 'password' :'text'\"\n" +
            "         v-model=\"modelValue\"\n" +
            "         class=\"ipt\"\n :style='{ height : height }' " +
            "         :title=\"title\"\n" +
            "         :placeholder=\"placeholder\">" +
            "  <div v-if='!!message' class='ipt-explain'>{{message}}</div>" +
            "  </div>"
        ),
        props   : {
            value      : null,
            placeholder: String,
            title      : String,
            password   : {type: Boolean, default: !1},
        },
        mixins  : [inputMixin],
        computed: {
            modelValue: {
                get: function () {
                    return this.value
                },
                set: function ( e ) {
                    this.$emit( "input", e );
                    if ( this.rule ) this.$nextTick( function () {
                        this._check( e )
                    } )
                }
            }
        },
    } );
    
    /**
     * select选择框
     */
    Vue.component( "u-select", {
        template: (
            "    <div class=\"ipt-warp\"  :style='{width : width, margin : margin, padding : padding }'>\n" +
            "      <select :style='{  height : height }' v-model='modelValue' class=\"ipt\">\n" +
            "        <slot></slot>\n" +
            "      </select>\n" +
            "      <div v-if='!!message' class='ipt-explain'>{{message}}</div>\n" +
            "    </div>"
        ),
        props   : {
            value: null,
            title: String,
        },
        mixins  : [inputMixin],
        computed: {
            modelValue: {
                get: function () {
                    return this.value
                },
                set: function ( e ) {
                    this.$emit( "input", e );
                    if ( this.rule ) this.$nextTick( function () {
                        this._check( e )
                    } )
                }
            }
        },
    } );
    Vue.component( 'vue-pagination', {
        props   : ['cur', 'all'],
        template: '<div class="page-bar"><ul> <li v-if="all>1"><a  class="setButtonClass(newcur)" @click="prvePage(newcur)">上一页</a></li><li v-for="index in indexs"  v-bind:class="{ active: newcur == index }"><a @click="btnClick(index)">{{ index < 1 ? "..." : index }}</a></li><li v-if="all>2"><a  @click="nextPage(newcur)">下一页</a></li></ul></div>',
        data    : function () {
            return {newcur: this.cur}
        },
        computed: {
            indexs: function () {
                var left = 1
                var right = this.all
                var ar = []
                if ( this.all >= 11 ) {
                    if ( this.newcur > 5 && this.newcur < this.all - 4 ) {
                        left = this.newcur - 5
                        right = this.newcur + 4
                    } else {
                        if ( this.newcur <= 5 ) {
                            left = 1
                            right = 10
                        } else {
                            right = this.all
                            left = this.all - 9
                        }
                    }
                }
                while ( left <= right ) {
                    ar.push( left )
                    left++
                }
                if ( ar[0] > 1 ) {
                    ar[0] = 1;
                    ar[1] = -1;
                }
                if ( ar[ar.length - 1] < this.all ) {
                    ar[ar.length - 1] = this.all;
                    ar[ar.length - 2] = 0;
                }
                return ar
            }
        },
        methods : {
            btnClick      : function ( data ) {
                if ( data < 1 ) return;
                if ( data != this.newcur ) {
                    this.newcur = data
                    this.$emit( 'btn-click', data )
                }
            },
            nextPage      : function ( data ) {
                if ( this.newcur >= this.all ) return;
                this.btnClick( this.newcur + 1 );
            },
            prvePage      : function ( data ) {
                if ( this.newcur <= 1 ) return;
                this.btnClick( this.newcur - 1 );
            },
            setButtonClass: function ( isNextButton ) {
                if ( isNextButton ) {
                    return this.newcur >= this.all ? "page-button-disabled" : ""
                }
                else {
                    return this.newcur <= 1 ? "page-button-disabled" : ""
                }
            }
        }
    } )
    
} );

(function ( udf ) {
    
    var utils = Vue.$utils;
    var isD = utils.isDefined;
    var to = utils.toString;
    
    Vue.$rules = {// 验证规则缓存
        // 非空校验
        UnEmpty: function ( v, c ) {
            c( isD( to( v ).trim() ) )
        },
        number : function ( v, c ) {
        }
    };
    
})();

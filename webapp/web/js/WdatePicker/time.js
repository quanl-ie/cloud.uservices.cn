//处理所有时间

    var makeDate = new Date();
    var nowDate = new Date();
    var timeStr = '';
    //var dateStr = nowDate.getDay();
    var yearStr = nowDate.getFullYear();
    var monthsStr = nowDate.getMonth();
    var dayStr = nowDate.getDate();

    //小于10前置0
    function addPreZero(num) {
        if(num<10) {
            return '0'+num;
        }
        return num;
    }


    //填充日期
    $(function () {

        nowDate.setDate(nowDate.getDate());

        $('.warp-date ul li h3').each(function (i,ele) {

            var newDate = new Date();//new Date();
            newDate.setDate(nowDate.getDate()+i);

            var weekStr = ['周日','周一','周二','周三','周四','周五','周六'];
            var week = weekStr[newDate.getDay()];

            switch(i) {
                case 0:
                    week = '今天';
                    break;
                case 1:
                    week = '明天';
                    break;
                case 2:
                    week = '后天';
                    break;
            }

            var month = addPreZero(newDate.getMonth()+1);
            var day = addPreZero(newDate.getDate());

            var str = week+month+'/'+day;
            $(ele).text(str);
        });

        noSelectTime();

    });
    var isToday = true ;

    //过期时间显示为灰色
    function noSelectTime() {
        //@todo上线要22改回18
        if (nowDate.getHours()<6 || nowDate.getHours()>=18){

            $('.tdpr-wap').css("borderColor","#ccc");
            $('.tdpr-wap a').css("color","#ccc");
            $('.tdpr-wap').unbind('click');
        }

        $('.shijian-wap li').each(function (i,ele) {
            $(this).removeClass('acti');
            //设置非选中
            if(dayStr == nowDate.getDate() && parseInt($('a',ele).text().substr(0,2))<=nowDate.getHours()){
                if(parseInt($('a',ele).text().substr(0,2))==nowDate.getHours() && parseInt($('a',ele).text().substr(-2))>nowDate.getMinutes()){
                    $(ele).removeClass('no-sele');
                }else {
                    $(ele).addClass('no-sele');
                    $(ele).unbind('click');
                }

            }else {
                $(ele).removeClass('no-sele');
            }

            //设计激活
            if(makeDate.getDate() == dayStr && parseInt($('a',ele).text().substr(0,2)) == parseInt(timeStr.substr(0,2)) && parseInt($('a',ele).text().substr(-2)) == parseInt(timeStr.substr(-2))){
                if (tee && !$(this).hasClass('no-sele')){
                    $(this).addClass('acti');
                }
            }

            $(this).unbind('click');

        });
    }
    //点击确定，隐藏选项卡且将选择的时间填充
    $('.yes-time-a').click(function (eve) {
        eve.preventDefault();
        $('.datexz-wap').hide();

        var weekStr = ['周日','周一','周二','周三','周四','周五','周六'];
        var week = weekStr[makeDate.getDay()];

        switch(makeDate.getDay()-nowDate.getDay()) {
            case 0:
                week = '今天';
                break;
            case 1:
                week = '明天';
                break;
            case 2:
                week = '后天';
                break;
        }

        //填写页面时间2017-09-29 19:17:00
        var showDate = ''+(makeDate.getFullYear())+'-'+(addPreZero(makeDate.getMonth()+1))+'-'+addPreZero(makeDate.getDate())+' '+addPreZero(makeDate.getHours())+':'+addPreZero(makeDate.getMinutes())+':00';
        $('#make_time').val(showDate);
        // console.log( $('.abcdef').val(123));
         $('.planTime').val(showDate);


        //将时间写入li的data数据中
        $('.lwsrs span').data('time',parseInt(makeDate.getTime()/1000));

        timeSelect = true;
        location.hash = 'home';
    });

    var tee=true;
    //立即上门
    var dianji = 1;
    $(".tdpr-wap").click(function(){
        // console.log(isToday);
        if(!isToday){
            return ;
        }
        if(tee){
            $(this).addClass('rotate');
            $('.no-time-a').css("display",'none');
            $(".shijian-wap ul li").removeClass('acti');
            tee=false;
            //预约时间为当前时间
            makeDate = new Date();
        }else{
            tee=true;
            $(this).removeClass('rotate');
            $('.no-time-a').css("display",'block');
            //清空预约时间
            makeDate = new Date();
        }
        dayStr = makeDate.getDate();
        makeDate.setHours(makeDate.getHours()+2)
        noSelectTime()
         $('#order_make_time_type').val(1);
    });
    //点击时间，移除立即上门激活
    $(".shijian-wap ul").delegate('li:not(.no-sele)','click',function(event) {
        $('.no-time-a').css("display",'none');
        $(".tdpr-wap").removeClass('rotate');
        tee=true;
        setMakeTime($('a',this).text());
        // console.log($(this).text());
        $('.shijian-wap ul li').css(
            {
            "border":"1px solid #999",
        });
        $('.shijian-wap ul li').find('img').hide();
        $(this).css({
            "border":"1px solid #2693ff",
        });
        $(this).find('img').show();
        $('#order_make_time_type').val(2);
    });
    //点击 日期选项卡，移除立即上门激活
    $(".datexz-box .warp-date li").click(function(event) {
        if($(this).index() == 0){
            $(".tdpr-wap").removeClass('no-sele')
            isToday = true ;
        }else {
            $(".tdpr-wap").addClass('no-sele');
            isToday = false ;
        }
        $(".tdpr-wap").removeClass('rotate');
        tee=true;
        setDate($('h3',this).text());
        $('.shijian-wap ul li').css("border","1px solid #999");
        $('.shijian-wap ul li').find('img').hide();
         $(".datexz-box .warp-date li").find("h3").css('color','#444');
        $(this).find("h3").css('color',"#2693ff");
    });
    //点击选择时间，弹出时间选择选项卡
    $("#plan_time").on('click',function(event) {
        $(".datexz-wap").show();
    });
    $('#rili').on('click',function () {
        $(".datexz-wap").show();
    })
    // 点击关闭时间选择选项卡
    $(".rogate").click(function(event) {
        $(".datexz-wap").hide();
    });

    //设置时间
    function setMakeTime(time){

        timeStr = time;
        makeDate.setFullYear(yearStr);
        makeDate.setMonth(monthsStr);
        makeDate.setDate(dayStr);

        makeDate.setHours(parseInt(time.substr(0,2)));
        makeDate.setMinutes(parseInt(time.substr(3,2)));
        makeDate.setSeconds(0);
    }

    //设置日期
    function setDate(date) {
        var nowDateTime = new Date();
        //预约月份小于当前月，年份加1
        if (parseInt(date.substr(-5,2))-1 < nowDateTime.getMonth()) {
            yearStr = nowDateTime.getFullYear()+1;
        }
        //否者年份为当前年份
        else{
            yearStr = nowDateTime.getFullYear();
        }

        monthsStr = parseInt(date.substr(-5,2)-1);
        dayStr = parseInt(date.substr(-2));
        noSelectTime();
    }

    $("#note").keyup(function(){  
        if($("#note").val().length > 200){
            $("#note").val( $("#note").val().substring(0,200) );
        }
            $("#word").text( $("#note").val().length ) ;
    });
    // zi
    jQuery.fn.extend({
            autoHeight: function(){
                return this.each(function(){
                    var $this = jQuery(this);
                    if( !$this.attr('_initAdjustHeight') ){
                        $this.attr('_initAdjustHeight', $this.outerHeight());
                    }
                    _adjustH(this).on('input', function(){
                        _adjustH(this);
                    });
                });
                function _adjustH(elem){
                    var $obj = jQuery(elem);
                    return $obj.css({height: $obj.attr('_initAdjustHeight'), 'overflow-y': 'hidden'})
                            .height( elem.scrollHeight );
                }
            }
        });
        // 使用
        // $(function(){
        //     $('#note').autoHeight();
        // });















;(function ( facory ) {
  if ( Vue ) facory();
})( function () {

  function isString( s ) {
    return typeof s === "string";
  }

// 匹配组件
  function checkName( name, componentTag ) {
    return isString( name )
      ? name === componentTag
      : name.test( componentTag ) === true;
  }

// 向下通知
  function broadcast( componentName, eventName, params ) {
    this.$children.forEach( child => {
      let name = child.$options._componentTag;
      if ( checkName( componentName, name ) ) {
        child.$emit.apply( child, [eventName].concat( params ) );
      } else {
        broadcast.apply( child, [componentName, eventName].concat( [params] ) );
      }
    } );
  }

  //
  Vue.mixin( {
    methods : {
      /**
       * 是不是指定名称的组件
       * @param vm
       * @param tagName
       * @returns {*|boolean}
       */
      $$is( vm, tagName ) {
        return vm && checkName( tagName, vm.$options._componentTag );
      },
      /**
       * 根据组件名称逐级查找父级组件（一旦找到则停止查找）
       * @param tagName
       * @param endTag
       * @returns {*|Vue}
       */
      $$parent( tagName, endTag ) {
        let parent = this.$parent || this.$root;
        let name = parent.$options._componentTag;
        const hasEnd = !!endTag && checkName( endTag, name );
        while ( !hasEnd && parent && (!name || !checkName( tagName, name )) ) {
          parent = parent.$parent;
          if ( parent ) name = parent.$options._componentTag;
        }
        return parent;
      },

      /**
       * 找到符合名称的子元素
       * @param tagName
       */
      $$children( tagName ) {
        let res = [];
        let that = this;
        (function find( children ) {
          if ( children && children.length ) {
            children.forEach( child => {
              if ( that.$$is( child, tagName ) ) res.push( child );
              find( child.$children );
            } );
          }
        })( this.$children );
        return res;
      },

      /**
       *【向上广播事件】
       * 逐级查找父组件，找到匹配的组件名称，并触发指定的自定义事件
       * @param componentName
       * @param eventName
       * @param params
       */
      $$dispatch( componentName, eventName, params ) {
        let parent;
        if ( parent = this.$$parent( componentName ) )
          parent.$emit.apply( parent, [eventName].concat( params ) );
      },

      /**
       *【向下广播事件】
       * 逐个查找每个子组件，找到匹配的子组件，并触发指定的自定义事件
       * @param componentName
       * @param eventName
       * @param params
       */
      $$broadcast( componentName, eventName, params ) {
        broadcast.call( this, componentName, eventName, params );
      },

    }
  } );

} );

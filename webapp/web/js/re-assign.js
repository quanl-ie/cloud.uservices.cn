var avatarUrl = $('#account_data').attr('data-img')=="/images/morentouxiang.png"?'/images/customer1.png':$('#account_data').attr('data-img');
var accountName = $('#account_data').attr('data-name');
var accountMobile =$('#account_data').attr('data-mobile') ;
var accountAddress =$('#account_data').attr('data-address') ;
var customerPosition = [Number($('#account_lon').val()),Number($('#account_lat').val())];//客户经纬度
var technicianData;
// 初始化地图
var map = new AMap.Map("container1", {
    resizeEnable: true,
    center:  customerPosition, //客户地址
    zoom: 12
});
AMapUI.loadUI(['control/BasicControl'], function(BasicControl) {

    zoomCtrl2 = new BasicControl.Zoom({
        position: 'br',
        showZoomNum: false
    });

    map.addControl(zoomCtrl2);
});
map.setFeatures(['road','bg','building']);
map.on('zoomend',function(){
    map.getZoom() == 18?map.setFeatures(['bj','road','point']):map.setFeatures(['bj','road']);
});
// 订单位置icon
var customerInfo = new AMap.InfoWindow({isCustom:true,offset: new AMap.Pixel(0, -40)});
var customer  = new AMap.Marker({
    map: map,
    position:customerPosition,
    icon: new AMap.Icon({
        size: new AMap.Size(40, 40),  //图标大小
        image: avatarUrl,
        imageOffset: new AMap.Pixel(0, 0)
    })
});
var customerHtml = '<div class="kh-content"><i class="icon1-cha"></i><p>联系人：'+accountName+'</p><p>联系电话：'+accountMobile+'</p><p>联系地址：'+accountAddress+'</p>'
    avatarUrl!="/images/customer1.png"?customerHtml += '<img src="'+avatarUrl+'" class="kh-cimg">':'';
    customerHtml +='</div>';
customer.content =customerHtml ;

AMap.event.addListener(customer, 'click', function(e) {
    customerInfo.setContent(e.target.content);
    customerInfo.open(map, customer.getPosition());
    circle.setRadius(0*1000);
    map.remove(positionIonRemove);
});
//点击关闭信息窗体
$(document).on('click','.icon1-cha',function() {
  customerInfo.close();
})
map.on('click',function(e) {customerInfo.close()})
//
var technicianArray = new Array();
$('#individual').on('click',function() {
    $('#headman').removeClass('elect-a');
    $(this).addClass('elect-a');
    technicianDisplay($('.technician-left-list'),$('.technician-left-list'),$('.technician-right-list'));

    $('#groupid').find('option').each(function () {
        console.log($(this).attr('data-leader'));
        if($(this).attr('data-total') >0){
            $(this).css('color','#000000');
        }
        else {
            $(this).css('color','#999999');
        }
    });
    getList();



//    console.log($('.amap-marker-label').width());
})
$('#headman').on('click',function() {

    $('#individual').removeClass('elect-a');
    $(this).addClass('elect-a');
    technicianDisplay($('.technician-right-list'),$('.technician-right-list'),$('.technician-left-list'));

    $('#groupid').find('option').each(function () {
        if($(this).attr('data-leader') >0){
            $(this).css('color','#000000');
        }
        else {
            $(this).css('color','#999999');
        }
    });
    getList();


})
function technicianDisplay(a,b,c){
    infoWindow==""?"":infoWindow.close();
    map.remove(positionIonRemove);
    map.remove(brightPositionRemove);
    map.setCenter(customerPosition);
    circle.setRadius(0);
    $(b).addClass('act').show();
    $(c).removeClass('act').hide();
    $('.checkbox-btn').find('input').prop('checked',false);
}
var technician = "";//可以服务的marker
var markers = [];//用于清除可以服务的marker
function workTechnician(dataArr) {
    // 图标遍历展示
    for (var i = 0; i <dataArr.length; i++) {
        technician = new AMap.Marker({
            position: [dataArr[i].lng, dataArr[i].lat],
            map: map,
            icon: new AMap.Icon({
                size: new AMap.Size(40, 40),  //图标大小
                image: dataArr[i].img,
                imageOffset: new AMap.Pixel(0, 0)
            }),
        });
        if(dataArr[i].name.length>5){
            dataArr[i].name = dataArr[i].name.substring(0,5)+'...';
        }
        markers.push(technician);
        technician.setLabel({//label默认蓝框白底左上角显示，样式className为：amap-marker-label
            offset: new AMap.Pixel(-18, -45),
            content: dataArr[i].name
        });
    }

}
var infoWindow="";
function openInfo(zb,jiName) {
    infoWindow = new AMap.InfoWindow({
        content: jiName,
        offset:new AMap.Pixel(-5, -40)
    });
    infoWindow.open(map,zb);
}
var circle = new AMap.Circle({
    map:map,
    radius: 1000, //半径
    strokeColor: "#F33", //线颜色
    strokeOpacity: 1, //线透明度
    strokeWeight: 3, //线粗细度
    fillColor: "#ee2200", //填充颜色
    fillOpacity: 0.35//填充透明度
});;
// 点击查看技师服务范围
var positionIonRemove = [],brightPositionRemove=[];
function seePosition(obj){
    map.remove(positionIonRemove);
    map.remove(brightPositionRemove);
    var ReceiptRange = Number($(obj).nextAll('.data-array').attr('data-jsradius'));//接单范围
    var ReceiptPosition = [Number($(obj).nextAll('.data-array').attr('data-jslng')),Number($(obj).nextAll('.data-array').attr('data-jslat'))];
    var acrualPosition = [Number($(obj).nextAll('.data-array').attr('data-lng')),Number($(obj).nextAll('.data-array').attr('data-lat'))]
    var avatar = $(obj).nextAll('.data-array').attr('data-avatar');//头像
    var jiName = $(obj).nextAll('.data-array').attr('data-name');
    console.log(ReceiptPosition);
    // 技师服务范围
    circle.setRadius(ReceiptRange*1000);
    circle.setCenter(ReceiptPosition);
    if(ReceiptPosition[0] ==0||ReceiptPosition[1] ==0){
        alert('该技师未设置接单范围')
    }else{
        var  positionIon = new AMap.Marker({
            position:ReceiptPosition,
            map: map,
            icon: new AMap.Icon({
                size: new AMap.Size(20, 20),  //图标大小
                image: "../images/location.png",
                imageOffset: new AMap.Pixel(0, 0)
            })
        });
        positionIonRemove.push(positionIon);
    }

    // 点亮技师实时位置
    if(acrualPosition[0] ==0||acrualPosition[1] ==0){
        alert('该技师未更新实时位置');
    }else{
        var brightPosition  = new AMap.Marker({
            position:acrualPosition,
            map: map,
            icon: new AMap.Icon({
                size: new AMap.Size(40, 40),  //图标大小
                image: avatar,
                imageOffset: new AMap.Pixel(0, 0),
            }),
        });
        brightPositionRemove.push(brightPosition);
        openInfo(acrualPosition,jiName);
        map.setCenter(acrualPosition);
    }

    // brightPosition.setContent(jiName);


    // technician.setLabel({//label默认蓝框白底左上角显示，样式className为：amap-marker-label
    //        offset: new AMap.Pixel(-20, -40),
    //        content: dataArr[i].name
    //    });
}

// circle.setMap(map);


//指派，改派提交
var layerIndex = null;
$('#btnAssign').click(function () {

    //选中的技师
    var technician_id = [];
    var technicianArr = [];
    $('.ck_technicianid').each(function () {
        if($(this).prop('checked') == true){
            technician_id.push($(this).val());
            technicianArr.push({
                id: $(this).val(),
                name:$(this).attr('data-jsname')
            });
        }
    });
    if(technician_id.length > 0)
    {
        //指派技师还是组长 1 技师 2组长
        var type = 2;
        if($('.assign-content').find('.act').index() == 0){
            type = 1;

            var $select = '';
            if(technicianArr.length == 1){
                $select = 'selected = "selected"';
            }
            //如果多技师要求选一个负责人
            var lenderHtml = '<option value="0">请选择</option>';
            for(i in technicianArr){
                lenderHtml+='<option '+$select+' value="'+technicianArr[i].id+'" '+(lender ==technicianArr[i].id?'selected="selected"':'' )+'>'+ technicianArr[i].name +'</option>>';
            }
            $('#optHeadman').html(lenderHtml);
        }

        var selectDate   = $('#selectDate').val();
        var plantimeType = $('#plan_time_type').val();
        var setTime      = $('#setTime').val();

        var plantime = selectDate;
        if(plantimeType == 5){
            plantime = selectDate+' '+setTime;
        }

        var timestamp = Date.parse(new Date());
        if($.trim(plantime)=='' ){
            alert('请选择服务时间', function () {
                $('#selectDate').click();
            });
            return false;
        }
        else if(plantimeType == 0){
            alert('请选择服务时间', function () {
                $('#plan_time_type').focus();
                $('#plan_time_type').click();
            });
            return false;
        }
        else if(plantimeType == 5 && toTimestamp(plantime) < timestamp/1000){
            alert('服务时间不能小于当前时间', function () {
                $('#selectDate').click();
            });
            return false;
        }
        
        if(type==1)
        {
            if(hasShowReason(technician_id)){
                $('#cancelLayer').find('#reason').hide();
            }
            else {
                $('#cancelLayer').find('#reason').show();
            }

            layerIndex = layer.open({
                type: 1,
                title: '提示',
                area: ['300px', '270px'],
                fixed: true,
                maxmin: false,
                content: $('#cancelLayer').html()
            });
            return false;
        }
        else
        {
            //检查是否修改了
            var changeStatus = getChangeStatus(technician_id,0);
            if(changeStatus == 1){
                alert('您未做任何改变，无需改派！');
                return false;
            }
            else if(changeStatus == 2)
            {
                $('#groupLeaderAssign').click();
            }
            else {
                layerIndex = layer.open({
                    type: 1,
                    title: '提示',
                    area: ['300px', '270px'],
                    fixed: true,
                    maxmin: false,
                    content: $('#groupLeaderAssignLayer').html()
                });
            }
            return false;
        }
    }
    else {
        if($('.assign-content').find('.act').index() == 0){
            alert('请选择技师');
        }
        else {
            alert('请选择组长');
        }
    }
});

//多技师选择负责人后指派操作
$('body').delegate('#confirmSubmitAssign','click',function () {
    var reason = $(this).parents('.layui-layer-content').find('#reason').val();
    var lender = $(this).parents('.layui-layer-content').find('#optHeadman').val();

    var technicianIds = [];
    $('.ck_technicianid').each(function () {
        if($(this).prop('checked') == true){
            technicianIds.push($(this).val());
        }
    });

    //检查是否修改了
    var changeStatus = getChangeStatus(technicianIds,lender);
    if(changeStatus == 1){
        alert('您未做任何改变，无需改派！');
        return false;
    }

    if(changeStatus == 0 && reason == ''){
        alert('请选择改派原因');
        return false;
    }
    if(lender == 0){
        alert('请选择工单负责人');
        return false;
    }

    $(this).parents('.layui-layer-content').find('#confirmSubmitAssign').hide();
    $(this).parents('.layui-layer-content').find('#confirmSubmitAssigning').show();

    var obj = this;
    ajaxAssign(1,technicianIds,lender,reason,function () {
        $(obj).parents('.layui-layer-content').find('#confirmSubmitAssign').show();
        $(obj).parents('.layui-layer-content').find('#confirmSubmitAssigning').hide();
    });
});
//改派组长
$('body').delegate('#groupLeaderAssign','click',function () {
    var reason = $(this).parents('.layui-layer-content').find('#reason').val();
    var lender = $(this).parents('.layui-layer-content').find('#optHeadman').val();

    var technicianIds = [];
    $('.ck_technicianid').each(function () {
        if($(this).prop('checked') == true){
            technicianIds.push($(this).val());
        }
    });

    var changeStatus = getChangeStatus(technicianIds,0);

    if(changeStatus == 0 && reason == ''){
        alert('请选择改派原因');
        return false;
    }
    if(lender == 0){
        alert('请选择工单负责人');
        return false;
    }


    $(this).parents('.layui-layer-content').find('#groupLeaderAssign').hide();
    $(this).parents('.layui-layer-content').find('#groupLeaderAssigning').show();

    var obj = this;
    ajaxAssign(2,technicianIds,0,reason,function () {
        $(obj).parents('.layui-layer-content').find('#groupLeaderAssign').show();
        $(obj).parents('.layui-layer-content').find('#groupLeaderAssigning').hide();
    });
});

//指派 与 后端交互
function ajaxAssign(type,technician_id,leader,reason,func){

    var selectDate   = $('#selectDate').val();
    var plantimeType = $('#plan_time_type').val();
    var setTime      = $('#setTime').val();
    if(typeof setTime == 'undefined'){
        setTime = '00:00';
    }

    var data = {
        work_no:$('#workno').val(),
        order_no:$("#orderno").val(),
        plan_time_date:selectDate,
        plan_time_hour:setTime,
        plan_time_type:plantimeType,
        type:type,
        technician_id:technician_id,
        leader:leader,
        reason:reason,
        groupid :$('#groupid').val()
    };

    $.post('/order/ajax-re-assign',data,function (json) {
        if(json.success == false){
            alert(json.message,function () {
                if(json.code == 20020){
                    layer.close(layerIndex);
                    $('.assign-title').find('.elect-a').click();
                }
            });
            func();
        }
        else {
            alert("改派成功",function(){
                layer.close(layerIndex);
                var local_url = $("#local_url").val();
                window.location.href=local_url;
                //location.replace($('.cancelLink').attr('href'));
            });
            func();
        }
    },'json')
}

//关闭弹层
$("body").delegate("#layerCancel","click",function(){
    if(layerIndex){
        layer.close(layerIndex);
    }
});
//搜索
$('.search').change(function () {
    getList();
});
//选择服务时间
var oldPlantime = '';
(function(){
    var selectDate   = $('#selectDate').val();
    var plantimeType = $('#plan_time_type').val();
    var setTime      = $('#setTime').val();
    if(typeof setTime == 'undefined'){
        setTime = '00:00';
    }
    oldPlantime = selectDate+ ''+ setTime+''+ plantimeType;
})();

$("body").delegate("#selectDate,#plan_time_type,#setTime","change",function(){

    var selectDate   = $('#selectDate').val();
    var plantimeType = $('#plan_time_type').val();
    var setTime      = $('#setTime').val();
    if(typeof setTime == 'undefined'){
        setTime = '00:00';
    }
    var tmp = selectDate+ ''+ setTime+''+ plantimeType;
    if(tmp != oldPlantime){
        oldPlantime = tmp;
        getList();
    }
});

//选择技师进行数据验证
$('body').delegate('.ck_technicianid','click',function () {
    //暂时不验证
    return true;
    if($(this).prop('checked') == true)
    {
        var radius = $(this).attr('data-radius');
        var time   = $(this).attr('data-time');
        var id     = $(this).val();
        var jsName   = $(this).attr('data-name');

        var selectDate   = $('#selectDate').val();
        var plantimeType = $('#plan_time_type').val();
        var setTime      = $('#setTime').val();
        if(typeof setTime == 'undefined'){
            setTime = '00:00';
        }

        if(radius == 1){
            $(this).prop('checked',false);
            confirm('技师【'+jsName+'】不在接单范围内，确定要选择？',function (obj) {
                $(obj).prop('checked',true);
                recordData();
            },this);
            return false;
        }
 /*       if(time == 1){
            $(this).prop('checked',false);
            confirm('技师【'+jsName+'】在所选服务时间前后半小时内有未完成的工单，确定要选择？',function (obj) {
                $(obj).prop('checked',true);
                recordData();
            },this);
            return false;
        }
*/
        var obj = this;
        $.getJSON('/order/ajax-has-plantime-conflict',{work_no:$('#workno').val(),technician_id:id,plan_time_date:selectDate,plan_time_hour:setTime,plan_time_type:plantimeType},function (json) {
            if(json.data.conflict == 1)
            {
              /*  $(obj).prop('checked',false);
                confirm('技师【'+jsName+'】在所选服务时间前后半小时内有未完成的工单，确定要选择？',function (obj) {
                    $(obj).prop('checked',true);
                    recordData();
                },obj);
            }
            else
            {*/
                var data = {
                    technician_id:id,
                    work_type:$('#work_type').val(),
                    address_id:$('#address_id').val(),
                    sale_order_id:$('#sale_order_id').val(),
                    plan_time_date:selectDate,
                    plan_time_hour:setTime,
                    plan_time_type:plantimeType
                };
                $.getJSON('/order/ajax-check-have-skills',data,function (json) {
                    var tip = '';
                    if(json.code == 20008){
                        tip = '技师【'+jsName+'】不在服务范围内，确定要选择？';
                    }
                  /*  else if(json.code == 20009){
                        tip = '技师【'+jsName+'】服务时间不在范围内，确定要选择？';
                    }*/
                    else if(json.code == 20010){
                        tip = '技师【'+jsName+'】不符合技能，确定要选择？';
                    }

                    if(tip!='')
                    {
                        $(obj).prop('checked',false);
                        confirm(tip,function (obj) {
                            $(obj).prop('checked',true);
                            recordData();
                        },obj);
                    }
                });
            }
        });
        recordData();
    }
});

//列表数据
function getList()
{
    var groupid = $('#groupid').val();
    var range   = $('#range').val();
    var skill   = $('#skill').val();
    var workno  = $('#workno').val();
    var orderno  = $('#orderno').val();
    var jobtitle = 2;
    if($('.assign-content').find('.act').index() == 0){
        jobtitle = 1;
    }

    var selectDate   = $('#selectDate').val();
    var plantimeType = $('#plan_time_type').val();
    var setTime      = $('#setTime').val();
    if(typeof setTime == 'undefined'){
        setTime = '00:00';
    }

    var data = {
        work_no:workno,
        order_no:orderno,
        grouping_id:groupid,
        range:range,
        skill:skill,
        jobtitle:jobtitle,
        plan_time_date:selectDate,
        plan_time_hour:setTime,
        plan_time_type:plantimeType
    };
    $('.assign-content').find('.act > ul').html('<div style="color:red;margin-top:100px;text-align: center;border:0px;padding:0px;"><img style="width: 20px;" src="/images/loading.gif"></div>');
    
    $.getJSON('/order/assign',data,function (json) {
        technicianArray = new Array();
        map.remove(markers);

        var groupLender = $('#groupLender').val();

        technicianData = json;
        var html = '';

        $('#groupid').find('option:selected').each(function () {
            if($(this).val() == groupid){
                $(this).css('color',json.length>0?'#000000':'#999999');
                $(this).attr('data-total',json.length =0?0:json.length);
            }
        });

        if(json.length > 0)
        {
            for(i in json)
            {
                var val = json[i];
                var imgy = val.technician_avatar=="http://fws.uservices.cn/images/morentouxiang.png"?"../images/jishitouxiang.png":val.technician_avatar;
                var obj = {
                    lng:val.lon,
                    lat:val.lat,
                    name:val.technician_name,
                    img:imgy
                }
                technicianArray.push(obj);
                html+='<li>';
                html+='    <p class="checkbox-btn">';
                if(jobtitle == 1){
                    var checkedHtml = '';
                    if(in_array(val.technician_id,oldJsId ) == true || in_array(val.technician_id,window.oldTechnicianIds)){
                        checkedHtml = 'checked="checked"';
                    }
                    html+='<input type="checkbox" data-name="'+val.technician_name+'" name="technicianid" '+(checkedHtml)+' class="ck_technicianid" data-radius="'+val.outServiceRadius+'" data-time="'+val.outJsWorkTime+'" data-jsname="'+val.technician_name+'" id="'+val.technician_id+'" value="'+val.technician_id+'">';
                }
                else {
                    var checkedHtml = '';
                    if(groupLender ==val.technician_id || in_array(val.technician_id,window.oldTechnicianIds)){
                        checkedHtml = 'checked="checked"';
                    }
                    html+='<input type="radio" '+(checkedHtml)+' data-name="'+val.technician_name+'" name="technicianid" class="ck_technicianid" data-radius="'+val.outServiceRadius+'" data-time="'+val.outJsWorkTime+'" id="'+val.technician_id+'" value="'+val.technician_id+'">';
                }
                html+='        <label class="brand-label" for="'+val.technician_id+'"><i class="gou-i"></i></label>';
                html+='    </p>';
                html+='    <label for="'+val.technician_id+'"><p>'+val.technician_name+'<span class="span-right999">'+val.scheduleSet+'【'+val.noFinshWork +'/'+val.totalWork+'】</p>';
                if(val.distance!='' && val.last_position!='') {
                    html += '    <p>实时距 ' + val.distance + 'km（' + val.last_position + '）</p></label>';
                }
                else {
                    html += '<p>实时距 --（未检测到技师位置）</p></label>';
                }
                html+='    <a href="javascript:" class="see-position" onclick="seePosition(this)">查看</a>';
                html+='        <input type="hidden" class="data-array" data-lng="'+val.lon+'"  data-lat="'+val.lat+'" data-jslng="'+val.js_mode_lon+'" data-jslat="'+val.js_mode_lat+'" data-jsradius="'+val.js_mode_radius+'" data-avatar="'+val.technician_avatar+'" data-name="'+val.technician_name+'">';
                html+=' </li>';
            }
        }else
        {
            html = '<div style="color:red;margin-top:100px;text-align: center;border:0px;padding:0px;">所选条件下无合适'+(jobtitle==2?'组长':'技师')+'</div>';
        }


        workTechnician(technicianArray);
        $('.assign-content').find('.act > ul').html(html);
    });
}

//判断是指派 还是改派
var oldJsId = [];
var lender = 0;
(function init(){
    var assignType = $('#assignType').val();
    oldJsId    = $('#member').val();
    oldJsId = oldJsId.split(',');
    lender = $('#lender').val();
    //组长id
    var groupLender = $('#groupLender').val();

    if(assignType == 1){
        $('#individual').click();
    }
    else {
        $('#headman').click();
    }
})();

//检查数据是否修改
function getChangeStatus(member,leader) {
    var assignType  = $('#assignType').val();
    var groupLender = $('#groupLender').val();
    var oldLender   = $('#lender').val();
    var oldMember   = $('#member').val();
    var oldPlantime = $('#oldPlantime').val();
    var planTime    = $('#plan_time').val();

    //多技师
    if(assignType == 1){
        oldMember = oldMember.split(',');
        if(timestampToTime(oldPlantime) == planTime && arraySort(oldMember).toString() == arraySort(member).toString() && oldLender == leader){
            return 1;
        }
        else if((timestampToTime(oldPlantime) != planTime || oldLender != leader) && arraySort(oldMember).toString() == arraySort(member).toString()){
            return 2;
        }
    }
    else { //组长
        groupLender = groupLender.split(',');
        if(timestampToTime(oldPlantime) == planTime &&  arraySort(groupLender).toString() == arraySort(member).toString() ){
            return 1;
        }
        else if(timestampToTime(oldPlantime) != planTime &&  arraySort(groupLender).toString() == arraySort(member).toString() ){
            return 2;
        }
    }
    return 0;
}
//检查改派原因是否要显示
function hasShowReason(member){
    var assignType  = $('#assignType').val();
    var oldMember   = $('#member').val();
    var oldPlantime = $('#oldPlantime').val();
    var planTime    = $('#plan_time').val();

    //多技师
    if(assignType == 1){
        oldMember = oldMember.split(',');
        if(arraySort(oldMember).toString() == arraySort(member).toString()){
            return true;
        }
    }
    return false;
}
//记录用户选择的数据
function recordData () {
    window.oldTechnicianIds = [];
    $('.ck_technicianid').each(function () {
        if($(this).prop('checked') == true){
            window.oldTechnicianIds.push($(this).val());
        }
    });
}
function getDepartment(el) {
        var id = $(el).next('input').val();
        var _this = $(el).parent('span');
        var auth = $('.data-auth').val();
        $.getJSON('/common/ajax-get-department',{'pid':id,"auth":auth},function (data) {
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>"
                $(data.data).each(function(i,el) {
                    ulData +="<li><span>";
                    el.exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="getDepartment(this)"></i>':'';
                    ulData+='<input type="checkbox" id="a'+el.id+'" value="'+el.id+'" name="department_id[]" >  <label for="a'+el.id+'">'+el.name+'<i class="gou-i"></i></label></span></li>';
                });
                ulData +="</ul>";
                _this.parent('li').append(ulData);
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');

            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })
    }
function  clickCategory(el) {
        //event.stopPropagation();
        $('.drop-dw-layerbox').is(':hidden')? $('.drop-dw-layerbox').show(): $('.drop-dw-layerbox').hide();
}
 // 选中内容
    $('#belongedBtn').bind('click', function() {
        //event.preventDefault();
        var id_array= [];
        var data_array=[];
        $('input[name="department_id[]"]:checked').each(function(){
            data_array.push($(this).next('label').text());
            id_array.push($(this).val());//向数组中添加元素
        });
        var idstr=id_array.join(',');//将数组元素连接起来以构建一个字符串
        var datatr = data_array.join('；')
        $('#department').val(datatr);
        $('#department_ids').val(idstr);
        $('.drop-dw-layerbox').hide();
        return false;
    });
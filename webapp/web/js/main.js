function parentReload( url ) {
    document.location.replace( url );
}

jQuery( function () {
    var udf;
    var labelPageHandler = {};
    var isDef = function ( s ) { return s !== null && s !== udf};
    var isBlank = function ( s ) { return jQuery.trim( s ).length <= 0 };
    var isString = function ( s ) { return isDef( s ) && typeof s === 'string' && !isBlank( s )};
    var isFunction = jQuery.isFunction;
    var indexOf = function ( a, i ) { return a.indexOf( i )};
    var removeInArray = function ( a, i ) {
        var ind = indexOf( a, i );
        if ( ind >= 0 ) a.splice( ind, 1 );
    };
    
    function showPage( page ) {
        page._isShow = true;
        page.pageWrap.addClass( 'ifrmbody-show' );
        page.nav.addClass( 'active' );
    }
    
    function hidePage( page ) {
        page._isShow = false;
        page.pageWrap.removeClass( "ifrmbody-show" );
        page.nav.removeClass( 'active' );
    }
    
    // 标签页导航tab容器
    var tabNavWrap = jQuery( '#ifrmTabNav' );
    // iFrame页面容器
    var iFrameWrap = jQuery( "#ifrmBox" );
    
    // 标签页队列
    var callbacks = jQuery.Callbacks();
    
    /**
     * 标签页对象
     * @constructor
     */
    function LabelPage( url, nav, wrap ) {
        var _this = this;
        var title, icon;
        //
        if ( nav instanceof jQuery && wrap instanceof jQuery ) {
            this.url = url;
            nav = this.nav = arguments[1];
            icon = this.closeIcon = nav.find( "i" );
            this.title = nav.find( "span" );
            wrap = this.pageWrap = arguments[2];
            this.page = wrap.find( 'iframe' );
            
        } else {
            this.url = url;
            // 创建一个标签页导航标签
            title = this.title = jQuery( "<span></span>" );
            icon = this.closeIcon = jQuery( "<i class=\"icon2-shanchu\"></i>" );
            //
            nav = this.nav = jQuery( "<li data-src=\"" + url + "\"></li>" ).append( title ).append( icon );
            // 创建一个iFrame页面，
            wrap = this.pageWrap = jQuery( "<div class='ifrmbody-list'></div>" );
            this.page = jQuery( "<iframe  frameborder=\"0\"></iframe>" ).appendTo( wrap );
            this.reload = true;
        }
        // 关闭标签页
        icon.on( 'click', function ( e ) {
            e.stopPropagation();
            _this.closePage();
        } );
        
        // 切换标签页
        nav.on( 'click', function ( e ) {
            e.stopPropagation();
            _this.switchPage();
        } );
        
        this.onLoaded( function ( page ) {
            // 每次iframe加载完成，修改导航标签上的标题文字
            this.updateTitle( page.contents().attr( "title" ) );
        } );
    }
    
    var urlReg = /\?(?:\w+=.+&?)*$/;
    /**
     * 打开页面
     * @param param
     */
    LabelPage.prototype.open = function ( param ) {
        param = isString( param ) ? param : "";
        if ( this.reload || param ) {
            this.param = param;
            this.reload = false;
            var url = this.url + (urlReg.test( url ) ? param : ('?' + param));
            this.page.prop( "src", url );
        }
        showPage( this );
    };
    
    /**
     * 隐藏当前页
     */
    LabelPage.prototype.close = function () {
        hidePage( this );
    };
    
    /**
     * 关闭当前页面
     */
    LabelPage.prototype.closePage = function () {
        labelPageHandler.closePage( this.url );
    };
    /**
     * 移除页面
     */
    LabelPage.prototype.remove = function () {
        if ( !this._removed ) {
            this.close();
            this.nav.off( 'click' );
            this.page.off( 'load' );
            this.closeIcon.off( 'click' );
            if ( isFunction( this._uninstall ) ) this._uninstall();
            this.nav.empty().remove();
            this.pageWrap.empty().remove();
            this._removed = true;
        }
    };
    
    /**
     * 初始添加到dom中
     * @returns {LabelPage}
     */
    LabelPage.prototype.append = function () {
        tabNavWrap.append( this.nav );
        iFrameWrap.append( this.pageWrap );
        return this;
    };
    
    /**
     * 修改title
     * @param title
     */
    LabelPage.prototype.updateTitle = function ( title ) {
        if ( isString( title ) ) this.title.text( this.labelTitle = title );
    };
    
    /**
     * 切换为当前标签页
     */
    LabelPage.prototype.switchPage = function () {
        labelPageHandler.openPage( this );
    };
    
    /**
     * 标签页加载完成后执行的函数
     * @param fn
     */
    LabelPage.prototype.onLoaded = function ( fn ) {
        var that = this, page = this.page.on( 'load', function () {
            if ( isFunction( fn ) ) fn.call( that, page );
        } );
    };
    
    //------------------------------------------------------
    var pageMap = {};// 标签页实例，url作为key
    var pageInstances = [];// 标签页顺序实例
    var leftNavCalls = jQuery.Callbacks();// 左侧导航栏回调函数队列
    
    /**
     * 创建回调队列
     * @param url
     * @param page
     */
    function createCall( url, page ) {
        // 创建标签页回调
        var register = function ( openUrl, param, openPage ) {
            // 如果要打开的url与当前标签页相等
            (url === openUrl || openPage === page)
                // 则打开
                ? page.open( param )
                // 否则隐藏（不是移除）
                : page.close();
        };
        // 卸载回调函数
        page._uninstall = function () {
            callbacks.remove( register );
            url = page = null;
        };
        
        // 添加标签页回调函数到队列中
        callbacks.add( register );
    }
    
    /**
     * 获取或者初始化一个标签页
     * @param url
     * @param nav
     * @param wrap
     * @returns {*}
     */
    function getPage( url, nav, wrap ) {
        var page = pageMap[url];
        // 如果标签页实例中没有当前url的页面
        if ( !page ) {
            // 创建一个
            page = pageMap[url] = new LabelPage( url, nav, wrap );
            pageInstances.push( page );
            // 将当前标签页添加到dom结构中，并创建队列
            createCall( url, page.append() );
        }
        return page;
    }
    
    /**
     * 发布打开标签页通知
     * @param url
     * @param param
     * @param page
     */
    function callOpenPage( url, param, page ) {
        callbacks.fire( url, param, page );
    }
    
    /**
     * 缓存标签页映射信息
     */
    function cachePage() {
        labelPageHandler.cache( pageInstances.slice() );
    }
    
    /**
     * 打开标签页
     */
    labelPageHandler.openPage = function ( url, title, param, reload ) {
        if ( jQuery.isPlainObject( url )
            || url instanceof LabelPage ) {
            param = url.param;
            title = url.title;
            url = url.url;
            reload = url.reload;
        }
        if ( !isString( url ) ) {
            throw new Error( "请给定要打开标签页的url地址！" );
        }
        //console.log( url );
        var page = getPage( url );
        page.updateTitle( title );
        if ( typeof reload === "boolean" ) page.reload = reload;
        callOpenPage( url, param, page );
        // 重置标签页导航栏位置，使当前要打开的标签页始终显示
        labelPageHandler.resetPositionNav( page.nav );
        // 高亮左侧导航栏对应url的菜单
        leftNavCalls.fire( url );
        // 缓存当前标签页
        cachePage();
        return page;
    };
    
    /**
     * 关闭标签页
     */
    labelPageHandler.closePage = function ( url ) {
        if ( !isString( url ) ) {
            throw new Error( "请给定要关闭标签页的url地址！" );
        }
        // 要移除关闭的标签页
        var closePage = pageMap[url];
        var isShow = closePage._isShow, nextIndex, nextShow;
        // 先隐藏要关闭的
        closePage.close();
        // 如果当前要关闭的标签页是显示状态
        if ( isShow ) {
            // 确定下一个要显示的标签页
            var index = indexOf( pageInstances, closePage );
            nextIndex = pageInstances.length === (index + 1) ? index - 1 : index + 1;
            nextShow = pageInstances[nextIndex];
            labelPageHandler.openPage( nextShow );
        }
        // 移除标签页
        closePage.remove();
        // 移除实例
        removeInArray( pageInstances, closePage );
        pageMap[url] = null;
        delete pageMap[url];
        // 缓存标签页
        cachePage();
    };
    
    // 点击左边侧边栏
    jQuery( document ).on( 'click', '.link-nav', function ( e ) {
        e.stopPropagation();
        var jq = jQuery( this ),
            url = jq.data( 'src' ),
            title = jq.find( 'a' ).text();
        // 打开标签页
        labelPageHandler.openPage( url, title, null, true );
    } );
    
    // 初始化左侧导航菜单栏回调队列
    jQuery( ".link-nav" ).each( function () {
        var jq = jQuery( this );
        var url = jq.data( "src" );
        var jqA = jq.find( 'a' );
        var parentA = jq.parent( 'ul.hover-nav' ).prev( "a" );
        // 添加回调函数，使打开的标签页url对应菜单高亮显示
        leftNavCalls.add( function ( openUrl ) {
            // 确定子级菜单
            if ( isString( openUrl ) ) {
                if ( url === openUrl ) {
                    jqA.addClass( 'colorfff' );
                    // 高亮父级菜单需要在确定子菜单后，再次执行队列确定
                    leftNavCalls.fire( jq );
                } else {
                    jqA.removeClass( 'colorfff' );
                    if ( parentA.hasClass( 'colorfff' ) ) {
                        parentA.removeClass( 'colorfff' );
                    }
                }
                // 确定父级菜单
            } else if ( openUrl instanceof jQuery && openUrl === jq ) {
                parentA.addClass( 'colorfff' );
            }
        } );
        
    } );
    
    // ======================================================
    // 初始化主页标签页
    var indexNav = jQuery( "#index-nav-li" ), indexPage = jQuery( "#index-page-div" );
    getPage( indexNav.data( 'src' ), indexNav, indexPage ).open();
    
    //退出登录
    $( '#logOff' ).on( 'click', function () {
        confirm( '确定退出？', function () {
            document.location.replace( '/site/logout' );
        } )
    } );
    
    //======================================
    // 标签页导航栏左右翻动
    (function () {
        
        var tabBox = $( '#tabNavBox' );
        var navWrap = $( '#ifrmTabNav' );
        var slice = Array.prototype.slice;
        
        function getNav() {
            return navWrap.children( 'li' );
        }
        
        function getBoxWidth() {
            return tabBox.width() - 70;
        }
        
        function getWrapPositionLeft() {
            return navWrap.position().left;
        }
        
        // 当标签页切换等状态发生时，需要进行处理让当前标签页导航标签始终显示出来
        labelPageHandler.resetPositionNav = function ( nav ) {
            // 显示的导航标签的占位宽度
            var width = nav.innerWidth();
            // 显示的导航标签相对标签导航栏的left位置
            var left = nav.position().left;
            // 可视区域的宽度
            var boxWidth = getBoxWidth();
            // 标签导航栏相对的left位置
            var pl = getWrapPositionLeft();
            
            // 如果当前显示导航标签在左边被遮挡（70是为了能点击到当前导航标签左边的导航标签）
            if ( (left - 70) < -pl ) {
                // 标签导航栏向左边移动
                moveRight( -left + 70 );
                // 如果当前导航标签在右侧被遮挡
            } else if ( (left + width) > (boxWidth + (-pl)) ) {
                // 标签导航栏向右边移动
                moveLeft( pl - ((left + width) - (boxWidth + (-pl))) );
            }
        };
        
        function moveLeft( left ) {
            navWrap.css( 'left', left + 'px' );
        }
        
        function moveRight( right ) {
            if ( right >= 0 ) right = 0;
            navWrap.css( 'left', right + 'px' );
        }
        
        //navPageLeft
        $( '#navPageRight' ).on( 'click', function () {
            var boxWidth = getBoxWidth();
            var pl = getWrapPositionLeft();
            var all = slice.call( getNav() ).reduce( function ( prev, curr ) {
                return prev + $( curr ).innerWidth();
            }, 0 );
            if ( (all + pl) > boxWidth ) moveLeft( pl - 50 );
        } );
        
        $( '#navPageLeft' ).on( 'click', function () {
            var pl = getWrapPositionLeft() || 0;
            if ( pl < 0 ) moveRight( pl + 50 );
        } );
        
    })();
    
    //============================================
    /**
     * header
     * 处理刷新页面和地址栏直接输入网址的情况
     */
    (function ( factory ) {
        var jqHeader = jQuery( "#header" ),
            id = jqHeader.data( 'id' ),
            session = "user-service-label-page" + id;
        if ( sessionStorage ) {
            factory( function () {
                return JSON.parse( sessionStorage.getItem( session ) );
            }, function ( result ) {
                sessionStorage.setItem( session, JSON.stringify( result ) );
            }, jqHeader );
        }
    })( function ( get, set, jqHeader ) {
        /**
         * 缓存标签页映射信息
         * @param pageArray
         */
        labelPageHandler.cache = function ( pageArray ) {
            set( pageArray.map( function ( page ) {
                return {
                    url    : page.url,
                    isShow : page._isShow,
                    title  : page.labelTitle,
                    param  : page.param,
                    reload : true
                };
            } ) );
        };
        var getCache = get();
        var showMap;
        var address = jqHeader.data( "address" );
        
        if ( !jQuery.isEmptyObject( getCache ) ) {
            getCache.forEach( function ( map ) {
                labelPageHandler.openPage( map );
                if ( map.isShow ) showMap = map;
            } );
        }
        //page.contents().attr( "title" )
        if ( isString( address ) ) {
            setTimeout( function () {
                labelPageHandler.openPage( { url : address } );
            } );
        } else if ( showMap ) {
            setTimeout( function () {
                labelPageHandler.openPage( showMap );
            } );
        }
        
    } );
    
    //========================================
    (function ( window ) {
        /**
         * 消息接受
         * @param event
         */
        function handler( event ) {
            var data = event.data;
            try {
                data = JSON.parse( data );
                labelPageHandler.openPage( data );
            } catch ( e ) {
                console.error( e );
            }
        }
        
        if ( window.addEventListener ) {
            window.addEventListener( 'message', handler );
        } else if ( window.attachEvent ) {
            window.attachEvent( 'onmessage', handler );
        }
        
    })( window );
    
} );
function confirm(message,func,obj){
    layer.confirm(message, {title:'提示'}, function(index){
        layer.close(index);
        func(obj);
    });
	return false;
}
	
function alert(message,func){
    layer.alert(message, function(index){
        layer.close(index);
        if(typeof(func) == 'function'){
            func();
        }
    });
}


//时间计算
function fun_submit_time(arg,dNum,type){
    var dNumInt  = parseInt(dNum);
    var date1 = new Date(arg);
    var date2 = new Date(arg);
    if(type == 'day'){
    	date2.setDate(date1.getDate()-dNumInt);

    }else if(type == "month"){ 
    	date2.setMonth(date1.getMonth()-dNumInt);
    }
   
    //补全时间
    var month = '' ;
    if((date2.getMonth()+1) <= 9){ 
        month = '0'+(date2.getMonth()+1) ;
    }else{ 
        month = (date2.getMonth()+1) ;
    }
    var day = '';
    if(date2.getDate() <= 9){ 
        day = '0'+ date2.getDate() ;
    }else{ 
        day = date2.getDate() ;
    }

    var times = date2.getFullYear()+"-"+month+"-"+day;
  
    return times;
}

//长度计算
function strlen(str) {
    var len = 0;
    for (var i = 0; i < str.length; i++) {
        var c = str.charCodeAt(i);
        //单字节加1 
        if ((c >= 0x0001 && c <= 0x007e) || (0xff60 <= c && c <= 0xff9f)) {
            len++;
        }
        else {
            len += 2;
        }
    }
    return len;
}
//截取字符串
function substr (str,n){ 
	var r=/[^\x00-\xff]/g; 
	if(str.replace(r,"mm").length<=n){return str;} 
	var m=Math.floor(n/2); 
	for(var i=m;i<str.length;i++){ 
    	if(str.substr(0,i).replace(r,"mm").length>=n){ 
    		return str.substr(0,i); 
    	} 
	} 
	return str; 
}
//左侧导航定位
function setNav (url){
    $('#leftside-navigation').find('a').each(function () {
        if($(this).attr('href') == url){
            $('#leftside-navigation').find('.active').removeClass('active');
            $(this).parent().addClass('active');
            $(this).parents('.sub-menu').addClass('active');
            return false;
        }
    });
}

function autocomplete(element,url,callbackFun) {
    $(element).autocomplete(url, {
        minChars: 1,
        matchCase:false,//不区分大小写
        autoFill: false,
        max: 10,
        dataType: 'json',
        width:$(element).outerWidth(true)+'px',
        extraParams:{v:function() { return $(element).val();}},
        parse: function(data) {
            return $.map(eval(data), function(row) {
                return {
                    data: row,
                    value: row.id,    //此处无需把全部列列出来，只是两个关键列
                    result: row.name
                }
            });
        },
        formatItem: function(row, i, max,term) {
            return  row.name;
        },
        formatMatch: function(row, i, max) {
            return row.name;
        },
        formatResult: function(row) {
            return row.name;
        },
        reasultSearch:function(row,v)//本场数据自定义查询语法 注意这是我自己新加的事件
        {
            //自定义在code或spell中匹配
            if(row.data.name.indexOf(v) == 0)
            {
                return row;
            }
            else
                return false;
        }
    }).result(function(event, data, formatted) { //回调
        (callbackFun)(data);
        $(element).val(data.name);
    });
}
//自动定位未定位的菜单
function setNoActiveMenu() {
    var cacheName = location.host.replace(/\./g,'') +"lasthref";
    if($('.sub-menu').find('li.active').length > 0){
        var href =$('.sub-menu').find('li.active').find('a').attr('href');
        localStorage[cacheName] = href;
    }
    else if($('.nano-content').find('li.active').length == 0) {
        setNav(localStorage[cacheName]);
    }
    else {
        localStorage[cacheName] = '';
    }
}
//转成时间戳
function toTimestamp(date) {
    date = new Date(Date.parse(date.replace(/-/g, "/")));
    date = date.getTime();
    return date/1000;
}
//数据函数
function in_array(search, array) {
    for (var i in array) {
        if (array[i] == search) {
            return true;
        }
    }
    return false;
}
//数组排序
function arraySort(arr){
    return arr.sort((function compare(a,b){
        return a - b;
    }));
}
//时间戳转换时间
function timestampToTime(timestamp,d) {
    var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    Y = date.getFullYear() + '-';
    M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
    D = (date.getDate() < 10?'0'+date.getDate(): date.getDate())+' '
    h = (date.getHours()<10?'0'+date.getHours():date.getHours()) + ':';
    m = (date.getMinutes()<10?'0'+date.getMinutes():date.getMinutes()) + ':';
    s = date.getSeconds()<10?'0'+date.getSeconds():date.getSeconds();
    //?
    
    if(d=="ymd"){
        return Y+M+D;
    }else{
        return Y+M+D+h+m+s;
    }
}

//只允许输入两位小数点
function clearNoNum(obj){
    if(obj.value !=''&& obj.value.substr(0,1) == '.'||obj.value=='0.00'){
        obj.value="";
    }
    obj.value = obj.value.replace(/^0*(0\.|[1-9])/, '$1');
    obj.value = obj.value.replace(/[^\d.]/g,"");
    obj.value = obj.value.replace(/\.{2,}/g,".");
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
    obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数
    if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
        if(obj.value.substr(0,1) == '0' && obj.value.length == 2){
            obj.value= obj.value.substr(1,obj.value.length);
        }
    }
}
//获取参数
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}
function trim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
}

function toArray(obj) {
    var a = [],key,val;
    for(key in obj){
        val = obj[key];
        if(typeof val === "object"){
            a.push(val);
        }
    }
    return a;
}
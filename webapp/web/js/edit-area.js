//admin/position/technicianscope
    var technicianId = $('#technicianId').attr('data-id'),lon = $('#technicianId').attr('data-lon'),lat =  $('#technicianId').attr('data-lat');
    var initializeLongitude ='';//初始经纬度
    var InitialAddress = $('#technicianId').attr('data-address');//初始地址
    var type =  $('#technicianId').attr('data-type');  //type  1、获取浏览器位置    2、获取传输地理位置
    var radii = $('#technicianId').attr('data-radius');
    var prohibit = $('#technicianId').attr('data-Isban');//判断是否选中 1、可以  0、不可以'
    var lng = '',lat1 = '',radii1='';
    var keyword = '',changeState='';
    var geolocation,map, marker,circle;
    console.log(type);
    // console.log('id:'+technicianId+'lon:'+lon +'lat:'+lat+'地址:'+InitialAddress+'半径:'+radii+'选中状态：'+prohibit);
     // 初始化加载地图
    map = new AMap.Map("container2", {
        resizeEnable: true,
        zoom:14
    });
    $(function () {
       prohibit==0?$('.prohibitModification').addClass('yes-xuanzhong'):$('.prohibitModification').removeClass('yes-xuanzhong');
       // 判断当前是否有定位
       if (type==1) {

            map.plugin('AMap.Geolocation', function() {
                geolocation = new AMap.Geolocation({
                    enableHighAccuracy: true,
                    timeout: 10000,
                    buttonOffset: new AMap.Pixel(10, 20),
                    zoomToAccuracy: true, 
                    buttonPosition:'RB'
                });
                geolocation.getCurrentPosition();
                AMap.event.addListener(geolocation, 'complete', onComplete);//返回定位信息
                
                // map.setCenter
                // AMap.event.addListener(geolocation, 'error', onError);      //返回定位出错信息
            });
       }else if(type==2){
            haveData();
       }
    })
    // 解析定位结果
    function onComplete(data){
        $('#import-location').val(data.formattedAddress);
        lng = data.position.getLng();
        lat1 = data.position.getLat();
        map.setZoom(14);
        radii1=5;
        prohibit = 1;
        keyword = data.formattedAddress;
        circle.setRadius(radii1*1000);
        initializeLongitude = [lng,lat1];
        circle.setCenter(initializeLongitude);
        map.setCenter(initializeLongitude);
        marker.setPosition(initializeLongitude);
    }
    // 有数据定位的情况
    function haveData(){
        initializeLongitude = [lon,lat];
        circle.setCenter(initializeLongitude);
        marker.setPosition(initializeLongitude);
        map.setCenter(initializeLongitude);

    }
       //图标 自定义定位标记
    marker = new AMap.Marker({
        map: map,
        icon: new AMap.Icon({
            size: new AMap.Size(30, 40),  //图标大小
            image: "../images/jishi.png",
            imageOffset: new AMap.Pixel(0, 0)
        })
    });
    // 圆范围
    circle = new AMap.Circle({
        // center: initializeLongitude,
        radius: radii*1000, //半径
        strokeColor: "#F33", //线颜色
        strokeOpacity: 1, //线透明度
        strokeWeight: 3, //线粗细度
        fillColor: "#ee2200", //填充颜色
        fillOpacity: 0.35//填充透明度
    });
    circle.setMap(map);
    // 禁止修改技术范围点击方法
    $('.prohibitModification').on('click',function(){
        if(prohibit==0){
            prohibit = 1;
            changeState= prohibit;
            $(this).removeClass('yes-xuanzhong');
        }else if(prohibit==1){
            prohibit = 0;
            changeState= prohibit;
            $(this).addClass('yes-xuanzhong');

        }
    })
    // 清除搜索框内容
    $('#location-delete').on('click',function(){
        $('#import-location').val('');
        $(this).hide();
    })
    $('#import-location').keyup(function(){
        keyword = $(this).val();
        keyword.length > 5 ? $('#location-delete').show() : $('#location-delete').hide();
    })
    // 搜索框点击事件
    $('#sousuo').on('click',function() {
           AMap.service(["AMap.PlaceSearch"], function() {
            var placeSearch = new AMap.PlaceSearch({ //构造地点查询类
                pageSize: 1,
                pageIndex: 1,
                city: "010", //城市
                map: map,
            });
            //关键字查询
            placeSearch.search( keyword,function(status,result) {
                console.log(result.poiList.pois[0].location.lng);
                lng = result.poiList.pois[0].location.lng;
                lat1 = result.poiList.pois[0].location.lat;
                var zuobiao = [lng,lat1];
                marker.setPosition(zuobiao);
                circle.setCenter(zuobiao);
            });
        });
    })
// 
 
        // 输入地址
            var auto = new AMap.Autocomplete({
                input: "import-location"
            });
            AMap.event.addListener(auto, "select", select);//注册监听，当选中某条记录时会触发
            function select(e) {
                var zuobiao1= e.poi.location;
                lng =  e.poi.location.getLng();
                lat1 =  e.poi.location.getLat();
                // 搜索地址的时候获取到的坐标轴
                var zuobiao = zuobiao1.toString().split(",");
                marker.setPosition(zuobiao);
                circle.setCenter(zuobiao);
                // sendPosition(zuobiao)
                if (e.poi && e.poi.location) {
                    map.setZoom(14);
                    map.setCenter(e.poi.location);
                }
            }
        
    // 根据坐标转换位置
    AMap.plugin('AMap.Geocoder',function(){
         var geocoder = new AMap.Geocoder({
           city:"010"
        });
        // 点击坐标获取经纬度
        // var clickEventListener = map.on('click', function(e) {
                
        //        // 点击地图的时候获取到的坐标轴
        //         var didian =(e.lnglat.getLng() + ',' + e.lnglat.getLat()).split(",");
        //         lng =  e.lnglat.getLng();
        //         lat1 =  e.lnglat.getLat();
        //         map.setCenter(didian);
        //         marker.setPosition(didian);
        //         circle.setCenter(didian);
        //         changelon = [lat1,lng];
        //         geocoder.getAddress(e.lnglat, function(status, result) {
        //          if (status === 'complete' && result.info === 'OK') {
        //                 // geocoder_CallBack(result);
        //                 keyword = result.regeocode.formattedAddress;
        //                 $('#import-location').val(result.regeocode.formattedAddress);
                       
        //             }else{
        //                 layer.msg('不在范围内', {icon: 1, time: 1000});
        //             }
                
        //         });
        //     });
    });

    function geocoder_CallBack(data) {
        var address = data.regeocode.formattedAddress; //返回地址描述
        // document.getElementById("result").innerHTML = address;
    }
    // 选择公里数
    $('.revise-location-p').on('click',function() {
        $('.revise-location-gongli').show(100);
    })
    $('.revise-location-km li').on('click',function(){
         $('.revise-location-gongli').hide(100);
         radii1=$(this).text();
         circle.setRadius(radii1*1000);
        $('.receivingRange').text($(this).text());
    })
        // 未更改状态点击保存
        function d() {
            lng = (lng=='')?lon :lng;
            lat1 =(lat1=='')?lat:lat1;
            radii1 = (radii1=='')?radii:radii1;
            keyword = (keyword=='')?InitialAddress:keyword;
            changeState = (changeState=='')?prohibit:changeState;
        }
        // 点击保存提交修改内容
        $('.conservation').on('click',function() {
            d();
            if(lng != '' || lat1!= '' || radii1!=''){
              $.ajax({
                         type: "POST",
                         url: "/technician/update-area",
                         data: {technician_id:technicianId,service_address:keyword,lon:lng,lat:lat1,service_radius:radii1,is_ban:changeState}, 
                         dataType:'json',
                         success: function (data) {
                            if (data.success == true) {
                                layer.confirm(data.message, {
                                    btn: ['确定'] //按钮
                                }, function(){
                                    location.href='/technician/index';
                                });
                            }else{
                                layer.alert(data.message);
                            }
                         }
                     }); 

            }else{
                layer.msg('请修改位置', {icon: 1, time: 1000});
            }
        })

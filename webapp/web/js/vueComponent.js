Vue.component( 'ui-form-item', {
    // props: ['message','hint'],
    props    : {
        message : {
            type    : Object,
            default : function () {
                return { hint : false };
            }
        },
        hint    : String
    },
    data     : function () {
        return ({ show : false });
    },
    template : '<div class="ui-form-item" v-bind:style="{\'padding-left\':message.left,width:message.width}"><label class="ui-form-item_lable" v-bind:style="{width:message.left}" v-if="message.label!=undefined"><i v-if="message.hint">*</i>{{message.label}}</label><slot></slot><p v-if="show" class="ui-hint">{{hint}}</p></div>'
} )
Vue.component( 'ui-form', {
        props    : {
            width : String
        },
        template : '<form action="" v-bind:style="{width:width}"><slot></slot></form>',
    }
)
Vue.component( 'ui-input', {
    props    : {
        type : {
            type         : String,
            default      : 'text',
            Placeholder1 : String
        }
    },
    template : '<div class="ui-input"><input v-bind:type="type"  @blur="onBlur"></div>',
    methods  : {
        onBlur : function ( e ) {
            console.log( this.$parent.message.hint && this.$parent.message.hint == true )
            if ( this.$parent.message.hint != undefined && this.$parent.message.hint == true ) {
                this.$parent.show = !e.target.value;
            }
            
        }
    }
} )
Vue.component( 'ui-select', {
    props    : {
        value : Array
    },
    template : "<select name=\"\" id=\"\" class=\"ui-select\" @change=\"onChange\">\n\t    \t<option value=\"0\">请选择</option>\n\t    \t<option v-for=\"(item,index) in value\"  :value=\"item.id\" :index='index'>{{item.title}}</option>\n\t    </select>",
    methods  : {
        onChange : function ( e ) {
            console.log( e.target.value )
        }
    }
} )
Vue.component( 'ui-button', {
    // props:['message'],
    props    : {
        message : {
            type    : Object,
            default : function () {
                return ({ title : '确定' })
            }
        },
        
    },
    data     : function () {
        return ({ btn : 'ui-btn' });
    },
    template :
        '<button :class="[btn,message.class]">{{message.title}}</button>'
} )
Vue.component( 'ui-col', {
    props    : {
        Class1 : String,
        show   : false
    },
    template : "<div :class=\"Class1\"><slot></slot><div class=\"justify-fix\" v-if=\"show\"></div></div>"
} )


;(function ( Vue, factory ) {
    if ( typeof Vue === "function" ) factory( Vue );
}( Vue, function ( Vue ) {
    var isSense = Vue.$$common.isSense;
    var paramReg = /([^?=&]+)=([^?=&]+)/ig;
    /**
     * 获取指定的url参数
     * @param name
     * @returns {*}
     */
    Vue.prototype.$$getQueryString = function getQueryString( name ) {
        var reg = new RegExp( "(^|&)" + name + "=([^&]*)(&|$)", "i" );
        var r = window.location.search.substr( 1 ).match( reg );
        if ( r != null ) return decodeURIComponent( r[2] );
        return "";
    };
    
    /**
     * 序列化所有url参数
     */
    Vue.prototype.$$mapUrlParam = function () {
        var fullUrl = decodeURIComponent( window.location.search );
        var result = {};
        if ( isSense( fullUrl ) ) {
            fullUrl.replace( paramReg, function ( _, key, val ) {
                var get = result[key];
                result[key] = isSense( get ) ? [].concat( get, val ) : val;
            } );
        }
        return result;
    };
    
} ));
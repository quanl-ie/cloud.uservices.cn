new Vue({
    el: ".panel-body",
    data: {
        cur: 1,//当前页
        all: 0,//全部页
        status: 0,//状态
        state:{},
        search: {
            work_no: '',//工单号
            tec_name: '',//技师名称
            tec_mobile: '',//技师电话
            service_type: '',//服务类型
            start_fee_time: '',//开始日期
            end_fee_time:'', //结束时间
            status:0,
            page:1
        },
        showCollectGoods:false,
        name: '',
        dataList: [],
        source:{
            typeList:[],
        },
        remark:{
            work_no:'',
            remark:''
        }
    },
    directives: {
        InitProp: Vue.$$dirs.initProp
    },
    methods: {
        listen: function (data) {
            this.cur = data;
            this.search.page = data;
            this.getData();
        },
        getData: function (e) {
            var _that = this;
            vpost('/api/fee/index',this.search,function (res) {
                _that.dataList = res.list;
                _that.all = res.totalPage;
                _that.source.typeList = res.type_list;
            },function (json) {
                alert(json.message);
            });
        },
        tabGetdata: function (e) {
            this.status = e.target.value;
            this.search.status = e.target.value;
            this.getData();
        },
        //显示确认收货
        showRemarkEvent:function (work_no,remark) {
            this.showCollectGoods = true;
            this.remark.work_no = work_no;
            this.remark.remark = remark;
        },
        //修改备注
        editRemarkEvent:function (id) {
            var _this = this;
            this.showCollectGoods = true;

            if(this.remark.work_no == ''){
                alert("工单号不能为空");
                return false;
            }
            if(this.remark.remark == ''){
                alert("备注不能为空");
                return false;
            }

            vpost('/api/fee/edit-remark',this.remark,function (json) {
                alert("操作成功", function(){
                    _this.getData();
                    _this.hideCollectGoodsEvent();
                });
            })
        },
        //隐藏确认收货
        hideCollectGoodsEvent: function (e) {
            this.showCollectGoods = false;
        },
        searchEvent:function(){
            this.getData();
        },
        changeDate: function(e){
            this.search[e.name]=e.time
        },
        confirmFee: function (work_no) {
            _this = this;
            confirm("请确认本次服务已收费，是否继续？", function () {
                vpost('/api/fee/confirm', {work_no:work_no},function (json) {
                    alert("操作成功", function(){
                        _this.getData();
                    });
                })
            });
        }
    },
    mounted: function (argument) {
        this.getData();
    }
})
var that= new Vue({
	el:".panel-body",
	data:{
		apll_detail:[],
		invoiceData:{//发货单模块数据
			send_no:'',//发货单号
			stock_no:'',//关联出库单
			send_title:'',//发货主题
			type:'',//发货类型
			method:'',//发货方式
			apply_name:'',
			create_time:'',//发货时间
			remark:'',//备注
		},
		status:'',//状态
		province:[],
		city:[],
		district:[],
		consigneeData:{//收货人信息
			name:'',//收货人信息
			mobile:'',//收货人手机
			address:'',//收货人地址
		},
		takeData:{//提货人信息
			name:'',//提货人姓名
			card:'',//身份证
			mobile:'',//手机
			license:'',//车牌
		},
		apllDetail:{//更新物流信息
			id:'',
			update_time:'',
			content:''
		},
		recInfo:{//更新收货人信息
			id:'',
			rec_name:'',
			rec_mobile:'',
			province_id:'',
			city_id:'',
			district_id:'',
			rec_address:''
		},
		pickInfo:{//更新提货人信息
			id: '', //发货单id
		    pick_name: '', //提货人姓名  
		    pick_mobile:'',//手机号
		    pick_card: '',//身份证号
		    pick_license:'' //车牌号

		},
		confirmCollectGoodsData:{
			send_id:'',
			rec_name:''

		},  
		product:[],
		dataobj:'',
		apllDetailCont:false,
		show1:false,
		show2:false,
		show3:false,
		show4:false,
		date:''
	},
	methods:{
		getData:function() {
			var url='/api/send-out/detail';
			var _that = this;
			var sendId = getQueryString("id");
			vpost(url, {send_id:sendId}, function ( json ) {
				var data = json.data.data;
				var initApll = {
					content:'产品已发货',
					update_time: data.create_time
				}
				_that.apll_detail = data.apll_detail
				_that.apll_detail.push(initApll);
				_that.invoiceData = {
					send_no:data.send_no,//发货单号
					stock_no:data.stock_no,//关联出库单
					send_title:data.send_title,//发货主题
					type:data.send_type_desc,//发货类型
					method:data.send_method_desc,//发货方式
					apply_name:data.apply_name,
					create_time:data.create_time,//发货时间
					remark:data.remark,//备注

				}
				_that.dataobj = data;
				_that.status=data.send_status
				_that.apllDetail.id = data.id;
				_that.province = data.province;
				_that.recInfo={//更新收货人信息
					id:data.id,
					rec_name:data.rec_name,
					rec_mobile:data.rec_mobile,
					province_id:data.province_id,
					city_id:data.city_id,
					district_id:data.district_id,
					rec_address: data.rec_address
				};
				_that.pickInfo={
					id: data.id, //发货单id
				    pick_name: data.pick_name, //提货人姓名  
				    pick_mobile:data.pick_mobile,//手机号
				    pick_card:data.pick_card,//身份证号
				    pick_license:data.pick_license //车牌号
				}
				_that.consigneeData = {
					name:data.rec_name,//收货人信息
					mobile:data.rec_mobile,//收货人手机
					address:data.rec_address,//收货人地址
				}
				_that.confirmCollectGoodsData = {
					send_id:data.id,
					rec_name:data.rec_name

				}
				_that.takeData = {
					name:data.pick_name,//提货人姓名
					card:data.pick_card,//身份证
					mobile:data.pick_mobile,//手机
					license:data.pick_license,//车牌
				}
				_that.product = data.product;

            } );
		},
		edit:function(e){
			e=='show1'?this.date = new Date().getTime():'';
			this[e]=this[e]==false?true:false;
		},
		submit:function(e){
			// /api/send-out/edit-rec-info 
			var _that = this;
			var url = e=='show1'?'/api/send-out/add-apll-detail':e=='show2'?'/api/send-out/edit-rec-info':'/api/send-out/edit-pick-info';
			var postData =  e=='show1'? this.apllDetail:e=='show2'?this.recInfo:this.pickInfo;
			if(e=='show1'){
				if(this.apllDetail.content==''){
					this.apllDetailCont=true;
					return false;
				}
			}
			vpost(url,{'postData':postData}, function ( json ) {
				_that[e]=false;
				_that.getData();
                _that.apllDetail.content = '';
			})
			// console.log(this.apllDetail);
		},
		confirmCollectGoodsEvent:function (id) {
            var _this = this;
            // this.showCollectGoods = true;

            if(this.confirmCollectGoodsData.rec_name == ''){
                alert("收货人姓名不能为空");
                return false;
            }

            vpost('/api/send-out/confirm',this.confirmCollectGoodsData,function (json) {
                alert("操作成功", function(){
                    _this.getData();
                    _this.show4=false;
                });
            })
        },
		layershow:function(name){
			this[name] = false;
		},
		changeDate: function ( date ) {
            this.apllDetail.update_time = date;
        },
	},
	watch:{

		// 省
        "recInfo.province_id" : function ( pro ) {
            axios.post( "/api/send-out/get-city", { province_id : pro } )
                .then( function ( list ) {
                	that.city = list.data.data; 
                } );
        },
        // 市
        "recInfo.city_id"     : function ( city ) {
            axios.post( "/api/send-out/get-district", { city_id : city } )
                .then( function ( list ) {
                	that.district = list.data.data 
                } );
        },
        "apllDetail.content":function(pro){
        	pro!=''?this.apllDetailCont=false:'';
        }
	},
	mounted:function() {
		this.getData();
	}
})

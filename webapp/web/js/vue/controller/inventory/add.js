;(function ( factory ) {
    if ( window && window.window === window && window.Vue ) new Vue( factory( window, window.Vue ) );
}( function ( win, Vue, _undefined ) {
    
    function toArray( obj ) {
        var a = [], key, val;
        for ( key in obj ) {
            val = obj[key];
            if ( typeof val === "object" ) {
                a.push( val );
            }
        }
        return a;
    }
    
    function each( list, fn ) {
        for ( var item, children, i = 0, l = list.length; i < l; i++ ) {
            item = list[i];
            children = item.children;
            fn( item );
            if ( !!children && children.length ) each( children, fn );
        }
    }
    
    var options = {
        el        : "#app",
        validator : true,
        data      : {
            page       : {// 分页参数
                pageNo    : 1,
                pageSize  : 10,
                total     : 0,
                totalPage : 0
            },
            source     : {
                type    : null,//盘点类型
                depot   : null,//库房
                proType : null
            },
            entity     : {
                odd_num    : "",
                subject    : "",
                type       : "",
                date       : null,
                begin_time : "",
                end_time   : "",
                depot_id   : "",
                class_ids  : [],
                remark     : "",
                userName   : "",
                time       : ""
            },
            products   : '',
            flag       : 0,//禁止重复提交
            disableBtn : false,
        },
        mounted   : function () {
            var that = this;
            this.initDataFunc();
            // 初始加载一级产品类目
            this.getgoodsCategory( function ( list ) {
                var root = { class_id : -1, title : "全部", exclude : true };
                root.children = list;
                that.source.proType = [root];
                that.$refs.treeSelect.$$selectAll();
            } );
        },
        methods   : {
            // 格式化类目选择文本显示
            formatText       : function ( text, isAllSelect ) {
                return isAllSelect ? '全部类目' : text;
            },
            //产品类目初始加载
            getgoodsCategory : function ( fn ) {
                vpost( "/common/class-list", null, function ( res ) {
                    fn( toArray( res ) );
                } )
            },
            // 是否显示下级箭头
            isHasChildren    : function ( data ) {
                return !!data.children && data.children.length > 0;
            },
            //初始化显示
            initDataFunc     : function () {
                var that = this, source = this.source, entity = that.entity;
                vpost( '/api/inventory/add-option', {}, function ( res ) {
                    entity.odd_num = res.oddNum;
                    source.type = res.typeArr;
                    source.type.unshift( { id : 0, title : "请选择盘点类型" } )
                    source.depot = res.depotArr;
                    entity.userName = res.userName;
                    entity.time = res.time;
                } );
            },
            //验证盘点主题
            validateTitle    : function ( value, callback ) {
                if ( value === '' ) {
                    callback( false, "请输入盘点主题" )
                } else if ( value.length > 32 ) {
                    callback( false, "盘点主题不能超过32个字符" )
                } else {
                    callback( true )
                }
            },
            // 产品明细
            getProducts      : function ( pageNo ) {
                var entity = this.entity, page = this.page, that = this;
                vpost( '/api/inventory/prod-detail', {
                    page      : pageNo,
                    class_ids : entity.class_ids,
                    depot_id  : entity.depot_id
                }, function ( res ) {
                    page.pageNo = res.page;
                    page.totalPage = res.totalPage;
                    page.total = res.totalCount;
                    that.products = res.list;
                } );
            },
            // 翻页
            changePage       : function ( pageNo ) {
                this.getProducts( pageNo );
            },
            // 提交
            onSubmit         : function () {
                var that = this, $$u = this.$$u;
                this.disableBtn = true;
                this.$$runValid( "add", function () {
                    var date,
                        entity = $$u.deepCopy( that.entity ),
                        type = entity.type;
                    if ( entity.date ) {
                        date = entity.date || [];
                        entity.begin_time = date[0];
                        entity.end_time = date[1];
                    }
                    delete entity.date;
                    if ( type === 0 ) {
                        entity.type = "";
                    }
                    if(that.products.length==0){
                        alert("产品明细不能为空！")
                        that.disableBtn = false;
                        return false;
                    }
                    vpost( '/api/inventory/add', entity, function ( res ) {
                        that.disableBtn = false;
                        alert( "操作成功！", function () {
                            document.location.replace( '/inventory/index' );
                        } )
                    }, function () {
                        that.disableBtn = false;
                    } );
                }, function () {
                    that.disableBtn = false;
                } );
            },
            
            //获取产品明细
            handleProdDetail : function ( data ) {
                var params = {
                    depot_id : data - '',
                    page     : this.page.pageNo,
                };
                var that = this;
                vpost( '/api/inventory/prod-detail', params, function ( res ) {
                    that.products = res.list;
                    that.page.totalPage = res.totalPage;
                    that.page.total = res.totalCount;
                } )
            }
        }
    };
    return options;
} ));
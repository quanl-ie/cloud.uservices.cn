;(function ( factory ) {
    if ( window && window.window === window && window.Vue ) new Vue( factory( window, window.Vue ) );
}( function ( win, Vue, _undefined ) {
    var options = {
        el      : "#app",
        data    : {
            id:getQueryString("id") ? getQueryString("id") : '',
            status:getQueryString("status") ? getQueryString("status") : '',
            page : {// 分页参数
                pageNo    : 1,
                pageSize  : 50,
                total     : 0,
                totalPage : 0
            },
            detailInfo:{
                baseInfo:{},
                prodList:{}
            },
        },
        methods : {
            getData:function(){
                var that = this;
                var postData  = {
                    id:this.id,
                    page:this.page.pageNo,
                    pageSize:this.page.pageSize
                };
                vpost( '/api/inventory/view', postData, function ( res ) {
                    that.detailInfo.baseInfo = res.baseInfo;
                    that.detailInfo.prodList = res.prodList.list;
                    that.page.total = res.prodList.totalCount;
                    that.page.totalPage = res.prodList.totalPage;
                } );
            },

            //分页
            changePage:function(pageNo){
                this.page.pageNo = pageNo;
                this.getData()
            },

            //下载盘点单
            handleExport:function(){
                document.location.href = this.detailInfo.baseInfo.downloadUrl;
            },

            //返回列表页
            handleToList(){
                document.location.href='/inventory/index';
            }
        },
        mounted(){
            this.getData()
        }
    };
    return options;
} ));
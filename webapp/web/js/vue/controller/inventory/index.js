;(function ( factory ) {
    if ( window && window.window === window && window.Vue ) new Vue( factory( window, window.Vue ) );
}( function ( win, Vue, _undefined ) {
    var $$u = Vue.$$common;
    var options = {
        el      : "#app",
        data    : {
            page   : {// 分页参数
                pageNo    : 1,
                pageSize  : 10,
                total     : 0,
                totalPage : 0
            },
            source : {
                status : null,
                type   : null
            },
            search : {
                date       : [],
                subject    : "",//盘点主题
                type       : "",//盘点类型
                begin_time : "",//开始时间
                end_time   : "",//结束时间
                warehouse  : "",//库房名称
                status     : ""//盘点状态
            },
            searchType:0,
            list   : null
        },
        created() {
            var that = this, source = that.source;
            vpost( '/api/inventory/search-option', {}, function ( res ) {
                source.status = res.statusArr;
                source.type = res.typeArr;
            } );
            this.getData( 1 );
        },
        methods : {

            // 翻页事件
            changePage( pageNo ) {
                var search  =this.search;
                if(this.searchType==0){
                    this.getData( pageNo,{ subject : this.search.subject } )
                }else{
                    this.getData( pageNo,search )
                }
            },

            // 获取列表数据
            getData( pageNo, param ) {
                var that = this, page = that.page;
                vpost( '/api/inventory/list', $$u.extend( {
                    page     : pageNo,
                    pageSize : page.pageSize
                }, param ), function ( res ) {
                    page.pageNo = res.page;
                    page.totalPage = res.totalPage;
                    page.total = res.totalCount;
                    that.list = res.list;
                } );
            },

            //高级搜索
            advSearch( close ) {
                this.searchType = 1;
                var search = this.search;
                var date = search.date || [];
                search.begin_time = date[0];
                search.end_time = date[1];
                this.getData( 1, search );
                close();
            },

            //基础搜索
            inputSearch( close ) {
                this.searchType = 0;
                this.getData( 1, { subject : this.search.subject } )
            },

            //创建盘点单
            handleCreate(){
                document.location.href = document.location.href.split('index')[0]+'add';
            }
        }
    };
    return options;
} ));
;(function ( factory ) {
    if ( window && window.window === window && window.Vue ) new Vue( factory( window, window.Vue ) );
}( function ( win, Vue, _undefined ) {
    var options = {
        el      : "#app",
        data    : {
            page : {// 分页参数
                pageNo    : 1,
                pageSize  : 50,
                total     : 0,
                totalPage : 0
            },
            id:getQueryString("id") ? getQueryString("id") : '',
            status:getQueryString("status") ? getQueryString("status") : '',
            detailInfo:{
                baseInfo:{},
                prodList:{}
            },
            flagCancel:0,
            disableCancelBtn:false,
            flagComplete:0,
            disableCompleteBtn:false
        },
        methods : {
            //初始化
            getData:function(){
                var that = this;
                var postData  = {
                    id:this.id,
                    page:this.page.pageNo,
                    pageSize:this.page.pageSize
                };
                vpost( '/api/inventory/view', postData, function ( res ) {
                    that.detailInfo.baseInfo = res.baseInfo;
                    that.detailInfo.prodList = res.prodList.list;
                    that.page.total = res.prodList.totalCount;
                    that.page.totalPage = res.prodList.totalPage;
                });
            },

            //分页
            changePage:function( pageNo ) {
                this.page.pageNo = pageNo;
                this.getData();
            },

            //作废||结束
            changeStatusFunc:function(type){
                var params = {
                    id:this.id,
                    type:type,
                    remark:this.detailInfo.baseInfo.remark
                };
                var that = this;
                if(type==1){
                    if(that.flagComplete==0){
                        that.flagComplete++;
                        that.disableCompleteBtn = true;
                        for(var i=0;i<that.detailInfo.prodList.length;i++){
                            if(that.detailInfo.prodList[i].real_num==''){
                                alert("请将实盘数量填写完整再结束盘点！");
                                return false;
                            }
                        }

                        setTimeout(function (){
                            that.flagComplete = 0;
                            that.disableCompleteBtn = false;
                        },500)
                        confirm("确认盘点完毕后，盘点数据将不能修改",function (){
                            vpost('/api/inventory/change-status',params,function(res){
                                alert("操作成功！",function(){
                                    document.location.href='/inventory/view?id='+that.id+'&status='+2;
                                });
                            })
                        })
                    }else{
                        that.flagComplete=0;
                        that.disableCompleteBtn = false;
                        return false;
                    }
                }else{
                    if(that.flagCancel==0){
                        that.flagCancel++;
                        that.disableCancelBtn = true;
                        setTimeout(function(){
                            that.flagCancel = 0;
                            that.disableCancelBtn = false;
                        },500)
                        confirm("盘点单作废后，盘点数据将无效",function (){
                            vpost('/api/inventory/change-status',params,function(res){
                                alert("操作成功！",function(){
                                    document.location.href='/inventory/view?id='+that.id+'&status='+3;
                                });
                            })
                        })
                    }else{
                        that.flagCancel = 0;
                        that.disableCancelBtn = false;
                        return false;
                    }
                }
            },

            //验证实盘数量
            handleValidateRealNum:function(data){
                var reg = /^\d+(\.\d{1,2})?$/;
                if(data.real_num===''){
                    alert("请输入实盘数量");
                }else if(!reg.test(data.real_num)){
                    alert("只能输入正整数或者小数点后保留两位的小数！");
                    data.real_num = data.num;
                }else{
                    var params = {
                        id:data.id,
                        real_num:data.real_num,
                        remark:data.remark,
                    }
                    vpost('/api/inventory/pandian',params,function (res) {
                        data.username = res.username;
                        data.calc_num = res.calcNum;
                    },function(err){
                        alert(err.message);
                    })
                }
            },

            //验证备注
            handleValidateRemark:function(data){
                var params = {
                    id:data.id,
                    real_num:data.real_num,
                    remark:data.remark,
                }
                vpost('/api/inventory/pandian',params,function (res) {
                    data.username = res.username;
                    data.calc_num = res.calcNum;
                },function(err){
                    alert(err.message);
                })
            },

            //下载盘点单
            handleExport:function(){
                document.location.href = this.detailInfo.baseInfo.downloadUrl;
            },

            //返回列表页
            handleToList(){
                document.location.href='/inventory/index';
            }

        },
        mounted(){
            this.getData()
        }
    };
    return options;
} ));
;(function (factory) {
    if (this && this.window === window && Vue) new Vue(factory(this, this.Vue));
}(function (win, Vue, _undefined) {
    var options = {
        el: "#app",
        data: {
            delStatus: false,
            closeStatus:false,
            delId:'',
            status: 1,//退货状态
            searchType : 0,
            search: {
                no: '',//退货单号
                subject: '',//退货主题
                create_time: '',//创建时间
                input : '' //基础搜索
            },
            source: {
                states: ["待审批", "执行中", '执行完毕', "审批未通过", "已关闭"]
            },
            dataList: [],
            state: {
                loading: false
            },
            page: {// 分页参数
                pageNo: 1,
                pageSize: 10,
                total: 25,
                totalPage: 3
            }
        },
        methods: {
            // 翻页事件
            changePage: function (pageNo) {
                //this.page.pageNo = pageNo;
                // 开始请求数据
                this.getData(pageNo);
            },
            getData: function (page) {
                var status = this.status;
                var no = this.search.no;
                var input = this.search.input;
                var subject = this.search.subject;
                var create_time = this.search.create_time;
                var start_create_time = create_time;
                var end_create_time = create_time;
                var page = page;
                var pagesize = this.page.pageSize;
                var _that = this;
                if(this.searchType == 1){
                    var postData =  {
                        'status': status,
                        'no': no,
                        'subject': subject,
                        'start_create_time': start_create_time,
                        'end_create_time': end_create_time,
                        'page': page,
                        'pagesize': pagesize
                    }
                }else{
                    var postData =  {
                        'no': input,
                        'status': status,
                        'page': page,
                        'pagesize': pagesize
                    }
                }
                vpost('/api/return-purchase/index',postData, function (res) {
                    _that.dataList = res.list;
                    _that.page.pageNo = res.page;
                    _that.page.total = res.totalCount;
                    _that.page.totalPage = res.totalPage;

                });
            },
            // tab选项卡选择事件
            tabChange: function (value) {
                this.status = value + 1;
                this.getData(1);
                //console.log("tab选项卡：" + (value+1));
            },
            //高级搜索
            searchData: function (close) {
                this.searchType = 1;
                this.getData(1);
                close();
            },
            //基础搜索
            inputData: function (close) {
                this.searchType = 0;
                this.getData(1);
                close();
            },
            //删除弹窗
            showDeleteEvent:function (id) {
                this.delStatus = true;
                this.delId = id;
            },
            deleteEvent : function () {
                var id = this.delId;
                vpost('/api/return-purchase/delete', {
                    'id': id
                }, function (res) {
                    alert("删除成功",function () {
                        document.location.replace('/return-purchase/index');
                    })
                });
            },
            //删除弹窗
            showCloseEvent:function (id) {
                this.closeStatus = true;
                this.delId = id;
            },
            closeEvent : function () {
                var id = this.delId;
                vpost('/api/return-purchase/close', {
                    'id': id
                }, function (res) {
                    alert("操作成功",function () {
                        document.location.replace('/return-purchase/index');
                    })
                });
            }
        }
    };
    return options;
}));
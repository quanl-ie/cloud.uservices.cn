;(function ( factory ) {
    if ( this && this.window === window && Vue ) new Vue( factory( this, this.Vue ) );
}( function ( win, Vue, _undefined ) {
    return {
        el   : "#app",
        data : {
            id : getQueryString("id") ? getQueryString("id") : '',
            no : "",
            subject : "",
            purchase_id : "",
            purchase_no : "",
            return_date : "",
            status  : "",
            status_desc : "",
            return_reason   : "",
            create_time     : "",
            description     : "",
            create_user_name: "",
            audit_status :"",
            audit_suggest   : "",
            closeShow :false,
            reasonShow : false,
            reason     : "",
            throwShow :  false,
            source  : {
                products : [],
                stock: [],
                send: []
            }
        },
        created: function () {
            if (this.id) {
                this.getData(this.id);
            }
        },
        methods: {
            getData : function (id) {
                var _that = this;
                vpost('/api/return-purchase/view', {'id': id}, function (res) {
                    _that.no        = res.info.no;
                    _that.subject   = res.info.subject;
                    _that.purchase_id = res.info.purchase_id;
                    _that.purchase_no = res.info.purchase_no;
                    _that.return_date = res.info.return_date;
                    _that.status       = res.info.status;
                    _that.status_desc       = res.info.status_desc;
                    _that.return_reason = res.info.return_reason;
                    _that.create_time = res.info.create_time;
                    _that.description = res.info.description;
                    _that.audit_status = res.info.audit_status;
                    _that.audit_suggest = res.info.audit_suggest;
                    _that.create_user_name = res.info.create_user_name;
                    _that.source.products = res.product;
                    _that.source.stock = res.stock;
                    _that.source.send  = res.send;
                }, function (res) {
                    alert(res.message, function () {
                        document.location.replace('/return-purchase/index');
                    });
                });
            },
            auditShowEvent : function (status) {
              if(status == 1){
                  this.throwShow = true;
              }
              if(status == 2){
                  this.reasonShow = true;
              }
            },
            auditEvent :function (status,close) {
                var _that = this;
                var postData = {
                    'id':_that.id,
                    'auditStatus': status,
                    'auditSuggest' : _that.reason
                };
                vpost('/api/return-purchase/check-option', postData, function (res) {
                    alert('审批成功',function () {
                        _that.getData(_that.id);
                        close();
                    });
                },function (msg) {
                    alert(msg.message);
                })
            },
            //出库
            stockOutEvent :function(){
                document.location.replace('/stock-out/add?rel_type=7&rel_id='+ this.id);
            },
            //关闭弹窗
            showCloseEvent:function () {
                this.closeShow = true;
            },
            closeEvent : function (close) {
                var id = this.id;
                var _that = this;
                vpost('/api/return-purchase/close', {
                    'id': id
                }, function (res) {
                    alert("操作成功",function () {
                        _that.getData(_that.id);
                        close();
                    })
                });
            },
            //返回列表
            returnEvent :function () {
                document.location.replace('/return-purchase/index');
            }
        }
    };
} ));
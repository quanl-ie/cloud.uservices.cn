;(function ( factory ) {
    var win = this, Vue = this.Vue;
    if ( win && win.window === window && Vue ) new Vue( factory( win, Vue ) );
}( function ( win, Vue, _undefined ) {
    return {
        el        : "#app",
        validator : {
            rules : {}
        },
        data      : {
            theme       : '',
            cause       : '',
            purchase_id : '',
            purchase_no : '',
            date        : null,
            remarks     : null,
            id          : getQueryString( "id" ) ? getQueryString( "id" ) : '', //采购单id
            return_id   : getQueryString( "return_id" ) ? getQueryString( "return_id" ) : '', //退货单如不为空 为修改
            source      : {
                products       : [],
                returnProducts : {}
            }
            
        },
        created   : function () {
            if ( this.return_id ) {
                this.edit( this.return_id );
            } else {
                this.beforeAdd( this.id );
            }
        },
        methods   : {
            createValid     : function ( data ) {
                var max = parseFloat( data.max );
                return function ( val, check ) {
                    if ( !check( parseFloat( val ) <= max ) ) {
                        alert( "剩余可退货产品数不足！", function () {
                            return false;
                        } )
                    }
                }
            },
            productsHandler : function ( products ) {
                return products.map( function ( item ) {
                    item.max = item.finish_num;
                    return item;
                } );
            },
            back            : function () {
                document.location.replace( '/return-purchase/index' );
            },
            onSub           : function () {
                var _data = this;
                this.$$runValid( 'create', function () {
                    console.log( 'done' );
                    var table = _data.$refs.table;
                    var postData = table.$$getSelection();
                    if ( postData.length < 1 ) {
                        alert( "请选择退货产品" );
                        return false;
                    }
                    vpost( '/api/return-purchase/add', {
                            'return_id'       : _data.return_id,
                            'subject'         : _data.theme,
                            'purchase_id'     : _data.purchase_id,
                            'apply_time'      : _data.date,
                            'reason'          : _data.cause,
                            'desc'            : _data.remarks,
                            'purchase_detail' : postData
                        },
                        function ( res ) {
                            alert( "操作成功！", function () {
                                document.location.replace( '/return-purchase/index' );
                            } );
                        }, function () {
                            alert( "操作失败！" );
                        } );
                }, function () {
                    console.log( "fail" );
                } )
            },
            edit            : function ( id ) {
                var _edit = this;
                vpost( '/api/return-purchase/before-edit', { 'return_id' : id }, function ( res ) {
                    _edit.theme = res.info.subject;
                    _edit.purchase_id = res.info.purchase_id;
                    _edit.purchase_no = res.info.purchase_no;
                    _edit.date = res.info.return_date;
                    _edit.cause = res.info.return_reason;
                    _edit.remarks = res.info.description;
                    _edit.source.products = _edit.productsHandler( res.purchaseDetail );
                }, function ( res ) {
                    alert( res.message, function () {
                        document.location.replace( '/return-purchase/index' );
                    } );
                } );
            },
            beforeAdd       : function ( id ) {
                var _that = this;
                vpost( '/api/return-purchase/before-add', { 'id' : id }, function ( res ) {
                    _that.purchase_id = res.purchase_id;
                    _that.purchase_no = res.purchase_no;
                    _that._list = _that.source.products = _that.productsHandler( res.purchaseDetail );
                }, function ( res ) {
                    alert( res.message, function () {
                        document.location.replace( '/return-purchase/index' );
                    } );
                } );
            }
        }
    };
} ));
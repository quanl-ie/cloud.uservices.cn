;(function (factory) {
    if (this && this.window === window && Vue) new Vue(factory(this, this.Vue));
}(function (win, Vue, _undefined) {
    var options = {
        el: "#app",
        data: {
            searchType : 0,
            search: {
                date:'',//日期
                company:'',//机构
                serviceType:'',//服务类型
                brandId:'',//品牌
            },
            dataList: [],
            brandList:[],//品牌
            serviceTypeList:[],//服务类型
            companyList:[],//机构
            pageParams: {// 分页参数
                pageNo: 1,
                pageSize: 10,
                total: 0,
                totalPage: 0
            }
        },
        mounted: function () {
            this.getData();
        },
        methods: {
            changePage(){

            },
            getData: function () {
                var postData;
                if(this.searchType == 1){
                    postData = {
                        'date':this.search.date,
                        'company':this.search.company,
                        'serviceType':this.search.serviceType,
                        'brand':this.search.brandId,
                        'page': this.pageParams.pageNo,
                        'pagesize': this.pageParams.pageSize
                    }
                }else{
                    postData = {
                        'page': this.pageParams.pageNo,
                        'pagesize': this.pageParams.pageSize
                    }
                }
                // vpost('/api/technician-appraisal/index',postData, function (res) {
                //     this.dataList = res.list;
                // });
            },
            // tab选项卡选择事件
            tabChange: function (value) {
                this.status = value+1;
                this.getData();
            },
            //高级搜索
            searchData: function (close) {
                this.searchType = 1;
                this.getData(1);
                close();
            },
            //基础搜索
            inputData: function (close) {
                this.searchType = 0;
                this.getData(1);
                close();
            },
            exportBtn:function () {
                document.location.href = '/technician-appraisal/import?technician_name='+this.search.name+'&month='+this.search.month;
            }
        }
    };
    return options;
}));
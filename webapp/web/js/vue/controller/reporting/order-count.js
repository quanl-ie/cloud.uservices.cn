;(function (factory) {
    if (this && this.window === window && Vue) new Vue(factory(this, this.Vue));
}(function (win, Vue, _undefined) {
    var options = {
        el: "#app",
        data: {
            searchType : 0,
            search: {
                date:'',//日期
                company:'',//机构
                serviceType:'',//服务类型
            },
            dataList: [],
            sumData:{
                new_work_sum:0,
                finish_work_sum:0,
                cancel_work_sum:0
            },//总计
            companyList:[],//机构信息
            serviceTypeList:[],//服务类型
            pageParams: {// 分页参数
                pageNo: 1,
                pageSize: 10,
                total: 0,
                totalPage: 0
            },
            nodataVisible:false,
            haveDataVisible:false,
        },
        methods: {
            changePage:function(pageNo){
                this.pageParams.pageNo = pageNo;
                this.getData()
            },
            getData:function() {
                var postData;
                var _that =this;
                if(this.searchType == 1){
                    postData = {
                        'date':this.search.date?this.search.date:undefined,
                        'company_id':(this.search.company&&this.search.company!='0')?this.search.company:undefined,
                        'work_type':(this.search.serviceType&&this.search.serviceType!='0')?this.search.serviceType:undefined,
                        'page': this.pageParams.pageNo,
                        'pageSize': this.pageParams.pageSize
                    }
                }else{
                    postData = {
                        'page': this.pageParams.pageNo,
                        'pageSize': this.pageParams.pageSize
                    }
                }
                this.sumData.new_work_sum = 0;
                this.sumData.finish_work_sum = 0;
                this.sumData.cancel_work_sum = 0;
                vpost('/api/reporting/company-order',postData, function (res) {
                    _that.dataList = res.list;
                    _that.pageParams.totalPage = res.totalPage;
                    _that.pageParams.total = res.totalCount;
                    if( _that.pageParams.total>0){
                        _that.nodataVisible = false;
                        _that.haveDataVisible = true;
                    } else{
                        _that.nodataVisible = true;
                        _that.haveDataVisible = false;
                    }
                    for(var i=0;i<_that.dataList.length;i++){
                        _that.sumData.new_work_sum+=_that.dataList[i].new_work_num-'';
                        _that.sumData.finish_work_sum+=_that.dataList[i].finish_work_num-'';
                        _that.sumData.cancel_work_sum+=_that.dataList[i].cancel_work_num-'';
                    }
                });
            },

            //获取机构
            getCompanyFunc:function(){
                var that = this;
                vpost('/api/reporting/department-list',{},function(res){
                    that.companyList = res;
                })
            },

            //获取服务类型
            getServiceTypeFunc:function(){
                var that = this;
                vpost('/api/reporting/server-type-list',{},function(res){
                    that.serviceTypeList = res;
                })
            },

            //高级搜索
            searchData:function(close) {
                this.searchType = 1;
                this.getData();
                close();
            },

            //导出
            exportBtn:function() {
                document.location.href = ' /reporting/company-order-excel?date='+this.search.date+'&company_id='+this.search.company+'&work_type='+this.search.serviceType;
            }
        },
        mounted: function () {
            this.getData();
            this.getCompanyFunc();
            this.getServiceTypeFunc()
        },
    };
    return options;
}));
;(function (factory) {
    if (this && this.window === window && Vue) new Vue(factory(this, this.Vue));
}(function (win, Vue, _undefined) {
    var options = {
        el: "#app",
        data: {
            searchType : 0,
            search: {
                date:'',//日期
                company:'',//机构
                serviceType:'',//服务类型
                technicianInfo:'',//技师姓名或电话
            },
            dataList: [],
            serviceTypeList:[],//服务类型信息
            companyList:[],//机构信息
            pageParams: {// 分页参数
                pageNo: 1,
                pageSize: 10,
                total: 0,
                totalPage: 0
            },
            nodataVisible:false,//无数据
            haveDataVisible:false,//有数据
        },
        methods: {
            changePage:function(pageNo){
                this.pageParams.pageNo = pageNo;
                this.getData();
            },

            //初始化
            getData:function() {
                var postData;
                var that = this;
                if(this.searchType == 1){
                    postData = {
                        'date':this.search.date?this.search.date:undefined,
                        // 'technicianInfo':this.search.technicianInfo,
                        'company_id':(this.search.company&&this.search.company!='0')?this.search.company:undefined,
                        'work_type':(this.search.serviceType&&this.search.serviceType!='0')?this.search.serviceType:undefined,
                        'page': this.pageParams.pageNo,
                        'pageSize': this.pageParams.pageSize
                    }
                }else{
                    postData = {
                        'name':this.search.technicianInfo,
                        'page': this.pageParams.pageNo,
                        'pageSize': this.pageParams.pageSize
                    }
                }
                vpost('/api/reporting/technician-order',postData, function (res) {
                    that.dataList = res.list;
                    that.pageParams.total = res.totalCount;
                    that.pageParams.totalPage = res.totalPage;
                    if(res.totalPage>0){
                        that.haveDataVisible = true;
                        that.nodataVisible = false;
                    }else{
                        that.haveDataVisible = false;
                        that.nodataVisible = true;
                    }
                });
            },

            //获取机构
            getCompanyFunc:function(){
                var that = this;
                vpost('/api/reporting/department-list',{},function(res){
                    that.companyList = res;
                })
            },

            //获取服务类型
            getServiceTypeFunc:function(){
                var that = this;
                vpost('/api/reporting/server-type-list',{},function(res){
                    that.serviceTypeList = res;
                })
            },

            //高级搜索
            searchData:function(close) {
                this.searchType = 1;
                this.getData();
                close();
            },
            //基础搜索
            inputData:function(close) {
                this.searchType = 0;
                this.getData();
                close();
            },
            exportBtn:function(){
                document.location.href = '/reporting/tec-order-excel?name='+this.search.technicianInfo+'&date='+this.search.date+'&company_id='+this.search.company+'&work_type='+this.search.serviceType;
            }
        },
        mounted: function () {
            this.getData();
            this.getCompanyFunc();
            this.getServiceTypeFunc();
        },
    };
    return options;
}));
;(function (factory) {
    if (this && this.window === window && Vue) new Vue(factory(this, this.Vue));
}(function (win, Vue, _undefined) {
    var options = {
        el: "#app",
        data: {
            searchType : 0,
            search: {
                date:'',//日期
                serviceType:'',//服务类型
                customerInfo:'',//客户姓名/电话
                provinceId:'',//省Id
                provinceName:'',//省名
                cityId:'',//市id,
                cityName:'',//市名,
                districtId:'',//区县id
                districtName:'',//区县名
            },
            dataList: [],
            order_num_sum:0,//合计
            serviceTypeList:[],//服务类型信息
            provinceIdListData:[],//省
            districtListData:[],//市
            cityListData:[],//县
            pageParams: {// 分页参数
                pageNo: 1,
                pageSize: 10,
                total: 0,
                totalPage: 0
            }
        },
        methods: {
            changePage:function(pageNo){
                this.pageParams.pageNo = pageNo;
                this.getData();
            },

            //初始化数据
            getData:function() {
                var postData;
                var that =this;
                this.order_num_sum = 0;
                if(this.searchType == 1){
                    postData = {
                        'date':this.search.date,
                        'name':this.search.customerInfo,
                        'work_type':this.search.serviceType,
                        'page': this.pageParams.pageNo,
                        'pageSize': this.pageParams.pageSize
                    }
                }else{
                    postData = {
                        'name':this.search.customerInfo,
                        'page': this.pageParams.pageNo,
                        'pageSize': this.pageParams.pageSize
                    }
                }
                vpost('/api/reporting/account-order',postData, function (res) {
                    that.dataList = res.list;
                    that.pageParams.totalPage = res.totalPage;
                    that.pageParams.total = res.totalCount;
                    for(var i=0;i<that.dataList.length;i++){
                        that.order_num_sum +=(that.dataList[i].order_num-'');
                    }
                });
            },

            //获取服务类型
            getServiceTypeFunc:function(){
                var that = this;
                vpost('/api/reporting/server-type-list',{},function(res){
                    that.serviceTypeList = res;
                })
            },

            //高级搜索
            searchData:function(close) {
                this.searchType = 1;
                this.getData();
                close();
            },

            //基础搜索
            inputData:function(close) {
                this.searchType = 0;
                this.getData();
                close();
            },

            exportBtn:function() {
                document.location.href = '/reporting/account-order-excel?name='+this.search.customerInfo+'&date='+this.search.date+'&work_type='+this.search.serviceType;
            }
        },
        mounted: function () {
            this.getData();
            this.getServiceTypeFunc()
        },
    };
    return options;
}));
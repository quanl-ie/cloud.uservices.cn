new Vue({
    el: ".panel-body",
    data: {
        cur: 1,//当前页
        all: 0,//全部页
        status: 1,//运输状态
        source: 1,
        state:{},
        search: {
            send_no: '',//发货单号
            start_create_time: '',//开始日期
            end_create_time: '',//结束日期
        },
        showCollectGoods:false,
        postData:{},
        name: '',
        dataList: [],
        source:{
            sendMethodList:[],
            sendTypeList:[]
        },
        confirmCollectGoodsData:{
            send_id:0,
            rec_name:''
        }
    },
    directives: {
        InitProp: Vue.$$dirs.initProp
    },
    methods: {
        listen: function (data) {
            this.cur = data;
            this.getData();
        },
        getData: function (e) {
            var data = {
                name: this.name,
                status: this.status,
                source: this.source,
                page: this.cur,
                postData: this.postData
            }
            var _that = this;
            vpost('/api/send-out/index',data,function (res) {
                _that.dataList = res.list;
                _that.all = res.totalPage;
            });
        },
        tabGetdata: function (e) {
            this.status = e.target.value;
            this.getData();
        },
        //显示确认收货
        showCollectGoodsEvent:function (id,rec_name) {
            this.showCollectGoods = true;
            this.confirmCollectGoodsData.send_id = id;
            this.confirmCollectGoodsData.rec_name = rec_name;
        },
        //确认收货
        confirmCollectGoodsEvent:function (id) {
            var _this = this;
            this.showCollectGoods = true;

            if(this.confirmCollectGoodsData.rec_name == ''){
                alert("收货人姓名不能为空");
                return false;
            }

            vpost('/api/send-out/confirm',this.confirmCollectGoodsData,function (json) {
                alert("操作成功", function(){
                    _this.getData();
                    _this.hideCollectGoodsEvent();
                });
            })
        },
        //隐藏确认收货
        hideCollectGoodsEvent: function (e) {
            this.showCollectGoods = false;
        },
        searchEvent:function(){
            this.postData = this.search;
            this.getData();
        },
        changeDate : function(e){
            this.search[e.name]=e.time
        }
    },
    mounted: function (argument) {
        this.status = !!this.state.state1 ? 1 : 2;
        this.getData();
        this.initSearchOption();
    }
})
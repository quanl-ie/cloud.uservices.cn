(function () {
    // 装载图片预览插件
    Vue.use( vuePhotoPreview, {
        fullscreenEl : false,//关闭全屏按钮
        bgOpacity    : 0.6
    } );
    
    new Vue( {
        el       : "#app",
        data     : {
            source : {
                goodsTypes      : [],//商品类型
                goodsBrands     : [],//商品品牌
                goodsCategories : [],//商品类目,
                goodsUnits      : [],//计量单位
            },
            goods  : {//商品
                goodsName          : "",//名称
                goodsType          : "",//类型
                goodsBrand         : "",//品牌
                goodsCategory      : "",//类目
                goodsModel         : "",//型号
                goodsUnit          : "",//单位
                goodsWarranty      : "", //保质期
                // 基本信息——商品图片
                // { file: '图片文件', index: 下标位置, desc: 详情描述 }
                files              : [],
                // 商品属性
                goodsAttributes    : [{
                    name  : null,//属性名称
                    value : null//属性值
                }],
                // 规格值
                specifications     : [{
                    name   : "红色",// 规格
                    number : "",//编号
                    price  : ""//价格
                }],
                // 商品详情-图片
                // { file: '图片文件', index: 下标位置, desc: 详情描述 }
                goodsDetailsImages : []
            }
        },
        computed : {
            // 查看详情模式下显示规格值
            showSpec : function () {
                if ( this.goods.specifications.length > 0 ) {
                    return this.goods.specifications.map( function ( spec ) { return spec.name } ).join( "、" );
                }
                return '无';
            }
        },
        mounted  : function () {
            var _this = this;
            var goods_id = getQueryString( "goods_id" );
            vpost( "/api/goods/view", { "goods_id" : goods_id }, function ( json ) {
                _this.goods.goodsName = json.name;//名称
                _this.goods.goodsType = json.type_desc;//类型
                _this.goods.goodsBrand = json.brand_name;//品牌
                _this.goods.goodsCategory = json.class_name;//类目
                _this.goods.goodsModel = json.model;//型号,
                _this.goods.goodsUnit = json.unit_desc;//单位
                _this.goods.goodsWarranty = json.warranty;//保质期
                _this.goods.specifications = json.sku_arr;//规格
                _this.goods.goodsAttributes = json.property_arr;//属性
                _this.goods.files = (json.thumbnail || []).map( function ( file, i ) { //商品图
                    return {
                        url   : file,
                        index : i
                    };
                } );
                _this.goods.goodsDetailsImages = (json.detail_arr || []).map( function ( file, i ) { //详情图
                    return {
                        url   : file.img_url,
                        desc  : file.description,
                        index : i
                    };
                } );
                // 在图片数据更新后调用this.$previewRefresh()，
                _this.$previewRefresh();
            } );
        }
    } );
})();
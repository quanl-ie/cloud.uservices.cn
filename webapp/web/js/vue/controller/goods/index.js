new Vue({
    el:".panel-body",
    data:{
        cur: 1,
        all: 0,
        status:1,
        name:'',
        keyword:'',
        dataList:[],
        iColor:{'color':'bbb'}
    },
    methods:{
        listen: function (data) {
            this.cur = data;
            this.getData();
        },
        getData:function(e) {
            _that = this;
            vpost('/api/goods/list',{name:this.name,status:this.status,page:this.cur,},function (res) {
                _that.dataList = res.list;
                _that.all = Number(res.totalPage);
            });
        },
        tabGetdata:function(e) {
            this.status = e.target.value;
            this.getData();
        },
        search : function (e) {
            this.name = this.keyword;
            this.getData();
        },
        //下架事件
        offlineEvent:function (goodsId) {
            var _this = this;
            confirm("确主要下架吗？",function () {
                vpost('/api/goods/onoff',{goods_id:goodsId,status:2},function (json) {
                    alert('下架成功',function(){
                        _this.getData();
                    });
                });
            })
        },
        //上架事件
        onlineEvent:function (goodsId) {
            var _this = this;
            confirm("确主要上架吗？",function () {
                vpost('/api/goods/onoff',{goods_id:goodsId,status:1},function (json) {
                    alert('上架成功',function(){
                        _this.getData();
                    });
                });
            })
        }

    },
    mounted:function(argument) {
        var status = getQueryString('status');
        if(status != null){
            this.status = status;

        }
        this.getData();
    },
    watch:{
        'keyword':function(e){
            console.log(e);
            e!=''?(this.iColor={'color':'#40a9ff'}):(this.iColor={'color':'#bbb'});
        }
    }
})
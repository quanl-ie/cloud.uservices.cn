(function () {
    new Vue( {
        el      : "#app",
        data    : {
            offlineSubmiting : false,
            onlineSubmiting  : false,
            source           : {
                goodsTypes         : [],//商品类型
                goodsBrands        : [],//商品品牌
                goodsCategories    : [],//商品类目,
                goodsUnits         : [],//计量单位
                goodsWarrantyUnits : [], //质保期单位
                options            : []//类目
            },
            goods            : {//商品
                productId          :0,
                goodsName          : "",//名称
                goodsType          : "",//类型
                goodsBrand         : "",//品牌
                goodsCategory      : "",//类目
                goodsCategoryLabel : '',
                goodsModel         : "",//型号
                goodsUnit          : "",//单位
                goodsWarrantyNum   : "",//质保期数量
                goodsWarrantyUnit  : "", //质保期单位
                // 基本信息——商品图片
                // { file: '图片文件', index: 下标位置, desc: 详情描述 }
                files              : [],
                // 商品属性
                goodsAttributes    : [{
                    attr_key   : '',//属性名称
                    attr_value : ''//属性值
                }],
                // 规格值
                specifications     : [{
                    name         : "",// 规格
                    goods_no     : "",//编号
                    market_price : ""//价格
                }],
                // 商品详情-图片
                // { file: '图片文件', index: 下标位置, desc: 详情描述 }
                goodsDetailsImages : []
            }
        },
        created : function () {
            var _this = this;
            vpost( "/api/goods/get-basics-config", [], function ( json ) {
                _this.source.goodsUnits = json.unitArr;
                _this.source.goodsTypes = json.typeArr;
                _this.source.goodsBrands = json.brandArr;
                _this.source.goodsWarrantyUnits = json.warrantyUnitArr
            } );
            
            //初始化类目
            this.getgoodsCategory( 0, function ( res ) {
                console.log( res );
                _this.source.options = res;
            } );
        },
        methods : {
            //联想
            changeGoodsName:function (json) {

                this.goods.specifications  =[{
                    name         : "",// 规格
                    goods_no     : "",//编号
                    market_price : ""//价格
                }];
                this.goods.goodsAttributes = [{
                    attr_key   : '',//属性名称
                    attr_value : ''//属性值
                }];

                this.goods.goodsDetailsImages = [];


                this.goods.goodsType = 1;
                this.goods.goodsBrand = json.brand_id;
                this.goods.goodsModel = json.model;
                this.goods.goodsUnit  = json.unit_id;
                this.goods.productId  = json.id;
                this.goods.goodsCategory = json.class_id;
                this.goods.goodsCategoryLabel = json.class_name;
                if(json.warranty_num>0){
                    this.goods.goodsWarrantyNum = json.warranty_num;
                }
                else {
                    this.goods.goodsWarrantyNum = '';
                }
                if(json.warranty_type>0){
                    this.goods.goodsWarrantyUnit = json.warranty_type;
                }
                else {
                    this.goods.goodsWarrantyUnit = '';
                }
                if(json.sale_price > 0){
                    this.goods.specifications[0].market_price = json.sale_price;
                }
                if(json.prod_no!=''){
                    this.goods.specifications[0].goods_no = json.prod_no;
                }
            },

            //加载类目子集选项
            loadChild : function ( value, call ) {
                this.getgoodsCategory( value, call );
            },
            
            // 商品名称联想
            loadGoodsName : function ( value, call ) {
                vpost( "/api/goods/search-product", { keyword : value }, call );
            },
            
            getgoodsCategory : function ( value, fn ) {
                vpost( "/common/ajax-get-class?flag=1&pid=" + value, {}, function ( res ) {
                    fn( toArray( res ) );
                } )
            },
            
            // 校验市场价
            validPrice     : function () {
                var specifications = this.goods.specifications;
                for ( var i = 0; i < specifications.length; i++ ) {
                    if ( !(/^\d+(\.\d{1,2})?$/.test( specifications[i].market_price )) ) {
                        alert( "请填写正确的市场价格！" )
                        return false;
                    }
                }
                return true;
            },
            // 添加商品属性
            addGoodsAttr   : function () {
                this.goods.goodsAttributes.push( { attr_key : '', attr_value : '' } )
            },
            // 添加规格值
            addSpec        : function () {
                var specificationsFlag = true;
                var specifications = this.goods.specifications;
                for ( var i = 0; i < specifications.length; i++ ) {
                    if ( specifications[i].name == '' ) {
                        specificationsFlag = false;
                        break;
                    }
                }
                if ( specificationsFlag == true ) {
                    this.goods.specifications.push( { name : "", goods_no : "", market_price : "" } );
                }
                else {
                    alert( '请输入规格值' );
                }
            },
            // 删除规格值
            deleteSpec     : function ( spec, index ) {
                this.goods.specifications.splice( index, 1 );
            },
            //删除属性
            deleteAttributes: function (attr,index) {
                this.goods.goodsAttributes.splice( index, 1 );
            },
            // 校验属性值同属性名称一起不能为空
            validAttrValue : function () {
                if ( this.goods.goodsAttributes.length > 0 ) {
                    var goodsAttributes = this.goods.goodsAttributes;
                    for ( var i = 0; i < goodsAttributes.length; i++ ) {
                        if ( trim( goodsAttributes[i].attr_key ) != '' && trim( goodsAttributes[i].attr_value ) == '' ) {
                            alert( '请输入商品属性值' );
                            return false;
                        }
                        else if ( trim( goodsAttributes[i].attr_key ) == '' && trim( goodsAttributes[i].attr_value ) != '' ) {
                            alert( '请输入商品属性名称' );
                            return false;
                        }
                    }
                }
                return true;
            },
            // 仅保存
            offlineSubmit  : function ( flag ) {
                if ( flag && this.validAttrValue() && this.validPrice() ) {
                    this.submit( 2 );
                }
                else {
                    window.scrollTo(0,0);
                }
            },
            //保存并上架
            onlineSubmit   : function ( flag ) {
                if ( flag && this.validAttrValue() && this.validPrice() ) {
                    this.submit( 1 );
                }
                else {
                    window.scrollTo(0,0);
                }
            },
            submit         : function ( status ) {
                var _this = this;
                
                //商品图片
                var imgUrlArr = [];
                if ( this.goods.files.length > 0 ) {
                    var files = this.goods.files;
                    for ( var i = 0; i < files.length; i++ ) {
                        if ( files[i].url != '' ) {
                            imgUrlArr.push( files[i].url )
                        }
                    }
                }
                //商品详情
                var goodsDetail = [];
                if ( this.goods.goodsDetailsImages.length > 0 ) {
                    var goodsDetailsImages = this.goods.goodsDetailsImages;
                    for ( var i = 0; i < goodsDetailsImages.length; i++ ) {
                        if ( goodsDetailsImages[i].url != '' || (goodsDetailsImages[i].desc != '' && goodsDetailsImages[i].desc != null) ) {
                            goodsDetail.push( {
                                'img_url'     : goodsDetailsImages[i].url,
                                'description' : goodsDetailsImages[i].desc == null ? '' : goodsDetailsImages[i].desc
                            } );
                        }
                    }
                }
                
                //重组数据
                var data = {
                    product_id         : this.goods.productId,
                    name               : this.goods.goodsName,
                    type_id            : this.goods.goodsType,
                    brand_id           : this.goods.goodsBrand,
                    class_id           : this.goods.goodsCategory,
                    model              : this.goods.goodsModel,
                    unit_id            : this.goods.goodsUnit,
                    warranty_num       : this.goods.goodsWarrantyNum,
                    warranty_unit      : this.goods.goodsWarrantyUnit,
                    img_url_arr        : imgUrlArr,
                    status             : status,
                    goods_property_arr : this.goods.goodsAttributes,
                    goods_detail_arr   : goodsDetail,
                    goods_sku_arr      : this.goods.specifications
                };
                if ( status == 1 ) {
                    _this.onlineSubmiting = true;
                }
                else {
                    _this.offlineSubmiting = true;
                }
                
                vpost( "/api/goods/add", data, function ( json ) {
                    alert( '提交成功', function () {
                        document.location.replace( '/goods/index' )
                    } );
                }, function ( json ) {
                    alert( json.message );
                    _this.onlineSubmiting = false;
                    _this.offlineSubmiting = false;
                } );
            },
            goback : function () {
                history.back();
            }
        },
    } );
})();
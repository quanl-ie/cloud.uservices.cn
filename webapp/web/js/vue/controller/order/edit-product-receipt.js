;(function ( factory ) {
    if ( window === window.window && Vue ) new Vue( factory( this, this.Vue ) );
}( function ( win, Vue, _undefined ) {
    'use strict';
    var options = {
        el        : "#app",
        data      : {
            images  : [],
            product : {}
        },
        validator : true,
        created   : function () {
            var param = this.$$mapUrlParam();
            vpost( "/api/order/product-view", param, function ( data ) {
                this.$set( this, "product", data );
                var images = this.images;
                (this.$$u.isArray( data.scope_img ) ? data.scope_img : []).forEach( function ( url ) {
                    images.push( { file : null, type : "jpg", status : 200, src : url } );
                } );
            }.bind( this ) );
        },
        methods   : {
            createRule  : function ( item ) {
                var label = item.keyValue;// 不能为空时，则校验此输入项
                return !!item.notNull ? "defined?请输入" + label : _undefined;
            },
            uploadError : function ( item, res ) {
                //console.log( res );
            },
            ajaxSave    : function () {
                var product = this.product;
                var images = this.images.map( function ( img ) {
                    return img.src;
                } );
                vpost( "/api/order/edit-product", {
                    order_no    : product.order || product.order_no,
                    id          : product.id,
                    snid        : product.snid,
                    sn          : product.sn,
                    remark      : product.remark,
                    scope_img   : images,
                    extend_data : product.extend_data
                }, function ( result ) {
                    if ( this.$$u.isSense( result.order_no ) ) history.go( -1 );
                }.bind( this ) );
            },
            save        : function () {
                var that = this;
                confirm( "确定保存？", function () {
                    // that.$refs.save.submit();
                    that.$$runValid( 'save', that.ajaxSave );
                } );
            }
        }
    };
    return options;
} ));
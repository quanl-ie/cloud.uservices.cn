;(function ( factory ) {
    if ( this && this.window === window && Vue ) new Vue( factory( this, this.Vue ) );
}( function ( win, Vue, _undefined ) {
    var options = {
        el   : "#app",
        data : {
            workData:[],
            tagList:[],
            orderNo:'',
            workNo:'',
            desc:'',
            params:{
                work_no:'',
                star_num:0,
                tag_arr:[],
                desc:''
            }
        },
        methods : {
            
            getOrderData:function (workNo,departmentId) {
                _that = this;
                vpost('/api/order/before-appraisal-add', {work_no:workNo,department_id:departmentId} , function (res) {
                    _that.workData = res;
                    _that.orderNo = res.order_no;
                    var technicianArr = [];
                    if(res.technicianArr.length > 0){
                        res.technicianArr.map(function (v) {
                            technicianArr.push(v.name +'('+v.title+')');
                        });

                        _that.workData.technicianStr = technicianArr.join('、');
                    }
                    else {
                        _that.workData.technicianStr = '';
                    }
                });
            },
            getTags:function (star_num) {
                _that = this;
                this.params.star_num = star_num;
                vpost('/api/appraisal/get-tag', {star_num:star_num,work_no:this.workNo} , function (res) {
                    _that.tagList = res;
                });
            },
            cancel:function () {
                history.back();
            },
            commit:function () {
                if(this.params.star_num == 0){
                    alert('请选择评价!');
                    return false;
                }

                var _that = this;

                vpost('/api/appraisal/add', {params:[_that.params]} , function (res) {
                    alert('提交成功',function () {
                        history.back();
                    });
                },function (json) {
                    alert(json.message);
                });
            }
        },
        mounted:function(argument) {
            var workNo       = getQueryString('work_no');
            var departmentId = getQueryString('department_id');

            this.workNo = workNo;
            this.params.work_no = workNo;

            this.getOrderData(workNo,departmentId);
        },
    };
    return options;
} ));
;(function ( factory ) {
    if ( window === window.window && Vue ) new Vue( factory( this, this.Vue ) );
}( function ( win, Vue, _undefined ) {
    'use strict';
    var options = {
        el      : "#app",
        data    : {
            work    : null,
            scene   : null,
            record  : "",
            orderNo : null,
            workNo  : null
        },
        created : function () {
            var data = win.$$response;
            var work = data.work_img;
            var scene = data.scene_img;
            var isArray = this.$$u.isArray;
            this.record = data.service_record;
            this.orderNo = data.order_no;
            this.workNo = data.work_no;
            if ( isArray( work ) ) this.work = work.map( function ( url ) {
                return { file : null, type : "jpg", status : 200, src : url };
            } );
            if ( isArray( scene ) ) this.scene = scene.map( function ( url ) {
                return { file : null, type : "jpg", status : 200, src : url };
            } );
            
        },
        methods : {
            beforeSubmit : function () {
                return true;
            },
            uploadError  : function ( item, res ) {
                console.log( res );
            },
            save         : function () {
                var that = this;
                confirm( '确定保存？', function () {
                    that.$refs.save.submit();
                }, 1 );
            }
        }
    };
    return options;
} ));
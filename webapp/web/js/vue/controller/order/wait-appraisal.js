;(function (factory) {
    if (this && this.window === window && Vue) new Vue(factory(this, this.Vue));
}(function (win, Vue, _undefined) {
    var options = {
        el: "#app",
        data: {
            dataList: [],
            page: {// 分页参数
                pageNo: 1,
                pageSize: 10,
                total: 0,
                totalPage: 0
            },
            search: {
                appraisal_status: 1,
                page: 1,
                pageSize: 10,
                account_name: '',
                account_mobile: '',
                timeSection: [],
                finish_start_time: '',
                finish_end_time: '',
                fuzzy: ''
            }
        },
        created: function () {
            var parseDate = this.$$parseDate;
            var format = "yyyy-MM-dd HH:mm:ss";
            this._parseDate = function (time) {
                return time && String(time).length ? parseDate(new Date(parseInt(time) * 1000), format) : "";
            };
        },
        methods: {

            //高级搜索
            searchData: function (close) {
                if (this.search.timeSection.length == 2) {
                    this.search.finish_start_time = this.search.timeSection[0];
                    this.search.finish_end_time = this.search.timeSection[1];
                }
                this.getData();
                close();
            },
            //基础搜索
            inputData: function (close) {
                this.search.finish_start_time = '';
                this.search.finish_end_time = '';
                this.search.account_name = '';
                this.search.account_mobile = '';
                this.getData();
                close();
            },
            getData: function () {
                _that = this;

                _that.search.page = _that.page.pageNo;
                _that.search.pageSize = _that.page.pageSize;

                vpost('/api/order/wait-appraisal', _that.search, function (res) {
                    _that.dataList = res.list;
                    _that.page.pageNo = res.page;
                    _that.page.total = res.totalCount;
                    _that.page.totalPage = res.totalPage;
                });
            },
            // 翻页事件
            changePage: function (pageNo) {
                this.page.pageNo = pageNo;
                // 开始请求数据
                this.getData();
            },

            exportBtn: function () {
                document.location.href = '/order/index?fuzzy=' + this.search.fuzzy + '&account_name=' + this.search.account_name + '&account_mobile=' + this.search.account_mobile + '&finish_start_time=' + this.search.finish_start_time + '&finish_end_time=' + this.search.finish_end_time + '&appraisal_status=1&export=1';
            }

        },
        mounted: function (argument) {
            this.getData();
        },
    };
    return options;
}));
;(function (factory) {
    if (this && this.window === window && Vue) new Vue(factory(this, this.Vue));
}(function (win, Vue, _undefined) {
    var options = {
        el: "#app",
        data: {
            searchType : 0,
            goods_len : 0,
            normal_len: 0,
            bad_len:0,
            search: {
                name: '',//技师姓名
                month: ''//日期
            },
            tagsHeader:{

            },
            tagsList: [],
            dataList: [],
            page: {// 分页参数
                pageNo: 1,
                pageSize: 10,
                total: 0,
                totalPage: 0
            }
        },
        created: function () {
            this.getTitle();
            this.getData();
        },
        methods: {
            // 翻页事件
            changePage: function (pageNo) {
            
            },

            getTitle: function () {
                var _that = this;
                vpost('/api/technician-appraisal/get-tags',{},function (res) {
                    var i,j,m,item,list=[],header=[];
                    for ( i in res) {
                        item = res[i];
                        m=item.length;
                        if(i == 1){
                            _that.goods_len = m - 1;
                        }
                        if(i == 2){
                            _that.normal_len = _that.goods_len+m;
                        }
                        if(i == 3){
                            _that.bad_len = _that.normal_len + m;
                        }
                        header.push({lable:(i==1?'好评':(i==2?'中评':'差评')),colspan:m});
                        for ( j = 0; j < m; j++) {
                            list.push(item[j]);
                        }
                    }
                    _that.tagsHeader = header;
                    _that.tagsList = list;
                });
            },
            getData: function (page) {
                var name = this.search.name;
                var month = this.search.month;
                var page = page;
                var pagesize = this.page.pageSize;
                var _that = this;
                if(this.searchType == 1){
                    var postData = {
                        'technician_name':name,
                        'month':month,
                        'page': page,
                        'pagesize': pagesize
                    }
                }else{
                    var postData =  {
                        'technician_name':name,
                        'page': page,
                        'pagesize': pagesize
                    }
                }
                vpost('/api/technician-appraisal/index',postData, function (res) {
                   _that.dataList = res.list;
                });
            },
            // tab选项卡选择事件
            tabChange: function (value) {
                this.status = value+1;
                this.getData();
                //console.log("tab选项卡：" + (value+1));
            },
            //高级搜索
            searchData: function (close) {
                this.searchType = 1;
                this.getData(1);
                close();
            },
            //基础搜索
            inputData: function (close) {
                this.searchType = 0;
                this.getData(1);
                close();
            },
            exportBtn:function () {
                document.location.href = '/technician-appraisal/import?technician_name='+this.search.name+'&month='+this.search.month;
            }
        }
    };
    return options;
}));
(function () {
    
    var merge = Vue.$utils.merge;
    var isDefined = Vue.$utils.isDefined;
    //var mergeCall = Vue.$utils.mergeCall;
    
    var postDataMap = ("send_no,stock_id,send_type,send_method," +
        "product_ids,send_title,apll_id,apll_no,rec_name,rec_mobile," +
        "rec_address,rec_date,province_id,city_id,district_id,pick_name," +
        "pick_mobile,pick_card,pick_license,remark,create_user_id").split( "," );
    
    var telephoneReg = /^(?:\d{3,4}[-])?(?:\d{7}|(?:1\d{10}))$/;
    var idCardReg = /^(?:\d{15})|(?:\d{18})|(?:\d{17}[xX])$/;
    var METHOD_THIRD = "3";//第三方物流
    var METHOD_SELF = "2";//客户自提
    var METHOD_CARRY = "1";//带货安装
    var that = new Vue( {
        el      : "#app",
        data    : {
            source : {
                sendMethods : null,
                province    : null,
                city        : null,
                district    : null,
                product     : null,
                logistics   : null
            },
            send   : {
                apply_id       : "",
                apply_name     : "",
                stock_id       : getQueryString( "id" ), // 出库ID
                // stock_id       : "55", // 出库ID
                send_no        : "",//发货单号
                stock_no       : "",  //出库单号
                send_type      : "",//发货类型ID
                send_type_name : "",//发货类型名称
                send_method    : "",//发货方式
                product_ids    : "",//发货产品
                product        : "",//产品信息
                province       : "",//省份
                send_title     : "",//发货主题
                apll_id        : "",//物流公司ID,
                apll_no        : "",//物流单号
                rec_name       : "",//收货人姓名,
                rec_mobile     : "",//收货人手机号,
                rec_address    : "",//收货人地址
                rec_date       : "",//预计到货时间
                province_id    : "",//收货人地址/省份ID,
                city_id        : "",//城市ID,
                district_id    : "",//区ID
                pick_name      : "",//提货人姓名
                pick_mobile    : "",//提货人手机号
                pick_card      : "",//提货人身份证号
                pick_license   : "",//提货人车牌号
                remark         : ""//备注
            },
            state  : {
                submit      : false,
                methodThird : METHOD_THIRD,
                methodSelf  : METHOD_SELF,
                methodCarry : METHOD_CARRY
            }
        },
        created : function () {
            // 获取基本数据
            var send = this.send, source = this.source;
            axios.post( "/api/send-out/before-add", {
                stock_id : this.send.stock_id
            } ).then( function ( res ) {
                merge( send, res, 0, "send_method,product,province" );
                source.sendMethods = res.send_method;
                source.product = res.product;
                source.province = res.province;
                send.send_title = res.subject;
                send.send_method = res.default_send_method;
                var acc, addr;
                if ( acc = res.account ) {
                    send.rec_name = acc.account_name;
                    send.rec_mobile = acc.mobile;
                }
                if ( addr = res.account_address ) {
                    send.province_id = addr.province_id;
                    send.city_id = addr.city_id;
                    send.district_id = addr.district_id;
                    send.rec_address = addr.address;
                }
            } );
            
        },
        
        watch   : {
            // 省
            "send.province_id" : function ( pro ) {
                axios.post( "/api/send-out/get-city", { province_id : pro } )
                    .then( function ( list ) { that.source.city = list } );
            },
            // 市
            "send.city_id"     : function ( city ) {
                axios.post( "/api/send-out/get-district", { city_id : city } )
                    .then( function ( list ) { that.source.district = list } );
            },
            // 获取物流公司列表 /api/send-out/get-apll
            "send.send_method" : function ( me ) {
                // 当选择物流公司时
                if ( me === METHOD_THIRD ) {
                    axios.post( "/api/send-out/get-apll" )
                        .then( function ( list ) { that.source.logistics = list } );
                }
            }
        },
        methods : {
            
            // 若选择第三方物流，则物流公司必选
            checkLogistics  : function ( val, check ) {
                check( (this.send.send_method !== METHOD_THIRD || isDefined( val )), "请选择物流公司" );
            },
            //
            checkPickName   : function ( val, check ) {
                var send = this.send, card = send.pick_card, mob = send.pick_mobile;
                check( !(isDefined( card ) || isDefined( mob )) || isDefined( val ), "请填写提货人姓名" );
            },
            checkPickCard   : function ( val, check ) {
                var send = this.send, name = send.pick_name, mob = send.pick_mobile;
                check( !((isDefined( val ) || isDefined( name ) || isDefined( mob ))) || (isDefined( val ) && idCardReg.test( val )), "请正确填写身份证号" );
            },
            checkPickMobile : function ( val, check ) {
                var send = this.send, card = send.pick_card, name = send.pick_name;
                check( !(isDefined( val ) || isDefined( name ) || isDefined( card )) || (isDefined( val ) && telephoneReg.test( val )), "请填写提货人电话" );
            },
            //
            changeDate      : function ( date ) {
                this.send.rec_date = date;
            },
            
            goBack : function () {
                history.go( -1 );
            },
            
            openTab : function () {
                window.open( "stock_out/view?id=" + this.send.stock_id );
            },
            
            submit : function ( flag ) {
                if ( flag ) {
                    var send = this.send, postData, that = this;
                    send.product_ids = this.source.product.map( function ( p ) { return p.prod_id} ).join( "," );
                    postData = merge( {}, send, postDataMap );
                    postData.create_user_id = send.apply_id;
                    this.state.submit = true;
                    //
                    // 带货安装 客户自提
                    if ( postData.send_method === METHOD_CARRY || postData.send_method === METHOD_SELF ) {
                        postData.apll_id = postData.apll_no = '';
                    }
                    // 客户自提
                    if ( postData.send_method === METHOD_SELF ) {
                        postData.rec_address = postData.rec_mobile = postData.rec_name =
                            postData.province_id = postData.city_id = postData.district_id = "";
                    }
                    //////////////////
                    axios.post( "/api/send-out/add", { postData : postData } ).then( function ( res ) {
                        that.state.submit = false;
                        alert( "操作成功！", function () {
                            document.location.replace( '/send-out/index' );
                        } );
                    }, function () {
                        that.state.submit = false;
                    } );
                }
            }
        },
    } );
})();
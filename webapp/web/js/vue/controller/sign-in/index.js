;(function (factory) {
    if (this && this.window === window && Vue) new Vue(factory(this, this.Vue));
}(function (win, Vue, _undefined) {

    var options={
        fullscreenEl:false //关闭全屏按钮
    }

    Vue.use(vuePhotoPreview,options)

    var options = {
        el: "#app",
        data: {
            dataList: [],
            page: {// 分页参数
                pageNo: 1,
                pageSize: 10,
                total: 0,
                totalPage: 0
            },
            search: {
                begin_time:'', //开始时间
                end_time:'', //结束时间
                department_id:'', //部门
                group_id:'', //小组
                technician_id:'', //技师
                page:1,
                category:'',
                timeSection: [],
                technician_name:''
            },
            source: {
                departmentList:[],
                groupList:[],
                technicianList:[],
                categoryList:[]
            }
        },
        created: function () {
            var parseDate = this.$$parseDate;
            var format = "yyyy-MM-dd HH:mm:ss";
            this._parseDate = function (time) {
                return time && String(time).length ? parseDate(new Date(parseInt(time) * 1000), format) : "";
            };
        },
        methods: {

            //高级搜索
            searchData: function (close) {
                this.search.page = 1;
                this.page.pageNo = 1;
                if (this.search.timeSection.length == 2) {
                    this.search.begin_time = this.search.timeSection[0];
                    this.search.end_time = this.search.timeSection[1];
                }
                this.getData();
                close();
            },
            //基础搜索
            inputData: function (close) {
                this.search.begin_time = '';
                this.search.end_time = '';
                this.search.department_id = '';
                this.search.group_id = '';
                this.search.technician_id = '';
                this.search.page = 1;
                this.search.category = '';

                this.search.page = 1;
                this.page.pageNo = 1;

                this.getData();
                close();
            },
            getData: function () {
                _that = this;

                _that.search.page = _that.page.pageNo;
                _that.search.pageSize = _that.page.pageSize;

                vpost('/api/sign-in/list', _that.search, function (res) {
                    _that.dataList = res.list;
                    _that.page.pageNo = res.page;
                    _that.page.total = res.totalCount;
                    _that.page.totalPage = res.totalPage;

                    _that.$previewRefresh()
                });
            },
            // 翻页事件
            changePage: function (pageNo) {
                this.page.pageNo = pageNo;
                // 开始请求数据
                this.getData();
            },

            initDepartment: function () {
                var _that = this;
                vpost('/api/sign-in/search-option',{type:1},function (res) {
                    _that.source.departmentList = res;
                });
            },
            initCategory: function () {
                var _that = this;
                vpost('/api/sign-in/category',{type:1},function (res) {
                    _that.source.categoryList = res;
                });
            },
            changeSearchGrouping: function (val) {
                var _that = this;
                vpost('/api/sign-in/search-option',{type:2,department_id:val},function (res) {
                    _that.source.groupList = res;
                    _that.source.technicianList = [];
                    _that.search.technician_id = '';
                    _that.search.group_id = '';
                });
            },
            changeSearchTechnician: function (val) {
                var _that = this;
                vpost('/api/sign-in/search-option',{type:3,department_id:_that.search.department_id,grouping_id:val},function (res) {
                    _that.source.technicianList = res;
                    _that.search.technician_id = '';
                });
            },

            exportBtn: function () {
                document.location.href = '/order/index?fuzzy=' + this.search.fuzzy + '&account_name=' + this.search.account_name + '&account_mobile=' + this.search.account_mobile + '&finish_start_time=' + this.search.finish_start_time + '&finish_end_time=' + this.search.finish_end_time + '&appraisal_status=1&export=1';
            }

        },
        mounted: function (argument) {
            this.getData();
            this.initDepartment();
            this.initCategory();
        },
    };
    return options;
}));
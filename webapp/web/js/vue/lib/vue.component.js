;(function ( factory ) {
    if ( Vue ) factory();
})( function ( un ) {
    var O = {};
    var N = "";
    var ts = O.toString;
    var RCT = /\[object\s(\w+)]/;
    var uid = 0;
    
    function ct( o ) {
        return (ts.call( o ).replace( RCT, "$1" )).toLocaleLowerCase()
    }
    
    function isD( i ) {
        return i !== null && i !== un && i !== N
    }
    
    function to( v ) {
        return !isD( v ) ? N : typeof v === "object" ? JSON.stringify( v, null, 2 ) : String( v );
    }
    
    function isFunction( f ) {
        return typeof f === "function";
    }
    
    Vue.$utils = { isDefined : isD, toString : to, classType : ct, isFunction : isFunction };
    // 点击元素外事件指令
    var ClickOutside = (function () {
        var nodeList = [];
        var prop = "vue-click-outside";
        var startClick;
        var uuid = 0;
        // 鼠标左键按下时，获取事件对象
        document.addEventListener( 'mousedown', function ( ev ) { (startClick = ev) } );
        
        // 鼠标左键放开后，循环注册过的对象，并执行对象的处理函数，传入参数
        document.addEventListener( 'mouseup', function ( ev ) {
            nodeList.forEach( function ( node ) { node[prop]( ev, startClick ) } );
        } );
        
        // 创建处理函数
        function createHandler( el, binding, vnode, methodName, handler ) {
            // 两个事件对象
            return function ( mouseup, mousedown ) {
                mouseup = mouseup || {};
                mousedown = mousedown || {};
                if ( !vnode || !vnode.context || !mouseup.target || !mousedown.target ||
                    // 如果两次事件均触发在同一个元素或者是其后代元素时
                    el.contains( mouseup.target ) || el.contains( mousedown.target ) || el === mouseup.target ) return;
                var nowHandler;
                // 如果有函数，且vm实例上有此函数
                (binding.expression && methodName && (nowHandler = vnode.context[methodName]))
                    ? nowHandler() : handler();
            }
        }
        
        function noop() {}
        
        // 初始化
        function init( el, binding, vnode ) {
            var methodName = binding.expression;
            var method = isFunction( binding.value ) ? binding.value : noop;
            var handler = createHandler( el, binding, vnode, methodName, method );
            handler.id = uuid++;
            el[prop] = handler;
        }
        
        return {
            // 首次绑定时
            bind   : function ( el, binding, vnode ) {
                init( el, binding, vnode );
                // 添加当前元素到全局列表中
                nodeList.push( el );
            },
            // 元素被更新时
            update : function ( el, binding, vnode ) {
                init( el, binding, vnode );
            },
            // 解绑时
            unbind : function ( el ) {
                var i = 0, l = nodeList.length, node;
                for ( ; i < l; i++ ) {
                    node = nodeList[i];
                    if ( node[prop].id === el[prop].id ) nodeList.splice( i, 1 );
                }
            }
        };
    })();
    
    function CR( r ) {
        var fn, s, rl, m, t = ct( r );
        if ( t === "function" ) {
            fn = function ( v, c ) {
                return r( v, c )
            }
        } else if ( t === 'string' ) {
            s = r.split( "::" ), rl = s[0], m = s[1];
            fn = function ( v, c ) {
                return (Vue.$rules[rl])( v, c )
            };
        } else if ( t === 'object' ) {
            fn = CR( r.rule ), m = r.msg;
        } else if ( t === "regexp" ) {
            fn = function ( v, c ) {
                return c( r.test( v ) )
            }
        }
        fn.msg = m;
        return fn;
    }
    
    // 验证
    function check( v ) {
        var vm = this, r = this._rule;
        if ( typeof r === "function" ) r( v, (function ( f, m ) {
            vm.message = (vm._flag = f) ? false : (m || r.msg);
            if ( !f ) vm.$emit( "input-error", vm.message );
            return f;
        }) );
        return vm._flag;
    }
    
    function getCalendar( y, m ) {
        y = parseInt( y );
        m = parseInt( m );
        var time = new Date( y, m - 1, 1 );
        var lastDate, nextDate;
        var lastMonth = m - 1;
        var nextMonth = m + 1;
        if ( m === 1 ) {
            lastDate = "" + (y - 1) + '-' + +12 + '-';
            nextDate = "" + y + '-' + 2 + '-';
            lastMonth = 12
        } else if ( m == 12 ) {
            lastDate = "" + y + '-' + 11 + '-';
            nextDate = "" + (y + 1) + '-' + 1 + '-';
            nextMonth = 1
        } else {
            lastDate = "" + y + '-' + (m - 1) + '-';
            nextDate = "" + y + '-' + (m + 1) + '-';
        }
        var maxNumber = 35;
        var r1 = [], r2 = [], r3 = [];
        var lastFix = time.getDay() - 1;
        lastFix = lastFix < 0 ? lastFix + 7 : lastFix;
        var lastMaxDate = new Date( y, m - 1, 0 ).getDate(); //上个月份最大天数
        var maxDate = new Date( y, m, 0 ).getDate();//当前月份的
        var i, t;
        var nextFix = maxNumber - maxDate - lastFix;
        for ( i = 0; i < lastFix; i++ ) {
            t = lastMaxDate - lastFix + i + 1;
            r1[i] = {
                month : lastMonth,
                day   : t,
                data  : lastDate + t
            }
        }
        for ( i = 0; i < maxDate; i++ ) {
            t = i + 1;
            r2[i] = {
                month : m,
                day   : t,
                data  : "" + y + '-' + +m + '-' + t
            }
        }
        for ( i = 0; i < nextFix; i++ ) {
            t = i + 1;
            r3[i] = {
                month : nextMonth,
                day   : t,
                data  : nextDate + t
            }
        }
        var result = r1.concat( r2, r3 );
        var ar = [];
        for ( i = 0; i < 5; i++ ) {
            ar.push( result.splice( 0, 7 ) )
        }
        return ar
    }
    
    var inputMixin = {
        props     : {
            placeholder : String,
            disabled    : { type : Boolean, default : !1 },
            rule        : null,
            width       : { type : String, default : "100%" },
            height      : { type : String, default : "32px" },
            padding     : { type : String, default : "" },
            margin      : { type : String, default : "" },
            textAlign   : null
        },
        data      : function () {
            return { message : false }
        },
        created   : function () {
            if ( isD( this.rule ) ) {
                this._rule = CR( this.rule );
                var form = this._form = this.$$parent( "u-form" );
                if ( form ) form.append( this );
                this._check = function ( v ) {
                    return check.call( this, v || this.modelValue )
                }
            }
        },
        destroyed : function () {
            if ( this._form ) this._form.remove( this );
        }
    };
    
    Vue.$$dirs = {};
    
    var initProp = Vue.$$dirs.initProp = {
        bind : function ( el, binding, vnode ) {
            var vm = vnode.context;
            var expression = binding.value;
            if ( typeof expression === "function" ) {
                expression( vm.$set );
            } else if ( Array.isArray( expression ) ) {
                vm.$set.apply( vm, expression );
            }
        }
    };
    
    /**
     * textarea组件
     */
    Vue.component( "u-textarea", {
        props    : {
            value       : null,
            computable  : { type : Boolean, default : true },
            placeholder : String,
            length      : { type : Number, default : 200 },
        },
        mixins   : [inputMixin],
        template : (
            " <div data-v-24ad2701 class=\"input-warp\"  :style='{ width : width, margin : margin, padding : padding }'>\n" +
            "    <textarea :disabled='disabled' data-v-24ad2701 class=\"ipt\" :style='{height : height}' :value=\"modelValue\" @input.stop='inputHandler' :placeholder=\"placeholder\"></textarea>\n" +
            "    <span v-if='computable' data-v-24ad2701 class=\"compute-font\">{{fontCount}} / {{length}}</span>\n" +
            "    <div v-if='!!message' class='ipt-explain'>{{message}}</div>" +
            "  </div>"
        ),
        data     : function () {return { fontCount : 0 }},
        methods  : {
            inputHandler : function ( e ) {
                var t = e.target.value, l, max;
                if ( this.computable && (l = t ? t.length : 0) > (max = this.length) ) {
                    l = max;
                    t = this.modelValue;
                }
                this.fontCount = l;
                this.modelValue = e.target.value = t;
                if ( this.rule ) this.$nextTick( function () {
                    this._check( t );
                } );
                
            }
        },
        computed : {
            modelValue : {
                get : function () {return this.value || ""},
                set : function ( t ) {this.$emit( "input", t )}
            }
        }
    } );
    
    /**
     * 图片选择组件
     */
    Vue.component( "select-image", {
        template : (
            "  <div data-v-895fa160 class=\"select-image-warp\">\n" +
            "    <iframe style='display: none' :name=\"'uploadIFrame' + _uid\" ref='uploadIFrame'></iframe>\n" +
            "    <ul data-v-895fa160 class=\"ul\" :class=\"[vertical ? 'vertical' : 'level']\">\n" +
            "      <li data-v-895fa160 v-for=\"img in getImages\">\n" +
            "        <div data-v-895fa160 class=\"image-content flex\">\n" +
            "          <img data-v-895fa160 ref=\"image\" :preview='_uid' class=\"flex-item-orig\" :src=\"img.url\"/>\n" +
            "          <div data-v-895fa160 v-if=\"vertical\" class=\"flex-item slot flex flex-jcz-center\">\n" +
            "            <span data-v-895fa160 v-if=\"readonly\" class='image-content-desc'>{{img.desc}}</span>\n" +
            "            <u-textarea v-else width='270px' height='82px' v-model=\"img.desc\" placeholder=\"请输入图片描述\"></u-textarea>\n" +
            "          </div>\n" +
            "          <div v-if=\"!readonly\" data-v-895fa160 class=\"image-content-mask\">\n" +
            "            <form method='post' enctype='multipart/form-data' :target=\"'uploadIFrame' + _uid\" ref='editForm' :action='uploadUrl'>\n" +
            "              <label data-v-895fa160 class=\"image-content-mask-btn image-content-mask-btn-left\">\n" +
            "                修改<input data-v-895fa160 type=\"file\" name='file' @change=\"edit(img, $event)\">\n" +
            "              </label>\n" +
            "            </form>\n" +
            "            <label data-v-895fa160 @click.stop=\"del(img)\"\n" +
            "                   class=\"image-content-mask-btn image-content-mask-btn-right\"></label>\n" +
            "          </div>\n" +
            "        </div>\n" +
            "      </li>\n" +
            "      <li v-if=\"!readonly && !isMax\" data-v-895fa160 class=\"select-image-add\">\n" +
            "        <form method=\"post\" enctype=\"multipart/form-data\" ref='addForm' :target=\"'uploadIFrame' + _uid\" :action='uploadUrl'>\n" +
            "          <label data-v-895fa160 class=\"image-content flex flex-zz-center flex-jcz-center flex flex-top\">\n" +
            "            <input data-v-895fa160 :multiple=\"multiple\" name='file' type=\"file\" @change=\"add\"/>\n" +
            "            <div data-v-895fa160 class=\"flex-item-orig select-image-add-icon\">+</div>\n" +
            "            <div data-v-895fa160 class=\"flex-item-orig select-image-add-text\">添加图片</div>\n" +
            "          </label>\n" +
            "        </form>\n" +
            "      </li>\n" +
            "    </ul>\n" +
            "  </div>"
        ),
        data     : function () {
            return { array : [], files : [], _uid : (uid += 1) }
        },
        props    : {
            uploadUrl : { type : String, default : "/upload/upload?source=goods" },
            images    : {
                type : Array, default : function () {
                    return []
                }
            },
            multiple  : { type : Boolean, default : !1 },
            imageType : { type : String, default : "jpg,jpeg,png,bmp" },
            length    : { type : Number, default : 5 },
            vertical  : { type : Boolean, default : !1 },
            readonly  : { type : Boolean, default : !1 },
            imageSize : { type : Number, default : 3145728 },
        },
        computed : {
            getRegExp : function () {
                return new RegExp( "(?:" + this.imageType.split( "," ).join( "|" ) + ")" )
            },
            getImages : function () {
                var t = this, e = this.images || [];
                return e.forEach( function ( e, n ) {
                    e.index = n, t.$nextTick( function () {
                        return t.getPath( e.file, function ( e ) {
                            return t.$refs.image[n].src = e
                        } )
                    } )
                } ), e
            },
            isMax     : function () {
                return this.length && this.images.length >= this.length
            },
        },
        mounted  : function () {
            var iframe = this.$refs.uploadIFrame;
            this.uploadedCall = function ( fn ) {
                iframe.onload = function () {
                    fn( JSON.parse( iframe.contentWindow.document.body.innerText ) );
                };
            }
        },
        methods  : {
            createdIterator : function ( a ) {
                var i = -1, l = a && a.length;
                return function ( call ) {
                    i += 1;
                    if ( l && (i < l) ) call( a[i] );
                };
            },
            toArray         : function ( t ) {
                return this.array.slice.call( t )
            },
            filter          : function ( t ) {
                var e = this, n = this.imageSize, i = !0,
                    a = t.filter( function ( t ) {
                        return e.getRegExp.test( t.type ) ? !(n && t.size > n) || (e.$emit( "file-size-error", t ), i = !1) : (e.$emit( "file-type-error", t ), i = !1)
                    } );
                return i ? a : [];
            },
            getPath         : function ( t, e ) {
                if ( t ) {
                    var n = new FileReader;
                    n.addEventListener( "load", function t( i ) {
                        e( i.target.result ), n.removeEventListener( "load", t )
                    } ), n.readAsDataURL( t )
                }
            },
            add             : function ( t ) {
                var s = this, e, n = this.filter( this.toArray( t.target.files ) ), i = this.images.length,
                    a = n.map( function ( t ) {
                        return { file : t, index : i, desc : null }
                    } );
                var iterator = this.createdIterator( a );
                iterator( function call( obj ) {
                    s.uploadedCall( function ( res ) {
                        if ( !res.url ) return alert( "图片上传失败，请重试上传！" );
                        obj.url = res.url;
                        (e = s.images).push( obj );
                        iterator( call );
                    } );
                    s.$refs.addForm.submit();
                } );
            },
            edit            : function ( t, e ) {
                var s = this, n = t.index, i = this.filter( this.toArray( e.target.files ) )[0];
                this.uploadedCall( function ( res ) {
                    if ( !res.url ) return alert( "图片上传失败，请重试上传！" );
                    var news = { file : i, index : n, desc : null, url : res.url };
                    s.images.splice( n, 1, news );
                    s.$emit( "edit-image", t, news );
                } );
                this.$refs.editForm[n].submit();
                // i && this.images && this.images.splice( n, 1, {file: i, index: n, desc: null} )
            },
            del             : function ( t ) {
                var e = t.index;
                this.images.splice( e, 1 );
                this.$emit( "edit-image", t );
            }
        }
    } );
    
    /**
     * form表单
     */
    Vue.component( "u-form", {
        template : (
            "  <form data-v-f4ca5dcc class=\"form\" :style=\"{ width }\">\n" +
            "    <slot></slot>\n" +
            "  </form>"
        ),
        props    : { width : String, labelWidth : String },
        created  : function () {
            var c = [];
            this.append = function ( v ) {
                (c.indexOf( v ) === -1) && c.push( v )
            };
            this.remove = function ( v ) {
                c.splice( c.indexOf( v ), 1 )
            };
            this.checkAll = function () {
                return !c.filter( function ( vm ) {
                    return !vm._check()
                } ).length;
            }
        }
    } );
    
    /**
     * form-item
     */
    Vue.component( "u-form-item", {
        template : (
            "  <div data-v-5b242029 v-if='!mini' :style='{height : height}' class=\"row form-item-row flex\">\n" +
            "    <div data-v-5b242029 class=\"col form-item-label flex-item-orig\">\n" +
            "      <span data-v-5b242029 v-if=\"required\" class=\"form-item-label-imp\">*</span>\n" +
            "      <slot name=\"label\"><label data-v-5b242029>{{label}}</label></slot>\n" +
            "    </div>\n" +
            "    <div data-v-5b242029 class=\"col form-item-content flex-item\">\n" +
            "      <slot></slot>\n" +
            "    </div>\n" +
            "  </div>\n" +
            "<div v-else data-v-5b242029 :style='{ width : width, height : height ,margin:margin}' class=\"field-row\">\n" +
            "   <label :style=\"{ 'vertical-align': labelVerticalAlign, width : labelWidth ,'line-height':labelHeight}\" data-v-5b242029><span data-v-5b242029 v-if=\"required\" class=\"form-item-label-imp\">*</span>{{label}}</label>\n" +
            "   <span data-v-5b242029 class=\"field-cell\" :style=\"{'margin-left':labelWidth,'line-height':labelHeight}\"><slot></slot></span>\n" +
            "</div>"
        ),
        props    : {
            labelVerticalAlign : { type : String, default : "" },
            width              : { type : String, default : "" },
            labelWidth         : { type : String, default : "" },
            labelHeight        : { type : String, default : "" },
            height             : { type : String, default : "" },
            mini               : { type : Boolean, default : !1 },
            label              : String,
            required           : { type : Boolean, default : !1 },
            labelColumn        : { type : Number, default : 6 },
            margin             : { type : String, default : "" }
        }
    } );
    
    /**
     * 按钮
     */
    Vue.component( "u-button", {
        template : (
            "  <button data-v-6b64a23e " +
            ":style='{ width :width, height: height, padding: padding, margin: margin }'" +
            " :type=\"nativeType\"" +
            " @click.stop='clickHandler'" +
            " class=\"btn\"" +
            " :disabled=\"disabled\" :class=\"'btn-'+type\">\n" +
            "    <slot></slot>\n" +
            "  </button>"
        ),
        props    : {
            width      : { type : String, default : "" },
            height     : { type : String, default : "" },
            padding    : { type : String, default : "" },
            margin     : { type : String, default : "" },
            type       : { type : String, default : "default" },
            nativeType : { type : String, default : "button" },
            disabled   : { type : Boolean, default : !1 },
            check      : { type : Boolean, default : !1 },
        },
        created  : function () {
            if ( this.check ) {
                var f = this.$$parent( "u-form" );
                if ( f ) this.$check = function () {
                    return f.checkAll();
                }
            }
        },
        methods  : {
            clickHandler : function () {
                this.$emit( "click", this.$check && this.$check() )
            }
        }
    } );
    
    /**
     * text输入框
     */
    Vue.component( "u-input", {
        template : (
            "  <div class='ipt-warp'  :style='{ width : width, margin : margin, padding : padding }'><input :type=\"password ? 'password' :'text'\"\n" +
            "         :value='modelValue' @input.stop='inputHandler''\n" +
            "         class=\"ipt\"\n :style=\"{ height : height,width:width,'text-align': textAlign }\" " +
            "         :title=\"title\" :disabled='disabled'\n" +
            "         :placeholder=\"placeholder\"><slot></slot>" +
            "  <div v-if='!!message' class='ipt-explain'>{{message}}</div>" +
            "  </div>"
        ),
        props    : {
            value       : null,
            placeholder : String,
            title       : String,
            password    : { type : Boolean, default : !1 },
            wordCount   : Number
        },
        mixins   : [inputMixin],
        methods  : {
            inputHandler( e ) {
                var max = this.maxWords, v = e.target.value, l;
                if ( max && (l = v.length) > max ) v = this.modelValue;
                e.target.value = this.modelValue = v;
                if ( this.rule ) this.$nextTick( function () {
                    this._check( v )
                } )
            }
        },
        computed : {
            maxWords() {
                var count = this.wordCount;
                return count && count > 0 ? count : 0;
            },
            modelValue : {
                get : function () {
                    return this.value || "";
                },
                set : function ( e ) {
                    this.$emit( "input", e );
                }
            }
        }
    } );
    
    /**
     * select选择框
     */
    Vue.component( "u-select", {
        template : (
            "    <div class=\"ipt-warp\"  :style='{width : width, margin : margin, padding : padding }'>\n" +
            "      <select :disabled='disabled' :style='{  height : height }' v-model='modelValue' class=\"ipt\">\n" +
            "        <slot></slot>\n" +
            "      </select>\n" +
            "      <div v-if='!!message' class='ipt-explain'>{{message}}</div>\n" +
            "    </div>"
        ),
        props    : {
            value : null,
            title : String,
        },
        mixins   : [inputMixin],
        computed : {
            modelValue : {
                get : function () {
                    return this.value || "";
                },
                set : function ( e ) {
                    this.changeHandler( e );
                    this.$emit( "input", e );
                }
            }
        },
        methods  : {
            changeHandler : function ( val ) {
                this.$emit( "change", val );
                if ( this.rule ) this.$nextTick( function () {
                    this._check( val );
                } )
            }
        },
    } );
    
    /**
     * 分页
     vue-pagination 分页组件
     cur：当前页
     all ：全部页
     */
    Vue.component( 'vue-pagination', {
        props    : ['cur', 'all'],
        template : '<div class="page-bar"><ul v-if="all>1"> <li v-if="all>1"><a  class="setButtonClass(newcur)" @click="prvePage(newcur)">上一页</a></li><li v-for="index in indexs"  v-bind:class="{ active: newcur == index }"><a @click="btnClick(index)">{{ index < 1 ? "..." : index }}</a></li><li v-if="all>1"><a  @click="nextPage(newcur)">下一页</a></li></ul></div>',
        data     : function () {
            return { newcur : this.cur }
        },
        computed : {
            indexs : function () {
                var left = 1
                var right = this.all
                var ar = []
                if ( this.all >= 11 ) {
                    if ( this.newcur > 5 && this.newcur < this.all - 4 ) {
                        left = this.newcur - 5
                        right = this.newcur + 4
                    } else {
                        if ( this.newcur <= 5 ) {
                            left = 1
                            right = 10
                        } else {
                            right = this.all
                            left = this.all - 9
                        }
                    }
                }
                while ( left <= right ) {
                    ar.push( left )
                    left++
                }
                if ( ar[0] > 1 ) {
                    ar[0] = 1;
                    ar[1] = -1;
                }
                if ( ar[ar.length - 1] < this.all ) {
                    ar[ar.length - 1] = this.all;
                    ar[ar.length - 2] = 0;
                }
                return ar
            }
        },
        methods  : {
            btnClick       : function ( data ) {
                if ( data < 1 ) return;
                if ( data != this.newcur ) {
                    this.newcur = data
                    this.$emit( 'btn-click', data )
                }
            },
            nextPage       : function ( data ) {
                if ( this.newcur >= this.all ) return;
                this.btnClick( this.newcur + 1 );
            },
            prvePage       : function ( data ) {
                if ( this.newcur <= 1 ) return;
                this.btnClick( this.newcur - 1 );
            },
            setButtonClass : function ( isNextButton ) {
                if ( isNextButton ) {
                    return this.newcur >= this.all ? "page-button-disabled" : ""
                }
                else {
                    return this.newcur <= 1 ? "page-button-disabled" : ""
                }
            }
        }
    } )
    /**
     * 日期选择
     vue-date 时间组件
     date：默认获取当前时间
     plural：多个时间传递input框绑定的数据名称  //参考发货列表
     @riqi ：选择日期回调
     timehsm ：时分秒  Boolean 默认：false
     max-date : 最大日期 格式//2018-01-12
     min-date : 最小日期
     */
    Vue.component( 'vue-date', {
        props      : {
            date        : { type : [Number, String], default : new Date().getTime() + "" },
            plural      : { type : String, default : "" },
            timehsm     : { type : Boolean, default : !1 },
            placeholder : { type : String, default : '请选择日期' },
            maxDate     : { type : String, default : '' },
            minDate     : { type : String, default : '' },
        },
        mixins     : [inputMixin],
        directives : { ClickOutside : ClickOutside },
        template   : (
            "    <div class='u-box' v-click-outside=\"clickOutSideHandler\" :style='{width:width,margin:margin}'>\n" +
            "       <div class=\"u-dt-input-wrap\">\n" +
            "           <input type=\"text\" readonly=\"readonly\" class=\"input u-dt-middle-input\" :placeholder=\"placeholder\" @focus=\"show=true\" v-model.trim=\"sel\"><i @click=\"show=true\" class=\"icon2-rili\"></i>\n" +
            "       </div>\n" +
            "       <div class=\"u-dt-box\" v-show=\"show\">\n" +
            "       <div class=\"u-dt-header\">" +
            "           <a class=\"u-dt-h-1\" @click=\"pickYear(-1)\">«</a>\n" +
            "           <a class=\"u-dt-h-2\" @click=\"pickMonth(-1)\">‹</a>\n" +
            "           <span class=\"u-dt-ym\">{{y}}年 {{m}}月</span>\n" +
            "           <a class=\"u-dt-h-3\" @click=\"pickMonth(1)\">›</a>\n" +
            "           <a class=\"u-dt-h-4\" @click=\"pickYear(1)\">»</a>\n" +
            "       </div>\n" +
            "       <table class=\"u-dt-table\">\n" +
            "           <thead><tr><th v-for=\"day in days\"><span>{{day}}</span></th></tr></thead>\n" +
            "           <tbody><tr v-for=\"cell in data\"><td v-for=\"item in cell\":class=\"{'u-dt-last': m!== item.month, 'u-dt-today': cur === item.data, 'u-dt-select': ym === item.data}\"><span :class=\"maxDate!=''&&new Date(maxDate).getTime()<new Date(item.data).getTime()?'u-dt-last-span':(minDate!=''&& new Date(minDate).getTime()>new Date(item.data).getTime())?'u-dt-last-span':''\"  @click=\"this.maxDate!=''&&new Date(maxDate).getTime()<new Date(item.data).getTime()?'':this.minDate!=''&&new Date(minDate).getTime()>new Date(item.data).getTime()?'':pickConform(item)\">{{ item.day }}</span></td></tr></tbody>\n" +
            "       </table>\n" +
            "       <div class=\"u-dt-footer\" v-if='timehsm' ><div class=\"u-ymd-box\"><input @input='keyup(\"h\")'  @blur='inputBlur(\"h\")'  type=\"type\" maxlength=\"2\" v-model=\"hms.h\">-<input maxlength=\"2\" @input='keyup(\"m\")'  @blur='inputBlur(\"m\")' v-model=\"hms.m\">-<input maxlength=\"2\" @input='keyup(\"s\")' v-model=\"hms.s\" @blur='inputBlur(\"s\")'></div><p v-if='false' href=\"javascript:\">{{sel}}</p><span class=\"btn btn-ok\" @click=\"confirm\">确定</span></div>\n" +
            "       </div></div>\n"
        ),
        data() {
            var days = ['一', '二', '三', '四', '五', '六', '日'];
            var d = '', sel = '';
            var len = ('' + this.date).length;
            
            function t( s ) {return s < 10 ? '0' + s : s;}
            
            if ( !this.date || (len != 13 && len != 10) ) {
                d = new Date()
            } else {
                d = len == 13 ? new Date( parseInt( this.date ) ) : new Date( this.date * 1000 )
            }
            if ( Object.prototype.toString.call( d ) === "[object Date]" ) {
                if ( isNaN( d.getTime() ) ) {
                    d = new Date()
                } else {
                    if ( this.timehsm ) {
                        sel = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + '  ' + t( d.getHours() ) + ':' + t( d.getMinutes() ) + ':' + t( d.getSeconds() );
                        
                    }
                    
                    if ( this.timehsm ) {
                        var dataObj = {
                            time : sel,
                            name : this.plural
                        }
                        this.plural == '' ? this.$emit( 'riqi', sel ) : this.$emit( 'riqi', dataObj );
                    } else {
                        sel = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate()
                    }
                    
                }
            } else {
                d = new Date()
            }
            if ( !this.date ) {
                sel = ''
            }
            var curTime = new Date();
            var cur = "" + curTime.getFullYear() + '-' + (curTime.getMonth() + 1) + '-' + curTime.getDate();//当前日期
            var y = d.getFullYear();
            var m = d.getMonth() + 1;
            var h = d.getHours();
            var mt = d.getMinutes();
            var s = d.getSeconds();
            var data = getCalendar( d.getFullYear(), d.getMonth() + 1 );
            return {
                days : days,
                cur  : cur,
                sel  : sel,
                ym   : '',
                y    : y,
                m    : m,
                h    : h,
                mt   : mt,
                s    : s,
                hms  : {
                    h : t( h ),
                    m : t( mt ),
                    s : t( s )
                },
                data : data,
                show : false
            }
        },
        methods    : {
            clickOutSideHandler : function () {
                this.show = false;
            },
            pickMonth( flag ) {
                if ( flag == -1 ) {
                    this.data = (this.m === 1) ?
                        getCalendar( parseInt( this.y-- ), this.m = 12 ) :
                        getCalendar( parseInt( this.y ), parseInt( this.m += flag ) );
                } else {
                    this.data = (this.m === 12) ?
                        getCalendar( parseInt( this.y++ ), this.m = 1 ) :
                        getCalendar( parseInt( this.y ), parseInt( this.m += flag ) );
                }
            },
            pickYear( flag ) {
                this.data = getCalendar( parseInt( this.y += flag ), parseInt( this.m ) );
            },
            pickConform( item ) {
                this.ym = item.data;
                if ( this.timehsm ) {
                    
                } else {
                    this.sel = item.data;
                    var dataObj = {
                        time : item.data,
                        name : this.plural
                    }
                    this.plural == '' ? this.$emit( 'riqi', item.data ) : this.$emit( 'riqi', dataObj );
                    this.show = false;
                }
                
            },
            keyup               : function ( e ) {
                e == 'h' && Number( this.hms.h ) > 23 ? (alert( '小时请输入0~23' ), this.hms.h = this.h) : e == 'm' && Number( this.hms.m ) > 59 ? (alert( '分钟请输入0~59' ), this.hms.m = this.mt) : e == 's' && Number( this.hms.s ) > 59 ? (alert( '秒请输入0~59' ), this.hms.s = this.s) : '';
            },
            confirm() {
                this.sel = this.ym != '' ? this.ym + '   ' + this.hms.h + ':' + this.hms.m + ':' + this.hms.s : this.cur + '   ' + this.hms.h + ':' + this.hms.m + ':' + this.hms.s;
                var dataObj = {
                    time : this.sel,
                    name : this.plural
                }
                this.plural == '' ? this.$emit( 'riqi', this.sel ) : this.$emit( 'riqi', dataObj );
                this.show = false;
            },
            inputBlur           : function ( e ) {
                10 > Number( this.hms[e] ) && this.hms[e].length < 2 && this.hms[e] != '' ? this.hms[e] = '0' + this.hms[e] : this.hms[e] == '' ? this.hms[e] = '00' : ''
            }
        }
    } );
    
    /**
     * 浮动面板
     */
    Vue.component( "u-popover", {
        template : (
            "<div data-v-1c132c38 :style=\"{ left : left, top : top, bottom : bottom, right : right, width : width,'z-index' : zIndex }\" class=\"u-popover\">\n" +
            "  <div data-v-1c132c38 class=\"u-popover-content\">\n" +
            "    <div data-v-1c132c38 v-if=\"arrowed\" :class=\"['u-popover-arrow-'+direction,'u-popover-arrow']\"></div>\n" +
            "    <div data-v-1c132c38 class=\"u-popover-inner\">\n" +
            "      <div data-v-1c132c38 v-if=\"titled\" class=\"u-popover-title\">\n" +
            "        <slot name=\"title\"></slot>\n" +
            "      </div>\n" +
            "      <div data-v-1c132c38 v-if=\"contented\" class=\"u-popover-inner-content\">\n" +
            "        <slot name=\"content\"></slot>\n" +
            "      </div>\n" +
            "      <slot v-else></slot>\n" +
            "    </div>\n" +
            "  </div>\n" +
            "</div>"
        ),
        props    : {
            titled    : { type : Boolean, default : !1 },
            contented : { type : Boolean, default : !1 },
            arrowed   : { type : Boolean, default : !0 },
            direction : { type : String, default : "down" },
            top       : { type : String, default : null },
            left      : { type : String, default : null },
            bottom    : { type : String, default : null },
            right     : { type : String, default : null },
            width     : { type : String, default : null },
            zIndex    : { type : String, default : "" }
        }
    } );
    
    /**
     * 下拉选项选择框
     */
    Vue.component( "u-selection", {
        template   : (
            "<div data-v-1c132c39 v-click-outside=\"clickOutSideHandler\" class=\"selection-warp clearfix\">\n" +
            "  <input data-v-1c132c39 :disabled='disabled' :style='{ width : width, margin : margin, padding : padding }' " +
            "type=\"text\" @focus=\"show = true\" :value='getTextLabel' class=\"ipt\">\n" +
            "  <u-popover top='95%' v-show=\"show\">\n" +
            //"    <div data-v-1c132c39 class=\"selection-content\"><ul data-v-1c132c39><slot></slot></ul></div>\n" +
            "    <ul data-v-1c132c39 class=\"selection-content\"><slot></slot></ul>\n" +
            "  </u-popover>\n" +
            "  <div v-if='!!message' class='ipt-explain'>{{message}}</div>\n" +
            "</div>"
        ),
        props      : {
            value      : null,
            name       : String,
            labelValue : String
        },
        directives : { ClickOutside : ClickOutside },
        mixins     : [inputMixin],
        data       : function () { return { selected : null, show : false }},
        methods    : {
            clickOutSideHandler : function () {
                this.show = false;
            },
            loadChildren        : function ( opt ) {
                if ( opt.hasChild && (!opt.hasOptions || !opt.lazy) ) {
                    this.$emit( "load-child", opt.getValue, function ( list ) {
                        opt.options = list;
                    } )
                }
            }
        },
        created    : function () {
            this.selected = this.labelValue;
        },
        watch      : {
            labelValue : function ( v ) {
                this.selected = v;
            }
        },
        computed   : {
            getTextLabel : function () {return this.selected;},
            modelValue   : {
                get : function () {
                    return this.value;
                },
                set : function ( val ) {
                    this.$emit( "input", val );
                    if ( this.rule ) this.$nextTick( function () {
                        this._check( val );
                    } )
                }
            }
        }
    } );
    
    /**
     * 层级下拉选项
     */
    Vue.component( "u-selection-option", {
        template : (
            "<li data-v-1c132c40>\n" +
            "    <div data-v-1c132c40 class=\"selection-option\">\n" +
            "        <i data-v-1c132c40 v-if='hasChild' @click.stop='show = false' class=\"selection-opts-open\" v-show='show'></i>\n" +
            "        <i data-v-1c132c40 v-if='hasChild'  @click.stop='openHandler(show = true)' class=\"selection-opts-close\" v-show='!show'></i>\n" +
            "        <label data-v-1c132c40>\n" +
            "            <input data-v-1c132c40 type=\"radio\" :value='getValue' @click.stop='clickHandler' v-model='currentVal' :name=\"name\">\n" +
            "            <i data-v-1c132c40 class=\"selection-opt-radio\"></i>\n" +
            "            <span data-v-1c132c40>{{getLabel}}</span>\n" +
            "        </label>\n" +
            "    </div>\n" +
            "    <ul data-v-1c132c40 v-show='show' v-if='hasChild && hasOptions' :style=\"{'padding-left': (30*level)+'px'}\">" +
            "      <u-selection-option :level='level+1' :value='value' :label='label' v-for='o in options' :option='o' :child='child'></u-selection-option>" +
            "    </ul>" +
            "</li>\n"
        ),
        props    : {
            lazy   : { type : Boolean, default : true },
            value  : { type : String, default : "value" },
            label  : { type : String, default : "label" },
            child  : { type : String, default : !1 },
            level  : { type : Number, default : 1 },
            option : Object
        },
        data     : function () { return { name : null, options : null, show : false } },
        created  : function () {
            var parent = this.$$parent( "u-selection" );
            if ( parent ) (this.name = (this._parent = parent).name);
        },
        computed : {
            hasChild   : function () {
                return this.option ? !!this.option[this.child] : false;
            },
            getValue   : function () {
                return this.option ? this.option[this.value] : "";
            },
            getLabel   : function () {
                return this.option ? this.option[this.label] : "";
            },
            currentVal : {
                get : function () {
                    var value = this._parent ? this._parent.modelValue : '';
                    if ( value === this.getValue && this._parent ) this._parent.selected = this.getLabel;
                    return value;
                },
                set : function ( value ) {
                    if ( this._parent ) this._parent.modelValue = value;
                }
            },
            hasOptions : function () {
                return this.options && this.options.length;
            }
        },
        methods  : {
            openHandler  : function () {
                if ( this._parent ) this._parent.loadChildren( this );
            },
            clickHandler : function () {
                if ( this._parent ) this._parent.clickOutSideHandler();
            }
        }
    } );
    // 物流跟踪
    /*
        setp-data:数据
        lfct：左边取值的名称//参考发货详情时间进度
        ctct：右边取值名称
        send-status：判断是否显示右侧进度操作，默认false
    */
    Vue.component( 'vue-step', {
        template : (
            "<ul class=\"u-sch-box\">\n" +
            "<li v-for=\"(item,index) in setpData\" :key='index' :style=\"setpData.length-1==index?'border-color:transparent':''\">\n" +
            "        <i class=\"icon-yuan\" :style=\"index==0?'background:#01b485':''\"></i>\n" +
            "        <span class=\"u-sch-list-left\">{{item[lfct]}}</span>\n" +
            "        <span class=\"u-sch-list-center\">{{item[ctct]}}</span><slot v-if=\"index==0&&sendStatus\"></slot>\n" +
            "    </li>\n" +
            "</ul>\n"
        ),
        props    : {
            lfct       : String,
            ctct       : String,
            setpData   : Array,
            sendStatus : { type : Boolean, default : !1 },
            itop       : { type : String, default : "" },
        },
        watch    : {
            setpData : function ( v ) {console.log( v )}
        },
        created  : function () {
            // console.log( this )
            // console.log( 1111 )
        }
        
    } );
    //弹出层
    /*
        vue-layer 弹出层
            height：高 
            widht :宽 
            lyTitle：'标题'
            showName : 多个图层拥有判断的名称参考发货详情
    */
    Vue.component( 'vue-layer', {
        template : (
            "<div class=\"u-layer-box\">\n" +
            "    <div class=\"u-layer-centered\">\n" +
            "        <div class=\"u-layer-body\" :style='{width:width,height:height}'>\n" +
            "            <p class=\"u-layer-title\">{{lyTitle}}<i class=\"icon2-cha1\" @click='close'></i></p>\n" +
            "            <div class=\"u-layer-content\"> <slot></slot></div>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>"
        ),
        props    : {
            height   : { type : String, default : '280px' },
            showName : '',
            lyTitle  : ''
        },
        mixins   : [inputMixin],
        data() {
            return {
                show : true
            }
        },
        methods  : {
            close : function () {
                this.$emit( 'show', this.showName )
            }
        }
        
    } )
    
    /**
     * 联想搜索
     */
    Vue.component( "u-association", {
        template   : (
            "<div data-v-1c132c41 @keydown.stop='keydownHandler' v-click-outside='outsideHandler' class=\"association-warp\">\n" +
            "   <input data-v-1c132c41 :placeholder='placeholder' @blur.stop='blurHandler' type=\"text\" :value='getInputText' @input.stop='inputHandler' :style='{ width : width, margin : margin, padding : padding }' class=\"ipt\" title=\"\">\n" +
            "   <u-popover v-show='show'>\n" +
            "     <ul data-v-1c132c41 class=\"association-content\">\n" +
            "       <li data-v-1c132c41 v-for='(opt,index) in eachOptions' v-html='opt.h' @click.stop='clickHandler(opt)' :class='{hover:(index === hover)}'></li>\n" +
            "     </ul>\n" +
            "   </u-popover>\n" +
            "   <div v-if='!!message' class='ipt-explain'>{{message}}</div>\n" +
            "</div>"
        ),
        mixins     : [inputMixin],
        props      : {
            labelProp : String,
            valueProp : String,
            label     : String
        },
        directives : { ClickOutside : ClickOutside },
        data       : function () {
            return {
                show    : false,
                old     : null,
                options : null,
                keyWord : null,
                hover   : 0
            }
        },
        computed   : {
            getInputText : function () {
                return this.keyWord || this.modelValue;
            },
            modelValue   : {
                get : function () {return this.keyWord || this.value},
                set : function ( value ) {
                    this.$emit( "input", value );
                }
            },
            eachOptions  : function () {
                var that = this, keyword = this.keyWord, reg = new RegExp( keyword, "g" );
                return (this.options || []).map( function ( opt ) {
                    var l = that.getLabel( opt );
                    var h = that.createOption( keyword, l, reg );
                    return { l : l, h : h, opt : opt };
                } );
            },
            getLength    : function () {
                return this.options ? this.options.length : 0
            }
        },
        methods    : {
            keydownHandler : function ( e ) {
                var keyCode = e.keyCode, index, l;
                if ( this.show && this.getLength ) {
                    l = this.getLength - 1;
                    if ( keyCode === 38 ) {
                        index = this.hover - 1;
                        this.hover = index < 0 ? l : index;
                        // 下
                    } else if ( keyCode === 40 ) {
                        index = this.hover + 1;
                        this.hover = index > l ? 0 : l;
                        // 回车
                    } else if ( keyCode === 108 || keyCode === 13 ) {
                        this.clickHandler( this.eachOptions[this.hover] );
                    }
                }
            },
            blurHandler    : function () {
                if ( this.rule ) this.$nextTick( function () {
                    this._check( this.keyWord );
                } );
            },
            clickHandler   : function ( opt ) {
                this.modelValue = opt.l;
                this.keyWord = opt.l;
                this.$emit( "change", opt.opt );
                this.show = false;
            },
            outsideHandler : function () {
                this.show = false;
            },
            createOption   : function ( key, text, reg ) {
                var res = "<span>" + key + "</span>";
                return text.replace( reg, function () {
                    return res;
                } );
            },
            inputHandler   : function ( e ) {
                var key = this.keyWord = this.modelValue = e.target.value;
                this.show = true;
                if ( this.old !== key ) {
                    this.loadOptions( key );
                    this.old = key;
                }
            },
            loadOptions    : function ( key ) {
                var that = this;
                if ( isD( key ) ) {
                    this.$emit( "load", key, function ( res ) {
                        that.options = res;
                    } );
                } else {
                    this.options = [];
                    this.show = false;
                }
            },
            getLabel       : function ( opt ) {
                return opt[this.labelProp];
            }
        }
    } );
    
} );
(function ( udf ) {
    
    var utils = Vue.$utils;
    var isD = utils.isDefined;
    var to = utils.toString;
    var isF = utils.isFunction;
    
    function isArray( a ) { return Array.isArray( a )}
    
    function merge( target, fro, needed, exclude ) {
        needed = isArray( needed ) ? needed : needed ? needed.split( "," ) : !1;
        exclude = isArray( exclude ) ? exclude : exclude ? exclude.split( "," ) : !1;
        if ( target && fro ) for ( var key in fro ) if ( (needed && needed.indexOf( key ) > -1) || (exclude && exclude.indexOf( key ) < 0) ) target[key] = fro[key];
        return target;
    }
    
    function mapString( str ) {
        var map = {};
        if ( str && str.length ) str.split( "," ).forEach( function ( v ) { map[v] = true } );
        return function ( p ) { return map[p]; };
    }
    
    var telephoneReg = /^(?:\d{3,4}[-])?(?:\d{7}|(?:1\d{10}))$/;
    var idCardReg = /^(?:\d{15})|(?:\d{18})|(?:\d{17}[xX])$/;
    Vue.$rules = {// 验证规则缓存
        // 非空校验
        UnEmpty      : function ( v, c ) {
            c( isD( to( v ).trim() ) )
        },
        // 电话校验//座机和手机
        telephone    : function ( v, c ) {
            c( telephoneReg.test( v ) );
        },
        // 要么为空要么正确填写座机和手机
        "?telephone" : function ( v, c ) {
            c( !isD( v ) || telephoneReg.test( v ) );
        },
        // 身份证校验15位和18位
        idCard       : function ( v, c ) {
            c( idCardReg.test( v ) );
        },
        // 要么为空要么正确填写身份证
        "?idCard"    : function ( v, c ) {
            c( !isD( v ) || idCardReg.test( v ) );
        }
    };
    
    //
    utils.merge = merge;
    utils.mapString = mapString;
    utils.isArray = isArray;
    
})();

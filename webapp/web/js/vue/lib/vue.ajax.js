var vpost = function ( url, data, callbackFun, errorFun ) {
    axios.post( url, data ).then( function ( response ) {
        callbackFun( response );
    }, function ( data ) {
        return Promise.reject( data );
    } ).catch( function ( error ) {
        console.log( error );
        
        if ( typeof (errorFun) != 'undefined' ) {
            errorFun( error );
        }
    } );
}
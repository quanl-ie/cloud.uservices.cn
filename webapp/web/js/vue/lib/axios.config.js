// axios异步请求、响应统一处理
(function () {
    function mergeParam( data ) {
        var post = new FormData();
        if ( typeof (data) === "object" && data !== null ) {
            Object.keys( data ).forEach( function ( key ) {
                post.append( key, data[key] )
            } );
        }
        return post;
    }
    
    function noop() {}
    
    // 添加请求拦截器
    axios.interceptors.request.use( function ( config ) {
        // 在发送请求之前做些什么
        return config;
    }, function ( error ) {
        // 对请求错误做些什么
        return Promise.reject( error );
    } );
    
    // 添加响应拦截器
    axios.interceptors.response.use( function ( response ) {
        var data;
        // 入股axios响应成功，提取服务返回的数据
        if ( data = response.data ) {
            // 如果业务逻辑响应成功
            if ( data.success ) {
                return data.data;
            } else if ( data.message ) {
                return Promise.reject( data );
            }
        }
        return Promise.reject( response );
    }, function ( error ) {
        // 对响应错误做点什么
        console.log( error );
        return Promise.reject( error );
    } );
    
    /**
     * 异步上传文件
     * @param url
     * @param data
     * @param progress
     * @returns {*}
     */
    axios.fileUpload = function ( url, data, progress ) {
        var notify = {};
        data = mergeParam( data );
        progress = typeof progress === "function" ? progress : noop;
        return axios.post( url, data, {
            // 设置http请求头 ，以表单传数据的格式来传递formData
            headers          : { 'Content-Type' : 'multipart/form-data' },
            // 文件上传进度事件
            onUploadProgress : function ( event ) {
                if ( event.lengthComputable ) {
                    // 计算上传百分比
                    notify.complete = (event.loaded / event.total * 100 | 0);
                    notify.event = event;
                    // 通过notify发出通知
                    progress( notify );
                }
            }
        } ).then( function ( data ) {
            if ( data.url ) return Promise.resolve( data );
        }, function ( resp ) {
            if ( resp.data && resp.data.url ) {
                return Promise.resolve( resp.data );
            }
        } );
    };
})();

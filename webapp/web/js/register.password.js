;(function ( window, $, jQuery, UDF ) {
    var REG_BLANK = /^\s*$/;
    var isFunction = jQuery.isFunction;
    var isDefined = function isDefined( d ) {return d !== null && d !== UDF};
    var isArray = function isArray( a ) {return Array.isArray( a );};
    var isString = function isString( s ) {return typeof s === "string";};
    var isBlank = function isBlank( s ) {return REG_BLANK.test( s );};
    // 初始化操作。
    var $$proving = (function () {
        var inputArray = [];
        var scopeMap = {};
        var collectPromise = jQuery.Callbacks();
        var globalApi = {};
        
        function noopProve( _, check ) {
            check( true );
        }
        
        function noop() {}
        
        function createBind( bind ) {
            return isFunction( bind )
                ? bind
                : isString( bind )
                    ? function ( input, api ) { input.on( bind, api.prove ) }
                    : noop;
        }
        
        function isInvalidPromise( promise ) {
            return !promise || promise.state() !== "pending";
        }
        
        function createScope( input, option ) {
            var warp = input.parents( ".ipt-group" );
            var explain = warp.find( ".ipt-explain" );
            var bind = createBind( option.bind );
            var proveFn = isFunction( option.prove ) ? option.prove : noopProve;
            var scopeApi = Object.create( globalApi );
            var promise;
            
            function error( message ) {
                message = isDefined( message ) ? message : "";
                warp.removeClass( "ipt-done" ).addClass( "ipt-error" );
                explain.empty().append( "<i class=\"fa fa-fw fa-times-circle\"></i>" + (message) );
                return message;
            }
            
            function success() {
                warp.removeClass( "ipt-error" ).addClass( "ipt-done" );
                explain.empty().append( "<i class=\"fa fa-fw fa-check-circle\"></i>" );
            }
            
            function prove( list ) {
                var deferred;
                if ( isInvalidPromise( promise ) ) {
                    deferred = jQuery.Deferred();
                    promise = deferred.promise();
                    proveFn( input.val(), function ( flag, message ) {
                        message = typeof flag !== "boolean" ? flag : message;
                        flag = flag === true;
                        flag ? deferred.resolve() : deferred.reject( message );
                        return promise;
                    }, scopeApi );
                    promise.done( success ).fail( error );
                }
                if ( isArray( list ) ) list.push( promise );
                return promise;
            }
            
            scopeApi.prove = prove;
            scopeApi.value = function () {
                return input.val();
            };
            scopeApi.rejected = function ( message ) {
                promise = jQuery.Deferred( function ( d ) {
                    d.reject( error( message ) );
                } );
            };
            bind( input, scopeApi );
            collectPromise.add( prove );
            return scopeApi;
        }
        
        // 收集需要校验的元素
        function collect( inputs, options ) {
            var inputMap = {};
            inputs.each( function () {
                var getJquery, input = $( this ),
                    name = input.prop( "name" ),
                    option = options[name];
                if ( isDefined( name ) && option ) {
                    if ( !inputMap[name] ) {
                        getJquery = inputMap[name] = jQuery();
                        inputArray.push( {
                            name   : name,
                            input  : getJquery,
                            option : option
                        } );
                    }
                    getJquery.push( this );
                }
            } );
        }
        
        // 初始化绑定并创建验证作用域
        function bindProve() {
            for ( var name, option, i = 0, l = inputArray.length; i < l; i++ ) {
                option = inputArray[i];
                name = option.name;
                scopeMap[name] = createScope( option.input, option.option, name );
            }
        }
        
        function assert( name ) {
            if ( !scopeMap[name] ) throw new Error( "找不到name为" + name + "的表单元素。" );
        }
        
        // 使某个表单验证错误
        globalApi.rejected = function ( name, message ) {
            assert( name );
            (scopeMap[name]).rejected( message );
        };
        //获取value
        globalApi.getValue = function ( name ) {
            assert( name );
            return (scopeMap[name]).value();
        };
        //手动触发校验
        globalApi.proveBy = function ( name ) {
            assert( name );
            return (scopeMap[name]).prove();
        };
        
        //
        return function ( form, options ) {
            collect( form.find( ".ipt:input" ), options );
            bindProve();
            return {
                rejected : globalApi.rejected,
                getValue : globalApi.getValue,
                proveBy  : globalApi.proveBy,
                proveAll : function () {
                    var promises = [];
                    collectPromise.fire( promises );
                    return jQuery.when.apply( null, promises );
                }
            };
        }
    }());
    
    var toString = Object.prototype.toString;
    //
    jQuery.fn.proving = function ( options ) {
        var form = this[0];
        if ( toString.call( form ) !== "[object HTMLFormElement]" ) {
            throw new Error( "jQuery.fn.proving需要选中一个form标签。" );
        }
        form = jQuery( form );
        return $$proving( form, options );
    };
    jQuery.$$util = {
        isDefined : isDefined,
        isArray   : isArray,
        isString  : isString,
        isBlank   : isBlank
    };
    
}( window, jQuery, jQuery ));
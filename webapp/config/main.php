<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-webapp',
    'defaultRoute' => 'site',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'webapp\controllers',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => ['log'],
    'modules' => [
        'redactor' => [
        'class' => 'yii\redactor\RedactorModule',
        'uploadDir' => '../../webapp/uploads',
        'uploadUrl' => 'http://192.168.0.164:8084',
        'imageAllowExtensions'=>['jpg','jpeg','png','gif']
        ],
    ],
    'language' =>'zh-CN',
    'timeZone' => 'Asia/Shanghai',
    'components' => [
        'request' => [
            // 'csrfParam' => '_csrf-webapp',
            'enableCsrfValidation' => true,
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
			'identityCookie' => ['name' => 'fws', 'httpOnly' => true],
            'idParam' => 'fws'
        ],
        'session' => [
            'class' => 'yii\redis\Session',
            'timeout'      => '84600000',

        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        // 'errorHandler' => [
            // 'errorAction' => 'site/error',
        // ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],

        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [],  // 去除 jquery.js
                    'sourcePath' => null,  // 防止在 frontend/web/asset 下生产文件
                ],
            ],
        ],

    ],
    'params' => $params,
];

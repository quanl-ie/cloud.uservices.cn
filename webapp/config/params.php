<?php
return [
    'adminEmail' => 'admin@example.com',
    // 图片服务器的域名设置，拼接保存在数据库中的相对地址，可通过web进行展示
    //'domain' => 'http://192.168.0.164:8082/',
    'domain' => 'http://manufactor.dev/',
    'token'      => '123456',
    //短信接口域名
    'sendMsg' => [
        'url'=>'http://192.168.0.154:8085/notice/sms-single',
        'sendSource' => 'serviceChannels',
        'sendType' => [
            'register'=>'channelsRegister',
            'forgetPwd'=>''
            ]
    ],
    'webuploader' => [
        // 后端处理图片的地址，value 是相对的地址
        'uploadUrl' => 'upload/upload',
        // 多文件分隔符
        'delimiter' => ',',
        // 基本配置
        'baseConfig' => [
            //'defaultImage' => 'http://192.168.0.164:8082/it/u=2056478505,162569476&fm=26&gp=0.jpg',
            'defaultImage' => 'http://images.dev.com/it/0.jpg',
                'disableGlobalDnd' => true,
            'accept' => [
                'title' => 'Images',
                'extensions' => 'gif,jpg,jpeg,bmp,png',
                'mimeTypes' => 'image/jpg,image/jpeg,image/png',
            ],
            'pick' => [
                'multiple' => false,
            ],
        ],
    ],
    'imageUploadRelativePath' => 'upload', // 图片默认上传的目录
    'imageUploadSuccessPath' => 'upload', // 图片上传成功后，路径前缀 
    'apiUrl'=>'http://192.168.0.231:10001/index/Createorder/createOrder',           //下单api地址
    'ossUrl'=>'http://imageservice.uservices.cn/index/oss/index',   //图片服务器oss 地址
    'pageSize'=>'4',
    'PositionURl'=>'http://position.uservices.cn/index/index', //技师打点地址
    'v' => date('YmdH') . '1.00.1'
];

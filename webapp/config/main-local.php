<?php
//test
$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'RcQOh0l7cbk1dgyoU-6mO9Oq7zPT1zPM',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' =>false,//这句一定有，false发送邮件，true只是生成邮件在runtime文件夹下，不发邮件
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.uservices.cn',  //每种邮箱的host配置不一样
                'username' => 'manufactor@uservices.cn',
                'password' => 'Qwer1234$',
                'port' => '587',
                'encryption' => '',
                //'sourceIp'=>'10.147.42.198'

            ],
            'messageConfig'=>[
                'charset'=>'UTF-8',
                'from'=>['manufactor@uservices.cn'=>'admin']
            ],
        ],
    ],
];

if (YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;

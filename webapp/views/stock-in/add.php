<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use webapp\models\BrandQualification;
    use yii\helpers\ArrayHelper;
    /* @var $this yii\web\View */
    /* @var $model webapp\models\BrandQualification */
    $this->title = '创建入库单';
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">创建入库单</span>
</div>
<!--引入CSS-->
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/webuploader/webuploader.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jquery.autocomplete.css');?>">
<!--引入JS-->
<?=Html::jsFile('@web/webuploader/webuploader.js')?>
<?=Html::jsFile('@web/js/bootstrap-select.js')?>
<style>
td{position: relative}
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body"><?php
                        $form = ActiveForm::begin([
                            'options' => [
                                'class' => 'form-horizontal',
                                'id' => 'stockIn',
                                'name' => 'form1',
                                'enctype' => 'multipart/form-data',
                            ],
                            'fieldConfig' => [
                                'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-xs-6\">{input}{error}</div>",
                                'labelOptions' => ['class' => 'col-xs-2 control-label text-right'],
                            ]
                        ]);
                    ?>
                    <div class="col-md-12">
                        <h5 class="block-h5 block-h5one">入库单信息</h5>
                    </div>

                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            入库单号：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <input type="hidden" name="type" value="<?=$data['type'] ?>">
                            <input type="text" name="no" class="form-control" id="no" value="<?=$data['no'] ?>" readonly="readonly"   placeholder="">
                        </div>

                        <div class="col-md-offset-2 col-md-6 misplacement" style="display:none" id="subjectct_msg">
                        </div>
                    </div>
                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            入库主题：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <input type="text" name="subject" class="form-control" maxlength="30" id="subject" value=""  placeholder="" >
                        </div>
                    </div>

                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 入库类型：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <select name="stock_type" id="stock_type" class="form-control" datatype="*" <?php if (isset($data['stock_type'])) : ?> disabled="disabled" <?php endif; ?> sucmsg=" " nullmsg="请选择入库类型" errormsg="请选择入库类型">

                                <option value="">选择类型</option>
                                <?php if ($stockType) : ?>
                                <?php foreach ($stockType as $key => $val) : ?>
                                <option value="<?=$key ?>" <?php if (isset($data['stock_type']) && $data['stock_type'] == $key) : ?> selected="selected" <?php endif; ?>><?=$val ?></option>
                                <?php endforeach; ?>
                                <?php endif; ?>

                            </select>
                        </div>

                        <div class="col-md-offset-2 col-md-6 misplacement" style="display:none" id="subjectct_msg">
                        </div>
                    </div>

                    <?php if (isset($data['rel_type'])) : ?>
                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            关联单：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <input type="hidden" name="rel_type" value="<?=$data['rel_type'] ?>">
                            <input type="hidden" name="rel_id" value="<?=$data['rel_id'] ?>">
                            <input type="hidden" name="rel_name" value="<?=$data['rel_name'] ?>">
                            <?php if($data['rel_type'] == 4){?>
                                <a href="JavaScript:void(0)"
                                   onclick='openPage("/exchange-goods/detail?id=<?=$data['rel_id'];?>","换货详情")'>
                                    <?=isset($data['rel_name'])?$data['rel_name']:"" ?>
                                </a>
                            <?php }elseif($data['rel_type'] == 2){?>
                                <a href="JavaScript:void(0)"
                                   onclick='openPage("/contract/detail?id=<?=$data['rel_id'];?>&contract_id=<?=$data['rel_id'];?>","合同详情")' class="line-height34">
                                    <?=isset($data['rel_name'])?$data['rel_name']:"" ?>
                                </a>
                            <?php }elseif($data['rel_type'] == 1){?>
                                <a href="JavaScript:void(0)"
                                   onclick='openPage("/purchase/detail?no=<?=$data['rel_no'];?>&t=view","采购单详情")' class="line-height34">
                                    <?=isset($data['rel_name'])?$data['rel_name']:"" ?>
                                </a>
                            <?php }elseif($data['rel_type'] == 3){?>
                                <a href="JavaScript:void(0)"
                                   onclick='openPage("/return-goods/detail?id=<?=$data['rel_id'];?>","退货单详情")' class="line-height34">
                                    <?=isset($data['rel_name'])?$data['rel_name']:"" ?>
                                </a>
                            <?php }elseif($data['rel_type'] == 6){?>
                                <a href="JavaScript:void(0)"
                                   onclick='openPage("/tech-prod-stock-return/detail?id=<?=$data['rel_id'];?>","详情")' class="line-height34">
                                    <?=isset($data['rel_no'])?$data['rel_no']:"" ?>
                                </a>
                            <?php }?>
                        </div>

                        <div class="col-md-offset-2 col-md-6 misplacement" style="display:none" id="subjectct_msg">
                        </div>
                    </div>
                    <?php endif; ?>



                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            申请人：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <input type="hidden" name="apply_user_id" value="<?=$data['apply_user_id'] ?>">
                            <input type="text" name="apply_user_name" value="<?=$data['apply_user_name'] ?>" class="form-control" readonly="readonly" id="apply_user_name">
                        </div>

                        <div class="col-md-offset-2 col-md-6 misplacement" style="display:none" id="subjectct_msg">
                        </div>
                    </div>

                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            备注：
                        </label>
                        <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7 addinput-list" style="margin-bottom: 30px;">
                            <textarea class="ramark-box" id="ramark" maxlength="200" placeholder="请输入备注信息（200字以内）" name="description" id="description"></textarea>
                            <p class="xianzhi"><span  id='word'>0</span>/200</p>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <h5 class="block-h5">产品明细</h5>
                    </div>

                    <div class="information-list">
                        <div class="col-xs-12 addinput-list" style="margin-left: 30px;margin-bottom: 20px;">
                            <table style="width: 90%;font-size: 12px;">
                                <thead>
                                <tr style="font-size: 14px;">
                                    <th style="width: 50px;" class="padding15">
                                        <input type="checkbox" name="" id="allDetailed">
                                        <label for="allDetailed"> <i class="gou-i"></i></label>
                                    </th>
                                    <th>产品名称</th>
                                    <th>产品编号</th>
                                    <th>产品品牌</th>
                                    <th>产品型号</th>
                                    <th>计量单位</th>
                                    <th width="80px">数量</th>
                                    <th>序列号</th>
                                    <th>库房</th>
                                    <th>批号</th>
<!--                                    <th>生产日期</th>-->
<!--                                    <th>有效日期</th>-->
                                </tr>
                                </thead>
                                <tfoot  id="show_prodcut"  >

                                <?php if(!empty($product)) :?>
                                <?php foreach ($product as $key=>$val): ?>
                                        <?php if((isset($val['type']) && $val['type']==1) || !isset($val['type'])):?>

                                    <tr id="<?=$val['prod_id'];?>">

                                        <td  class="padding15">
                                            <input type="checkbox" class="available_cost_type select_product" name="prod[<?=$val['prod_id']?>][prod_id]"  id="prod_id<?=$val['prod_id']?>" >
                                            <label for="prod_id<?=$val['prod_id']?>"> <i class="gou-i"></label>
                                        </td>
                                        <td>
                                            <input class="prod_id" value="<?=$val['prod_id']?>" name="prod[<?=$val['prod_id']?>][prod_id]"  type="hidden"/><?=$val['prod_name']?>
                                        </td>
                                        <td><?=$val['prod_no']?></td>
                                        <td><?=$val['brand_name']?></td>
                                        <td><?=$val['model']?></td>
                                        <td><?=$val['unit_name']?></td>
                                        <td>
                                            <input class="prod_num" name="prod[<?=$val['prod_id']?>][prod_num]" datatype="n"  sucmsg=" " nullmsg="请输入数量" errormsg="请输入正整数" type="text" value="<?=$val['prod_num'] ?>" maxlength="10" <?php if(isset($data['stock_type']) && $data['stock_type'] == 3){ echo 'readonly="readonly"';} ?>/>
                                            <p class="misplacement" id="prod_id-yz" style="display: none">1122333</p>
                                        </td>
                                        <td>
                                            <input  class="serial_number"  name="prod[<?=$val['prod_id']?>][serial_number]"   type="text" maxlength="100" />
                                        </td>
                                        <td>
                                            <select class="depot_id" name="prod[<?=$val['prod_id']?>][depot_id]" datatype="*"  sucmsg=" " nullmsg="请选择库房" errormsg="请选择库房">
                                                <option value="">请选择</option>
                                                <?php if (!empty($depot)) : ?>
                                                    <?php foreach ($depot as $k => $v) : ?>
                                                        <option value="<?=$k ?>"><?=$v ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input  class="prod_batch"  name="prod[<?=$val['prod_id']?>][prod_batch]"   type="text" maxlength="10" />
                                        </td>
<!--                                        <td>-->
<!--                                            <input class="prod_date" readonly="readonly" name="prod[--><?//=$val['prod_id']?><!--][prod_date]" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" type="text" />-->
<!--                                        </td>-->
<!--                                        <td>-->
<!--                                            <input class="effective_date" readonly="readonly" name="prod[--><?//=$val['prod_id']?><!--][effective_date]" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" type="text"/>-->
<!--                                        </td>-->
                                    </tr>
                                            <?php endif;?>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                <div id="stock_in_msg" class="Validform_checktip Validform_wrong" style="display:none;"></div>
                                </tfoot>
                                <div id="product_hide" class="" style="display:none;"></div>
                            </table>
                            <?php if(isset($data['stock_type'])==false ||(isset($data['stock_type']) && ($data['stock_type']!=1 && $data['stock_type']!=3) )):?>
                            <div class="bottom-btn-box">
                                <a href="javascript:" class="btn btn-orange mgr8" id="addBtn">添加</a>
                                <a class="btn bg-f7 delet-btn" href="javascript:">删除</a>
                            </div>
                            <?php endif;?>
                        </div>
                        <div class="col-md-offset-2 col-md-6 misplacement"  style="display:none" id="brand_msg">
                        </div>
                    </div>
                    <hr style="clear: both;">
                    <div class="information-list" style="text-align: center;">
                            <a href="/stock-in/wait-check"><button type="button"  class="btn bg-f7  mgr8">返回</button></a>
                            <input type="submit" class="btn btn-orange  btnSubmit" id="btnSt"  value="提交"  />
                            <input type="button" class="btn btn-orange btnSubmiting" style="display: none;" value="提交中..."  />
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- 时间插件/ -->
<?=Html::jsFile('@web/js/WdatePicker/WdatePicker.js')?>
<?=Html::jsFile('@web/js/WdatePicker/time.js')?>
<script type="text/javascript" src="/js/jquery-migrate-1.1.0.js"></script>
<script src="/js/jquery.autocomplete.js" type="text/javascript"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    function openPage(url,title) {
        window.parent.postMessage(JSON.stringify({
            url: url,
            title: title,
        }), "*")
    }
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    }
    // 字数限制
    $("#remarks").keyup(function(){
        if($("#remarks").val().length > 200){
            $("#remarks").val( $("#remarks").val().substring(0,200) );
        }
        $("#word").text( $("#remarks").val().length ) ;
    });
    //单项选择
    $('.radio-li').on('click',function() {
        $(this).find('span').css({'background':'#2693ff','color':'#fff'});
        $(this).siblings('li').find('span').css({'background':'#fff','color':'#333','border-color':'#ccc'});
        $(this).parent('ul').next('input').val($(this).find('span').attr('id'));
    })
    // 选择
    $('.xiangxia').on('click',function() {
        $('.addinput-list-data').hide();
        $(this).next('.addinput-list-data').show();
    })
    //
    $('.addinput-list-data li').on('click',function() {
        $('.addinput-list-data').hide();
        $(this).parent('ul').siblings('.form-control').val($(this).attr('id '));
    })


    $("#addBtn").click(function(){
        layer.open({
            type: 2,
            title: '选择产品',
            area: ['800px', '600px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/product/get-list',
            end:function(){
                var optionStr = '<option value="">请选择</option>';
                <?php if (!empty($depot)) : ?>
                <?php foreach ($depot as $key => $val) : ?>
                optionStr+='<option value="<?=$key ?>"><?=$val ?></option>';
                <?php endforeach; ?>
                <?php endif; ?>

                var str = $("#product_hide").html();
                data = eval("("+str+")");
                prod_id  = data.data.id;
                model    = data.data.model;
                if(model == null){
                    model = '';
                }
                html = '<td  class="padding15"><input type="checkbox" class="available_cost_type select_product" name="prod['+prod_id+'][prod_id]"  id="prod_id'+prod_id+'" > <label for="prod_id'+prod_id+'"> <i class="gou-i"></label> ' +
                    '</td>'+
                    '<td><input class="prod_id" value="'+prod_id+'" name="prod['+prod_id+'][prod_id]"  type="hidden"/>'+data.data.prod_name+'</td>'+
                    '<td>'+data.data.prod_no+'</td>'+
                    '<td>'+data.data.brand_name+'</td>'+
                    '<td>'+model+'</td>'+
                    '<td>'+data.data.unit_name+'</td>'+
                    '<td><input class="prod_num" value="1" datatype="n"  sucmsg=" " nullmsg="请输入数量" errormsg="请输入正整数" name="prod['+prod_id+'][prod_num]" /></td>'+
                    '<td><input class="serial_number" name="prod['+prod_id+'][serial_number]"   type="" maxlength="100" /></td>'+
                    '<td>' +
                    '<select class="depot_id" name="prod['+prod_id+'][depot_id]" datatype="*"  sucmsg=" " nullmsg="请选择库房" errormsg="请选择库房">'+optionStr+'</select>'+
                    '</td>'+
                    '<td><input  class="prod_batch"  name="prod['+prod_id+'][prod_batch]"   type="" maxlength="10" /></td>'
                    // '<td><input class="prod_date" name="prod['+prod_id+'][prod_date]" id="datetimepicker-pay-top"  readonly="readonly" onclick="WdatePicker({dateFmt:\'yyyy-MM-dd HH:mm:ss\'})" type="" maxlength="10" /></td>'+
                    // '<td><input class="effective_date" name="prod['+prod_id+'][effective_date]" type="" id="datetimepicker-pay-end" readonly="readonly" onclick="WdatePicker({dateFmt:\'yyyy-MM-dd HH:mm:ss\'})" maxlength="10" /></td>'
                ;

                var show_product = $("#show_prodcut").html();

                if(show_product.length>0){
                    var haveExist = $("#"+data.data.id).find("#prod_id"+prod_id).val();
                    //勾选重复的产品不会显示
                    if(!haveExist){
                        $("#show_prodcut").append('<tr id="'+data.data.id+'">'+html+'</tr>');
                    }else {
                        alert('产品重复添加');
                    }
                }
                $("#stock_in_msg").css("display",'none');
            }
        });
    });
    $('body').on('keyup','.prod_num',function () {
        var prod_num = $(this).val();
        if(isNaN(prod_num)){
            alert('请输入数字');
            $(this).val(1);
            return false;
        }
    });

    $('#allDetailed').on('change',function () {
        var amountText =$('.amount');
        var checkboxInput = $('tfoot input[type=checkbox]');
        var sum=0,sum1=0;

        if( $(this).is(':checked')){
            checkboxInput.prop("checked",true);
            amountText.each(function() {
                sum += Number($(this).text());
            })
            sum1 = sum + Number($('#otherExpenses').val());

        }else{
            sum1 = Number($('#otherExpenses').val());
            checkboxInput.prop("checked",false);
        }
        $('#allCost').text(sum1);
    })

    //表单提交验证
    $("#stockIn").Validform({
        tiptype:3,
        beforeSubmit:function(curform){
            var prodcutHidden  = $("#show_prodcut").html();
            var productLength = 0;
            if(prodcutHidden.length>0){
                var checkboxInput = $(".select_product");
                var sum = 0;
                ids =new Array();
                $(".select_product").each(function(i){
                    ids[i] = $(this).val();
                })
                var productLength = ids.length;
                if (productLength <= 0) {
                    $("#stock_in_msg").css("display",'block');
                    $("#stock_in_msg").html('请选择产品');
                    return false;
                }
            }
            $("#stock_type").removeAttr('disabled');
            $("#btnSt").prop("disabled", true);
        },
    });


    var deleteIds = '';
    //删除产品
    $(".delet-btn").click(function(){
        deleteIds = new Array();
        $(".select_product").each(function(i){
            if( $(this).is(':checked')){
                var id = $(this).parent().parent().attr('id');
                if(id>0){
                    deleteIds[i] = id;
                }
            }
        })
        if(deleteIds.length>0){
            confirm('确认删除产品吗?',function(){
                for(j=0;j<=deleteIds.length;j++){
                    $("#"+deleteIds[j]).remove();
                }
                $("#allDetailed").attr("checked",false);
            },this)
        }else{
            alert('请选择需要删除的产品');
        }
    })

    //全选删除按钮
    $('#allDetailed').on('change',function () {
        var checkboxInput = $('tfoot input[type=checkbox]');
        var sum=0,sum1=0;
        if( $(this).is(':checked')){
            checkboxInput.prop("checked",true);
        }else{
            checkboxInput.prop("checked",false);
        }
    });

    //提示
    $(".tip").popover();
</script>


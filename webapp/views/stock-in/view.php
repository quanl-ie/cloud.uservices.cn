<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use webapp\models\BrandQualification;
    use yii\helpers\ArrayHelper;
    /* @var $this yii\web\View */
    /* @var $model webapp\models\BrandQualification */
    $this->title = '入库单详情';
    $this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .information-list {margin-top: 0;}
</style>

<div class="jn-title-box no-margin-bottom">
        <span class="jn-title" id="no" data-no="">入库单详情</span>
        <div class="right-btn-box">
            <button class="btn btn-orange" onclick="javascript:location.href='/stock-in/wait-check';" >返回</button>
        </div>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h5 class="block-h5 block-h5one" >入库单信息</h5>
                    <div class="information-list" style="height: auto;">
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">入库单单号：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['no']) ? $stockInfo['no'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">入库主题：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['subject']) ? $stockInfo['subject'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">入库类型：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['stock_type_desc']) ? $stockInfo['stock_type_desc'] : "" ?></span></div>
                        </div>
                        <?php if (isset($stockInfo['rel_name'])) : ?>
                            <div class="col-xs-12	col-sm-12	col-md-4  col-lg-3">
                                <label   class="left-title80">关联单：</label>
                                <div  class="right-title3">
                                    <?php if($stockInfo['rel_type'] == 1){?>
                                        <a href="/purchase/detail?no=<?=$stockInfo['rel_no'];?>&t=view" target="_blank" class="line-height34">
                                            <span><?=isset($stockInfo['rel_name'])?$stockInfo['rel_name']:"" ?></span>
                                        </a>
                                    <?php }elseif($stockInfo['rel_type'] == 2){?>
                                        <a href="/contract/detail?id=<?=$stockInfo['rel_id'];?>&contract_id=<?=$stockInfo['rel_id'];?>" target="_blank" class="line-height34">
                                            <span><?=isset($stockInfo['rel_name'])?$stockInfo['rel_name']:"" ?></span>
                                        </a>
                                    <?php }elseif($stockInfo['rel_type'] == 3){?>
                                        <a href="/return-goods/detail?id=<?=$stockInfo['rel_id'];?>" target="_blank" class="line-height34">
                                            <span><?=isset($stockInfo['rel_name'])?$stockInfo['rel_name']:"" ?></span>
                                        </a>
                                    <?php }elseif($stockInfo['rel_type'] == 4){?>
                                        <a href="/exchange-goods/detail?id=<?=$stockInfo['rel_id'];?>" target="_blank" class="line-height34">
                                            <span><?=isset($stockInfo['rel_name'])?$stockInfo['rel_name']:"" ?></span>
                                        </a>
                                    <?php }elseif($stockInfo['rel_type'] == 6){?>
                                        <a href="/tech-prod-stock-return/detail?id=<?=$stockInfo['rel_id'];?>" target="_blank" class="line-height34">
                                            <span><?=isset($stockInfo['rel_name'])?$stockInfo['rel_name']:"" ?></span>
                                        </a>
                                    <?php }?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">入库状态：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['audit_status_desc']) ? $stockInfo['audit_status_desc'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">申请人：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['apply_user_name']) ? $stockInfo['apply_user_name'] : "" ?></span></div>
                        </div>
                        
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">申请时间：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['create_time']) ? $stockInfo['create_time'] : "" ?></span></div>
                        </div>
                        <?php if (isset($stockInfo['audit_user_id'])): ?>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">审批人：</label>
                            <div  class="right-title3 2693ff"><?=isset($stockInfo['audit_user_name']) ? $stockInfo['audit_user_name'] : "" ?></div>
                        </div>

                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">审批时间：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['audit_date']) ? $stockInfo['audit_date'] : "" ?></span></div>
                        </div>
                        <?php endif; ?>
                        <div class="col-xs-12	col-sm-12	col-md-12	col-lg-12">
                            <label  class="left-title80">备注：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['description']) ? $stockInfo['description'] : "" ?></span></div>
                        </div>
                    <?php if (isset($stockInfo['audit_user_id'])): ?>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">审批意见：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['audit_suggest']) ? $stockInfo['audit_suggest'] : "" ?></span></div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <h5 class="block-h5">产品明细</h5>
                    <div class="information-list" style="height: auto">
                        <table>
                            <thead>
                            <tr>
                                <th>产品名称</th>
                                <th>产品编号</th>
                                <th>产品品牌</th>
                                <th>产品型号</th>
                                <th>计量单位</th>
                                <th>数量</th>
                                <th>序列号</th>
                                <th>库房</th>
                                <th>批号</th>
<!--                                <th>生产日期</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($stockDetail)) : ?>
                            <?php foreach ($stockDetail as $val) : ?>
                            <tr>
                                <td><?=$val['prod_name'] ?></td>
                                <td><?=$val['prod_no'] ?></td>
                                <td><?=$val['brand_name'] ?></td>
                                <td><?=$val['model'] ?></td>
                                <td><?=$val['unit_name'] ?></td>
                                <td><?=$val['prod_num'] ?></td>
                                <td><?=$val['serial_number'] ?></td>
                                <td><?=$val['depot_name'] ?></td>
                                <td><?=$val['prod_batch'] ?></td>
<!--                                <td>--><?//=$val['prod_date'] ?><!--</td>-->
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    }
</script>
<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    $this->title = '入库管理列表';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time()?>">
<style type="text/css">
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
        border-top:2px solid #2693ff;
    }
    table{
        margin-top: 20px;
        border-right:1px solid #ccc;border-bottom:1px solid #ccc;
    }
    th,td{
        height: 40px;
        text-align: center;
        border-left:1px solid #ccc;border-top:1px solid #ccc
    }
    .icon-rili{position: absolute;right: 17px;top: 1px;height: 34px;line-height: 34px;background: #fff;width: 24px;text-align: center;}
    .calendar-icon{width: 100%;height: 36px;position: absolute;top:0;left: 0;cursor: pointer;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">入库管理列表</span>
    <div class="right-btn-box">
        <?php if(in_array('/stock-in/add',$selfRoles)){?>
            <a href="/stock-in/add"><span class="btn btn-success  icon iconfont">添加</span></a>
        <?php };?>
    </div>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body" style="font-size: 14px;">
                    <div class="col-md-12  col-lg-12">
                        <div style="width: 100%;" class="tab-group">
                            <ul id="myTab" class="nav nav-tabs">
                                <?php if(in_array('/stock-in/wait-check',$selfRoles)){?>
                                    <li><a href="/stock-in/wait-check?status=1" >待审批入库</a></li>
                                <?php };?>
                                <?php if(in_array('/stock-in/finished',$selfRoles)){?>
                                    <li class="active"><a href="/stock-in/finished?status=2">已入库</a></li>
                                <?php };?>
                                <?php if(in_array('/stock-in/unchecked',$selfRoles)){?>
                                    <li><a href="/stock-in/unchecked?status=3">未通过</a></li>
                                <?php };?>
                            </ul>
                        </div>
                        
                        <div id="myTabContent" class="tab-content">
                            <!--搜索开始-->
                            <div class="form-group jn-form-box">
                                <form action="/stock-in/finished" class="form-horizontal form-border" id="form">
                                    <input type="hidden" name="status" value="2">
                                    <div class="form-group jn-form-box">
                                        <!-- 搜索栏 -->
                                        <div class="col-sm-12 no-padding-left">
                                            
                                            <div class="single-search">
                                                <label class="search-box-lable">入库单号</label>
                                                <div class="single-search-kuang1">
                                                    <input type="text" class="form-control" name="no" value="<?php echo isset($_GET['no'])?$_GET['no']:'';?>">
                                                </div>
                                            </div>
                                            <div class="single-search">
                                                <label class="search-box-lable">入库主题</label>
                                                <div class="single-search-kuang1">
                                                    <input type="text" class="form-control" name="subject" value="<?php echo isset($_GET['subject'])?$_GET['subject']:'';?>">
                                                </div>
                                            </div>
                                            
                                            <div class="single-search">
                                                <label class="search-box-lable">入库类型</label>
                                                <div class="single-search-kuang1">
                                                    <select class="form-control" name="stock_type">
                                                        <option value="">请选择</option>
                                                        <?php if ($stockTypeList): ?>
                                                            <?php foreach ($stockTypeList as $key => $val): ?>
                                                                <option value="<?=$key; ?>" <?php if (isset($_GET['stock_type']) && $_GET['stock_type'] == $key) : ?>selected="selected"<?php endif; ?> ><?=$val; ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="tow-search" >
                                                <label class="search-box-lable">申请时间</label>
                                                <div class="single-search-kuang2">
                                                    <div class="half-search">
                                                        <input type="text" id="datetimepicker-pay-top" class="form-control" name="start_create_time" value="<?php echo isset($_GET['start_create_time'])?$_GET['start_create_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})">
                                                        <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})">
                                        </span>
                                                        <i class="icon2-rili"></i>
                                                        <span class="zhi"> 至</span>
                                                    </div>
                                                    
                                                    <div class="half-search">
                                                        <input type="text" id="datetimepicker-pay-end" class="form-control" name="end_create_time" value="<?php echo isset($_GET['end_create_time'])?$_GET['end_create_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})" >
                                                        <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})">
                                        </span>
                                                        <i class="icon2-rili"></i>
                                                    </div>
                                                    <p class="city3_fix"></p>
                                                </div>
                                            </div>
    
                                            <div class="tow-search" >
                                                <label class="search-box-lable">审批时间</label>
                                                <div class="single-search-kuang2">
                                                    <div class="half-search">
                                                        <input type="text" id="datetimepicker-pay-top1" class="form-control" name="start_audit_date" value="<?php echo isset($_GET['start_audit_date'])?$_GET['start_audit_date']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end1\')}'})">
                                                        <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top1',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end1\')}'})">
                                        </span>
                                                        <i class="icon2-rili"></i>
                                                        <span class="zhi"> 至</span>
                                                    </div>
            
                                                    <div class="half-search">
                                                        <input type="text" id="datetimepicker-pay-end1" class="form-control" name="end_audit_date" value="<?php echo isset($_GET['end_audit_date'])?$_GET['end_audit_date']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top1\')}'})" >
                                                        <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-end1',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top1\')}'})">
                                        </span>
                                                        <i class="icon2-rili"></i>
                                                    </div>
                                                    <p class="city3_fix"></p>
                                                </div>
                                            </div>
                                            <div class="search-confirm">
                                                <button class="btn btn-success"><i class=" "></i> 搜索</button>
                                            </div>
                                        
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--搜索结束-->
                            <div class="tab-pane fade in active" id="customer-order">
                                <table  style="width: 100%;text-align:center;">
                                    <thead>
                                    <tr>
                                        <th>入库单号</th>
                                        <th>入库主题</th>
                                        <th>入库类型</th>
                                        <th>入库状态</th>
                                        <th>产品数量</th>
                                        <th>申请时间</th>
                                        <th>审批时间</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($data):?>
                                        <?php foreach ($data as $key=>$val):  ?>
                                            <tr>
                                                <td><?=isset($val['no']) ? $val['no'] : '空'; ?></td>
                                                <td><?=isset($val['subject']) ? $val['subject'] : ''; ?></td>
                                                <td><?=isset($val['stock_type_desc']) ? $val['stock_type_desc'] : '空'; ?></td>
                                                <td><?=isset($val['status_desc']) ? $val['status_desc'] : '空'; ?></td>
                                                <td><?=isset($val['prod_sum']) ? $val['prod_sum']  : '空'; ?></td>
                                                <td><?=isset($val['create_time']) ? $val['create_time'] : '空'; ?></td>
                                                <td><?=isset($val['audit_date']) ? $val['audit_date'] : '空'; ?></td>
                                                <td>
                                                    <?php if(in_array('/stock-in/view',$selfRoles)){?>
                                                        <a href="/stock-in/view?id=<?=$val['id']?>">查看</a>
                                                    <?php };?>
                                                </td>
                                            </tr>
                                        <?php endforeach;?>
                                    <?php else:?>
                                        <tr>
                                            <td colspan="8" class="no-record">暂无相关数据</td>
                                        </tr>
                                    <?php endif;?>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div id="customer-product" class="tab-pane fade" ></div>
                            <div id="customer-address" class="tab-pane fade" ></div>
                        </div>
                    
                    </div>
                    <div class="col-xs-12 text-center pagination">
                        <?php if ($pageHtml) {echo $pageHtml;}  ;?>
                    </div>
                </div>
            
            </div>
        
        </div>
    
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    }
</script>
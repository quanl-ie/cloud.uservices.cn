<?php
$this->title = '资料管理';
$this->params['breadcrumbs'][] = $this->title;

use common\models\ServiceClass;
?>

<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">资料管理</span>
    <?php // if(in_array('/role/add', $selfRoles)):?>
    <a href="/user-info/show" class="btn btn-success jn-btn" style=" color:#FFF;float:right "> 返回</a> 
    <?php // endif;?>
</div> 

<section id="main-content">
<div class="row">
    <div class="col-md-12">                    
        <div class="panel panel-default">                     
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover" style="width:60%;" align="center">
                        <thead>
                            <tr>
                                <th style="width: 200px">厂家名称</th>
                                <th><?php echo $infoArr['company']?></th>                              
                            </tr>
                            <tr>
                                <th>联系人</th>
                                <th><?php echo $infoArr['contact']?></th>                              
                            </tr>
                            <tr>
                                <th>电话</th>
                                <th><?php echo $infoArr['mobile']?></th>                              
                            </tr>
                            <tr>
                                <th>电子邮箱</th>
                                <th><?php echo $infoArr['email']?></th>                              
                            </tr>
                            <tr>
                                <th>厂家LOGO</th>
                                <th><?php echo Yii::$app->params['imageUrl'].$infoArr['logo']?></th>                              
                            </tr>
                            <tr>
                                <th>厂家地址</th>
                                <th><?php echo $infoArr['address']?></th>                              
                            </tr>
                            <tr>
                                <th>营业执照</th>
                                <th><?php echo Yii::$app->params['imageUrl'].$infoArr['business_licence']?></th>                              
                            </tr>
                            <tr>
                                <th>厂家简介</th>
                                <th><?php echo $infoArr['company_desc']?></th>                              
                            </tr>
                            <tr>
                                <th>经营产品</th>
                                <th><?php echo ServiceClass::getClassName($infoArr['class_id']);?></th>                              
                            </tr>
                        </thead>
                    </table>
                </div>
            </div> 
       </div>
   </div>
 </div>
</section>
<script>
window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    myAlert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    <?php endif;?>
}
</script>
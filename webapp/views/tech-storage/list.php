<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    $this->title = '产品列表';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">

<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4{ padding-left: 0;}
    body{overflow-x: hidden;background: #fff;font-family: "微软雅黑"}
    table{border-collapse: collapse;}
   .table-bordered>tbody>tr>td,.table-bordered>thead>tr>th{border-right:1px solid transparent;overflow: hidden;text-align: center;padding: 10px;font-size: 12px}
   .form-horizontal .form-group{margin:0;}
   .table-striped>tbody>tr:nth-child(odd)>td{background: #fff}
</style>
<body>
<div class="row popup-boss">
    <form action="/product/get-list" class="form-horizontal form-border" id="form">
       <div class="table-responsive" style="width: 90%;margin:0 auto;height: 65px">
                <div class="col-sm-5 search-box">
                    <label class=" search-box-lable">产品名称</label>
                    <div class="single-search-kuang1">
                        <input type="text" class="form-control" name="prod_name" maxlength="20" value="<?=isset($_GET['prod_name']) ? $_GET['prod_name'] : '' ?>">
                    </div>
                </div>
                <div class="col-sm-5 search-box">
                    <label class=" search-box-lable">产品编号</label>
                    <div class="single-search-kuang1">
                        <input type="text" class="form-control" name="prod_no" maxlength="20" value="<?=isset($_GET['prod_no']) ? $_GET['prod_no'] : '' ?>">
                    </div>
                </div>
                <div class="search-confirm">
                    <button class="btn btn-orange">
                        <i class=" "></i> 查询
                    </button>
                </div>
        </div>
        <div class="table-responsive"  style="width: 90%;margin:0 auto;">
            <table class="table table-bordered table-striped table-hover" style="width:100%;">
                <thead bgcolor="#f8f8f8" style="border-collapse: collapse;">
                <tr>
                    <th></th>
                    <th>备件名称</th>
                    <th>产品类目</th>
                    <th>品牌</th>
                    <th>型号</th>
                    <th>库存</th>
                </tr>
                </thead>
                <tbody>
                <?php if($data){?>
                    <?php foreach ($data as $key=>$val){ ?>
                        <tr>
                            <td>
                                <input type="radio" name="prod_id" class="prod_id" value="<?=$val['id']?>" class="available_cost_type" id="bh<?=$val['id']?>"/>
                                <label for="bh<?=$val['id']?>" style="cursor: pointer;"></label>
                            </td>
                            <td>
                                <?php echo isset($val['prod_name']) ? $val['prod_name'] :""; ?>
                            </td>
                            <td>
                                <?php echo isset($val['class_name'])?$val['class_name']:''; ?>
                            </td>
                            <td>
                                <?php echo isset($val['brand_name']) ? $val['brand_name'] :""; ?>
                            </td>
                            <td>
                                <?php echo isset($val['model']) ? $val['model'] :""; ?>
                            </td>
                            <td>
                                <?php echo isset($val['stock_num'])?$val['stock_num']:''; ?>
                                <input class="stock_num" type="hidden" value="<?php echo isset($val['stock_num'])?$val['stock_num']:''; ?>" />
                            </td>
                        </tr>
                    <?php }?>
                <?php }else{?>
                    <tr><td colspan="11" class="no-record">该技师暂无备件库存</td></tr>
                <?php }?>
                </tbody>
            </table>
        </div>
    </form>
</div>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>

<script>

    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };


    $(".prod_id").click(function(){
        var prodId = $(this).val();
        var html = '';
        var stock_num = $(this).parents('tr').find('.stock_num').val();
        $.ajax({
            url:'/product/get-detail?id='+prodId,
            type:'get',
            success:function(msg){
                data = eval("("+msg+")");
                if(data.success==true){
                    parent.$("#product_hide").html(msg+'+stock_num='+stock_num); //父页面接收json
                    var index = parent.layer.getFrameIndex(window.name); //关闭当前弹出层
                    parent.layer.close(index);
                }else{

                }
                alert('没有该产品');

            }
        })
    })
</script>
</body>
</html>




















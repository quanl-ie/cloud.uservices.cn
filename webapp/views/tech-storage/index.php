<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
$this->title = '技师备件信息';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
    .search-box-kuang>.col-sm-6{
        padding-left: 0;
        position: relative;
    }
    .icons-calendar{
        position: absolute;
        right: 24px;top: 9px;
    }
    .pagination{text-align: center;}
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
        border-top:2px solid #2693ff;
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">技师备件信息列表</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div style="width: 100%;" class="tab-group">
                        <ul id="myTab" class="nav nav-tabs">
                            <?php if(in_array('/tech-storage/index',$selfRoles)){?>
                                <li class="active"><a href="/tech-storage?technician_id=<?php echo $params['technician_id']?>" >备件库存</a></li>
                            <?php };?>
                            <?php if(in_array('/tech-prod-use-record/index',$selfRoles)){?>
                                <li><a href="/tech-prod-use-record?technician_id=<?php echo $params['technician_id']?>">使用记录</a></li>
                            <?php };?>
                        </ul>
                    </div>
                    <div class="form-group jn-form-box" style="padding-bottom:20px;">
                        <!-- 搜索栏 -->
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead bgcolor="#455971">
                            <tr>
                                <th>备件名称</th>
                                <th>产品类目</th>
                                <th>品牌</th>
                                <th>型号</th>
                                <th>库存</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if($data){?>
                                <?php foreach ($data as $key=>$val){ ?>
                                    <tr>
                                        <td>
                                            <?php echo isset($val['prod_name']) ? $val['prod_name'] :""; ?>
                                        </td>
                                        <td>
                                            <?php echo isset($val['class_name'])?$val['class_name']:''; ?>
                                        </td>
                                        <td>
                                            <?php echo isset($val['brand_name']) ? $val['brand_name'] :""; ?>
                                        </td>
                                        <td>
                                            <?php echo isset($val['model']) ? $val['model'] :""; ?>
                                        </td>
                                        <td>
                                            <?php echo isset($val['stock_num'])?$val['stock_num']:''; ?>
                                        </td>
                                    </tr>
                                <?php }?>
                            <?php }else{?>
                                <tr><td colspan="11" class="no-record">该技师暂无备件库存</td></tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo $pageHtml;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
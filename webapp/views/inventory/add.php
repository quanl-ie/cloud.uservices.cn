<?php

use yii\helpers\Url;
use webapp\models\BrandQualification;

$this->title = '创建盘点单';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<style>
    .us-hr {
        margin: 0 0 6px 0;
        border-bottom: 1px solid #eeeeee;
    }

    textarea.us-ipt {
        line-height: 17px;
    }
</style>
<section id="main-content">
    <div id="app" class="panel-body" v-cloak>
        <us-bus-title title="创建盘点单"></us-bus-title>
        <us-form name="add" label-whl="90px" item-whl="320px" wrap-whl="200px" :styles="{ paddingTop : '24px'}">
            <us-section-page title-line title="盘点单信息">
                <us-form-row>
                    <us-form-item label="盘点单号：">
                        <us-text-content whl=",32px,32px">
                            <us-text-content whl=",32px,32px">{{entity.odd_num}}</us-text-content>
                        </us-text-content>
                    </us-form-item>
                    <us-form-item label="盘点主题：" required>
                        <us-ipt-text v-model="entity.subject" :rule="validateTitle"
                                     placeholder="请输入盘点主题(限制32个字)" word-size="32"></us-ipt-text>
                    </us-form-item>
                    <us-form-item label="盘点类型：" whl="400px">
                        <div class="us-fx">
                            <us-ipt-select v-model="entity.type" placeholder="请选择盘点类型">
                                <us-select-option v-for="(val, index) in source.type"
                                                  :value="val.id"
                                                  :key="index"
                                                  :label="val.title"></us-select-option>
                            </us-ipt-select>
                            <us-ipt-datetime v-if="entity.type!=1 && entity.type!=''"
                                             v-model="entity.date" :styles="{marginLeft:'10px'}" type="date" section
                                             ipt-whl="200px"></us-ipt-datetime>
                        </div>
                    </us-form-item>
                </us-form-row>
                <us-form-row>
                    <us-form-item label="仓库：" required>
                        <us-ipt-select @change="handleProdDetail(entity.depot_id)"
                                       v-model="entity.depot_id"
                                       placeholder="请选择仓库" rule="defined?请选择仓库">
                            <us-select-option v-for="(val, index) in source.depot"
                                              :value="val.id"
                                              :key="index"
                                              :label="val.name"></us-select-option>
                        </us-ipt-select>
                    </us-form-item>
                    <us-form-item label="产品类目：" required>
                        <us-ipt-tree-select
                                ref="treeSelect"
                                placeholder="请选择产品类目"
                                @change="getProducts(1)"
                                multiple
                                default-open
                                default-select-all
                                name="tree"
                                rule="defined?请选择产品类目！"
                                :format-text="formatText"
                                v-model="entity.class_ids">
                            <us-tree-option :has-child="isHasChildren"
                                            :exclude="opt.exclude"
                                            v-for="(opt, index) in source.proType"
                                            value-field="class_id"
                                            label-field="title"
                                            :data="opt"
                                            :key="index"></us-tree-option>
                        </us-ipt-tree-select>
                    </us-form-item>
                </us-form-row>
                <us-form-row>
                    <us-form-item label="盘点人：">
                        <us-text-content whl=",32px,32px">{{entity.userName}}</us-text-content>
                    </us-form-item>
                    <us-form-item label="盘点时间：">
                        <us-text-content whl=",32px,32px">{{entity.time}}</us-text-content>
                    </us-form-item>
                </us-form-row>
                <us-form-item label="备注：" whl="800px">
                    <us-ipt-textarea
                            v-model="entity.remark"
                            word-size="200"
                            whl="600px"
                            placeholder="请描述"></us-ipt-textarea>
                </us-form-item>
            </us-section-page>
            <us-section-page title-line title="产品明细">
                <us-table :data="products">
                    <us-table-column label="序号" field="tempId" width="50px"></us-table-column>
                    <us-table-column label="产品名称" field="prod_name"></us-table-column>
                    <us-table-column label="产品编号" field="prod_no"></us-table-column>
                    <us-table-column label="产品品牌" field="brandName"></us-table-column>
                    <us-table-column label="产品型号" field="model"></us-table-column>
                    <us-table-column label="计量单位" field="unitDesc"></us-table-column>
                    <us-table-column label="账面数量" field="num"></us-table-column>
                </us-table>
                <div class="us-t-center">
                    <us-pagination
                            v-show="products && products.length"
                            @change="changePage"
                            :page-no="page.pageNo"
                            :total-page="page.totalPage"
                            :total="page.total"></us-pagination>
                </div>
            </us-section-page>
            <div class="text-center">
                <us-button @click="history.go(-1)">取消</us-button>
                <us-button type="primary" @click="onSubmit" :disabled="disableBtn">确认创建</us-button>
            </div>
        </us-form>
    </div>
</section>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/inventory/add.js?v=<?php echo $v; ?>"></script>

<?php

use yii\helpers\Url;
use webapp\models\BrandQualification;

$this->title = '盘点单详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<style>
    .us-hr {
        margin: 0 0 6px 0;
        border-bottom: 1px solid #eeeeee;
    }
    textarea.us-ipt{
        line-height: 17px;
    }
</style>
<section id="main-content">
    <div id="app" class="panel-body" v-cloak>
        <us-bus-title title="盘点单详情">
            <div style="float: right;">
                <?php if (in_array('/inventory/download', $selfRoles)): ?>
                    <us-button type="primary" @click="handleExport">下载盘点清单</us-button>
                <?php endif; ?>
                <?php if(in_array('/inventory/jiesu',$selfRoles)): ?>
                <us-button type="primary" :disabled="disableCompleteBtn" @click="changeStatusFunc(1)">结束盘点</us-button>
                <?php endif; ?>
                <?php if(in_array('/inventory/zuofei',$selfRoles)): ?>
                <us-button :disabled="disableCancelBtn" @click="changeStatusFunc(2)">作废盘点单</us-button>
                <?php endif; ?>
                <us-button @click="handleToList">返回</us-button>
            </div>
        </us-bus-title>
        <us-form label-whl="90px" item-whl="370px" wrap-whl="200px" :styles="{ paddingTop : '24px'}">
            <us-section-page title-line title="盘点单信息">
                <us-form-row>
                    <us-form-item label="盘点单号：" type="mini">
                        <us-text-content>{{detailInfo.baseInfo.oddNum}}</us-text-content>
                    </us-form-item>
                    <us-form-item label="盘点主题：" type="mini">
                        <us-text-content>{{detailInfo.baseInfo.subject}}</us-text-content>
                    </us-form-item>
                    <us-form-item label="盘点类型：" type="mini">
                        <us-text-content>{{detailInfo.baseInfo.typeDesc?detailInfo.baseInfo.typeDesc+'   ' +detailInfo.baseInfo.date:"暂无"}}</us-text-content>
                    </us-form-item>
                </us-form-row>
                <us-form-row>
                    <us-form-item label="仓库：" type="mini">
                        <us-text-content>{{detailInfo.baseInfo.warehouse}}</us-text-content>
                    </us-form-item>
                    <us-form-item label="产品类目：" type="mini">
                        <us-text-content>{{detailInfo.baseInfo.classDesc}}</us-text-content>
                        </us-text-content>
                    </us-form-item>
                </us-form-row>
                <us-form-row>
                    <us-form-item label="盘点人：" type="mini">
                        <us-text-content>{{detailInfo.baseInfo.userName}}</us-text-content>
                    </us-form-item>
                    <us-form-item label="盘点时间：" type="mini">
                        <us-text-content>{{detailInfo.baseInfo.createTime}}</us-text-content>
                    </us-form-item>
                </us-form-row>
                <us-form-item label="备注：" whl="800px" type="mini">
                    <us-ipt-textarea
                            word-size="200"
                            whl="600px"
                            placeholder="请描述" v-model="detailInfo.baseInfo.remark"></us-ipt-textarea>
                </us-form-item>
            </us-section-page>
            <us-section-page title-line title="产品明细">
                <us-table :data="detailInfo.prodList">
                    <us-table-column label="序号" field="tempId" width="50px"></us-table-column>
                    <us-table-column label="产品名称" field="prod_name"></us-table-column>
                    <us-table-column label="产品编号" field="prod_num"></us-table-column>
                    <us-table-column label="产品品牌" field="prod_brand_name"></us-table-column>
                    <us-table-column label="产品型号" field="prod_model"></us-table-column>
                    <us-table-column label="计量单位" field="prod_unit"></us-table-column>
                    <us-table-column label="账面数量" field="num"></us-table-column>
                    <us-table-column label="实盘数量">
                        <template slot-scope="scope">
                            <us-ipt-text style="width: 80px;" v-model="scope.data.real_num" placeholder="实盘数量" @blur="handleValidateRealNum(scope.data)"></us-ipt-text>
                        </template>
                    </us-table-column>
                    <us-table-column label="盈亏数量" field="calc_num"></us-table-column>
                    <us-table-column label="实盘人" field="username"></us-table-column>
                    <us-table-column label="备  注" >
                        <template slot-scope="scope">
                            <us-ipt-text style="width: 80px;" word-size="30" placeholder="限30字" v-model="scope.data.remark" @blur="handleValidateRemark(scope.data)"></us-ipt-text>
                        </template>
                    </us-table-column>
                </us-table>
                <div class="text-center">
                    <us-pagination
                            v-if="detailInfo.prodList && detailInfo.prodList.length"
                            @change="changePage"
                            :page-no="page.pageNo"
                            :total-page="page.totalPage"
                            :total="page.total"></us-pagination>
                </div>
            </us-section-page>
        </us-form>
    </div>
</section>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/inventory/inventorying.js?v=<?php echo $v; ?>"></script>

<?php

use yii\helpers\Url;
use webapp\models\BrandQualification;

$this->title = '库存盘点列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/us-icon.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/css/return-purchase/index.css'); ?>?v=<?php echo time(); ?>">
<div id="app" v-cloak>
    <section id="main-content">
        <div class="panel-body">
            <us-adv-search title="库存盘点列表" placeholder="搜索盘点主题"
                           v-model="search.subject"
                           @on-input-search="inputSearch"
                           @on-search="advSearch">
                <div slot="adv-header" style="float: right;">
                    <?php if(in_array('/inventory/add',$selfRoles)): ?>
                    <us-button type="primary" @click="handleCreate">创建盘点单</us-button>
                    <?php endif; ?>
                </div>
                <template slot-scope="handler">
                    <us-form label-whl="70px" item-whl="300px">
                        <us-form-row>
                            <us-form-item label="盘点主题：">
                                <us-ipt-text whl="200px" v-model="search.subject"></us-ipt-text>
                            </us-form-item>
                            <us-form-item label="库房名称：">
                                <us-ipt-text whl="200px" v-model="search.warehouse"></us-ipt-text>
                            </us-form-item>
                            <us-form-item label="盘点状态：">
                                <us-ipt-select whl="200px" v-model="search.status">
                                    <us-select-option label="全部"></us-select-option>
                                    <us-select-option v-for="(val, key) in source.status"
                                                      :value="key"
                                                      :key="key"
                                                      :label="val"></us-select-option>
                                </us-ipt-select>
                            </us-form-item>
                            <us-form-item label="盘点类型：">
                                <us-ipt-select whl="200px" v-model="search.type">
                                    <us-select-option label="全部"></us-select-option>
                                    <us-select-option v-for="(val, index) in source.type"
                                                      :value="val.id"
                                                      :key="index"
                                                      :label="val.title"></us-select-option>
                                </us-ipt-select>
                            </us-form-item>
                        </us-form-row>
                        <us-form-row>
                            <us-form-item label="盘点时间：" whl="320px">
                                <us-ipt-datetime section v-model="search.date" type="date"
                                                 whl="200px"></us-ipt-datetime>
                            </us-form-item>
                        </us-form-row>
                    </us-form>
                </template>
            </us-adv-search>

            <div class="table-list-wrap">
                <us-table :data="list">
                    <us-table-column label="盘点编号" field="odd_num">
                        <template slot-scope="scope">
                            <?php if(in_array('/inventory/inventorying',$selfRoles)): ?>
                            <a :href="'/inventory/inventorying?id='+scope.data.id+'&status='+scope.data.status" v-if="scope.data.status==1">{{scope.data.odd_num}}</a>
                            <?php endif; ?>
                            <?php if(in_array('/inventory/view',$selfRoles)): ?>
                            <a :href="'/inventory/view?id='+scope.data.id+'&status='+scope.data.status" v-if="scope.data.status==2||scope.data.status==3">{{scope.data.odd_num}}</a>
                            <?php endif; ?>
                        </template>
                    </us-table-column>
                    <us-table-column label="盘点主题" field="subject"></us-table-column>
                    <us-table-column label="盘点库房" field="name"></us-table-column>
                    <us-table-column label="盘点状态" field="statusDesc"></us-table-column>
                    <us-table-column label="盘点类型" field="typeDesc"></us-table-column>
                    <us-table-column label="盘点人" field="createUserName"></us-table-column>
                    <us-table-column label="盘点日期" field="create_time"></us-table-column>
                    <us-table-column label="操作">
                        <template slot-scope="scope">
                            <?php if(in_array('/inventory/view',$selfRoles)): ?>
                            <a :href="'/inventory/inventorying?id='+scope.data.id+'&status='+scope.data.status" v-if="scope.data.status == 1">盘点</a>
                            <?php endif; ?>
                            <?php if(in_array('/inventory/view',$selfRoles)): ?>
                            <a :href="'/inventory/view?id='+scope.data.id+'&status='+scope.data.status" v-else>查看详情</a>
                            <?php endif; ?>
                        </template>
                    </us-table-column>
                </us-table>
                <div class="text-center">
                    <us-pagination
                            @change="changePage"
                            :page-no="page.pageNo"
                            :total-page="page.totalPage"
                            :total="page.total"></us-pagination>
                </div>
            </div>
        </div>

    </section>
</div>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/inventory/index.js?v=<?php echo $v; ?>"></script>
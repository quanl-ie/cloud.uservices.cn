
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = '编辑合同';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">编辑合同
    <!--    <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="
            选择客户：请在您所创建的客户中选择，选择客户后，系统会自动将该客户的联系方式及地址进行填充，如客户未创建，请点击右侧添加按钮进行创建<br/><br/>

选择产品：请在您所创建的客户产品中选择，选择产品后，系统会自动将产品类目、品牌、序列号等信息进行填充，如未创建产品，请点击右侧添加按钮进行创建<br/><br/>

质保状态：在创建订单时，请严格确认客户设备的质保状态来选择类型，以免影响服务价格<br/><br/>

派单方式：您可选择指定合作服务商或输入技师电话进行服务，也可通过优服务平台进行派单，服务商需要通过合作申请后方可选择
        "></span>-->
    </span>
</div> 
<!--引入CSS-->
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/webuploader/webuploader.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jquery.autocomplete.css');?>?v=<?php echo time();?>">
<!--引入JS-->
<?=Html::jsFile('@web/webuploader/webuploader.js')?>
<?=Html::jsFile('@web/js/bootstrap-select.js')?>
<?=Html::jsFile('@web/webuploader/init-file.js')?>
<style>
   .misplacement{position: absolute;bottom: -28px;left: 15px;display: none}
   .block-h5{padding-top: 30px;}
   .form-group>.col-xs-2{width: 0;display: none;}
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body"><?php
                    $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'form-horizontal',
                            'id' => 'contract_edit',
                            'name' => 'form1',
                            'enctype' => 'multipart/form-data',
                        ],
                        'fieldConfig' => [
                            'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-xs-10\">{input}{error}</div>",
                            'labelOptions' => ['class' => 'col-xs-2 control-label text-right'],
                        ]
                    ]);
                    ?>
                    <div class="col-md-12">
                        <h5 class="block-h5" style="padding-top: 0">基本信息</h5>
                    </div>
                    <div class="information-list">
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0">
                            <label class="name-title-lable" for="workorder-account_id">
                                <input type="hidden" name="id" class="form-control" id="id" value="<?=$info['id']?>" />
                                <i class="red-i">*</i> 客户：
                            </label>
                            <span class="c_red"></span>
                            <div class="addinput-list">
                                <p style="line-height: 30px"><?=$info['account_name']?></p>
                                <input type="hidden" class="form-control" id="account_name"   value="<?=$info['account'].'/'.$info['mobile']?>" accept="">
                                <input type="hidden" name="account_id" class="form-control" id="account_id"   value="<?=$info['account_id']?>" accept="">
                                <p class=" misplacement"  id="account_msg"></p>
                            </div>
                        </div>
                    </div>
                    <div class="information-list">
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0">
                            <label class="name-title-lable" for="workorder-account_id">
                                <i class="red-i">*</i> 合同主题：
                            </label>
                            <span class="c_red"></span>
                            <div class="addinput-list">
                                <input type="text" name="subject" class="form-control" maxlength="20" id="subject" value="<?=$info['subject']?>"  datatype="*" sucmsg=" " nullmsg="请输入合同主题（20字以内）"  placeholder="请输入合同主题（20字以内）" >
                                 <p class=" misplacement"  id="subject_msg"></p>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0">
                            <label class="name-title-lable" for="workorder-account_id">
                                <i class="red-i"></i> 合同类型：
                            </label>
                            <span class="c_red"></span>
                            <div class="addinput-list">
                                <select name="contract_type" id="contract_type" class="form-control">
                                    <option value="">请选择合同类型</option>
                                    <option value="1" <?php if ($info['type'] == 1) { ?>selected="selected"<?php };?>>销售</option>
                                    <option value="2" <?php if ($info['type'] == 2) { ?>selected="selected"<?php };?>>分销</option>
                                    <option value="3" <?php if ($info['type'] == 3) { ?>selected="selected"<?php };?>>售后服务</option>
                                </select>
                                <p class="misplacement"  id="contract_type_msg"></p>
                            </div>
                        </div>
                    </div>
                    <div class="information-list">
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0">
                            <label class="name-title-lable" for="workorder-account_id">
                                <i class="red-i"></i> 合同分类：
                            </label>
                            <span class="c_red"></span>
                            <div class="addinput-list">
                                <select name="contract_classify" id="contract_classify" class="form-control">
                                    <option value="">请选择合同分类</option>
                                    <?php if($contractClassifyList){?>
                                        <?php foreach ($contractClassifyList as $key=>$val){?>
                                            <option value="<?php echo $key ?>" <?php if($info['classify']==$key){?>selected<?php };?>><?=$val?></option>
                                        <?php }?>
                                    <?php }?>
                                </select>
                                <p class="misplacement"  id="contract_classify_msg"></p>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0">
                            <label class="name-title-lable" for="workorder-account_id">
                                <i class="red-i"></i> 合同品牌：
                            </label>
                            <span class="c_red"></span>
                            <div class="addinput-list">
                                <select name="contract_brand" id="contract_brand" class="form-control">
                                    <option value="">请选择合同品牌</option>
                                    <?php if($brand){?>
                                        <?php foreach ($brand as $key=>$val){?>
                                            <option value="<?php echo $key ?>" <?php if($info['brand']==$key){?>selected<?php };?>><?=$val?></option>
                                        <?php }?>
                                    <?php }?>
                                </select>
                                <p class="misplacement"  id="contract_brand_msg"></p>
                            </div>
                        </div>
                    </div>
                    <div class="information-list">
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0">
                            <label class="name-title-lable" for="workorder-account_id">
                                <i class="red-i">*</i> 签订日期：
                            </label>
                            <div class="addinput-list">
                                <input type="text" name="sigin_date" class="form-control enYMD" value="<?=substr($info['signed_date'],0,10)?>"  readonly id="siginDate" placeholder="请选择">
                                <i class="icon2-rili riqi1"></i>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0">
                            <label class="name-title-lable" for="workorder-account_id">
                                <i class="red-i">*</i> 开始日期：
                            </label>
                            <div class="addinput-list">
                                <input type="text" name="start_date" class="form-control enYMD" value="<?=substr($info['start_date'],0,10)?>"  readonly id="startDate" placeholder="请选择">
                                <i class="icon2-rili riqi1"></i>
                                <div class="misplacement" style="display:none" id="time_msg"></div>
                            </div>
                        </div>
                    </div>
                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 收款状态：
                        </label>
                        <span class="c_red"></span>
                        <div class="addinput-list">
                            <div class="roundRadio select-Zhouqi">
                                <input type="radio" class="available_cost_type" name="collection_status" value="1" id="available_cost_type1" <?php if($info['collection_status'] == 1){echo 'checked="checked"' ;}?>>
                                <label for="available_cost_type1">全款</label>
                            </div>
                            <div class="roundRadio select-Zhouqi">
                                <input type="radio" class="available_cost_type collection_status" name="collection_status" value="3" id="available_cost_type3" <?php if($info['collection_status'] == 3){echo 'checked="checked"' ;}?>>
                                <label for="available_cost_type3">定金</label>
                            </div>
                            <div class="roundRadio  select-Zhouqi">
                                <input type="radio" class="available_cost_type" name="collection_status" value="2" id="available_cost_type2" <?php if($info['collection_status'] == 2){echo 'checked="checked"' ;}?>>
                                <label for="available_cost_type2">未收款</label>
                            </div>
                             <p class="misplacement" style="display:none" id="collection_status_msg"></p>
                        </div>
                    </div>
                    <div class="information-list" id="paymentMethodType" <?php if($info['collection_status'] == 2){echo 'style="display: none"' ;}?>>
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0">
                            <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 付款方式：
                            </label>
                            <div class="addinput-list">
                                <select name="payment_type" class="form-control">
                                    <option value=null>请选择付款方式</option>
                                    <option  value="1" <?php if ($info['payment_type'] == 1) { ?>selected="selected"<?php };?>>现场收费</option>
                                    <option  value="2" <?php if ($info['payment_type'] == 2) { ?>selected="selected"<?php };?>>微信</option>
                                    <option  value="3" <?php if ($info['payment_type'] == 3) { ?>selected="selected"<?php };?>>支付宝</option>
                                    <option  value="4" <?php if ($info['payment_type'] == 4) { ?>selected="selected"<?php };?>>打款公司</option>
                                    <option  value="5" <?php if ($info['payment_type'] == 5) { ?>selected="selected"<?php };?>>门店代收</option>
                                </select>
                                <p class="misplacement"  id="payment_type_msg"></p>
                            </div>
                        </div>
                        <div  class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0" id="dataReceipt" <?php if($info['collection_status'] == 2){echo 'style="display: none"' ;}?>>
                            <label class="name-title-lable" for="workorder-account_id">
                                <i class="red-i">*</i> 付款日期：
                            </label>
                            <div class="addinput-list">
                                <input type="text" name="collection_date" class="form-control enYMD"  value="<?=substr($info['collection_date'],0,10)?>" readonly id="collectionDate" placeholder="请选择">
                                <i class="icon2-rili riqi1"></i>
                                   <p class="misplacement" style="display:none" id="collection_date_msg"></p>
                            </div>
                            
                        </div>
                    </div>
                    <div class="information-list">
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0" id="dataEstimate">
                            <label class="name-title-lable" for="workorder-account_id">
                                <i class="red-i"></i> 预计安装日期：
                            </label>
                            <div class="addinput-list">
                                <input type="text" name="estimate_date" class="form-control enYMD"  value="<?=substr($info['estimate_date'],0,10)?>" readonly id="estimateDate" placeholder="请选择">
                                <i class="icon2-rili riqi1"></i>
                                <p class="misplacement"  id="estimate_date_msg"></p>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0 region-limit">
                            <label class="name-title-lable" for="workorder-account_id">
                                <i class="red-i"></i> 销售店面：
                            </label>
                            <div class=" addinput-list">
                                <input type="text" class="form-control" value="<?= isset($department_edit['id']) ? $department_edit['name'] : '请选择销售店面' ?>" id="category1"  placeholder="请选择上级机构" readonly  sucmsg="" nullmsg="请选择机构">
                                <input type="hidden" id="select_department_id" value="<?= isset($department_edit['id']) ? $department_edit['id'] : $define_department['id'] ?>" name="department_id">
                                <?php if($auth !=1){?>
                                    <i class="icon iconfont icon-shouqi1"></i>
                                    <p class="misplacement" style="bottom: -5px;" id="work_type_msg">请选择上级机构</p>
                                    <div class="drop-dw-layerbox">
                                        <div class="ul-box-h180">
                                            <ul>
                                                <?php if (!empty($department)) : ?>
                                                    <?php foreach ($department as $key => $val) : ?>
                                                        <li>
                                                   <span>
                                                      <?php if ($val['exist'] == 1) : ?>
                                                          <i class="icon2-shixinyou"  onclick="clickCategory(this) "></i>
                                                          <input type="radio" value="<?=$val['id'] ?>" name="department_id" id="<?=$val['id'] ?>" class="pr1" onclick="changeCategory(this)">
                                                      <?php else:?>
                                                          <input type="radio" value="<?=$val['id'] ?>" name="department_id" id="<?=$val['id'] ?>" class="pr1" onclick="changeCategory(this)">
                                                      <?php endif;?>
                                                       <label for="<?=$val['id'] ?>"><?=$val['name'] ?></label>
                                                  </span>
                                                        </li>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>
       
                    <div class="information-list">
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0">
                            <label class="name-title-lable" for="workorder-account_id">
                                <i class="red-i"></i> 销售人：
                            </label>
                            <span class="c_red"></span>
                            <div class="addinput-list">
                                <select class="form-control" id="sale_user_id" value="" name="sale_user_id" sucmsg=" " nullmsg="请选择销售人">
                                    <option value="<?= isset($info['sale_user_id']) ? $info['sale_user_id'] : '' ?>"><?= isset($info['sale_user_id']) ? $info['sale_user_info'] : '请先选择销售店面' ?></option>
                                </select>
                                <p class="misplacement"  id="sale_user_name_msg"></p>
                            </div>
                        </div>
                    </div>
                    <!--<div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i"></i> 销售人：
                        </label>
                        <span class="c_red"></span>
                        <div class="addinput-list">
                            <input type="text" name="sale_user_name" class="form-control" maxlength="20" id="sale_user_name"   value="<?/*=$info['sale_user_name']*/?>" datatype="*" sucmsg=" " nullmsg="请输入销售人姓名（20字以内）"  placeholder="请输入销售人姓名（20字以内）">
                            <p class="misplacement" style="display:none" id="sale_user_name_msg"></p>
                        </div>
                    </div>
                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i"></i> 销售电话：
                        </label>
                        <span class="c_red"></span>
                        <div class="addinput-list">
                            <input type="text" name="sale_phone" class="form-control" maxlength="11" id="sale_phone"   value="<?/*=$info['sale_phone']*/?>" datatype="*" sucmsg=" " nullmsg="请输入销售人电话"  placeholder="请输入销售人电话"  onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">
                            <p class="misplacement"  id="sale_phone_msg"></p>
                        </div>
                    </div>-->


                    <div class="col-md-12">
                        <h5 class="block-h5" style="margin-top: 30px;">产品明细</h5>
                    </div>

                    <div class="information-list">
                        <div class="col-xs-12 addinput-list" style="margin-left: 30px;margin-bottom: 20px;">
                            <table style="width: 95%;font-size: 12px;">
                                <thead>
                                <tr style="font-size: 14px;">
                                    <th style="width: 50px;" class="padding15">
                                        <input type="checkbox" name="" id="allDetailed">
                                        <label for="allDetailed"> <i class="gou-i"></i></label>
                                    </th>
                                    <th style="min-width: 40px">产品名称</th>
                                    <th style="min-width: 40px">产品编号</th>
                                    <th style="min-width: 40px">产品类型</th>
                                    <th style="min-width: 40px">产品类目</th>
                                    <th style="min-width: 80px;">品牌</th>
                                    <th style="width: 10%;">型号</th>
                                    <th style="width: 60px;">数量</th>
                                    <th style="min-width: 30px">单价</th>
                                    <th style="width: 80px;">折扣(%)</th>
                                    <th style="min-width: 50px;">总价</th>
                                    <th style="width: 80px;">折后总价</th>
                                    <th style="min-width: 60px">质保终止日期</th>
                                </tr>
                                </thead>
                                <!--<tfoot  id="show_prodcut"  >-->
                                <tbody id="materielList">
                                <?php foreach($detail as $v) :?>
                                    <tr data-id="<?=$v['prod_id']?>" id="tr<?=$v['prod_id']?>">
                                        <td class="padding15">
                                            <input type="hidden"  name="prod[<?=$v['prod_id']?>][prod_id]"    value="<?=$v['prod_id']?>" />
                                            <input type="checkbox" class="available_cost_type select_product" id="prod_id<?=$v['prod_id']?>"  value="<?=$v['prod_id']?>" />
                                            <label for="prod_id<?=$v['prod_id']?>" > <i class="gou-i"></label></td>
                                        <td>
                                            <?php if($v['prod_type'] == 3){?>
                                                <a href="javascript:" class="viewProductBag" id-data="<?=$v['prod_id']?>"><?=$v['prod_name']?></a>
                                            <?php }else{?>
                                                <?=$v['prod_name']?>
                                            <?php }?>
                                        </td>
                                        <td><?=$v['prod_no']?></td>
                                        <td><?=$v['type_name']?></td>
                                        <td><?=$v['class_name']?></td>
                                        <td><?=$v['brand_name']?></td>
                                        <td><?=$v['model']?></td>
                                        <td><input class="prod_num"  name="prod[<?=$v['prod_id']?>][prod_num]"  type="" maxlength="3"  value="<?=$v['prod_num']?>"/></td>
                                        <td><input class="price"     name="prod[<?=$v['prod_id']?>][sale_price]"   type="" size="10" value="<?=$v['sale_price']?>" onfocus="clearNoNum(this)"  onkeyup="clearNoNum(this)"/></td>
                                        <td><input class="rate"     type=""  name="prod[<?=$v['prod_id']?>][rate]"  maxlength="3" value="<?=$v['rate']?>"/></td>
                                        <td><span class="amount"><?=$v['amount']?></span><input     class="amount_hidden" name="prod[<?=$v['prod_id']?>][amount]"  type="hidden" value="<?=$v['amount']?>"/></td>
                                        <td><span class="total_amount"><?=$v['total_amount']?></span><input     class="total_amount_hidden" name="prod[<?=$v['prod_id']?>][total_amount]"   type="hidden"  value="<?=$v['total_amount']?>"/></td>
                                        <td><input class="warranty_date"  name="prod[<?=$v['prod_id']?>][warranty_date]" type="" value="<?=substr($v['warranty_date'],0,10)?>" maxlength="10" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"  readonly="readonly" /></td>
                                    </tr>
                                <?php endforeach;?>
                                <!-- <tr><td colspan="11">请选择产品<a id="addBtn">立即添加</a></td></tr> -->
                                </tbody>
                                <div id="product_hide" style="display:none;"><?php if(!empty($detail)){echo 1;}else{echo '';}?></div>
                            </table>
                            <div id="productBtnBox" class="margin-top30 bottom-btn-box">
                                <a href="javascript:" class="btn btn-orange " id="addBtn" style="color: #fff;"  onclick="addPr ()">选择产品</a>
                                <a class="btn delet-btn" href="javascript:" style="margin-left: 15px;color: #444;width: 80px">删除</a>
                            </div>
                            <p class="misplacement"  style="display:none" id="product_msg"></p>
                        </div>
                    </div>
                    <div class="information-list">
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0">
                            <label class="name-title-lable" for="workorder-account_id">
                                <i class="red-i"></i> 其他费用：
                            </label>
                            <span class="c_red"></span>
                            <div class="addinput-list">
                                <input type="text" name="another_cost_amount" class="form-control" value="<?=$info['another_cost_amount']?>" maxlength="30" id="otherExpenses" onfocus="clearNoNum(this)"  onkeyup="clearNoNum(this)" placeholder="请输入其它费用" >
                                 <p class="misplacement" style="display:none" id="another_cost_amount_msg"></p>
                            </div>
                        </div>
                    </div>
                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 合同总额：
                        </label>
                        <span class="c_red"></span>
                        <div class="addinput-list">
                            <input type="hidden" name="total_amount" class="form-control" maxlength="30" id="total_amount" value="<?=$info['total_amount']?>" />
                            <p id="allCost" class="height34"><?=$info['total_amount']?></p>
                             <p class="misplacement" style="display:none" id="total_amount_msg"></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5 class="block-h5">开票信息</h5>
                    </div>
                    <div class="information-list">
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0">
                            <label class="name-title-lable" for="workorder-account_id">
                                <i class="red-i">*</i> 开票类型：
                            </label>
                            <div class="addinput-list">
                                <div class="roundRadio select-Zhouqi">
                                    <input type="radio" class="available_cost_type invoice_type" name="invoice_type" value="1" <?php if($info['invoice_type']==1){echo 'checked';}?> id="kp1">
                                    <label for="kp1">不开票</label>
                                </div>
                                <div class="roundRadio  select-Zhouqi">
                                    <input type="radio" class="available_cost_type invoice_type" name="invoice_type"  value="2" id="kp2" <?php if($info['invoice_type']==2){echo 'checked';}?>>
                                    <label for="kp2">企业</label>
                                </div>
                                <div class="roundRadio  select-Zhouqi">
                                    <input type="radio" class="available_cost_type invoice_type" name="invoice_type"  value="3" <?php if($info['invoice_type']==3){echo 'checked';}?> id="kp3">
                                    <label for="kp3">个人</label>
                                </div>
                                 <p class="misplacement"  id="invoice_type_msg"></p>
                            </div>
                        </div>
                    </div>
                    <!--发票内容-->
                    <?php if($info['invoice_type']>=2) :?>
                    <div id="invoice_div">
                        <div class="information-list">
                            <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0">
                                <label class="name-title-lable" for="workorder-account_id">
                                    <i class="red-i">*</i> 发票抬头：
                                </label>
                                <div class="addinput-list">
                                    <input type="text" name="invoice_title" class="form-control" maxlength="30" id="invoice_title" placeholder="请输入发票抬头"  value="<?=$info['invoice_title']?>">
                                    <p class="misplacement" id="invoice_title_msg"></p>
                                </div>
                            </div>
                            <?php if($info['invoice_type']==2) :?>

                            <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0"  id="invoice_tax_no">
                                <label class="name-title-lable" for="workorder-account_id">
                                    <i class="red-i">*</i> 发票税号：
                                </label>
                                <span class="c_red"></span>
                                <div class="addinput-list">
                                    <input type="text" name="invoice_tax_no" class="form-control" maxlength="30" id="invoice_tax_no_input" placeholder="请输入纳税人识别号"  value="<?=$info['invoice_tax_no']?>" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')">
                                    <p class="misplacement"  id="invoice_tax_no_msg"></p>
                                </div>
                            </div>
                            <?php endif;?>
                        </div>                            
                    </div>
                     <?php endif;?>
                    <!--发票内容结束-->
                    <div class="col-md-12">
                        <h5 class="block-h5">收货信息</h5>
                    </div>

                    <div class="information-list">
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0">
                            <label class="name-title-lable" for="workorder-account_id">
                                <i class="red-i"></i> 收货方式：
                            </label>
                            <span class="c_red"></span>
                            <div class="addinput-list">
                                <select name="express_type" class="form-control">
                                    <option value=null>请选择收货方式</option>
                                    <option value="1" <?php if($info['express_type']==1){echo 'selected';}?>>带货安装</option>
                                    <option  value="2" <?php if($info['express_type']==2){echo 'selected';}?>>客户自提</option>
                                    <option  value="3" <?php if($info['express_type']==3){echo 'selected';}?> >物流运输</option>
                                </select>
                                 <p class="misplacement" id="subjectct_msg"></p>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 padding-left0">
                            <label class="name-title-lable" for="workorder-account_id">
                                <i class="red-i"></i> 收货地址：
                            </label>
                            <div class="addinput-list">
                                <select type="text" class="form-control" name="address_id" id="account_address">
                                    <?php if(!empty($info['delivery_address'])):?>
                                        <?php echo $info['delivery_address'];?>
                                    <?php else: ?>
                                        <option value="0">请选择客户</option>
                                    <?php endif;?>
                                    <?=$address?>
                                </select>
                                <input name="delivery_address" id="delivery_address" type="hidden" value=" "/>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-2 col-sm-2 col-xs-2 add-btn padding-left0">
                            <span class="btn btn-success  " style=" color:#FFF;font-size:14px; " id="addAccountAddres"  data-toggle="modal" data-target="#scrollingModal" title="<?php if(isset($info['account_id'])) echo $info['account_id'];?>">添加</span>
                            <p class="misplacement"  id="address_msg"></p>
                        </div>
                       
                    </div>
                    <div class="col-md-12">
                        <h5 class="block-h5">其它信息</h5>
                    </div>
                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            附件：
                        </label>
                        <div class="addinput-list" >
                            <?= $form->field($model, 'attachment_url')->widget('manks\FileInput', [
                                    'clientOptions'=>[
                                        'server' => Url::to('../upload/upload-other?source=fws'),
                                        'pick'=>[
                                            'multiple'=>true,
                                        ],
                                        'accept' => [
                                            //'title' => 'Images',
                                            'extensions' => 'gif,jpg,jpeg,bmp,png,xlsx,xls,docx,pdf,txt,doc',
                                            'mimeTypes' => 'image/jpg,image/jpeg,image/png,application/vnd.ms-excel,text/plain,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                        ],
                                        'fileNumLimit'=>'5',
                                        'duplicate'=>true,
                                        'fileSingleSizeLimit'=>'2097152', //限制上传大小为2M
                                    ]
                                ]
                            ); ?>
                            <span style="position: absolute;left: 104px;color: red;z-index: 9;top: 7px;">支持上传jpg、png、gif、pdf、txt、docx、xlsx格式的文件</span>
                        </div>
                        <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10 addinput-list" style="margin-top: -30px" id="editConFile">
                            <div class="input-group multi-img-details" id="editFile">
                                <?php if(isset($info['attachment_data'])) {?>
                                    <?php foreach ($info['attachment_data'] as $k=>$v) {?>
                                        <div class="multi-item" style="max-width: 250px;margin-bottom: 5px;">
                                            <span style="margin-right: 10px;"><a href="<?php echo $v['attachment_url']?>" target="_blank"><?php echo $v['name']?></a></span>
                                            <input type="hidden" name="ContractForm[attachment_url][]" value="<?php echo $v['name'].'*'.$v['attachment_url']?>">
                                            <a class="delFile" href="javascript:;">删除</a>
                                        </div>
                                        <br>
                                    <?php }?>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            备注：
                        </label>
                        <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7 addinput-list" style="margin-bottom: 30px;">
                            <textarea class="ramark-box" id="remarks" maxlength="200" placeholder="请输入备注信息（200字以内）" name="express_description" id="express_description"><?php if(isset($info['express_description'])):?><?=$info['express_description']?><?php endif;?></textarea>
                            <p class="xianzhi"><span  id='word'>0</span>/200</p>
                        </div>
                    </div>
                    <hr style="clear: both;">
                     <div class="information-list" style="text-align: center;">
                        <input type="submit" class="btn btn-orange mgr20 btnSubmit"  value="提交"  />
                        <input type="button" class="btn btn-orange mgr20 btnSubmiting" style="display: none;" value="提交中..."  />
                        <a href="/contract/wait-check"><button type="button" style="border: 1px solid #cccccc;" class="btn bg-f7">返回</button></a>
                           
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- 时间插件/ -->

<script type="text/javascript" src="/js/jquery-migrate-1.1.0.js"></script>
<script src="/js/jquery.autocomplete.js" type="text/javascript"></script>
<script>
    var myDate = new Date();
    var myMonth = myDate.getMonth()<10?"0"+(myDate.getMonth()+1):myDate.getMonth();
    var myDay = myDate.getDate()<9?'0'+myDate.getDate():myDate.getDate();
    var minDate = myDate.getFullYear()+'-'+myMonth+'-'+myDay;
    //console.log(myDate.getDate());
    jeDate('#siginDate', {
        format: 'YYYY-MM-DD',
        maxDate: '2099-06-16 23:59:59'
    })
    jeDate('#startDate', {
        format: 'YYYY-MM-DD',
        maxDate: '2099-06-16 23:59:59'
    })

    $('#collectionDate').on('click',function() {
        jeDate('#collectionDate', {
            trigger:false,
            isToday:false,
            format: 'YYYY-MM-DD',
            //minDate: $('#startDate').val().length>0?$('#startDate').val():minDate,
            maxDate: '2099-06-16 23:59:59'
        })
    })
    //预计安装日期
    $('#estimateDate').on('click',function() {
        jeDate('#estimateDate', {
            trigger:false,
            isToday:false,
            format: 'YYYY-MM-DD',
            //minDate: $('#startDate').val().length>0?$('#startDate').val():minDate,
            maxDate: '2099-06-16 23:59:59'
        })
    })

    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    }
    // 字数限制
    $("#remarks").keyup(function(){
        if($("#remarks").val().length > 200){
            $("#remarks").val( $("#remarks").val().substring(0,200) );
        }
        $("#word").text( $("#remarks").val().length ) ;
    });
    //单项选择
    $('.radio-li').on('click',function() {
        $(this).find('span').css({'background':'#2693ff','color':'#fff'});
        $(this).siblings('li').find('span').css({'background':'#fff','color':'#333','border-color':'#ccc'});
        $(this).parent('ul').next('input').val($(this).find('span').attr('id'));
    })
    // 选择
    $('.xiangxia').on('click',function() {
        $('.addinput-list-data').hide();
        $(this).next('.addinput-list-data').show();
    })
    //
    $('.addinput-list-data li').on('click',function() {
        $('.addinput-list-data').hide();
        $(this).parent('ul').siblings('.form-control').val($(this).attr('id '));
    })

    //添加客户
    var iframeSrc = '';
    $("#addAccount").click(function () {
        iframeSrc = 'order';
        layer.open({
            type: 2,
            title: '添加客户',
            area: ['500px', '400px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/account/add/'
        });
    });
    //添加客户地址
    var iframeCallbackAddressId = 0;
    $("#addAccountAddres").click(function() {
        var id = $(this).attr('title');
        if(id==null || id==undefined || id=='' || isNaN(parseInt(id))) {
            //$("#address_msg").text("请选择客户");
            $("#address_msg").show();
        }else {
            layer.open({
                type: 2,
                title: '添加客户地址',
                area: ['580px', '400px'],
                fixed: false, //不固定
                maxmin: false,
                content: '/account-address/add?id='+id,
                end  : function(){
                    account_address(id)
                }
            });
            account_address(id)
        }
    });


    // 收款状态改变
    $("input[name='collection_status']").change(function(){
        if($(this).attr('id')=="available_cost_type2"){
            $('#paymentMethodType').hide();
            $('#dataReceipt').hide();
            $('#collection_date').val('');
        }else{
            $('#dataReceipt').show();
            $('#paymentMethodType').show();
        }
    })

    /************************************ajax获取客户相关信息********************************************/
    var iframeCallbackSaleOrderId = 0;

    //模糊查询选中
    var accountId = 0;
    function accountName(id) {

        $("#brand_msg").hide();
        $("#address_msg").hide();
        $("#provide_msg").hide();

        accountId = id;
        $("#account_id").val(id);
        $("#addAccountAddres").attr('title',id);
        $("#addAccountBrand").attr('title',id);
        $("#account_select").hide();
        account_address(id);
        service_brand(id);
    }
    //查询客户对应的地址信息
    function account_address(account_id) {
        $.post('/order/get-address',{account_id:account_id},function(res) {
            $("#account_address").html(res);
            $('#delivery_address').val($('#account_address').find('option:first').text());
            if(iframeCallbackAddressId>0){
                $("#account_address").val(iframeCallbackAddressId);
            }
        })
    }
    //查询客户对应服务产品
    function service_brand(account_id) {
        $.post('/order/get-service-brand',{account_id:account_id},function(res) {
            $("#account_service_brand").html(res);
            if(iframeCallbackSaleOrderId > 0 ){
                $("#account_service_brand").val(iframeCallbackSaleOrderId);
                $("#account_service_brand").change();
            }
        })
    }

    /***********************     END       ************************/
    /**********************   提交信息验证   ************************/



    //获取焦点提示框消失
    $("#account_name").focus(function() {
        $("#account_msg").text("");
        $("#account_msg").hide();
    })
    $("#subject").focus(function(){
        $("#subject_msg").text("");
        $("#subject_msg").hide();
    })
    $("#datetimepicker-pay-top").focus(function() {
        $("#time_msg").text("");
        $("#time_msg").hide();
    })
    $("#datetimepicker-pay-end").focus(function() {
        $("#time_msg").text("");
        $("#time_msg").hide();
    })

    $("#collection_date").focus(function() {
        $("#collection_date_msg").text("");
        $("#collection_date_msg").hide();
    })
    $("#invoice_title").focus(function() {
        $("#invoice_title_msg").text("");
        $("#invoice_title_msg").hide();
    })
    $("#invoice_tax_no_input").focus(function() {
        $("#invoice_tax_no_msg").text("");
        $("#invoice_tax_no_msg").hide();
    })
    $("#contract_type").click(function() {
        $("#contract_type_msg").text("");
        $("#contract_type_msg").hide();
    })
    $("#contract_classify").click(function() {
        $("#contract_classify_msg").text("");
        $("#contract_classify_msg").hide();
    })
    $("#contract_brand").click(function() {
        $("#contract_brand_msg").text("");
        $("#contract_brand_msg").hide();
    })
    $("#payment_type").click(function() {
        $("#payment_type_msg").text("");
        $("#payment_type_msg").hide();
    })
    $("#estimateDate").click(function() {
        $("#estimate_date_msg").text("");
        $("#estimate_date_msg").hide();
    })
    $("#account_name").autocomplete("/account/sreach", {
        minChars: 1,
        matchCase:false,//不区分大小写
        autoFill: false,
        max: 10,
        dataType: 'json',
        width:$('#account_name').outerWidth(true)+'px',
        extraParams:{v:function() { return $('#account_name').val();}},
        parse: function(data) {
            return $.map(eval(data), function(row) {
                return {
                    data: row,
                    value: row.id,    //此处无需把全部列列出来，只是两个关键列
                    result: row.name
                }
            });
        },
        formatItem: function(row, i, max,term) {
            return  row.name;
        },
        formatMatch: function(row, i, max) {
            return row.name;
        },
        formatResult: function(row) {
            return row.name;
        },
        reasultSearch:function(row,v)//本场数据自定义查询语法 注意这是我自己新加的事件
        {
            //自定义在code或spell中匹配
            if(row.data.name.indexOf(v) == 0)
            {
                return row;
            }
            else
                return false;
        }
    }).result(function(event, data, formatted) { //回调
        if(data.id>0){
            accountName(data.id);
            $('#account_name').val(data.name);
        }
        else {
            $('#account_name').val('');
        }
    });



    //填充用户信息
    function fillAccount($accountId,$accountName) {
        $("#account_name").val($accountName);
        $('#account_id').val($accountId);
        accountName($accountId);
    }

    $("#account_address").click(function() {
        if(( $.trim($("#account_address").html()) == '')){
            $("#address_msg").text("请先选择客户");
            $("#address_msg").show();
        }else{
            var accountAddress = $("#account_address").find("option:selected").text();
            var accountName = $("#account_name").val();
            $("#delivery_address").val(accountAddress);
        }

    });





    $("#sale_phone").blur(function(){
        var mobile = $(this).val();
        pre = /^[1][3,4,5,7,8][0-9]{9}$/;
        if(!pre.test(mobile)){
            $("#sale_phone_msg").html('请输入正确的手机号码格式');
            $("#sale_phone_msg").css("display",'block');
        }else{
            $("#sale_phone_msg").html('');
            $("#sale_phone_msg").css("display",'none');
        }

    })
    var loop=1;//循环数据
    var chlidMsg='';
    function addPr () {
        $("#product_msg").hide();
        var start_date = $("#datetimepicker-pay-end").val();
        layer.open({
            type: 2,
            title: '添加服务产品',
            area: ['800px', '400px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/product/share-list?type=1',
            end:function(){
                //var str = $("#product_hide").html();
                if(chlidMsg!=""){
                    console.log(chlidMsg);
                    var html="";
                    $.each(chlidMsg,function(i,data) {
                        html += '<tr id="tr'+data.id+'" data-id="'+data.id+'"><td class="padding15"><input type="checkbox" name="product_id" class="available_cost_type select_product" id="prod_id'+data.id+'"  value="'+data.id+'"> <label for="prod_id'+data.id+'"><i class="gou-i"></label> ' +
                            '<input type="hidden" name="prod['+data.id+'][prod_id]"    value="'+data.id+'">'+
                            '</td><td>';
                        data.type_id=="3"?  html +='<a href="javascript:" class="viewProductBag" id-data="'+data.id+'">'+data.prod_name+'</a>':  html +=data.prod_name;
                        html +='</td><td>'+data.prod_no+'</td>'+
                            '<td>'+data.type_name+'</td>'+
                            '<td>'+data.class_name+'</td>'+
                            '<td>'+data.brand_name+'</td>'+
                            '<td>'+data.model+'</td>'+
                            '<td><input class="prod_num"  name="prod['+data.id+'][prod_num]"  type="" maxlength="9"  value="1"/></td>'+
                            '<td><input class="price"     name="prod['+data.id+'][sale_price]"   type="" size="10" value="'+Number(data.sale_price)+'" onfocus="clearNoNum(this)"  onkeyup="clearNoNum(this)"/></td>'+
                            '<td><input class="rate"     type=""    name="prod['+data.id+'][rate]"   maxlength="3" value="100"/></td>'+
                            '<td ><span class="amount">'+data.sale_price+'</span> ' +
                            '<input     class="amount_hidden" name="prod['+data.id+'][amount]"  type="hidden" value="'+data.sale_price+'"/></td>'+
                            '<td ><span class="total_amount">'+data.sale_price+'</span> ' +
                            '<input     class="total_amount_hidden" name="prod['+data.id+'][total_amount]"   type="hidden"  value="'+data.sale_price+'"/></td>'+
                            '<td><input readonly="readonly"  class="enYMD warranty_date"  name="prod['+data.id+'][warranty_date]" type=""  value="'+timestampToTime(data.create_time,'ymd')+'" maxlength="10" onclick="jeData(this) " />' +
                            '</td></tr>';
                    })
                    getAllTotal();
                    $('#materielList').find('.tr1').remove();
                    $('#productBtnBox').show();
                    $('#materielList').append(html);
                }
                getAllTotal();
                // return false;
                // if(!warranty_date){
                //     warranty_date = '';
                // }

                // var show_product = $("#show_prodcut").html();
                // if(show_product.length>0){
                //     var haveExist = $("#"+data.data.id).find("#prod_id"+prod_id).val();
                //     if(!haveExist){  //勾选重复的产品不会显示
                //         $("#show_prodcut").append('<tr id="'+data.data.id+'">'+html+'</tr>');
                //         getAllTotal();
                //     }
                // }
                // $("#product_msg").html('');
                // $("#product_msg").hide();

                //产品数量验证
                $(".prod_num").keyup(function(){
                    var prod_num = $(this).val();
                    if(isNaN(prod_num)){
                        alert('请输入数字');
                        $(this).val('1');
                        return false;
                    }
                    if(prod_num!='' && prod_num==0){
                        $(this).val('1');
                    }
                    var id = $(this).parents('tr').attr("id");
                    getTotal(id);
                    getAllTotal();
                });


                //产品价格验证
                $(".price").blur(function(){
                    var price = $(this).val();
                    if(/^(?!(0[0-9]{0,}$))[0-9]{1,}[.]{0,}[0-9]{0,}$/.test(price))
                    {
                        price = new Number(price);
                        price = price.toFixed(2);
                        $(this).val(price);

                    }else{
                        alert('请输入正确的价格');
                        return false;
                    }
                    var id = $(this).parents('tr').attr("id");
                    getTotal(id);
                    getAllTotal();
                });
                $(".rate").keyup(function(){
                    var rate = $(this).val();
                    if(isNaN(rate)){
                        alert('请输入数字');
                        $(this).val(1);
                        return false;
                    }
                    if(rate!='' && rate==0){
                        $(this).val('1');
                        return false;
                    }
                    var id = $(this).parents('tr').attr("id");
                    getTotal(id);
                    getAllTotal();
                });
                //计算单个产品总额
                function getTotal(id){
                    var price      =  $("#"+id).find('.price').val();
                    var prod_num   =  $("#"+id).find('.prod_num').val();
                    var rate       =  $("#"+id).find('.rate').val();
                    var total      = price*prod_num;
                    total          = new Number(total);
                    total          = total.toFixed(2);
                    var total_amount = total*rate/100;
                    total_amount = new Number(total_amount);
                    total_amount = total_amount.toFixed(2);
                    $("#"+id).find('.amount').html(total);
                    $("#"+id).find('.amount_hidden').val(total);
                    $("#"+id).find('.total_amount').html(total_amount);
                    $("#"+id).find('.total_amount_hidden').val(total_amount);

                };


            }
        });
    }


    //计算合同总额
    function getAllTotal(){
        another_cost_amount = $("#otherExpenses").val();
        if(another_cost_amount != ''){
            another_cost_amount =  parseFloat(another_cost_amount,2);
        }else {
            another_cost_amount =  0.00;
        }
        //获取产品明细总价
        var checkboxInput = $(".select_product");
        var sum = 0;
        $("td .total_amount").each(function(i){
            // id = $(this).text();
            // var total_amount = $("#"+id).find('.total_amount_hidden').val();
            var total_amount = $(this).text();
            sum = sum+parseFloat(total_amount);
        });

        sum = another_cost_amount+sum;
        sum = new Number(sum);
        sum = sum.toFixed(2);
        $("#total_amount").val(sum);
        $('#allCost').text(sum);
        another_cost_amount = new Number(another_cost_amount);
        another_cost_amount = another_cost_amount.toFixed(2);
        $("#otherExpenses").val(another_cost_amount);
    }


/*    //全选计算合同总额
    $('#allDetailed').on('change',function () {
        var amountText =$('.total_amount');

        var checkboxInput = $('tfoot input[type=checkbox]');
        var sum=0,sum1=0;

        if( $(this).is(':checked')){
            checkboxInput.prop("checked",true);
            amountText.each(function() {
                sum += Number($(this).text());
            })
            sum1 = sum + Number($('#otherExpenses').val());
            $("#product_msg").html('');
            $("#product_msg").hide();

        }else{
            sum1 = Number($('#otherExpenses').val());
            checkboxInput.prop("checked",false);
        }
        $('#allCost').text(sum1)
    })*/

    //其他费用填写数据之后计算总费用
    $('#otherExpenses').on('blur',function () {
        var another_cost_amount = $(this).val();
        if(isNaN(another_cost_amount)){
            alert('请输入正确的费用类型');
            return false;
        }
        getAllTotal()
    })


    //价格数据格式化
    function clearNoNum(obj){
        if(obj.value !=''&& obj.value.substr(0,1) == '.'||obj.value=='0.00'){
            obj.value="";

        }
        obj.value = obj.value.replace(/^0*(0\.|[1-9])/, '$1');
        obj.value = obj.value.replace(/[^\d.]/g,"");
        obj.value = obj.value.replace(/\.{2,}/g,".");
        obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
        obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数
        if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
            if(obj.value.substr(0,1) == '0' && obj.value.length == 2){
                obj.value= obj.value.substr(1,obj.value.length);
            }
        }
    }

    //删除产品
    $(".delet-btn").click(function(){
        ids = new Array();
        $(".select_product").each(function(i){
            if( $(this).is(':checked')){
                var id = $(this).val();
                if(id>0){
                    ids[i] = id;
                }
            }
        })
        if(ids.length>0){
            confirm('确认删除产品吗?',function(){
                for(j=0;j<=ids.length;j++){
                    $("#tr"+ids[j]).remove();
                    getAllTotal();
                }
                if($('#materielList tr').length==0){$('#materielList').html('<tr class="tr1"><td colspan="13">请选择产品<a href="javascript:" class="get-share-list" onclick="addPr ()">立即添加</a></td></tr>');$('#productBtnBox').hide()}
                $("#allDetailed").attr("checked",false);
            },this)
        }else{
            alert('请选择需要删除的产品');
        }
    })

    //全选删除按钮
    $('#allDetailed').on('change',function () {
            var _that = $(this);
            $('#materielList tr').each(function() {
                if(_that.is(':checked')){
                    $('#prod_id'+$(this).data('id')).prop('checked',true);
                }else{
                    $('#prod_id'+$(this).data('id')).prop('checked',false);
                }
                
            })
    });

    //发票显示js
    $(".invoice_type").click(function(){
        var inputValue = $(this).val();
        if(inputValue==1){
            $("#invoice_div").css("display",'none');
        }else if(inputValue==2){
            $("#invoice_div").css("display",'block');
            $("#invoice_tax_no").css("display",'block');
        }else{
            $("#invoice_div").css("display",'block');
            $("#invoice_tax_no").css("display",'none');
        }

    })



    //表单数据验证
    $("#contract_edit").submit(function(){
        var subject = $("#subject").val();
        var signTime = $("#siginDate").val();
        var startTime  = $("#startDate").val();
        var collectionDate = $("#collectionDate").val();   //付款日期
        var estimateDate = $("#estimateDate").val();       //预计安装日期
        var costTYpe = $(".available_cost_type:checked").val();
        var saleUserName = $("#sale_user_name").val();
        var salePhone = $("#sale_phone").val();
        var prodcutHidden  = $("#materielList").find('tr').attr('class')!='tr1'?$("#materielList").find('tr').length:0;
        var productLength = 0;
        var contractType = $("#contract_type").val();   //合同类型
        var contractClassify = $("#contract_classify").val();   //合同分类
        var contractBrand = $("#contract_brand").val();   //合同品牌
        var paymentType = $("#payment_type").val();   //付款方式
        //计算勾选的产品个数
        if(prodcutHidden.length>0){
            var checkboxInput = $(".select_product");
            var sum = 0;
            ids =new Array();
            $("input[name='product_id']").each(function(i){
            //$(".select_product").each(function(i){
                if(this.checked){
                    ids[i] = $(this).val();
                }
            })
            productLength = ids.length;
        }

        //验证产品价格
        noPrice = new Array();
        $(".price").each(function(i){
            var price = parseFloat($(this).val(),2);
            if(price<=0.00){
                noPrice[i] = price;

            }
        })
        var noPriceLen = noPrice.length;

        var invoiceType = $(".invoice_type:checked").val();
        //console.log(invoiceType);return false;
        if(invoiceType==2){
            var invoiceTitle = $("#invoice_title").val();
            var invoiceTaxNo = $("#invoice_tax_no_input").val();
        }
        if(invoiceType==3){
            var invoiceTitle = $("#invoice_title").val();
        }

         var accountAddress = $("#account_address").val();
        if(!accountAddress){
            $("#delivery_address").val('');
        }else{
            var accountAddress = $("#account_address").find("option:selected").text();
            var accountName = $("#account_name").val();
            $("#delivery_address").val(accountAddress);
        }

        if(!subject){
            $("#subject_msg").css("display",'block');
            $("#subject_msg").html('请输入合同主题');
            return false;
        }
        /*if(contractType == '' || contractType == 0){
            $("#contract_type_msg").css("display",'block');
            $("#contract_type_msg").html('请选择合同类型');
            return false;
        }
        if(contractClassify == '' || contractClassify == 0){
            $("#contract_classify_msg").css("display",'block');
            $("#contract_classify_msg").html('请选择合同分类');
            return false;
        }
        if(contractBrand == '' || contractBrand == 0){
            $("#contract_brand_msg").css("display",'block');
            $("#contract_brand_msg").html('请选择合同品牌');
            return false;
        }*/
        if(!signTime){
            $("#time_msg").css("display",'block');
            $("#time_msg").html('请选择签订日期');
            return false;
        }
        if(!startTime){
            $("#time_msg").css("display",'block');
            $("#time_msg").html('请选择开始日期');
            return false;
        }
        if(costTYpe==1 || costTYpe==3){
            if(paymentType == '' || paymentType == 0){
                $("#payment_type_msg").css("display",'block');
                $("#payment_type_msg").html('请选择付款方式');
                return false;
            }
        }
        if(!collectionDate && costTYpe==1){
            $("#collection_date_msg").css("display",'block');
            $("#collection_date_msg").html('请选择付款日期');
            return false;
        }
        if(!collectionDate && costTYpe==3){
            $("#collection_date_msg").css("display",'block');
            $("#collection_date_msg").html('请选择付款日期');
            return false;
        }
        /*if(!estimateDate){
            $("#estimate_date_msg").css("display",'block');
            $("#estimate_date_msg").html('请选择预计安装日期');
            return false;
        }*/
        /*  if(!saleUserName){
              $("#sale_user_name_msg").css("display",'block');
              $("#sale_user_name_msg").html('请输入销售人');
              return false;
          }

          if(!salePhone){
              $("#sale_phone_msg").css("display",'block');
              $("#sale_phone_msg").html('请输入销售电话');
              return false;
          }*/
        if(!prodcutHidden){
            $("#product_msg").css("display",'block');
            $("#product_msg").html('请添加产品');
            return false;
        }
        /*if(prodcutHidden.length>0 && productLength==0){
            $("#product_msg").css("display",'block');
            $("#product_msg").html('请勾选产品');
            return false;
        }


*/
        if(noPriceLen){
            $("#product_msg").css("display",'block');
            $("#product_msg").html('产品价格不能为零');
            return false;
        }

        if(invoiceType==2){
            if(!invoiceTitle){
                $("#invoice_title_msg").css("display",'block');
                $("#invoice_title_msg").html('请输入发票抬头');
                return false;
            }
            if(!invoiceTaxNo){
                $("#invoice_tax_no_msg").css("display",'block');
                $("#invoice_tax_no_msg").html('请输入发票税号');
                return false;
            }
        }

        if(invoiceType==3){
            if(!invoiceTitle){
                $("#invoice_title_msg").css("display",'block');
                $("#invoice_title_msg").html('请输入发票抬头');
                return false;
            }
        }
    })


    //提示
    $(".tip").popover();
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message');?>');
    setTimeout('window.location.href="/contract/wait-check";',1500);
    <?php endif;?>
    <?php if($info['status']>=2 && $info['audit_status']==1) :?>
    alert('该合同以通过审批，不可修改');
    setTimeout('window.location.href="/contract/executing";',1500);
    <?php endif;?>


   $(".product_delete").live('click',function(){
        $(".select_product").each(function(i){
            alert(33);
        })
    })
    //选择门店
    function  clickCategory(el) {
        var auth = <?php echo $auth;?>;
        var id = $(el).next('input').val();
        var _this = $(el).parent('span');
        $.getJSON('/common/ajax-get-department',{'pid':id,'auth':auth,'type':2},function (data) {
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>"
                $(data.data).each(function(i,el) {
                    ulData +="<li><span>";
                    el.exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="clickCategory(this)"></i>':'';
                    ulData+='<input type="radio" value="'+el.id+'" name="department_id" id="'+el.id+'" class="pr1" onclick="changeCategory(this)"><label for="'+el.id+'">'+el.name+'</label></span></li>';
                });
                ulData +="</ul>";
                _this.parent('li').append(ulData);
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');

            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })

    }
    function changeCategory(el) {
        $('.drop-dw-layerbox').hide();
        $('#select_department_id').val($(el).next('label').attr("for"));   //父级机构id
        $('#category1').val($(el).next('label').text());
        //选择机构部门后获取用户列表
        $("#sale_user_id").html("<option value=''>请选择销售人</option>");
        var SelectDepartmentId = $('#select_department_id').val();
        if(SelectDepartmentId != ''){
            $.get("/user/get-user-list", { "pid": SelectDepartmentId }, function (data) {
                $("#sale_user_id").html(data);
            });
        }

    }
    // 点击空白处关闭
    $(document).click(function(event) {
        var region = $('.region-limit');
        if (!region.is(event.target)&&region.has(event.target).length ===0) {
            $('.drop-dw-layerbox').hide();
        }
    });
    $('#category1').on('click',function(event) {
        event.preventDefault();
        /* $("#select_department_id").remove();
         $("#category1").val('');*/
        $('.drop-dw-layerbox').is(':hidden') ? $('.drop-dw-layerbox').show():$('.drop-dw-layerbox').hide();
        $('.role').val('');
        $('#role_Select').val('');

    })
    //更改上传文件按钮文字
    changeUp();
    function changeUp() {
        $('.btn-default').html('点击上传');
    }
    //查看配件包，物料清单
    $(document).on('click','.viewProductBag',function() {
        var prodId = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['查看物料清单', 'font-size:18px;'],
            area: ['800px', '500px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/product/materials-detail?id='+prodId,
        });
        return false;
    })
</script>


<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
use common\models\DictEnum;
/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = '合同详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time();?>">
<style type="text/css">
    .information-list {margin-top: 0;}
</style>

<div class="jn-title-box no-margin-bottom">
    <span class="jn-title" id="no" data-no="<?php $info['no'];?>">合同详情</span>
    <div  class="right-btn-box">
        <?php if(($info['audit_status'] == 0 && $info['status'] == 1)||($info['audit_status'] == 2 && $info['status'] == 1)){?>
            <!--<a href="" class="cancelOrder">
                <button class="btn  btn-orange">打印</button>
            </a>-->
            <?php if(in_array('/contract/check-option',$selfRoles)):?>
                <a href="" class="setAudit" data-type="no">
                    <button class="btn  btn-orange" class="checkbtn" id="refused">拒绝</button>
                </a>
                <a href="" class="setAudit" data-type="ok">
                    <button class="btn  btn-orange" class="checkbtn" id="agreen">同意</button>
                </a>
            <?php endif;?>
        <?php }?>
        <?php if($info['audit_status'] == 2 && in_array($info['status'],[2,3]) ){?>
            <!--<a href="" class="cancelOrder">
                <button class="btn  btn-orange">打印</button>
            </a>-->
            <a href="/order/re-assign?work_no=" >
                <button class="btn  btn-orange">入库</button>
            </a>
        <?php }?>
        <?php if($info['audit_status'] == 2 && $info['status'] == 1 ){?>
            <!--<a href="" class="cancelOrder">
                <button class="btn  btn-orange">打印</button>
            </a>-->
        <?php }?>
        <button class="btn bg-f7" onclick="javascript:history.back(-1);">返回</button>
    </div>
</div>

<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body" style="padding:0 30px 50px 30px;">
                    <h5 class="block-h5" style="margin-top: 0">基本信息</h5>
                    <div class="information-list">
                        <div class="col-xs-12	col-sm-12	col-md-6    col-lg-4">
                            <label  class="left-title80">合同编号：</label>
                            <input type="hidden" value="<?=$info['id']?>" id="id"/>
                            <div class="right-title3"><span><?=$info['no']?></span></div>
                        </div>
                        <div class="col-xs-12	col-sm-12	col-md-6    col-lg-4">
                            <label  class="left-title80">合同主题：</label>
                            <div  class="right-title3"><span><?=$info['subject']?></span></div>
                        </div>
                        <div class="col-xs-12	col-sm-12	col-md-6    col-lg-4">
                            <label   class="left-title80">关联客户：</label>
                            <div  class="right-title3"><span><a href="/account/view?account_id=<?=$info['account_id']?>"><?=$info['account']?></a></span></div>
                        </div>
                        <div class="col-xs-12	col-sm-12	col-md-6    col-lg-4">
                            <label   class="left-title80">合同类型：</label>
                            <div  class="right-title3"><span><?=$info['contract_type']?></a></span></div>
                        </div>
                        <div class="col-xs-12	col-sm-12	col-md-6    col-lg-4">
                            <label   class="left-title80">合同分类：</label>
                            <div  class="right-title3"><span><?=isset($info['contract_classify'])?$info['contract_classify']:''?></a></span></div>
                        </div>
                        <div class="col-xs-12	col-sm-12	col-md-6    col-lg-4">
                            <label   class="left-title80">合同品牌：</label>
                            <div  class="right-title3"><span><?=$info['contract_brand']?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                            <label  class="left-title80">签订日期：</label>
                            <div class="right-title3"><span><?=substr($info['signed_date'],0,10)?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                            <label   class="left-title80">开始日期：</label>
                            <div  class="right-title3"><span><?=substr($info['start_date'],0,10)?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                            <label   class="left-title80">合同状态：</label>
                            <div  class="right-title3 2693ff"><span><?=$info['show_status']?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                            <label   class="left-title80">付款状态：</label>
                            <div  class="right-title3"><span><?=$info['show_collection_status']?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                            <label   class="left-title80">付款方式：</label>
                            <div  class="right-title3"><span><?=$info['show_payment_type']?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                            <label  class="left-title80">出库进展：</label>
                            <div class="right-title3"><span><?=isset($info['showStoreOutStatus'])?$info['showStoreOutStatus']:''?></span></div>
                        </div>
                        <?php if($shipSys == 1){?>
                            <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                                <label  class="left-title80">发货进展：</label>
                                <div class="right-title3"><span><?=isset($info['ship_status_desc'])?$info['ship_status_desc']:''?></span></div>
                            </div>
                        <?php }?>
                        <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                            <label   class="left-title80">付款日期：</label>
                            <div  class="right-title3"><span><?=substr($info['collection_date'],0,10)?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                            <label   class="left-title80">预计安装时间：</label>
                            <div  class="right-title3"><span><?=substr($info['estimate_date'],0,10)?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                            <label  class="left-title80">销售人：</label>
                            <div class="right-title3"><span><?=$info['sale_user_name']?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                            <label   class="left-title80">销售人电话：</label>
                            <div  class="right-title3"><span><?=$info['sale_phone']?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                            <label   class="left-title80">销售店面：</label>
                            <div  class="right-title3"><span><?=$info['sale_shop_name']?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                            <label   class="left-title80">创建人：</label>
                            <div  class="right-title3"><span><?=$info['create_user']?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                            <label  class="left-title80">创建时间：</label>
                            <div class="right-title3"><span><?=substr($info['create_time'],0,10)?></span></div>
                        </div>
                    </div>
                    <h5 class="block-h5">产品明细</h5>
                    <div class="information-list" style="height: auto">
                        <table>
                            <thead>
                            <tr>
                                <th>产品名称</th>
                                <th>产品编号</th>
                                <th>产品类型</th>
                                <th>产品类目</th>
                                <th>品牌</th>
                                <th>型号</th>
                                <th>数量</th>
                                <th>单价</th>
                                <th>折扣（%）</th>
                                <th>总价</th>
                                <th>折后总价</th>
                                <th>质保终止日期</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($detail as $v) :?>
                                <tr>
                                    <td><?php if($v['prod_type'] == 3){?>
                                            <a href="javascript:" class="viewProductBag" id-data="<?=$v['prod_id']?>"><?=$v['prod_name']?></a>
                                        <?php }else{?>
                                            <?=$v['prod_name']?>
                                        <?php }?>
                                    </td>
                                    <td><?=$v['prod_no']?></td>
                                    <td><?=$v['type_name']?></td>
                                    <td><?=$v['class_name']?></td>
                                    <td><?=$v['brand_name']?></td>
                                    <td><?=$v['model']?></td>
                                    <td><?=$v['prod_num']?></td>
                                    <td><?=$v['sale_price']?></td>
                                    <td><?=$v['rate']?>%</td>
                                    <td><?=sprintf('%0.2f',$v['sale_price']*$v['prod_num'])?></td>
                                    <td><?=$v['total_amount']?></td>
                                    <td><?=substr($v['warranty_date'],0,10)?></td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    <div class="information-list">
                        <div class="col-xs-12	col-sm-12	col-md-6    col-lg-4">
                            <label  class="left-title80">其它费用：</label>
                            <div class="right-title3"><span><?=$info['another_cost_amount']?></div>
                        </div>
                    </div>
                    <div class="information-list">
                        <div class="col-xs-12	col-sm-12	col-md-6    col-lg-4">
                            <label  class="left-title80">合同总额：</label>
                            <div class="right-title3"><span><?=$info['total_amount']?></div>
                        </div>
                    </div>
                    <h5 class="block-h5">开票信息</h5>
                    <div class="information-list">
                        <div class="col-xs-12	col-sm-12	col-md-8	col-lg-8">
                            <label  class="left-title80">开票类型：</label>
                            <div class="right-title3"><span><?=$info['show_invoice_type']?></div>
                        </div>
                    </div>
                    <?php if($info['invoice_type']>1) :?>
                        <div class="information-list">
                            <div class="col-xs-12	col-sm-12	col-md-8	col-lg-8">
                                <label  class="left-title80">发票抬头：</label>
                                <div class="right-title3"><span><?=$info['invoice_title']?></div>
                            </div>
                        </div>
                        <?php if($info['invoice_type']==2) :?>
                            <div class="information-list">
                                <div class="col-xs-12	col-sm-12	col-md-8	col-lg-8">
                                    <label  class="left-title80">税号：</label>
                                    <div class="right-title3"><span><?=$info['invoice_tax_no']?></div>
                                </div>
                            </div>
                        <?php endif;?>
                    <?php endif;?>
                    <h5 class="block-h5">收货信息</h5>
                    <div class="information-list">
                        <div class="col-xs-12	col-sm-12	col-md-8	col-lg-8">
                            <label  class="left-title80">发货方式：</label>
                            <div class="right-title3"><span><?=$info['show_express_type']?></div>
                        </div>
                    </div>
                    <div class="information-list">
                        <div class="col-xs-12	col-sm-12	col-md-8	col-lg-8">
                            <label  class="left-title80">收货地址：</label>
                            <div class="right-title3"><span><?=$info['delivery_address']?></div>
                        </div>
                    </div>
                    <h5 class="block-h5">其它信息</h5>
                    <div class="information-list">
                        <div class="col-xs-12	col-sm-12	col-md-8	col-lg-8">
                            <label  class="left-title80">附件：</label>
                            <?php if($info['attachment_data']) foreach ($info['attachment_data'] as $key=>$val){?>
                                <div class="right-title3">
                                    <a href="<?php echo $val['attachment_url']?>" target="_blank"><?=$val['name']?></a>
                                </div>
                            <?php }?>
                        </div>
                    </div>
                    <div class="information-list">
                        <div class="col-xs-12	col-sm-12	col-md-8	col-lg-8">
                            <label  class="left-title80">备注：</label>
                            <div class="right-title3"><span><?=isset($info['express_description'])?$info['express_description']:'无';?></div>
                        </div>
                    </div>
                    <?php if($info['audit_status']){?>
                        <h5 class="block-h5">审批记录</h5>
                        <div class="information-list">
                            <div class="col-xs-12	col-sm-12	col-md-8	col-lg-8">
                                <label  class="left-title80">审批状态：</label>
                                <div class="right-title3"><span><?php if($info['audit_status'] == 1){echo '审核通过';}elseif ($info['audit_status'] == 2){echo '审核不通过';};?></div>
                            </div>
                            <div class="col-xs-12	col-sm-12	col-md-8	col-lg-8">
                                <label  class="left-title80">审批人：</label>
                                <div class="right-title3"><span><?=isset($info['audit_user_name'])?$info['audit_user_name']:'无';?></div>
                            </div>
                            <div class="col-xs-12	col-sm-12	col-md-8	col-lg-8">
                                <label  class="left-title80">审批时间：</label>
                                <div class="right-title3"><span><?=isset($info['audit_date'])?$info['audit_date']:'';?></div>
                            </div>
                        </div>
                    <?php }?>
                    <!-- 合同审核后的 -->
                    <?php if($info['status']>2 && $info['audit_status']==1) :?>
                        <h5 class="block-h5">出库单</h5>
                        <div class="information-list" style="height: auto">
                            <table>
                                <thead>
                                <tr>
                                    <th>出库单号</th>
                                    <th>出库状态</th>
                                    <th>出库产品数</th>
                                    <th>出库时间</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($stock_info)&&!empty($stock_info)) :?>
                                    <?php foreach($stock_info as $k => $v):?>
                                        <tr>
                                            <td><a href="/stock-out/view?id=<?=$v['id']?>"><?php echo $v['no'];?></a></td>
                                            <td><?php echo isset($v['status_desc'])?$v['status_desc']:''?></td>
                                            <td><?php echo isset($v['prod_sum'])?$v['prod_sum']:''?></td>
                                            <td><?php echo $v['audit_date']?></td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php else :?>
                                    <td colspan="4">暂无出库数据</td>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                        <h5 class="block-h5">发货单</h5>
                        <div class="information-list" style="height: auto">
                            <table>
                                <thead>
                                <tr>
                                    <th>发货单号</th>
                                    <th>发货状态</th>
                                    <th>发货类型</th>
                                    <th>发货方式</th>
                                    <th>发货产品数</th>
                                    <th>发货时间</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($send_list_info)&&!empty($send_list_info)) :?>
                                    <?php foreach($send_list_info as $k => $v):?>
                                        <tr>
                                            <td><a href="/send-out/view?id=<?=$v['id']?>"><?php echo $v['send_no'];?></a></td>
                                            <td><?php echo isset($v['send_status_desc'])?$v['send_status_desc']:''?></td>
                                            <td><?php echo isset($v['send_type_desc'])?$v['send_type_desc']:''?></td>
                                            <td><?php echo isset($v['send_method_desc'])?$v['send_method_desc']:''?></td>
                                            <td><?php echo isset($v['prod_sum'])?$v['prod_sum']:''?></td>
                                            <td><?php echo $v['create_time']?></td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php else :?>
                                    <td colspan="6">暂无发货数据</td>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                        <h5 class="block-h5">退货单</h5>
                        <div class="information-list" style="height: auto">
                            <table>
                                <thead>

                                <tr>
                                    <th>退货编号</th>
                                    <th>退货主题</th>
                                    <th>客户姓名</th>
                                    <th>电话</th>
                                    <th>退货金额</th>
                                    <!--<th>退货阶段</th>-->
                                    <th>退货状态</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($return_info)&&!empty($return_info)) :?>
                                    <?php foreach($return_info as $k => $v):?>
                                        <tr>
                                            <td><a href="/return-goods/detail?id=<?=$v['id']?>"><?php echo $v['no']?></a></td>
                                            <td><?php echo $v['subject']?></td>
                                            <td><?php echo $info['account']?></td>
                                            <td><?php echo $info['mobile']?></td>
                                            <td><?php echo $v['total_amount']?></td>
                                            <!--<td></td>-->
                                            <td><?php if($v['status']==2){
                                                    echo '审批通过';
                                                }elseif($v['status']==3){
                                                    echo '执行中';
                                                }elseif($v['status']==4){
                                                    echo '执行完毕';
                                                }
                                                ?></td>
                                            <!--<td><?php //echo DictEnum::getOne(['dict_enum_id' => $return_info['status'], 'dict_key' => 'enum_stock_in_status'])['dict_enum_value'];?></td>-->
                                        </tr>
                                    <?php endforeach;?>
                                <?php else :?>
                                    <td colspan="6">暂无退货数据</td>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                        <h5 class="block-h5">换货单</h5>
                        <div class="information-list" style="height: auto">
                            <table>
                                <thead>
                                <tr>
                                    <th>换货编号</th>
                                    <th>换货主题</th>
                                    <th>客户姓名</th>
                                    <th>电话</th>
                                    <th>换货金额</th>
                                    <!--<th>换货阶段</th>-->
                                    <th>换货状态</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($exchange_info)&&!empty($exchange_info)) :?>
                                    <?php foreach($exchange_info as $k => $v):?>
                                        <tr>
                                            <td><a href="/exchange-goods/detail?id=<?=$v['id']?>"><?php echo $v['no'];?></a></td>
                                            <td><?php echo $v['subject'];?></td>
                                            <td><?php echo $info['account'];?></td>
                                            <td><?php echo $info['mobile'];?></td>
                                            <td><?php echo $v['total_amount'];?></td>
                                            <td><?php if($v['status']==2){
                                                    echo '审批通过';
                                                }elseif($v['status']==3){
                                                    echo '执行中';
                                                }elseif($v['status']==4){
                                                    echo '执行完毕';
                                                }
                                                ?></td>
                                            <!--<td><?php //echo DictEnum::getOne(['dict_enum_id' => $return_info['status'], 'dict_key' => 'enum_exchange_status'])['dict_enum_value'];?></td>-->
                                            <!--<td></td>-->
                                        </tr>
                                    <?php endforeach;?>
                                <?php else :?>
                                    <td colspan="6">暂无换货数据</td>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                        <h5 class="block-h5">售后服务记录</h5>
                        <div class="information-list" style="height: auto">
                            <table style="width:100%">
                                <tr>
                                    <th>订单编号</th>
                                    <!--<th>服务类型</th>-->
                                    <th>服务流程</th>
                                    <th>预约服务时间</th>
                                    <th>下单时间</th>
                                    <th>状态</th>
                                </tr>
                                <?php if(!empty($order_data_info))
                                {
                                    foreach($order_data_info as $key=>$val)
                                    {
                                        $flag = true;
                                        foreach($val['work_stage_arr'] as $k=>$v) {
                                            ?>
                                            <tr>
                                                <?php if($flag == true){ ?>
                                                    <td rowspan="<?php echo count($val['work_stage_arr'] );?>">
                                                        <?php if (in_array('/order/view', $selfRoles)) { ?>
                                                            <a href="/order/view?order_no=<?php echo $val['order_no']; ?>"><?php echo $val['order_no']; ?></a>
                                                        <?php } else {
                                                            ; ?>
                                                            <?php echo $val['order_no']; ?>
                                                        <?php } ?>
                                                    </td>
                                                <?php }?>
                                                <td><?php echo $v['work_stage_desc']; ?></td>
                                                <td><?php echo isset($v['plan_time'])?common\models\Order::getPlantimeType($v['plan_time_type'],date('Y-m-d',$v['plan_time'])):'' ;?></td>
                                                <td><?php echo isset($v['create_time'])?date('Y-m-d H:i',$v['create_time']):'' ;?></td>
                                                <td><?php echo isset($statusDesc[$v['status']])?$statusDesc[$v['status']]:'' ;?></td>
                                            </tr>
                                            <?php $flag = false;
                                        }
                                    }
                                }else { ?>
                                    <tr>
                                        <td colspan="5">暂无售后服务数据</td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- 设置同意拒绝弹层 -->
<div id="noLayer" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <p style="margin:0 0 10px 0;text-align: center;">确认拒绝审批合同吗？</p>
        <div class="select-fuji">
            <textarea placeholder="请填写审批意见，非必填" class="opinionContent" style="width: 100%;height: 100px;"></textarea>
        </div>
        <div style="text-align: center;margin-top:30px;">
            <button class="btn" id="layerCancel" style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search" ></i>取消</button>
            <button class="btn btn-success" id="layerSubmit"  style="padding: 4px 12px" data-type="2"><i class="fa fa-search"></i>拒绝</button>
        </div>
    </div>
</div>
<div id="okLayer" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <p style="margin:0 0 10px 0;text-align: center;">确认同意审批合同吗？</p>
        <div class="select-fuji">
            <textarea placeholder="请填写审批意见，非必填" class="opinionContent" style="width: 100%;height: 100px;"></textarea>
        </div>
        <div style="text-align: center;margin-top:30px;">
            <button class="btn" id="layerCancel" style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search" ></i>取消</button>
            <button class="btn btn-success" id="layerSubmit"  style="padding: 4px 12px" data-type="1"><i class="fa fa-search"></i>同意</button>
        </div>
    </div>
</div>


<script>
    var layerIndex = null;
    //no = $(this).attr('data-no');
    $(".setAudit").click(function () {
        var setType = $(this).attr('data-type');
        if(setType == 'ok'){
            var statusType = 2;
            layerIndex = layer.open({
                type: 1,
                title: '提示',
                area: ['400px', '300px'],
                fixed: true,
                maxmin: false,
                content: $('#okLayer').html()
            });
        }else {
            var statusType = 3;
            layerIndex = layer.open({
                type: 1,
                title: '提示',
                area: ['400px', '300px'],
                fixed: true,
                maxmin: false,
                content: $('#noLayer').html()
            });
        }
        return false;
    });
    //
    $("body").delegate("#layerSubmit","click",function(){
        var contractNo = $('#no').attr('data-no');
        var auditStatus = $(this).attr('data-type');
        var auditSuggest = $(this).parents('div').find('.opinionContent').val();

        if($.trim(auditStatus) == ''){
            alert('操作状态错误！');
            return false;
        }
        id = $("#id").val();
        $.ajax({
            url:'/contract/check-option',
            type:'POST',
            data:{id:id,auditStatus:auditStatus,auditSuggest:auditSuggest},
            datatype:'json',
            success:function (msg) {
                data = eval('('+msg+')');
                if(data.code == 200){
                    if(layerIndex){
                        layer.close(layerIndex);
                    }
                    alert('操作成功');
                    setTimeout(function () {
                        window.location.reload(true);
                    },1000);
                }
                else {
                    alert(data.message);
                }
            }

        });
    });
    //关闭弹层
    $("body").delegate("#layerCancel","click",function(){
        if(layerIndex){
            layer.close(layerIndex);
        }
    });
    //查看配件包，物料清单
    $(".viewProductBag").click(function(){
        var prodId = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['查看物料清单', 'font-size:18px;'],
            area: ['800px', '500px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/product/materials-detail?id='+prodId,
        });
        return false;
    });
</script>

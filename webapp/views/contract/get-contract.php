<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    $this->title = '选择合同';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">

<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4{ padding-left: 0;}
    body{overflow-x: hidden;background: #fff;font-family: "微软雅黑"}
    table{border-collapse: collapse;border:1px solid #ccc;}
   .table-bordered>tbody>tr>td,.table-bordered>thead>tr>th{border-right:1px solid transparent;overflow: hidden;text-align: center;padding: 10px;font-size: 12px}
   .table-bordered tr>.border-rt-c{border-right: 1px solid #ccc;}
   .form-horizontal .form-group{margin:0;}
   /*.table-striped>tbody>tr:nth-child(odd)>td{background: #fff}*/
</style>
<body>
<div class="row popup-boss">
    <form action="/contract/get-contract" class="form-horizontal form-border" id="form">
       <div class="table-responsive" style="width: 90%;margin:15px 0 0 30px;height: 65px">
           <div class="single-search">
               <label class=" search-box-lable">合同编号：</label>
               <div  class="single-search-kuang1">
                   <input type="text" class="form-control" name="no" value="<?php echo isset($_GET['no'])?$_GET['no']:'';?>">
               </div>
           </div>

           <div class="single-search">
               <label class=" search-box-lable">合同主题：</label>
               <div  class="single-search-kuang1">
                   <input type="text" class="form-control" name="subject" value="<?php echo isset($_GET['subject'])?$_GET['subject']:'';?>">
               </div>
           </div>

           <div class="single-search">
               <label class=" search-box-lable">客户姓名：</label>
               <div  class="single-search-kuang1">
                   <input type="text" class="form-control" name="account_name" value="<?php echo isset($_GET['account_name'])?$_GET['account_name']:'';?>">
               </div>
           </div>

           <div class="single-search">
               <label class=" search-box-lable">客户电话：</label>
               <div  class="single-search-kuang1">
                   <input type="text" class="form-control" name="account_mobile" value="<?php echo isset($_GET['account_mobile'])?$_GET['account_mobile']:'';?>">
               </div>
           </div>
            <div class="search-confirm">
                <button class="btn btn-orange">
                    <i class=" "></i> 查询
                </button>
            </div>
        </div>
        <div class="table-responsive"  style="width: 90%;margin:0 auto;">
            <table class="table table-bordered table-striped table-hover" style="width:100%;">
                <thead bgcolor="#f8f8f8" style="border-collapse: collapse;">
                <tr>
                    <th></th>
                    <th>合同编号</th>
                    <th>合同主题</th>
                    <th>客户姓名</th>
                    <th class="border-rt-c">客户电话</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($data)) : ?>
                    <?php foreach ($data as $key => $val) : ?>
                        <tr>
                            <td>
                                <input type="radio" name="contract_id" class="contract_id" value="<?=$val['id']?>" class="available_cost_type" id="bh<?=$val['id']?>"/>
                                 <label for="bh<?=$val['id']?>" style="cursor: pointer;"></label>
                            </td>
                            <td><?=isset($val['no'])?$val['no']:'' ?></td>
                            <td><?=isset($val['subject'])?$val['subject']:'' ?></td>
                            <td><?=isset($val['account_name'])?$val['account_name']:'' ?></td>
                            <td class="border-rt-c"><?=isset($val['account_mobile'])?$val['account_mobile']:'' ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr><tr><td colspan="7" class="no-record">暂无数据</td></tr></tr>
                <?php endif; ?>
                </tbody>
            </table>
            <div class="col-xs-12 text-center  pagination">
                <?php echo $pageHtml;?>
            </div>
        </div>
    </form>
</div>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>
<script>
    var contractIdArray = [];
    var parentBox = parent.$('#relationContract');
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
        parentBox.find('li').each(function(i) {
           contractIdArray.push();
           $('#bh'+ $(this).find('.right-delete-a').data('id')).attr('disabled',true);
           $('#bh'+ $(this).find('.right-delete-a').data('id')).next('label').addClass('disabled-label')
        })
    };
    $(".contract_id").click(function(){
        var contractId = $(this).val();
        var html = '';

        $.ajax({
            url:'/contract/get-contract-detail?id='+contractId,
            type:'get',
            dataType:'json',
            success:function(msg){
                var data = msg.data;
                console.log(data);
                if(msg.code==200){
                  var ahtml = '<li class="line-height34"><div class="right-item-shanchu"><a href="javascript:viod(0)" class="viewContractDetail" id-data="'+data.id+'">'+data.subject+'</a><a href="javascript:" class="right-delete-a" data-id="'+data.id+'">删除</a></div></li>'
                  parentBox.html(ahtml);
                  parentBox.show();
                  parentBox.next('.get-contract').text('点击修改');
                  var index = parent.layer.getFrameIndex(window.name); //关闭当前弹出层
                   parent.layer.close(index);
                }
                // data = eval("("+msg+")");
                // if(data.success==true){
                //     parent.$("#product_hide").html(msg); //父页面接收json
                //     var index = parent.layer.getFrameIndex(window.name); //关闭当前弹出层
                //     parent.layer.close(index);
                // }else{
                //     alert(msg.message);
                // }
                

            }
        })
    })
</script>
</body>
</html>





















<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$this->title = isset($infos['id']) ? '编辑库房信息' :'添加库房';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">

<style type="text/css">
   .addinput-list{padding-left: 0}
</style>
<div class="jn-title-box no-margin-bottom">
     <span class="jn-title"><?php echo isset($infos['id']) ? '编辑库房信息' :'添加库房';?></span>
</div>
<section id="main-content">
    <div class="panel-body">
        <?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal',
                'id' => 'depot-add',
                'name' => 'form1',
                'enctype' => 'multipart/form-data',
            ]
        ]);
        ?>
        <input type="hidden" value="<?php echo isset($infos['id']) ? $infos['id'] :'';?>" id="id" name="id">
        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
            <label class="name-title-lable" >
                库房编号：
            </label>
            <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                <input type="text" value="<?php echo isset($infos['no']) ? $infos['no'] :'';?>" name="no" id="no" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" placeholder="请输入库房编号（数字或字母）" sucmsg=" " nullmsg="请填写库房编号" errormsg="请填写字母或数字" maxlength="20" class="form-control inputxt" />
                <div class="Validform_checktip Validform_wrong" id="no_msg"></div>
            </div>
            
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
            <label class="name-title-lable" >
                <i class="red-i">*</i>库房名称：
            </label>
            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                <input type="text" value="<?php echo isset($infos['name']) ? $infos['name'] :'';?>" name="name" class="form-control inputxt" datatype="zh1a" errormsg="不允许输入特殊字符" placeholder="请输入库房名称" sucmsg=" " nullmsg="请输入库房名称" maxlength="20" />
            </div>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
            <label class="name-title-lable" >
                <i class="red-i">*</i>联系人姓名：
            </label>
            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                <input type="text" value="<?php echo isset($infos['contact_name']) ? $infos['contact_name'] :'';?>" name="contact_name" class="form-control inputxt" datatype="zh1a" errormsg="不允许输入特殊字符" placeholder="请输入管理人员姓名" sucmsg=" " nullmsg="请输入联系人姓名" maxlength="20" />
            </div>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
            <label class="name-title-lable" >
                <i class="red-i">*</i>联系电话：
            </label>
            <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                <input type="text" value="<?php echo isset($infos['contact_mobile']) ? $infos['contact_mobile'] :'';?>" name="contact_mobile" id="contact_mobile" datatype="/^[1][3,4,5,6,7,8,9][0-9]{9}$/" placeholder="管理人员电话" sucmsg=" " nullmsg="请输入联系电话" errormsg="手机号格式错误" maxlength="11" class="form-control inputxt" />
            </div>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
            <label class="name-title-lable">
                <i class="red-i">*</i>联系地址：
            </label>
            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                <div  class="three-linkage" style="width: 100%;">
                    <div  class="linkage-justify-list" style="width: 30%">
                        <select class="col-md-4 col-sm-4 col-xs-4 form-control" id="province" value="<?php echo isset($infos['province'])?$infos['province']:'';?>" name="province" datatype="*" sucmsg=" " nullmsg="请选择联系地址">
                            <option value="">请选择省</option>
                            <?php foreach ($province as $key => $val){?>
                                <option value="<?php echo $val['region_id'] ?>" <?php if(isset($infos['province_id']) && $infos['province_id']==$val['region_id']){?>selected<?php };?>><?=$val['region_name'] ?></option>
                            <?php };?>
                        </select>
                    </div>
                    <div  class="linkage-justify-list" style="width: 30%">
                        <select class="form-control" id="city" value="<?php echo isset($infos['city_id'])?$infos['city_id']:'';?>" name="city" datatype="*" sucmsg=" " nullmsg="请选择联系地址">
                            <?php if (!empty($infos['city_id'])) :?>
                                <option value="<?=$infos['city_id'] ?>" selected="selected" ><?php echo $infos['city_name'];?></option>
                            <?php else :?>
                                <option value="">请选择市</option>
                            <?php endif ;?>
                        </select>
                    </div>
                    <div  class="linkage-justify-list" style="width: 30%">
                        <select class="form-control" id="district" value="<?php echo isset($infos['district'])?$infos['district']:'';?>" name="district" datatype="*" sucmsg=" " nullmsg="请选择联系地址">
                            <?php if (!empty($infos['district_id'])) :?>
                                <option value="<?php echo $infos['district_id'] ?>" selected="selected" ><?php echo $infos['district_name'] ?></option>
                            <?php else :?>
                                <option value="">请选择区</option>
                            <?php endif ;?>
                        </select>
                    </div>
                    <div class="justify_fix"></div>
                    <div class="justify_fix"></div>
                </div>
                <span class="error-span"></span>
                <div class="Validform_checktip Validform_wrong"></div>
            </div>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
            <label class="name-title-lable" >
                <i class="red-i">*</i>详细地址：
            </label>
            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                <input type="text" value="<?php echo isset($infos['address']) ? $infos['address'] :'';?>" name="address" class="form-control inputxt" datatype="*" placeholder="请输入详细地址" sucmsg=" " nullmsg="请输入详细地址" maxlength="50" />
            </div>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>
        <div  class="col-md-12"><hr></div>
        <div  class="col-md-12 information-list" style="margin-top: 0;text-align: center;">
           
            <a href="/depot/index"><button type="button"  class="btn bg-f7">取消</button></a>
            <input type="submit" id="btnSt" value="确认" class="btn btn-orange mgr20" style="margin-left: 15px">
        </div>
        <?php ActiveForm::end(); ?>
    </div>     
</section>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    //验证信息
    $('#no').blur(function() {
        var reg = /^[A-Za-z0-9]*$/;
        var str = $(this).val();
        var id  = $('#id').val();
        if(str){
            if(!reg.test(str)){
                $('#no_msg').text("请填写正确的编号，仅允许字母、数字");
                $('#no_msg').show();
                $('#no').val('');
            }else {
                $('#no_msg').hide();
            }
            Vinfos(str,id);
        }
    });
    function Vinfos(str,id){
        $.post('/depot/verify-data',{str:str,id:id},function(data) {
            var res = $.parseJSON(data);
            console.log(res);
            if(res.code != 200){
                $('#no_msg').text("此编号已被占用，请换一个");
                $('#no_msg').show();
                //alert(res.msg);
                $('#no').val('');
            }
        })
    }
    //表单验证
    $("#depot-add").Validform({
        tiptype:3,
        datatype:{
            "zh1a":/^[\u4e00-\u9fa5_a-zA-Z0-9]+$/,   //只允许数字，字母，汉字
        },
        beforeSubmit:function(curform){
            $("#btnSt").prop("disabled", true);
        },
        callback:function(data){
            /*if (data.code == 200) {
                parent.alert(data.message);
                parent.layer.close(index);
            }else{
                $.Hidemsg();
                alert(data.message);
            }*/
        }
    });

    //关闭iframe
    $('#closeIframe').click(function(){
        parent.layer.close(index);
    });
    //根据开通城市父级id获取市级数据
    $("#province").change(function() {
        $("#city").html("<option value=''>请选择市</option>");
        $("#district").html("<option value=''>请选择区</option>");
        var pid = $("#province").val();
        if(pid != ''){
            $.get("/depot/get-city", { "province_id": pid }, function (data) {
                $("#city").html(data);
            });
        }
    });
    //根据城市获取区县
    $("#city").change(function() {
        var pid = $("#city").val();
        if(pid != ''){
            $.get("/depot/get-district", { "city_id": pid }, function (data) {
                $("#district").html(data);
            })
        }
    });
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
$this->title = '库房列表';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">库房管理列表</span>
    <?php if(in_array('/depot/add',$selfRoles)):?>
        <a href="/depot/add" style="color: #fff;"><span class="btn btn-success jn-btn" style=" color:#FFF;float:right;width:auto;"  data-toggle="modal" data-target="#scrollingModal">添加</span></a>
    <?php endif;?>
</div>
<section id="main-content">
    <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead bgcolor="#455971">
                    <tr>
                        <!--<th>序号</th>-->
                        <th>库房编号</th>
                        <th>库房名称</th>
                        <th>联系人姓名</th>
                        <th>联系人电话</th>
                        <th>联系地址</th>
                        <th>创建时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if($data){?>
                        <?php foreach ($data as $key=>$val){?>
                            <tr>
                                <!--<td><?php /*echo isset($val['id']) ? $val['id'] : ''; */?></td>-->
                                <td>
                                    <?php echo $val['no']; ?>
                                </td>
                                <td>
                                    <?php echo $val['name']; ?>
                                </td>
                                <td>
                                    <?php echo $val['contact_name']; ?>
                                </td>
                                <td>
                                    <?php echo isset($val['contact_mobile']) ? $val['contact_mobile'] : ''; ?>
                                </td>
                                <td>
                                    <?php echo isset($val['full_address']) ? $val['full_address'] : ''; ?>
                                </td>
                                <td>
                                    <?php echo isset($val['create_time']) ? $val['create_time'] : ''; ?>
                                </td>
                                <td class="operation">
                                    <?php if(in_array('/depot/edit',$selfRoles)):?>
                                        <a href="/depot/edit?id=<?php echo $val['id'];?>" class="update">编辑</a>
                                    <?php endif;?>
                                    <?php if(in_array('/depot/del',$selfRoles)):?>
                                        <a href="javascript:;" onclick="delChick(<?php echo $val['id']?>)">删除</a>
                                    <?php endif;?>
                                </td>
                            </tr>
                        <?php }?>
                    <?php }else{?>
                        <tr><td colspan="11" class="no-record">还没有添加库房
                                <span>
                                    <?php if(in_array('/depot/add',$selfRoles)):?>
                                        <a href="/depot/add">立即添加</a>
                                    <?php endif;?>
                                </span>
                            </td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
        <div class="col-xs-12 text-center  pagination">
            <?php echo $pageHtml;?>
        </div>
    </div>
</section>
<script>
    //删除库房
    function delChick(id){
        $.post('/depot/del',{id:id},function(data) {
            var res = $.parseJSON(data);
            if(res.code == 20007){
                alert(res.message);
            }else {
                confirm('删除该库房将不能恢复！',function(){
                    Del(id);
                },1);
            }
        })
        //Del(id);
        /*confirm('删除该库房将不能恢复！',function(){
            Del(id);
        },1);*/
        return false;
    }
    //删除操作
    function Del(id){
        $.post('/depot/del',{id:id,is_del:1},function(data) {
            var res = $.parseJSON(data);
            alert(res.message);
            setTimeout(function(){location.reload();},2000);
            //window.location.reload();
        })
    }
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
<?php

use yii\helpers\Url;
use webapp\models\BrandQualification;

$this->title = '采购退货';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/css/return-purchase/add.css'); ?>?v=<?php echo time(); ?>">
<section id="app">
    <div class="jn-title-box no-margin-bottom">
        <span class="jn-title"><e v-if="return_id !== '' && return_id > 0">修改</e><e v-if="return_id === ''">添加</e>采购退货</span>
    </div>
    <div id="app" class="panel-body">
        <us-form label-whl="90px" item-whl="500px" name="create">
            <us-section-page title-line title="基本信息">
                <us-form-item label="退货主题：" required>
                    <us-ipt-text rule="defined?退货主题不能为空" v-model="theme"></us-ipt-text>
                </us-form-item>
                <us-form-item label="关联采购单：" type="mini">
                    <us-text-content><a :href="'/purchase/detail?id='+ purchase_id + '&t=1'" target="_blank">{{purchase_no}}</a>
                    </us-text-content>
                </us-form-item>
                <us-form-item label="申请退货日期：" required>
                    <us-ipt-datetime type="date" v-model="date" rule="defined?请选择退货日期"></us-ipt-datetime>
                </us-form-item>
                <us-form-item label="退货原因：" required>
                    <us-ipt-select v-model="cause" rule="defined?请选择退货原因">
                        <us-select-option value="1" label="产品质量"></us-select-option>
                        <us-select-option value="2" label="服务质量"></us-select-option>
                        <us-select-option value="3" label="其他"></us-select-option>
                    </us-ipt-select>
                </us-form-item>
            </us-section-page>
            <us-section-page title-line title="退货明细">
                <us-form-item whl="1100px" label-whl="0">
                    <us-table :data="source.products" :handler="source.returnProducts" ref="table">
                        <us-table-column checkbox width="50px"></us-table-column>
                        <us-table-column label="产品名称" field="prod_name"></us-table-column>
                        <us-table-column label="产品编号" field="prod_no"></us-table-column>
                        <us-table-column label="品牌" field="brand_name"></us-table-column>
                        <us-table-column label="型号" field="model"></us-table-column>
                        <us-table-column label="采购数量" field="prod_num"></us-table-column>
                        <us-table-column label="采购价" field="price"></us-table-column>
                        <us-table-column label="采购总价" field="total_amount"></us-table-column>
                        <us-table-column label="退货数量">
                            <template slot-scope="scope">
                                <us-ipt-text whl="60px" v-model="scope.data.finish_num" :rule="createValid(scope.data)"></us-ipt-text>
                            </template>
                        </us-table-column>
                        <us-table-column label="退货总价">
                            <template slot-scope="scope">
                                {{scope.data.finish_num*scope.data.price}}
                            </template>
                        </us-table-column>
                    </us-table>
                </us-form-item>
                <us-form-item label-whl="50px" label="备注：">
                    <us-ipt-textarea whl="350px" v-model="remarks" word-size="200"></us-ipt-textarea>
                </us-form-item>
                <hr class="hr us-hr">
                <div class="us-footer-button">
                    <us-button @click="back">取消</us-button>
                    <us-button type="primary" @click="onSub">提交</us-button>
                </div>
            </us-section-page>
        </us-form>
    </div>
</section>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/return-purchase/add.js?v=<?php echo $v; ?>"></script>
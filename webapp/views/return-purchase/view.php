<?php

use yii\helpers\Url;
use webapp\models\BrandQualification;

$this->title = '采购退货详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/us-icon.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/css/return-purchase/index.css'); ?>?v=<?php echo time(); ?>">
<div id="app" v-cloak>
    <section id="main-content">
        <div class="panel-body">
            <us-bus-title title="采购退货详情">
                <div style="float: right">
                    <us-button @click="auditShowEvent(2)" v-if="status == 1">拒绝</us-button>
                    <us-button type="primary" @click="auditShowEvent(1)" v-if="status == 1">同意</us-button>
                    <us-button type="primary" @click="stockOutEvent()" v-if="status==2">出库</us-button>
                    <us-button @click="showCloseEvent()" v-if="status==2">关闭</us-button>
                    <us-button @click="returnEvent()" >返回</us-button>
                </div>
            </us-bus-title>
            <us-form label-whl="90px" item-whl="320px" wrap-whl="150px">
                <us-section-page title-line title="基本信息">
                    <us-form-row>
                        <us-form-item label="退货编号：" type="mini">
                            <us-text-content>{{no}}</us-text-content>
                        </us-form-item>
                        <us-form-item label="退货主题：" type="mini">
                            <us-text-content>{{subject}}</us-text-content>
                        </us-form-item>
                        <us-form-item label="关联采购单：" type="mini">
                            <us-text-content>{{purchase_no}}</us-text-content>
                        </us-form-item>
                    </us-form-row>
                    <us-form-row>
                        <us-form-item label="申请退货日期：" type="mini">
                            <us-text-content>{{return_date}}</us-text-content>
                        </us-form-item>
                        <us-form-item label="退货原因：" type="mini">
                            <us-text-content>{{return_reason}}</us-text-content>
                        </us-form-item>
                        <us-form-item label="退货状态：" type="mini">
                            <us-text-content>{{status_desc}}</us-text-content>
                        </us-form-item>
                    </us-form-row>
                    <us-form-row>
                        <us-form-item label="申请人：" type="mini">
                            <us-text-content>{{create_user_name}}</us-text-content>
                        </us-form-item>
                        <us-form-item label="创建时间：" type="mini">
                            <us-text-content>
                                {{create_time}}
                            </us-text-content>
                        </us-form-item type="mini">
                        <us-form-item label="打回原因：" v-if="audit_status == 2 && status == 4">
                            <us-text-content>{{audit_suggest}}</us-text-content>
                        </us-form-item>
                    </us-form-row>
                </us-section-page>
                <us-section-page title-line title="退货明细">
                    <us-table :data="source.products">
                        <us-table-column label="产品名称" field="prod_name"></us-table-column>
                        <us-table-column label="产品编号" field="prod_no"></us-table-column>
                        <us-table-column label="品牌" field="brand_name"></us-table-column>
                        <us-table-column label="型号" field="model"></us-table-column>
                        <us-table-column label="采购数量" field="purchase_num"></us-table-column>
                        <us-table-column label="采购价" field="purchase_price"></us-table-column>
                        <us-table-column label="采购总价" field="purchase_total_price"></us-table-column>
                        <us-table-column label="退货数量" field="return_num"></us-table-column>
                        <us-table-column label="退货总价" field="return_total_price"></us-table-column>
                    </us-table>
                    <us-form-item label-whl="50px" label="备注：">
                        <us-text-content>{{description}}</us-text-content>
                    </us-form-item>
                </us-section-page>
                <us-section-page title-line title="出库明细" v-show="source.stock && source.stock.length">
                    <us-table :data="source.stock">
                        <us-table-column label="出库单号" field="no"></us-table-column>
                        <us-table-column label="出库状态" field="status"></us-table-column>
                        <us-table-column label="出库产品数" field="prod_num"></us-table-column>
                        <us-table-column label="申请时间" field="create_time"></us-table-column>
                        <us-table-column label="出库时间" field="audit_date"></us-table-column>
                    </us-table>
                </us-section-page>
                <us-section-page title-line title="发货明细" v-show="source.send && source.send.length">
                    <us-table :data="source.send">
                        <us-table-column label="发货单号" field="send_no"></us-table-column>
                        <us-table-column label="发货状态" field="send_status"></us-table-column>
                        <us-table-column label="出库产品数" field="prod_num"></us-table-column>
                        <us-table-column label="申请时间" field="create_time"></us-table-column>
                        <us-table-column label="发货时间" field="create_time"></us-table-column>
                    </us-table>
                </us-section-page>
            </us-form>
            <us-dialog title="提示" v-model="reasonShow" footer whl="220px,150px">
                <div style="text-align: center">
                    请输入打回原因
                    <textarea style="width: 190px;height: 90px" v-model="reason"></textarea>
                </div>
                <div slot="dialog-footer" slot-scope="handler">
                    <us-button @click="handler.close()">取消</us-button>
                    <us-button type="primary" @click="auditEvent(2,handler.close)">确定</us-button>
                </div>
            </us-dialog>
            <us-dialog title="提示" v-model="throwShow" footer whl="220px,80px">
                <div style="text-align: center">
                    确定要将此退货单信息审批通过吗?
                </div>
                <div slot="dialog-footer" slot-scope="handler">
                    <us-button @click="handler.close()">取消</us-button>
                    <us-button type="primary" @click="auditEvent(1,handler.close)">确定</us-button>
                </div>
            </us-dialog>
            <us-dialog title="确认" v-model="closeShow" footer whl="220px,80px">
                <div style="text-align: center">
                    确认要关闭此条数据吗？
                </div>
                <div slot="dialog-footer" slot-scope="handler">
                    <us-button @click="handler.close()">取消</us-button>
                    <us-button type="primary" @click="closeEvent(handler.close)">确定</us-button>
                </div>
            </us-dialog>
        </div>
    </section>
</div>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/return-purchase/view.js?v=<?php echo $v; ?>"></script>
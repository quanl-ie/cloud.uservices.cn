<?php

use yii\helpers\Url;
use webapp\models\BrandQualification;

$this->title = '采购退货列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/us-icon.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/css/return-purchase/index.css'); ?>?v=<?php echo time(); ?>">
<div id="app" v-cloak>
    <section id="main-content">
        <div class="panel-body">
            <us-adv-search title="采购退货列表" placeholder="搜索退货编号"
                           @on-input-search="inputData"
                           @on-search="searchData"
                            v-model="search.no">
                <template slot-scope="handler">
                    <us-form label-whl="70px" item-whl="260px">
                        <us-form-row>
                            <us-form-item label="退货编号：">
                                <us-ipt-text whl="140px" v-model="search.no"></us-ipt-text>
                            </us-form-item>
                            <us-form-item label="退货主题：">
                                <us-ipt-text whl="140px" v-model="search.subject"></us-ipt-text>
                            </us-form-item>
                            <us-form-item label="创建时间：" whl="320px">
                                <us-ipt-datetime section type="date" whl="200px"
                                                 v-model="search.create_time"></us-ipt-datetime>
                            </us-form-item>
                        </us-form-row>
                    </us-form>
                </template>
            </us-adv-search>
            <us-tabs @change="tabChange" :labels="source.states"></us-tabs>
            <div class="table-list-wrap">
                <us-loading v-model="state.loading"></us-loading>
                <us-table :data="dataList" dynamic-column>
                    <us-table-column label="退货编号" field="no">
                        <template slot-scope="scope">
                            <a :href="'/return-purchase/view?id=' + scope.data.id">{{scope.data.no}}</a>
                        </template>
                    </us-table-column>
                    <us-table-column label="退货主题" field="subject"></us-table-column>
                    <us-table-column label="退货数量" field="prod_num"></us-table-column>
                    <us-table-column label="创建时间" field="create_time"></us-table-column>
                    <us-table-column label="审批时间" field="audit_date" v-if="status == 2"></us-table-column>
                    <us-table-column label="打回原因" field="audit_suggest" v-if="status == 4"></us-table-column>
                    <us-table-column label="操作">
                        <template slot-scope="scope">

                                <?php if (in_array('/return-purchase/audit', $selfRoles)): ?>
                                    <a v-if="scope.data.status == 1" :href="'/return-purchase/view?id=' + scope.data.id">审批</a>
                                <?php endif; ?>
                                <?php if (in_array('/return-purchase/stock', $selfRoles)): ?>
                                    <a v-if="scope.data.status == 2" :href="'/stock-out/add?rel_type=7&rel_id=' + scope.data.id">出库</a>
                                <?php endif; ?>
                                <?php if (in_array('/return-purchase/close', $selfRoles)): ?>
                                    <a href="javascript:" @click="showCloseEvent(scope.data.id)"
                                       v-if='scope.data.status == 2'>关闭</a>
                                <?php endif; ?>
                                <?php if (in_array('/return-purchase/edit', $selfRoles)): ?>
                                    <a v-if="scope.data.status == 1 || scope.data.status==4" :href="'/return-purchase/add?return_id='+scope.data.id"
                                       :data-id='scope.data.id'>修改</a>
                                <?php endif; ?>
                                <?php if (in_array('/return-purchase/close', $selfRoles)): ?>
                                    <a href="javascript:" @click="showDeleteEvent(scope.data.id)"
                                       v-if='scope.data.status==1 || scope.data.status==4'>删除</a>
                                <?php endif; ?>
                        </template>
                    </us-table-column>
                </us-table>
                <div class="pagination-align">
                    <us-pagination
                            @change="changePage"
                            :page-no="page.pageNo"
                            :total-page="page.totalPage"
                            :total="page.total"></us-pagination>
                </div>
                <us-dialog title="确认" v-model="delStatus" footer whl="220px,80px">
                    <div style="text-align: center">
                        确认要删除此条数据吗？
                    </div>
                    <div slot="dialog-footer" slot-scope="handler">
                        <us-button @click="handler.close()">取消</us-button>
                        <us-button type="primary" @click="deleteEvent()">确定</us-button>
                    </div>
                </us-dialog>
                <us-dialog title="确认" v-model="closeStatus" footer whl="220px,80px">
                    <div style="text-align: center">
                        确认要关闭此条数据吗？
                    </div>
                    <div slot="dialog-footer" slot-scope="handler">
                        <us-button @click="handler.close()">取消</us-button>
                        <us-button type="primary" @click="closeEvent()">确定</us-button>
                    </div>
                </us-dialog>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/return-purchase/index.js?v=<?php echo $v; ?>"></script>
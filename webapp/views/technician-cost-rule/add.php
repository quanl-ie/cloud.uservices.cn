<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
$this->title = '添加技师结算规则';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style>
    .form-control[readonly]{cursor: pointer;}
    table{width: 90%;margin:0 auto;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">添加技师结算规则</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/technician-cost-rule/add" method="post" id="tech_cost-rule">
                        <div class="information-list">
                            <label class="name-title-lable" ><span class="red-i">*</span> 结算规则名称：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <input type="text" id="technician_cost_name" name="technician_cost_name" datatype="*" sucmsg=" " nullmsg="请输入结算规则名称"   placeholder="请输入结算规则名称" maxlength="20" class="form-control inputxt" />
                            </div>
                        </div>



                        <div class="information-list">
                            <label class="name-title-lable" ><span class="red-i"></span> 结算收费项目：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <p style="line-height: 34px">百分比结算以合作收费标准进行计算</p>
                                <?php if(!empty($data['costItem'])):?>
                                <?php foreach($data['costItem'] as $k=>$v){?>
                                    <div class="chargeDiv">
                                        <p><?=$v['cs_name']?></p>
                                        <ul class="chargeUl">
                                            <li style="margin-right: 20px;line-height: 30px">结算方式:</li>
                                            <li>
                                                <div class="roundRadio">
                                                    <input type="hidden" value="<?=$v['cs_id']?>"  name="cost_item[<?=$k?>][id]"/>
                                                    <!-- <input type="radio" name="" id="digne<?=$k?>"> -->
                                                    <input  type="radio"  id="cost_type<?=$k?>" value="1" name="cost_item[<?=$k?>][cost_type]" checked/>
                                                    <label for="cost_type<?=$k?>">定额结算</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="roundRadio">
                                                    <input  type="radio"  id="cost_type1<?=$k?>" value="2" name="cost_item[<?=$k?>][cost_type]"/>
                                                    <label for="cost_type1<?=$k?>">百分比结算</label>
                                                </div>
                                            </li>
                                            <li style="width: 220px">
                                                <input type="text" name="cost_item[<?=$k?>][value]"  class="form-control inputxt abc" placeholder="请输入结算金额（单位：元）" onkeyup="clearNoNum(this)"  />
                                            </li>
                                        </ul>
                                    </div>
                                <?php }?>
                                 <div class="hover-right-hint" style="top: 200px">
                                    <div class="hover-right-hintbox">
                                        <i class="lingdang"></i>
                                        <p>如不填写，则该收费项目不参与结算</p>
                                    </div>
                                </div>
                                <?php else:?>
                                    <div>
                                        <span class="no-data-zi" style="color: red">您还没有配置过收费项目</span>
                                        <span class="btn btn-success" onclick="addParentIframe()">添加收费项目</span>
                                    </div>
                                    <br />
                                <?php endif;?>
                            </div>
                        </div>

                        <div class="information-list">
                            <label class="name-title-lable" ><span class="red-i">*</span> 适用技师：</label>
                            <input type="hidden" value="" name="technician" id="technician"/>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <a href="javascript:" class="btn btn-orange jn-btn" style="color: #fff" id="service_provider">请选择</a>
                            </div>

                        </div>
                        <div id="service_provider_box" style="width: 300px;margin-left:116px;margin-top:15px">
                            <ul class="selected-Ul" style="width: 580px;overflow: hidden;">
                            </ul>
                        </div>
                        <hr>
                        <div class="information-list">
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list" >
                                <a href="/technician-cost-rule/index" class="btn ignore jn-btn" style="border:1px solid #ccc;margin-right: 20px">取消</a>
                                <button class="btn btn-orange jn-btn" id="add_data">提交</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>




    <!-- 弹窗信息 -->
    <div class="add-payService" id="add-payService" style="display: none">
        <h4 style="padding: 10px 20px;background: #F1F2F7;margin-top: 0">选择技师</h4>
            <div>
                <table>
                    <thead>
                        <tr style="background: #f8f8f8">
                            <th style="width: 60px"></th>
                            <th>技师姓名</th>
                        </tr>
                    </thead>
                    <tbody>
                   <?php foreach ($data['technician'] as $key => $val) : ?>
                        <tr>
                            <td>
                                <input type="checkbox" class="get_technician"   value="<?=$val['id']?>" id="<?=$val['id']?>" data="<?=$val['name'] ?>">
                                <label class="brand-label" for="<?=$val['id']?>"><i class="gou-i"></i></label>
                            </td>
                            <td><?=$val['name'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
        </div>
        <div class="information-list">
            <p class="xian"></p>
        </div>
        <div class="information-list">
            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list" style="margin-bottom: 80px">
                <a href="javascript:" class="btn ignore jn-btn" id="cancelPopup"  style="border:1px solid #ccc;margin-right: 20px">取消</a>
                <button class="btn btn-orange jn-btn" id="okPopup">提交</button>
            </div>
        </div>
    </div>


</section>


<script type="text/javascript">
 // 添加技师
    var layerIndex = null;
    $('#service_provider').on('click',function() {
        //alert(33);return false;
        layerIndex=layer.open({
            title:false,
            closeBtn: 0,
            type: 1,
            skin: 'yourclass', //加上边框
            area: ['520px', '340px'], //宽高
            content: $('.add-payService')
        });
    })

    //技师数据保存
    $("#okPopup").on('click',function(){
        var technician = '';
        var nameData =[];
        $.each($('.get_technician:checked'),function(i){
            var id = $(this).val();
            var name = $(this).attr('data');
            if(id){
                technician = technician+id+',';
                nameData[i]= name;
            }

        });
        if(technician<0){
            alert('请勾选技师');
            return false;
        }else{
            var selectedHtml = '';
            $.each(nameData,function(i,pro) {
               console.log(pro);
               selectedHtml+=`<li>${pro}、</li>`;
            })
            $('.selected-Ul').html(selectedHtml);
            $("#technician").val(technician);
            layer.close(layerIndex); //关闭弹窗
        }
    })
    // 取消
    $('#cancelPopup').on('click',function() {
       layer.close(layerIndex);
    })
    $("#add_data").click(function(){
        var cost_name = $("#technician_cost_name").val();
        var technician = $("#technician").val();
        if(cost_name.length<=0){
            alert('请输入规则名称');
            return false;
        }
        if(technician.length<=0){
            alert('请选择技师');
            return false;
        }
        return true;
    })
    $('input').change(function  () {
        if($(this).val()==1){
            $(this).parents('.chargeUl').find('.abc').attr('placeholder','请输入结算金额（单位：元）');
        }else{
            $(this).parents('.chargeUl').find('.abc').attr('placeholder','请输入结算百分比（单位：%）');
        }
    })

    //表单ajax提交
    // 限制两位小数
    function clearNoNum(obj){    
        if(obj.value !=''&& obj.value.substr(0,1) == '.'){  
            obj.value="";  
        }  
        obj.value = obj.value.replace(/^0*(0\.|[1-9])/, '$1');
        obj.value = obj.value.replace(/[^\d.]/g,""); 
        obj.value = obj.value.replace(/\.{2,}/g,".");     
        obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");      
        obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数       
        if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额  
            if(obj.value.substr(0,1) == '0' && obj.value.length == 2){  
                obj.value= obj.value.substr(1,obj.value.length);      
            }  
        }      
    }


     //添加
     function addParentIframe () {
         layer.open({
             type: 2,
             title: '请选择收费项目',
             area: ['700px', '450px'],
             fixed: false, //不固定
             maxmin: false,
             scrollbar: false,
             content: ['/cost-item/add', 'no'],
             end:function(){
    //                window.parent.document.location.reload();
                 location.reload();
             }
         })
     }

</script>
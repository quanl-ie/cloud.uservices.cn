<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
$this->title = '修改技师结算规则';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style>
    .form-control[readonly]{cursor: pointer;}
    table{width: 90%;margin:0 auto;}
    .selected-Ul>li{
    float: left;padding:10px 5px;
}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">修改技师结算规则</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/technician-cost-rule/edit" method="post" id="tech_cost_rule_edit">
                        <div class="information-list">
                            <input name="id" value="<?=$info['id']?>" type="hidden" />
                            <label class="name-title-lable" ><span class="red-i">*</span> 结算规则名称：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <input type="text" id="technician_cost_name" value="<?=$info['technician_cost_name']?>" name="technician_cost_name" datatype="*" sucmsg=" " nullmsg="请输入结算规则名称"   placeholder="请输入结算规则名称" maxlength="20" class="form-control inputxt" />
                            </div>
                        </div>
                        <div class="information-list">
                            <label class="name-title-lable" > 结算收费项目：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <p style="line-height: 34px">百分比结算以合作收费标准进行计算</p>
                                <?php foreach($costItem as $k=>$val){?>
                                    <div class="chargeDiv">
                                        <p><?=$val['cs_name']?></p>
                                        <ul class="chargeUl">
                                            <li style="margin-right: 20px;line-height: 30px">结算方式:</li>
                                            <li>
                                                <div class="roundRadio">
                                                    <input type="hidden" value="<?=$val['id']?>"  name="cost_item[<?=$k?>][id]"/>
                                                    <input   class="cost_type" type="radio"  id="cost_type<?=$k?>" value="1"  <?php if($val['cost_type']==1){echo 'checked';}?> name="cost_item[<?=$k?>][cost_type]" value='1'/>
                                                    <label for="cost_type<?=$k?>">定额结算</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="roundRadio">
                                                    <input  type="radio" class="cost_type"  id="cost_type1<?=$k?>" value="2" <?php if($val['cost_type']==2){echo 'checked';}?> name="cost_item[<?=$k?>][cost_type]"  value='2'/>
                                                    <label for="cost_type1<?=$k?>">百分比结算</label>
                                                </div>
                                            </li>
                                            <li style="width: 220px">
                                                <input type="text" name="cost_item[<?=$k?>][value]" value="<?php if($val['cost_type']==1){echo sprintf('%0.2f',$val['fixed_amount']);}else{echo $val['pecent'];}?>"  class="form-control inputxt switchRadio" placeholder="请输入结算金额（单位：元）"  onkeyup="clearNoNum(this)"/>
                                            </li>
                                        </ul>
                                    </div>
                                <?php }?>
                            </div>
                        </div>

                        <div class="information-list">
                            <label class="name-title-lable" ><span class="red-i">*</span> 适用技师：</label>
                            <input type="hidden" value="<?php
                            if($technician){
                                foreach($technician as $v){
                                    if($v['check_status']==1){
                                        $tech[] = $v['id'];
                                    }
                                }
                                if(isset($tech))
                                $tech = implode($tech,',');
                                echo isset($tech)?$tech:'';}?>" name="technician" id="technician" />
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <a href="javascript:" class="btn btn-orange jn-btn" style="color: #fff" id="service_provider">请选择</a>
                            </div>

                        </div>
                        <div id="service_provider_box" style="width: 300px;margin-left:116px;margin-top:15px">
                            <ul class="selected-Ul" style="width: 580px;overflow: hidden;">
                                <?php if($technician) :?>
                                <?php foreach($technician as $v) :?>
                                <?php if($v['check_status']==1) :?>
                                <li><?=$v['name']?></li>
                                <?php endif;?>
                                <?php endforeach;?>
                                <?php endif;?>
                            </ul>
                        </div>

                        <hr style="clear: both">
                        <div class="information-list">
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <a href="/technician-cost-rule/index" class="btn ignore jn-btn" style="border:1px solid #ccc;margin-right: 20px">取消</a>
                                <button class="btn btn-orange jn-btn" id="add_data">提交</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>




    <!-- 弹窗信息 -->
    <div class="add-payService" id="add-payService" style="display: none">
        <h4 style="padding: 10px 20px;background: #F1F2F7;margin-top: 0">选择技师</h4>
        <div>
            <table>
                <thead>
                <tr style="background: #f8f8f8">
                    <th style="width: 60px"></th>
                    <th>技师姓名</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($technician as $key => $val) : ?>
                    <tr>
                        <td>
                            <input type="checkbox" class="get_technician"   value="<?=$val['id']?>" <?php if($val['check_status']==1) :?>checked<?php endif;?> id="<?=$val['id']?>" data="<?=$val['name'] ?>">
                            <label class="brand-label" for="<?=$val['id']?>"><i class="gou-i"></i></label>
                        </td>
                        <td><?=$val['name'] ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="information-list">
            <p class="xian"></p>
        </div>
        <div class="information-list">
            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list" style="margin-bottom: 80px">
                <a href="javascript:" class="btn ignore jn-btn" id="cancelPopup"  style="border:1px solid #ccc;margin-right: 20px">取消</a>
                <button class="btn btn-orange jn-btn" id="okPopup">提交</button>
            </div>
        </div>
    </div>


</section>


<script type="text/javascript">

    // 添加技师
    var layerIndex = null;
    $('#service_provider').on('click',function() {
        //alert(33);return false;
        layerIndex=layer.open({
            title:false,
            closeBtn: 0,
            type: 1,
            skin: 'yourclass', //加上边框
            area: ['520px', '340px'], //宽高
            content: $('.add-payService')
        });
    })

    //技师数据保存
    $("#okPopup").on('click',function(){
        var technician = '';
        var nameData =[];
        $.each($('.get_technician:checked'),function(i){
            var id = $(this).val();
            var name = $(this).attr('data');
            if(id){
                technician = technician+id+',';
                nameData[i]= name;
            }

        });
        if(technician<0){
            alert('请勾选技师');
            return false;
        }else{
            var selectedHtml = '';
            $.each(nameData,function(i,pro) {
               console.log(pro);
               selectedHtml+=`<li>${pro}、</li>`;
            })
            $('.selected-Ul').html(selectedHtml);
            $("#technician").val(technician);
            layer.close(layerIndex); //关闭弹窗
        }
    })
    // 取消
    $('#cancelPopup').on('click',function() {
        location.href = '/technician-cost-rule/index';
    })
    $("#add_data").click(function(){
        var cost_name = $("#technician_cost_name").val();
        var technician = $("#technician").val();
        if(cost_name.length<=0){
            alert('请输入规则名称');
            return false;
        }
        if(technician.length<=0){
            alert('请选择技师');
            return false;
        }

    })
    $(".cost_type").change(function(){
        var cost_type  = $(this).val();
        if($(this).val()==1){
            $(this).parents('.chargeUl').find('.switchRadio').val('');
            $(this).parents('.chargeUl').find('.switchRadio').attr('placeholder','请输入结算金额（单位：元）');
        }else{
            $(this).parents('.chargeUl').find('.switchRadio').val('');
            $(this).parents('.chargeUl').find('.switchRadio').attr('placeholder','请输入结算百分比（单位：%）');
        }

    })
/*    $('input').change(function  () {
        $(this).parents('.chargeUl').find('.switchRadio').val('');
        if($(this).val()==1){
            $(this).parents('.chargeUl').find('.switchRadio').attr('placeholder','请输入结算金额（单位：元）');
        }else{
            $(this).parents('.chargeUl').find('.switchRadio').attr('placeholder','请输入结算百分比（单位：%）');
        }
    })*/

    //表单ajax提交
    // 限制两位小数
    function clearNoNum(obj){
        if(obj.value !=''&& obj.value.substr(0,1) == '.'){
            obj.value="";

        }

        obj.value = obj.value.replace(/^0*(0\.|[1-9])/, '$1');
        obj.value = obj.value.replace(/[^\d.]/g,"");
        obj.value = obj.value.replace(/\.{2,}/g,".");
        obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
        obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数
        if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
            if(obj.value.substr(0,1) == '0' && obj.value.length == 2){
                obj.value= obj.value.substr(1,obj.value.length);
            }
        }
    } 

</script>
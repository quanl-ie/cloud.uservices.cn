<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = '技师结算规则列表';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .pagination{text-align: center;}
    .search-box-kuang>.col-sm-6{
    padding-left: 0;
    position: relative;
    }
    .icons-calendar{
        position: absolute;
        right: 24px;top: 9px;
    }
    .popover{max-width: 500px !important;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">技师结算规则列表</span>
    <a href="/technician-cost-rule/add" style="color: #fff"> <span class="btn btn-success jn-btn" style=" color:#FFF;float:right;width:auto " id="addParentIframe"  data-toggle="modal" data-target="#scrollingModal">
      <i class="fa fa-plus icon iconfont"></i>&nbsp;添加</span></a>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/technician-cost-rule/index" class="form-horizontal form-border" id="form">
                        <div class="form-group jn-form-box">
                            <div class="form-group jn-form-box">
                                <!-- 搜索栏 -->
                                <div class="col-sm-12 no-padding-left">

                                    <div class="single-search">
                                        <label class=" search-box-lable">结算规则名称</label>
                                        <div class="single-search-kuang1">
                                            <input type="text" class="form-control" name="technician_cost_name" value="<?php echo isset($_GET['technician_cost_name'])?$_GET['technician_cost_name']:'';?>">
                                        </div>
                                    </div>
                                    <div class="single-search">
                                        <label class=" search-box-lable">技师姓名</label>
                                        <div class="single-search-kuang1">
                                            <input type="text" class="form-control" name="technician_name" value="<?php echo isset($_GET['technician_name'])?$_GET['technician_name']:'';?>">

                                        </div>
                                    </div>

                                    <div class="search-confirm"> 
                                        <button class="btn btn-success"><i class=" "></i> 搜索</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead bgcolor="#455971">
                                <tr>
                                    <th>结算规则名称</th>
                                    <th>适用技师</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($data):?>

                                    <?php foreach ($data as $key=>$val):  ?>
                                        <tr>
                                            <td><?=$val['technician_cost_name'] ? $val['technician_cost_name'] : '空'; ?></td>
                                            <td><?=isset($val['technician'])?$val['technician']:'暂未选择技师'?></td>
                                           <td class="operation">
                                               <a class="clickEdit" href="/technician-cost-rule/edit?id=<?=$val['id']?>" id-data="<?=$val['id']?>">编辑</a>

                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php else:?>
                                    <tr>
                                    <tr><td colspan="9" class="no-record">暂无数据</td></tr>
                                    </tr>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo isset($pageHtml)?$pageHtml:'';?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
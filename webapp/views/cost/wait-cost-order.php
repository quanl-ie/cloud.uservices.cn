<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    $this->title = '结算管理';
    $this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time();?>">
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">待结算订单列表</span>
</div>


<section id="main-content">
    <div class="panel-body">
        <div class="top-data-box">
            <div style="background: #4e8de3;">
                待结算总金额
                <p><?=isset($sumPrice) ? $sumPrice : 0.00?></p>
            </div>
             <div style="background: #61b7df;">
                待结算订单数
                <p><?=isset($sum) ? $sum : 0?></p>
            </div>
        </div>
        <form action="/cost/wait-cost-order" class="form-horizontal form-border" id="form">
            <!--搜索开始-->
            <div class="form-group jn-form-box">
                <div class="col-sm-12 no-padding-left">
                    <div class="single-search">
                        <label class="search-box-lable">订单编号</label>
                        <div class="single-search-kuang1">
                            <input type="text" class="form-control" name="order_no" value="<?php echo isset($_GET['order_no'])?$_GET['order_no']:'';?>">
                        </div>
                    </div>

                    <div class="single-search">
                        <label class="search-box-lable">服务类型</label>
                        <div class="single-search-kuang1">
                            <select class="form-control" name="type_id">
                                <option value="-1">请选择</option>
                                <?php if (isset($type) && !empty($type)) : ?>
                                    <?php foreach ($type as $key => $val) : ?>
                                        <option value="<?=$key?>" <?php if(isset($_GET['type_id']) && $_GET['type_id']==$key):?>selected<?php endif;?>><?=$val;?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="tow-search">
                        <label class="search-box-lable">完成服务时间</label>
                        <div class="single-search-kuang2">
                            <div  class="half-search">
                                <input type="text" id="datetimepicker-create-top" class="form-control" name="start_time" value="<?php echo isset($_GET['start_time'])?$_GET['start_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-create-end\')}'})">
                                <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-create-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-create-end\')}'})"><i class="icon2-rili"></i></span>
                                <span class="zhi"> 至</span>
                            </div>
                            <div  class="half-search">
                                <input type="text" id="datetimepicker-create-end" class="form-control" name="end_time" value="<?php echo isset($_GET['end_time'])?$_GET['end_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-create-top\')}'})" >
                                <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-create-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-create-top\')}'})"><i class="icon2-rili"></i></span>
                            </div>
                            <p class="city3_fix"></p>
                        </div>
                    </div>

                    <div class="search-confirm">
                        <button class="btn btn-success "><i class=" "></i> 搜索</button>
                    </div>
                </div>
            </div>
            <!--搜索结束-->
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead bgcolor="#2693ff">
                    <tr>
                        <th>订单编号</th>
                        <th>服务类型</th>
                        <th>服务产品</th>
                        <th>服务品牌</th>
                        <th>收费总金额</th>
                        <th>完成服务时间</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(isset($data) && !empty($data)):?>
                        <?php foreach ($data as $key=>$val):?>
                            <tr>
                                <td>
                                    <?php if(in_array('/order/view',$selfRoles)):?>
                                        <a href="/order/view?order_no=<?=isset($val['order_no']) ? $val['order_no'] : '';?>"><?=isset($val['order_no']) ? $val['order_no'] : '';?></a>
                                    <?php else :?>
                                    <?=isset($val['order_no']) ? $val['order_no'] : '';?>
                                    <?php endif; ?>
                                </td>
                                <td><?=isset($val['type_name']) ? $val['type_name'] : '';?></td>
                                <td><?=isset($val['prod_name']) ? $val['prod_name'] : '';?></td>
                                <td><?=isset($val['brand_name']) ? $val['brand_name'] : '';?></td>
                                <td><?=isset($val['pay_amount']) ? $val['pay_amount'] : '';?></td>
                                <td><?=isset($val['create_time']) ? $val['create_time'] : '';?></td>

                            </tr>
                        <?php endforeach;?>
                    <?php else:?>
                        <tr>
                        <tr><td colspan="11" class="no-record">暂无数据</td></tr>
                        </tr>
                    <?php endif;?>
                    </tbody>
                </table>
            </div>
        </form>
        <div class="col-xs-12 text-center  pagination">
            <?php echo $pageHtml;?>
        </div>
    </div>      
</section>

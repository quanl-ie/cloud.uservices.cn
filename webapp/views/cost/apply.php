<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
$this->title = '申请结算';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style>
    .form-control[readonly]{cursor: pointer;}
    .icon-rili{position: absolute;right: 20px;top: 8px}
    .zhi{position: absolute;right: -5px;top: 8px}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">申请结算</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/cost/apply" method="post" id="cost-apply">
                        <div class="information-list">
                            <label class="name-title-lable" ><span class="red-i">*</span> 商家名称：</label>
                            <div class="col-lg-6 col-md-6 col-sm-10 col-xs-10  addinput-list">
                                <select name="sj_id" id="sj_id" class="form-control">
                                    <option value=''>请选择商家</option>
                                    <?php foreach($data as $v){
                                        ?>
                                        <option value="<?=$v['id']?>"><?=$v['company']?></option>
                                    <?php }?>
                                </select>
                                <input name="company" value="" type="hidden" id="company"/>
                                <input name="is_submit" value="1" type="hidden" id="is_submit"/>
                            </div>
                        </div>
                        <div class="information-list">
                            <label class="name-title-lable" ><span class="red-i">*</span> 结算时间段：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <div class="col-lg-4 col-md-5 col-sm-5 col-xs-5" style="padding-left: 0">
                                    <input type="text" id="datetimepicker-pay-top" class="form-control" name="cost_start_time" value="<?php echo isset($_GET['plan_start_time'])?$_GET['plan_start_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})">
                                     <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})"><i class="icon2-rili"></i></span>
                                    <span class="zhi">至</span>
                                </div>
                               
                                <div class="col-lg-4 col-md-5 col-sm-5 col-xs-5">
                                    <input type="text" id="datetimepicker-pay-end" class="form-control" name="cost_end_time" value="<?php echo isset($_GET['plan_end_time'])?$_GET['plan_end_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})" >
                                    <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})"><i class="icon2-rili"></i></span>
                                </div>
                                
                                <a class="getData btn btn-orange" value="查询" id="getData">查询</a>
                                <div class="hover-right-hint">
                                    <div class="hover-right-hintbox">
                                        <i class="lingdang"></i>
                                        <p>请选择要结算的时间段，时间间隔不超过6个月</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--这里开始需要带出的数据-->
                        <div id="show_div"></div>

                        <div class="information-list" id="get-data">
                                <ul class="charge-content-ul" style="margin-top: 15px">
                                    <li>
                                        <label class="name-title-lable">合计订单数：</label>
                                        <span id="technician_count">0</span>单
                                    </li>
                                    <!-- 
                                    <li>
                                        <label class="name-title-lable">合计订单金额：</label>
                                        <span id="work_total_amount">0.00</span>元
                                    </li>
                                    -->
                                    <li>
                                        <label class="name-title-lable">预计结算金额：</label>
                                        <span id="amount_total">0.00元</span>
                                        <input type="hidden" id="checklist_ids" name="checklist_ids" value=""/>
                                    </li>
                                </ul>
                            </div>
                        </div>
                            <!--带出数据结束 -->

                        <hr>
                        <div class="information-list" style="padding-bottom: 80px">
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <a href="/cost/index" class="btn ignore jn-btn" style="border:1px solid #ccc;margin-right: 20px">取消</a>
                                <button class="btn btn-orange jn-btn">提交</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $("#sj_id").change(function(){
        var company = $(this).find("option:selected").text();
        $("#company").val(company);
    })






    $(".getData").click(function(){
        var start_time = $("#datetimepicker-pay-top").val();
        var end_time   = $("#datetimepicker-pay-end").val();
        var sj_id = $("#sj_id").val();
        var company = $("#company").val();
                

        $.ajax({
            url:'/cost/apply/',
            type:'POST',
            data:{mark:true,startTime:start_time,endTime:end_time,sj_id:sj_id,company:company},
            success:function(msg){
                var dataObj=eval("("+msg+")");//转换为json对象
                var tech_str = '';
                if(dataObj.CostItem.length == 0){
                    alert("该时间段没有结算数据，请重新选择");
                    return false;
                }
                $.each(dataObj.CostItem,function(idx,item){
                    tech_str +=   '<tr>'+
                        '<td>'+item.cost_name+'</td>'+
                        '<td>'+item.amount+'</td>'+
                        '</tr>';

                })
                str =   '<div class="information-list" id="get-data">'+
                    '<label class="name-title-lable" ><span class="red-i">*</span> 收费项目金额：</label>'+
                    '<div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">'+
                    '<div class="dts-list-xq">'+
                    '<table style="margin-top:15px;">'+
                    '<thead>'+
                    '<tr style="background: #f8f8f8">'+
                    '<th>费用类型</th>'+
                    '<th>收费金额</th>'+
                    '</tr>'+
                    '</thead>'+
                    '<tbody>'+
                    tech_str+
                    '<tr>'+
                    '</tr>'+
                    '</tbody>'+
                    '</table>'+

                    ' </div>'+
                    '</div>'+
                    ' </div>';
                $("#show_div").html(str);
                $("#technician_count").html(dataObj.orderSum.number);
                $("#work_total_amount").html(dataObj.orderSum.amount);
                $("#amount_total").html(dataObj.orderSum.amount_expect);
                $("#checklist_ids").val(dataObj.orderSum.checklist_id);
              /*  $(".check_id").click(function(){
                    var checklist_id = '';
                    var i = 0,amount = 0,money=0;
                    $.each($('.check_id:checked'),function(){
                        var id = $(this).val();
                        if(id){
                            checklist_id = checklist_id+id+',';
                            i++;
                            money = $("#"+id).html();
                            amount = amount+parseFloat(money);
                        }
                    });
                    $("#technician_count").html(i);
                    $("#work_total_amount").html(amount);
                    $("#checklist_ids").val(checklist_id);
                })*/
            }
        })
    })


    $("#cost-apply").submit(function(){
        var count = $("#technician_count").html();
        if(count ==0){
            alert('该结算单不存在结算数据,请重新选择');
            return false;
        }
        return true;

    })

</script>
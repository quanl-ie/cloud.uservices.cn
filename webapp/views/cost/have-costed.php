<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '商家结算单列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
        border-top:2px solid #2693ff;
    }
    table{
        margin-top: 20px;
        border-right:1px solid #ccc;border-bottom:1px solid #ccc;
    }
    th,td{
        height: 40px;
        text-align: center;
        border-left:1px solid #ccc;border-top:1px solid #ccc
    }

</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">商家结算单列表</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body" style="font-size: 14px;">
                    <div class="col-md-12  col-lg-12">
                        <div style="width: 100%;" class="tab-group">
                            <ul id="myTab" class="nav nav-tabs">
                                <li><a href="/cost/wait-check" >待对账</a></li>
                                <li><a href="/cost/wait-cost">待结算</a></li>
                                <li class="active"><a href="/cost/have-costed">已结算</a></li>
                            </ul>
                        </div>

                        <div id="myTabContent" class="tab-content">
                            <!--搜索开始-->
                            <div class="form-group jn-form-box">
                                <form action="/cost/have-costed" class="form-horizontal form-border" id="form">
                                    <div class="form-group jn-form-box">
                                        <!-- 搜索栏 -->
                                        <div class="col-sm-12 no-padding-left">

                                            <div class="single-search">
                                                <label class=" search-box-lable">商家名称</label>
                                                <div class="single-search-kuang1">
                                                    <input type="text" class="form-control" name="company" value="<?php echo isset($_GET['company'])?$_GET['company']:'';?>">
                                                </div>
                                            </div>


                                            <div class="search-confirm"> 
                                                <button class="btn btn-success"><i class="icon iconfont icon-sousuo2"></i> 搜索</button>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--搜索结束-->
                            <div class="tab-pane fade in active" id="customer-order">
                                <table  style="width: 100%;text-align:center;">
                                    <thead>
                                    <tr>
                                        <th>商家名称</th>
                                        <th>订单金额</th>
                                        <th>提交日期</th>
                                        <th>结算金额</th>
                                        <th>结算日期</th>
                                        <th>状态</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($data):?>
                                        <?php foreach ($data as $key=>$val):  ?>
                                            <tr>
                                                <td><?=$val['company'] ? $val['company'] : '空'; ?></td>
                                                <td><?=$val['amount'] ? sprintf('%0.2f',$val['amount']) : '空'; ?></td>
                                                <td><?=$val['create_time'] ?  date('Y-m-d H:i:s',$val['create_time']) : '空'; ?></td>
                                                <td><?=$val['amount_real'] ? sprintf('%0.2f',$val['amount_real']) : '空'; ?></td>
                                                <td><?=$val['cost_time'] ?  date('Y-m-d H:i:s',$val['cost_time']) : '空'; ?></td>
                                                <td>
                                                    <?php if($val['status']==1):?>
                                                        生成中
                                                    <?php elseif($val['status']==2):?>
                                                        待对账
                                                    <?php elseif($val['status']==3):?>
                                                        待结算
                                                    <?php elseif($val['status']==4):?>
                                                        已结算
                                                    <?php endif;?>
                                                </td>
                                                <td><a href="/cost/detail?id=<?=$val['id']?>">查看详情</a></td>

                                            </tr>
                                        <?php endforeach;?>
                                    <?php else:?>
                                        <tr>
                                        <tr><td colspan="7" class="no-record">暂无相关结算单</td></tr>
                                        </tr>
                                    <?php endif;?>
                                    </tbody>
                                </table>
                            </div>

                            <div id="customer-product" class="tab-pane fade" ></div>
                            <div id="customer-address" class="tab-pane fade" ></div>
                        </div>

                    </div>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo $pageHtml;?>
                    </div>
                </div>

            </div>

        </div>

    </div>
</section>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$this->title = '结算单详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    th{text-align: center;}
    .details-column-title{font-weight: 600;width: 120px}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">结算单详情 </span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <!-- 内容 -->
                    <?php if(!empty($data)) : ?>
                    <div id="" class="tab-content col-md-12">
                        <div class="details-column">
                            <span class="details-column-title">商家名称：</span>
                            <div class="details-column-content">
                                <?php echo $data['company'];?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">结算时间段：</span>
                            <div class="details-column-content">
                                <?php echo date("Y-m-d",$data['begin_time']).'——'.date('Y-m-d',$data['end_time']);?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">状态：</span>
                            <div class="details-column-content">
                                <?php if($data['status']==1) :?>
                                    <span class="red-i">生成中
                                    </span>
                                <?php elseif($data['status']==2):?>
                                    <span class="2693ff">
                                        待对账
                                    </span>
                                <?php elseif($data['status']==3):?>
                                    <span class="red-i">
                                        待结算
                                    </span>
                                    </span>
                                <?php elseif($data['status']==4):?>
                                    <span class="2693ff">
                                        已结算
                                    </span>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">收费项目金额：</span>
                            <div class="details-column-content">
                                <div class="dts-list-xq">
                                    <table>
                                        <thead>
                                            <tr style="background: #f8f8f8">
                                                <th>收费项目</th>
                                                <th>收费标准</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($costItem as $v)  :?>
                                            <tr>
                                                <td><?=$v['cost_name']?></td>
                                                <td><?=sprintf('%0.2f',$v['amount'])?>元</td>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">对账明细： </span>
                            <div class="details-column-content">
                                <a href="/cost/detail?id=<?=$data['id']?>&export=1" class="btn btn-orange" style="color: #fff">
                                    导出
                                </a>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">创建时间： </span>
                            <div class="details-column-content">
                                <?=date('Y-m-d H:i:s',$data['create_time'])?>
                            </div>
                        </div>

                        <?php if($data['status']==3):?>

                        <div class="details-column">
                            <span class="details-column-title">对账时间： </span>
                            <div class="details-column-content">
                                <?=date('Y-m-d H:i:s',$data['check_time'])?>
                            </div>
                        </div>
                        <?php endif;?>

                        <?php if($data['status']==4):?>

                        <div class="details-column">
                            <span class="details-column-title">对账时间： </span>
                            <div class="details-column-content">
                                <?=date('Y-m-d H:i:s',$data['check_time'])?>
                            </div>
                        </div>
                     <!--   <div class="details-column">
                            <span class="details-column-title">结算人： </span>
                            <div class="details-column-content">
                                <?/*=$data['cost_name']*/?>
                            </div>
                        </div>-->
                        <div class="details-column">
                            <span class="details-column-title">结算时间： </span>
                            <div class="details-column-content">
                                <?=date('Y-m-d H:i:s',$data['cost_time'])?>
                            </div>
                        </div>
                        <?php endif;?>
                        <!--
                        <div class="details-column">
                            <span class="details-column-title">总计金额： </span>
                            <div class="details-column-content">
                                <?=sprintf('%0.2f',$data['amount'])?>
                            </div>
                        </div>
                        -->
                        <div class="details-column">
                            <span class="details-column-title">预计结算金额： </span>
                            <div class="details-column-content">
                                <?=sprintf('%0.2f',$data['amount_expect'])?>
                            </div>
                        </div>
                        <?php if($data['status']>2) :?>

                        <div class="details-column">
                            <span class="details-column-title">实际金额： </span>
                            <div class="details-column-content">
                                <?=sprintf('%0.2f',$data['amount_real'])?>
                            </div>
                        </div>
                         <div class="details-column">
                            <span class="details-column-title">备注： </span>
                            <div class="details-column-content">
                                <!-- 备注内容 -->

                                <div>
                                    <?php if(!empty($data['remark'])){ echo $data['remark'];}else{ echo "无";}?>
                                </div>
                                <?php if($data['error_file_path']) :?>
                                    <div class="download">
                                        <div class="download-title">
                                            <i class=""></i>
                                            附件(用于提供异常订单信息):1个
                                        </div>
                                        <div class="download-list">
                                            <div  class="download-list-left" style="width: 50px">
                                                <img src="<?php echo Url::to('/images/excl.png');?>" alt="placeholder+image">
                                            </div>
                                            <div  class="download-list-right">
                                                <a href="<?=$data['error_file_path']?>">附件下载</a>
                                            </div>
                                        </div>

                                    </div>
                                <?php endif;?>
                                <!-- 下载内容 -->

        <!--                        <div class="download">
                                    <div class="download-title">
                                        <i class=""></i>
                                        附件（1个）
                                    </div>
                                    <div class="download-list">
                                        <div  class="download-list-left">
                                            <img src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" alt="placeholder+image">
                                        </div>
                                        <div  class="download-list-right">
                                            <span>异常订单列表.pdf</span>
                                            <a href="#">下载</a>
                                        </div>
                                    </div>
                                    <div class="download-list">
                                        <div  class="download-list-left">
                                            <img src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" alt="placeholder+image">
                                        </div>
                                        <div  class="download-list-right">
                                            <span>异常订单列表.pdf</span>
                                            <a href="javascript:">下载</a>
                                        </div>
                                    </div>
                                </div>-->
                                <?php endif;?>
                                <?php endif;?>

                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
    <img src="" id="enlargeImg" style="display: none">
</section>
<script>

    // 图片放大
    $('.enlarge-img').on('click',function() {
        var aa = $(this).attr('src');
        var bb = aa.substring(0,aa.indexOf('?'));
        $('#enlargeImg').attr('src',bb);
        setTimeout(function() {
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: '516px',
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#enlargeImg')
            },300);
        })

    })
    //查看技能
    $(".viewSkill").click(function(){
        var id = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['技能信息', 'font-size:18px;'],
            area: ['700px', '400px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/technician/get-skill?id='+id,
        });
        return false;
    });
    //查看实时位置
    $(".viewLocation").click(function(){
        var id = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['技师位置', 'font-size:18px;'],
            area: ['800px', '500px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/technician/view-location?id='+id,
        });
        return false;
    });
    var index = parent.layer.getFrameIndex(window.name);
    //修改技师位置
    $(".EditArea").click(function(){
        var id = $(this).attr("id-data");
        $.ajax({
            url:'/technician/verify',
            data:{id:id},
            type:'POST',
            dataType:'json',
            success:function(data){
                if (data.success == false) {
                    layer.confirm(data.message, {
                        btn: ['立即设置'] //按钮
                    }, function(){
                        location.href='/technician/edit-area?id='+id;
                    });
                }else{
                    location.href='/technician/edit-area?id='+id;
                }
            }
        });
        return false;
    });
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        setInterval('window.location.href="/cost/apply"',1500);
        <?php endif;?>
    };
</script>
<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = '结算管理';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    body,h4,h5,p,div{font-family: "微软雅黑"}
    a:hover{text-decoration: none;}
    h4{display: inline-block;margin:0;line-height: 47px}
    .col-md-12 ,.col-sm-12{padding-right: 0}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">结算管理</span>
    <!-- <span class="btn btn-orange"><i class="fa fa-plus icon-tianjiajiahaowubiankuang icon iconfont"></i>&nbsp;<a href="/cost/apply" style="color: #fff">申请结算</a></span> -->
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div style="margin-bottom: 30px">
                <div style="padding-left: 0;margin-bottom: 20px;width: 99%;height: 34px;overflow: hidden;">
                   <h4>商家结算</h4> 
                   <a href="/cost/apply" class="btn btn-orange" style="color: #fff;float: right;">申请结算</a>
                </div>
                
                <div  class="settlement-box"  style="width: 49%;display: inline-block;margin-right: 1%;">
                    <h5 class="cost-title">商家结算单</h5>
                    <p class="cost-jinge">已结算金额：<span><?=sprintf('%0.2f',$data['checkList']['haveCostMoney'])?></span></p>
                    <div style="margin-top: 20px;">
                        <div class="col-md-4 col-sm-4">
                            <a href="/cost/wait-check" class="numberbox">
                                <p style="color: #00a2ff"><?=$data['checkList']['unCheck']?></p>
                                <span>待对账</span>
                            </a>

                        </div>
                        <div class="col-md-4 col-sm-4">
                            <a href="/cost/wait-cost" class="numberbox numberbox-auto">
                                <p style="color: #ff5c5e"><?=$data['checkList']['unCost']?></p>
                                <span>待结算</span>
                            </a>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <a href="/cost/have-costed" class="numberbox numberbox-right">
                                <p style="color: #ffa94e"><?=$data['checkList']['haveCost']?></p>
                                <span>已结算</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="settlement-box" style="width: 49%;display: inline-block;">
                    <h5 class="cost-title">商家订单结算</h5>
                    <p class="cost-jinge">待结算金额：<span><?=sprintf('%0.2f',$data['checkOrderList']['unCostMoney'])?></span></p>
                    <div style="margin-top: 20px;">
                        <div class="col-md-4 col-sm-4">
                            <a  href="/cost-order/wait-cost" class="numberbox">
                                <p style="color: #01b485"><?=$data['checkOrderList']['unCost']?></p>
                                <span>待结算</span>
                            </a>
                        </div>

                        <div class="col-md-4 col-sm-4">
                            <a  href="/cost-order/have-costed" class="numberbox numberbox-right">
                                <p style="color: #ff5c5e"><?=$data['checkOrderList']['haveCost']?></p>
                                <span>已结算</span>
                            </a>
                        </div>
                    </div>
                </div>
                
            </div>
            <div><p class="xian"></p></div>
            <div style="margin-bottom: 50px;">
                <div style="padding-left: 0;margin-bottom: 20px;width: 99%;height: 34px;overflow: hidden;margin-top: 24px;">
                    <h4>技师结算</h4>
                    <a href="/technician-check-list/apply" class="btn btn-orange" style="color: #fff;float: right;">技师费用结算</a>
                </div>
                <div  class="settlement-box"   style="width: 49%;display: inline-block;margin-right: 1%;">
                    <h5 class="cost-title">技师结算单</h5>
                    <p class="cost-jinge">已结算金额：<span><?=sprintf('%0.2f',$data['techCheckList']['haveCostMoney'])?></span></p>
                    <div style="margin-top: 20px;">
                        <div class="col-md-4  col-sm-4">
                            <a  href="/technician-check-list/wait-check" class="numberbox">
                                <p style="color: #00a2ff"><?=$data['techCheckList']['unCheck']?></p>
                                <span>待对账</span>
                            </a>

                        </div>
                        <div class="col-md-4 col-sm-4">
                            <a  href="/technician-check-list/wait-cost" class="numberbox numberbox-auto">
                                <p style="color: #ff5c5e"><?=$data['techCheckList']['unCost']?></p>
                                <span>待结算</span>
                            </a>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <a  href="/technician-check-list/have-costed" class="numberbox numberbox-right">
                                <p style="color: #ffa94e"><?=$data['techCheckList']['haveCost']?></p>
                                <span>已结算</span>
                            </a>

                        </div>
                    </div>

                </div>
                <div class="settlement-box"  style="width: 49%;display: inline-block;">
                    <h5 class="cost-title">技师工单结算</h5>
                    <p class="cost-jinge">待结算金额：<span><?=sprintf('%0.2f',$data['techCheckOrderList']['unCostMoney'])?></span></p>
                    <div style="margin-top: 20px;">
                        <div class="col-md-4  col-sm-4">
                            <a href="/technician-cost-work/wait-cost" class="numberbox numberbox-right">
                                <p style="color: #01b485"><?=$data['techCheckOrderList']['unCost']?></p>
                                <span>待结算</span>
                        </div>

                        </a>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <a  href="/technician-cost-work/have-costed" class="numberbox numberbox-right">
                            <p style="color: #ff5c5e"><?=$data['techCheckOrderList']['haveCost']?></p>
                            <span>已结算</span>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<script>
    $(function(){
        fontSize ();
    })
    $(window).resize(function (){
        fontSize ();
    });
    function fontSize () {
        var whdef = 100/1920;
        var wH = window.innerHeight;
        var wW = window.innerWidth;
        var rem = wW * whdef;
        $('html').css('font-size', rem + "px");
    }
</script>
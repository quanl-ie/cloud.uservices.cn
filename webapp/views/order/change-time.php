<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '修改服务时间';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jedate.css');?>?v=<?php echo time()?>">

<!DOCTYPE html>
<html>
<head>
</head>
<body>
<style type="text/css">
    .city3{width: 45%;}
    body{background:#fff;}
    .time-equable1{margin-top: 10px;position: relative;width: }
    .addinput-list{margin-right: 30px;}
</style>
<section id="main-content">
  <div style="height: 225px;">
    <div class="information-list">
        <label class="name-title-lable" for="workorder-account_id">
           选择日期：
        </label>
        <span class="c_red"></span>
        <div class="addinput-list">
            <input type="hidden" id="order_no" value="<?=$orderNo?>">
            <span id="all_selectDate">
                <input type="text"  class="form-control  planTime" id="selectDate" name="selectDate"  placeholder="请选择服务时间" readonly>
                <i class="icon2-rili riqi1" id="rili"></i>
            </span>
            <p class=" misplacement" id='plan_selectDate_msg'>错误提示</p>
        </div>
    </div>
     <div class="information-list">
        <label class="name-title-lable" for="workorder-account_id">
           选择服务时间：
        </label>
        <span class="c_red"></span>
        <div class="addinput-list">
             <select name="plan_time_type" id="plan_time_type" class="plan_time_type form-control">
                <option value="0">请选择</option>
                 <?php foreach($setTimeType as $k=>$v) :?>
                     <option value="<?=$k?>"><?=$v?></option>
                 <?php endforeach;?>
            
            </select>
            <p class=" misplacement" id='plan_time_msg'>错误提示</p>
        </div>
         
    </div>
     <div class="information-list" id="show_setTime_box"  style="display: none">
        <label class="name-title-lable" for="workorder-account_id">
           具体时间：
        </label>
        <span class="c_red"></span>
        <div class="addinput-list">
            <div id="show_setTime"></div>
        </div>

    </div>
  </div>

    <hr>
    <div class="information-list" style="text-align: center;">
        <button class="btn bg-f7" id="closeIframe">取消</button>
        <a class="no-time-a btn btn-orange" onclick="saveTime()" href="javascript:">确定</a>
    </div>
  <!--   <p class="xian"></p>
    <div style="text-align: center;clear: both;">
        <button class="btn-a" id="closeIframe">取消</button>
        <button class="btn-a btn-orange" id="baocun">保存</button>
    </div> -->
</section>
<script src="/js/jquery-1.10.2.min.js"></script>
<?=Html::jsFile('@web/js/WdatePicker/WdatePicker.js')?>
<!-- <?=Html::jsFile('@web/js/WdatePicker/time.js')?> -->

<script src="/js/jedate.js"></script>

<script>
    $('#all_selectDate').on('click',function() {
        jeDate('#selectDate', {
            trigger:false,
            isToday:false,
            format: 'YYYY-MM-DD',
            maxDate: '2099-06-16 23:59:59',
            donefun:function(){
                var d = new Date();
                var selectDate = new Date($('#selectDate').val());
                if(selectDate.getDate() == d.getDate())
                {
                    $('.plan_time_type').find('option').each(function(){
                        if(d.getHours() >=12 && $(this).val() == 2){
                            $(this).attr('disabled','disabled');
                            $(this).css('background','#f8f8f8');
                        }
                        if(d.getHours() >=18 && $(this).val() == 3){
                            $(this).attr('disabled','disabled');
                            $(this).css('background','#f8f8f8');
                        }
                     });
                }
                else 
                {
                    $('.plan_time_type').find('option').each(function(){
                        $(this).removeAttr('disabled');
                        $(this).removeAttr('style');
                    });
                }
            }
        })
        $(".plan_time_type").val(0);
        $(".plan_time_type").attr('selected');
        $("#show_setTime").html('');
        $("#plan_time_type").attr('disabled',false);
        $("#plan_time_msg").html('');
    })


/*    $("#plan_time_type").focus(function(){
        $(this).attr('disabled',false);
    })*/
    //服务时间类型为5时候，显示具体时间下拉框
    $("#plan_time_type").change(function(){
        var id = $(this).val();
        var selectDate = $("#selectDate").val();
        if(selectDate==''){
            $("#plan_selectDate_msg").html('请选择日期');
            $("#plan_selectDate_msg").css('display','block');
            $(this).attr('disabled',true);
            return false;
        }else{
            $("#plan_selectDate_msg").css('display','none');
        }
        if(id!=0){
          $("#plan_time_msg").hide();
        }
        if(id==5){
            $.ajax({
                url:'/order/set-time',
                type:'POST',
                data:{time_type:id,date:selectDate},
                success:function (msg) {
                    $("#show_setTime").html(msg);

                }
            })
            $("#show_setTime_box").css('display','block');
        }else{
            $("#show_setTime_box").css('display','none');
        }
    })
    
    
    var index = parent.layer.getFrameIndex(window.name);

   //提交数据
   function saveTime () {
       var orderNo   = $('#order_no').val();
       var select_date              = $("#selectDate").val();//选择日期
       var plan_time_type           = $(".plan_time_type").val();//服务时间类型
       var set_time                 = $("#setTime").val();  //选择具体时间

       //验证时间
       if(select_date==null || select_date==undefined || select_date==''){
           $("#plan_selectDate_msg").text("请选择日期");
           $("#plan_selectDate_msg").show();
           return false;
       }

       //验证时间
       if(plan_time_type==0 || plan_time_type==undefined || plan_time_type==''){
           $("#plan_time_msg").text("请选择服务时间类型");
           $("#plan_time_msg").show();
           return false;
       }

       //验证时间
       if(plan_time_type==5&&(set_time==null || set_time==undefined || set_time=='')){
           $("#setTime").text("请选择具体的服务时间");
           $("#plan_time_msg").show();
           return false;
       }
       
      $.ajax({
           url:'/order/change-time',
           data:{order_no:orderNo,select_date:select_date,plan_time_type:plan_time_type,set_time:set_time},
           type:'POST',
           dataType:'json',
           success:function(data) {
               parent.layer.close(index);
               parent.layer.alert(data.message);
               //parent.reloadPage();
               window.parent.location.reload();
           }
       })
   }

    //关闭iframe
    $('#closeIframe').click(function(){
        parent.layer.close(index);
    });
</script>
</body>
</html>

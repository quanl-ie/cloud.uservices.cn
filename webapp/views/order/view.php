<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = '订单详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::cssFile('@web/webuploader/webuploader.css') ?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">

<style type="text/css">
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
        border-top: 2px solid #2693ff;
    }

    tr {
        height: 50px;
        line-height: 50px;
        border-left: 1px solid #ccc;
    }

    tr > td:nth-child(1), tr > th:nth-child(1) {
        padding-left: 20px;
        position: relative;
    }

    th, td {
        text-align: left;
        border: none;
    }

    .remarks {
        text-align: right;
    }

    .sf-tr {
        border-bottom: 1px solid #cdcdcc;
    }

    .sf-tr > td, .sf-tr > th {
        text-align: center;
        font-weight: 500;
    }

    td > a:link, td > a:visited {
        color: #264be8
    }

    .edit-product {
        display: none;
    }

    .hover-right-hint {
        top: 150px;
        right: 30px;
    }

    .edit-product-hover:hover .hover-right-hint {
        display: block;
    }

    .lianxiren {
        display: block;
    }

    .us-rate {
        display: inline-block;
        position: relative;
        padding: 3px 0;
        vertical-align: middle;
        margin-bottom: 0;
    }

    .us-rate li {
        float: left;
        min-width: 25px;
        height: 25px;
        line-height: 25px;
        margin-right: 5px;
    }

    .us-rate li .fa {
        font-size: 23px;
        color: #e9e9e9;
    }

    .us-rate-text {
        margin-left: 5px;
    }

    .us-rate-li-active i.us-rate-star {
        color: #ff860d;
    }

    .us-rate-text {
        margin-left: 5px;
        font-family: Microsoft YaHei ";
        font-size: 14px;
        font-weight: 400;
        width: auto;
    }

    .us-tag {
        font-family: "Microsoft YaHei", sans-serif;
        font-size: 12px;
        color: #333333;
        list-style: none;
        font-weight: 400;
        letter-spacing: 0;
        display: inline-block;
        position: relative;
        vertical-align: middle;
        /*min-width: 90px;*/
        height: 30px;
        line-height: 30px;
        padding: 0 8px;
        text-align: center;
        overflow: hidden;
        -ms-text-overflow: ellipsis;
        text-overflow: ellipsis;
        margin-bottom: 8px;
    }

    .us-text-desc, .us-text-desc-time {
        font-family: "Microsoft YaHei", sans-serif;
        font-size: 12px;
        color: #333333;
        list-style: none;
        font-weight: 400;
        letter-spacing: 0;
        padding-top: 10px;
    }

    .us-text-desc-time {
        color: #8a8a8a;
    }

    .us-dts-label {
        height: 25px;
        line-height: 25px;
    }

    .dts-list-evaluate {
        border: 1px solid #dddddd;
    }

    .dts-list-evaluate > div {
        padding: 10px 14px;
    }

    .dts-list-evaluate > div:first-child {
        border-bottom: 1px solid #dddddd;
        background: #fbfbfb;
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title"><?php echo isset($result['data']['subject_name']) ? $result['data']['subject_name'] : ''; ?></span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body" style="font-size: 14px;">
                    <?php if ($result['data']){ ?>
                    <div class="col-md-4 col-lg-3 view-left1" style="border-right: 1px solid #cdcdcc">
                        <div>
                            <div class="order-top">
                                <p class="order-number"><i
                                            class="icon iconfont icon-dingdan"></i><?= isset($result['data']['order_no']) ? $result['data']['order_no'] : '' ?>
                                </p>
                                <span class="ordoer-state <?php if (isset($result['data']['status']) == 4) {
                                    echo "green";
                                } elseif (isset($result['data']['status']) == 6) {
                                    echo "gray";
                                } else {
                                    echo "orange";
                                } ?>">
                                    <?php echo isset($result['data']['status_desc']) ? $result['data']['status_desc'] : '' ?>
                                </span>
                            </div>
                            <div>
                                <ul class="order-details">
                                    <li><h5 class="title-vh5" style="margin-top: 10px">客户信息</h5></li>
                                    <li>客户姓名
                                        ：<?= isset($result['data']['account_name']) ? $result['data']['account_name'] : '' ?></li>
                                    <li>客户电话
                                        ：<?= isset($result['data']['account_mobile']) ? $result['data']['account_mobile'] : '' ?></li>
                                    <li>服务地址 ：<?=
                                        $result['data']['conact_name'] . ' ' . $result['data']['conact_phone'] . ' ' . $result['data']['address_desc'] ?></li>
                                    <?php if ($result['data']['contract_no']): ?>
                                        <li>关联合同 ：<a
                                                    href="/contract/detail?id=<?php echo $result['data']['contract_id'] ?>&contract_id=<?php echo $result['data']['contract_id'] ?>"><?= $result['data']['contract_no'] ?></a>
                                        </li>
                                    <?php endif; ?>
                                    <li><h5 class="title-vh5">下单信息</h5></li>
                                    <li>下单人 ：<?php
                                        if (isset($result['data']['create_user'])) {
                                            echo $result['data']['create_user'];
                                        }
                                        ?></li>
                                    <li>所属机构
                                        ：<?= isset($result['data']['direct_company_name']) ? $result['data']['direct_company_name'] : '' ?></li>
                                    <li>下单时间
                                        ：<?= isset($result['data']['create_time']) ? $result['data']['create_time'] : '' ?></li>

                                    <li><h5 class="title-vh5">其它信息</h5></li>
                                    <li style="padding-top: 10px">
                                        <span class="o-details-span">故障图片：</span>
                                        <div class="o-details-img">
                                            <?php if (isset($result['data']['workDetail'][0]['fault_img_arr'])) foreach ($result['data']['workDetail'][0]['fault_img_arr'] as $key => $val) { ?>
                                                <?php if ($val): ?>
                                                    <div class="gz-img">
                                                        <img src="<?php echo $val . '?x-oss-process=image/resize,m_fixed,h_150,w_250'; ?>"
                                                             alt="">
                                                    </div>
                                                <?php endif; ?>
                                            <?php } ?>
                                        </div>

                                    </li>
                                    <li style="padding-top: 10px">
                                        备注：<?php echo isset($result['data']['description']) ? $result['data']['description'] : '无' ?></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8  col-lg-9  view-right1">
                        <div class="dts-top">
                            <label class="dts-title"></label>
                            <div class="dts-btnBox">
                                <?php if ((isset($result['data']['direct_company_type']) == 1 && isset($result['data']['direct_company_id']) == $directCompanyId) || $departmentId == isset($result['data']['service_department_id'])) { ?>
                                    <!-- 待指派 -->
                                    <?php if ($result['data']['status'] == 1 || $result['data']['status'] == 8): ?>
                                        <?php if (false && $result['data']['service_department_id'] != $result['data']['department_id']): ?>
                                            <a href="javascript:void(0);"
                                               data-href="<?php echo $result['data']['order_no']; ?>"
                                               class="rejectAssign">驳回</a>
                                        <?php endif; ?>
                                        <div class="edit-product-hover" style="display: inline-block;">
                                            <!--                                           <a href="/sale-order/edit?id=-->
                                            <?php //echo $result['data']['workDetail'][0]['productArr']['id'].'&order_no='.$result['data']['order_no'];?><!--">-->
                                            <!--                                               <button href="javascript:" class="btn btn-orange edit-products"  data-id="-->
                                            <?php //echo $result['data']['workDetail'][0]['productArr']['id'];?><!--">编辑产品信息</button>-->
                                            <!--                                           </a>-->
                                            <div class="hover-right-hint">
                                                <div class="hover-right-hintbox">
                                                    <i class="lingdang"></i>
                                                    <p>订单验收完成后不可修改产品信息</p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if (false && $departmentId == $result['data']['department_id']) { ?>
                                            <a href="<?php echo $result['data']['order_no']; ?>" class="cancelOrder">
                                                <button class="btn btn-orange">取消订单</button>
                                            </a>
                                        <?php } else if (false && $departmentId != $result['data']['department_id'] && $result['data']['is_reject'] == 2) { ?>
                                            <a href="javascript:void(0);"
                                               data-href="<?php echo $result['data']['order_no']; ?>"
                                               class="rejectAssign">
                                                <button class="btn btn-orange">驳回</button>
                                            </a>
                                        <?php } ?>
                                        <!-- 待服务 -->
                                    <?php elseif ($result['data']['status'] == 2 || $result['data']['status'] == 9): ?>
                                        <!-- 只有下单机构才有 取消操作 -->
                                        <?php if ($departmentId == $result['data']['department_id']): ?>
                                            <div class="edit-product-hover" style="display: inline-block;">
                                                <!--<a href="/sale-order/edit?id=<?php /*echo $result['data']['workDetail'][0]['productArr']['id'].'&order_no='.$result['data']['order_no'];*/ ?>">
                                                   <button href="javascript:" class="btn btn-orange edit-products"  data-id="<?php /*echo $result['data']['workDetail'][0]['productArr']['id'];*/ ?>">编辑产品信息</button>
                                               </a>-->
                                                <div class="hover-right-hint">
                                                    <div class="hover-right-hintbox">
                                                        <i class="lingdang"></i>
                                                        <p>订单验收完成后不可修改产品信息</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="<?php echo $result['data']['order_no']; ?>" class="cancelOrder">
                                                <!--                                               <button class="btn btn-orange">取消订单</button>-->
                                            </a>
                                            <?php if (false && $departmentId != $result['data']['department_id'] && $result['data']['is_reject'] == 2) { ?>
                                                <a href="javascript:void(0);"
                                                   data-href="<?php echo $result['data']['order_no']; ?>"
                                                   class="rejectAssign">
                                                    <button class="btn btn-orange">驳回</button>
                                                </a>
                                            <?php } ?>
                                            <!-- 待服务-->
                                        <?php endif; ?>

                                        <!-- 只有被指派的机构才有驳回操作 -->
                                        <?php if (false && $result['data']['service_department_id'] != $result['data']['department_id'] && $result['data']['assign_type'] != 1): ?>
                                            <a href="javascript:void(0);"
                                               data-href="<?php echo $result['data']['order_no']; ?>"
                                               class="rejectAssign">驳回订单</a>
                                        <?php endif; ?>
                                        <!-- 服务中 -->
                                    <?php elseif ($result['data']['status'] == 3): ?>
                                        <!--<a href="/sale-order/edit?id=<?php /*echo $result['data']['workDetail'][0]['productArr']['id'].'&order_no='.$result['data']['order_no'];*/ ?>">
                                           <button href="javascript:" class="btn btn-orange edit-products"  data-id="<?php /*echo $result['data']['workDetail'][0]['productArr']['id'];*/ ?>">编辑产品信息</button>
                                       </a>-->
                                        <div class="hover-right-hint">
                                            <div class="hover-right-hintbox">
                                                <i class="lingdang"></i>
                                                <p>订单验收完成后不可修改产品信息</p>
                                            </div>
                                        </div>
                                        <!-- 只有干活那个机构才有 -->
                                        <?php if (false && isset($result['data']['assign_type']) == 2): ?>
                                            <a href="/work/set-finish?work_no=<?php echo $result['data']['order_no']; ?>&type=2"
                                               class="update btn btn-orange" style="color: #fff;">完成服务</a>
                                        <?php endif; ?>
                                        <!-- 待验收 -->
                                    <?php elseif ($result['data']['status'] == 4 || $result['data']['status'] == 7): ?>
                                        <!--<a href="/sale-order/edit?id=<?php /*echo $result['data']['workDetail'][0]['productArr']['id'].'&order_no='.$result['data']['order_no'];*/ ?>">
                                           <button href="javascript:" class="btn btn-orange edit-products"  data-id="<?php /*echo $result['data']['workDetail'][0]['productArr']['id'];*/ ?>">编辑产品信息</button>
                                       </a>-->
                                        <div class="hover-right-hint">
                                            <div class="hover-right-hintbox">
                                                <i class="lingdang"></i>
                                                <p>订单验收完成后不可修改产品信息</p>
                                            </div>
                                        </div>
                                        <!-- 当上级验收驳回后，上级不能操作 -->
                                        <?php if (false && isset($result['data']['operation_department_id']) != $result['data']['service_department_id']): ?>
                                            <a href="javascript:void(0)"
                                               data-orderno="<?php echo $result['data']['order_no'] ?>"
                                               class="acceptancePass">
                                                <button class="btn btn-orange mgr8">验收通过</button>
                                            </a>
                                        <?php endif; ?>

                                        <!-- 只有下级确认验收后才有这个操作 -->
                                        <?php if (false && $result['data']['status'] == 4 && isset($result['data']['assign_type']) == 1): ?>
                                            <a href="javascript:void(0)"
                                               data-orderno="<?php echo $result['data']['order_no'] ?>"
                                               class="acceptanceFailure">
                                                <button class="btn btn-orange">验收驳回</button>
                                            </a>
                                        <?php endif; ?>
                                        <!--                                       <a href="/work/add?order_no=--><?php //echo $result['data']['order_no'];?><!--"><button class="btn btn-orange mgr8">预约下次上门</button></a>-->
                                        <!-- 待审核 -->
                                    <?php elseif (false && $result['data']['status'] == 10): ?>
                                        <a href="javascript:void(0);"
                                           data-href="<?php echo $result['data']['order_no']; ?>"
                                           class="adoptAudit">通过</a>
                                        <a href="javascript:void(0);"
                                           data-href="<?php echo $result['data']['order_no']; ?>"
                                           class="rejectAudit">驳回</a>
                                    <?php endif; ?>

                                <?php } ?>

                                <?php /*if($result['data']['status'] == 1){*/ ?><!--
                                   <a href="javascript:;" onclick="accept(<?php /*echo $result['data']['order_no'];*/ ?>)" class="update">
                                       <button class="btn btn-orange">确认接单</button>
                                   </a>
                                   <a href="<?php /*echo $result['data']['order_no'];*/ ?>" class="refuseOrder">
                                       <button class="btn btn-orange">拒绝此单</button>
                                   </a>
                               --><?php /*}*/ ?>
                                <!-- 小程序订单待审核显示操作项目 -->
                                <?php if (isset($result['data']['status'])) { ?>
                                    <?php if ($result['data']['status'] == 10) { ?>
                                        <a href="javascript:void(0);"
                                           data-href="<?php echo $result['data']['order_no']; ?>" class="adopt">
                                            <button class="btn btn-orange">通过</button>
                                        </a>
                                        <a href="javascript:void(0);"
                                           data-href="<?php echo $result['data']['order_no']; ?>" class="reject">
                                            <button class="btn btn-orange">驳回</button>
                                        </a>
                                    <?php } ?>
                                <?php } ?>

                                <!-- 待服务显示修改服务时间 -->
                                <?php if (isset($result['data']['status']) == 2) { ?>
                                    <!--<button class="btn btn-orange" >改派技师</button>-->
                                    <div class="edit-product-hover" style="display: inline-block;">

                                        <div class="hover-right-hint">
                                            <div class="hover-right-hintbox">
                                                <i class="lingdang"></i>
                                                <p>订单验收完成后不可修改产品信息</p>
                                            </div>
                                        </div>
                                    </div>

                                <?php } ?>
                                <!-- 待再次服务显示修改服务时间 -->
                                <?php /*if($result['data']['status'] == 1){*/ ?><!--
                                   <button class="btn btn-orange">改派技师</button>
                                   <button class="btn btn-orange changeTime" data-no="<?php /*echo $result['data']['order_no'];*/ ?>">修改服务时间</button>
                               --><?php /*}*/ ?>
                                <?php if (in_array(isset($result['data']['status']), [3, 4, 7])) { ?>
                                    <!--<div class="edit-product-hover" style="display: inline-block;">
                                       <a href="/sale-order/edit?id=<?php /*echo $result['data']['workDetail'][0]['productArr']['id'];*/ ?>">
                                           <button href="javascript:" class="btn btn-orange edit-products"  data-id="<?php /*echo $result['data']['workDetail'][0]['productArr']['id'];*/ ?>">编辑产品信息2</button>
                                       </a>                              			<div class="hover-right-hint">
		                                    <div class="hover-right-hintbox">
		                                        <i class="lingdang"></i>
		                                        <p>订单验收完成后不可修改产品信息</p>
		                                    </div>
		                                </div>   
                                  </div>-->
                                <?php } ?>
                                <?php /*if($result['data']['status'] == 4 && $result['data']['visit_status'] != 1){*/ ?><!--
                                   <button class="btn btn-orange">确认回访</button>
                               --><?php /*}*/ ?>
                                <button class="btn bg-f7" onclick="javascript:history.back(-1);">返回</button>
                                <!--                               <a href="/order/index"><button class="btn bg-f7">返回</button></a>-->
                            </div>
                        </div>
                        <p class="xian"></p>
                        <ul id="myTab" class="nav nav-tabs">
                            <li class="active"><a href="#customer-notes" data-toggle="tab">服务详情</a></li>
                            <li>
                                <a href="#order-dynamic" data-toggle="tab">
                                    订单动态
                                </a>
                            </li>
                            <li><a href="#productInformation" data-toggle="tab">产品信息</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade" id="order-dynamic">
                                <table style="width: 100%;text-align:center">
                                    <thead style="text-align: center;;background: #fff;">
                                    <tr style="border-left: 1px solid transparent">
                                        <th style="width: 150px">动态</th>
                                        <th>动态描述</th>
                                        <th style="width:200px">更新时间</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if (!empty($result['data']['process'])) { ?>
                                        <?php $i = 0;
                                        foreach ($result['data']['process'] as $k => $v): $i++; ?>
                                            <tr>
                                                <td style="float: left;">
													<span class="circular">
														<?php if ($i == 1): ?>
                                                            <i class="red-i1"></i>
                                                        <?php else: ?>
                                                            <i class="SpringGreen-i"></i>
                                                        <?php endif; ?>
													</span>
                                                    <?= $v['title'] ?>
                                                </td>
                                                <td style="line-height: 2;padding-top: 10px;"><?= $v['content'] ?></td>
                                                <td style="float: left;"><?= $v['create_time'] ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php } ?>

                                    </tbody>
                                </table>
                            </div>

                            <!-- 服务详情 -->
                            <div class="tab-pane fade in active " id="customer-notes">
                                <!-- 完成待验收和完成 -->
                                <div class="yes-complete">
                                    <div class="total-cost">
                                        <?php if (!empty($result['data']['workArr'])) foreach ($result['data']['workArr'] as $key => $val) { ?>
                                            <div class="details-list">
                                                <div class="dts-list" style="position: relative;margin-top: 0;">
                                                    <label class="dts-title">
                                                        <?php echo isset($result['data']['work_type_desc']) ? $result['data']['work_type_desc'] : '' ?>
                                                        -<?php echo isset($val['work_stage_desc']) ? $val['work_stage_desc'] : '' ?>
                                                        <?php echo isset($val['work_no']) ? $val['work_no'] : '' ?>

                                                    </label>
                                                    <div class="dts-list-xq">
                                                        <span class="ordoer-left-state <?php if ($val['status'] == 4 || $result['data']['status'] == 10 || $result['data']['status'] == 11) {
                                                            echo "green";
                                                        } elseif ($val['status'] == 6) {
                                                            echo "gray";
                                                        } else {
                                                            echo "btn-orange";
                                                        } ?>">
                                                            <?php if ($val['cancel_status'] == 2) { ?>
                                                                已取消
                                                            <?php } elseif ($result['data']['status'] == 10 && $result['data']['source'] == 4) { ?>
                                                                <!-- 小程序带审核时候，工单信息显示此状态-->
                                                                待审核
                                                            <?php } elseif ($result['data']['status'] == 11 && $result['data']['source'] == 4) { ?>
                                                                审核未通过
                                                            <?php } else { ?>
                                                                <?php echo isset($val['status_desc']) ? $val['status_desc'] : '' ?>
                                                            <?php } ?>

                                                        </span>
                                                        <?php if ($val['status'] == 4 && $val['visit_status'] != 1) { ?>
                                                            <!--<button class="ordoer-left-state btn-orange">确认回访</button>-->
                                                        <?php } ?>
                                                        <?php if ($val['status'] == 4 && isset($result['data']['status']) && $result['data']['status']== 4) { ?>
                                                            <a href="/order/edit-service-receipt?no=<?=$val['work_no']?>"><button class="btn btn-orange" style="float: right;padding: 2px 6px;font-size: 14px">编辑</button></a>
                                                        <?php } ?>

                                                        <div style="position: absolute;right: 0;top: 0;">
                                                            <!--待指派-->
                                                            <?php if (false && $val['status'] == 1 && $result['data']['status'] != 6 && $result['data']['status'] == 1) { ?>
                                                                <a href="javascript:void(0);"
                                                                   data-href="<?php echo $result['data']['order_no']; ?>"
                                                                   data-id="<?php echo isset($result['data']['sale_order_id']) ? $result['data']['sale_order_id'] : '' ?>"
                                                                   data-addressId="<?php echo $result['data']['address_id']; ?>"
                                                                   data-workType="<?php echo $result['data']['work_type']; ?>"
                                                                   class="assignFws">
                                                                    <button class="btn btn-orange">指派</button>
                                                                </a>
                                                                <?php if (in_array($val['status'], [1, 2, 6])): ?>
                                                                    <button class="btn btn-orange changeTime"
                                                                            data-no="<?php echo $result['data']['order_no']; ?>">
                                                                        修改服务时间
                                                                    </button>
                                                                <?php endif; ?>
                                                                <?php if (false && $result['data']['service_department_id'] != $result['data']['department_id']) { ?>
                                                                    <a href="javascript:void(0);"
                                                                       data-href="<?php echo $result['data']['order_no']; ?>"
                                                                       class="rejectAssign">
                                                                        <button class="btn btn-orange">驳回</button>
                                                                    </a>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            <!-- 待服务 -->
                                                            <?php if ((false && ($val['status'] == 2 && $result['data']['status'] != 6) || $result['data']['status'] == 2)) { ?>
                                                                <!--                                                             <a href="javascript:void(0);" data-href="--><?php //echo $result['data']['order_no'];?><!--" data-id="--><?php //echo isset($result['data']['sale_order_id'])?$result['data']['sale_order_id']:''?><!--" data-addressId="--><?php //echo $result['data']['address_id'];?><!--" data-workType="--><?php //echo $result['data']['work_type']; ?><!--" class="reAssignFws">-->
                                                                <!--                                                                <button class="btn btn-orange">改派</button>-->
                                                                <!--                                                            </a>-->
                                                                <?php if (in_array($val['status'], [1, 2, 6])): ?>
                                                                    <button class="btn btn-orange changeTime"
                                                                            data-no="<?php echo $result['data']['order_no']; ?>">
                                                                        修改服务时间
                                                                    </button>
                                                                <?php endif; ?>
                                                                <!-- 只有指派了技师的机构才可以开始服务 -->
                                                                <?php if (false && isset($result['data']['assign_type']) == 2) { ?>
                                                                    <a href="javascript:void(0);"
                                                                       data-href="<?php echo $result['data']['order_no']; ?>"
                                                                       class="startService">
                                                                        <button class="btn btn-orange">开始服务</button>
                                                                    </a>
                                                                <?php }; ?>

                                                            <?php } ?>
                                                            <!-- 服务中 -->
                                                            <?php if (false && $val['status'] == 3 && $result['data']['status'] != 6) { ?>
                                                                <!-- 只有干活那个机构才有 -->
                                                                <?php if (isset($result['data']['assign_type']) == 2) { ?>
                                                                    <a href="/work/set-finish?work_no=<?php echo $result['data']['order_no']; ?>&type=2"
                                                                       class="update">
                                                                        <button class="btn btn-orange">完成服务</button>
                                                                    </a>
                                                                <?php }; ?>
                                                            <?php } ?>
                                                            <div>
                                                                <?php //if($result['data']['department_id'] != $result['data']['service_department_id']){?>
                                                                <span>
                                                                        <?php if ($val['appraisal_status'] && $val['appraisal_status'] == '1' && $departmentId != $val['technician_store_id']) { ?>
                                                                            <a href="/order/add-appraisal?work_no=<?php echo $val['work_no']; ?>&department_id=<?php echo $val['technician_store_id']; ?>">
                                                                                <button class="btn btn-orange">评价本次服务</button>
                                                                            </a>
                                                                        <?php } ?>
                                                                    </span>
                                                                <?php //}?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="dts-list" style="clear:both;">
                                                    <span style="width: 70px">服务产品：</span>
                                                    <div class="dts-list-xq" style="margin-left: 70px;">
                                                        <? /*=isset($result['data']['service_content'])?$result['data']['service_content']:'';*/ ?>
                                                        <ul>
                                                            <?php if ($result['data']['sale_order_new_arr']) foreach ($result['data']['sale_order_new_arr'] as $key1 => $val1) { ?>
                                                                <?php if ($val1['prod_info']['product_type_id'] == 3) { ?>
                                                                    <li style="margin-bottom: 8px;"><a href="#"
                                                                                                       class="viewProductBag"
                                                                                                       id-data="<?php echo $val1['prod_info']['prod_id'] ?>"><?php echo $val1['prod_info']['prod_name']; ?></a>×<?php echo $val1['prod_info']['unit_num_name']; ?>
                                                                    </li>
                                                                <?php } else { ?>
                                                                    <li style="margin-bottom: 8px;"><?php if ($val1['prod_info']['brand_name']) {
                                                                            echo $val1['prod_info']['brand_name'] . '-';
                                                                        }
                                                                        echo $val1['prod_info']['prod_name'] . '×' . $val1['prod_info']['unit_num_name']; ?></li>
                                                                <?php }; ?>
                                                            <?php } ?>
                                                        </ul>
                                                        <?php if ($result['data']['sale_order_arr_num'] > 5) { ?>
                                                            <a href="" class="viewAllProduct"
                                                               id-data="<?php echo $result['data']['order_no']; ?>">查看更多</a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="dts-list" style="clear:both;">
                                                    <span style="width: 100px">预约上门时间：</span>
                                                    <div class="dts-list-xq">
                                                        <?php if (strpos($val['work_plan_time'], '1970') !== false): ?>
                                                            暂无
                                                        <?php else: ?>
                                                            <?= isset($val['work_plan_time']) ? $val['work_plan_time'] : '' ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <?php if ($val['status'] == 4 || $val['status'] == 5): ?>
                                                    <div class="dts-list">
                                                        <span style="width: 98px">服务完成时间：</span>
                                                        <div class="dts-list-xq">
                                                            <?php if ($val['finsh_service_time'] != '') {
                                                                echo date('Y-m-d H:i', $val['finsh_service_time']);
                                                            } ?>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="dts-list">
                                                    <span>技师信息:</span>
                                                    <div class="dts-list-xq">
                                                        <?php if ($val['technicianArr']): ?>
                                                            <?php foreach ($val['technicianArr'] as $jsVal) : ?>
                                                                <div class="tc-duojishi">
                                                                    <div class="touxiang">
                                                                        <img src="<?php echo $jsVal['avatar']; ?>?x-oss-process=image/resize,m_fixed,h_150,w_250">
                                                                    </div>
                                                                    <div class="lianxiren">
                                                                        <?php echo $jsVal['name'] . ($jsVal['title'] == '' ? '' : ('(' . $jsVal['title'] . ')')); ?></br>
                                                                    </div>
                                                                </div>
                                                            <?php endforeach; ?>
                                                        <?php else: ?>
                                                            暂无
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <?php if (!in_array($val['status'], [1, 2, 3])) { ?>
                                                    <div class="dts-list">
                                                        <span>服务工单：</span>
                                                        <div class="dts-list-xq">
                                                            <?php if ($val['work_img']) foreach ($val['work_img'] as $k1 => $v1) { ?>
                                                                <div class="gz-img" style="border:1px solid #ffffff;">
                                                                    <img class="enlarge-img"
                                                                         src="<?php echo $v1 . '?x-oss-process=image/resize,m_fixed,h_150,w_250'; ?>"
                                                                         alt="">
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="dts-list">
                                                        <span>现场拍照：</span>
                                                        <div class="dts-list-xq">
                                                            <?php if ($val['scene_img']) foreach ($val['scene_img'] as $k2 => $v2) { ?>
                                                                <div class="gz-img" style="border:1px solid #ffffff;">
                                                                    <img class="enlarge-img"
                                                                         src="<?php echo $v2 . '?x-oss-process=image/resize,m_fixed,h_150,w_250'; ?>"
                                                                         alt="">
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="dts-list">
                                                        <span>服务记录：</span>
                                                        <div class="dts-list-xq">
                                                            <?php if ($val['service_record'] != '') {
                                                                echo $val['service_record'];
                                                            } else {
                                                                echo '';
                                                            } ?>
                                                        </div>
                                                    </div>
                                                    <!--<div class="dts-list">
                                                        <span>产品记录：</span>
                                                        <div class="dts-list-xq">
                                                            <?php /*if($val['product_logs']) foreach ($val['product_logs'] as $k3=>$v3){*/ ?>
                                                                <p class="product-record-list">
                                                                    序列号：<?php /*echo isset($v3['sn'])?$v3['sn']:'';*/ ?>
                                                                    </br>
                                                                    设备名称：<?php /*echo isset($v3['product_name'])?$v3['product_name']:'';*/ ?>
                                                                </p>
                                                            <?php /*}*/ ?>
                                                        </div>
                                                    </div>-->
                                                    <div class="dts-list">
                                                        <span>客户签名：</span>
                                                        <div class="dts-list-xq">
                                                            <?php if ($val['signature']) foreach ($val['signature'] as $k4 => $v4) { ?>
                                                                <div class="gz-img">
                                                                    <img class="enlarge-img"
                                                                         src="<?php echo $v4 . '?x-oss-process=image/resize,m_fixed,h_150,w_250'; ?>"
                                                                         alt="">
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="dts-list">
                                                    <span>收费项目：</span>
                                                    <div class="dts-list-xq" style="padding-right: 0;">
                                                        <table>
                                                            <thead>
                                                            <tr class="sf-tr" style="background: #f8f8f8">
                                                                <th>项目名称</th>
                                                                <th>单价</th>
                                                                <th>数量</th>
                                                                <th>实际总价</th>
                                                                <th>付费方</th>
                                                                <th>凭证</th>
                                                                <th>操作</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php foreach ($val['work_cost'] as $k4 => $v4) { ?>
                                                                <tr class="sf-tr">
                                                                    <td><?php echo isset($v4['work_cost_name']) ? $v4['work_cost_name'] : ''; ?></td>
                                                                    <td><?php echo isset($v4['cost_price']) ? $v4['cost_price'] : ''; ?>
                                                                        元
                                                                    </td>
                                                                    <td><?php echo isset($v4['cost_number']) ? number_format($v4['cost_number'], 2, '.', '') : ''; ?></td>
                                                                    <td><?php echo isset($v4['cost_real_amount']) ? number_format($v4['cost_real_amount'], 2, '.', '') : ''; ?>
                                                                        元
                                                                    </td>
                                                                    <td><?php echo isset($v4['cost_payer']) ? $v4['cost_payer'] : ''; ?></td>
                                                                    <td>
                                                                        <?php if (in_array('/work/view-voucher', $selfRoles)): ?>
                                                                            <a href="<?php echo $v4['id']; ?>"
                                                                               class="view_voucher"
                                                                               data-id="<?php echo $v4['id']; ?>"
                                                                               data-dep-id="<?php echo isset($result['data']['service_department_id']) ? $result['data']['service_department_id'] : ''; ?>">立即查看</a>
                                                                        <?php endif; ?>
                                                                    </td>
                                                                    <td>
                                                                    <?php if (in_array('/work/edit-work-cost', $selfRoles) && in_array($val['status'], [1, 2, 3])): ?>
                                                                            <a href="javascript:" class="edit_work_cost"
                                                                               data-id="<?php echo $v4['id']; ?>"
                                                                               data-pay-id="<?php echo $v4['payer_id']; ?>">修改</a>
                                                                    <?php endif; ?>
                                                                    </td>

                                                                </tr>
                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                                <?php if ($val['appraisal_arr']) { ?>
                                                    <div class="dts-list">
                                                        <span class="us-dts-label">评价信息：</span>
                                                        <div class="dts-list-xq dts-list-evaluate"
                                                             style="padding-right: 0;">
                                                            <div>
                                                                <ul class="us-rate us-ul clearfix">
                                                                    <?php if ($val['appraisal_arr']['star_num']) { ?>
                                                                        <?php for ($i = 0; $i < $val['appraisal_arr']['star_num']; $i++) { ?>
                                                                            <li class="us-rate-li-active">
                                                                                <i class="fa fa-fw fa-star us-rate-star"></i>
                                                                            </li>
                                                                        <?php } ?>
                                                                        <?php if (5 - $val['appraisal_arr']['star_num'] > 0) { ?>
                                                                            <?php for ($x = 0; $x < 5 - $val['appraisal_arr']['star_num']; $x++) { ?>
                                                                                <li>
                                                                                    <i class="fa fa-fw fa-star us-rate-star"></i>
                                                                                </li>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    <?php } elseif ($val['appraisal_arr']['star_num'] == 0) { ?>
                                                                        <?php for ($x = 0; $x < 5; $x++) { ?>
                                                                            <li>
                                                                                <i class="fa fa-fw fa-star us-rate-star"></i>
                                                                            </li>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                    <li class="us-rate-text">
                                                                        <?php echo isset($val['appraisal_arr']['star_desc']) ? $val['appraisal_arr']['star_desc'] : ''; ?>
                                                                    </li>
                                                                </ul>
                                                                <div>
                                                                    <?php if ($val['appraisal_arr']['tag_arr']) foreach ($val['appraisal_arr']['tag_arr'] as $k6 => $v6) { ?>
                                                                        <span class="us-tag">[&nbsp;<?php echo $v6; ?>
                                                                            &nbsp;]</span>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="us-text-desc"><?php echo isset($val['appraisal_arr']['desc']) ? $val['appraisal_arr']['desc'] : ''; ?></div>
                                                                <div class="us-text-desc-time"><?php echo isset($val['appraisal_arr']['create_time']) ? $val['appraisal_arr']['create_time'] : ''; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                                <?php if ($val['cost_divide_arr']): ?>
                                                    <div class="dts-list">
                                                        <span>技师分成：</span>
                                                        <div class="dts-list-xq">
                                                            <form class="dts-list-xq">
                                                                <table>
                                                                    <thead>
                                                                    <tr class="sf-tr" style="background: #f8f8f8">
                                                                        <th>项目名称</th>
                                                                        <th>结算规则</th>
                                                                        <th>预结算金额</th>
                                                                        <th style="width: 100px">结算金额</th>
                                                                        <th style="width: 150px;">备注</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <?php foreach ($val['cost_divide_arr'] as $k5 => $v5) { ?>
                                                                        <tr class="divide-into-tr"
                                                                            data-id="<?php echo isset($v5['id']) ? $v5['id'] : ''; ?>">
                                                                            <td><?php echo isset($v5['cost_name']) ? $v5['cost_name'] : ''; ?></td>
                                                                            <td><?php echo isset($v5['divide_rules']) ? $v5['divide_rules'] : ''; ?></td>
                                                                            <td><?php echo isset($v5['expect_amount']) ? $v5['expect_amount'] / 10000 : ''; ?>
                                                                                元
                                                                            </td>
                                                                            <td>
                                                                                <span class="post-editing"><?php echo isset($v5['divide_amount']) ? $v5['divide_amount'] / 10000 : ''; ?>
                                                                                    元</span>
                                                                            </td>
                                                                            <td>
                                                                                <span class="post-editing"><?php echo isset($v5['comment']) ? $v5['comment'] : ''; ?></span>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            </form>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                                <div style="text-align: right;margin-top: 30px">
                                                    <?php if (in_array('/work/add-work-cost', $selfRoles) && $val['cancel_status'] != 2 && in_array($val['status'], [1, 2, 3])): ?>
                                                        <?php if (isset($result['data']['status']) && $result['data']['status'] != 6): ?>
                                                            <button class="btn btn-orange add_work_cost" data-no="">
                                                                添加收费项目
                                                            </button>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </div>
                                                <div style="display: none">
                                                    <span id="work_no_add"
                                                          data-id="<?php echo isset($val['work_no']) ? $val['work_no'] : ''; ?>"></span>
                                                    <span id="department_id"
                                                          data-id="<?php echo isset($result['data']['service_department_id']) ? $result['data']['service_department_id'] : ''; ?>"></span>
                                                </div>
                                                <div class="dts-list"></div>
                                            </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <!-- 产品信息 -->
                            <?php if (isset($result['data']['product_new_arr'])) { ?>
                                <div class="tab-pane fade" id="productInformation">
                                    <?php foreach ($result['data']['product_new_arr'] as $kk => $vv) { ?>

                                        <div class="dts-list">
                                            <div style="position: relative;">
                                                <?php if ($vv['product']['prod_id'] && $vv['product']['source'] == 2 && $result['data']['status'] == 10) { ?>
                                                    <span style="border-color: #01b485 #01b485 transparent transparent;"
                                                          class="left-jiao">
                                                            <i class="icon2-gou"></i>
                                                        </span>
                                                <?php } ?>
                                                <?php if (!$vv['product']['prod_id'] && $vv['product']['source'] == 2 && $result['data']['status'] == 10) { ?>
                                                    <span style="border-color: #eb7331 #eb7331 transparent transparent;"
                                                          class="left-jiao" title="请将该产品修改为产品库的产品"><i
                                                                class="icon2-wenhao"></i></span>
                                                <?php } ?>
                                            </div>
                                            <?php if ($vv['product']['product_type_id'] == 3) { ?>
                                                <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                                                    <label class="left-title80">产品编号：</label>
                                                    <div class="right-title3"
                                                         title="<?php echo isset($vv['product']['prod_no']) ? $vv['product']['prod_no'] : ''; ?>"><?php echo isset($vv['product']['prod_no']) ? $vv['product']['prod_no'] : ''; ?></div>
                                                </div>
                                                <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                                                    <label class="left-title80">产品名称：</label>
                                                    <div class="right-title3"
                                                         title="<?php echo isset($vv['product']['prod_name']) ? $vv['product']['prod_name'] : ''; ?>">
                                                        <?php if (isset($vv['product']['prod_name'])) { ?>
                                                            <a href="#" class="viewProductBag"
                                                               id-data="<?= $vv['product']['prod_id'] ?>"><?= $vv['product']['prod_name'] ?></a>
                                                        <?php }; ?>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                                                    <label class="left-title80">产品编号：</label>
                                                    <div class="right-title3"
                                                         title="<?php echo isset($vv['product']['prod_no']) ? $vv['product']['prod_no'] : ''; ?>"><?php echo isset($vv['product']['prod_no']) ? $vv['product']['prod_no'] : ''; ?></div>
                                                </div>
                                                <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                                                    <label class="left-title80">产品属性：</label>
                                                    <div class="right-title3"
                                                         title="<?php echo !empty($vv['product']['intention_type']) ? ($vv['product']['intention_type'] == 1 ? '客户产品' : '意向产品') : ''; ?>"><?php echo !empty($vv['product']['intention_type']) ? ($vv['product']['intention_type'] == 1 ? '客户产品' : '意向产品') : ''; ?></div>
                                                </div>
                                                <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                                                    <label class="left-title80">产品名称：</label>
                                                    <?php if (!$vv['product']['prod_id'] && $vv['product']['source'] == 2) { ?>
                                                        <!--当产品没有id，并且是小程序添加的时候显示此信息-->
                                                        <div class="right-title3"
                                                             title="<?php echo isset($vv['product']['prod_name_wechat']) ? $vv['product']['prod_name_wechat'] : ''; ?>"><?php echo isset($vv['product']['prod_name_wechat']) ? $vv['product']['prod_name_wechat'] : ''; ?></div>
                                                    <?php } else { ?>
                                                        <div class="right-title3"
                                                             title="<?php echo isset($vv['product']['prod_name']) ? $vv['product']['prod_name'] : ''; ?>"><?php echo isset($vv['product']['prod_name']) ? $vv['product']['prod_name'] : ''; ?></div>
                                                    <?php } ?>
                                                </div>
                                                <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                                                    <label class="left-title80">品牌：</label>
                                                    <div class="right-title3"
                                                         title="<?php echo isset($vv['product']['brand_name']) ? $vv['product']['brand_name'] : ''; ?>"> <?php echo isset($vv['product']['brand_name']) ? $vv['product']['brand_name'] : ''; ?></div>
                                                </div>
                                                <!--判断服务类型不是看现场的时候显示以下字段信息-->
                                                <?php if ($result['data']['work_type'] != 16) { ?>
                                                    <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                                                        <label class="left-title80">产品型号：</label>
                                                        <div class="right-title3"
                                                             title="<?php echo isset($vv['product']['model']) ? $vv['product']['model'] : ''; ?>"><?php echo isset($vv['product']['model']) ? $vv['product']['model'] : $vv['product']['prod_series']; ?></div>
                                                    </div>
                                                    <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                                                        <label class="left-title80">序列号：</label>
                                                        <div class="right-title3"
                                                             title="<?php echo isset($vv['sn']['sn']) ? $vv['sn']['sn'] : ''; ?>"><?php echo isset($vv['sn']['sn']) ? $vv['sn']['sn'] : ''; ?></div>
                                                    </div>
                                                    <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                                                        <label class="left-title80">购买日期：</label>
                                                        <div class="right-title3"
                                                             title="<?php echo !empty($vv['product']['buy_time']) ? (date('Y-m-d', $vv['product']['buy_time'])) : ''; ?>"><?php echo !empty($vv['product']['buy_time']) ? (date('Y-m-d', $vv['product']['buy_time'])) : ''; ?></div>
                                                    </div>
                                                    <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                                                        <label class="left-title80">出厂日期：</label>
                                                        <div class="right-title3"><?php echo !empty($vv['product']['produce_time']) ? (date('Y-m-d', $vv['product']['produce_time'])) : ''; ?></div>
                                                    </div>

                                                    <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                                                        <label class="left-title80">质保状态：</label>
                                                        <div class="right-title3"><?php echo isset($amount_type[$vv['product']['is_scope']]) ? $amount_type[$vv['product']['is_scope']] : ''; ?></div>
                                                    </div>
                                                    <div class="col-xs-12   col-sm-12   col-md-6    col-lg-4">
                                                        <label class="left-title80">质保期：</label>
                                                        <div class="right-title3"><?php echo isset($vv['product']['warranty']) ? $vv['product']['warranty'] : ''; ?></div>
                                                    </div>
                                                    <div class="col-xs-12   col-sm-12   col-md-12    col-lg-12">
                                                        <label class="left-title80">质保凭证：</label>
                                                        <div class="right-title3">
                                                            <?php if (isset($vv['product']['scope_img'])) foreach ($vv['product']['scope_img'] as $kkk => $vvv) { ?>
                                                                <?php if (!empty($vvv)) { ?>
                                                                    <div class="gz-img">
                                                                        <img class="enlarge-img"
                                                                             src="<?php echo $vvv . '?x-oss-process=image/resize,m_fixed,h_150,w_250" width="300px" height="200px"'; ?>">
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12   col-sm-12   col-md-12    col-lg-12">
                                                        <label class="left-title80">备注：</label>
                                                        <div class="right-title3">
                                                            <?php echo isset($vv['sn']['remark']) ? $vv['sn']['remark'] : ''; ?>
                                                        </div>
                                                    </div>
                                                    <?php if($dynaactionform && (!$vv['sn'] || !isset($vv['sn']['extend_data']) || !$vv['sn']['extend_data'])) :?>
                                                    <div class="col-xs-12   col-sm-12   col-md-12    col-lg-12">
                                                        <label class="left-title80">故障描述：</label>
                                                        <div class="right-title3">
                                                            <?php echo $result['data']['subject_name']; ?>
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                    <!-- 动态表单显示 -->
                                                    <?php if($vv['sn'] && isset($vv['sn']['extend_data']) && $vv['sn']['extend_data'] !='')  :?>
                                                       <?php foreach (json_decode($vv['sn']['extend_data'],true) as $extVal) :?>
                                                            <div class="col-xs-12   col-sm-12   col-md-12    col-lg-12">
                                                                <label class="left-title80"><?php echo $extVal['keyValue'];?>：</label>
                                                                <div class="right-title3">
                                                                    <?php echo $extVal['value']; ?>
                                                                </div>
                                                            </div>
                                                        <?php endforeach;?>
                                                    <?php endif;?>

                                                    <!--当订单为小程序下单，待审核状态时候，该产品可编辑-->
                                                    <?php if (isset($result['data']['status']) && $result['data']['status'] == 10 && $result['data']['source'] == 4) { ?>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 edit-product"
                                                             style="display: inline-block;margin-top: 20px;text-align: right;">
                                                            <a href="/sale-order/edit?id=<?php echo $vv['product']['id']; ?>">
                                                                <button href="javascript:"
                                                                        class="btn btn-orange edit-products"
                                                                        data-id="<?php echo $vv['product']['id']; ?>">
                                                                    编辑产品信息
                                                                </button>
                                                            </a>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (isset($result['data']['status']) && $result['data']['status'] == 4) { ?>
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: inline-block;margin-top: 20px;text-align: right;">
                                                            <a href="/order/edit-product-receipt?id=<?php echo $vv['product']['id']; ?>&order_no=<?php echo $result['data']['order_no']?>&snid=<?php echo isset($vv['sn']['id'])?$vv['sn']['id']:''; ?>">
                                                                <button href="javascript:" class="btn btn-orange">编辑产品信息</button>
                                                            </a>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <div style="clear: both;"></div>
                                        </div>
                                    <?php } ?>


                                    <!-- </div> -->
                                </div>
                            <?php } ?>
                        </div>

                    </div>
                </div>
                <?php } ?>
            </div>
        </div>

    </div>
</section>
<!-- 图片放大 -->
<div class="imgEnlarge-box">
    <div class="hezi">
        <div class="imgEnlarge-chlid">
            <img src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" alt="placeholder+image"
                 id="enlargeImg">
        </div>
    </div>
</div>
<!-- 取消订单弹层 -->
<div id="cancelLayer" style="display: none">
    <div class="col-md-12" style="text-align: center;padding:10px">
        <p class="col-md-12"><b>提示</b></p>
        <p class="col-md-12">请选择取消订单原因</p>
        <select class="col-md-12 form-control cancelReason">
            <option value="">请选择</option>
            <option value="客户服务内容变更">客户服务内容变更</option>
            <option value="订单内容填写有误">订单内容填写有误</option>
            <option value="其他原因">其他原因</option>
        </select>
        <div class="col-sm-12" style="text-align: center;margin-top:15px;">
            <button class="btn btn-success" id="layerSubmit"><i class="fa fa-search"></i>确定</button>
            <button class="btn layerCancel" id="layerCancel"><i class="fa fa-search"></i>取消</button>
        </div>
    </div>
</div>
<!-- 拒接弹层 -->
<!--<div id="refuseLayer" style="display: none">
    <div class="col-md-12" style="text-align: center;padding:10px">
        <p style="margin:0 0 20px 0">请选择拒绝理由</p>
        <div class="select-fuji">
            <select  class="form-control refuseReason" style="width: 90%;margin-left: 5%">
                <option value="">请选择</option>
                <option value="无可指派技师">无可指派技师</option>
                <option value="无法完成此任务">无法完成此任务</option>
                <option value="其他原因">其他原因</option>
            </select>
        </div>
        <div style="text-align: center;margin-top:30px;">
            <button class="btn btn-success" id="layerSubmit" style="padding: 4px 12px" ><i class="fa fa-search"></i>确定</button>
            <button class="btn" id="layerCancel" style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search"></i>取消</button>
        </div>
    </div>
</div>-->
<!-- 待验收 验证驳回 -->
<div id="acceptanceFailureLayer" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <p style="margin:0 0 10px 0;text-align: left;">请输入驳回原因</p>
        <div class="select-fuji">
            <textarea placeholder="" id="remark" style="width: 100%;height:80px;"></textarea>
        </div>
        <div style="margin-top:30px;text-align:center;">
            <button class="btn btn-success" id="acceptanceFailureSubmit" style="padding: 4px 12px"><i
                        class="fa fa-search"></i>确定
            </button>
            <button class="btn" id="layerCancel" style="margin-left: 20px;padding: 4px 12px"><i
                        class="fa fa-search"></i>取消
            </button>
        </div>
    </div>
</div>
<!-- 不通过弹层 -->
<div id="rejected" style="display: none">
    <div class="col-md-12" style="text-align: center;padding:10px">
        <p class="col-md-12"><b></b></p>
        <p style="margin-bottom: 15px">请选择驳回原因</p>
        <select class="form-control cancelReason" style="width: 90%;margin-left: 5%">
            <option value="">请选择</option>
            <option value="非本公司产品">非本公司产品</option>
        </select>
        <div class="col-sm-12" style="text-align: center;margin-top:30px;">
            <button class="btn btn-success" id="layerSubmits" style="padding: 4px 12px"><i class="fa fa-search"></i>确定
            </button>
            <button class="btn layerCancel" style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search"></i>取消
            </button>
        </div>
    </div>
</div>
<!-- 指派驳回 -->
<div id="rejectAssignLayer" style="display: none">
    <div class="col-md-12" style="text-align: center;padding:10px">
        <p class="col-md-12"><b>提示</b></p>
        <p class="col-md-12">请选择驳回订单原因</p>
        <select class="col-md-12 form-control rejectReason">
            <option selected="" value="">请选择</option>
            <option value="非本公司产品">非本公司产品</option>
            <option value="无技师可指派">无技师可指派</option>
            <option value="客户临时改约">客户临时改约</option>
            <option value="不具备进场条件">不具备进场条件</option>
            <option value="其他原因">其他原因</option>
        </select>
        <div class="col-sm-12" style="text-align: center;margin-top:15px;">
            <button class="btn btn-success" id="rejectAssignSubmit"><i class="fa fa-search"></i>确定</button>
            <button class="btn layerCancel"><i class="fa fa-search"></i>取消</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    ////提供给弹框调用
    function showAlert( message ) {
        alert( message, function () {
            document.location.reload( true );
        } );
    }

    window.onload = function () {
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert( '<?php echo Yii::$app->session->getFlash('message'); ?>' );
        window.parent.document.location.reload();
        <?php endif;?>
        $( '.view-left1' ).height() < $( '.view-right1' ).height() ? $( '.view-left1' ).css( 'css', $( '.view-right1' ).height() + 50 + 'px' ) : '';
        var layerIndex = null;
        var orderNo = '';
        $( ".reject" ).click( function () {
            layerIndex = layer.open( {
                type    : 1,
                title   : '提示',
                area    : ['300px', '220px'],
                fixed   : true,
                maxmin  : false,
                content : $( '#rejected' ).html()
            } );
            orderNo = $( this ).data( 'href' );
            return false;
        } );
        //关闭弹层
        $( "body" ).delegate( ".layerCancel", "click", function () {
            if ( layerIndex ) {
                layer.close( layerIndex );
            }
        } );
        //提交不通过订单
        $( "body" ).delegate( "#layerSubmits", "click", function () {
            var cancelReason = $( this ).parents( 'div' ).find( '.cancelReason' ).find( 'option:selected' ).val();
            if ( $.trim( orderNo ) == '' ) {
                alert( '订单号不能为空' );
                return false;
            }
            if ( $.trim( cancelReason ) == '' ) {
                alert( '请选择驳回原因' );
                return false;
            }
            $.getJSON( '/order/not-through', { 'order_no' : orderNo, 'reason' : cancelReason }, function ( json ) {
                if ( json.success == 1 ) {
                    if ( layerIndex ) {
                        layer.close( layerIndex );
                    }
                    alert( '操作成功', function () {
                        window.location.reload( true );
                    } );
                }
                else {
                    alert( json.message );
                }
            } );
        } );

        //提示
        $( ".tip" ).popover();
    }
    // 点击编辑
    $( '#edit' ).on( 'click', function () {
        $( '.post-editing' ).hide();
        $( '.edit-ipt' ).show();
        $( '.editors' ).show();
        $( this ).hide();
    } )
    $( '#edit-cancel' ).on( 'click', function () {
        $( '.post-editing' ).show();
        $( '.edit-ipt' ).hide();
        $( '.editors' ).hide();
        $( '#edit' ).show();
    } )
    //查看凭证详情
    $( ".view_voucher" ).click( function () {
        var voucher_id = $( this ).attr( 'data-id' );
        var dep_id = $( this ).attr( 'data-dep-id' );
        layer.open( {
            type      : 2,
            title     : "收费项目凭证",
            area      : ['700px', '450px'],
            fixed     : false, //不固定
            maxmin    : false,
            scrollbar : false,
            content   : ['/work/view-voucher?id=' + voucher_id + '&department_id=' + dep_id, 'no'],
            /*end:function(){
                location.reload();
            }*/
        } );
        return false;
    } );
    //添加收费项目
    $( ".add_work_cost" ).click( function () {
        var workNO = $( '#work_no_add' ).attr( 'data-id' );       //工单号码
        var departmentId = $( '#department_id' ).attr( 'data-id' );     //订单来源机构id

        if ( workNO === null || workNO === undefined || workNO == '' ) {
            alert( '数据获取失败，请联系管理员' );
            return false;
        }
        if ( departmentId === null || departmentId === undefined || departmentId == '' ) {
            alert( '数据获取失败，请联系管理员' );
            return false;
        }

        window.location.href = '/work/add-work-cost?work_no=' + workNO + '&department_id=' + departmentId;
        //window.location.href = '/work/add-work-cost?work_no='+workNO+'&type='+type+'&type_id='+type_id+'&standard_id='+standard_id+'&technician_id='+technician_id;

        /*layer.open({
            type: 2,
            title: '添加收费项目',
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/work/add-work-cost?work_no='+workNO+'&department_id='+departmentId, 'no'],
            /!*end:function(){
                location.reload();
            }*!/
        });*/
        return false;
    } );
    //编辑收费项目
    $( ".edit_work_cost" ).click( function () {
        var workNO = $( '#work_no_add' ).attr( 'data-id' );       //工单号码
        var work_cost_id = $( this ).attr( 'data-id' );     //修改时候此条收费标准id
        var cost_id = $( this ).attr( 'data-id' );     //修改时候此条收费标准id
        var tech_id = $( '#technician_id' ).attr( 'data-id' );
        var departmentId = $( '#department_id' ).attr( 'data-id' );     //订单来源机构id
        var payerId = $( this ).attr( 'data-pay-id' );     //订单来源机构id
        //window.location.href = '/work/edit-work-cost?work_no='+workNO+'&type='+type+'&type_id='+type_id+'&standard_id='+standard_id+'&cost_id='+cost_id+'&technician_id='+tech_id+'&work_cost_id='+work_cost_id;
        window.location.href = '/work/edit-work-cost?work_no=' + workNO + '&department_id=' + departmentId + '&cost_id=' + cost_id + '&technician_id=' + tech_id + '&work_cost_id=' + work_cost_id + '&payer_id=' + payerId;
        /*layer.open({
            type: 2,
            title: '添加收费项目',
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/work/add-work-cost?work_no='+workNO, 'no'],
            /!*end:function(){
                location.reload();
            }*!/
        });*/
        return false;
    } );
    // 切换产品信息
    $( '#myTab' ).find( 'li' ).on( 'click', function () {
        $( this ).find( 'a' ).text() == '产品信息' ? $( '.edit-product' ).show() : $( '.edit-product' ).hide();

        setTimeout( function () {
            console.log( $( '.view-left1' ).height() );
            console.log( $( '.view-right1' ).height() );
            $( '.view-left1' ).height() < $( '.view-right1' ).height() ? $( '.view-left1' ).css( 'height', $( '.view-right1' ).height() + 50 + 'px' ) : '';
        }, 200 );

    } )

    //拒绝接单
    /*$(".refuseOrder").click(function () {
        layerIndex = layer.open({
            type: 1,
            title: '提示',
            area: ['300px', '220px'],
            fixed: false,
            maxmin: false,
            content: $('#refuseLayer').html()
        });
        orderNo = $(this).attr('href');
        return false;
    });*/
    //取消订单
    $( ".cancelOrder" ).click( function () {
        layerIndex = layer.open( {
            type    : 1,
            title   : '提示',
            area    : ['300px', '220px'],
            fixed   : true,
            maxmin  : false,
            content : $( '#cancelLayer' ).html()
        } );
        orderNo = $( this ).attr( 'href' );
        //提交取消订单
        $( "body" ).delegate( "#layerSubmit", "click", function () {
            var cancelReason = $( this ).parents( 'div' ).find( '.cancelReason' ).find( 'option:selected' ).val();
            if ( $.trim( orderNo ) == '' ) {
                alert( '订单号不能为空' );
                return false;
            }
            if ( $.trim( cancelReason ) == '' ) {
                alert( '请选择取消订单原因' );
                return false;
            }
            $.getJSON( '/order/cancel', { 'order_no' : orderNo, 'cancel_reason' : cancelReason }, function ( json ) {
                if ( json.success == 1 ) {
                    if ( layerIndex ) {
                        layer.close( layerIndex );
                    }
                    alert( '取消订单成功' );
                    window.location.reload( true );
                }
                else {
                    alert( json.message );
                }
            } );
        } );

        return false;
    } );
    //提交取消订单
    /*$("body").delegate("#layerSubmit","click",function(){
        var refuseReason = $(this).parents('div').find('.refuseReason').find('option:selected').val();
        if($.trim(orderNo) == ''){
            alert('订单号不能为空');
            return false;
        }
        if($.trim(refuseReason) == ''){
            alert('请选择拒绝理由');
            return false;
        }
        $.getJSON('/order/refuse-order',{'order_no':orderNo,'refuse_reason':refuseReason},function (data) {
            if (data.success == false) {
                alert(data.message);
            }else{
                layer.alert(data.message, {
                }, function(){
                    location.href='/order/wait';
                });
            }
        });
    });*/
    //关闭弹层
    $( "body" ).delegate( "#layerCancel", "click", function () {
        if ( layerIndex ) {
            layer.close( layerIndex );
        }
    } );

    //接受
    var acceptFlag = true;

    function accept( order_no ) {
        var url = '/order/accept-order';
        layer.confirm( '确定要承接此订单吗？', {
            btn  : ['确定', '取消'],
            area : ['300px', '220px']
        }, function () {
            $( '.confirm' ).hide();
            $( '.confirming' ).show();

            if ( acceptFlag == true ) {
                acceptFlag = false;
                $.getJSON( '/order/accept-order', { 'order_no' : order_no }, function ( data ) {
                    if ( data.success == false ) {
                        alert( data.message );
                    } else {
                        layer.alert( data.message, {}, function () {
                            location.href = '/work/wait-assign';
                        } );
                    }
                } );
            }
        }, function () {
            layer.close();
        } );
    }

    //修改服务时间
    $( ".changeTime" ).click( function () {
        var orderNO = $( this ).attr( 'data-no' );
        layer.open( {
            type      : 2,
            title     : '请选择服务时间',
            area      : ['400px', '400px'],
            fixed     : false, //不固定
            maxmin    : false,
            scrollbar : false,
            content   : ['/order/change-time?order_no=' + orderNO, 'no'],
            end       : function () {
                location.reload();
            }
        } );
        return false;
    } );
    // 图片放大
    $( '.gz-img img' ).on( 'click', function () {
        var imgUrl = $( this ).attr( 'src' );
        var bb = imgUrl.substring( 0, imgUrl.indexOf( '?' ) );
        bb = bb + '?x-oss-process=image/auto-orient,1'
        var imgaa = new Image();

        var winWidht = $( window ).width();
        var winHight = $( window ).height();
        setTimeout( function () {
            imgaa.src = bb;
            $( '.imgEnlarge-chlid img' ).css( 'width', 'auto' );
            $( '.imgEnlarge-chlid img' ).css( 'height', 'auto' );
            if ( imgaa.width > winWidht ) {
                $( '.imgEnlarge-chlid img' ).css( 'width', winWidht - 40 + 'px' );
                $( '.imgEnlarge-chlid img' ).css( 'height', 'auto' )
            } else {
                $( '.imgEnlarge-chlid img' ).css( 'width', imgaa.width + 'px' );
                $( '.imgEnlarge-chlid img' ).css( 'height', 'auto' );
            }
            if ( imgaa.height > winHight ) {
                $( '.imgEnlarge-chlid img' ).css( 'height', winHight - 40 + 'px' );
                $( '.imgEnlarge-chlid img' ).css( 'width', 'auto' );
            } else {
                $( '.imgEnlarge-chlid img' ).css( 'height', imgaa.height + 'px' );
                $( '.imgEnlarge-chlid img' ).css( 'width', 'auto' )
            }
        }, 500 )

        $( '#enlargeImg' ).attr( 'src', bb );
        $( '.imgEnlarge-box' ).show();
    } )
    $( '.imgEnlarge-box' ).on( 'click', function () {
        $( '.imgEnlarge-box' ).hide();
    } );

    //查看回访
    $( function () {
        $( "body" ).delegate( ".viewVisit", "click", function () {
            workNo = $( this ).attr( 'href' );
            $.getJSON( '/work/view-visit', { 'work_no' : workNo }, function ( json ) {
                if ( json.success == 1 ) {
                    alert( json.data.content );
                }
                else {
                    alert( json.message );
                }
            } );
            return false;
        } );
    } );
    //编辑产品信息
    $( ".edit-products" ).click( function () {
        var prod_id = $( this ).attr( 'data-id' );
        layer.open( {
            type    : 2,
            title   : '编辑服务产品',
            area    : ['700px', '400px'],
            fixed   : false, //不固定
            maxmin  : false,
            content : ['/sale-order/edit?id=' + prod_id, 'yes']
            /*end:function(){
                location.reload();
            }*/
        } );
        return false;
    } );

    //小程序订单通过操作
    $( '.adopt' ).click( function () {
        var orderNo = $( this ).attr( 'data-href' );
        confirm( '确定要将此订单通过审核吗？', function ( obj ) {
            $.getJSON( '/order/through-audit', { order_no : orderNo }, function ( json ) {
                if ( json.success == true ) {
                    alert( '操作成功', function () {
                        document.location.reload( true );
                    } );
                }
                else {
                    alert( json.message );
                }
            } );
        }, this );
    } );
    //指派
    var iframeCallbackAddressId = 0;
    $( ".assignFws" ).click( function () {
        var order_no = $( this ).attr( 'data-href' );
        var address_id = $( this ).attr( 'data-addressid' );
        layer.open( {
            type    : 2,
            title   : false,
            area    : ['580px', '350px'],
            fixed   : false, //不固定
            maxmin  : false,
            content : '/order/assigin-institution?order_no=' + order_no + '&address_id=' + address_id,
        } );
    } );
    //改派
    $( ".reAssignFws" ).click( function () {
        var order_no = $( this ).attr( 'data-href' );
        var address_id = $( this ).attr( 'data-addressid' );
        layer.open( {
            type    : 2,
            title   : false,
            area    : ['580px', '350px'],
            fixed   : false, //不固定
            maxmin  : false,
            content : '/order/re-assigin-institution?order_no=' + order_no + '&address_id=' + address_id,
        } );
    } );
    //待验收 验收通过
    $( '.acceptancePass' ).click( function () {
        var orderNo = $( this ).data( 'orderno' );
        confirm( '确定要将此订单验收通过吗？', function ( obj ) {
            $.getJSON( '/order/ajax-confirm-finish', { order_no : orderNo }, function ( json ) {
                if ( json.success == true ) {
                    alert( '验收成功', function () {
                        document.location.reload( true );
                    } );
                }
                else {
                    alert( json.message );
                }
            } );
        }, this );
    } );
    //开始服务
    $( '.startService' ).click( function () {
        var orderNo = $( this ).data( 'href' );
        confirm( "确定要将此订单开始服务吗？", function ( obj ) {
            $.getJSON( '/work/ajax-start-service', { work_no : orderNo, type : 2 }, function ( json ) {
                if ( json.success == true ) {
                    alert( '操作成功', function () {
                        document.location.reload( true );
                    } );
                }
                else {
                    alert( json.message );
                }
            } );
        }, this );
    } );
    //待验收 验收驳回
    $( '.acceptanceFailure' ).click( function () {
        layerIndex = layer.open( {
            type    : 1,
            title   : '提示',
            area    : ['400px', '280px'],
            fixed   : true,
            maxmin  : false,
            content : $( '#acceptanceFailureLayer' ).html()
        } );
        orderNo = $( this ).attr( 'data-orderno' );

        //提交取消订单
        $( "body" ).delegate( "#acceptanceFailureSubmit", "click", function () {
            var remark = $( this ).parents( 'div' ).find( '#remark' ).val();
            if ( $.trim( orderNo ) == '' ) {
                alert( '订单号不能为空' );
                return false;
            }
            if ( $.trim( remark ) == '' ) {
                alert( '请输入驳回原因' );
                return false;
            }
            $.getJSON( '/order/ajax-acceptance-failure', {
                'order_no' : orderNo,
                'reason'   : remark
            }, function ( json ) {
                if ( json.success == 1 ) {
                    if ( layerIndex ) {
                        layer.close( layerIndex );
                    }
                    alert( '操作成功' );
                    window.location.reload( true );
                }
                else {
                    alert( json.message );
                }
            } );
        } );

        return false;
    } );

    //指派驳回
    $( 'body' ).delegate( '.rejectAssign', 'click', function () {
        layerIndex = layer.open( {
            type    : 1,
            title   : '提示',
            area    : ['300px', '220px'],
            fixed   : true,
            maxmin  : false,
            content : $( '#rejectAssignLayer' ).html()
        } );
        orderNo = $( this ).data( 'href' );

        //提交取消订单
        $( "body" ).delegate( "#rejectAssignSubmit", "click", function () {
            var rejectReason = $( this ).parents( 'div' ).find( '.rejectReason' ).find( 'option:selected' ).val();
            if ( $.trim( orderNo ) == '' ) {
                alert( '订单号不能为空' );
                return false;
            }
            if ( $.trim( rejectReason ) == '' ) {
                alert( '请选择驳回订单原因' );
                return false;
            }
            $.getJSON( '/order/reject', { 'order_no' : orderNo, 'reject_reason' : rejectReason }, function ( json ) {
                if ( json.success == 1 ) {
                    if ( layerIndex ) {
                        layer.close( layerIndex );
                    }
                    alert( '驳回成功' );
                    window.location.reload( true );
                }
                else {
                    alert( json.message );
                }
            } );
        } );
        return false;
    } );
    //查看所有服务内容
    $( ".viewAllProduct" ).click( function () {
        var orderNo = $( this ).attr( "id-data" );
        layer.open( {
            type    : 2,
            title   : ['全部服务内容', 'font-size:18px;'],
            area    : ['600px', '380px'],
            fixed   : false, //不固定
            maxmin  : false,
            content : '/order/view-all-product?order_no=' + orderNo,
        } );
        return false;
    } );
    //查看配件包，物料清单
    $( ".viewProductBag" ).click( function () {
        var prodId = $( this ).attr( "id-data" );
        layer.open( {
            type    : 2,
            title   : ['查看物料清单', 'font-size:18px;'],
            area    : ['800px', '500px'],
            fixed   : false, //不固定
            maxmin  : false,
            content : '/product/materials-detail?id=' + prodId,
        } );
        return false;
    } );
</script>
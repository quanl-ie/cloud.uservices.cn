<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '改派机构';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time()?>">
<style type="text/css">
    body{background: #fff;}
     ul{padding-left: 5px;}
     .ul-box-h180{padding:0 20px;}
     .pop-title100 span{line-height: 70px;}
</style>
<div class="pop-title100">
    <span class="bottom-orangexian">机构</span>
    <span>
            <a href="/order/re-assign?order_no=<?=$order_no?>&local_url=<?=$location_url?>" target="_parent">技师</a>
    </span>
    <p class="position-left">改派给</p>
</div>
    <form action="?" method="post" id="reassigin-institution-add">
        <div class="information-list" style="height: auto;">
            <div>
                <input  type="hidden" name="department_id" id="department_id" value="<?=$department_id?>" />
                <input  type="hidden" name="top_id" id="top_id" value="<?=$top_id?>" />
                <input  type="hidden" name="order_no" id="order_no" value="<?=$order_no?>" />
                <input  type="hidden" name="address_id" id="address_id" value="<?=$address_id?>" />
                <input type="hidden" id="local_url" value="<?=$location_url?>"/>
                <input type="hidden" class="total_work" value="<?=$total_work?>"/>
                <input type="hidden" class="service_department_id" value="<?=$service_department_id?>"/>
                <div class="ul-box-h180" id="show_data">
                    <ul id="level-tree"></ul>
                </div>
            </div>
        </div>
        <hr>
        <div  class="information-list text-center">
            <input type="button"  class="btn bg-f7" id="closeIframe" value="取消">
            <input type="submit"  class="btn btn-orange"   value="提交" class="btn-orange">
        </div>
    </form>

<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>

    <script>
    window.onload = function(){
       // selectGray ();
        mechanismInit();
    }
    var service_department_id = $(".service_department_id").val();
    function mechanismInit() {
        var pid = $('#top_id').val();
        var address_id = $("#address_id").val();
        var total_work = $('.total_work').val();
        var disabled_flag = '';
        var ulData = '';
        $.getJSON('/organization/ajax-get-department',{'pid':pid,"total_work":total_work,"service_area":"1","address_id":address_id},function (json) {
            var data = json.data;
            var message = json.message;
            if(json.success){
                $.each(data,function(index, data) {
                    ulData +="<li><span>";
                    data.sub==1?ulData+= '<i class="icon2-shixinyou tree-i"></i>':'';
                    ulData+='<input type="radio" value="'+data.id+'" name="service_depart_id" id="jg'+data.id+'"';
                    data.id == service_department_id?ulData+='disabled="disabled" ':'';
                    ulData+= 'class="pr1"><label for="jg'+data.id+'"';
                    data.id == service_department_id?ulData+='class="disabled-label"':'';
                    ulData+='>'+data.name+'</label></span></li>';
                });
                $('#level-tree').html(ulData);
            }else{
                $("#show_data").html(message);
            }

        })

    }
     // 选择类目
    $(document).on('click','.tree-i', function(event) {
        event.preventDefault();
        var url = '/organization/ajax-get-department'
        getDepartment($(this),url);
    });
    function getDepartment(el,url) {
        var id = $(el).next('input').val();
        var address_id = $("#address_id").val();
        var total_work = $('.total_work').val();
        var _this = $(el).parent('span');
        var auth = $('.data-auth').val();
        $.getJSON(url,{'pid':id,"total_work":total_work,"service_area":"1","address_id":address_id},function (data) {
            console.log(data);
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>"
                $(data.data).each(function(i,el) {
                    ulData +="<li><span>";
                    el.exist==1?ulData+= '<i class="icon2-shixinyou tree-i"></i>':'';
                    ulData+='<input type="radio" id="a'+el.id+'" value="'+el.id+'" name="service_depart_id"';
                    el.id == service_department_id?ulData+='disabled="disabled" ':'';
                    ulData+=' ><label for="a'+el.id+'"';
                    el.id == service_department_id?ulData+='class="disabled-label"':'';
                    ulData+='>'+el.name+'<i class="gou-i"></i></label></span></li>';
                });
                ulData +="</ul>";
                _this.parent('li').append(ulData);
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');


            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })
    }


 var index = parent.layer.getFrameIndex(window.name);
    $("#reassigin-institution-add").Validform({
        ajaxPost:true,
        callback:function(data){
            if (data.code == 200) {
                var local_url = $("#local_url").val();
                parent.layer.close(index);
                parent.layer.alert(data.message);
                parent.window.location.href=local_url;

            }else{
                $.Hidemsg();
                alert(data.message);
            }
        }
    });

    //关闭弹层
    $("#closeIframe").click(function () {
        parent.layer.close(index);
    });

</script>
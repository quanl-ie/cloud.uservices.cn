<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '全部订单';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time();?>">
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">全部订单</span>
</div> 
<section id="main-content">
<div class="row">
    <div class="col-md-12">                    
        <div class="panel panel-default">                     
            <div class="panel-body">
                <form action="/order/index" class="form-horizontal form-border" id="form">
                    <!--搜索开始-->
                    <div class="form-group jn-form-box">
                        <div class="col-sm-12 no-padding-left">
                            <div class="single-search">
                                <label class="search-box-lable">订单编号</label>
                                <div class="single-search-kuang1">
                                    <input type="text" class="form-control" maxlength="20" name="order_no" value="<?php echo isset($_GET['order_no'])?$_GET['order_no']:'';?>">
                                </div>
                            </div>
                            <div class="single-search">
                                <label class="search-box-lable">客户姓名</label>
                                <div class="single-search-kuang1">
                                    <input type="text" class="form-control" name="account_name" maxlength="20" value="<?php echo isset($_GET['account_name'])?$_GET['account_name']:'';?>">
                                </div>
                            </div>
                            <div class="single-search">
                                <label class="search-box-lable">客户电话</label>
                                <div class="single-search-kuang1">
                                    <input type="text" class="form-control" name="account_mobile" maxlength="11" value="<?php echo isset($_GET['account_mobile'])?$_GET['account_mobile']:'';?>">
                                </div>
                            </div>
                            <div class="single-search">
                                <label class="search-box-lable">技师姓名</label>
                                <div class="single-search-kuang1">
                                    <input type="text" class="form-control" name="tec_name" maxlength="20" value="<?php echo isset($_GET['tec_name'])?$_GET['tec_name']:'';?>">
                                </div>
                            </div>
                            <div class="single-search">
                                <label class="search-box-lable">技师电话</label>
                                <div class="single-search-kuang1">
                                    <input type="text" class="form-control" name="tec_mobile" maxlength="11" value="<?php echo isset($_GET['tec_mobile'])?$_GET['tec_mobile']:'';?>">
                                </div>
                            </div>
                            <div class="single-search">
                                <label class="search-box-lable">状态</label>
                                <div class="single-search-kuang1">
                                    <select class="form-control" name="status">
                                        <option value="">请选择</option>
                                        <?php foreach ($statusArr as $key=>$val):?>
                                            <option value="<?php echo $key; ?>" <?php if(isset($_GET['status']) && $_GET['status']==$key):?>selected<?php endif; ?>><?php echo $val;?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="single-search">
                                <label class="search-box-lable">服务类型</label>
                                <div class="single-search-kuang1">
                                    <select class="form-control" name="work_type">
                                        <option value="">请选择</option>
                                        <?php foreach ($workTypeArr as $key=>$val):?>
                                            <option value="<?php echo $val['id']; ?>" <?php if(isset($_GET['work_type']) && $_GET['work_type']==$val['id']):?>selected<?php endif; ?>><?php echo $val['title'];?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <!--<div class="single-search region-limit" style="overflow: visible;">
                                <label class=" search-box-lable">产品类目</label>
                                <div class="single-search-kuang1">
                                    <input type="hidden" name="class_id" id="class_id" value="<?/*=isset($_GET['class_ids']) ? $_GET['class_ids'] : ''*/?>">
                                    <input type="text" class="form-control" id="class_name" value="<?/*=isset($_GET['class_name']) ? $_GET['class_name'] : '' */?>" placeholder="请选择类目" readonly onclick="clickCategory(this,'product-name')">
                                    <div class="drop-dw-layerbox" style="display: none;">
                                        <div class="ul-box-h180">
                                            <ul id="depart1" class="depart">
                                                <?php /*if (!empty($class)) : */?>
                                                    <?php /*foreach ($class as $key => $val) : */?>
                                                        <li>
                                                             <span>
                                                                <?php /*if (isset($val['exist']) && $val['exist'] == 1) : */?>
                                                                    <i class="icon2-shixinyou tree-i"></i>
                                                                <?php /*endif;*/?>
                                                                 <input type="checkbox" id="product-name<?/*=$val['id'] */?>" value="<?/*=$val['id'] */?>" name="product-name" >
                                                                 <label for="product-name<?/*=$val['id'] */?>"><?/*=$val['class_name'] */?><i class="gou-i"></i></label>
                                                            </span>
                                                        </li>
                                                    <?php /*endforeach; */?>
                                                <?php /*endif; */?>
                                            </ul>
                                        </div>
                                        <button class="btn btn-orange belongedBtn" id="belongedBtn">确定</button>
                                    </div>
                                </div>
                            </div>-->
                            <div class="single-search region-limit" style="overflow: visible;">
                                <label class=" search-box-lable">所属机构</label>
                                <div class="single-search-kuang1" id="de">
                                    <input type="hidden" name="department_id" id="department_id" value="<?=isset($_GET['department_ids']) ? $_GET['department_ids'] : ''?>">
                                    <input type="text" class="form-control" value="<?=isset($_GET['department_names']) ? $_GET['department_names'] : '' ?>" id="department"  readonly <?php if(!empty($department)){ echo ' placeholder="请选择机构"';}?> onclick="clickCategory(this,'a')">
                                    <div class="drop-dw-layerbox" style="display: none;">
                                        <div class="ul-box-h180">
                                            <input type="hidden" class="data-auth" value="<?php echo $auth;?>">
                                            <ul id="depart2" class="depart">
                                              <?php if (!empty($department)) {?>
                                                    <?php foreach ($department as $key => $val) { ?>
                                                      <li>
                                                        <span>
                                                        <?php if ($val['exist'] == 1){?>
                                                            <i class="icon2-shixinyou  tree-i"></i>
                                                            <input type="checkbox" id="a<?=$val['id'] ?>" value="<?=$val['id'] ?>" name="department-name" >
                                                        <?php }else{?>
                                                            <input type="checkbox" id="a<?=$val['id'] ?>" value="<?=$val['id'] ?>" name="department-name" >
                                                        <?php };?>
                                                         <label for="a<?=$val['id'] ?>"><?=$val['name'] ?><i class="gou-i"></i></label>
                                                        </span>
                                                      </li>
                                                    <?php }?>
                                                <?php } ?>
                                                <!--<li>
                                                    <span>
                                                        <i class="icon2-shixinyou tree-i"></i>
                                                        <input type="checkbox" id="mechanism9" value="9" name="mechanism">
                                                        <label for="mechanism9">z的大企业<i class="gou-i"></i></label>
                                                    </span>
                                                </li>-->
                                            </ul>
                                        </div>
                                        <button class="btn btn-orange" id="belongedBtn1">确定</button>
                                    </div>
                                </div>
                            </div>
                            <div class="single-search">
                                <label class="search-box-lable">质保状态</label>
                                <div class="single-search-kuang1">
                                    <select class="form-control" name="amount_type">
                                        <option value="">请选择</option>
                                        <option value="1" <?php if(isset($_GET['amount_type']) && $_GET['amount_type']==1):?>selected<?php endif;?>>保内</option>
                                        <option value="2" <?php if(isset($_GET['amount_type']) && $_GET['amount_type']==2):?>selected<?php endif;?>>保外</option>
                                    </select>
                                </div>
                            </div>
                            <div class="three-search">
                                <label class="search-box-lable">所在区域</label>
                                <div  class="single-search-kuang3">
                                    <div class="one-third">
                                        <select  class="form-control" id="province" name="province_id" datatype="*" sucmsg=" " nullmsg="请选择所在区域">
                                            <option value="">请选择省</option>
                                            <?php foreach ($province as $key => $val): ?>
                                                <option value="<?=$val['region_id'] ?>" <?php if(isset($_GET['province_id']) && $_GET['province_id']==$val['region_id']):?>selected<?php endif; ?>><?=$val['region_name'] ?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <div  class="one-third">
                                        <select class="form-control" id="city" name="city_id" datatype="*" sucmsg=" " nullmsg="请选择所在区域">
                                            <option value="">请选择市</option>
                                            <?php if($cityList){?>
                                                <?php foreach ($cityList as $key => $val): ?>
                                                    <option value="<?=$val['region_id'] ?>" <?php if(isset($_GET['city_id']) && $_GET['city_id']==$val['region_id']):?>selected<?php endif; ?>><?=$val['region_name'] ?></option>
                                                <?php endforeach;?>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div  class="one-third">
                                        <select class="form-control" id="district" name="district_id" datatype="*" sucmsg=" " nullmsg="请选择所在区域">
                                            <option value="">请选择区</option>
                                            <?php if($districtList){?>
                                                <?php foreach ($districtList as $key => $val): ?>
                                                    <option value="<?=$val['region_id'] ?>" <?php if(isset($_GET['district_id']) && $_GET['district_id']==$val['region_id']):?>selected<?php endif; ?>><?=$val['region_name'] ?></option>
                                                <?php endforeach;?>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="justify_fix"></div>
                                </div>
                            </div>

                            <div class="tow-search">
                                <label class="search-box-lable">服务时间</label>
                                <div class="single-search-kuang2">
                                    <div  class="half-search">
                                        <input type="text" id="datetimepicker-pay-top" class="form-control" name="plan_start_time" value="<?php echo isset($_GET['plan_start_time'])?$_GET['plan_start_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})">
                                        <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})">
                                            <i class="icon2-rili"></i>
                                        </span>
                                        <span class="zhi"> 至</span>
                                    </div>

                                    <div class="half-search">
                                        <input type="text" id="datetimepicker-pay-end" class="form-control" name="plan_end_time" value="<?php echo isset($_GET['plan_end_time'])?$_GET['plan_end_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})" >
                                        <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})">
                                            <i class="icon2-rili"></i>
                                        </span>
                                        
                                    </div>
                                    <p class="city3_fix"></p>
                                </div>
                            </div>
                            <div class="tow-search">
                                <label class="search-box-lable">下单时间</label>
                                <div class="single-search-kuang2">
                                    <div  class="half-search">
                                        <input type="text" id="datetimepicker-create-top" class="form-control" name="create_start_time" value="<?php echo isset($_GET['create_start_time'])?$_GET['create_start_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-create-end\')}'})">
                                        <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-create-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-create-end\')}'})"><i class="icon2-rili"></i></span>
                                        <span class="zhi"> 至</span>
                                    </div>
                                    <div  class="half-search">
                                        <input type="text" id="datetimepicker-create-end" class="form-control" name="create_end_time" value="<?php echo isset($_GET['create_end_time'])?$_GET['create_end_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-create-top\')}'})" >
                                        <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-create-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-create-top\')}'})"><i class="icon2-rili"></i></span>
                                    </div>
                                    <p class="city3_fix"></p>
                                </div>
                            </div>
                            <div class="search-confirm2">
                                <button class="btn btn-success "><i class=" "></i> 搜索</button>
                                 <a target="_blank" href="?<?php echo $exportParams;?>" style="color: #fff;" class="btn btn-orange"><span  data-toggle="modal" data-target="#scrollingModal" title="先搜索后再导出">导出</span></a>
                            </div>
                        </div>
                        <div class="table-responsive col-sm-12">
                            <table class="table table-bordered table-striped table-hover">
               
                                <!--搜索结束-->
                                <thead bgcolor="#2693ff">
                                    <tr>
                                        <th>订单编号</th>
                                        <th>订单来源</th>
                                        <th>主题</th>
                                        <th>客户姓名</th>
                                        <th>客户电话</th>
                                        <th>所属机构</th>
                                       <!-- <th>品牌</th>
                                        <th>产品名称</th>-->
                                        <th>服务类型</th>
                                        <th>预约服务时间</th>
                                        <th>下单时间</th>
                                        <th>技师姓名</th>
                                        <th>技师电话</th>
                                        <th>状态</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($data['list']) && $data['list']):?>
                					<?php foreach ($data['list'] as $key=>$val):?>
                                    <tr>
                                        <td>
                                            <?php if(in_array('/order/view',$selfRoles)){?>
                                                <a href="/order/view?order_no=<?php echo $val['order_no'];?>"><?php echo $val['order_no'];?></a>
                                            <?php }else{;?>
                                                <?php echo $val['order_no'];?>
                                            <?php }?>
                                        </td>
                                        <td><?php echo isset($val['source_desc'])?$val['source_desc']:'';?></td>
                                        <td><?php echo $val['subject_name'];?></td>
                                        <td>
                                            <?php if(in_array('/account/view',$selfRoles)){?>
                                                <a href="/account/view?account_id=<?php echo $val['account_id'];?>"><?php echo $val['account_name'];?></a>
                                            <?php }else{;?>
                                                <?php echo $val['account_name'];?>
                                            <?php }?>
                                        </td>
                                        <td><?php echo $val['account_mobile'];?></td>
                                        <td><?php echo $val['department_name'];?></td>
                                       <!-- <td><?php /*echo $val['brand_name'];*/?></td>
                                        <td><?php /*echo $val['prod_desc']; */?></td>-->
                                        <td><?php echo $val['work_type_desc'];?></td>
                                        <td><?php if($val['plan_time'] != 0 && $val['plan_time'] != null) { echo \common\models\Order::getPlantimeType($val['plan_time_type'],$val['plan_time']);} ?></td>
                                        <td><?php echo date('Y-m-d H:i:s',$val['create_time']); ?></td>
                                        <td><?php echo isset($val['technician_name'])?$val['technician_name']:'';?></td>
                                        <td><?php echo isset($val['technician_mobile'])?$val['technician_mobile']:'';?></td>
                                        <td><?php echo $val['status_desc']; ?><?php if($val['status'] == 7): ?>
                                                <span class="tip icon2-bangzhu" style="position:inherit;" title="驳回原因" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="<?php echo $val['remarks'];?>"></span>
                                            <?php endif; ?></td>
                                        <td  class="operation">

                                            <?php if($directCompanyId == $val['direct_company_id'] && $directCompanyId == $val['sever_direct_id']):?>
                                                <?php if(in_array($val['status'],[1,8])):?> <!-- 待指派 -->
                                                    <?php if(in_array('/order/main-assigin',$selfRoles)){?>
                                                        <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" data-id="<?php echo $val['sale_order_id'];?>" data-addressId="<?php echo $val['address_id'];?>" data-workType="<?php echo $val['work_type']; ?>" class="assignFws">指派</a>
                                                    <?php }?>
                                                        <?php if(in_array('/order/change-time',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="chagePlantime">修改服务时间</a>
                                                        <?php }?>
                                                        <?php if($departmentId == $val['service_department_id'] && $departmentId != $val['order_department_id'] && $val['total_work'] <= 1):?>
                                                        <?php if(in_array('/order/reject',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="rejectAssign">驳回订单</a>
                                                        <?php }?>
                                                        <?php endif; ?>
                                                        <?php if($directCompanyId == $val['direct_company_id'] && $val['total_work'] <= 1 && ($departmentId != $val['service_department_id'] || $departmentId == $val['order_department_id'])):?>
                                                            <?php if(in_array('/order/cancel',$selfRoles)){?>
                                                                <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="cancelOrder">取消订单</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                <?php elseif($val['status'] == 2 || $val['status'] == 9):?><!-- 待服务 -->
                                                    <?php if(in_array('/order/main-re-assigin',$selfRoles)){?>
                                                        <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" data-id="<?php echo $val['sale_order_id'];?>" data-addressId="<?php echo $val['address_id'];?>" data-workType="<?php echo $val['work_type']; ?>" class="reAssignFws">改派</a>
                                                    <?php }?>
                                                    <?php if(in_array('/order/change-time',$selfRoles)){?>
                                                        <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="chagePlantime">修改服务时间</a>
                                                    <?php }?>
                                                        <!-- 只有指派了技师才可以开始服务 -->
                                                        <?php if($val['assign_type'] ==2): ?>
                                                        <?php if(in_array('/work/ajax-start-service',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="startService">开始服务</a>
                                                        <?php }?>
                                                        <?php endif; ?>
                                                        <?php if($departmentId == $val['service_department_id'] && $departmentId != $val['order_department_id'] && $val['total_work'] <= 1):?>
                                                        <?php if(in_array('/order/reject',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="rejectAssign">驳回订单</a>
                                                        <?php }?>
                                                        <?php endif; ?>
                                                        <?php if($directCompanyId == $val['direct_company_id'] && $val['total_work'] <= 1 && ($departmentId != $val['service_department_id'] || $departmentId == $val['order_department_id'])):?>
                                                        <?php if(in_array('/order/cancel',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="cancelOrder">取消订单</a>
                                                        <?php }?>
                                                        <?php endif; ?>
                                                 <?php elseif($val['status'] == 3): ?> <!-- 服务中 -->
                                                    <?php if(in_array('/work/set-finish',$selfRoles)){?>
                                                        <a href="/work/set-finish?work_no=<?php echo $val['order_no'];?>&type=2" class="update">完成服务</a>
                                                    <?php }?>
                                                         <?php elseif($val['status'] == 4 || ($val['status'] ==7 && $val['assign_type'] == 2)): ?>
                                                           <!-- <?php /*if($departmentId != $val['service_department_id'] || ($departmentId == $val['order_department_id'] && $departmentId == $val['service_department_id'])):*/?>
                                                                <?php /*if(in_array('/order/ajax-confirm-finish',$selfRoles)){*/?>
                                                                    <a href="javascript:void(0)" data-orderno="<?php /*echo $val['order_no']*/?>" data-flag="2" class="acceptancePass">验收通过</a>
                                                                <?php /*}*/?>
                                                            --><?php /*else:*/?>
                                                            <!-- modify 公司下属部门 验证通过不需要提交给上级  （上边的注释 下边a标签去掉 data-flag 可改回需上级审核）-->
                                                             <?php if(in_array('/order/ajax-confirm-finish',$selfRoles)){?>
                                                                    <a href="javascript:void(0)" data-orderno="<?php echo $val['order_no']?>" data-flag="2" class="acceptancePass">验收通过</a>
                                                             <?php }?>
                                                                <?php if($val['assign_type'] == 1): ?>
                                                                    <?php if(in_array('/order/ajax-acceptance-failure',$selfRoles)){?>
                                                                        <a href="javascript:void(0)" data-orderno="<?php echo $val['order_no']?>" class="acceptanceFailure">验收驳回</a>
                                                                    <?php }?>
                                                                <?php endif; ?>
                                                           <!-- --><?php /*endif;*/?>
                                                            <?php if($val['assign_type'] == 2):?>
                                                                <?php if(in_array('/work/add',$selfRoles)){?>
                                                                    <a href="/work/add?order_no=<?php echo $val['order_no'];?>" class="update">预约下次上门</a>
                                                                <?php }?>
                                                            <?php endif; ?>
                                                <?php elseif($val['status'] == 10): ?>
                                                    <?php if(in_array('/order/through-audit',$selfRoles)){?>
                                                        <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="adoptAudit">通过</a>
                                                    <?php }?>
                                                    <?php if(in_array('/order/not-through',$selfRoles)){?>
                                                        <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="rejectAudit">驳回</a>
                                                    <?php }?>
                                                <?php endif;?>
                                            <?php elseif($directCompanyId == $val['direct_company_id'] && $directCompanyId != $val['sever_direct_id']):?>
                                                <?php if(in_array($directCompanyId,$val['service_department_ids'])):?>
                                                <?php if($val['total_work'] == 2):?>
                                                    <?php if($val['status'] == 4 || ($val['status'] ==7 && $val['assign_type'] == 2)): ?>
                                                        <?php if(in_array('/order/ajax-confirm-finish',$selfRoles)){?>
                                                            <a href="javascript:void(0)" data-orderno="<?php echo $val['order_no']?>" data-flag="2" class="acceptancePass">验收通过</a>
                                                        <?php }?>
                                                        <?php if($val['assign_type'] == 1): ?>
                                                            <?php if(in_array('/order/ajax-acceptance-failure',$selfRoles)){?>
                                                                <a href="javascript:void(0)" data-orderno="<?php echo $val['order_no']?>" class="acceptanceFailure">验收驳回</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                        <?php if($val['assign_type'] == 2):?>
                                                            <?php if(in_array('/work/add',$selfRoles)){?>
                                                                <a href="/work/add?order_no=<?php echo $val['order_no'];?>" class="update">预约下次上门</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                    <?php elseif($val['status'] == 10): ?>
                                                        <?php if(in_array('/order/through-audit',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="adoptAudit">通过</a>
                                                        <?php }?>
                                                        <?php if(in_array('/order/not-through',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="rejectAudit">驳回</a>
                                                        <?php }?>
                                                    <?php endif;?>
                                                <?php else:?>
                                                    <?php if(in_array($val['status'],[1,8])):?> <!-- 待指派 -->
                                                        <?php if(in_array('/order/main-assigin',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" data-id="<?php echo $val['sale_order_id'];?>" data-addressId="<?php echo $val['address_id'];?>" data-workType="<?php echo $val['work_type']; ?>" class="assignFws">指派</a>
                                                        <?php }?>
                                                        <?php if(in_array('/order/change-time',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="chagePlantime">修改服务时间</a>
                                                        <?php }?>
                                                        <?php if($departmentId == $val['service_department_id'] && $departmentId != $val['order_department_id'] && $val['total_work'] <= 1):?>
                                                            <?php if(in_array('/order/reject',$selfRoles)){?>
                                                                <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="rejectAssign">驳回订单</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                        <?php if($directCompanyId == $val['direct_company_id'] && $val['total_work'] <= 1 && ($departmentId != $val['service_department_id'] || $departmentId == $val['order_department_id'])):?>
                                                            <?php if(in_array('/order/cancel',$selfRoles)){?>
                                                                <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="cancelOrder">取消订单</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                    <?php elseif($val['status'] == 2 || $val['status'] == 9):?><!-- 待服务 -->
                                                        <?php if(in_array('/order/main-re-assigin',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" data-id="<?php echo $val['sale_order_id'];?>" data-addressId="<?php echo $val['address_id'];?>" data-workType="<?php echo $val['work_type']; ?>" class="reAssignFws">改派</a>
                                                        <?php }?>
                                                        <?php if(in_array('/order/change-time',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="chagePlantime">修改服务时间</a>
                                                        <?php }?>
                                                        <!-- 只有指派了技师才可以开始服务 -->
                                                        <?php if($val['assign_type'] ==2): ?>
                                                            <?php if(in_array('/work/ajax-start-service',$selfRoles)){?>
                                                                <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="startService">开始服务</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                        <?php if($departmentId == $val['service_department_id'] && $departmentId != $val['order_department_id'] && $val['total_work'] <= 1):?>
                                                            <?php if(in_array('/order/reject',$selfRoles)){?>
                                                                <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="rejectAssign">驳回订单</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                        <?php if($directCompanyId == $val['direct_company_id'] && $val['total_work'] <= 1 && ($departmentId != $val['service_department_id'] || $departmentId == $val['order_department_id'])):?>
                                                            <?php if(in_array('/order/cancel',$selfRoles)){?>
                                                                <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="cancelOrder">取消订单</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                    <?php elseif($val['status'] == 4 || ($val['status'] ==7 && $val['assign_type'] == 2)): ?>
                                                        <?php if(in_array('/order/ajax-confirm-finish',$selfRoles)){?>
                                                            <a href="javascript:void(0)" data-orderno="<?php echo $val['order_no']?>" data-flag="2" class="acceptancePass">验收通过</a>
                                                        <?php }?>
                                                        <?php if($val['assign_type'] == 1): ?>
                                                            <?php if(in_array('/order/ajax-acceptance-failure',$selfRoles)){?>
                                                                <a href="javascript:void(0)" data-orderno="<?php echo $val['order_no']?>" class="acceptanceFailure">验收驳回</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                        <?php if($val['assign_type'] == 2):?>
                                                            <?php if(in_array('/work/add',$selfRoles)){?>
                                                                <a href="/work/add?order_no=<?php echo $val['order_no'];?>" class="update">预约下次上门</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                        <?php elseif($val['status'] == 10): ?>
                                                                <?php if(in_array('/order/through-audit',$selfRoles)){?>
                                                                    <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="adoptAudit">通过</a>
                                                                <?php }?>
                                                                <?php if(in_array('/order/not-through',$selfRoles)){?>
                                                                    <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="rejectAudit">驳回</a>
                                                                <?php }?>
                                                        <?php endif;?>
                                                    <?php endif;?>
                                                <?php endif;?>
                                            <?php elseif($directCompanyId != $val['direct_company_id'] && $directCompanyId == $val['sever_direct_id']):?>
                                                <?php if(in_array($val['status'],[1,8])):?> <!-- 待指派 -->
                                                    <?php if(in_array('/order/main-assigin',$selfRoles)){?>
                                                        <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" data-id="<?php echo $val['sale_order_id'];?>" data-addressId="<?php echo $val['address_id'];?>" data-workType="<?php echo $val['work_type']; ?>" class="assignFws">指派</a>
                                                    <?php }?>
                                                    <?php if(in_array('/order/change-time',$selfRoles)){?>
                                                        <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="chagePlantime">修改服务时间</a>
                                                    <?php }?>
                                                    <?php if($departmentId == $val['service_department_id'] && $departmentId != $val['order_department_id'] && $val['total_work'] <= 1):?>
                                                        <?php if(in_array('/order/reject',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="rejectAssign">驳回订单</a>
                                                        <?php }?>
                                                    <?php endif; ?>

                                                <?php elseif($val['status'] == 2 || $val['status'] == 9):?><!-- 待服务 -->
                                                    <?php if(in_array('/order/main-re-assigin',$selfRoles)){?>
                                                        <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" data-id="<?php echo $val['sale_order_id'];?>" data-addressId="<?php echo $val['address_id'];?>" data-workType="<?php echo $val['work_type']; ?>" class="reAssignFws">改派</a>
                                                    <?php }?>
                                                    <?php if(in_array('/order/change-time',$selfRoles)){?>
                                                        <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="chagePlantime">修改服务时间</a>
                                                    <?php }?>
                                                    <!-- 只有指派了技师才可以开始服务 -->
                                                    <?php if($val['assign_type'] ==2): ?>
                                                        <?php if(in_array('/work/ajax-start-service',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="startService">开始服务</a>
                                                        <?php }?>
                                                    <?php endif; ?>
                                                    <?php if($departmentId == $val['service_department_id'] && $departmentId != $val['order_department_id'] && $val['total_work'] <= 1):?>
                                                        <?php if(in_array('/order/reject',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="rejectAssign">驳回订单</a>
                                                        <?php }?>
                                                    <?php endif; ?>
                                                <?php elseif($val['status'] == 3): ?> <!-- 服务中 -->
                                                    <?php if(in_array('/work/set-finish',$selfRoles)){?>
                                                        <a href="/work/set-finish?work_no=<?php echo $val['order_no'];?>&type=2" class="update">完成服务</a>
                                                    <?php }?>
                                                <?php elseif($val['status'] == 4 || ($val['status'] ==7 && $val['assign_type'] == 2)): ?>
                                                    <?php if(in_array('/order/ajax-confirm-finish',$selfRoles)){?>
                                                        <a href="javascript:void(0)" data-orderno="<?php echo $val['order_no']?>" class="acceptancePass">验收通过</a>
                                                    <?php }?>
                                                    <?php if($val['assign_type'] == 1): ?>
                                                        <?php if(in_array('/order/ajax-acceptance-failure',$selfRoles)){?>
                                                            <a href="javascript:void(0)" data-orderno="<?php echo $val['order_no']?>" class="acceptanceFailure">验收驳回</a>
                                                        <?php }?>
                                                    <?php endif; ?>
                                                    <?php if($val['assign_type'] == 2):?>
                                                        <?php if(in_array('/work/add',$selfRoles)){?>
                                                            <a href="/work/add?order_no=<?php echo $val['order_no'];?>" class="update">预约下次上门</a>
                                                        <?php }?>
                                                    <?php endif; ?>
                                            <?php elseif($val['status'] == 10): ?>
                                                    <?php if(in_array('/order/through-audit',$selfRoles)){?>
                                                        <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="adoptAudit">通过</a>
                                                    <?php }?>
                                                    <?php if(in_array('/order/not-through',$selfRoles)){?>
                                                        <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="rejectAudit">驳回</a>
                                                    <?php }?>
                                            <?php endif;?>
                                            <?php elseif($directCompanyId != $val['direct_company_id'] && $directCompanyId != $val['sever_direct_id']):?>
												<?php if(in_array($directCompanyId,$val['service_department_ids'])):?>
                                                <?php if($val['total_work'] == 2):?>
                                                    <?php if($val['status'] == 4 || ($val['status'] ==7 && $val['assign_type'] == 2)): ?>
                                                        <?php if(in_array('/order/ajax-confirm-finish',$selfRoles)){?>
                                                            <a href="javascript:void(0)" data-orderno="<?php echo $val['order_no']?>" class="acceptancePass">验收通过</a>
                                                        <?php }?>
                                                        <?php if($val['assign_type'] == 1): ?>
                                                            <?php if(in_array('/order/ajax-acceptance-failure',$selfRoles)){?>
                                                                <a href="javascript:void(0)" data-orderno="<?php echo $val['order_no']?>" class="acceptanceFailure">验收驳回</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                        <?php if($val['assign_type'] == 2):?>
                                                            <?php if(in_array('/work/add',$selfRoles)){?>
                                                                <a href="/work/add?order_no=<?php echo $val['order_no'];?>" class="update">预约下次上门</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                    <?php elseif($val['status'] == 10): ?>
                                                        <?php if(in_array('/order/through-audit',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="adoptAudit">通过</a>
                                                        <?php }?>
                                                        <?php if(in_array('/order/not-through',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="rejectAudit">驳回</a>
                                                        <?php }?>
                                                    <?php endif;?>
                                                <?php else:?>
                                                    <?php if(in_array($val['status'],[1,8])):?> <!-- 待指派 -->
                                                        <?php if(in_array('/order/main-assigin',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" data-id="<?php echo $val['sale_order_id'];?>" data-addressId="<?php echo $val['address_id'];?>" data-workType="<?php echo $val['work_type']; ?>" class="assignFws">指派</a>
                                                        <?php }?>
                                                        <?php if(in_array('/order/change-time',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="chagePlantime">修改服务时间</a>
                                                        <?php }?>
                                                        <?php if($departmentId == $val['service_department_id'] && $departmentId != $val['order_department_id'] && $val['total_work'] <= 1):?>
                                                            <?php if(in_array('/order/reject',$selfRoles)){?>
                                                                <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="rejectAssign">驳回订单</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                        <?php if($directCompanyId == $val['direct_company_id'] && $val['total_work'] <= 1 && ($departmentId != $val['service_department_id'] || $departmentId == $val['order_department_id'])):?>
                                                            <?php if(in_array('/order/cancel',$selfRoles)){?>
                                                                <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="cancelOrder">取消订单</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                    <?php elseif($val['status'] == 2 || $val['status'] == 9):?><!-- 待服务 -->
                                                        <?php if(in_array('/order/main-re-assigin',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" data-id="<?php echo $val['sale_order_id'];?>" data-addressId="<?php echo $val['address_id'];?>" data-workType="<?php echo $val['work_type']; ?>" class="reAssignFws">改派</a>
                                                        <?php }?>
                                                        <?php if(in_array('/order/change-time',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="chagePlantime">修改服务时间</a>
                                                        <?php }?>
                                                        <!-- 只有指派了技师才可以开始服务 -->
                                                        <?php if($val['assign_type'] ==2): ?>
                                                            <?php if(in_array('/work/ajax-start-service',$selfRoles)){?>
                                                                <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="startService">开始服务</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                        <?php if($departmentId == $val['service_department_id'] && $departmentId != $val['order_department_id'] && $val['total_work'] <= 1):?>
                                                            <?php if(in_array('/order/reject',$selfRoles)){?>
                                                                <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="rejectAssign">驳回订单</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                        <?php if($directCompanyId == $val['direct_company_id'] && $val['total_work'] <= 1 && ($departmentId != $val['service_department_id'] || $departmentId == $val['order_department_id'])):?>
                                                            <?php if(in_array('/order/cancel',$selfRoles)){?>
                                                                <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="cancelOrder">取消订单</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                    <?php elseif($val['status'] == 4 || ($val['status'] ==7 && $val['assign_type'] == 2)): ?>
                                                        <?php if(in_array('/order/ajax-confirm-finish',$selfRoles)){?>
                                                            <a href="javascript:void(0)" data-orderno="<?php echo $val['order_no']?>" class="acceptancePass">验收通过</a>
                                                        <?php }?>
                                                        <?php if($val['assign_type'] == 1): ?>
                                                            <?php if(in_array('/order/ajax-acceptance-failure',$selfRoles)){?>
                                                                <a href="javascript:void(0)" data-orderno="<?php echo $val['order_no']?>" class="acceptanceFailure">验收驳回</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                        <?php if($val['assign_type'] == 2):?>
                                                            <?php if(in_array('/work/add',$selfRoles)){?>
                                                                <a href="/work/add?order_no=<?php echo $val['order_no'];?>" class="update">预约下次上门</a>
                                                            <?php }?>
                                                        <?php endif; ?>
                                                    <?php elseif($val['status'] == 10): ?>
                                                        <?php if(in_array('/order/through-audit',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="adoptAudit">通过</a>
                                                        <?php }?>
                                                        <?php if(in_array('/order/not-through',$selfRoles)){?>
                                                            <a href="javascript:void(0);" data-href="<?php echo $val['order_no'];?>" class="rejectAudit">驳回</a>
                                                        <?php }?>
                                                    <?php endif;?>
                                                <?php endif;?>
											<?php endif;?>
                                        <?php endif;?>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                    <?php else:?>
                                        <tr>
                                            <tr><td colspan="13" class="no-record">暂无数据</td></tr>
                                        </tr>
                                    <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                </form>
                <div class="col-xs-12 text-center pagination">
                        <?php echo $pageHtml;?>
                </div>
            </div>
       </div>
   </div>
 </div>
</section>

<!-- 取消订单弹层 -->
<div id="cancelLayer" style="display: none">
    <div class="col-md-12" style="text-align: center;padding:10px">
        <p class="col-md-12"><b>提示</b></p>
        <p class="col-md-12">请选择取消订单原因</p>
        <select  class="col-md-12 form-control cancelReason">
            <option value="">请选择</option>
            <option value="客户服务内容变更">客户服务内容变更</option>
            <option value="订单内容填写有误">订单内容填写有误</option>
            <option value="其他原因">其他原因</option>
        </select>
        <div class="col-sm-12" style="text-align: center;margin-top:15px;">
            <button class="btn btn-success" id="layerSubmit"><i class="fa fa-search"></i>确定</button>
            <button class="btn layerCancel"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>

<!-- 小程序审核驳回 -->
<div id="rejectAuditLayer" style="display: none">
    <div class="col-md-12" style="text-align: center;padding:10px">
        <p class="col-md-12"><b></b></p>
        <p style="margin-bottom: 15px">请选择驳回原因</p>
        <select  class="form-control cancelReason" style="width: 90%;margin-left: 5%">
            <option value="">请选择</option>
            <option value="非本公司产品">非本公司产品</option>
        </select>
        <div class="col-sm-12" style="text-align: center;margin-top:30px;">
            <button class="btn btn-success" id="rejectAuditSubmit"  style="padding: 4px 12px"><i class="fa fa-search"></i>确定</button>
            <button class="btn layerCancel"  style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>

<!-- 待验收 验证驳回 -->
<div id="acceptanceFailureLayer" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <p style="margin:0 0 10px 0;text-align: left;">请输入驳回原因</p>
        <div class="select-fuji">
            <textarea placeholder="" id="remark" style="width: 100%;height:80px;"></textarea>
        </div>
        <div style="margin-top:30px;text-align:center;">
            <button class="btn btn-success" id="acceptanceFailureSubmit"  style="padding: 4px 12px"><i class="fa fa-search"></i>确定</button>
            <button class="btn" id="layerCancel" style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>

<!-- 指派驳回 -->
<div id="rejectAssignLayer" style="display: none">
    <div class="col-md-12" style="text-align: center;padding:10px">
        <p class="col-md-12"><b>提示</b></p>
        <p class="col-md-12">请选择驳回订单原因</p>
        <select  class="col-md-12 form-control rejectReason">
            <option selected="" value="">请选择</option>
            <option value="非本公司产品">非本公司产品</option>
            <option value="无技师可指派">无技师可指派</option>
            <option value="客户临时改约">客户临时改约</option>
            <option value="不具备进场条件">不具备进场条件</option>
            <option value="其他原因">其他原因</option>
        </select>
        <div class="col-sm-12" style="text-align: center;margin-top:15px;">
            <button class="btn btn-success" id="rejectAssignSubmit"><i class="fa fa-search"></i>确定</button>
            <button class="btn layerCancel"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>


<script src="<?php echo Url::to('/js/orderTree.js');?>"></script>
<script>
window.onload = function() {
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    <?php endif;?>
}
//提示
$(".tip").popover();
var layerIndex = null;
var orderNo = '';

//关闭弹层
$("body").delegate(".layerCancel","click",function(){
    if(layerIndex){
        layer.close(layerIndex);
    }
});

//取消订单
$(".cancelOrder").click(function () {
    layerIndex = layer.open({
        type: 1,
        title: '提示',
        area: ['300px', '220px'],
        fixed: true,
        maxmin: false,
        content: $('#cancelLayer').html()
    });
    orderNo = $(this).data('href');

    return false;
});
//提交取消订单
$("body").delegate("#layerSubmit","click",function(){
    var cancelReason = $(this).parents('div').find('.cancelReason').find('option:selected').val();
    if($.trim(orderNo) == ''){
        alert('订单号不能为空');
        return false;
    }
    if($.trim(cancelReason) == ''){
        alert('请选择取消订单原因');
        return false;
    }
    $.getJSON('/order/cancel',{'order_no':orderNo,'cancel_reason':cancelReason},function (json) {
        if(json.success == 1){
            if(layerIndex){
                layer.close(layerIndex);
            }
            alert('取消订单成功',function () {
                window.location.reload(true);
            });
        }
        else {
            alert(json.message);
        }
    });
});

//修改服务时间
$('.chagePlantime').click(function () {
    var orderNo = $(this).data('href');
    layer.open({
        type: 2,
        title: '请选择服务时间',
        area: ['400px', '400px'],
        fixed: false, //不固定
        maxmin: false,
        content: ['/order/change-time?order_no='+orderNo,'no'],
    });
    return false;
});
//根据城市父级id获取市级数据
$("#province").change(function() {
    $("#city").html("<option value=''>请选择市</option>");
    $("#district").html("<option value=''>请选择区</option>");
    var pid = $("#province").val();
    if(pid != ''){
        $.get("/account/get-city", { "province_id": pid }, function (data) {
            $("#city").html(data);
        });
    }
});
//根据城市获取区县
$("#city").change(function() {
    var pid = $("#city").val();
    if(pid != ''){
        $.get("/account/get-district", { "city_id": pid }, function (data) {
            $("#district").html(data);
        })
    }
});
//指派
var iframeCallbackAddressId = 0;
$(".assignFws").click(function() {
    var order_no   = $(this).attr('data-href');
    var address_id = $(this).attr('data-addressid');
    layer.open({
        type: 2,
        title: false,
        area: ['580px', '350px'],
        fixed: false, //不固定
        maxmin: false,
        content: '/order/assigin-institution?order_no=' + order_no+'&address_id='+address_id,
    });
});
//改派
$(".reAssignFws").click(function() {
    var order_no   = $(this).attr('data-href');
    var address_id = $(this).attr('data-addressid');
    layer.open({
        type: 2,
        title: false,
        area: ['580px', '350px'],
        fixed: false, //不固定
        maxmin: false,
        content: '/order/re-assigin-institution?order_no=' + order_no+'&address_id='+address_id,
    });
});

//小程序设审核通过操作
$('.adoptAudit').click(function () {
    var orderNo = $(this).data('href');
    confirm('确定要将此订单通过审核吗？',function(obj){
        $.getJSON('/order/through-audit',{order_no:orderNo},function (json) {
            if(json.success == true){
                alert('操作成功',function () {
                    document.location.reload(true);
                });
            }
            else {
                alert(json.message);
            }
        });
    },this);
});

//小程序审核驳回
$(".rejectAudit").click(function () {
    layerIndex = layer.open({
        type: 1,
        title: '提示',
        area: ['300px', '220px'],
        fixed: true,
        maxmin: false,
        content: $('#rejectAuditLayer').html()
    });
    orderNo = $(this).data('href');
    //提交不通过订单
    $("body").delegate("#rejectAuditSubmit","click",function(){
        var cancelReason = $(this).parents('div').find('.cancelReason').find('option:selected').val();
        if($.trim(orderNo) == ''){
            alert('订单号不能为空');
            return false;
        }
        if($.trim(cancelReason) == ''){
            alert('请选择驳回原因');
            return false;
        }
        $.getJSON('/order/not-through',{'order_no':orderNo,'reason':cancelReason},function (json) {
            if(json.success == 1){
                if(layerIndex){
                    layer.close(layerIndex);
                }
                alert('操作成功',function () {
                    window.location.reload(true);
                });
            }
            else {
                alert(json.message);
            }
        });
    });

    return false;
});

//待验收 验收通过
$('.acceptancePass').click(function () {
    var orderNo = $(this).data('orderno');
    var flag = $(this).data('flag');//判断是服务本身机构完成 1 还是 有数据权限的下级完成 2
    confirm('确定要将此订单验收通过吗？', function (obj) {
        $.getJSON('/order/ajax-confirm-finish', {order_no: orderNo,flag:flag}, function (json) {
            if (json.success == true) {
                alert('验收成功', function () {
                    document.location.reload(true);
                });
            }
            else {
                alert(json.message);
            }
        });
    }, this);
});
//待验收 验收驳回
$('.acceptanceFailure').click(function () {
    layerIndex = layer.open({
        type: 1,
        title: '提示',
        area: ['400px', '280px'],
        fixed: true,
        maxmin: false,
        content: $('#acceptanceFailureLayer').html()
    });
    orderNo = $(this).attr('data-orderno');

    //提交取消订单
    $("body").delegate("#acceptanceFailureSubmit","click",function(){
        var remark = $(this).parents('div').find('#remark').val();
        if($.trim(orderNo) == ''){
            alert('订单号不能为空');
            return false;
        }
        if($.trim(remark) == ''){
            alert('请输入驳回原因');
            return false;
        }
        $.getJSON('/order/ajax-acceptance-failure',{'order_no':orderNo,'reason':remark},function (json) {
            if(json.success == 1){
                if(layerIndex){
                    layer.close(layerIndex);
                }
                alert('操作成功');
                window.location.reload(true);
            }
            else {
                alert(json.message);
            }
        });
    });

    return false;
});


//指派驳回
$('body').delegate('.rejectAssign','click',function () {
    layerIndex = layer.open({
        type: 1,
        title: '提示',
        area: ['300px', '220px'],
        fixed: true,
        maxmin: false,
        content: $('#rejectAssignLayer').html()
    });
    orderNo = $(this).data('href');

    //提交取消订单
    $("body").delegate("#rejectAssignSubmit","click",function(){
        var rejectReason = $(this).parents('div').find('.rejectReason').find('option:selected').val();
        if($.trim(orderNo) == ''){
            alert('订单号不能为空');
            return false;
        }
        if($.trim(rejectReason) == ''){
            alert('请选择驳回订单原因');
            return false;
        }
        $.getJSON('/order/reject',{'order_no':orderNo,'reject_reason':rejectReason},function (json) {
            if(json.success == 1){
                if(layerIndex){
                    layer.close(layerIndex);
                }
                alert('驳回成功');
                window.location.reload(true);
            }
            else {
                alert(json.message);
            }
        });
    });
    return false;
});

//开始服务
$('.startService').click(function () {
    var orderNo = $(this).data('href');
    confirm("确定要将此订单开始服务吗？", function (obj) {
        $.getJSON('/work/ajax-start-service', {work_no: orderNo, type: 2}, function (json) {
            if (json.success == true) {
                alert('操作成功', function () {
                    document.location.reload(true);
                });
            }
            else {
                alert(json.message);
            }
        });
    }, this);
});

//根据城市获取区县
$("#city").change(function() {
    var pid = $("#city").val();
    if(pid != ''){
        $.get("/account/get-district", { "city_id": pid }, function (data) {
            $("#district").html(data);
        })
    }
});
// 选择类目
$(document).on('click','#depart1 .tree-i', function(event) {
    console.log(5555555);
    event.preventDefault();
    var url = '/common/ajax-get-class?mark=1&type=1'
    // var url = '/common/ajax-get-department';
    getDepartment($(this),url,"product-name");
});
// 选择机构
$(document).on('click','#depart2 .tree-i',function(event) {
    console.log(2222);
    var company_id = <?php echo $directCompanyId;?>;
    var self_id = <?php echo $departmentId;?>;
    var url = '/common/ajax-get-department?self_department_id='+self_id+'&company_id='+company_id;
    getDepartment($(this),url,'department-name');
});

</script>
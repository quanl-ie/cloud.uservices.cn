<?php

use yii\helpers\Url;
use webapp\models\BrandQualification;

$this->title = '待评价订单';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/us-icon.css'); ?>?v=<?php echo time(); ?>">
<style type="text/css">

    .us-table-cell-break {
        font-family: "Microsoft YaHei", sans-serif;
        font-size: 12px;
        list-style: none;
        font-weight: 400;
        padding: 0;
        letter-spacing: 0;
        white-space: nowrap;
        color: #8c8e99;
        display: inline-block;
        margin: 0 40px 0 0;
    }

</style>
<div id="app" v-cloak>
    <section id="main-content">
        <div class="panel-body">
            <us-adv-search title="待评价订单" placeholder="搜索客户姓名或客户电话"
                           @on-input-search="inputData"
                           @on-search="searchData"
                           v-model="search.fuzzy">
                <us-form label-whl="90px" item-whl="250px" wrap-whl="150px">
                    <us-form-row>
                        <us-form-item label="客户姓名：">
                            <us-ipt-text v-model="search.account_name"></us-ipt-text>
                        </us-form-item>
                        <us-form-item label="客户电话：">
                            <us-ipt-text v-model="search.account_mobile"></us-ipt-text>
                        </us-form-item>
                        <us-form-item label="完成服务时间：" whl="400px">
                            <us-ipt-datetime section whl="300px" v-model="search.timeSection"></us-ipt-datetime>
                        </us-form-item>
                        <us-form-item label-whl="0">
                            <us-button type="primary" @click="exportBtn">导出</us-button>
                        </us-form-item>
                    </us-form-row>
                </us-form>
            </us-adv-search>
            <us-table :data="dataList">
                <us-table-column label="订单编号" field="order_no">
                    <template slot-scope="scope">
                        <?php if (in_array('/order/view', $selfRoles)): ?>
                            <a :href="'/order/view?order_no='+scope.data.order_no">{{scope.data.order_no}}</a>
                        <?php else: ?>
                            {{scope.data.order_no}}
                        <?php endif; ?>
                    </template>
                </us-table-column>
                <us-table-column label="订单来源" field="source_desc"></us-table-column>
                <!--                <us-table-column label="主题" field="subject_name"></us-table-column>-->
                <us-table-column label="客户姓名" field="account_name"></us-table-column>
                <us-table-column label="客户电话" field="account_mobile"></us-table-column>
                <!--                <us-table-column label="所属机构" field="department_name"></us-table-column>-->
                <us-table-column label="服务类型" field="work_type_desc"></us-table-column>
                <!--                <us-table-column label="服务机构" field="service_depar_name"></us-table-column>-->
                <us-table-column label="完成服务时间" field="finish_time"></us-table-column>
                <us-table-column label="下单时间" field="create_time">
                    <template slot-scope="scope">{{_parseDate(scope.data.create_time)}}</template>
                </us-table-column>
                <us-table-column label="操作">
                    <template slot-scope="scope">
                        <?php if (in_array('/order/add-appraisal', $selfRoles)): ?>
                            <a v-if="scope.data.login_direct_company_id != scope.data.service_department_id" :href="'/order/view?order_no='+scope.data.order_no">去评价</a>
                        <?php endif; ?>
                    </template>
                </us-table-column>
                <us-table-column break-col>
                    <div slot-scope="scope">
                        <span class="us-table-cell-break">主题：{{scope.data.subject_name}}</span>
                        <span class="us-table-cell-break">所属机构：{{scope.data.department_name}}</span>
                        <span class="us-table-cell-break">服务机构：{{scope.data.service_depar_name}}</span>
                    </div>
                </us-table-column>
            </us-table>
            <div class="text-center">
                <us-pagination
                        @change="changePage"
                        :page-no="page.pageNo"
                        :total-page="page.totalPage"
                        :total="page.total"></us-pagination>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/order/wait-appraisal.js?v=<?php echo $v; ?>"></script>

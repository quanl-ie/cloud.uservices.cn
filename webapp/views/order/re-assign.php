<?php
use yii\helpers\Url;
$this->title = '改派技师';
$this->params['breadcrumbs'][] = $this->title;

?>
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.4.1&key=bbcdabda1427179d77bb95fac7b2b902&plugin=AMap.Autocomplete"></script>
<script type="text/javascript" src="http://cache.amap.com/lbs/static/addToolbar.js"></script>
<script src="http://webapi.amap.com/ui/1.0/main.js?v=1.0.11"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/assign.css');?>">
<style type="text/css">
    #main-content>.panel-body{height: calc(100% - 50px);padding:0;}
    .name-title-lable{width: 100px;}
    .addinput-list{margin-left: 100px;}
    .technician-right-list{display: none;}
    .amap-icon img{width: 100%;height: 100%;border-radius: 50%;}
    li label{font-weight: normal;cursor: pointer}
    .name-title-lable{padding-right: 10px;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">改派技师</span>
    <input type="hidden" id="account_data" data-name="<?php echo isset($account['name'])?$account['name']:''?>"  data-mobile="<?php echo isset($account['mobile'])?$account['mobile']:''?>" data-img="<?php if(!empty($account['avatar_url'])){echo $account['avatar_url'];}else{echo Url::to('/images/customer1.png');}?>" data-address="<?php echo isset($account['address'])?$account['address']:''?>">
    <!-- 时间 -->
    <div class="right-btn-box">
        <div style="float: left;margin-left: 100px;height: 36px;">
            <div class="assign-search-list" style="margin-top: 0;width: 538px;">
                <label class="name-title-lable">服务时间</label>
                <div class="addinput-list">
                     <div class="time-equable">
                        <!--日历选择框-->
                        <input type="text"  class="form-control  planTime" id="selectDate" name="selectDate" value="<?php if(isset($workData['plan_time'])) echo date("Y-m-d", ($workData['plan_time']==0?time():$workData['plan_time']))?>"  placeholder="请选择服务时间" readonly onclick="selectDateDay()">
                        <i class="icon2-rili riqi1" id="rili" onclick="selectDateDay()"></i>
                    </div>
                    <div class="time-equable">
                        <!--类型选择框-->
                        <select name="plan_time_type" id="plan_time_type" style="width: 160px;" class="plan_time_type form-control">
                            <option value="0">请选择</option>
                            <?php if(!empty($setTimeType)) :?>
                            <?php foreach($setTimeType as $k=>$v) :?>
                            <option <?php if($workData['plan_time_type'] == $k):?>selected="selected"<?php endif;?> value="<?=$k?>"><?=$v?></option>
                            <?php endforeach;?>
                            <?php endif;?>
                        </select>
                    </div>
                    <div class="time-equable">
                        <!--选择具体时间时才显示setTime这个框-->
                        <div id="show_setTime" <?php if($workData['plan_time_type'] != 5):?>style="display: none"<?php endif;?>>
                            <select id="setTime" class="form-control" name="setTime">
                                <?php for($i=0;$i<24;$i++):
                                    $time = sprintf('%02d',$i);
                                    $hour = date("H:i",$workData['plan_time']);
                                    ?>
                                    <option <?php if($hour == ($time.':00')):?>selected="selected"<?php endif;?> value="<?php echo $time.':00' ?>"><?php echo $time;?>:00</option>
                                    <option <?php if($hour == ($time.':30')):?>selected="selected"<?php endif;?> value="<?php echo $time.':30' ?>"><?php echo $time;?>:30</option>
                                <?php endfor;?>
                            </select>
                        </div>
                    </div>
                    <div  class="city23_fix"></div>
                </div>
            </div>
        </div>
        <a href="<?php echo $referrer==''?'/work/wait-assign':$local_url;?>" id="goback" style="float:right;margin-right:20px;" class="btn bg-f7 cancelLink">返回</a>
    </div>
</div>
<section id="main-content">
    <div class="panel-body">
        <div class="assign-search-box">
            <div class="assign-search-list">
                <label class="name-title-lable">选择分组</label>
                <div class="addinput-list">
                    <select class="form-control search" id="groupid">
                        <option data-leader="0" data-total="<?php echo $defaultGroupCount;?>" <?php if($defaultGroupCount == 0):?>style="color:#999999;"<?php endif;?>  value="0">默认组</option>
                        <?php foreach ($groupingArr as $val): ?>
                            <option data-leader="<?php echo $val['group_leader']?$val['group_leader']:'0'; ?>" data-total="<?php echo $val['total_technician'];?>" <?php if($val['total_technician']==0):?>style="color:#999999;" <?php endif;?> <?php if($oldTechnicianArr['groupingId'] == $val['id']):?>selected="selected"<?php endif;?> value="<?php echo $val['id'];?>"><?php echo $val['name'];?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>
            <!--
            <div class="assign-search-list">
                <label class="name-title-lable">接单范围</label>
                <div class="addinput-list">
                    <select class="form-control search" id="range">
                        <option value="0">全部</option>
                        <option value="1">接单范围</option>
                    </select>
                </div>
            </div>
            <div class="assign-search-list">
                <label class="name-title-lable">技能</label>
                <div class="addinput-list">
                    <select class="form-control search" id="skill">
                        <option value="0">全部</option>
                        <option value="1">符合技能</option>
                    </select>
                </div>
            </div>
            -->
        </div>
        <div class="assign-box">
            <!-- 展示列表 -->
            <div class="technician-box">
                <div class="assign-title">
                    <a id="individual" href="javascript:">指派技师</a>
                    <a id="headman" href="javascript:">指派组长</a>
                    <a style="position: absolute;text-align: right;color:#337ab7;right: 10px;top: 3px;text-decoration:underline;" id="refreshPos" href="javascript:">刷新位置</a>
                </div>
                <div class="assign-content" style="height:calc(100% - 180px)">
                    <div class="technician-left-list">
                        <ul>

                        </ul>
                    </div>
                    <div  class="technician-right-list">
                        <ul>

                        </ul>
                    </div>
                    <div class="shrink" onclick="shrink()"></div>
                </div>
                <div class="assign-btn">
                    <input type="hidden" id="orderno" value="<?php echo $orderNo;?>" />
                    <input type="hidden" id="local_url" value="<?php echo $local_url;?>" />
                    <input type="hidden" id="workno" value="<?php echo $workNo;?>" />
                    <input type="hidden" id="groupLender" value="<?php echo implode(',',$oldTechnicianArr['groupLender']);?>" />
                    <input type="hidden" id="lender" value="<?php echo $oldTechnicianArr['lender'];?>" />
                    <input type="hidden" id="member" value="<?php echo implode(',',$oldTechnicianArr['member']);?>" />
                    <input type="hidden" id="oldPlantime" value="<?php echo $workData['plan_time'];?>" />

                    <a href="<?php echo $referrer==''?'/work/wait-assign':$local_url;?>" class="btn bg-f7 mgr20 cancelLink">取消</a>
                    <button class="btn btn-orange" id="btnAssign">确认</button>
                    <button class="btn btn-orange" id="btnAssigning" style="display: none;">提交中</button>
                </div>
            </div>
            <div id="container1" style="height: 100%;background: #f5f5f5"></div>
        </div>
    </div>
</section>
<!-- 选择技师 -->
<div id="cancelLayer" style="display: none">
    <div class="col-md-12" style="text-align: center;padding:10px 30px;">
        <p class="col-md-12" style="margin-bottom: 20px">请选择工单负责人</p>
        <select class="col-md-12 form-control reason" id="reason">
            <option value="">请选择改派原因</option>
            <option value="技师未携带全部配件">技师未携带全部配件</option>
            <option value="技师距离服务地址过远">技师距离服务地址过远</option>
            <option value="服务时间与其他工单冲突">服务时间与其他工单冲突</option>
            <option value="技师不具备该服务技能">技师不具备该服务技能</option>
            <option value="技师已休息 /请假 /离职">技师已休息 /请假 /离职</option>
            <option value="其他原因">其他原因</option>
        </select>
        <p class="col-md-12" style="margin-bottom: 20px"></p>
        <select  class="col-md-12 form-control" id="optHeadman">
            <option value="">请选择工单负责人</option>
        </select>
        <div class="col-sm-12" style="margin-top:28px;">
            <button class="btn btn-orange" id="confirmSubmitAssign"><i class="fa fa-search"></i>确定</button>
            <button class="btn btn-orange" id="confirmSubmitAssigning" style="display: none;"><i class="fa fa-search"></i>提交中</button>
            <button class="btn bg-f7" id="layerCancel"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>
<!-- 改派原因-->
<div id="groupLeaderAssignLayer" style="display: none">
    <div class="col-md-12" style="text-align: center;padding:10px 30px;">
        <p class="col-md-12" style="margin-bottom: 20px">请选择改派原因</p>
        <select class="col-md-12 form-control reason" id="reason">
            <option value="">请选择改派原因</option>
            <option value="技师未携带全部配件">技师未携带全部配件</option>
            <option value="技师距离服务地址过远">技师距离服务地址过远</option>
            <option value="服务时间与其他工单冲突">服务时间与其他工单冲突</option>
            <option value="技师不具备该服务技能">技师不具备该服务技能</option>
            <option value="技师已休息 /请假 /离职">技师已休息 /请假 /离职</option>
            <option value="其他原因">其他原因</option>
        </select>
        <div class="col-sm-12" style="margin-top:28px;">
            <button class="btn btn-orange" id="groupLeaderAssign"><i class="fa fa-search"></i>确定</button>
            <button class="btn btn-orange" id="groupLeaderAssigning" style="display: none;"><i class="fa fa-search"></i>提交中</button>
            <button class="btn bg-f7" id="layerCancel"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>
<div style="display: none;">
    <input type="hidden" id="account_lat" value="<?php echo $addressArr['lat'];?>" />
    <input type="hidden" id="account_lon" value="<?php echo $addressArr['lon'];?>" />
    <input type="hidden" id="service_title" value="<?php echo $title;?>" />
    <input type="hidden" id="work_type" value="<?php echo $workData['work_type'];?>" />
    <input type="hidden" id="address_id" value="<?php echo $workData['address_id'];;?>" />
    <input type="hidden" id="sale_order_id" value="<?php echo $workData['workDetail'][0]['sale_order_id'];;?>" />
</div>
<script type="text/javascript" src="<?php echo Url::to('/js/re-assign.js?v='.Yii::$app->params['v']);?>"></script>
<script type="text/javascript" src="<?php echo Url::to('/js/WdatePicker/WdatePicker.js');?>"></script>
<script type="text/javascript" src="<?php echo Url::to('/js/WdatePicker/time.js?v='.Yii::$app->params['v']);?>"></script>
<script type="text/javascript">
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
        // height ()
    }
    $(window).resize(function (){
        // height ();
    });
    // 选择日期
    //日历插件调用
    var myDate = new Date();
    var myMonth = myDate.getMonth()<10?"0"+(myDate.getMonth()+1):myDate.getMonth();
    var myDay = myDate.getDate()<9?'0'+myDate.getDate():myDate.getDate();
    var minDate = myDate.getFullYear()+'-'+myMonth+'-'+myDay;
    function selectDateDay() {
        jeDate('#selectDate', {
            trigger:false,
            isToday:false,
            format: 'YYYY-MM-DD',
            maxDate: '2099-06-16 23:59:59',
            donefun:function(){
                var d = new Date();
                var selectDate = new Date($('#selectDate').val());
                if(selectDate.getDate() == d.getDate())
                {
                    $('.plan_time_type').find('option').each(function(){
                        if(d.getHours() >=12 && $(this).val() == 2){
                            $(this).attr('disabled','disabled');
                            $(this).css('background','#cccccc');
                        }
                        if(d.getHours() >=18 && $(this).val() == 3){
                            $(this).attr('disabled','disabled');
                            $(this).css('background','#cccccc');
                        }
                     });
                }
                else 
                {
                    $('.plan_time_type').find('option').each(function(){
                        $(this).removeAttr('disabled');
                        $(this).removeAttr('style');
                    });
                }
                if($('#selectDate').val() !='<?php echo date("Y-m-d",$workData['plan_time'])?>') {
                    $(".plan_time_type").val(0);
                    $(".plan_time_type").attr('selected');
                    $("#show_setTime").html('');
                }
            }
        })
    }
     //服务时间类型为5时候，显示具体时间下拉框
    $("#plan_time_type").click(function(){
        var id = $(this).val();
        var selectDate = $("#selectDate").val();
        if(selectDate==''){
            $("#plan_time_msg").html('请选择日期');
            $("#plan_time_msg").css('display','block');
            
            $(this).attr('disabled',true);
            return false;
        }
        if(id==5){
            $.ajax({
                url:'/order/set-time',
                type:'POST',
                data:{time_type:id,date:selectDate},
                success:function (msg) {
                    $("#show_setTime").html(msg);
                }
            })
            $("#show_setTime").css('display','block');
        }else{
            $("#show_setTime").css('display','none');
        }
    })    
    
    function height () {
        var height = $(window).height();
        $('.technician-total ul').css('height',height-200);
        $('#container1').css('height',height-200);
    }
    // 侧边收缩
    function shrink(){
        var width = $('.technician-box').width();
        // $('.technician-box').position().left==0?$('.technician-box').css({'left':-width+'px'}):$('.technician-box').css('left','0');
        if($('.technician-box').position().left==0){
            $('.shrink').css('background', 'url(../images/dakai.png)');
            $('.technician-box').css('left',-width+'px')
        }else{
            $('.shrink').css('background', 'url(../images/shouhui.png)');
            $('.technician-box').css('left','0')
        }
    }

    $('.cancelLink').click(function () {
        confirm('是否取消改派?', function (url) {
            document.location.replace(url);
        },$(this).attr('href'));
        return false;
    });

    //刷新位置
    $('#refreshPos').click(function () {
        $('.assign-title .elect-a').click();
    });
</script>

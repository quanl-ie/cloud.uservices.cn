<?php
use yii\helpers\Url;
$this->title = '指派技师';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" href="http://cache.amap.com/lbs/static/main1119.css"/>
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.4.2&key=bbcdabda1427179d77bb95fac7b2b902&plugin=AMap.Geocoder"></script>
<script type="text/javascript" src="http://cache.amap.com/lbs/static/addToolbar.js"></script>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<style>
    .amap-icon>img{width: 100%;height: 100%;}
    .technician-tc{padding-left: 10px}
    .technician-tc>li{list-style-type: none;font-size: 14px;line-height: 2;color: #888;}
</style>
<div class="page-container">
    <?php if (!isset($data['lon']) && !isset($data['lat'])): ?>
    <h5 class="son-menu">地址信息错误</h5>
    <?php else:?>

    <div id="container" style="height: 600px;"></div>
    <?php endif;?>
</div>

<script>
window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    window.parent.document.location.reload();
    <?php endif;?>
}
    var lon = <?php echo $data['lon'] ?>;
    var lat = "<?php echo $data['lat'] ?>";

    var lnglatXY = [lon, lat]; //已知点坐标
    console.log(lnglatXY);
    var map = new AMap.Map("container", {
        resizeEnable: true,
        center:lnglatXY,
        zoom: 10
    });
    // openInfo ();
     var geocoder = new AMap.Geocoder({
        radius: 1000,
        // extensions: "all"
    });
    geocoder.getAddress(lnglatXY, function(status, result) {
        if (status === 'complete' && result.info === 'OK') {
            // geocoder_CallBack(result);
             openInfo (result);
        }
    });
    function openInfo (data) {
        console.log(data);
        technician = new AMap.Marker({
            position:lnglatXY,
            map: map,
            icon: new AMap.Icon({
                size: new AMap.Size(40, 50),  //图标大小
                image: "<?php echo Url::to('/images/customer.png');?>",
                imageOffset: new AMap.Pixel(0, 0)
            })

        });
        var address = data.regeocode.formattedAddress; //返回地址描述
        contss = '<div style="display: inline-block;width:313px">\n' +
            '          <ul class="technician-tc">\n' +
            '            <li>服务内容：<?php echo $data['service_content'] ?><li>\n'+
            '            <li>服务时间：<?php echo $data['plan_time'] ?></li>\n' +
            '            <li>服务地址:<?php echo $data['address'] ?><li>\n' +
            '         </ul>\n' +
            '     </div>';
        technician.content =contss;
        technician.on('click', markerClick);//图标点击事件
    }
    function markerClick(e) {
        technicianInfo.setContent(e.target.content);
        technicianInfo.open(map, e.target.getPosition());
    }

    var technicianInfo = new AMap.InfoWindow({offset: new AMap.Pixel(16, -45)});

</script>
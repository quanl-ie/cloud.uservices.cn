<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
$this->title = '新建订单';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">新建订单</span>
</div>
<!--引入CSS-->
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/webuploader/webuploader.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jquery.autocomplete.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time();?>">
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo Url::to('/css/jquery.pagination.css');?><!--?v=--><?php //echo time();?><!--">-->
<!--引入JS-->
<?=Html::jsFile('@web/webuploader/webuploader.js')?>
<?=Html::jsFile('@web/js/bootstrap-select.js')?>
<?//=Html::jsFile('@web/js/jquery.pagination.min.js')?>
<?=Html::jsFile('@web/webuploader/init.js')?>
<style>
    .datexz-wap{
        top: 37px;
    }
    .popover{max-width: 500px !important;}
    ul{margin-bottom: 0}
    .field-orderform-fault_img{margin-top: 15px}
    .input-group{margin-left: 17px;margin-bottom: 0px;}
    .radio-list{margin-top: 6px;width: auto;margin-right: 20px;float: left;}
    .radio-list>label{font-weight: 500;}
    .nodata-btn-orange{width: 120px;margin-right: 20px;}
    .layui-layer-setwin .layui-layer-close2,.layui-layer-setwin .layui-layer-close2:hover{right: 15px;width: 20px;height: 20px;}
    .city3_fix{width: 120px;}
    .trHidden{display:none;}
    .multi-img-details{margin-bottom: 0;margin-top: 0;}
    .help-block{margin:0;}
    .viewProductBag{clear: both; margin-bottom: 10px;display: block;}
    .block-h5{border-bottom: 1px solid #eee;margin-bottom: 20px;font-weight: normal;}
    .form-control{font-size: 12px;}
    .search-result-list {
        padding: 10px 0;
    }

    .search-result-list + .tab-nav {
        margin-top: 0;
    }

    .search-result-list,
    .search-result-list > ul,
    .search-result-list > ul > li {
        position: relative;
        overflow: hidden;
        clear: both;
        zoom: 1;
    }

    .search-result-list > p {
        margin: 0 0 4px;
    }

    .search-result-list > ul > li {
        border: 1px solid #ddd;
        border-bottom: none;
        padding-left: 15px;
    }

    .search-result-list > ul > li:last-child {
        border-bottom: 1px solid #ddd;
    }

    .search-result-list .radio-list {
        margin: 0;
        height: 31px;
        line-height: 31px;
    }
</style>
<section id="main-content">
    <div class="panel-body">
        <div style="margin:25px 52% 0 15px;">
            <h5 class="block-h5 block-h5one" style="margin-bottom: 0;">用户信息</h5>
        </div>
        <div class="left-content-box">
            <?php
            $form = ActiveForm::begin([
                'options' => [
                    'class' => 'form-horizontal',
                    'id' => 'form2',
                    'name' => 'form1',
                    'enctype' => 'multipart/form-data',
                ],
                'fieldConfig' => [
                    'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"addinput-list  left-float\">{input}{error}</div>",
                    'labelOptions' => ['class' => 'name-title-lable'],
                ]
            ]);
            ?>
            <p class="top-block-hint">
                <i class="red-hint-i"></i>
                <span> 新建订单前，请确保已完成品牌、产品类目、服务类型的配置。</span>
            </p>
           
            <div class="information-list field-workorder-account_id">
                <label class="name-title-lable" for="workorder-account_id">
                    <i class="red-i">*</i> 客户：
                </label>
                <span class="c_red"></span>
                <?php if($type == 1):?>
                    <?php if($contract_info):?>
                        <div class="addinput-list">
                            <span  class="form-control" style="border: 0;"><?php echo $contract_info['info']['account']." / ".$contract_info['info']['mobile']."（".$contract_info['info']['department_name']."）";?></span>
                            <input type="hidden" name="account_id" class="form-control" id="account_id" value="<?php echo $contract_info['info']['account_id']?>">
                            <p class="misplacement" id="account_msg"></p>
                        </div>
                    <?php endif;?>
                <?php else:?>
                    <div class="addinput-list">
                        <input type="text" name="" class="form-control" maxlength="30" id="account_name" <?php if($data):?> value="<?=$data['account']['account_name']?>" <?php endif;?>>
                        <input type="hidden" name="account_id" class="form-control" id="account_id" <?php if($data):?> value="<?=$data['account']['id']?>" <?php endif;?> accept="">
                        <input type="hidden" id="ufw_id" value="">
                        <?php if(in_array('/account/add',$selfRoles)){?>
                            <span class="btn btn-success  " style=" color:#FFF;float:right;font-size:14px; " id="addAccount"  data-toggle="modal" data-target="#scrollingModal">添加</span>
                        <?php }?>
                        <p class="misplacement" id="account_msg"></p>
                    </div>
                <?php endif;?>
            </div>

            <div class="information-list field-workorder-account_id">
                <label class="name-title-lable" for="workorder-account_id">
                    <i class="red-i">*</i> 联系地址：
                </label>
                <span class="c_red"></span>
                <?php if($type == 1):?>
                    <div class=" addinput-list">
                        <select type="text" class="form-control" name="address_id" id="account_address">
                            <?php if($address):?>
                                <option value="0">请选择</option>
                            <?php else: ?>
                                <option value="0">请先选择客户</option>
                            <?php endif;?>
                            <?php foreach ($address as $k => $v):?>
                                <option <?php if($data['address_id'] == $k):?> selected="selected" <?php endif;?> value="<?=$k?>"><?=$v?></option>
                            <?php endforeach;?>
                        </select>
                        <p class="misplacement"  id="address_msg"></p>
                    </div>
                <?php else:?>
                    <div class=" addinput-list">
                        <select type="text" class="form-control" name="address_id" id="account_address">
                            <?php if($address):?>
                                <option value="0">请选择</option>
                            <?php else: ?>
                                <option value="0">请先选择客户</option>
                            <?php endif;?>
                            <?php foreach ($address as $k => $v):?>
                                <option <?php if($data['address_id'] == $k):?> selected="selected" <?php endif;?> value="<?=$k?>"><?=$v?></option>
                            <?php endforeach;?>
                        </select>
                        <?php if(in_array('/account-address/add',$selfRoles)){?>
                            <span class="btn btn-success  " style=" color:#FFF;float:right;font-size:14px; " id="addAccountAddres"  data-toggle="modal" data-target="#scrollingModal" title="<?php if(isset($data['account']['id'])) echo $data['account']['id'];?>">添加</span>
                        <?php }?>
                        <p class="misplacement"  id="address_msg"></p>
                    </div>
                <?php endif;?>
            </div>
            <input type="hidden" id="order_type" value="<?php echo $type; ?>">
            <?php if($type == 1):?>
                <div class="information-list field-workorder-account_id">
                    <label class="name-title-lable" for="workorder-account_id">
                        <i class="red-i">*</i> 关联合同：
                    </label>
                    <span class="c_red"></span>
                    <?php if($contract_info):?>
                        <div class="col-lg-5 col-md-5 col-sm-5  addinput-list">
                            <span class="form-control" style="border: 0;"><a target="_blank" href="/contract/detail?id=<?php echo $contract_info['info']['id']?>"><?php echo $contract_info['info']['no'];?></a></span>
                            <input type="hidden" name="contract_id" id="contract_id" value="<?php echo $contract_info['info']['id']?>">
                            <p class="misplacement" id="account_msg"></p>
                        </div>
                    <?php endif;?>
                </div>
            <?php endif;?>
            <div class="col-md-12">
                <h5 class="block-h5 ">服务信息</h5>
            </div>
            <div class="information-list field-workorder-account_id">
                <label class="name-title-lable" for="workorder-account_id">
                    主题名称：
                </label>
                <?php if($type == 1):?>
                    <?php if($contract_info):?>
                        <div class="addinput-list">
                            <input type="text" class="form-control" value="<?php echo $contract_info['info']['subject']?>" id="theme_name" maxlength="30" placeholder="请输入订单主题（30字以内）">
                        </div>
                    <?php endif;?>
                <?php else:?>
                    <div class="addinput-list">
                        <input type="text" class="form-control" id="theme_name" maxlength="30" placeholder="请输入订单主题（30字以内）">
                    </div>
                <?php endif;?>
            </div>
            <div class="information-list field-workorder-account_id height-auto" style="margin-top: 10px">
                <label class="name-title-lable" for="workorder-account_id">
                    <i class="red-i">*</i> 服务类型：
                </label>
                <span class="c_red"></span>
                <?php if($type == 1):?>
                    <div class="addinput-list margin-right0">
                        <ul class="radio-li-boss"  style="padding-bottom: 12px;">
                            <?php if($serviceType){?>
                                <?php foreach($serviceType as $val):?>
                                    <div class="radio-list">
                                        <input type="radio" name="service_type" value="<?php echo $val['id'];?>"  id="type<?php echo $val['id'];?>" <?php if($val['id']==$data['work_type']){ echo "checked='checked'";}?>>
                                        <label for="type<?php echo $val['id'];?>"><?php echo $val['title'];?></label>
                                    </div>
                                <?php endforeach;?>
                            <?php }else{?>
                                <span class="btn btn-success"  id="addParentIframe">添加服务类型</span>
                            <?php }?>

                        </ul>
                        <input type="hidden" name="work_type" value="<?php if(isset($data['work_type'])) echo $data['work_type'] ?>" class="determined-val" id="service_type">
                        <p class="misplacement"  style="bottom: -10px" id="service_type_msg"></p>
                    </div>
                <?php else:?>
                    <div class="addinput-list left-float margin-right0" style="margin-bottom: 0px;">
                        <?php if($serviceType){?>
                            <?php foreach($serviceType as $val):?>
                                <div class="radio-list">
                                    <input type="radio" name="service_type" value="<?php echo $val['id'];?>"  id="type<?php echo $val['id'];?>">
                                    <label for="type<?php echo $val['id'];?>"><?php echo $val['title'];?></label>
                                </div>
                            <?php endforeach;?>
                        <?php }else{?>
                            <span class="btn btn-success"  id="addParentIframe">添加服务类型</span>
                        <?php }?>
                        <input type="hidden" name="work_type" value="<?php if(isset($data['work_type'])) echo $data['work_type'] ?>" class="determined-val" id="service_type">
                        <p class="misplacement bottom-20"  id="service_type_msg"></p>
                    </div>
                <?php endif;?>
            </div>
            <?php if($serviceWorkStage){?>
                <div class="information-list field-workorder-account_id field-flow" <?php if($type != 1){ echo 'style="display: none;height:auto"';}?>>
                    <label class="name-title-lable" for="workorder-account_id">
                        <i class="red-i">*</i> 服务流程：
                    </label>
                    <span class="c_red"></span>
                    <div class="addinput-list">
                        <div id="tbodyContent" style="text-align: left">
                            <?php foreach($serviceWorkStage as $key=>$val){?>
                                <div class="radio-list">
                                    <input type="radio" name="work_stage" value="<?php echo $val['id'];?>"  id="work_stage<?php echo $key;?>">
                                    <label for="work_stage<?php echo $key;?>"><?php echo $val['title'];?></label>
                                </div>
                            <?php }?>
                        </div>
                        <input type="hidden" name="work_stage" value="<?php if(isset($data['work_stage'])) echo $data['work_stage'] ?>" class="determined-val" id="work_stage">
                        <p class="misplacement"  style="bottom: -55px" id="work_stage_msg"></p>
                    </div>
                </div>
            <?php }?>
            <div class="information-list field-workorder-account_id padding-top10 height-auto">
                <label class="name-title-lable" for="workorder-account_id">
                    <i class="red-i">*</i> 服务产品：
                </label>
                <span class="c_red"></span>
                <?php if($type == 1):?>
                    <?php if($contract_info):?>
                        <?php foreach ($contract_info['detail'] as $val):  ?>
                            <div class="addinput-list add-product-list">
                                <div class="padding-left0 ">
                                    <select type="text" class="form-control" name="sale_order_id" id="account_service_brand">
                                        <option value="<?= $val['sale_order_id']?>">
                                            <?php if($val['model'] != ''):?>
                                                <?= $val['model']."/"?>
                                            <?php endif;?>
                                            <?php if($val['prod_name'] != ''):?>
                                                <?= $val['prod_name']?>
                                            <?php endif;?>
                                            <?php if($val['brand_name'] != ''):?>
                                                <?= '/'.$val['brand_name']?>
                                            <?php endif;?>
                                    </select>
                                    <p class="misplacement" id="brand_msg"></p>
                                </div>
                                <div  class="padding-right0 product-jia">
                                    <i class="icon1-cha"></i>
                                    <input type="text" class="form-control padding-right0" id="productIpt1" value="<?= $val['prod_num']?>" maxlength="9" onkeyup="clearNoNum(this)">
                                    <span class="unit unit1"><?= $val['unit_name']?></span>
                                    <p class="misplacement">请填写数量</p>
                                </div>
                                <a href="javascript:" class="delete-a" style="right: 30px;" data-id="'+number+'">移除</a>
                                <?php if($val['prod_type'] == 3):?>
                                    <a href="javascript:" class="viewProductBag" id-data="<?= $val['prod_id'] ?>">查看物料详情</a>
                                <?php endif;?>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                <?php else:?>
                    <div class="addinput-list add-product-list">
                        <div class=" padding-left0 ">
                            <select type="text" class="form-control" name="sale_order_id" id="account_service_brand">
                                <?php if($service):?>
                                    <option value="0">请选择</option>
                                <?php else: ?>
                                    <option value="0">请先选择客户及服务类型</option>
                                <?php endif;?>
                                <?php foreach ($service as $k => $v):?>
                                    <option <?php if($data['detail']['data']['workDetail'][0]['sale_order_id'] == $k):?> selected="selected" <?php endif;?> value="<?=$k?>"><?=$v?></option>
                                <?php endforeach;?>
                            </select>
                            <p class="misplacement" id="brand_msg"></p>
                        </div>
                        <div  class="padding-right0 product-jia">
                            <i class="icon1-cha"></i>
                            <input type="text" class="form-control padding-right0" id="productIpt1" value="1" maxlength="9" onkeyup="clearNoNum(this)">
                            <span class="unit unit1"></span>
                            <p class="misplacement">请填写数量</p>

                        </div>
                        <div class="add-btn">
                            <?php if(in_array('/sale-order/add',$selfRoles)){?>
                                <span class="btn btn-success"  style="margin-left: 20px; " id="addAccountBrand"  data-toggle="modal" data-target="#scrollingModal"  title='<?php if(isset($data['account_id'])) echo $data['account_id'];?>'>添加</span>
                            <?php }?>
                        </div>
                        <?php if(in_array('/account/view',$selfRoles)){?>
                            <div class="record">
                                <!-- <p style="margin-bottom: 5px;">服务记录</p> -->
                                <!-- <ul>
                                    <li><a href="javascript:">记录1</a></li>
                                    <li><a href="javascript:">记录2</a></li>
                                    <li><a href="javascript:">记录3将js放到html Dom节点下。先绘制节点，然后在执行js就不会出现此问题将js放到html Dom节点下。先绘制节点，然后在执行js就不会出现此问题将js放到html Dom节点下。先绘制节点，然后在执行js就不会出现此问题</a></li>
                                </ul>
                                <a href="javascript:" class="see-more">查看更多</a> -->
                            </div>
                        <?php };?>

                    </div>
                    <div class="information-list field-workorder-account_id " id="judgeBox" style="height: auto;">
                        <div class="addinput-list">
                            <div id="addProductBox">
                                <!-- <div class="add-product-list">
                            <div class="col-xs-7 col-md-9 padding-left0">
                                <select type="text" class="form-control" name="sale_order_id" id="account_service_brand">
                                    <?php if($service):?>
                                    <option value="0">请选择</option>
                                    <?php else: ?>
                                    <option value="0">请先选择客户</option>
                                    <?php endif;?>
                                    <?php foreach ($service as $k => $v):?>
                                    <option <?php if($data['detail']['data']['workDetail'][0]['sale_order_id'] == $k):?> selected="selected" <?php endif;?> value="<?=$k?>"><?=$v?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div  class=" col-md-3 padding-right0 product-jia">
                                <i class="icon1-cha"></i>
                                <input type="number" class="form-control padding-right0">
                                <span class="unit">台</span>
                            </div>
                            <a href="javascript:" class="delete-a">移除</a>
                        </div> -->
                            </div>
                            <a href="javascript:" id="addProductList">
                                <i class="icon2-jia"></i>
                            </a>
                        </div>
                    </div>
                <?php endif;?>

            </div>



            <!-- style="display: <?php if($serviceWorkStage){echo 'block' ;}else{echo 'none';}?>" -->


            <div class="information-list field-workorder-account_id" id="qualityState" style="height: 25px;margin-top: 10px;">
                <label class="name-title-lable" for="workorder-account_id">
                    <i class="red-i">*</i> 质保状态：
                </label>
                <span class="c_red"></span>
                <div class="addinput-list left-float">
                    <?php foreach ($amount_type as $k => $v):?>

                        <div class="radio-list">
                            <input type="radio" name="amount_type" value="<?=$k;?>"  id="amount_type<?=$k;?>">
                            <label for="amount_type<?=$k;?>"><?=$v;?></label>
                        </div>
                    <?php endforeach;?>
                    <p class="misplacement bottom-20"  id='amount_msg'></p>
                </div>

            </div>


            <div class="information-list field-workorder-account_id">
                <label class="name-title-lable" for="workorder-account_id">
                    <i class="red-i">*</i> 服务时间：
                </label>
                <div class="addinput-list">
                    <div class="equable-box">
                        <div class="time-equable">
                            <input type="text"  class="form-control  planTime" id="selectDate" name="selectDate" value="<?php if(isset($data['plan_time'])) echo date("Y-m-d H:i:s",$data['plan_time'])?>"  onclick="selectDateDay()" placeholder="请选择服务时间" readonly>
                            <i class="icon2-rili riqi1" id="rili" style="right: 5px;" onclick="selectDateDay()" ></i>
                        </div>
                        <div class="time-equable">
                            <!-- 时间插件 -->
                            <select name="plan_time_type" id="plan_time_type" class="plan_time_type form-control">
                                <option value="0">请选择</option>
                                <?php foreach($setTimeType as $kk=>$vv) :?>
                                    <option value="<?=$kk?>"><?=$vv?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="time-equable">
                            <!--选择具体时间时才显示setTime这个框-->
                            <div id="show_setTime" style="display: none">
                            </div>
                        </div>
                        <div class="city23_fix"></div>
                    </div>
                    <p class=" misplacement" id='plan_time_msg' style="bottom: -10px;"></p>
                </div>
            </div>
            <!--故障图片 -->
            <div  class="information-list" style="clear: both;" id="faultImg">
                <?= $form->field($model, 'fault_img')->widget('manks\FileInput', [
                        'clientOptions'=>[
                            'server' => Url::to('../upload/upload?source=fws'),
                            'pick'=>[
                                'multiple'=>true,
                            ],
                            'fileNumLimit'=>'5',
                            'duplicate'=>true,
                            'fileSingleSizeLimit'=>'2097152', //限制上传大小为2M
                        ]
                    ]
                ); ?>
            </div>
            <div class="information-list field-workorder-account_id height-auto margin-top0" style="padding-top: 20px;">
                <label class="name-title-lable" for="workorder-account_id">
                    备注：
                </label>
                <div class="addinput-list">
                    <textarea class="ramark-box" id="remarks" maxlength="200" placeholder="请填写客户的服务需求" name="description" id="description"><?php if(isset($data['description'])):?><?=$data['description']?><?php endif;?></textarea>
                    <p class="xianzhi"><span  id='word'>0</span>/200</p>
                </div>
            </div>

            <div class="information-list field-workorder-account_id" style="height: auto;display: none;" id="dismantleOrder">
                <label class="name-title-lable" for="workorder-account_id">
                    拆单：
                </label>
                <div class="addinput-list">
                    <table>
                        <tr>
                            <th>产品</th>
                            <th width="120px">分单</th>
                        </tr>
                        <tbody id="dismantleOrderTbody">
                        <!-- <tr>
                            <td>产品*5台</td>
                            <td>
                                <select name="order" class="dismantle-order-select" id="dismantleOrder1">
                                    <option value="2">订单1</option>
                                </select>
                            </td>
                        </tr> -->
                        <tr class="tr1"><td class="td1">请选择服务产品</td><td  class="td2"><select name="order" class="dismantle-order-select" id="dismantleOrder1"><option value="2">订单1</option></select></td></tr>
                        </tbody>
                    </table>
                    <div style="text-align: right"><a id="toggleCDBtn" style="display: none;" href="javascript:void(0);">展开</a></div>
                </div>
            </div>
            <div class="information-list field-workorder-account_id" style="margin-top: 10px;">
                <?php if(in_array('/order/main-assigin',$selfRoles)){?>
                    <label class="name-title-lable" for="workorder-account_id">
                        指派：
                    </label>
                    <div class="addinput-list left-float" style="margin-bottom: 30px;width: calc(100% - 190px)">
                        <div>
                            <div class="radio-list">
                                <input type="radio" value="1" name="assign" id="assign1" class="pr1" data-id="<?php echo isset($DepartmentInfo['id'])?$DepartmentInfo['id']:''?>"><label for="assign1">机构</label>
                            </div>
                            <div class="radio-list">
                                <input type="radio" value="2" name="assign" id="assign2" class="pr1"><label for="assign2">技师</label>
                            </div>
                        </div>
                        <p class=" misplacement" id='plan_assign_msg'>错误提示</p>
                        <div class="drop-dw-layerbox" style="position: static;">
                            <ul id="level-tree">
                            </ul>
                        </div>
                    </div>
                <?php }?>
            </div>

            <div class="information-list" style="height: auto;"><hr></div>
            <div class="information-list" style="text-align: center;">
                <a href="/order/index"><button type="button"  class="btn bg-f7 mgr20">返回</button></a>
                <input type="button" class="btn btn-orange btnSubmit"  value="提交"  />
                <input type="button" class="btn btn-orange  btnSubmiting" style="display: none; " value="提交中..."  />

            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="right-content-box">
            <div class="top-search">
                <input type="text" placeholder="请输入智邦客户的联系电话" id="zb_search_mobile">
                <!-- <input type="button" value="" id="zbs" style="width: 80px;" > -->
                <i class="icon2-sousuo" id="zbs"></i>
            </div>
            <div class="search-result-list" id="select_account" style="display: none;">

            </div>
            <div class="tab-nav">
                <ul id="tabNav">
                    <li class="active" data-url="tabClient">客户</li>
                    <li data-url="tabTalk">洽谈</li>
                    <li  data-url="tabContract">合同</li>
                    <li data-url="afterSaleBox">售后</li> 
                </ul>
            </div>
            <!--  -->
            <div class="tab-content" style="display: none;">
                <!-- 客户 -->
                <div id="tabClient" class="tab-content-list" style="display: block;">
                    <table id="account_info"></table>
                    <h5 id="pxx" style="display: none;">联系人信息</h5>
                    <table id="preson"></table>
                </div>
                <!-- 洽谈 -->
                <div id="tabTalk"  class="tab-content-list">
                    <table id="reply"></table>
                </div>
                <!-- 合同 -->
                <div id="tabContract" class="tab-content-list" style="border-bottom:1px solid #ddd"></div>
               <!-- <div class="box">
                    <div id="pagination2" class="page fl"></div>
                </div>-->
                <!-- 售后 -->
                <div  class="tab-content-list" id="afterSaleBox">
                    <table id="after_sale"></table>
                </div>
            </div>
            <div class="no-data-zhibang" id="zhibangNodata">
                <i class="icon2-chakan-2" style="font-size: 40px;color: #ccc;"></i>
                <p style="color: #666;margin-top: 15px;">未找到相关客户，请重新填写搜索信息后再次尝试</p>
          
            </div>
        </div>
    </div>
</section>
<!-- 时间插件/ -->
<?=Html::jsFile('@web/js/WdatePicker/WdatePicker.js')?>
<?=Html::jsFile('@web/js/WdatePicker/time.js')?>
<script type="text/javascript" src="/js/jquery-migrate-1.1.0.js"></script>
<script src="/js/jquery.autocomplete.js" type="text/javascript"></script>
<script>

    var addJudge = 1,reload=2;
    window.onload = function(){
        selectGray ();
        //mechanismInit();
    }
    function mechanismInit(addressId) {
        var pid        = $('#assign1').attr('data-id');
        var addressId  = $("#account_address").val();  //客户地址id
        var ulData = '';
        $.getJSON('/organization/ajax-get-department',{'pid':pid,"total_work":1,"service_area":1,'address_id':addressId,'service_area':1},function (json) {
            var data = json.data;
            for (item in data){
                ulData +="<li><span>";
                data[item].sub==1?ulData+= '<i class="icon2-shixinyou tree-i"  onclick="clickCategory(this)"></i>':'';
                ulData+='<input type="radio" value="'+data[item].id+'" name="service_depart_id" id="jg'+data[item].id+'" class="pr1"><label for="jg'+data[item].id+'">'+data[item].name+'</label></span></li>';
            }
            $('#level-tree').html(ulData);
        })

    }

    // 字数限制
    $("#remarks").keyup(function(){
        if($("#remarks").val().length > 200){
            $("#remarks").val( $("#remarks").val().substring(0,200) );
        }
        $("#word").text( $("#remarks").val().length ) ;
    });
    // 未选择select字灰色
    function selectGray () {
        var unSelected = "#999";
        var selected = "#333";
        $(document).find("select").each(function(i) {
            if($(this).val() == "0"||$(this).val() == ""||$(this).val() == 0){
                $(this).css("color", unSelected);
                $(this).find("option").css("color",unSelected);
            }
            $(this).find('option').each(function(i) {
                if($(this).val()==''||$(this).val()=='0'){
                     $(this).css("color", unSelected);
                    $(this).prop('disabled',true);
                }else{
                    $(this).css("color", selected);
                }

            });
        });

    }
    $(document).on('click','select',function () {
            if($(this).val() == "0"||$(this).val() == ""||$(this).val() == 0){
                $(this).css('color','#999');
                $(this).find("option").css("color",'#999');
            }else{
                $(this).css('color','#333');
                $(this).find("option").css("color",'#333');
            }
            $(this).find('option').each(function(i) {
                if($(this).val()==''||$(this).val()=='0'){
                    $(this).css("color", '#999');
                    $(this).prop('disabled',true);
                }else{
                    $(this).css("color", '#333');
                }

            });
    });
    //添加服务类型
    $("#addParentIframe").click(function () {
        layer.open({
            type: 2,
            title: '请选择服务类类型',
            area: ['400px', '250px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/type/add', 'no'],
            end:function(){
                location.reload();
            }
        });
    });
    //单项选择
    $('.radio-li').on('click',function() {
        $(this).find('span').css({'background':'#2693ff','color':'#fff'});
        $(this).siblings('li').find('span').css({'background':'#fff','color':'#333','border-color':'#ccc'});
        $(this).parent('ul').next('input').val($(this).find('span').attr('id'));
    })
    // 选择
    $('.xiangxia').on('click',function() {
        $('.addinput-list-data').hide();
        $(this).next('.addinput-list-data').show();
    })
    //
    $('.addinput-list-data li').on('click',function() {
        $('.addinput-list-data').hide();
        $(this).parent('ul').siblings('.form-control').val($(this).attr('id '));
    })

    //添加客户
    var iframeSrc = '';
    $("#addAccount").click(function () {
        iframeSrc = 'order';
        layer.open({
            type: 2,
            title: '添加客户',
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/account/add/'
        });
    });
    //添加客户地址
    var iframeCallbackAddressId = 0;
    $("#addAccountAddres").click(function() {
        var id = $(this).attr('title');
        if(id==null || id==undefined || id=='' || isNaN(parseInt(id))) {
            $("#address_msg").text("请先选择客户");
            $("#address_msg").show();
        }else {
            layer.open({
                type: 2,
                title: '添加客户地址',
                area: ['700px', '450px'],
                fixed: false, //不固定
                maxmin: false,
                content: '/account-address/add?id='+id,
                end  : function(){
                    account_address(id)
                }
            });
            account_address(id)
        }
    });
    //添加服务产品
    var iframeCallbackSaleOrderId = 0;
    var callbackJudge = 0;
    $("#addAccountBrand").click(function() {
        var id = $(this).attr('title');
        // if(id==null || id==undefined || id=='' || isNaN(parseInt(id))) {

        // }
        if($('#account_id').val()!=""&&proving($('input[name="service_type"]',false))){
            layer.open({
                type: 2,
                title: false,
                area: ['700px', '400px'],
                fixed: false, //不固定
                maxmin: false,
                content: '/sale-order/add?accountId='+id+'&source=1',
                end  : function(){
                    if($('.add-product-list').length == 0){
                        service_brand(id);
                    }else if($('#account_service_brand').find('option').length<2){
                        service_brand(id);
                    }else if($('#account_service_brand').find('option:checked').attr('data-intent')==callbackJudge){
                        $('#addProductList').click();
                    }else{
                        $('#addProductList').click();
                    }
                    // service_brand(id);
                    // $('#account_service_brand').val()
                    // $.ajax({
                    //     url: '/order/get-sale-info',
                    //     type: 'POST',
                    //     dataType: 'json',
                    //     data: {account_id:$('#account_id').val(),type_id:$('input[name="service_type"]:checked').val()},
                    //     success:function(res) {
                    //         console.log(res);
                    //        $.each(res.data,function(index, el) {
                    //            console.log(index);
                    //            console.log(el);
                    //        });
                    //     }
                    // })
                    //$('#addProductList').parents('#judgeBox').show();
                    //$('#addProductList').show();
                }
            });
            // service_brand(id)
        }else{
            $("#brand_msg").text("请先选择客户及服务类型");
            $("#brand_msg").show();
        }
    });


    //模糊查询选中
    var accountId = 0;
    function accountName(id) {

        $("#brand_msg").hide();
        $("#address_msg").hide();
        $("#provide_msg").hide();

        accountId = id;
        $("#account_id").val(id);
        $("#addAccountAddres").attr('title',id);
        $("#addAccountBrand").attr('title',id);
        $("#account_select").hide();
        account_address(id);
        var type = "<?php echo $type;?>";
        if(type != 1){
            var type_id = $('input[name="service_type"]:checked').val();
            if(type_id != '' && type_id != null){
                service_brand(id);
            }
        }

    }
    //查询客户对应的地址信息
    function account_address(account_id) {
        $.post('/order/get-address',{account_id:account_id},function(res) {
            $("#account_address").html(res);
            if($("#account_address").val()=="0"||($("#account_address").val()=="")){
                $("#account_address").css('color','#999')
            }else{
                $("#account_address").css('color','#333')
            }
            setTimeout(function () {
                $("#account_address option:eq(1)").prop('selected',false);
            },500);
            if(iframeCallbackAddressId>0){
                $("#account_address").val(iframeCallbackAddressId);
            }
        })
    }
    //查询客户对应服务产品
    function service_brand(account_id) {
        var serverTypeId = $('input[name="service_type"]:checked').val();
        var type_id = 0;
        if(serverTypeId == <?php echo Yii::$app->params['chaidan']['workType'];?>){
            type_id = 1;
        }else{
            type_id = 2;
        }
        $.post('/order/get-service-brand',{account_id:account_id,type_id:type_id},function(res) {
            $("#account_service_brand").html(res);
            if(iframeCallbackSaleOrderId > 0 ){
                $("#account_service_brand").val(iframeCallbackSaleOrderId);
                $("#account_service_brand").change();
            }
            setTimeout(function () {
                if($("#account_service_brand").find('option').length <=2){
                    $('#addProductList').hide();
                    $('#addProductList').parents('#judgeBox').hide();
                }
                else {
                    $('#addProductList').show();
                    $('#addProductList').parents('#judgeBox').show();
                }
            },300);
        })
    }

    //地址坐标修正后调用
    function doSubmit(){
        $(".btnSubmit").click();
    }

    $(".btnSubmit").click(function(e){
        if(checkAll())
        {
            var type = $("#order_type").val();
            var serverTypeId = $('input[name="service_type"]:checked').val();
            var data = {
                account_id:$('#account_id').val(),//客户
                address_id:$('#account_address').val(),//服务地址
                subject_name:$('#theme_name').val(),//主题名称
                contract_id:$('#contract_id').val(), //合同id
                work_type:serverTypeId,//服务类型
                is_scope:serverTypeId!=<?php echo Yii::$app->params['chaidan']['workType'];?>? $('input[name="amount_type"]:checked').val():'',//质保状态
                selectDate:$('#selectDate').val(),//服务时间
                plan_time_type:$('#plan_time_type').val(),//中间时间
                setTime:$('select[name="setTime"]').val(),//具体时间
                description:$('#remarks').val(),//备注
                OrderForm:[],//故障图片
                assign:$('input[name="assign"]:checked').val(),//指派
                orderList:[],
                service_depart_id:$('input[name="service_depart_id"]:checked').val(),//机构
                work_stage:$('input[name="work_stage"]:checked').val(),//服务流程
                judge:1,//1不拆单，2拆单 判断是否拆单
                order_type:type,//合同派单

            }
            var orderAraay = [],newOrderAraay = [],oderAraay1=[];
            if($('input[name="service_type"]:checked').val()=='<?php echo Yii::$app->params['chaidan']['workType'];?>'){
                $('#dismantleOrderTbody tr').each(function(i) {
                    var groupNum = $(this).find('.dismantle-order-select').val();
                    var productId = $(this).data('name');
                    var productNum = $(this).data('number');
                    var obj = {'product_id':productId,'num':productNum};
                    if(typeof(orderAraay[groupNum] )== 'undefined'){
                        orderAraay[groupNum] = [];
                    }
                    orderAraay[groupNum].push(obj);
                });
                for(i=0; i<orderAraay.length;i++)
                {
                    if(typeof(orderAraay[i]) != 'undefined'){
                        newOrderAraay.push( orderAraay[i]);
                    }
                }
                data.orderList=newOrderAraay;
                data.OrderForm = [];
                data.judge = 2;
            }
            else
            {
                $('.add-product-list').each(function(i,el) {
                    var obj = {
                        product_id:$(this).find('select[name="sale_order_id"]').val(),
                        num:$(this).find('.product-jia input').val()
                    }

                    oderAraay1.push(obj);
                })
                $('input[name="OrderForm[fault_img][]"]').each(function(i) {
                    data.OrderForm.push($(this).val())
                })

                data.orderList=oderAraay1
                data.judge = 1
            }

            //检查地址坐标是否有
            var addressId = $('#account_address').val();
            $.getJSON('/account-address/get-coordinate',{address_id:addressId},function(json){
                 if(json.status == 0){
                     layer.open({
                         type: 2,
                         title: '修正客户地址坐标',
                         area: ['700px', '450px'],
                         fixed: false, //不固定
                         maxmin: false,
                         scrollbar:false,
                         content: '/account-address/change-coordinate?id='+addressId
                     });
                 }
                 else
                 {
                     $.ajax({
                         url:'/order/add',
                         type:'POST',
                         data:data,
                         dataType:'json',
                         success:function(el) {
                             if(el.success){
                                 alert('添加成功');
                                 var jumpUrl = el.data.url;
                                 setTimeout(location.href = jumpUrl,1000)
                             }else{
                                 alert("订单产品数量不能大于合同产品数量");
                                 $('.btnSubmit').show();
                                 $('.btnSubmiting').hide();
                             }
                         }
                     })
                     $('.btnSubmit').hide();
                     $('.btnSubmiting').show();
                 }
            });
        }
        else {
            $('.btnSubmit').show();
            $('.btnSubmiting').hide();
        }
        return false;
    });

    //验证
    function checkAll()
    {
        //验证客户是否选中
        var flag = true;
        var account_id               = $("#account_id").val();//客户
        var account_name               = $("#account_name").val();//客户
        var account_address          = $("#account_address").val();//地址
        var tbodyContent             =$("#tbodyContent .radio-list").length; //获取服务流程
        var assign1                  =$('#assign1');//判断是否选择机构
        //验证客户
        if(account_id.length==0 || isNaN(parseInt(account_id))){
            flag = false;
            $("#account_msg").text("请先选择客户");
            $("#account_msg").show();
        }
        if(account_name == null || account_name == undefined || account_name == '' ){
            flag = false;
            $("#account_msg").text("请先选择客户");
            $("#account_msg").show();
        }
        //验证客户地址A==null||A!=undefined||A==""
        if(account_address==null || account_address==undefined || account_address=='' || isNaN(parseInt(account_address))){
            flag = false;
            $("#address_msg").text("请先选择客户地址");
            $("#address_msg").show();
        }
        //验证服务产品
        $('.add-product-list').each(function(i) {
            if($(this).find('select[name="sale_order_id"]').val()==""||$(this).find('select[name="sale_order_id"]').find('option:selected').val()=="请选择"){
                flag = false;
                $(this).find('select[name="sale_order_id"]').next('p').text('请选择服务产品');
                $(this).find('select[name="sale_order_id"]').next('p').show();
            }
            if($(this).find('.product-jia input').val()<1||$(this).find('.product-jia input').val()==""){
                flag = false;
                $(this).find('.product-jia .misplacement').text('数量不能小于1');
                $(this).find('.product-jia .misplacement').show();
            }
        })
        //验证服务类型
        if(proving($('input[name="service_type"]'),true)){
            flag = false;
            $("#service_type_msg").text("请选择服务类型");
            $("#service_type_msg").show();
        }
        //验证质保
        if(proving($('input[name="amount_type"]'),true)&&$('input[name="service_type"]:checked').val()!=<?php echo Yii::$app->params['chaidan']['workType'];?>){
            flag = false;
            $("#amount_msg").text("请选择质保状态");
            $("#amount_msg").show();
        }
        if(tbodyContent >0){
            //验证服务流程
            if(proving($('input[name="work_stage"]'),true)){
                flag = false;
                $("#work_stage_msg").text("请选择服务流程");
                $("#work_stage_msg").show();
            }
        }
        if (assign1.prop('checked')) {
            var serviceDepart = $('input[name="service_depart_id"]');
            var tag = false;
            serviceDepart.each(function(index, el) {
                if (this.checked) {
                    tag = true;
                }
            });
            if (!tag) {
                flag = false;
                $("#plan_assign_msg").text("请选择服务机构");
                $("#plan_assign_msg").show();
            }else{
                $("#plan_assign_msg").hide();
            }

        }
        //验证时间
        var select_date              = $("#selectDate").val();//选择日期
        var plan_time_type           = $(".plan_time_type").val();//服务时间类型
        var set_time                 = $("#setTime").val();  //选择具体时间
        if(select_date==null || select_date==undefined || select_date==''){
            $("#plan_time_msg").text("请选择日期");
            $("#plan_time_msg").show();
            return false;
        }

        //验证时间
        if(plan_time_type==0 || plan_time_type==undefined || plan_time_type==''){
            $("#plan_time_msg").text("请选择服务时间类型");
            $("#plan_time_msg").show();
            return false;
        }

        //验证时间
        if(plan_time_type==5&&(set_time==null || set_time==undefined || set_time=='')){
            $("#setTime").text("请选择具体的服务时间");
            $("#plan_time_msg").show();
            return false;
        }

        return flag;
    }

    //获取焦点提示框消失
    $("#account_name").focus(function() {
        $("#account_msg").text("");
        $("#account_msg").hide();
    })
    $("#account_address").focus(function() {
        $("#address_msg").text("");
        $("#address_msg").hide();
    })
    $("#account_service_brand").focus(function() {
        $("#brand_msg").text("");
        $("#brand_msg").hide();
    })

    $("#selectDate").click(function() {
        $("#plan_time_msg").text("");
        $("#plan_time_msg").hide();
    })
    $(".plan_time_type").click(function() {
        $("#plan_time_msg").text("");
        $("#plan_time_msg").hide();
    })
    $("#set_time").click(function() {
        $("#plan_time_msg").text("");
        $("#plan_time_msg").hide();
    })
    $(".service_type").click(function() {
        $("#service_type_msg").text("");
        $("#service_type_msg").hide();
    })
    $(".amount_type").click(function() {
        $("#amount_msg").text("");
        $("#amount_msg").hide();
    })
    // 失去焦点
    var blurAccountName = $('#account_id').val();
    $("#account_name").autocomplete("/account/sreach", {
        minChars: 1,
        matchCase:false,//不区分大小写
        autoFill: false,
        max: 10,
        dataType: 'json',
        width:$('#account_name').outerWidth(true)+'px',
        extraParams:{v:function() { return $('#account_name').val();}},
        parse: function(data) {
            return $.map(eval(data), function(row) {
                return {
                    data: row,
                    value: row.id,    //此处无需把全部列列出来，只是两个关键列
                    result: row.name
                }
            });
        },
        formatItem: function(row, i, max,term) {
            return  row.name;
        },
        formatMatch: function(row, i, max) {
            return row.name;
        },
        formatResult: function(row) {
            return row.name;
        },
        reasultSearch:function(row,v)//本场数据自定义查询语法 注意这是我自己新加的事件
        {
            //自定义在code或spell中匹配
            if(row.data.name.indexOf(v) == 0)
            {
                return row;
            }
            else
                return false;
        }
    }).result(function(event, data, formatted) { //回调
        if(data.id>0){
            if($("#account_id").val() != data.id){
                $('.record').html('');
                $('.record').hide();
                $('#addProductBox').html('');
                $('#productIpt1').val('1');
                $('#dismantleOrderTbody').html('<tr class="tr1">请选择服务产品<td class="td1"></td><td  class="td2"><select name="order" class="dismantle-order-select" id="dismantleOrder1"><option value="2">订单1</option></select></td></tr>')
            }

            accountName(data.id);
            $('#account_name').val(data.name);
        }
        else {
            $('#account_name').val('');
        }
    });

    //填充用户信息
    function fillAccount($accountId,$accountName) {
        selectGray ();
        $("#account_name").val($accountName);
        $('#addProductBox').html('');
        $('#account_id').val($accountId);
        accountName($accountId);
    }

    $("#account_address").click(function() {
        if(( $.trim($("#account_address").html()) == '')){
            $("#address_msg").text("请先选择客户");
            $("#address_msg").show();
        }
        $('.drop-dw-layerbox').hide();
    });

    $("#account_service_brand").click(function() {
        selectGray ();
        if(( $.trim($("#account_service_brand").html()) == '')){
            $("#brand_msg").text("请先选择客户");
            $("#brand_msg").show();
        }
    });
    <?php if($accountArr):?>
    fillAccount(<?php echo $accountArr['id'];?>, '<?php echo $accountArr['account_name'] . '/' . $accountArr['mobile'];?>');
    <?php endif; ?>

    //提示
    $(".tip").popover();

    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message');?>');
    <?php endif;?>

    var myDate = new Date();
    var myMonth = myDate.getMonth()<10?"0"+(myDate.getMonth()+1):myDate.getMonth();
    var myDay = myDate.getDate()<9?'0'+myDate.getDate():myDate.getDate();
    var minDate = myDate.getFullYear()+'-'+myMonth+'-'+myDay;

    function selectDateDay() {
        jeDate('#selectDate', {
            trigger:false,
            isToday:false,
            format: 'YYYY-MM-DD',
            maxDate: '2099-06-16 23:59:59',
            donefun:function(){
                $("#plan_time_type").attr('disabled',false);
                var d = new Date();
                var selectDate = new Date($('#selectDate').val());
                if(selectDate.getDate() == d.getDate())
                {
                    $('.plan_time_type').find('option').each(function(){
                        if(d.getHours() >=12 && $(this).val() == 2){
                            $(this).attr('disabled','disabled');
                            $(this).css('background','#cccccc');
                        }
                        if(d.getHours() >=18 && $(this).val() == 3){
                            $(this).attr('disabled','disabled');
                            $(this).css('background','#cccccc');
                        }
                    });
                }
                else
                {
                    $('.plan_time_type').find('option').each(function(){
                        $(this).removeAttr('disabled');
                        $(this).removeAttr('style');
                    });
                }
            }
        })
        $(".plan_time_type").val(0);
        $(".plan_time_type").attr('selected');
        $("#show_setTime").html('');
    }
    $('.setok').click(function() {
        $('#selectDate').val().length==0?console.log(1):console.log(2);
    })
    //服务时间类型为5时候，显示具体时间下拉框
    $("#plan_time_type").on('click',function(){
        var id = $(this).val();
        var selectDate = $("#selectDate").val();
        if(selectDate==''){
            $("#plan_time_msg").html('请选择日期');
            $("#plan_time_msg").css('display','block');

            $(this).attr('disabled',true);
            return false;
        }
        if(id==5){
            $.ajax({
                url:'/order/set-time',
                type:'POST',
                data:{time_type:id,date:selectDate},
                success:function (msg) {
                    $("#show_setTime").html(msg);
                }
            })
            $("#show_setTime").css('display','block');
        }else{
            $("#show_setTime").css('display','none');
        }
    })
    // 指派切换
    $('input[name="assign"]').on('change', function(event) {
        event.preventDefault();

        var _self = $(this).parent('span');
        if($(this).attr('id')=='assign1'){
            $('.drop-dw-layerbox').show();
            var addressId  = $("#account_address").val();  //客户地址id
            if(addressId != 0){
                mechanismInit(addressId);
            }
        }else{
            $('#plan_assign_msg').hide();
            $('.drop-dw-layerbox').hide();
            $('input[name="service_depart_id"]').prop('checked',false);
        }
    });
    function clickCategory(el) {
        var id = $(el).next('input').val();
        var addressId = $("#account_address").val();  //客户地址id
        var _this = $(el).parent('span');
        $.getJSON('/organization/ajax-get-department',{'pid':id,"service_area":"1","address_id":addressId},function (data) {
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>"
                $(data.data).each(function(i,el) {
                    ulData +="<li><span>";
                    el.exist==1?ulData+= '<i class="icon2-shixinyou tree-i"  onclick="clickCategory(this)"></i>':'';
                    ulData+='<input type="radio" id="a'+el.id+'" value="'+el.id+'" name="service_depart_id" >  <label for="a'+el.id+'">'+el.name+'<i class="gou-i"></i></label></span></li>';
                });
                ulData +="</ul>";
                _this.parent('li').append(ulData);
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');


            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })
    }
    // 添加服务产品
    var addAccountId = $('#account_id').val();
    var addName = [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,31,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50];
    var intent = "";
    var totalProduct = 0;
    $('#addProductList').on('click',function() {
        var productXml = '',option='',tdXml='',tdXmlOption='';
        var number = addName[0];
        var banArray = [];
        $('.add-product-list select').each(function(i) {
            // banArray.push($(this).find('option:selected').attr('sn-id'));
            banArray.push($(this).val());
        });
        var flag = true;
        if($('#account_id').val()==""){
            flag = false;
            $('#account_msg').text("请填写客户");
            $('#account_msg').show();
        }
        console.log(proving('input[name="service_type',true));
        if(proving('input[name="service_type',true)){
            flag = false;
            $('#service_type_msg').show();
            $('#service_type_msg').text('请选择服务类型');
        }
        if($('#account_service_brand').val()=="0"||$('#account_service_brand').val()==""){
            flag = false;
            $('#brand_msg').show();
            $('#brand_msg').text('请选择服务产品');
        }
        if(flag){
            $.ajax({
                url:'/order/get-sale-info',
                data:{account_id:$('#account_id').val(),intent:intent},
                type:'POST',
                dataType:'json',
                success:function(data){
                    $.each(data.data,function(i, el) {
                        option +='<option value="'+el.id+'"';
                        // el.sn_id!=null?option +='sn-id="'+el.id+el.sn_id+'"':option+='sn-id="'+el.id+'"';
                        option+='" data-name="'+el.unit+'" data-intent="'+el.intent+'"';
                        $.each(banArray,function(i,ele) {
                            // ele==el.id?option+= 'disabled="disabled"':'';
                            // var snId = el.sn_id!=null?el.id+el.sn_id:el.id;
                            ele==el.id?option+= 'style="display:none"':'';
                        });
                        option+='>'+el.value+'</option>';
                    });
                    productXml+='<div class="add-product-list"><div class="padding-left0 rem"><select type="text" class="form-control order'+number+'" data-id="'+number+'" name="sale_order_id" ><option value="0">请选择</option>'+option+'</select><p class="misplacement">请选择服务产品</p></div><div  class="padding-right0 product-jia"><i class="icon1-cha"></i><input type="text" maxlength="9" onkeyup="clearNoNum(this)" class="form-control padding-right0"  data-id="'+number+'" value="1"><span class="unit unit'+number+'"  data-id="'+number+'"></span><p class="misplacement">请填写数量</p></div><a href="javascript:" class="delete-a" data-id="'+number+'">移除</a><div class="record"></div></div>'
                    data.data.length>$('.add-product-list').length?$('#addProductBox').append(productXml):alert('最多添加'+data.data.length+'个');
                    // $('#addProductBox').append(productXml);
                    for (var i = 1; i < $('.add-product-list').length+1; i++) {
                        tdXmlOption += '<option value="'+i+'">订单'+i+'</option>';
                    }
                    $('.dismantle-order-select').html(tdXmlOption);
                    tdXml += '<tr class="tr'+number+'"><td class="td1">'+$(".order"+number).find('option:selected').text()+'*0'+$(".unit"+number).text()+'</td><td  class="td2"><select name="order" class="dismantle-order-select" id="dismantleOrder1">'+tdXmlOption+'</select></td></tr>';
                    // tdXml='<tr><td>产品sfagfshsdhd*9台</td><td><select name="order" class="dismantle-order-select" id="dismantleOrder1"><option value="2">订单1</option></select></td></tr>'
                    // console.log(tdXml);
                    $('#dismantleOrderTbody').append(tdXml);
                    selectGray ();
                    addAccountId =  $('#account_id').val();
                    addName.splice(0,1);
                    window.totalProduct = data.data.length;
                    if(totalProduct <= $('.add-product-list').length){
                        $('#addProductList').hide();
                    }
                    //拆单展开收起
                    toggleCD();
                }

            });
        }else{
            // alert('请选择客户');
        }

    })
    $(document).on('click','.delete-a',function() {
        var _thatId = $(this).siblings('.rem').find('option:selected').val();
        var _thatHtml = $(this).siblings('.rem').find('option:selected').prop('outerHTML');
        //console.log($('.add-product-list select').find('option'));

        //modify by liquan
        $('.add-product-list select').append(_thatHtml);
       /* $('.add-product-list select').find('option').each(function(i) {
            $(this).parent('select').append(_thatHtml);
            if($(this).val()==_thatId){
                $(this).show()
            }else{
                $(this).parent('select').append(_thatHtml);
                return false;
            }

        })*/

        var type = "<?php echo $type;?>";
        $(this).parent('.add-product-list').remove();
        if(type == 1 && $('.add-product-list').length == 1){
            $('.delete-a').hide();
        }
        var dataId = $(this).attr('data-id');
        addName.push(dataId);
        addName.sort(function(a,b) {
            return a-b;
        })
        if(type != 1){
            $('.tr'+dataId).remove();
            toggleCD();
            if(window.totalProduct > $('.add-product-list').length){
                $('#addProductList').show();
            }
        }
    });

    //拆单展开收起
    var toggleFlag = true;
    function toggleCD() {
        var totalTr = $('#dismantleOrderTbody tr').length;
        console.log(totalTr);
        if(totalTr > 5){
            $('#toggleCDBtn').show();
        }
        else {
            $('#toggleCDBtn').hide();
        }
        $('#dismantleOrderTbody tr').each(function (i) {
            if(i > 4 && $('#toggleCDBtn').html() == '展开'){
                $(this).addClass('trHidden');
            }else{
                $(this).removeClass('trHidden');
            }
        });
    }

    $('#toggleCDBtn').click(function () {
        if(toggleFlag == true){
            $('#toggleCDBtn').html('收起');
            $('#dismantleOrderTbody tr').removeClass('trHidden');
        }
        else {
            $('#toggleCDBtn').html('展开');
            toggleCD();
        }
        toggleFlag = !toggleFlag;
    });

    var unlock = '';
    $(document).on('click','.add-product-list select',function() {
        unlock = $(this).val();
    })
    // 拆单
    $(document).on('change','.add-product-list select',function() {
        var _that = $(this);
        $('.add-product-list select').each(function(i) {
            if(_that.attr('class')!=$(this).attr('class')){
                $(this).find('option').each(function() {
                    // $(this).val()== _that.val()?$(this).attr('disabled',true):'';
                    // $(this).val()==unlock?$(this).attr('disabled',false):'';
                    $(this).val()== _that.val()?$(this).hide():'';
                    $(this).val()==unlock?$(this).show():'';
                })
            }

        })
        _that.next('.misplacement').hide();
        _that.parent('div').siblings('.product-jia').find('.unit').text(_that.find('option:selected').attr('data-name'));
        var text = _that.find('option:selected').text() +' * '+_that.parent('div').next('.product-jia').find('input').val()+_that.parent('div').next('.product-jia').find('.unit').text();
        $('.tr'+_that.attr('data-id')).find('.td1').text(text);
        $('.tr'+_that.attr('data-id')).attr({
            'data-name':_that.val(),
            'data-number':+_that.parent('div').next('.product-jia').find('input').val()
        })

        if($('input[name="service_type"]:checked').val()=="<?php echo Yii::$app->params['chaidan']['workType'];?>"){
            $('.record').hide();
            // $(this).parent('div').siblings('.record').html('');

        }else{
            var recordXml ="";
            var that = _that;
            var url = "/account/service-record?account_id="+$('#account_id').val()+"&sale_order_id="+$(this).val()
            $.ajax({
                url:url,
                type:'GET',
                dataType:'json',
                success:function(data) {
                    recordXml +='<p style="margin-bottom: 5px;">服务记录：</p><ul>';
                    $.each(data,function(i,el) {
                        var date=new Date();
                        if(i<3){
                            var recordArr = new Array();
                            if(el.work_stage_desc){
                                recordArr.push(el.work_stage_desc);
                            }
                            if(el.finsh_service_time){
                                recordArr.push(timestampToTime(el.finsh_service_time,"ymd"));
                            }
                            if(el.service_record){
                                recordArr.push(el.service_record);
                            }
                            if(recordArr.length > 0 ){
                                recordXml +='<li><a target="_blank" href="/order/view?order_no='+el.order_no+'">'+(recordArr.join(' - '))+'</a></li>';
                            }
                        }
                    })
                    recordXml+='</ul>'
                    data.length>3? (recordXml+='<a href="javascript:" class="see-more" style="color:#999;" onclick="seeMore(\''+url+'\')">查看更多</a>'):'';
                    that.parent('div').siblings('.record').html(recordXml);
                    data.length>0? that.parent('div').siblings('.record').show():that.parent('div').siblings('.record').hide();
                }

            })
        }

    })
    $(document).on('blur','.product-jia input',function() {

        var  dataId = $(this).attr('data-id');
        var text = $('.order'+dataId).find('option:selected').text() + ' * '+ $(this).val() + $('.unit'+ dataId).text();
        $('.tr'+dataId).find('.td1').text(text);
        $('.tr'+dataId).attr({
            'data-name':$('.order'+dataId).val(),
            'data-number':$(this).val()
        })
        $(this).val()>0?$(this).siblings('.misplacement').hide():$(this).siblings('.misplacement').show();
        // 保留两位小数^\d+(\.\d{0,2})?$  整数^[1-9]\d*$
        // if(/^\d+(\.\d{0,2})?$/.test($(this).val())){
        //    $(this).siblings('.misplacement').hide()
        // }else{
        //    $(this).siblings('.misplacement').text('请正确填写');
        //    $(this).siblings('.misplacement').show();
        // }


    });
    //选择服务类型
    $('input[name="service_type"]').on('change',function() {
        $("#service_type_msg").hide();
        $('.record').html('');
         $('.record').hide();
        $.ajax({
            url:'/flow/get-flow-stage',
            type:"POST",
            dataType:'json',
            data:{id:$(this).val(),is_default:2},
            success:function(data) {
                var procedureXml = "";
                if(data.code == 200){
                    $.each(data.data,function(i,el) {
                        procedureXml += '<div class="radio-list"><input type="radio" name="work_stage" value="'+el.id+'"  id="work_stage'+el.id+'"><label for="work_stage'+el.id+'">'+el.title+'</label></div>'
                    })
                    $('#tbodyContent').html(procedureXml);
                    $(".field-flow").show();
                }else{
                    $('#tbodyContent').html(procedureXml);
                    $(".field-flow").hide();
                }

            }
        })
        for (var i = 2,length=$('.add-product-list').length+1; i < length; i++) {
            addName.push(i);
        }
        addName.sort(function(a,b) {
            return a-b;
        })
        $('#addProductBox').html('');
        $('#dismantleOrderTbody').find('tr').each(function(i) {
            $(this).attr('class')!='tr1'?$(this).remove():$(this).find('.td1').text('请选择服务产品');$('#dismantleOrder1').html('<option value="1">订单1</option>');
        })
        // $('#dismantleOrderTbody').html('');
        var dismantle_status = "<?php echo $dismantling_status;?>";
        if($(this).val()=="<?php echo Yii::$app->params['chaidan']['workType'];?>"){
            addJudge=1;
            $('#faultImg').hide();
            $('.record').hide();
            $('#qualityState').hide();
            if(dismantle_status == 1){ //拆单开关   1开启 2 关闭
                $('#dismantleOrder').show();
            }

            // var tdXmlOption = '<option value="1">订单1</option>';
            // var tdXml = '<tr class="tr1"><td class="td1"></td><td  class="td2"><select name="order" class="dismantle-order-select" id="dismantleOrder1">'+tdXmlOption+'</select></td></tr>'
            // $('#dismantleOrderTbody').append(tdXml);
            // $('#dismantleOrder1').parent('td').prev('td').text($('#account_service_brand').text()+'*'+ $('#productIpt1').val()+$('#productIpt1').next('span').text());

        }else{
            addJudge=3;
            $('#faultImg').show();
            $('#serviceRecord').show();
            $('#qualityState').show();
            $('#dismantleOrder').hide();
        }
        var type = "<?php echo $type;?>";
        if(type != 1){
            var acount_id = $("#account_id").val();
            service_brand(acount_id);
        }
    })
    //智邦添加产品时调用
    function referPro(){
        $("#service_type_msg").hide();
        $('.record').html('');
        $('.record').hide();
        var type = $('input[type=radio][name="service_type"]:checked').val();
        for (var i = 2,length=$('.add-product-list').length+1; i < length; i++) {
            addName.push(i);
        }
        addName.sort(function(a,b) {
            return a-b;
        })
        $('#addProductBox').html('');
        $('#dismantleOrderTbody').find('tr').each(function(i) {
            $(this).attr('class')!='tr1'?$(this).remove():$(this).find('.td1').text('请选择服务产品');$('#dismantleOrder1').html('<option value="1">订单1</option>');
        })
        var dismantle_status = "<?php echo $dismantling_status;?>";
        if(type=="<?php echo Yii::$app->params['chaidan']['workType'];?>"){
            addJudge=1;
            $('#faultImg').hide();
            $('.record').hide();
            $('#qualityState').hide();
            if(dismantle_status == 1){ //拆单开关   1开启 2 关闭
                $('#dismantleOrder').show();
            }
        }else{
            addJudge=3;
            $('#faultImg').show();
            $('#serviceRecord').show();
            $('#qualityState').show();
            $('#dismantleOrder').hide();
        }
        var acount_id = $("#account_id").val();
        service_brand(acount_id);

    }
    $('#account_service_brand').on('change',function() {
        var option="",_that=$(this);
        var banArray = [];
        $('.add-product-list select').each(function(i) {
            // banArray.push($(this).find('option:selected').attr('sn-id'));
            banArray.push(_that.val())
        })
        if(intent != _that.find('option:selected').attr('data-intent')){
            $("#addProductBox").html('');
            $('#dismantleOrderTbody tr').each(function(i) {
                if($(this).attr('class')!='tr1'){
                    $(this).remove();
                }
            })
            intent = _that.find('option:selected').attr('data-intent');
            $.ajax({
                url:'/order/get-sale-info',
                data:{account_id:$('#account_id').val(),intent:intent},
                type:'POST',
                dataType:'json',
                success:function(data) {
                    //modify by liquan
                    if(data.data.length > 1){
                        option+='<option value="">请选择</option>'
                        $.each(data.data,function(i, el) {
                            option +='<option value="'+el.id+'"';
                            // el.sn_id!=null?option += 'sn-id="'+el.id+el.sn_id+'"':option +='sn-id="'+el.id+'"';
                            option+='" data-name="'+el.unit+'" data-intent="'+el.intent+'"';
                            $.each(banArray,function(i,ele) {
                                // ele==el.id?option+= 'disabled="disabled"':'';
                                // var snId = el.sn_id!=null?el.id+el.sn_id:el.id;
                                ele==el.id?option+= 'style="display:none"':'';
                            });
                            option+='>'+el.value+'</option>';
                        });
                        $('#addProductBox').find('select[name="sale_order_id"]').each(function(i) {
                            $(this).html(option);
                        });
                        $("#addProductList").show();
                    }else{
                        $("#addProductList").hide();
                    }
                }
            })
        }
        setTimeout(function() {
            var text = _that.find('option:selected').text() +' * '+_that.parent('div').next('.product-jia').find('input').val()+$('.unit').text();
            $('.tr1').find('.td1').text(text);
            $('.tr1').attr({
                'data-name':_that.val(),
                'data-number':$('#productIpt1').val()
            })
        },200)

    })
    $('#productIpt1').blur(function() {
        // $('#dismantleOrder1').parent('td').prev('td').text($('#account_service_brand').find('option:selected').text()+'*'+ $(this).val()+$(this).next('span').text());
        $('.tr1').find('.td1').text($('#account_service_brand').find('option:selected').text()+'*'+ $(this).val()+$(this).next('span').text());
        $('.tr1').attr({
            'data-name':$('#account_service_brand').val(),
            'data-number':$(this).val()
        })
    });
    $(document).on('click','input[type="radio"]',function() {
        $(this).parents('.addinput-list').find('.misplacement').hide();
    })
    //循环验证
    function proving(el,bool) {
        var dd = bool;
        $(el).each(function(i,el) {
            if(el.checked){
                dd =!bool;
            }
        })
        return dd;
    }
    // 查看更多记录
    var seeMoreLayer='';
    function seeMore(el) {
        seeMoreLayer=layer.open({
            title:"查看更多记录",
            type: 2,
            skin: 'yourclass', //加上边框
            area: ['700px', '450px'], //宽高
            content:el
        });
    }
    //查看配件包，物料清单
    $(".viewProductBag").click(function(){
        var prodId = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['查看物料清单', 'font-size:18px;'],
            area: ['800px', '500px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/product/materials-detail?id='+prodId,
        });
        return false;
    });
    //提供给弹框调用,不要删除
    function showAlert(message) {}

    //获取智邦客户信息
    $("#zb_search_mobile").keyup(function(event){
        $(this).val()!=''?$('#zbs').css('color','#2693ff'):$('#zbs').css('color','#ccc');
        if(event.keyCode ==13){
            getData();
            $(this).blur();
        }
    });
    $("#zb_search_mobile").blur(function() {
        $(this).val()!=''?$('#zbs').css('color','#2693ff'):$('#zbs').css('color','#ccc');
    })
    // v3.0

    $("#zbs").click(function () {
        getData();
    });
    function getData(){
        var mobile = $("#zb_search_mobile").val();
        if(mobile == ''){
            alert("联系电话不能为空");
            $('#zhibangNodata').show();
            return false;
        }
        //开启遮罩层
        var index = layer.load(2, {
            shade: [0.5, '#ccc'],
        });
        //清空之前的信息
        $("#ufw_id").val('');
        $("#addAccountAddres").attr('title','');
        $("#addAccountBrand").attr('title','');
        $('.tab-content-list').hide();
        $('.tab-content-list').eq(0).show();
        $('#tabNav li').removeClass('active');
        $('#tabNav li').eq(0).addClass('active');
        $('.tab-content').show();
        //调用智邦客户类
        Account.accountInfo(mobile,'');
        //回写客户信息
        $("input[name=service_type]").attr("checked",false);
        $('#addProductBox').html('');
        $('#account_service_brand').html('<option value=0>请先选择客户及服务类型</option>');
        var clearInterObj = setInterval(function(){
            if($("#ufw_id").val() != ''){
                clearInterval(clearInterObj);
                getAccount($("#ufw_id").val());
            }
        },100);
        $("#order_type").val(2);
        $("#service_type_msg").hide();
        $('.record').html('');
        $('.record').hide();
        //数据数据结束后关闭遮罩
        layer.close(index);
    }
    $("body").on('change','#zb_search_mobile',function () {
        //开启遮罩层
        var index = layer.load(2, {
            shade: [0.5, '#ccc'],
        });
        $("#ufw_id").val('');
        $("#select_account").hide();
        $("#pxx").hide();
        $("#account_info").html('');
        $("#preson").html('');
        $("#reply").html('');
        $("#after_sale").html('');
        $("#tabContract").html('');
        $("#service_type_msg").hide();
        $('.record').html('');
        $('.record').hide();
        //数据数据结束后关闭遮罩
        layer.close(index);
    })
    $("#select_account").delegate(".radio-list input",'click',function (e) {
        e.stopPropagation();
        //开启遮罩层
        var index = layer.load(2, {
            shade: [0.5, '#ccc'],
        });
        $("#ufw_id").val('');
        $("#pxx").hide();
        $("#account_info").html('');
        $("#preson").html('');
        $("#reply").html('');
        $("#after_sale").html('');
        $("#tabContract").html('');
        var id = $(this).val();
        //var mobile = $(this).find("input").data('id'); //有时客户关联的手机号格式错误
        //取搜索框内的手机号
        var mobile = $("#zb_search_mobile").val();
        if(mobile == ''){
            alert("联系电话不能为空");
            $('#zhibangNodata').show();
            return false;
        }
        Account.accountInfo(mobile,id);
        $("input[name=service_type]").attr("checked",false);
        $('#addProductBox').html('');
        $('#account_service_brand').html('<option value=0>请先选择客户及服务类型</option>');
        //回写客户信息
        var clearInterObj = setInterval(function(){
            if($("#ufw_id").val() != ''){
                clearInterval(clearInterObj);
                getAccount($("#ufw_id").val());
            }
        },100);
        $("#order_type").val(2);
        //数据数据结束后关闭遮罩
        layer.close(index);

    })
    // 顶级切换
    $('#tabNav li').on('click',function() {
        $('.tab-content-list').hide();
        $('#'+$(this).data('url')).show();
        $('#zhibangNodata').hide();
        $('#tabNav li').removeClass('active');
        $(this).addClass('active');
        if($(this).data('url')=='tabClient' && $('#account_info').html()==''){
            $('#zhibangNodata').show();
        }
        if($(this).data('url')=='tabTalk' && $('#reply').html()==''){
            $('#zhibangNodata').show();
        }

        if($(this).data('url') == "tabContract"){
            if($('#tabContract').html()==''){
                $('#tabContract').hide();
                $('#zhibangNodata').show();
            }
            //开启遮罩
            var index = layer.load(2, {
                shade: [0.5, '#ccc'],
            });
            // 函数调用
            var user_id = $("#user_id").val();
            if(user_id !='' && typeof(user_id) != "undefined"){
                Contract.contractList(user_id);
            }
            //数据结束后关闭遮罩
            layer.close(index);
        }
        if($(this).data('url') == "afterSaleBox"){
            if($('#after_sale').html()==''){
                $('#zhibangNodata').show();
            }
            //开启遮罩
            var index = layer.load(2, {
                shade: [0.5, '#ccc'],
            });
            // 函数调用
            var user_id = $("#user_id").val();
            if(user_id !='' && typeof(user_id) != "undefined"){
                Service.serviceList(user_id);
            }
            //数据结束后关闭遮罩
            layer.close(index);
        }
    })
    // 合同tab切换
    $(document).on('click','.towStageNav li',function() {
        $(document).find('.three-level-box').hide();
        $(this).siblings('li').removeClass('active');
        $(this).addClass('active');
        $(this).parents(".tow-stage-nav").siblings('.tow-list-box').hide();
        $(this).parents(".tow-stage-nav").siblings('.'+$(this).data('url')).show();

    })
    // 一级详情
    $(document).on('click','.one-details-a',function() {
        //开启遮罩
        var index = layer.load(2, {
            shade: [0.5, '#ccc'],
        });
        var cid = $(this).parents('.stair').find('.cid').val();
        var strc = Contract.contractDetail(cid, $(this).parents('.stair').next('.stair-content'));
        //数据结束后关闭遮罩
        layer.close(index);
        if($(this).parents('.stair').next('.stair-content').is(':hidden')){
            $(this).parents('.stair').next('.stair-content').show();
            $(this).parents('.stair').css('border-bottom','1px solid #ddd');
            $(this).html("收起详情");
        }else{
            $(this).parents('.stair').next('.stair-content').hide();
            $(this).parents('.stair').css('border-bottom','0px solid #ddd');
            $(this).html("展开详情");
        }
    })
    // 二级别详情
    $(document).on('click','.tow-details-a',function() {
        if($(".active").data('url') == "afterSaleBox"){
            //开启遮罩
            var index = layer.load(2, {
                shade: [0.5, '#ccc'],
            });
            var cid = $(this).parents('tr').find('.sid').val();
            var strc = Service.serviceDetail(cid, $(this).parents('tr').next('.three-level-box'));
            //数据结束后关闭遮罩
            layer.close(index);
        }
        $('.product-add-order-a').hide();
        if($(this).parents('tr').next('.three-level-box').is(':hidden')){
            $(this).next('.product-add-order-a').show();
            $(this).html("收起详情");
            $(this).parents('tr').next('.three-level-box').show();
            //展开详情后的小箭头
            //<i class = "icon2-xai1"></i>
            //$(document).find('.tow-details-a').find('i').attr('class','icon2-xai1');
            //$(this).find('i').attr('class','icon2-shang1');
        }else{
            $(this).parents('tr').next('.three-level-box').hide();
            //$(this).find('i').attr('class','icon2-xai1');
            $(this).html("展开详情");
        }
    })
    //添加售后产品 到订单
    $(document).on('click','.product-add-order-a',function() {
        //客户id
        var uid = $("#ufw_id").val();
        //售后id
        var sid = $(this).parents('tr').find('.sid').val();
        var prod_id = $(this).parents('tr').next('tr').find('.sale_pro').val();
        var prod_name = $(this).parents('tr').next('tr').find('.sale_pro').data('id');
        var prodArr = [];
        var prod_arr = [];
        prod_arr = {
            id: prod_id,
            name:prod_name
        };
        prodArr.push(prod_arr);
        $.post('/order/add-zb-product',{prod_arr: JSON.stringify(prodArr),account_id:uid,contract_id:sid },function(res) {
            if(res.success){
                if(typeof(res.data) == "object"){
                    var mes = '';
                    $.each(res.data,function(k,val){
                        mes += '&nbsp;&nbsp;'+val.name+'&nbsp;&nbsp;';
                    });
                    alert(mes+'在U服务未找到，因此未添加至订单，但可在产品管理添加后，再次添加。');
                }else{
                    alert(res.message);
                }
            }else{
                alert(res.message);return false;
            }
            referPro();
        })
    })
    //添加合同产品 到订单
    $(document).on('click','.add_product_list',function() {
        var uid = $("#ufw_id").val();
        var all = $(this).parents('p').next('table').children('tbody').find('input:checkbox:checked');
        var cid = $(this).parents('.stair-content').siblings('.stair').find('.cid').val();
        var prodArr = [];
        var prod_arr = [];
        if(all.length == 0){
            alert('您还未选择产品，请选择产品后，再次尝试。');
            return false;
        }
        $.each(all,function(n,value){
            prod_arr = {
                id: value.value,
                name: value.getAttribute('data-id')
            };
            prodArr.push(prod_arr);
        });
        //console.log(JSON.stringify(prodArr));
        $.post('/order/add-zb-product',{prod_arr: JSON.stringify(prodArr),account_id:uid,contract_id:cid },function(res) {
            if(res.success){
                if(typeof(res.data) == "object"){
                    var mes = '';
                    $.each(res.data,function(k,val){
                        mes += '&nbsp;&nbsp;'+val.name+'&nbsp;&nbsp;';
                    });
                    alert(mes+'在U服务未找到，因此未添加至订单，但可在产品管理添加后，再次添加。');
                }else{
                    alert(res.message);
                }
            }else{
                alert(res.message); return false;
            }
            referPro();
        })
    })
    //查看图片
    $(document).on('click','.seeimg',function() {
        window.open($(this).attr('src'));
    })

    function  getAccount(uid) {
        $.post('/account/get-account-info',{account_id:uid},function(res) {
            if(res){
                var account = res.account;
                var address = res.address;
                var str = '';
                $("#account_name").val(account.account_name+"/"+account.mobile+"/"+account.department_name);
                $("#account_id").val(account.id);
                $.each(address,function(k,v){
                    str += '<option value="'+v.id+'">'+v.conact_name+v.address+'</option>';
                });
                $("#account_address").html(str);
            }else{
                alert("系统错误，请重新搜索");
            }
        })
    }
    //客户类
    var Account = {
        accountInfo: function(mobile,account) {
            $.post('/order/ajax-get-zb-data',{zb_mobile:mobile,select_id:account},function(res) {
                //console.log(res.data);return false;
                if(res.success){
                    //客户基本信息
                    if(res.data.length > 1){  //多个客户先选择 在查询
                        var p = '<p>请选择客户</p> <ul>';
                        $.each(res.data,function(n,value){
                            p += '<li class="select_company"><div class="radio-list"><input type="radio" name="service_type" data-id="'+value.mobile+'" value="'+value.company+'" id="people'+n+'"><label for="people'+n+'">'+value.companyname+'</label></div></li>';
                        });
                        p += '</ul>';



                        $("#select_account").html(p);
                        $("#select_account").show();
                        $("#pxx").hide();
                        $("#account_info").html('');
                        $("#preson").html('');
                        $("#reply").html('');
                        $("#after_sale").html('');
                        $("#tabContract").html('');
                        return false;
                    }
                    if(typeof(res.data.uid) != 'undefined'){
                        $("#ufw_id").val(res.data.uid);
                        $("#addAccountAddres").attr("title",res.data.uid);
                        $("#addAccountBrand").attr("title",res.data.uid);
                    }else if(res.code == "-201"){
                        alert('未能解析到该客户地址，因此未能同步至优服务，但可在U服务客户管理新增该客户。');
                    }else if(res.code == "-202"){
                        alert('在优服务上未找到该客户对应的销售，请在优服务添加销售人员后再尝试！');
                    }
                    var s = (res.data.custom_Info.sort1).split('-');
                    var str = '<tr><input type="hidden" id="user_id" value="'+res.data.search_user.company+'"><th width="72px">客户名称</th><td>'+res.data.search_user.companyname+'</td><th width="72px">客户地址</th><td>'+res.data.custom_Info.address+'</td></tr>';
                    str +='<tr><th>跟进程度</th><td>'+s[1]+'</td><th>销售人员</th><td>'+res.data.custom_Info.cateid+'</td></tr>';
                    str +='<tr><th>零售门店</th><td>'+s[0]+'</td><th>客户来源</th><td>'+res.data.custom_Info.ly+'</td></tr><tr><th>添加时间</th><td>'+res.data.custom_Info.date2+'</td><th>添加人员</th><td>'+res.data.custom_Info.cateadd+'</td></tr>';
                    str +='<tr><th>领用时间</th><td>'+res.data.custom_Info.date2+'</td><th>机器码</th><td>'+(typeof(res.data.telzdy_extra) == "undefined" ? '': res.data.telzdy_extra.zdy1)+'</td></tr>';

                    //联系人
                    $("#pxx").show();
                    var p = '<tr><th width="50%">联系人</th><th>手机</th></tr>';
                    $.each(res.data.preson,function(n,value){
                        p += '<tr><td>'+value.name+'</td><td>'+value.mobile+'</td></tr>';
                    });
                    //洽谈记录
                    if(typeof(res.data.reply) != 'undefined'){
                        var rep = '<tr><th width="110px">时间</th><th width="90px">跟进方式</th><th>内容</th></tr>';
                        $.each(res.data.reply,function(k,val){
                            var st = val.intro;
                            re = new RegExp('src="','g');
                            var Newstr = st.replace(re, 'class= "seeimg" src="http://www.bfznkj.com:84/SYSA');
                            rep += '<tr><td>'+ substr(val.date7,11)+'</td><td>'+val.sort98s+'</td><td><span>'+Newstr+'(跟进:'+val.name+'  '+val.date7+')</span></td></tr>';
                        });
                        $('#zhibangNodata').hide();
                        $("#reply").html(rep);
                    }else{
                        $("#reply").html('');
                        $('#zhibangNodata').find('p').text('暂无数据');
                        $('#zhibangNodata').show();
                    }
                    $("#account_info").html(str);
                    $("#preson").html(p);
                     $('#zhibangNodata').hide();


                }else{
                    //清空全部标签页
                    $("#pxx").hide();
                    $("#account_info").html('');
                    $("#preson").html('');
                    $("#reply").html('');
                    $("#after_sale").html('');
                    $("#tabContract").html('');
                    $('#zhibangNodata').find('p').text('未找到相关客户，请重新填写搜索信息后再次尝试');
                    $('#zhibangNodata').show();
                    alert(res.message); return false;

                     var flag = true;
                    $('.tab-content-list').each(function(i) {
                        $(this).is(':hidden')?'':flag = false;
                    })
                    console.log(flag);
                    if(flag){
                        $('#zhibangNodata').find('p').text('未找到相关客户，请重新填写搜索信息后再次尝试');
                        $('#zhibangNodata').show();
                    }
                    alert(res.message); return false;
                }

            });
        }
    };
    //售后服务
    var Service ={
        serviceList:function (id) {
            $.post('/order/get-service',{user_id:id},function(res) {
                if(res.success){
                    //售后服务列表
                    if(res.data.data != undefined){
                        var list = res.data.data;
                       var p = '<tr><th width="20%">售后日期</th><th>售后主题</th><th width="100px">处理结果</th><th width="180px">操作</th></tr>';
                       $.each(list,function(n,value){
                            p += '<tr><td>'+value.date1+'</td><td>'+value.title+'</td><td>'+value.result1+'</td><td><a href="javascript:" class="tow-details-a">展开详情</a><a href="javascript:" class="product-add-order-a">产品添加至订单</a></td><input type="hidden" class="sid" value="'+value.ord+'"><tr class="three-level-box"> </tr> </tr>';
                        })
                        $('#zhibangNodata').hide();
                        $("#after_sale").html(p);

                     }else{
                        $("#after_sale").html('');
                        $('#zhibangNodata').find('p').text('暂无数据');
                         $('#zhibangNodata').show();
                     }
                }
            })
        },
        serviceDetail:function (sid,obj) {
            $.post('/order/get-service-detail',{service_id:sid},function(res) {
                if(res.success){
                    //售后服务详情
                    var detail = res.data
                    var data = [];
                    if(typeof(detail.base) != "undefined"){
                        $.each(detail.base,function(k,val){
                           if(k=='售后主题'){
                               data['shzt'] = val;
                           }
                            if(k=='售后编号'){
                                data['shbh'] = val;
                            }
                            if(k=='接待人员'){
                                data['jdr'] = val;
                            }
                            if(k=='售后人员'){
                                data['shr'] = val;
                            }
                            if(k=='售后分类'){
                                data['shfl'] = val;
                            }
                            if(k=='售后方式'){
                                data['shfs'] = val;
                            }
                            if(k=='处理结果'){
                                data['res'] = val;
                            }
                            if(k=='紧急程度'){
                                data['jjcd'] = val;
                            }
                            if(k=='售后日期'){
                                data['shrq'] = val;
                            }
                            if(k=='具体时间'){
                                data['date'] = val;
                            }
                            if(k=='涉及产品'){
                                data['pro'] = val;
                            }
                            if(k=='产品编号'){
                                data['pron'] = val;
                            }
                            if(k=='产品型号'){
                                data['prot'] = val;
                            }
                            if(k=='关联联系人'){
                                data['person'] = val;
                            }
                            if(k=='手机'){
                                data['mobile'] = val;
                            }
                            if(k=='关联客户'){
                                data['account'] = val;
                            }
                        });
                        if(typeof(detail.service_cl) != "undefined"){
                            $.each(detail.service_cl,function(k2,val2){
                                if(k2 == '售后内容'){
                                    data['content'] = val2;
                                }
                                if(k2 == '当前处理人'){
                                    data['dqclr'] = val2;
                                }
                                if(k2 == '备注'){
                                    data['shbz'] = val2.replace(/<\/?.+?>/g,"");
                                }
                            })
                            if(typeof(detail.service_cl.intro8) != "undefined"){
                                data['jsfk'] = detail.service_cl.intro8;
                            }else{
                                data['jsfk'] = '';
                            }
                            if(typeof(detail.service_cl.intro2) != "undefined"){
                                data['cljg'] = detail.service_cl.intro2;
                            }else{
                                data['cljg'] = '';
                            }

                        }
                    }
                    var p = ' <input type="hidden" class="sale_pro" value="'+data['pron']+'" data-id="'+data['pro']+'"><td colspan="5" style="padding: 10px"><table>';
                    p += '<tr><th width="78px">售后主题</th><td colspan="5">'+data['shzt']+'</td></tr>';
                    p += '<tr><th>接待人员</th><td>'+data['jdr']+'</td><th width="85px;">当前处理人</th><td>'+(data.hasOwnProperty('dqclr') == true ? data['dqclr'] : "")+'</td><th width="70px">售后编号</th><td>'+data["shbh"]+'</td></tr>';
                    p += '<tr><th>售后分类</th><td>'+data['shfl']+'</td><th>售后方式</th><td>'+data['shfs']+'</td><th>处理结果</th><td>'+data['res']+'</td></tr>';
                    p += '<tr><th>紧急程度</th><td>'+data['jjcd']+'</td><th>售后日期</th><td>'+data['shrq']+'</td><th>具体时间</th><td>'+data['date']+'</td></tr>';
                    p += '<tr><th>涉及产品</th><td>'+data['pro']+'</td><th>产品编号</th><td>'+data['pron']+'</td><th>产品型号</th><td>'+data['prot']+'</td></tr>';
                    p += '<tr><th>关联联系人</th><td>'+data['person']+'</td><th>手机</th><td colspan="3">'+data['mobile']+'</td></tr>';
                    p += '<tr><th>售后人员</th><td colspan="5">'+data["shr"]+'</td></tr>';
                    p += '<tr><th>售后内容</th><td colspan="5">'+data['content']+'</td></tr>';
                    p += '<tr><th>处理结果</th><td colspan="5">'+data['cljg']+'</td></tr>';
                    p += '<tr><th>技师反馈</th><td colspan="5">'+data['jsfk']+'</td></tr>';
                    p += '<tr><th>备注</th><td colspan="5">'+(data.hasOwnProperty('shbz') == true ? data['shbz'] : "")+'</td></tr>';
                    p += '</table> </td>';
                    obj.html(p);
                }
            });
        }  
    };
    //合同类
    var Contract = {
        //合同列表
        contractList: function(id) {
            $.post('/order/get-contract',{user_id:id},function(res) {
                if(res.success){
                    //合同列表
                    if(res.data.data != undefined){
                        var list = res.data.data;
                        var p = '<div class="contract-list top-no-border"><div class="stair"> <div class="left">合同日期</div> <div class="center">合同主题</div> <div class="right no-border">操作</div> </div> </div>';
                        $.each(list,function(n,value){
                            p += '<div  class="contract-list"><div class="stair"> <div class="left">'+value.dateQD+'</div><div class="center">'+value.title+'</div><div class="right no-border"><a href="javascript:" class="one-details-a">展开详情</a></div><input type="hidden" class="cid" value="'+value.ord+'"></div><div class="stair-content"></div></div>';
                        });
                        $('#zhibangNodata').hide();
                        $("#tabContract").html(p);
                        $("#tabContract").show();
                    }else{
                        $('#tabContract').hide();
                        $("#tabContract").html('');
                        $('#zhibangNodata').find('p').text('暂无数据');
                        $('#zhibangNodata').show();
                    }
                    //分页
                    // var page = res.data.page;
                    // $('#pagination2').pagination({
                    //     pageCount:page.pagecount,   //总页码
                    //     prevContent:'上页',
                    //     nextContent:'下页',
                    //     isHide:	true,   // 	总页数为0或1时隐藏分页控件
                    //     current:page.pageindex,                    //当前页码
                    //     callback:function (api) {     //这是一个回调函数
                    //         $.ajax({
                    //             url:'/order/get-contract',
                    //             type:'POST',
                    //             data:{
                    //                 user_id:id,
                    //                 page:api-1    //这个方法就是用来获取点击了哪一页
                    //             },
                    //             success:function (data) {
                    //                 console.log(data);             //返回那一页的数据
                    //             }
                    //         })
                    //     }
                    // });
                }
            });
        },
        //合同详情
        contractDetail:function(cid,obj) {
            $.post('/order/get-contract-detail',{contract_id:cid},function(res) {
                if(res.success){
                    var data = res.data;
                    var str = '<div class="tow-stage-nav"><ul class="towStageNav">';
                        str += '<li class="active" data-url="essentialInformation">基本信息</li>';
                        str += '<li data-url="productListBox">产品</li>';
                        str += '<li data-url="deliveryOfCargoFromStorage">出库</li>';
                        str += '<li class="no-border" data-url="deliverGoods">发货</li>';
                        str += '</ul>';
                        str += '</div>';
                    //发货
                    str += '<div class="tow-list-box deliverGoods"><table>';
                    str += '<tr> <th width="20%">发货主题</th><th>发货人</th> <th>发货状态</th> <th width="80px">发货日期</th> <th width="80px">发货明细</th></tr>';
                    if(typeof(data.send) != "undefined"){
                        $.each(data.send,function(k,v){
                            str += '<tr><td>'+v.title+'</td>';
                            str += '<td>'+v.catename+'</td>';
                            str += '<td>'+v.status+'</td>';
                            str += '<td>'+v.date1+'</td>';
                            str += '<td><a href="javascript:" class="tow-details-a">展开详情</td>';
                            //详情
                            str += '<tr class="three-level-box"><td colspan="7" style="padding: 10px"> <table>';
                            str += '<tr><th>产品名称</th> <th>编号</th> <th>型号</th> <th>单位</th> <th>数量</th></tr>';
                            if(typeof(v.productInfo) != "undefined"){
                                $.each(v.productInfo,function(n,m){
                                    str += '<tr><td>'+m.title+'</td>';
                                    str += '<td>'+m.order1+'</td>';
                                    str += '<td>'+m.type1+'</td>';
                                    str += '<td>'+m.unitall+'</td>';
                                    str += '<td>'+m.num1+'</td></tr>';
                                })
                            }
                            str += ' </table></td></tr>';
                        })
                    }
                    str += ' </tr></table></div>';
                    //出库
                    str += '<div class="tow-list-box deliveryOfCargoFromStorage"><table>';
                    str += '<tr> <th width="20%">出库主题</th> <th width="15%">库管</th> <th>出库状态</th> <th>审批日期</th> <th width="80px">出库明细</th></tr>';
                    if(typeof(data.kuout) != "undefined"){
                        $.each(data.kuout,function(k,v){
                            str += '<tr><td>'+v.title+'</td>';
                            str += '<td>'+v.kgName+'</td>';
                            str += '<td>'+v.status+'</td>';
                            str += '<td>'+v.dateSP+'</td>';
                            str += '<td><a href="javascript:" class="tow-details-a">展开详情</td>';
                            //详情
                            str += '<tr class="three-level-box"><td colspan="7" style="padding: 10px"> <table>';
                            str += '<tr><th>产品名称</th> <th>编号</th> <th>型号</th> <th>单位</th> <th>数量</th></tr>';
                            if(typeof(v.productList) != "undefined"){
                                $.each(v.productList,function(n,m){
                                    str += '<tr><td>'+m.title+'</td>';
                                    str += '<td>'+m.order1+'</td>';
                                    str += '<td>'+m.type1+'</td>';
                                    str += '<td>'+m.unitall+'</td>';
                                    str += '<td>'+m.num1+'</td></tr>';
                                })
                            }
                            str += ' </table></td></tr>';
                        })
                    }
                    str += ' </tr></table></div>';
                    //产品
                    str += '<div  class="tow-list-box productListBox" ><p class="title">产品明细<a href="javascript:" class="add_product_list">产品添加至订单</a></p>';
                    str += '<table><thead><tr><th width="16px" class="padding15"><input type="checkbox" class="allProduct" id="allProduct'+cid+'"><label for="allProduct'+cid+'"><i class="gou-i"></i></label></th>';
                    str += '<th colspan="2">产品名称</th><th>编号</th><th>型号</th><th>单位</th><th>数量</th></tr></thead><tbody>';

                    if(typeof(data.productList) != "undefined"){
                        var p_num = 0;
                        $.each(data.productList,function(n,m){
                            str += ' <tr><td  class="padding15"><input type="checkbox" id="product'+cid+n+'" value="'+m.order1+'" data-id="'+m.title+'"><label for="product'+cid+n+'"><i class="gou-i"></i></label></td>';
                            str += '<td colspan="2">'+m.title+'</td>';
                            str += '<td>'+m.order1+'</td>';
                            str += '<td>'+m.type1+'</td>';
                            str += '<td>'+m.unitall+'</td>';
                            str += '<td>'+m.num1+'</td></tr>';
                            p_num = parseInt(p_num)+parseInt(m.num1);
                        })
                    }
                    str +='<th colspan="6">合计</th> <td>'+p_num+'</td> </tr>';
                    var bz = '';
                    var pj = '';
                    var dz = ''
                    if(typeof(data.deliveryInfo) != "undefined")
                    {
                        if(typeof(data.deliveryInfo.ht_bz) != "undefined")
                        {
                            bz = data.deliveryInfo.ht_bz;
                        }
                        if(typeof(data.deliveryInfo.ht_jhdz) != "undefined")
                        {
                            dz = data.deliveryInfo.ht_jhdz;
                        }
                        if(typeof(data.deliveryInfo.ht_pj) != "undefined")
                        {
                            pj = data.deliveryInfo.ht_pj;
                        }
                    }

                    str +='<tr><th colspan="2">交货地址</th><td colspan="5">'+dz+'</td></tr>';
                    str +='<tr><th colspan="2">配件</th><td colspan="5">'+pj+'</td></tr>';
                    str +='<tr><th colspan="2">备注</th><td colspan="5">'+bz+'</td></tr></tfoot></table></div>';
                    <!-- 基本信息 -->
                    if(typeof(data.baseInfo) != "undefined"){
                        var Newstr = '';
                        if(typeof(data.baseInfo.htbz) != "undefined"){
                            var st = data.baseInfo.htbz;
                            re = new RegExp('src="','g');
                            Newstr = st.replace(re, 'class="seeimg" src="http://www.bfznkj.com:84');
                        }
                        str += '<div class="tow-list-box essentialInformation"  style="display: block"><table>';
                        str += '<tr><th>合同主题</th><td>'+data.baseInfo.title+'</td><th>合同分类</th><td>'+data.baseInfo.htfl+'</td></tr>';
                        str += '<tr><th>门店信息</th><td>'+(typeof(data.contractzdy_extra) == "undefined"?'':data.contractzdy_extra.zdy5)+'</td><th>销售人员</th><td>'+data.baseInfo.salesMan+'</td></tr>';
                        str += '<tr><th>签订日期</th><td>'+data.baseInfo.dateQD+'</td><th>品牌</th><td>'+(typeof(data.contractzdy_extra) == "undefined"?'': (typeof(data.contractzdy_extra.meju_5) == "undefined"?"":data.contractzdy_extra.meju_5))+'</td></tr>';
                        str += '<tr><th>合同概要</th> <td colspan="3" class="outline-img-box">'+Newstr+'</td></tr>';
                        str += '<tr><th>关联主客户</th> <td colspan="3">'+data.baseInfo.companyName+'</td></tr>';
                        str += '</tr></table></div>';
                    }
                    str += '</div></div>';
                    obj.html(str);
                }
            });
        }
    };
    //合同全选
    $('body').delegate('.allProduct','click',function () {
        $(this).parents('table').find('input[type="checkbox"]').prop('checked',$(this).prop('checked'));
    })
</script>


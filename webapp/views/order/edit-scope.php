<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$this->title = '编辑质保状态';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">编辑质保状态</span>
</div>
<!--引入CSS-->
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/webuploader/webuploader.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jquery.autocomplete.css');?>">
<!--引入JS-->
<?=Html::jsFile('@web/webuploader/webuploader.js')?>
<?=Html::jsFile('@web/js/bootstrap-select.js')?>
<style>
    .datexz-wap{top: 37px;}
    .form-control[disabled], .form-control[readonly]{background: #fff;border: 1px solid  #ccc;cursor:pointer;}
    .popover{max-width: 500px !important;}
    input[name=fault_img]{display:none !important;}
    .help-block,.input-group{margin-bottom: 0};
    .btn-default:hover{color: #fff;}ul{margin-bottom: 0}
    em{font-style: normal;}
    .layui-layer-dialog .layui-layer-content{text-align: center;}
    .layui-layer-btn{text-align: center;}
    .layui-layer-btn0{position: static;width: auto;}
    .layui-layer-btn a{height: auto;line-height: 2}
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default" >
                <div class="panel-body"><?php
                    $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'form-horizontal',
                            'id' => 'form2',
                            'name' => 'form1',
                            'enctype' => 'multipart/form-data',
                        ],
                        'fieldConfig' => [
                            'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-lg-6 col-md-7 col-sm-7 col-xs-7 fuzzy-query\">{input}{error}</div>",
                            'labelOptions' => ['class' => 'name-title-lable'],
                        ]
                    ]);
                    ?>
                    <input type="text" name="order_no" value="<?php echo isset($data['order_no'])?$data['order_no']:'';?>" id="order_no" hidden>
                    <input type="text" name="work_no" value="<?php echo isset($data['work_no'])?$data['work_no']:'';?>" id="work_no" hidden>
                    <input type="text" name="src_type" value="<?php echo isset($data['src_type'])?$data['src_type']:'';?>" id="src_type" hidden>
                    <input type="text" name="src_id" value="<?php echo isset($data['src_id'])?$data['src_id']:'';?>" id="src_id" hidden>
                    <div class="form-group field-workorder-account_id">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 质保状态：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fuzzy-query">
                            <select name="scope_id" id="scope_id" class="form-control" datatype="*" sucmsg=" " nullmsg="请选择质保状态">
                                <option value="">请选择</option>
                                <?php if($data['amount_type']) foreach($data['amount_type'] as $key=>$val){?>
                                        <option value="<?php echo $key;?>" <?php if(isset($data['order_scope']) && $data['order_scope']==$key){?>selected<?php };?>><?php echo $val; ?></option>;
                                <?php }?>
                            </select>
                             <p class="misplacement"  id="scope_id_msg"></p>
                        </div>
                       
                    </div>
                    <?= $form->field($data['model'], 'scope_img')->widget('manks\FileInput', [
                            'clientOptions'=>[
                                'server' => Url::to('../upload/upload?source=fws'),
                                'pick'=>[
                                    'multiple'=>true,
                                ],
                                'fileNumLimit'=>'5',
                                'duplicate'=>true,
                                'fileSingleSizeLimit'=>'2097152', //限制上传大小为2M
                            ],
                        ]
                    ); ?>
                    <div class="form-group field-workorder-account_id">
                        <label class="name-title-lable"></label>
                        <?php if($data['scope_img']) {?>
                        <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7 fuzzy-query">
                            <?php foreach ($data['scope_img'] as $k=>$v) {?>
                                <?php if($v):?>
                                <div class="gz-img">
                                    <img src="<?php echo $v?>" alt="">
                                </div>
                                <?php endif;?>
                            <?php }?>
                        </div>
                        <?php }?>
                        <input type="text" name="scope_img" hidden="hidden" value="<?php echo isset($data['scope_img'])?implode(',', $data['scope_img']):'';?>"  />
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 col-md-3 col-sm-3 col-xs-3 control-label text-center"> </label>
                        <div  class="col-lg-6 col-md-7 col-sm-7 col-xs-7 fuzzy-query">
                            <div style="padding:0px;text-align: left;">
                                <button type="button" class="btn bg-f7 mgr20" onclick="javascript:history.back(-1);">取消</button>
                                <input type="submit" id="btnSubmit" class="btn btn-orange mgr20 " style=" color:#FFF;" value="提交"  />
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="/js/jquery-migrate-1.1.0.js"></script>
<script src="/js/jquery.autocomplete.js" type="text/javascript"></script>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>

<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    }

    /**********************   提交信息验证   ************************/
    $("#btnSubmit").click(function(e){
        var flag = checkAll();
        //询问框
        if(flag){
            confirm('确定修改质保凭证吗?',function(){
                $('#form2').submit();
            },1);
        }
        return false;
    });
    function checkAll() {
        //验证
        var flag = true;
        var scope_id              = $("#scope_id").val();//质保状态
        var cost_id               = $("#cost_id").val();//收费项目
        //编辑时候需要work_id
        var work_id              = $("#work_id").val();  //编辑当前收费项目id
        //alert(work_id);return false;
        //质保状态
        if(scope_id == ''){
            flag = false;
            $("#scope_id_msg").text("质保状态");
            $("#scope_id_msg").show();
        }
        return flag;
    }

    //获取焦点提示框消失
    $("#scope_id").change(function() {
        $("#scope_id_msg").text("");
        $("#scope_id_msg").hide();
    })
    //提示
    // $(".tip").popover();
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message');?>');
    <?php endif;?>

</script>


<?php

use yii\helpers\Url;
use webapp\models\BrandQualification;

$this->title = '添加评论';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<style>
    .us-hr {
        margin: 0 0 6px 0;
        border-bottom: 1px solid #eeeeee;

    }
</style>
<section id="main-content">
    <div id="app" class="panel-body" v-cloak>
        <us-bus-title title="发表评价" border></us-bus-title>
        <us-form label-whl="90px" item-whl="370px" wrap-whl="200px" :styles="{ paddingTop : '24px'}">
            <us-form-row>
                <us-form-item label="服务类型：" type="mini">
                    <us-text-content>{{workData.work_type_desc}}</us-text-content>
                </us-form-item>
                <us-form-item label="服务流程：" type="mini">
                    <us-text-content>{{workData.work_stage_desc}}</us-text-content>
                </us-form-item>
                <us-form-item label="服务机构：" type="mini">
                    <us-text-content>{{workData.service_department_name}}</us-text-content>
                </us-form-item>
            </us-form-row>
            <us-form-row>
                <us-form-item label="服务技师：" type="mini">
                    <us-text-content>{{workData.technicianStr}}</us-text-content>
                </us-form-item>
                <us-form-item label="完成时间：" type="mini">
                    <us-text-content>{{workData.finsh_service_time}}</us-text-content>
                </us-form-item>
            </us-form-row>
            <us-form-item label="评价：">
                <us-rate levels="1,2,3,4,5" @click="getTags"></us-rate>
            </us-form-item>
            <us-form-item v-show="tagList && tagList.list && tagList.list.length">
                <us-tagger tag-whl="90px" whl="900px" v-model="params.tag_arr">
                    <us-tag :value="item.id" v-for='(item,index) in tagList.list' clickable>{{item.title}}</us-tag>
                </us-tagger>
            </us-form-item>
            <us-form-item>
                <us-ipt-textarea
                        word-size="200"
                        whl="900px"
                        placeholder="请描述下对技师服务的整体感受" v-model="params.desc"></us-ipt-textarea>
            </us-form-item>
<!--            <hr class="hr us-hr">-->
            <us-form-item>
                <us-button @click="cancel">取消</us-button>
                <us-button type="primary" @click="commit">提交</us-button>
            </us-form-item>
        </us-form>
    </div>
</section>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/order/add-appraisal.js?v=<?php echo $v; ?>"></script>

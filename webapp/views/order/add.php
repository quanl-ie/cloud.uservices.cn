<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = '新建订单';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">新建订单
        <!--        <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="-->
        <!--            选择客户：请在您所创建的客户中选择，选择客户后，系统会自动将该客户的联系方式及地址进行填充，如客户未创建，请点击右侧添加按钮进行创建<br/><br/>-->
        <!---->
        <!--选择产品：请在您所创建的客户产品中选择，选择产品后，系统会自动将产品类目、品牌、序列号等信息进行填充，如未创建产品，请点击右侧添加按钮进行创建<br/><br/>-->
        <!---->
        <!--质保状态：在创建订单时，请严格确认客户设备的质保状态来选择类型，以免影响服务价格<br/><br/>-->
        <!---->
        <!--派单方式：您可选择指定合作服务商或输入技师电话进行服务，也可通过优服务平台进行派单，服务商需要通过合作申请后方可选择-->
        <!--        "></span>-->
    </span>
</div>
<!--引入CSS-->
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/webuploader/webuploader.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jquery.autocomplete.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time();?>">
<!--引入JS-->
<?=Html::jsFile('@web/webuploader/webuploader.js')?>
<?=Html::jsFile('@web/js/bootstrap-select.js')?>
<?=Html::jsFile('@web/webuploader/init.js')?>
<style>
    .datexz-wap{
        top: 37px;
    }
    .popover{max-width: 500px !important;}
    ul{margin-bottom: 0}
    .field-orderform-fault_img{margin-top: 15px}
    .input-group{margin-left: 17px;}
    .radio-list{margin-top: 8px;}
    .radioLable, .nodata-btn-orange{width: 120px;margin-right: 20px;float: left;}
    .layui-layer-setwin .layui-layer-close2,.layui-layer-setwin .layui-layer-close2:hover{right: 15px;width: 20px;height: 20px;}
    .city3_fix{width: 120px;}
    .trHidden{display:none;}
    .multi-img-details{margin-bottom: 0;}
    .help-block{margin:0;}
    .viewProductBag{clear: both; margin-bottom: 10px;display: block;}
</style>
<section id="main-content">
    <div class="panel-body"><?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal',
                'id' => 'form2',
                'name' => 'form1',
                'enctype' => 'multipart/form-data',
            ],
            'fieldConfig' => [
                'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"addinput-list  left-float width540-box\">{input}{error}</div>",
                'labelOptions' => ['class' => 'name-title-lable'],
            ]
        ]);
        ?>
        <p class="top-block-hint">
            <i class="red-hint-i"></i>
            <span> 新建订单前，请确保已完成品牌、产品类目、服务类型的配置。</span>
        </p>
        <div class="col-md-12" style="margin-top: 25px;">
            <h5 class="block-h5 block-h5one">用户信息</h5>
        </div>
        <div class="information-list field-workorder-account_id">
            <label class="name-title-lable" for="workorder-account_id">
                <i class="red-i">*</i> 客户：
            </label>
            <span class="c_red"></span>
            <?php if($type == 1):?>
                <?php if($contract_info):?>
                    <div class="addinput-list width630-box">
                        <input type="text" name="" disabled class="form-control width540" style="border: 0;" value="<?php echo $contract_info['info']['account']." / ".$contract_info['info']['mobile']."（".$contract_info['info']['department_name']."）";?>" >
                        <input type="hidden" name="account_id" class="form-control" id="account_id" value="<?php echo $contract_info['info']['account_id']?>">
                        <p class="misplacement" id="account_msg"></p>
                    </div>
                <?php endif;?>
            <?php else:?>
                <div class="addinput-list width630-box">
                    <input type="text" name="" class="form-control width540" maxlength="30" id="account_name" <?php if($data):?> value="<?=$data['account']['account_name']?>" <?php endif;?>>
                    <input type="hidden" name="account_id" class="form-control" id="account_id" <?php if($data):?> value="<?=$data['account']['id']?>" <?php endif;?> accept="">
                    <?php if(in_array('/account/add',$selfRoles)){?>
                        <span class="btn btn-success  " style=" color:#FFF;float:right;font-size:14px; " id="addAccount"  data-toggle="modal" data-target="#scrollingModal">添加</span>
                    <?php }?>
                    <p class="misplacement" id="account_msg"></p>
                </div>
            <?php endif;?>
        </div>

        <div class="information-list field-workorder-account_id">
            <label class="name-title-lable" for="workorder-account_id">
                <i class="red-i">*</i> 联系地址：
            </label>
            <span class="c_red"></span>
            <?php if($type == 1):?>
                <div class="col-xs-5 addinput-list">
                    <select type="text" class="form-control" name="address_id" id="account_address">
                        <?php if($address):?>
                            <option value="0">请选择</option>
                        <?php else: ?>
                            <option value="0">请先选择客户</option>
                        <?php endif;?>
                        <?php foreach ($address as $k => $v):?>
                            <option <?php if($data['address_id'] == $k):?> selected="selected" <?php endif;?> value="<?=$k?>"><?=$v?></option>
                        <?php endforeach;?>
                    </select>
                    <p class="misplacement"  id="address_msg"></p>
                </div>
            <?php else:?>
                <div class="col-xs-5 addinput-list width630-box">
                    <select type="text" class="form-control width540" name="address_id" id="account_address">
                        <?php if($address):?>
                            <option value="0">请选择</option>
                        <?php else: ?>
                            <option value="0">请先选择客户</option>
                        <?php endif;?>
                        <?php foreach ($address as $k => $v):?>
                            <option <?php if($data['address_id'] == $k):?> selected="selected" <?php endif;?> value="<?=$k?>"><?=$v?></option>
                        <?php endforeach;?>
                    </select>
                    <?php if(in_array('/account-address/add',$selfRoles)){?>
                        <span class="btn btn-success  " style=" color:#FFF;float:right;font-size:14px; " id="addAccountAddres"  data-toggle="modal" data-target="#scrollingModal" title="<?php if(isset($data['account']['id'])) echo $data['account']['id'];?>">添加</span>
                    <?php }?>
                    <p class="misplacement"  id="address_msg"></p>
                </div>
            <?php endif;?>
        </div>
        <?php if($type == 1):?>
            <div class="information-list field-workorder-account_id">
                <label class="name-title-lable" for="workorder-account_id">
                    <i class="red-i">*</i> 关联合同：
                </label>
                <span class="c_red"></span>
                <?php if($contract_info):?>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 addinput-list">
                        <span class="form-control width540" style="border: 0;"><a target="_blank" href="/contract/detail?id=<?php echo $contract_info['info']['id']?>"><?php echo $contract_info['info']['no'];?></a></span>
                        <input type="hidden" name="contract_id" id="contract_id" value="<?php echo $contract_info['info']['id']?>">
                        <p class="misplacement" id="account_msg"></p>
                    </div>
                <?php endif;?>
            </div>
        <?php endif;?>
        <div class="col-md-12">
            <h5 class="block-h5 ">服务信息</h5>
        </div>
        <div class="information-list field-workorder-account_id">
            <label class="name-title-lable" for="workorder-account_id">
                主题名称：
            </label>
            <?php if($type == 1):?>
                <?php if($contract_info):?>
                    <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7 addinput-list">
                        <input type="text" class="form-control width540" value="<?php echo $contract_info['info']['subject']?>" id="theme_name" maxlength="30" placeholder="请输入订单主题（30字以内）">
                    </div>
                <?php endif;?>
            <?php else:?>
                <div class="addinput-list width540-box">
                    <input type="text" class="form-control width540" id="theme_name" maxlength="30" placeholder="请输入订单主题（30字以内）">
                </div>
            <?php endif;?>
        </div>
        <div class="information-list field-workorder-account_id height-auto">
            <label class="name-title-lable" for="workorder-account_id">
                <i class="red-i">*</i> 服务类型：
            </label>
            <span class="c_red"></span>
            <?php if($type == 1):?>
                <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7 addinput-list">
                    <ul class="radio-li-boss"  style="padding-bottom: 12px;">
                        <?php if($serviceType){?>
                            <?php foreach($serviceType as $val):?>
                                <div class="radioLable">
                                    <input type="radio" name="service_type" value="<?php echo $val['id'];?>"  id="type<?php echo $val['id'];?>" <?php if($val['id']==$data['work_type']){ echo "checked='checked'";}?>>
                                    <label for="type<?php echo $val['id'];?>"><?php echo $val['title'];?></label>
                                </div>
                            <?php endforeach;?>
                        <?php }else{?>
                            <span class="btn btn-success"  id="addParentIframe">添加服务类型</span>
                        <?php }?>

                    </ul>
                    <input type="hidden" name="work_type" value="<?php if(isset($data['work_type'])) echo $data['work_type'] ?>" class="determined-val" id="service_type">
                    <p class="misplacement"  style="bottom: -10px" id="service_type_msg"></p>
                </div>
            <?php else:?>
                <div class="addinput-list width560-box left-float" style="margin-bottom: 6px;">
                    <?php if($serviceType){?>
                        <?php foreach($serviceType as $val):?>
                            <div class="radioLable">
                                <input type="radio" name="service_type" value="<?php echo $val['id'];?>"  id="type<?php echo $val['id'];?>">
                                <label for="type<?php echo $val['id'];?>"><?php echo $val['title'];?></label>
                            </div>
                        <?php endforeach;?>
                    <?php }else{?>
                        <span class="btn btn-success"  id="addParentIframe">添加服务类型</span>
                    <?php }?>
                    <input type="hidden" name="work_type" value="<?php if(isset($data['work_type'])) echo $data['work_type'] ?>" class="determined-val" id="service_type">
                    <p class="misplacement bottom-20"  id="service_type_msg"></p>
                </div>
            <?php endif;?>
        </div>
        <?php if($serviceWorkStage){?>
            <div class="information-list field-workorder-account_id field-flow padding-top10 height-auto margin-bottom10" <?php if($type != 1){ echo 'style="display: none;"';}?>>
                <label class="name-title-lable" for="workorder-account_id">
                    <i class="red-i">*</i> 服务流程：
                </label>
                <span class="c_red"></span>
                <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7 addinput-list">
                    <div id="tbodyContent" style="text-align: left">
                        <?php foreach($serviceWorkStage as $key=>$val){?>
                            <div class="radioLable">
                                <input type="radio" name="work_stage" value="<?php echo $val['id'];?>"  id="work_stage<?php echo $key;?>">
                                <label for="work_stage<?php echo $key;?>"><?php echo $val['title'];?></label>
                            </div>
                        <?php }?>
                    </div>
                    <input type="hidden" name="work_stage" value="<?php if(isset($data['work_stage'])) echo $data['work_stage'] ?>" class="determined-val" id="work_stage">
                    <p class="misplacement"  style="bottom: -15px" id="work_stage_msg"></p>
                </div>
            </div>
        <?php }?>
        <div class="information-list field-workorder-account_id padding-top10 height-auto">
            <label class="name-title-lable" for="workorder-account_id">
                <i class="red-i">*</i> 服务产品：
            </label>
            <span class="c_red"></span>
            <?php if($type == 1):?>
                <?php if($contract_info):?>
                    <?php foreach ($contract_info['detail'] as $val):  ?>
                        <div class="addinput-list width630-box add-product-list">
                            <div class="width390 padding-left0 ">
                                <select type="text" class="form-control" name="sale_order_id" id="account_service_brand">
                                    <option value="<?= $val['sale_order_id']?>">
                                        <?php if($val['model'] != ''):?>
                                            <?= $val['model']."/"?>
                                        <?php endif;?>
                                        <?php if($val['prod_name'] != ''):?>
                                            <?= $val['prod_name']?>
                                        <?php endif;?>
                                        <?php if($val['brand_name'] != ''):?>
                                            <?= '/'.$val['brand_name']?>
                                        <?php endif;?>
                                </select>
                                <p class="misplacement" id="brand_msg"></p>
                            </div>
                            <div  class="width120 padding-right0 product-jia">
                                <i class="icon1-cha"></i>
                                <input type="text" class="form-control padding-right0" id="productIpt1" value="<?= $val['prod_num']?>" maxlength="9" onkeyup="clearNoNum(this)">
                                <span class="unit unit1"><?= $val['unit_name']?></span>
                                <p class="misplacement">请填写数量</p>
                            </div>
                            <a href="javascript:" class="delete-a" style="right: 30px;" data-id="'+number+'">移除</a>
                            <?php if($val['prod_type'] == 3):?>
                                <a href="javascript:" class="viewProductBag" id-data="<?= $val['prod_id'] ?>">查看物料详情</a>
                            <?php endif;?>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>
            <?php else:?>
                <div class="addinput-list width630-box add-product-list">
                    <div class=" width390 padding-left0 ">
                        <select type="text" class="form-control" name="sale_order_id" id="account_service_brand">
                            <?php if($service):?>
                                <option value="0">请选择</option>
                            <?php else: ?>
                                <option value="0">请先选择客户及服务类型</option>
                            <?php endif;?>
                            <?php foreach ($service as $k => $v):?>
                                <option <?php if($data['detail']['data']['workDetail'][0]['sale_order_id'] == $k):?> selected="selected" <?php endif;?> value="<?=$k?>"><?=$v?></option>
                            <?php endforeach;?>
                        </select>
                        <p class="misplacement" id="brand_msg"></p>
                    </div>
                    <div  class="width120 padding-right0 product-jia">
                        <i class="icon1-cha"></i>
                        <input type="text" class="form-control padding-right0" id="productIpt1" value="1" maxlength="9" onkeyup="clearNoNum(this)">
                        <span class="unit unit1"></span>
                        <p class="misplacement">请填写数量</p>

                    </div>
                    <div class="add-btn">
                        <?php if(in_array('/sale-order/add',$selfRoles)){?>
                            <span class="btn btn-success"  style="margin-left: 20px; " id="addAccountBrand"  data-toggle="modal" data-target="#scrollingModal"  title='<?php if(isset($data['account_id'])) echo $data['account_id'];?>'>添加</span>
                        <?php }?>
                    </div>
                    <?php if(in_array('/account/view',$selfRoles)){?>
                        <div class="record" style="width: 540px">
                            <!-- <p style="margin-bottom: 5px;">服务记录</p> -->
                            <!-- <ul>
                                <li><a href="javascript:">记录1</a></li>
                                <li><a href="javascript:">记录2</a></li>
                                <li><a href="javascript:">记录3将js放到html Dom节点下。先绘制节点，然后在执行js就不会出现此问题将js放到html Dom节点下。先绘制节点，然后在执行js就不会出现此问题将js放到html Dom节点下。先绘制节点，然后在执行js就不会出现此问题</a></li>
                            </ul>
                            <a href="javascript:" class="see-more">查看更多</a> -->
                        </div>
                    <?php };?>

                </div>
                <div class="information-list field-workorder-account_id " id="judgeBox" style="height: auto;">
                    <div class="addinput-list width650-box">
                        <div id="addProductBox">
                            <!-- <div class="add-product-list">
                        <div class="col-xs-7 col-md-9 padding-left0">
                            <select type="text" class="form-control" name="sale_order_id" id="account_service_brand">
                                <?php if($service):?>
                                <option value="0">请选择</option>
                                <?php else: ?>
                                <option value="0">请先选择客户</option>
                                <?php endif;?>
                                <?php foreach ($service as $k => $v):?>
                                <option <?php if($data['detail']['data']['workDetail'][0]['sale_order_id'] == $k):?> selected="selected" <?php endif;?> value="<?=$k?>"><?=$v?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div  class="col-xs-5 col-md-3 padding-right0 product-jia">
                            <i class="icon1-cha"></i>
                            <input type="number" class="form-control padding-right0">
                            <span class="unit">台</span>
                        </div>
                        <a href="javascript:" class="delete-a">移除</a>
                    </div> -->
                        </div>
                        <a href="javascript:" id="addProductList">
                            <i class="icon2-jia"></i>
                        </a>
                    </div>
                </div>
            <?php endif;?>

        </div>



        <!-- style="display: <?php if($serviceWorkStage){echo 'block' ;}else{echo 'none';}?>" -->


        <div class="information-list field-workorder-account_id" id="qualityState">
            <label class="name-title-lable" for="workorder-account_id">
                <i class="red-i">*</i> 质保状态：
            </label>
            <span class="c_red"></span>
            <div class="addinput-list width650-box left-float">
                <?php foreach ($amount_type as $k => $v):?>

                    <div class="radioLable">
                        <input type="radio" name="amount_type" value="<?=$k;?>"  id="amount_type<?=$k;?>">
                        <label for="amount_type<?=$k;?>"><?=$v;?></label>
                    </div>
                <?php endforeach;?>
                <p class="misplacement bottom-20"  id='amount_msg'></p>
            </div>

        </div>


        <div class="information-list field-workorder-account_id">
            <label class="name-title-lable" for="workorder-account_id">
                <i class="red-i">*</i> 服务时间：
            </label>
            <div class="addinput-list width540-box">
                <div class="equable-box">
                    <div class="time-equable">
                        <input type="text"  class="form-control  planTime" id="selectDate" name="selectDate" value="<?php if(isset($data['plan_time'])) echo date("Y-m-d H:i:s",$data['plan_time'])?>"  onclick="selectDateDay()" placeholder="请选择服务时间" readonly>
                        <i class="icon2-rili riqi1" id="rili" style="right: 5px;" onclick="selectDateDay()" ></i>
                    </div>
                    <div class="time-equable">
                        <!-- 时间插件 -->
                        <select name="plan_time_type" id="plan_time_type" class="plan_time_type form-control">
                            <option value="0">请选择</option>
                            <?php foreach($setTimeType as $kk=>$vv) :?>
                                <option value="<?=$kk?>"><?=$vv?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="time-equable">
                        <!--选择具体时间时才显示setTime这个框-->
                        <div id="show_setTime" style="display: none">
                        </div>
                    </div>
                    <div class="city23_fix"></div>
                </div>
                <p class=" misplacement" id='plan_time_msg' >错误提示</p>
            </div>
        </div>
        <!--故障图片 -->
        <div  class="information-list" style="clear: both;" id="faultImg">
            <?= $form->field($model, 'fault_img')->widget('manks\FileInput', [
                    'clientOptions'=>[
                        'server' => Url::to('../upload/upload?source=fws'),
                        'pick'=>[
                            'multiple'=>true,
                        ],
                        'fileNumLimit'=>'5',
                        'duplicate'=>true,
                        'fileSingleSizeLimit'=>'2097152', //限制上传大小为2M
                    ]
                ]
            ); ?>
        </div>
        <div class="information-list field-workorder-account_id height-auto margin-top0" style="padding-top: 10px;">
            <label class="name-title-lable" for="workorder-account_id">
                备注：
            </label>
            <div class="addinput-list width540-box" style="margin-bottom: 30px">
                <textarea class="ramark-box" id="remarks" maxlength="200" placeholder="请填写客户的服务需求" name="description" id="description"><?php if(isset($data['description'])):?><?=$data['description']?><?php endif;?></textarea>
                <p class="xianzhi"><span  id='word'>0</span>/200</p>
            </div>
        </div>

        <div class="information-list field-workorder-account_id" style="height: auto;display: none;" id="dismantleOrder">
            <label class="name-title-lable" for="workorder-account_id">
                拆单：
            </label>
            <div class="addinput-list width630-box" style="margin-bottom: 30px">
                <table>
                    <tr>
                        <th>产品</th>
                        <th width="120px">分单</th>
                    </tr>
                    <tbody id="dismantleOrderTbody">
                    <!-- <tr>
                        <td>产品*5台</td>
                        <td>
                            <select name="order" class="dismantle-order-select" id="dismantleOrder1">
                                <option value="2">订单1</option>
                            </select>
                        </td>
                    </tr> -->
                    <tr class="tr1"><td class="td1">请选择服务产品</td><td  class="td2"><select name="order" class="dismantle-order-select" id="dismantleOrder1"><option value="2">订单1</option></select></td></tr>
                    </tbody>
                </table>
                <div style="text-align: right"><a id="toggleCDBtn" style="display: none;" href="javascript:void(0);">展开</a></div>
            </div>
        </div>
        <div class="information-list field-workorder-account_id">
            <?php if(in_array('/order/main-assigin',$selfRoles)){?>
                <label class="name-title-lable" for="workorder-account_id">
                    指派：
                </label>
                <div class="addinput-list width540-box left-float" style="margin-bottom: 30px">
                    <div>
                        <div class="radio-list">
                            <input type="radio" value="1" name="assign" id="assign1" class="pr1" data-id="<?php echo isset($DepartmentInfo['id'])?$DepartmentInfo['id']:''?>"><label for="assign1">机构</label>
                        </div>
                        <div class="radio-list">
                            <input type="radio" value="2" name="assign" id="assign2" class="pr1"><label for="assign2">技师</label>
                        </div>
                    </div>
                    <p class=" misplacement" id='plan_assign_msg'>错误提示</p>
                    <div class="drop-dw-layerbox" style="position: static;">
                        <ul id="level-tree">
                        </ul>
                    </div>
                </div>
            <?php }?>
        </div>

        <div class="information-list"><hr></div>
        <div class="information-list" style="text-align: center;">
            <a href="/order/index"><button type="button"  class="btn bg-f7 mgr20">返回</button></a>
            <input type="button" class="btn btn-orange btnSubmit"  value="提交"  />
            <input type="button" class="btn btn-orange  btnSubmiting" style="display: none; " value="提交中..."  />


        </div>
        <?php ActiveForm::end(); ?>
    </div>
   <!-- <input type="text" value="">
    <input type="button" value="智邦搜索" id="zbs">-->
</section>
<!-- 时间插件/ -->
<?=Html::jsFile('@web/js/WdatePicker/WdatePicker.js')?>
<?=Html::jsFile('@web/js/WdatePicker/time.js')?>
<script type="text/javascript" src="/js/jquery-migrate-1.1.0.js"></script>
<script src="/js/jquery.autocomplete.js" type="text/javascript"></script>
<script>
    var addJudge = 1,reload=2;
    window.onload = function(){
        selectGray ();
        //mechanismInit();
    }
    //loading层
    $("#zbs").click(function () {
        var index = layer.load(2, {
            shade: [0.5, '#ccc'],
        });
        //alert("未发现匹配数据");
        //数据数据结束后关闭遮罩
        //layer.close(index);
    });
    function mechanismInit(addressId) {
        var pid        = $('#assign1').attr('data-id');
        var addressId  = $("#account_address").val();  //客户地址id
        var ulData = '';
        $.getJSON('/organization/ajax-get-department',{'pid':pid,"total_work":1,"service_area":1,'address_id':addressId,'service_area':1},function (json) {
            var data = json.data;
            for (item in data){
                ulData +="<li><span>";
                data[item].sub==1?ulData+= '<i class="icon2-shixinyou tree-i"  onclick="clickCategory(this)"></i>':'';
                ulData+='<input type="radio" value="'+data[item].id+'" name="service_depart_id" id="jg'+data[item].id+'" class="pr1"><label for="jg'+data[item].id+'">'+data[item].name+'</label></span></li>';
            }
            $('#level-tree').html(ulData);
        })

    }

    // 字数限制
    $("#remarks").keyup(function(){
        if($("#remarks").val().length > 200){
            $("#remarks").val( $("#remarks").val().substring(0,200) );
        }
        $("#word").text( $("#remarks").val().length ) ;
    });
    // 未选择select字灰色
    function selectGray () {
        var unSelected = "#999";
        var selected = "#333";
        $(document).find("select").each(function(i) {
            if($(this).val() == "0"||$(this).val() == ""||$(this).val() == 0){
                $(this).css("color", unSelected);
                $(this).find("option").css("color",unSelected);
            }
            $(this).find('option').each(function(i) {
                if($(this).val()==''||$(this).val()=='0'){
                     $(this).css("color", unSelected);
                    $(this).prop('disabled',true);
                }else{
                    $(this).css("color", selected);
                }

            }); 
        });

    }
    $(document).on('click','select',function () {
            if($(this).val() == "0"||$(this).val() == ""||$(this).val() == 0){
                $(this).css('color','#999');
                $(this).find("option").css("color",'#999');
            }else{
                $(this).css('color','#333');
                $(this).find("option").css("color",'#333');
            }
            $(this).find('option').each(function(i) {
                if($(this).val()==''||$(this).val()=='0'){
                    $(this).css("color", '#999');
                    $(this).prop('disabled',true);
                }else{
                    $(this).css("color", '#333');
                }

            }); 
    });
    //添加服务类型
    $("#addParentIframe").click(function () {
        layer.open({
            type: 2,
            title: '请选择服务类类型',
            area: ['400px', '250px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/type/add', 'no'],
            end:function(){
                location.reload();
            }
        });
    });
    //单项选择
    $('.radio-li').on('click',function() {
        $(this).find('span').css({'background':'#2693ff','color':'#fff'});
        $(this).siblings('li').find('span').css({'background':'#fff','color':'#333','border-color':'#ccc'});
        $(this).parent('ul').next('input').val($(this).find('span').attr('id'));
    })
    // 选择
    $('.xiangxia').on('click',function() {
        $('.addinput-list width650-box-data').hide();
        $(this).next('.addinput-list width650-box-data').show();
    })
    //
    $('.addinput-list width650-box-data li').on('click',function() {
        $('.addinput-list width650-box-data').hide();
        $(this).parent('ul').siblings('.form-control').val($(this).attr('id '));
    })


    //添加客户
    var iframeSrc = '';
    $("#addAccount").click(function () {
        iframeSrc = 'order';
        layer.open({
            type: 2,
            title: '添加客户',
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: true,
            content: '/account/add/'
        });
    });
    //添加客户地址
    var iframeCallbackAddressId = 0;
    $("#addAccountAddres").click(function() {
        var id = $(this).attr('title');
        if(id==null || id==undefined || id=='' || isNaN(parseInt(id))) {
            $("#address_msg").text("请先选择客户");
            $("#address_msg").show();
        }else {
            layer.open({
                type: 2,
                title: '添加客户地址',
                area: ['700px', '450px'],
                fixed: false, //不固定
                maxmin: true,
                content: '/account-address/add?id='+id,
                end  : function(){
                    account_address(id)
                }
            });
            account_address(id)
        }
    });
    //添加服务产品
    var iframeCallbackSaleOrderId = 0;
    var callbackJudge = 0;
    $("#addAccountBrand").click(function() {
        var id = $(this).attr('title');
        // if(id==null || id==undefined || id=='' || isNaN(parseInt(id))) {

        // }
        if($('#account_id').val()!=""&&proving($('input[name="service_type"]',false))){
            layer.open({
                type: 2,
                title: false,
                area: ['700px', '400px'],
                fixed: false, //不固定
                maxmin: false,
                content: '/sale-order/add?accountId='+id+'&source=1',
                end  : function(){
                    if($('.add-product-list').length == 0){
                        service_brand(id);
                    }else if($('#account_service_brand').find('option').length<2){
                        service_brand(id);
                    }else if($('#account_service_brand').find('option:checked').attr('data-intent')==callbackJudge){
                        $('#addProductList').click();
                    }else{
                        $('#addProductList').click();
                    }
                    // service_brand(id);
                    // $('#account_service_brand').val()
                    // $.ajax({
                    //     url: '/order/get-sale-info',
                    //     type: 'POST',
                    //     dataType: 'json',
                    //     data: {account_id:$('#account_id').val(),type_id:$('input[name="service_type"]:checked').val()},
                    //     success:function(res) {
                    //         console.log(res);
                    //        $.each(res.data,function(index, el) {
                    //            console.log(index);
                    //            console.log(el);
                    //        });
                    //     }
                    // })
                    //$('#addProductList').parents('#judgeBox').show();
                    //$('#addProductList').show();
                }
            });
            // service_brand(id)
        }else{
            $("#brand_msg").text("请先选择客户及服务类型");
            $("#brand_msg").show();
        }
    });


    //模糊查询选中
    var accountId = 0;
    function accountName(id) {

        $("#brand_msg").hide();
        $("#address_msg").hide();
        $("#provide_msg").hide();

        accountId = id;
        $("#account_id").val(id);
        $("#addAccountAddres").attr('title',id);
        $("#addAccountBrand").attr('title',id);
        $("#account_select").hide();
        account_address(id);
        var type = "<?php echo $type;?>";
        if(type != 1){
            var type_id = $('input[name="service_type"]:checked').val();
            if(type_id != '' && type_id != null){
                service_brand(id);
            }
        }

    }
    //查询客户对应的地址信息
    function account_address(account_id) {
        $.post('/order/get-address',{account_id:account_id},function(res) {
            $("#account_address").html(res);
            if($("#account_address").val()=="0"||($("#account_address").val()=="")){
                $("#account_address").css('color','#999')
            }else{
                $("#account_address").css('color','#333')
            }
            setTimeout(function () {
                $("#account_address option:eq(1)").prop('selected',false);
            },500);
            if(iframeCallbackAddressId>0){
                $("#account_address").val(iframeCallbackAddressId);
            }
        })
    }
    //查询客户对应服务产品
    function service_brand(account_id) {
        var serverTypeId = $('input[name="service_type"]:checked').val();
        var type_id = 0;
        if(serverTypeId == <?php echo Yii::$app->params['chaidan']['workType'];?>){
            type_id = 1;
        }else{
            type_id = 2;
        }
        $.post('/order/get-service-brand',{account_id:account_id,type_id:type_id},function(res) {
            $("#account_service_brand").html(res);
            if(iframeCallbackSaleOrderId > 0 ){
                $("#account_service_brand").val(iframeCallbackSaleOrderId);
                $("#account_service_brand").change();
            }
            setTimeout(function () {
                if($("#account_service_brand").find('option').length <=2){
                    $('#addProductList').hide();
                    $('#addProductList').parents('#judgeBox').hide();
                }
                else {
                    $('#addProductList').show();
                    $('#addProductList').parents('#judgeBox').show();
                }
            },300);
        })
    }

    $(".btnSubmit").click(function(e){
        if(checkAll())
        {
            var type = "<?php echo $type;?>";
            var serverTypeId = $('input[name="service_type"]:checked').val();
            var data = {
                account_id:$('#account_id').val(),//客户
                address_id:$('#account_address').val(),//服务地址
                subject_name:$('#theme_name').val(),//主题名称
                contract_id:$('#contract_id').val(), //合同id
                work_type:serverTypeId,//服务类型
                is_scope:serverTypeId!=<?php echo Yii::$app->params['chaidan']['workType'];?>? $('input[name="amount_type"]:checked').val():'',//质保状态
                selectDate:$('#selectDate').val(),//服务时间
                plan_time_type:$('#plan_time_type').val(),//中间时间
                setTime:$('select[name="setTime"]').val(),//具体时间
                description:$('#remarks').val(),//备注
                OrderForm:[],//故障图片
                assign:$('input[name="assign"]:checked').val(),//指派
                orderList:[],
                service_depart_id:$('input[name="service_depart_id"]:checked').val(),//机构
                work_stage:$('input[name="work_stage"]:checked').val(),//服务流程
                judge:1,//1不拆单，2拆单 判断是否拆单
                order_type:type,//合同派单

            }
            var orderAraay = [],newOrderAraay = [],oderAraay1=[];
            if($('input[name="service_type"]:checked').val()=='<?php echo Yii::$app->params['chaidan']['workType'];?>'){
                $('#dismantleOrderTbody tr').each(function(i) {
                    var groupNum = $(this).find('.dismantle-order-select').val();
                    var productId = $(this).data('name');
                    var productNum = $(this).data('number');
                    var obj = {'product_id':productId,'num':productNum};
                    if(typeof(orderAraay[groupNum] )== 'undefined'){
                        orderAraay[groupNum] = [];
                    }
                    orderAraay[groupNum].push(obj);
                });
                for(i=0; i<orderAraay.length;i++)
                {
                    if(typeof(orderAraay[i]) != 'undefined'){
                        newOrderAraay.push( orderAraay[i]);
                    }
                }
                data.orderList=newOrderAraay;
                data.OrderForm = [];
                data.judge = 2;
            }
            else
            {
                $('.add-product-list').each(function(i,el) {
                    var obj = {
                        product_id:$(this).find('select[name="sale_order_id"]').val(),
                        num:$(this).find('.product-jia input').val()
                    }

                    oderAraay1.push(obj);
                })
                $('input[name="OrderForm[fault_img][]"]').each(function(i) {
                    data.OrderForm.push($(this).val())
                })

                data.orderList=oderAraay1
                data.judge = 1
            }

            $.ajax({
                url:'/order/add',
                type:'POST',
                data:data,
                dataType:'json',
                success:function(el) {
                    if(el.success){
                        alert('添加成功');
                        var jumpUrl = el.data.url;
                        setTimeout(location.href = jumpUrl,1000)
                    }else{
                        alert("订单产品数量不能大于合同产品数量");
                        $('.btnSubmit').show();
                        $('.btnSubmiting').hide();
                    }
                }
            })
            $('.btnSubmit').hide();
            $('.btnSubmiting').show();
        }
        else {
            $('.btnSubmit').show();
            $('.btnSubmiting').hide();
        }
        return false;
    });

    //验证
    function checkAll()
    {
        //验证客户是否选中
        var flag = true;
        var account_id               = $("#account_id").val();//客户
        var account_name             = $("#account_name").val();//客户
        var account_address          = $("#account_address").val();//地址
        var tbodyContent             =$("#tbodyContent .radioLable").length; //获取服务流程
        var assign1                  =$('#assign1');//判断是否选择机构
        var type                     = "<?php echo $type;?>";
        //验证客户

        if(account_id.length==0 || isNaN(parseInt(account_id))){
            flag = false;
            $("#account_msg").text("请先选择客户");
            $("#account_msg").show();
        }
        if(type != 1 && (account_name == null || account_name == undefined || account_name == '')){
            flag = false;
            $("#account_msg").text("请先选择客户");
            $("#account_msg").show();
        }
        //验证客户地址A==null||A!=undefined||A==""
        if(account_address==null || account_address==undefined || account_address=='' || isNaN(parseInt(account_address))){
            flag = false;
            $("#address_msg").text("请先选择客户地址");
            $("#address_msg").show();
        }
        //验证服务产品
        $('.add-product-list').each(function(i) {
            if($(this).find('select[name="sale_order_id"]').val()==""||$(this).find('select[name="sale_order_id"]').find('option:selected').val()=="请选择"){
                flag = false;
                $(this).find('select[name="sale_order_id"]').next('p').text('请选择服务产品');
                $(this).find('select[name="sale_order_id"]').next('p').show();
            }
            if($(this).find('.product-jia input').val()<1||$(this).find('.product-jia input').val()==""){
                flag = false;
                $(this).find('.product-jia .misplacement').text('数量不能小于1');
                $(this).find('.product-jia .misplacement').show();
            }
        })
        //验证服务类型
        if(proving($('input[name="service_type"]'),true)){
            flag = false;
            $("#service_type_msg").text("请选择服务类型");
            $("#service_type_msg").show();
        }
        //验证质保
        if(proving($('input[name="amount_type"]'),true)&&$('input[name="service_type"]:checked').val()!=<?php echo Yii::$app->params['chaidan']['workType'];?>){
            flag = false;
            $("#amount_msg").text("请选择质保状态");
            $("#amount_msg").show();
        }
        if(tbodyContent >0){
            //验证服务流程
            if(proving($('input[name="work_stage"]'),true)){
                flag = false;
                $("#work_stage_msg").text("请选择服务流程");
                $("#work_stage_msg").show();
            }
        }
        if (assign1.prop('checked')) {
            var serviceDepart = $('input[name="service_depart_id"]');
            var tag = false;
            serviceDepart.each(function(index, el) {
                if (this.checked) {
                    tag = true;
                }
            });
            if (!tag) {
                flag = false;
                $("#plan_assign_msg").text("请选择服务机构");
                $("#plan_assign_msg").show();
            }else{
                $("#plan_assign_msg").hide();
            }

        }
        //验证时间
        var select_date              = $("#selectDate").val();//选择日期
        var plan_time_type           = $(".plan_time_type").val();//服务时间类型
        var set_time                 = $("#setTime").val();  //选择具体时间
        if(select_date==null || select_date==undefined || select_date==''){
            $("#plan_time_msg").text("请选择日期");
            $("#plan_time_msg").show();
            return false;
        }

        //验证时间
        if(plan_time_type==0 || plan_time_type==undefined || plan_time_type==''){
            $("#plan_time_msg").text("请选择服务时间类型");
            $("#plan_time_msg").show();
            return false;
        }

        //验证时间
        if(plan_time_type==5&&(set_time==null || set_time==undefined || set_time=='')){
            $("#setTime").text("请选择具体的服务时间");
            $("#plan_time_msg").show();
            return false;
        }

        return flag;
    }

    //获取焦点提示框消失
    $("#account_name").focus(function() {
        $("#account_msg").text("");
        $("#account_msg").hide();
    })
    $("#account_address").focus(function() {
        $("#address_msg").text("");
        $("#address_msg").hide();
    })
    $("#account_service_brand").focus(function() {
        $("#brand_msg").text("");
        $("#brand_msg").hide();
    })

    $("#selectDate").click(function() {
        $("#plan_time_msg").text("");
        $("#plan_time_msg").hide();
    })
    $(".plan_time_type").click(function() {
        $("#plan_time_msg").text("");
        $("#plan_time_msg").hide();
    })
    $("#set_time").click(function() {
        $("#plan_time_msg").text("");
        $("#plan_time_msg").hide();
    })
    $(".service_type").click(function() {
        $("#service_type_msg").text("");
        $("#service_type_msg").hide();
    })
    $(".amount_type").click(function() {
        $("#amount_msg").text("");
        $("#amount_msg").hide();
    })
    // 失去焦点
    var blurAccountName = $('#account_id').val();
    $("#account_name").autocomplete("/account/sreach", {
        minChars: 1,
        matchCase:false,//不区分大小写
        autoFill: false,
        max: 10,
        dataType: 'json',
        width:$('#account_name').outerWidth(true)+'px',
        extraParams:{v:function() { return $('#account_name').val();}},
        parse: function(data) {
            return $.map(eval(data), function(row) {
                return {
                    data: row,
                    value: row.id,    //此处无需把全部列列出来，只是两个关键列
                    result: row.name
                }
            });
        },
        formatItem: function(row, i, max,term) {
            return  row.name;
        },
        formatMatch: function(row, i, max) {
            return row.name;
        },
        formatResult: function(row) {
            return row.name;
        },
        reasultSearch:function(row,v)//本场数据自定义查询语法 注意这是我自己新加的事件
        {
            //自定义在code或spell中匹配
            if(row.data.name.indexOf(v) == 0)
            {
                return row;
            }
            else
                return false;
        }
    }).result(function(event, data, formatted) { //回调
        if(data.id>0){
            if($("#account_id").val() != data.id){
                $('.record').html('');
                $('.record').hide();
                $('#addProductBox').html('');
                $('#productIpt1').val('1');
                $('#dismantleOrderTbody').html('<tr class="tr1">请选择服务产品<td class="td1"></td><td  class="td2"><select name="order" class="dismantle-order-select" id="dismantleOrder1"><option value="2">订单1</option></select></td></tr>')
            }

            accountName(data.id);
            $('#account_name').val(data.name);
        }
        else {
            $('#account_name').val('');
        }
    });

    //填充用户信息
    function fillAccount($accountId,$accountName) {
        selectGray ();
        $("#account_name").val($accountName);
        $('#addProductBox').html('');
        $('#account_id').val($accountId);
        accountName($accountId);
    }

    $("#account_address").click(function() {
        if(( $.trim($("#account_address").html()) == '')){
            $("#address_msg").text("请先选择客户");
            $("#address_msg").show();
        }
        $('.drop-dw-layerbox').hide();
    });

    $("#account_service_brand").click(function() {
        selectGray ();
        if(( $.trim($("#account_service_brand").html()) == '')){
            $("#brand_msg").text("请先选择客户");
            $("#brand_msg").show();
        }
    });
    <?php if($accountArr):?>
    fillAccount(<?php echo $accountArr['id'];?>, '<?php echo $accountArr['account_name'] . '/' . $accountArr['mobile'];?>');
    <?php endif; ?>

    //提示
    $(".tip").popover();

    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message');?>');
    <?php endif;?>

    var myDate = new Date();
    var myMonth = myDate.getMonth()<10?"0"+(myDate.getMonth()+1):myDate.getMonth();
    var myDay = myDate.getDate()<9?'0'+myDate.getDate():myDate.getDate();
    var minDate = myDate.getFullYear()+'-'+myMonth+'-'+myDay;

    function selectDateDay() {
        jeDate('#selectDate', {
            trigger:false,
            isToday:false,
            format: 'YYYY-MM-DD',
            maxDate: '2099-06-16 23:59:59',
            donefun:function(){
                $("#plan_time_type").attr('disabled',false);
                var d = new Date();
                var selectDate = new Date($('#selectDate').val());
                if(selectDate.getDate() == d.getDate())
                {
                    $('.plan_time_type').find('option').each(function(){
                        if(d.getHours() >=12 && $(this).val() == 2){
                            $(this).attr('disabled','disabled');
                            $(this).css('background','#cccccc');
                        }
                        if(d.getHours() >=18 && $(this).val() == 3){
                            $(this).attr('disabled','disabled');
                            $(this).css('background','#cccccc');
                        }
                    });
                }
                else
                {
                    $('.plan_time_type').find('option').each(function(){
                        $(this).removeAttr('disabled');
                        $(this).removeAttr('style');
                    });
                }
            }
        })
        $(".plan_time_type").val(0);
        $(".plan_time_type").attr('selected');
        $("#show_setTime").html('');
    }
    $('.setok').click(function() {
        $('#selectDate').val().length==0?console.log(1):console.log(2);
    })
    //服务时间类型为5时候，显示具体时间下拉框
    $("#plan_time_type").on('click',function(){
        var id = $(this).val();
        var selectDate = $("#selectDate").val();
        if(selectDate==''){
            $("#plan_time_msg").html('请选择日期');
            $("#plan_time_msg").css('display','block');

            $(this).attr('disabled',true);
            return false;
        }
        if(id==5){
            $.ajax({
                url:'/order/set-time',
                type:'POST',
                data:{time_type:id,date:selectDate},
                success:function (msg) {
                    $("#show_setTime").html(msg);
                }
            })
            $("#show_setTime").css('display','block');
        }else{
            $("#show_setTime").css('display','none');
        }
    })
    // 指派切换
    $('input[name="assign"]').on('change', function(event) {
        event.preventDefault();

        var _self = $(this).parent('span');
        if($(this).attr('id')=='assign1'){
            $('.drop-dw-layerbox').show();
            var addressId  = $("#account_address").val();  //客户地址id
            if(addressId != 0){
                mechanismInit(addressId);
            }
        }else{
            $('#plan_assign_msg').hide();
            $('.drop-dw-layerbox').hide();
            $('input[name="service_depart_id"]').prop('checked',false);
        }
    });
    function clickCategory(el) {
        var id = $(el).next('input').val();
        var addressId = $("#account_address").val();  //客户地址id
        var _this = $(el).parent('span');
        $.getJSON('/organization/ajax-get-department',{'pid':id,"service_area":"1","address_id":addressId},function (data) {
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>"
                $(data.data).each(function(i,el) {
                    ulData +="<li><span>";
                    el.exist==1?ulData+= '<i class="icon2-shixinyou tree-i"  onclick="clickCategory(this)"></i>':'';
                    ulData+='<input type="radio" id="a'+el.id+'" value="'+el.id+'" name="service_depart_id" >  <label for="a'+el.id+'">'+el.name+'<i class="gou-i"></i></label></span></li>';
                });
                ulData +="</ul>";
                _this.parent('li').append(ulData);
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');


            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })
    }
    // 添加服务产品
    var addAccountId = $('#account_id').val();
    var addName = [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,31,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50];
    var intent = "";
    var totalProduct = 0;
    $('#addProductList').on('click',function() {
        var productXml = '',option='',tdXml='',tdXmlOption='';
        var number = addName[0];
        var banArray = [];
        $('.add-product-list select').each(function(i) {
            // banArray.push($(this).find('option:selected').attr('sn-id'));
            banArray.push($(this).val());
        });
        var flag = true;
        if($('#account_id').val()==""){
            flag = false;
            $('#account_msg').text("请填写客户");
            $('#account_msg').show();
        }
        console.log(proving('input[name="service_type',true));
        if(proving('input[name="service_type',true)){
            flag = false;
            $('#service_type_msg').show();
            $('#service_type_msg').text('请选择服务类型');
        }
        if($('#account_service_brand').val()=="0"||$('#account_service_brand').val()==""){
            flag = false;
            $('#brand_msg').show();
            $('#brand_msg').text('请选择服务产品');
        }
        if(flag){
            $.ajax({
                url:'/order/get-sale-info',
                data:{account_id:$('#account_id').val(),intent:intent},
                type:'POST',
                dataType:'json',
                success:function(data){
                    $.each(data.data,function(i, el) {
                        option +='<option value="'+el.id+'"';
                        // el.sn_id!=null?option +='sn-id="'+el.id+el.sn_id+'"':option+='sn-id="'+el.id+'"';
                        option+='" data-name="'+el.unit+'" data-intent="'+el.intent+'"';
                        $.each(banArray,function(i,ele) {
                            // ele==el.id?option+= 'disabled="disabled"':'';
                            // var snId = el.sn_id!=null?el.id+el.sn_id:el.id;
                            ele==el.id?option+= 'style="display:none"':'';
                        });
                        option+='>'+el.value+'</option>';
                    });
                    productXml+='<div class="add-product-list width540-box"><div class="width390 padding-left0"><select type="text" class="form-control order'+number+'" data-id="'+number+'" name="sale_order_id" ><option value="0">请选择</option>'+option+'</select><p class="misplacement">请选择服务产品</p></div><div  class="width120 padding-right0 product-jia"><i class="icon1-cha"></i><input type="text" maxlength="9" onkeyup="clearNoNum(this)" class="form-control padding-right0"  data-id="'+number+'" value="1"><span class="unit unit'+number+'"  data-id="'+number+'"></span><p class="misplacement">请填写数量</p></div><a href="javascript:" class="delete-a" data-id="'+number+'">移除</a><div class="record"></div></div>'
                    data.data.length>$('.add-product-list').length?$('#addProductBox').append(productXml):alert('最多添加'+data.data.length+'个');
                    // $('#addProductBox').append(productXml);
                    for (var i = 1; i < $('.add-product-list').length+1; i++) {
                        tdXmlOption += '<option value="'+i+'">订单'+i+'</option>';
                    }
                    $('.dismantle-order-select').html(tdXmlOption);
                    tdXml += '<tr class="tr'+number+'"><td class="td1">'+$(".order"+number).find('option:selected').text()+'*0'+$(".unit"+number).text()+'</td><td  class="td2"><select name="order" class="dismantle-order-select" id="dismantleOrder1">'+tdXmlOption+'</select></td></tr>';
                    // tdXml='<tr><td>产品sfagfshsdhd*9台</td><td><select name="order" class="dismantle-order-select" id="dismantleOrder1"><option value="2">订单1</option></select></td></tr>'
                    // console.log(tdXml);
                    $('#dismantleOrderTbody').append(tdXml);
                    selectGray ();
                    addAccountId =  $('#account_id').val();
                    addName.splice(0,1);
                    window.totalProduct = data.data.length;
                    if(totalProduct <= $('.add-product-list').length){
                        $('#addProductList').hide();
                    }
                    //拆单展开收起
                    toggleCD();
                }

            });
        }else{
            // alert('请选择客户');
        }

    })
    $(document).on('click','.delete-a',function() {
        var _thatId = $(this).siblings('.width390').find('option:selected').val();
        var _thatHtml = $(this).siblings('.width390').find('option:selected').prop('outerHTML');
        //modify by liquan
        $('.add-product-list select').append(_thatHtml);
        /* $('.add-product-list select').find('option').each(function(i) {
             $(this).parent('select').append(_thatHtml);
             if($(this).val()==_thatId){
                 $(this).show()
             }else{
                 $(this).parent('select').append(_thatHtml);
                 return false;
             }

         })*/
        var type = "<?php echo $type;?>";
        $(this).parent('.add-product-list').remove();
        if(type == 1 && $('.add-product-list').length == 1){
            $('.delete-a').hide();
        }
        var dataId = $(this).attr('data-id');
        addName.push(dataId);
        addName.sort(function(a,b) {
            return a-b;
        })
        if(type != 1){
            $('.tr'+dataId).remove();
            toggleCD();
            if(window.totalProduct > $('.add-product-list').length){
                $('#addProductList').show();
            }
        }
    });

    //拆单展开收起
    var toggleFlag = true;
    function toggleCD() {
        var totalTr = $('#dismantleOrderTbody tr').length;
        console.log(totalTr);
        if(totalTr > 5){
            $('#toggleCDBtn').show();
        }
        else {
            $('#toggleCDBtn').hide();
        }
        $('#dismantleOrderTbody tr').each(function (i) {
            if(i > 4 && $('#toggleCDBtn').html() == '展开'){
                $(this).addClass('trHidden');
            }else{
                $(this).removeClass('trHidden');
            }
        });
    }

    $('#toggleCDBtn').click(function () {
        if(toggleFlag == true){
            $('#toggleCDBtn').html('收起');
            $('#dismantleOrderTbody tr').removeClass('trHidden');
        }
        else {
            $('#toggleCDBtn').html('展开');
            toggleCD();
        }
        toggleFlag = !toggleFlag;
    });

    var unlock = '';
    $(document).on('click','.add-product-list select',function() {
        unlock = $(this).val();
    })
    // 拆单
    $(document).on('change','.add-product-list select',function() {
        var _that = $(this);
        $('.add-product-list select').each(function(i) {
            if(_that.attr('class')!=$(this).attr('class')){
                $(this).find('option').each(function() {
                    // $(this).val()== _that.val()?$(this).attr('disabled',true):'';
                    // $(this).val()==unlock?$(this).attr('disabled',false):'';
                    $(this).val()== _that.val()?$(this).hide():'';
                    $(this).val()==unlock?$(this).show():'';
                })
            }

        })
        _that.next('.misplacement').hide();
        _that.parent('div').siblings('.product-jia').find('.unit').text(_that.find('option:selected').attr('data-name'));
        var text = _that.find('option:selected').text() +' * '+_that.parent('div').next('.product-jia').find('input').val()+_that.parent('div').next('.product-jia').find('.unit').text();
        $('.tr'+_that.attr('data-id')).find('.td1').text(text);
        $('.tr'+_that.attr('data-id')).attr({
            'data-name':_that.val(),
            'data-number':+_that.parent('div').next('.product-jia').find('input').val()
        })

        if($('input[name="service_type"]:checked').val()=="<?php echo Yii::$app->params['chaidan']['workType'];?>"){
            $('.record').hide();
            // $(this).parent('div').siblings('.record').html('');

        }else{
            var recordXml ="";
            var that = _that;
            var url = "/account/service-record?account_id="+$('#account_id').val()+"&sale_order_id="+$(this).val()
            $.ajax({
                url:url,
                type:'GET',
                dataType:'json',
                success:function(data) {
                    recordXml +='<p style="margin-bottom: 5px;">服务记录：</p><ul>';
                    $.each(data,function(i,el) {
                        var date=new Date();
                        if(i<3){
                            var recordArr = new Array();
                            if(el.work_stage_desc){
                                recordArr.push(el.work_stage_desc);
                            }
                            if(el.finsh_service_time){
                                recordArr.push(timestampToTime(el.finsh_service_time,"ymd"));
                            }
                            if(el.service_record){
                                recordArr.push(el.service_record);
                            }
                            if(recordArr.length > 0 ){
                                recordXml +='<li><a target="_blank" href="/order/view?order_no='+el.order_no+'">'+(recordArr.join(' - '))+'</a></li>';
                            }
                        }
                    })
                    recordXml+='</ul>'
                    data.length>3? (recordXml+='<a href="javascript:" class="see-more" style="color:#999;" onclick="seeMore(\''+url+'\')">查看更多</a>'):'';
                    that.parent('div').siblings('.record').html(recordXml);
                    data.length>0? that.parent('div').siblings('.record').show():that.parent('div').siblings('.record').hide();
                }

            })
        }

    })
    $(document).on('blur','.product-jia input',function() {

        var  dataId = $(this).attr('data-id');
        var text = $('.order'+dataId).find('option:selected').text() + ' * '+ $(this).val() + $('.unit'+ dataId).text();
        $('.tr'+dataId).find('.td1').text(text);
        $('.tr'+dataId).attr({
            'data-name':$('.order'+dataId).val(),
            'data-number':$(this).val()
        })
        $(this).val()>0?$(this).siblings('.misplacement').hide():$(this).siblings('.misplacement').show();
        // 保留两位小数^\d+(\.\d{0,2})?$  整数^[1-9]\d*$
        // if(/^\d+(\.\d{0,2})?$/.test($(this).val())){
        //    $(this).siblings('.misplacement').hide()
        // }else{
        //    $(this).siblings('.misplacement').text('请正确填写');
        //    $(this).siblings('.misplacement').show();
        // }


    });
    // 请求服务产品数据
    function getProduct() {




    }
    //选择服务类型
    $('input[name="service_type"]').on('change',function() {
        $("#service_type_msg").hide();
        $('.record').html('');
         $('.record').hide();
        $.ajax({
            url:'/flow/get-flow-stage',
            type:"POST",
            dataType:'json',
            data:{id:$(this).val(),is_default:2},
            success:function(data) {
                var procedureXml = "";
                if(data.code == 200){
                    $.each(data.data,function(i,el) {
                        procedureXml += '<div class="radioLable"><input type="radio" name="work_stage" value="'+el.id+'"  id="work_stage'+el.id+'"><label for="work_stage'+el.id+'">'+el.title+'</label></div>'
                    })
                    $('#tbodyContent').html(procedureXml);
                    $(".field-flow").show();
                }else{
                    $('#tbodyContent').html(procedureXml);
                    $(".field-flow").hide();
                }

            }
        })
        for (var i = 2,length=$('.add-product-list').length+1; i < length; i++) {
            addName.push(i);
        }
        addName.sort(function(a,b) {
            return a-b;
        })
        $('#addProductBox').html('');
        $('#dismantleOrderTbody').find('tr').each(function(i) {
            $(this).attr('class')!='tr1'?$(this).remove():$(this).find('.td1').text('请选择服务产品');$('#dismantleOrder1').html('<option value="1">订单1</option>');
        })
        // $('#dismantleOrderTbody').html('');
        var dismantle_status = "<?php echo $dismantling_status;?>";
        if($(this).val()=="<?php echo Yii::$app->params['chaidan']['workType'];?>"){
            addJudge=1;
            $('#faultImg').hide();
            $('.record').hide();
            $('#qualityState').hide();
            if(dismantle_status == 1){ //拆单开关   1开启 2 关闭
                $('#dismantleOrder').show();
            }

            // var tdXmlOption = '<option value="1">订单1</option>';
            // var tdXml = '<tr class="tr1"><td class="td1"></td><td  class="td2"><select name="order" class="dismantle-order-select" id="dismantleOrder1">'+tdXmlOption+'</select></td></tr>'
            // $('#dismantleOrderTbody').append(tdXml);
            // $('#dismantleOrder1').parent('td').prev('td').text($('#account_service_brand').text()+'*'+ $('#productIpt1').val()+$('#productIpt1').next('span').text());

        }else{
            addJudge=3;
            $('#faultImg').show();
            $('#serviceRecord').show();
            $('#qualityState').show();
            $('#dismantleOrder').hide();
        }
        var type = "<?php echo $type;?>";
        if(type != 1){
            var acount_id = $("#account_id").val();
            service_brand(acount_id);
        }
    })
    $('#account_service_brand').on('change',function() {
        var option="",_that=$(this);
        var banArray = [];
        $('.add-product-list select').each(function(i) {
            // banArray.push($(this).find('option:selected').attr('sn-id'));
            banArray.push(_that.val())
        })
        if(intent != _that.find('option:selected').attr('data-intent')){
            $("#addProductBox").html('');
            $('#dismantleOrderTbody tr').each(function(i) {
                if($(this).attr('class')!='tr1'){
                    $(this).remove();
                }
            })
            intent = _that.find('option:selected').attr('data-intent');
            $.ajax({
                url:'/order/get-sale-info',
                data:{account_id:$('#account_id').val(),intent:intent},
                type:'POST',
                dataType:'json',
                success:function(data) {
                    //modify by liquan
                    if(data.data.length>1){
                        option+='<option value="">请选择</option>'
                        $.each(data.data,function(i, el) {
                            option +='<option value="'+el.id+'"';
                            // el.sn_id!=null?option += 'sn-id="'+el.id+el.sn_id+'"':option +='sn-id="'+el.id+'"';
                            option+='" data-name="'+el.unit+'" data-intent="'+el.intent+'"';
                            $.each(banArray,function(i,ele) {
                                // ele==el.id?option+= 'disabled="disabled"':'';
                                // var snId = el.sn_id!=null?el.id+el.sn_id:el.id;
                                ele==el.id?option+= 'style="display:none"':'';
                            });
                            option+='>'+el.value+'</option>';
                        });
                        $('#addProductBox').find('select[name="sale_order_id"]').each(function(i) {
                            $(this).html(option);
                        });
                        $("#addProductList").show();
                    }else{
                        $("#addProductList").hide();
                    }
                }
            })
           /* $('#dismantleOrderTbody tr').each(function(i) {
                if(_that.attr('class')!='tr1'){
                    _that.find('.td1').text('请选择产品')
                }
            })*/
        }
        setTimeout(function() {
            var text = _that.find('option:selected').text() +' * '+_that.parent('div').next('.product-jia').find('input').val()+$('.unit').text();
            $('.tr1').find('.td1').text(text);
            $('.tr1').attr({
                'data-name':_that.val(),
                'data-number':$('#productIpt1').val()
            })
        },200)

    })
    $('#productIpt1').blur(function() {
        // $('#dismantleOrder1').parent('td').prev('td').text($('#account_service_brand').find('option:selected').text()+'*'+ $(this).val()+$(this).next('span').text());
        $('.tr1').find('.td1').text($('#account_service_brand').find('option:selected').text()+'*'+ $(this).val()+$(this).next('span').text());
        $('.tr1').attr({
            'data-name':$('#account_service_brand').val(),
            'data-number':$(this).val()
        })
    });
    $(document).on('click','input[type="radio"]',function() {
        $(this).parents('.addinput-list').find('.misplacement').hide();
    })
    //循环验证
    function proving(el,bool) {
        var dd = bool;
        $(el).each(function(i,el) {
            if(el.checked){
                dd =!bool;
            }
        })
        return dd;
    }
    // 查看更多记录
    var seeMoreLayer='';
    function seeMore(el) {
        seeMoreLayer=layer.open({
            title:"查看更多记录",
            type: 2,
            skin: 'yourclass', //加上边框
            area: ['700px', '450px'], //宽高
            content:el
        });
    }
    //查看配件包，物料清单
    $(".viewProductBag").click(function(){
        var prodId = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['查看物料清单', 'font-size:18px;'],
            area: ['800px', '500px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/product/materials-detail?id='+prodId,
        });
        return false;
    });
    //提供给弹框调用,不要删除
    function showAlert(message) {}
</script>


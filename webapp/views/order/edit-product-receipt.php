<?php
/**
 * Created by PhpStorm.
 * User: quanl
 * Date: 2018/11/19
 * Time: 14:00
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = '编辑产品回单信息';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--引入CSS-->
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/us-icon.css'); ?>?v=<?php echo time(); ?>">
<!-- 产品信息 -->
<section id="main-content">
    <div id="app" class="panel-body" v-cloak>
        <us-bus-title title="编辑产品回单信息" border>
            <div style="float: right">
                <us-button type="primary" @click="save">保存</us-button>
                <us-button @click="history.go(-1)">返回</us-button>
            </div>
        </us-bus-title>
        <us-form label-whl="100px"
                 item-whl="380px"
                 wrap-whl="200px"
                 name="save"
                 :styles="{ paddingTop : '24px'}">
            <us-form-row>
                <us-form-item label="产品编号：" type="mini">
                    <us-text-content>{{product.prodNo}}</us-text-content>
                </us-form-item>
                <us-form-item label="产品属性：" type="mini">
                    <us-text-content>{{product.intentionType}}</us-text-content>
                </us-form-item>
                <us-form-item label="产品名称：" type="mini">
                    <us-text-content>{{product.prodName}}</us-text-content>
                </us-form-item>
            </us-form-row>
            <us-form-row>
                <us-form-item label="品牌：" type="mini">
                    <us-text-content>{{product.brandName}}</us-text-content>
                </us-form-item>
                <us-form-item label="产品型号：" type="mini">
                    <us-text-content>{{product.model}}</us-text-content>
                </us-form-item>
                <us-form-item label="购买日期：" type="mini">
                    <us-text-content>{{product.buyTime}}</us-text-content>
                </us-form-item>
            </us-form-row>
            <us-form-row>
                <us-form-item label="出厂日期：" type="mini">
                    <us-text-content>{{product.produceTime}}</us-text-content>
                </us-form-item>
                <us-form-item label="质保状态：" type="mini">
                    <us-text-content>{{product.scope}}</us-text-content>
                </us-form-item>
                <us-form-item label="质保期：" type="mini">
                    <us-text-content>{{product.scpeNum}}</us-text-content>
                </us-form-item>
            </us-form-row>
            <us-form-item label="序列号：">
                <us-ipt-text v-model="product.sn"></us-ipt-text>
            </us-form-item>
            <us-form-item label="备注：" whl="600px">
                <us-ipt-textarea whl="500px" v-model="product.remark" word-size="200"></us-ipt-textarea>
            </us-form-item>
            <us-form-item label="凭证：" whl="990px">
                <us-upload upload-url="/upload/upload?source=fws"
                           suffix="jpg,jpeg,png,bmp"
                           :data.sync="images"
                           :size="10"
                           preview="0"
                           placeholder="上传图片"
                           :upload-error="uploadError"></us-upload>
            </us-form-item>
            <!--            -->
            <us-form-item v-for="(item, index) in product.extend_data"
                          v-show="item.isShow"
                          :required="!!item.notNull"
                          :key="index"
                          :label="item.keyValue+'：'" whl="600px">
                <us-ipt-textarea
                        v-if="Number(item.eType) === 2"
                        whl="500px"
                        :rule="createRule(item)"
                        v-model="item.value"
                        :word-size="(item.ext && item.ext.maxLength || 200)">
                </us-ipt-textarea>
                <us-ipt-text
                        v-else
                        :rule="createRule(item)"
                        v-model="item.value">
                </us-ipt-text>
            </us-form-item>

        </us-form>
    </div>
</section>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/common.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/order/edit-product-receipt.js?v=<?php echo $v; ?>"></script>

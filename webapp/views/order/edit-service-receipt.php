<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = '编辑服务回单信息';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">编辑服务回单信息</span>
</div>-->
<!--引入CSS-->
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/us-icon.css'); ?>?v=<?php echo time(); ?>">
<style type="text/css">
</style>
<section id="main-content">
    <div id="app" class="panel-body" v-cloak>
        <us-bus-title title="编辑服务回单信息" border>
            <div style="float: right">
                <us-button type="primary" @click="save">保存</us-button>
                <us-button @click="history.go(-1)">返回</us-button>
            </div>
        </us-bus-title>
        <us-form label-whl="90px" :styles="{ paddingTop : '24px'}" :before-submit="beforeSubmit">
            <us-form-item label="服务工单：">
                <us-upload upload-url="/upload/upload?source=fws"
                           suffix="jpg,jpeg,png,bmp"
                           :data.sync="work"
                           placeholder="上传图片"
                           :upload-error="uploadError"></us-upload>
            </us-form-item>
            <us-form-item label="现场拍照：">
                <us-upload upload-url="/upload/upload?source=fws"
                           suffix="jpg,jpeg,png,bmp"
                           :data.sync="scene"
                           placeholder="上传图片"
                           :upload-error="uploadError"></us-upload>
            </us-form-item>
            <us-form-item label="服务记录：">
                <us-ipt-textarea whl="500px" v-model="record" word-size="200"></us-ipt-textarea>
            </us-form-item>
        </us-form>
        <!--        -->
        <form ref="save" style="display: none" method="post" action="/order/edit-service-receipt">
            <input type="hidden" name="order_no" :value="orderNo">
            <input type="hidden" name="work_no" :value="workNo">
            <input type="hidden" name="service_record" :value="record">
            <input type="hidden" v-for="(file, index) in work"
                   :name="'work_img['+index+']'" v-if="file.src" :value="file.src">
            <input type="hidden" v-for="(file, index) in scene"
                   :name="'scene_img['+index+']'" v-if="file.src" :value="file.src">
        </form>
    </div>
</section>
<script type="text/javascript">
    (function ( jsStr ) {
        'use strict';
        try {
            window.$$response = JSON.parse( jsStr );
        } catch ( e ) {
            console.error( e );
        }
    })( '<?php echo $data;?>' );
</script>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/order/edit-service-receipt.js?v=<?php echo $v; ?>"></script>

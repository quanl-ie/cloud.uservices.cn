<?php
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use backstage\assets\AppAsset;
use yii\helpers\Url;
use webapp\models\Menu;
$this->title = '编辑菜单';
$this->params['breadcrumbs'][] = ['label' => '菜单管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    .modal-content {border: none;border-radius: 0px;box-shadow: inherit !important;}
    .ck_role .checkbox{float:left;margin-right:10px;}
    .radio{display: inline-block;padding-left: 50px;margin-top: -5px;}
    .radio input[type="radio"]{position: static;margin-left: 0}.radio{margin-right: 40px;}
    input[type="radio"]{position: static;}
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body"><?php
                    $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'form-horizontal',
                            'id' => 'form2',
                            'name' => 'form1',
                            'enctype' => 'multipart/form-data',

                        ],
                        'fieldConfig' => [
                            'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-xs-6\">{input}{error}</div>",
                            'labelOptions' => ['class' => 'col-xs-3 control-label text-right'],
                        ]
                    ]);
                    ?>
                    <?php echo $form->field($model, 'parent_id')->dropDownList(Menu::getParent(),['id' => 'parentid']);?>
                    <?php echo $form->field($model, 'name')->input('text',['maxlength' => 30]);?>
                    <?php echo $form->field($model, 'url')->input('text',['maxlength' => 100]);?>
                    <?php echo $form->field($model, 'weight')->input('text',['maxlength' => 20]);?>
                    <?php echo $form->field($model, 'class')->input('text',['maxlength' => 50]);?>
                    <?php echo $form->field($model, 'is_show')->radioList(Menu::IsShow());?>
                    <div class="form-group">
                        <label class="col-xs-3"> </label>
                        <div class="col-xs-6 text-right">
                            <input type="submit" class="btn btn-orange mgr20 " style=" color:#FFF;" value="提交" />
                            <a type="button" class="btn btn-green-line-min btnClose" href="/menu/index">取消</a>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script>

window.onload = function(){
    if(<?=$model->parent_id?>!=0)
        $('#url').show();
    $("#parentid").change(function() {
        var parentid = $("#parentid").val();
        if(parentid!=0)
        {
            $("#url").show();
            $("#weight").hide();
        }
        else
        {
            $('#url').hide();
            $("#weight").show();
        }
    })
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    <?php endif;?>
}
</script>

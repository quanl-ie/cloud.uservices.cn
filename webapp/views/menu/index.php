<?php
use backstage\assets\AppAsset;

$this->title = '菜单管理';
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    td {
         cursor:pointer
    }
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-13">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="box-header" style="height: 50px;">
                        <a href="/menu/add">
                            <button type="button" class="btn btn-orange">添加菜单</button>
                        </a>
                    </div>
                    <div class="form-group">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead bgcolor="#455971">
                                <tr>
                                    <th>模块名称</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($module as $key => $value):?>
                                    <tr>
                                        <td><span onclick="showMenu(<?=$value['id']?>)"><?=$value['name']?></span>
                                            <div class="menu_<?=$value['id']?> menuArr" style="margin-left: 80px;display:none"></div>
                                        </td>
                                        <td>
                                            <a href="/menu/edit?id=<?=$value['id']?>">编辑</a>
                                            <a href="/menu/menu-del?id=<?=$value['id']?>">删除</a>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    setTimeout(function () {
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    },500);
    <?php endif;?>
}
</script>
<?php
$this->registerJsFile('@web/js/menu.js');
?>
  
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;

$this->title = '消息中心';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
    .pagination{
        text-align: center;
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title"><?= Html::encode($this->title) ?></span>
</div>
<section id="main-content">
    <div class="row">
    <div class="col-md-12">
        <ul id="tabs" class="nav nav-tabs">
            <li id="li_bus_msg" class="switch <?php echo $msg_type==2?' active':'';?>" value="2">
                <a href="/message?msg_type=2"  aria-expanded="">业务消息</a>
            </li>
            <li id="li_sys_msg" class="switch <?php echo $msg_type==1?' active':'';?>" value="1">
                <a href="/message?msg_type=1" aria-expanded="">系统消息</a>
            </li>
        </ul>
     </div>
    </div>
    
    <div class="tab-content">
      <!--业务消息-->
        <div class="tab-pane active"id="bus_msg"> 
    　　　　        <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead bgcolor="#455971">
                            <tr>
                                <th>消息</th>
                                <th width="200">时间</th>
                            </tr>
                            </thead>
                            <tbody class="msg_list">
                            <?php if(!empty($dataProvider)){?>
                            <?php foreach ($dataProvider as $k=>$v){?>
                            <tr>
                                <td style="text-align: left;"><a href="<?php echo $v['link_info'];?>" style="color: <?php echo $v['read_status']==1?' #888888':'';?>"><?php echo $v['title'];?></a></td>
                                <td><?php echo $v['create_time'];?></td>
                            </tr>
                            <?php }
                            }else{
                                echo '<tr><td colspan="2">暂无数据</td></tr>';
                            }?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo $pageHtml;?>
                    </div>
                </div>
            </div>　　
        </div>
      <!--系统消息-->
        　　<div class="tab-pane"id="sys_msg"> <!--ID要对应上面的第2个 item 的URL-->
        　　　　 系统消息....　　
        　　</div>
    </div>

</section>
<script type="text/javascript">
window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    window.parent.document.location.reload();
    <?php endif;?>
}
</script>
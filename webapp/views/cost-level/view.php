<?php
$this->title = '结算规则详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="/css/iconfont.css">
<style type="text/css">
    .region-box-ul,.region-box-title{margin-bottom: 0;}
    .icon-shouqi1{position: absolute;right: 25px;top: 10px;}
    .form-control[readonly]{cursor: pointer;background: #fff}
    .add-payService>.information-list>.addinput-list{margin-left: 120px;}
    li>input[type="radio"]{position:static;clip:rect(0,0,0,0);opacity:0}
    li>input[type="radio"]:checked+label::before{background-color:#2693ff;background-clip:content-box;padding:2px;width:15px;height:15px}
    li>input[type="radio"]+label::before{content:"\a0";display:inline-block;vertical-align:middle;font-size:18px;width:15px;height:15px;margin-right:10px;border-radius:50%;border:1px solid #2693ff;margin-top:-3px}
    .information-list{margin-top: 0;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">收费标准查看</span>
    <span class="btn btn-success jn-btn icon iconfont" style=" color:#FFF;float:right;width:auto;" onclick="javascript:location.href='/cost-level/index';">返回</span>
</div> 
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="information-list">
                        <label class="name-title-lable" >收费标准名称：</label>
                        <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                            <input type="hidden" id="serviceProviderId" value="<?=isset($data['id']) ? $data['id'] : '' ?>">
                            <input type="hidden" id="costId" value="<?=isset($data['id']) ? $data['id'] : '' ?>">
                            <input type="text" value="<?= isset($data['name']) ? $data['name'] :'' ?>" class="form-control" style='border: 0px solid #fff;'>
                        </div>
                    </div>

                    <!--<div class="information-list">

                        <label class="name-title-lable" >订单结算标准：</label>

                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 addinput-list" style="margin-bottom: 10px;">
                                <div>
                                    <input type="text" value=""  id="CostDate" class="form-control" style='border: 0px solid #fff;'>
                                </div>
                        </div>
                    </div>-->
                    <div class="information-list"  style="height: auto;">

                        <label class="name-title-lable" >服务城市：</label>

                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 addinput-list" style="margin-bottom: 20px;">
                                
                                <table>
                                    <thead>
                                    <tr>
                                        <th>省份</th>
                                        <th>城市</th>
                                        <th style="width: 45%;">区县</th>

                                    </tr>
                                    </thead>
                                    <tbody  id="cityBox">
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                        </div>
                    </div>
                    <div class="information-list"  style="height: auto;">

                        <label class="name-title-lable" >结算项目明细：</label>

                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 addinput-list" style="margin-bottom: 30px;">
                            <table>
                                <thead>
                                <tr>
                                    <th>品牌</th>
                                    <th>产品类目</th>
                                    <th>服务类型</th>
                                    <th style="width: 35%;">结算收费项目</th>
                                </tr>
                                </thead>
                                <tbody id="detailedTbody">

                                </tbody>
                            </table>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>
    </div>

</section>
<script type="text/javascript">

    var ruleId,serviceProviderId;
    serviceProviderId = $("#serviceProviderId").val();
    costId = $("#costId").val();
    $(function() {
        
        $('#'+ruleId).addClass('opt-rule-name');
        var Url = "/cost-level/rule-link-age?id="+costId;
        ergodic (Url);
    })
    
    var province,city,district;
    var tbody = document.getElementsByTagName('tbody')[1];
    function ergodic (Url) {
        $.ajax({
            url:Url,
            type:'POST',
            dataType:'json',
            success:function(data){
                if(data.code!=200){
                    alert(data.message);
                    return false;
                }
                //var costDete = data.data.costRule.real_cost_date;
                //$("#CostDate").val(costDete);
                var rule =  data.data.rule;
                province = data.data.province;
                city = data.data.city;
                district=data.data.district;
                var table = document.getElementById('cityBox');
                var num = null;
                var temp = null;
                var temp1 = null;
                function  countNum(proID) {

                    for (var i = 0; i < city.length; i++) {
                        if(city[i].parent_id==proID){
                            temp++;
                        }
                    }
                    num=temp;
                    temp=0;
                    return num;
                }

                var insertData = "";
                // insertData+="<tr><th>省份</th><th>城市</th><th>区县</th></tr>"
                for (var i = 0; i < province.length; i++) {
                    insertData+="<tr>";
                    insertData+="<td rowspan='"+countNum(province[i].region_id)+"'>"+province[i].region_name;
                    insertData+="</td>";
                    for (var j = 0; j < city.length; j++) {
                        if (city[j].parent_id==province[i].region_id) {
                            insertData+="<td>"+ city[j].region_name;
                            insertData+="</td>";
                            var districts = "";
                            for (var k = 0; k < district.length; k++) {
                                if (district[k].parent_id==city[j].region_id){
                                    districts+= district[k].region_name+"&nbsp;&nbsp;";
                                }
                            }
                            insertData+="<td>"+districts;
                            insertData+="</td>";
                            break;
                        }
                    };

                    if (countNum(province[i].region_id)>=2) {
                        for (var j = 0; j < city.length; j++) {
                            if (city[j].parent_id==province[i].region_id) {
                                temp1++;
                                if (temp1>=2) {
                                    insertData+="<tr>";
                                    insertData+="<td>"+ city[j].region_name;
                                    insertData+="</td>";
                                    var districts = "";
                                    for (var k = 0; k < district.length; k++) {
                                        if (district[k].parent_id==city[j].region_id){
                                            districts+= district[k].region_name+"&nbsp;&nbsp;";
                                        }
                                    }
                                    insertData+="<td>"+districts;
                                    insertData+="</td>";
                                    insertData+="</tr>";
                                }
                            }
                        };
                        temp1=null;
                    }
                }
                // table.innerHTML=insertData;
                $('#cityBox').html(insertData);
                // 结算项目明细
                var insert = "";
                var insert2 = "";
                var classList = "";
                var typeList = "";

                var length1 = 0;
                var length2 = 0;
                var brand_name_length = [];
                var calssList_length = [];

                for(item in rule) {
                    length1 = 0;
                    classList = rule[item].classList;
                    for(item2 in classList) {
                        length2 = 0
                        typeList = classList[item2].typeList;
                        for(item3 in typeList) {
                            length1++;
                            length2++;
                        }
                        calssList_length.push(length2)
                    }
                    brand_name_length.push(length1)
                }

                var itemNum = 0;
                var item2Num = 0;

                for(item in rule) {
                    length1 = 0;
                    insert2 += "<tr>";
                    insert2 += "<td rowspan=" + brand_name_length[itemNum++] + ">" + rule[item].brand_name + "</td>";
                    classList = rule[item].classList;
                    for(item2 in classList) {
                        length2 = 0;
                        insert2 += "<td rowspan=" + calssList_length[item2Num++] + ">" + classList[item2].class_name + "</td>";
                        typeList = classList[item2].typeList;
                        for(item3 in typeList) {
                            insert2 += "<td>" + typeList[item3].type_name + "</td>" +  "<td style='padding:10px 0'>";
                            length1++;
                            length2++;
                            itemList = typeList[item3].itemList;
                            for (var i = 0; i < itemList.length; i++) {
                                insert2 +="<div style='text-align:left;line-height:2'>&nbsp;&nbsp;&nbsp;"+ itemList[i] +"</div>";
                            };
                            insert2 +="</td></tr>"
                        }
                    }
                }
                $("#detailedTbody").append(insert2)
            }
        })
    }

    /*$('.rule-click').on('click',function() {
        ruleId = $(this).attr('id');
        var Url = "/cooporation/ajax-edit-rule?id="+ruleId+"&service_provider_id="+serviceProviderId;
        ergodic (Url);
        $('.rule-click').removeClass('opt-rule-name');
        $(this).addClass('opt-rule-name');
    });*/

</script>
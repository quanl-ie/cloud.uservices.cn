<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<!DOCTYPE html>
<html>
<head>
</head>
<style>
    body{background: #fff;}
    ul{padding-left: 20px; }
    .ul-box-h180 .depart .icon{top: 0;}
</style>
<div class="popup-boss" style="background: #fff;">
    <?php if(!empty($data)){?>
        <div class="col-md-8 information-list" style="height: auto;padding:0 15px;">
            <div class="ul-box-h180">
                <ul id="depart1" class="depart" class="depart" style="height:auto;padding-left: 0;">
                    <?php foreach ($data as $key=>$val){?>
                    <li>
                        <span>
                            <?php if($val['exist']==1){?>
                                <i class="icon2-shixinyou" onclick="getDepartment(this)"></i>
                            <?php }?>
                            <input type="checkbox" id="a<?php echo $val['id']?>" value="<?php echo $val['id']?>" name="category">
                             <label for="a<?php echo $val['id']?>"><?php echo $val['class_name']?><i class="gou-i"></i></label>
                        </span>
                    </li>
                    <?php }?>
                </ul>
            </div>   
        </div>
        <hr>
        <div  class="col-md-8 information-list" style="text-align: center;margin-bottom: 30px">
            <input type="button" id="closeIframe" class="btn bg-f7 mgr8" value="取消">
            <button id="transmit" class="btn btn-orange">确定</button>
        </div>
    <?php }else{?>
        <div class="no-data-page">
            <div>
                <div class="kbtp"></div>
                <p class="no-data-zi">您还没有配置过服务类型</p>
                <span class="btn btn-orange" id="add_link" >配置服务类型</span>
            </div>
        </div>
    <?php }?>
</div>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script>
    var category = [];
    var layerWindow = parent.layer.getFrameIndex(window.name); //获取窗口索引
    var parentIframeId = parent.$('#parentIframeContern').attr('data-id').split(',');
    var parentIframeName = parent.$('#parentIframeContern').find('p').text().split(',');
    $(function() {
        $.each(parentIframeId,function(i,el) {
            $('#a'+el).prop('checked',true);
        })
    })
    function getDepartment(el) {
        var id = $(el).next('input').val();
        var _this = $(el).parent('span');
            var ulData = '';
            if (_this.next('ul').size()==0) {
                $.post('/cost-level/get-class',{'pid':id},function (data) {
                ulData +="<ul>"
                $.each(data,function(i,el) {
                    ulData +="<li><span>";
                    el.exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="getDepartment(this)"></i>':'';
                    ulData+='<input type="checkbox" id="a'+el.id+'" value="'+el.id+'" name="category" >  <label for="a'+el.id+'">'+el.class_name+'<i class="gou-i"></i></label></span></li>';
                });
                ulData +="</ul>";
                _this.parent('li').append(ulData);
                $.each(parentIframeId,function(i,el) {
                    $('#a'+el).prop('checked',true);
                })
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');



            },'json')

            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
    }
    var idArray = [],nameArray1 = [];
    $(document).on('change','input[type="checkbox"]',function(event) {
        var that = $(this);
        if($(this).is(":checked")){
            idArray.push($(this).val());
            nameArray1.push($(this).next('label').text());
        }else{
            idArray.removeVal($(this).val());
            nameArray1.removeVal($(this).next('label').text());
        }
         $.each(parentIframeId,function(i,el) {
            if(that.val()==el){
                parentIframeId.removeVal(el);
                parentIframeName.removeVal($('#a'+el).next('label').text());
                console.log($('#a'+el).next('label').text());
            }
        })
    })
    Array.prototype.removeVal = function(val) {
        for (var i = 0; i < this.length; i++) {
            if(this[i]==val){
                this.splice(i,1);
                break;
            }
        }
    }
    $('#transmit').bind('click',function() {
        var mergeIdArray = idArray.concat(parentIframeId);
        var nameArrayContent = nameArray1.concat(parentIframeName).join(',');
        var nameArray = '<p style="line-height:1.5;margin-top:7px;">'+nameArrayContent+'</p>';
         parent.$('#parentIframeContern').attr('data-id',mergeIdArray)
        parent.$('#parentIframeContern').html(nameArray);
        parent.categoryIdArray=mergeIdArray;
        parent.layer.close(layerWindow);
    });
    var index = parent.layer.getFrameIndex(window.name);
    //关闭iframe
    $('#closeIframe').click(function(){
        parent.layer.close(index);
    });
    $('#add_link').click('click',function() {
        parent.layer.close(index);
        parent.location.href="/category/index";
    });
</script>
</body>
</html>
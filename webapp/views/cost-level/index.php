<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = '收费标准管理';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .search-box-kuang>.col-sm-6{
    padding-left: 0;
    position: relative;
    }
    .icons-calendar{
        position: absolute;
        right: 24px;top: 9px;
    }
    .popover{max-width: 500px !important;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">收费标准管理
    <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="用于创建售后订单并给予合作服务商价格参考"></span>
    </span>
    <!-- <div class="right-btn-group">                                
        <a href="/cost-level/add" >添加</a>
    </div>   -->
    <?php if($directCompanyId == $directTopId){?>
        <?php if(in_array('/cost-level/add',$selfRoles)){?>
            <a href="/cost-level/add" class="btn btn-success jn-btn" style=" color:#FFF;float:right;width:auto;">添加</a>
        <?php };?>
    <?php }?>
    <!--<span class="btn btn-success jn-btn" style=" color:#FFF;float:right;width:auto;" id="addParentIframe"  data-toggle="modal" data-target="#scrollingModal">添加</span>-->
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php if (!empty($data)){ ?>
                    <form action="/sale-order/index" class="form-horizontal form-border" id="form">
                        <div class="form-group jn-form-box">

                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead bgcolor="#455971">
                                <tr>
                                    <th>收费标准名称</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                    
                                    <?php foreach ($data as $val): ?>
                                    <tr>
                                        <td><?=$val['level_title'] ? $val['level_title'] : '' ; ?></td>
                                        <td><?php if($val['status'] == 1){echo '已启用';}elseif($val['status'] == 2){echo '已禁用';}?></td>
                                        <td>

                                            <?php if($directCompanyId == $directTopId){?>
                                                <?php if(in_array('/cost-level/edit',$selfRoles)){?>
                                                    <a href="/cost-level/edit?id=<?= $val['id'] ?>" >编辑</a>
                                                <?php };?>
                                                <?php if(in_array('/cost-level/view',$selfRoles)){?>
                                                    <a href="/cost-level/view?id=<?= $val['id'] ?>" >查看</a>
                                                <?php };?>
                                                <?php if($val['status'] == 1){?>
                                                    <?php if(in_array('/cost-level/set-status',$selfRoles)){?>
                                                        <a href="/cost-level/set-status?id=<?= $val['id'] ?>&t=1" onclick="return confirm('您确定要禁用吗？',function(obj){ document.location.href = obj.href ;} , this);">禁用</a>
                                                    <?php };?>
                                                <?php }elseif($val['status'] == 2){?>
                                                    <?php if(in_array('/cost-level/set-status-on',$selfRoles)){?>
                                                        <a href="/cost-level/set-status-on?id=<?= $val['id'] ?>&t=2" onclick="return confirm('您确定要启用吗？',function(obj){ document.location.href = obj.href ;} , this);">启用</a>
                                                    <?php };?>
                                                <?php }?>
                                            <?php }else{?>
                                                <?php if(in_array('/cost-level/view',$selfRoles)){?>
                                                    <a href="/cost-level/view?id=<?= $val['id'] ?>" >查看</a>
                                                <?php };?>
                                            <?php }?>

                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <div class="col-xs-12 text-center  pagination">
                    </div>
                    <?php }else{ ?>
                        <div class="no-data-page">
                            <?php if($departmentId == $directTopId){?>
                                <div>
                                    <div class="kbtp"></div>
                                    <p class="no-data-zi">您还没有添加过收费标准</p>
                                    <?php if(in_array('/cost-level/add',$selfRoles)){?>
                                        <a  href="/cost-level/add" class="btn btn-success">添加收费标准</a>
                                    <?php };?>
                                </div>
                            <?php }else{?>
                                <div>
                                    <div class="kbtp"></div>
                                    <p class="no-data-zi">您还没有收费标准</p>
                                </div>
                            <?php }?>
                        </div>
                    <?php }; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".tip").popover();
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>',function(){
            window.parent.document.location.reload();
        });
        <?php endif;?>
        var  heightKs = ($(window).height())-265+'px';
        $('.no-data-page').height(heightKs);
    };
    $(window).resize(function(){
        var heightKs = ($(window).height())-265+'px';
        $('.no-data-page').height(heightKs);
    });
    //添加
    $("#addParentIframe").click(function () {
        layer.open({
            type: 2,
            title: '请选择收费项目',
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/cost-item/add', 'no'],
            end:function(){
//                window.parent.document.location.reload();
                location.reload();
            }
        });
    });

</script>
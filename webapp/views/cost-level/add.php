<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '收费标准管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    .region-box-ul,.region-box-title{margin-bottom: 0;}
    .addinput-list>.icon-shouqi1{position: absolute;right: 5px;top: 10px;}
    .form-control[readonly]{cursor: pointer;background: #fff}
    .add-payService>.information-list>.addinput-list{margin-left: 120px;}
</style>
<div class="jn-title-box no-margin-bottom">
    <p class="jn-title">
        收费标准管理
        <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-content="用于创建售后订单并给予合作服务商价格参考 "></span>
    </p>
</div> 
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <!-- <form  method="post" id="cost-rule"> -->
                        <div class="information-list">
                            <label class="name-title-lable" ><span class="red-i">*</span>收费标准名称：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <input type="text" id="level_title" name="level_title" value="<?= isset($level['level_title']) ? $level['level_title'] : '' ?>" data='<?= isset($level['id']) ? $level['id']: '' ?>' placeholder="请输入收费标准名称" maxlength="20" class="form-control inputxt" />
                                <div class="hover-right-hint">
                                    <div class="hover-right-hintbox">
                                        <i class="lingdang"></i>
                                        <p>用于创建售后订单并给予合作服务商价格参考</p>
                                    </div>
                                </div>                         
                            </div>
                        </div>
                        <div class="information-list">
                            <label class="name-title-lable" ><span class="red-i">*</span>服务城市：</label>
                            <?php if($province):?>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list bothEnds">
                               <div class="city3">
                                    <div class="region-box">
                                        <p class="region-box-title">选择服务省份</p>
<!--                                        <span class="whole-region-check">-->
<!--                                            <input type="checkbox" id="wholeProvince" class="wholeProvince" name="">-->
<!--                                            <label class="brand-label" for="wholeProvince"><i class="gou-i"></i>全部</label>-->
<!--                                        </span> -->
                                        <ul class="region-box-ul" id="provinceUl">
                                        <?php if (!empty($province)): ?>
                                        <?php foreach ($province as $key => $val): ?>
                                        <?php if (isset($provinceId) && in_array($val['region_id'], $provinceId)) : ?>
                                        <li>
                                            <input type="checkbox" id="<?=$val['region_id'] ?>" checked="checked" name="">
                                            <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                                        </li>
                                        <?php else : ?>
                                        <li>
                                            <input type="checkbox" id="<?=$val['region_id'] ?>" name="">
                                            <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                                        </li>
                                        <?php endif; ?>
                                        <?php endforeach ; ?>
                                        <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="city3">
                                    <div class="region-box">
                                        <p class="region-box-title">选择服务城市</p>
                                        <span class="whole-region-check">
                                            <input type="checkbox" id="wholeCity" name="">
                                            <label class="brand-label" for="wholeCity"><i class="gou-i"></i>全部</label>
                                        </span> 
                                        <ul class="region-box-ul" id="cityUl">
                                        <?php if (!empty($province)): ?>
                                        <?php foreach ($city as $key => $val): ?>
                                        <?php if (isset($cityId) && in_array($val['region_id'], $cityId)) : ?>
                                        <li>
                                            <input type="checkbox" id="<?=$val['region_id'] ?>" checked="checked" name="">
                                            <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                                        </li>
                                        <?php else : ?>
                                        <li>
                                            <input type="checkbox" id="<?=$val['region_id'] ?>" name="">
                                            <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                                        </li>
                                        <?php endif; ?>
                                        <?php endforeach ; ?>
                                        <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="city3">
                                    <div class="region-box">
                                        <p class="region-box-title">选择服务区/县</p>
                                        <span class="whole-region-check">
                                            <input type="checkbox" id="wholeArea" name="">
                                            <label class="brand-label" for="wholeArea"><i class="gou-i"></i>全部</label>
                                        </span> 
                                        <ul class="region-box-ul" id="areaUl">
                                        <?php if (!empty($province)): ?>
                                        <?php foreach ($district as $key => $val): ?>
                                        <?php if (isset($districtId) && in_array($val['region_id'], $districtId)) : ?>
                                        <li area="<?=$val['region_id'] ?>">
                                            <input type="checkbox" id="<?=$val['region_id'] ?>" checked="checked" name="">
                                            <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                                        </li>
                                        <?php else : ?>
                                        <li>
                                            <input type="checkbox" id="<?=$val['region_id'] ?>" name="">
                                            <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                                        </li>
                                        <?php endif; ?>
                                        <?php endforeach ; ?>
                                        <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="city3_fix"></div>
                            </div>
                            <?php else:?>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list bothEnds">
                                <span style="color: red">您还没有配置过服务区域</span>
                                <button onclick="addServiceArea()" class="btn btn-orange jn-btn">立即配置</button>
                            </div>
                            <?php endif;?>
                        </div>
                    <div class="information-list">
                        <label class="name-title-lable"><span class="red-i">*</span> 产品类目：</label>
                        <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list bothEnds" id="productCategory">
                            <a href="javascript:" class="btn btn-orange" id="parentIframe" style="color: #fff;width: 136px;display: block;">添加产品类目</a>
                            <div id="parentIframeContern" data-id="<?php echo isset($class_ids)?$class_ids:'';?>"><?php if(isset($class_ids_name)){?> <p style="line-height:1.5;margin-top:7px;"><?php echo isset($class_ids_name)?$class_ids_name:'';?></p> <?php }?></div>

                            <!--<div class="radioLable">
                                <input type="radio" name="class_id" value="-1" checked="checked"  id="class_id_0">
                                <label  for="class_id_0">通用</label>
                            </div>-->
                            <?php /*if (!empty($class)) : */?><!--
                                <?php /*foreach ($class as $key => $val) : */?>
                                    <div class="radioLable">
                                        <input type="radio" name="class_id" value="<?/*=$key */?>" id="class_id_<?/*=$key */?>">
                                        <label  for="class_id_<?/*=$key */?>"> <?/*=$val */?></label>
                                    </div>
                                <?php /*endforeach; */?>
                            --><?php /*endif; */?>
                            <div class="city3_fix"></div>
                            <div class="city3_fix"></div>
                            <div class="city3_fix"></div>
                            <div class="city3_fix"></div>
                        </div>
                    </div>

                        <div class="information-list">
                            <label class="name-title-lable" ><span class="red-i">*</span> 品牌：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list bothEnds" id="brand">
                               <div class="radioLable">
                                    <input type="radio" name="brand_id" value="-1" checked="checked"  id="brand_id_0">
                                    <label  for="brand_id_0">通用</label>
                                </div>
                                <?php if (!empty($brand)) : ?>
                                <?php foreach ($brand as $key => $val) : ?>
                                <div class="radioLable">
                                    <input type="radio" name="brand_id"  value="<?=$key ?>" id="brand_id_<?=$key ?>">
                                    <label  for="brand_id_<?=$key ?>"> <?=$val ?></label>
                                </div>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                            </div>
                        </div>

                        <div class="information-list">
                            <label class="name-title-lable" ><span class="red-i">*</span> 服务类型：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list bothEnds" id="serviceType">
                                <div class="radioLable">
                                    <input type="radio" name="type_id" value="-1" checked="checked"  id="type_id_0">
                                    <label  for="type_id_0">通用</label>
                                </div>
                                <?php if (!empty($type)) : ?>
                                <?php foreach ($type as $key => $val) : ?>
                                <div class="radioLable">
                                    <input type="radio" name="type_id" value="<?=$key ?>" id="type_id_<?=$key ?>">
                                    <label  for="type_id_<?=$key ?>"> <?=$val ?></label>
                                </div>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                            </div>
                        </div>
                        <div class="information-list">
                            <label class="name-title-lable" ><span class="red-i">*</span> 收费项目：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <div style="padding-bottom: 30px;display: none;" id="charge-column" >
                                    <table>
                                        <thead>
                                          <tr>
                                                <th>收费项目</th>
                                                <th>计量单位</th>
                                                <th>单价</th>
                                                <th>操作</th>
                                          </tr>
                                        </thead>
                                        <tbody id="tbodyContent">
                                        </tbody>
                                    </table>
                                </div>
                              <div>
                                  <a href="javascript:" class="btn btn-orange" id="addCharge" style="color: #fff;width: 136px;display: block;">添加收费项目</a>
                              </div>
                            </div>
                        </div>
                        <div class="information-list" style="padding-top: 30px"><p class="xian"></p></div>
                        <div class="information-list">
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <a href="javascript:" class="btn ignore jn-btn" id="cancel_set" data-id="<?php echo isset($cost_level_id)?$cost_level_id:''?>" data-ids="<?php echo isset($cost_level_item_ids)?$cost_level_item_ids:'';?>" style="border:1px solid #ccc;margin-right: 20px">取消</a>
                                <button class="btn btn-orange jn-btn" id="chargeStandard">提交</button>
                            </div>
                        </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </div>
    <!-- 弹窗信息 -->
    <div class="add-payService" id="add-payService" style="display: none">
        <h4 style="padding: 10px 20px;background: #F1F2F7;margin-top: 0">添加收费项目</h4>
        <div class="information-list">
            <label class="name-title-lable" ><span class="red-i">*</span> 收费项目：</label>
            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list" style="padding:0">
               <input type="text" id="payService" name="" class="form-control inputxt weekData" placeholder="请选择收费项目" readonly data_id="">
                <i class="icon iconfont icon-shouqi1"></i>
                <ul class="weekUl">
                    <?php if (!empty($cost_item)) : ?>
                    <?php foreach ($cost_item as $key => $val) : ?>
                        <li value="<?=$key?>"><?=$val ?></li>
                    <?php endforeach; ?>
                    <?php else:?>
                        <li onclick="addCostItem()">添加收费项目</li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable" ><span class="red-i">*</span> 计量单位：</label>
            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list" style="padding:0">
               <input type="text" name="" id="measurementUnit" class="form-control inputxt weekData" placeholder="请选择计量单位" data_id="" readonly>
                <i class="icon iconfont icon-shouqi1"></i>
                <ul class="weekUl">
                    <?php if (!empty($unit)): ?>
                    <?php foreach ($unit as $key => $val) : ?>
                    <li value="<?=$key?>"><?=$val ?></li>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable" ><span class="red-i">*</span> 单价：</label>
            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list" style="padding:0">
               <input type="text" id="unitPrice" datatype="*" sucmsg=" " nullmsg="请输入单价"   placeholder="请输入单价" maxlength="20" class="form-control inputxt"   onkeyup="clearNoNum(this)" ng-pattern="/[^a-zA-Z]/" />
            </div>
        </div>
        <p class="height10"></p>
        <hr>
        <div class="information-list text-center" style="padding-top: 15px">
                <a href="javascript:" class="btn ignore jn-btn" id="cancelPopup"  style="border:1px solid #ccc;margin-right: 20px">取消</a>
                <button class="btn btn-orange jn-btn" id="okPopup">提交</button>
            
        </div>
    </div>

</section>
<script type="text/javascript">
    $("#cancel_set").on('click',function(){
        var ids     = $('#cancel_set').attr('data-ids');
        var levelId = $('#cancel_set').attr('data-id');
        if(ids != '' && levelId !=''){
            $.ajax({
                url:'/cost-level/reset-item',
                type:'POST',
                data:{ids:ids,level_id:levelId},
                success:function (msg) {
                    window.location="/cost-level/index";
                }
            })
        }else {
            window.location="/cost-level/index";
        }
    });

    var id1=[],id2=[],id3=[],emptyId2=[],emptyId3=[],districtIds=[],categoryIdArray = '';
    $(function() {
        areaUl ();
        dataPull ();
        $(".tip").popover();
    })
    // 联动点击
    $('#provinceUl li input').on('change',function() {
        $('#wholeArea').prop("checked",false);
        $('#wholeCity').prop("checked",false);
        $('#areaUl').html('');
        var elDate = {
            Url:'/cost-level/city',
            element:$('#provinceUl li input'),
            data:{province:[]},
            box:$('#cityUl'),
            judge:1

        }
         linkage (elDate);
        if ($("#provinceUl li input[type=checkbox]:checked").length === elDate.element.length ) {
            $('#wholeProvince').prop("checked",true);
        }else{
            $('#wholeProvince').prop("checked",false);
        }
    });
    $('#cityUl').on('change','li input',function() {
        $('#wholeArea').prop("checked",false);
        var elDate = {
            Url:'/cost-level/district',
            element:$('#cityUl li input'),
            data:{citys:[]},
            box:$('#areaUl'),
            judge:2
        }
         linkage (elDate);
        var wholeCheck = $('#cityUl li input');
        if ($("#cityUl li input[type=checkbox]:checked").length ===  elDate.element.length ) {
            $('#wholeCity').prop("checked",true);
        }else{
            $('#wholeCity').prop("checked",false);
        }
    });
    $('#areaUl').on('change','li input',function() {
        areaUl ();
        var wholeCheck = $('#areaUl li input');
        if ($("#areaUl li input[type=checkbox]:checked").length ===  wholeCheck.length ) {
            $('#wholeArea').prop("checked",true);
        }else{
            $('#wholeArea').prop("checked",false);
        }

    })
    function linkage (el) {
        // elment:获取input的值,
        var provinceUlHtml='';
        var inputChecked = el.element;
        var ids      = [];
        if(inputChecked.is(':checked')){
            for(var i=0;i<inputChecked.length;i++){
                if(inputChecked[i].checked){
                    if (inputChecked[i].getAttribute('id') !== '') {
                        ids[i] = inputChecked[i].getAttribute('id');
                    }
                }
            }
            (el.judge==2)?el.data.citys=ids : el.data.province = ids;
        }
        var data1 = (el.judge==2)?el.data.citys.length:el.data.province.length;
        if(data1>0){
            $.ajax({
                url:el.Url,
                data:el.data,
                type:'POST',
                dataType:'json',
                success:function(data){
                    if(el.judge==1){
                        $.each(data.city,function (i,pro) {
                            emptyId2[i]=pro.region_id;
                            provinceUlHtml +='<li><input type="checkbox" id="'+pro.region_id+'" name=""><label class="brand-label" for="'+pro.region_id+'"><i class="gou-i"></i>'+pro.region_name+'</label></li>';
                        });
                    }else{
                        $.each(data.district,function (i,pro) {
                              provinceUlHtml +='<li><input type="checkbox" id="'+pro.region_id+'" name=""><label class="brand-label" for="'+pro.region_id+'"><i class="gou-i"></i>'+pro.region_name+'</label></li>';
                              
                        })
                    }
                    el.box.html(provinceUlHtml);
                }
            });
        }else{
            el.box.html('');
        }
    }
    function areaUl () {
        var selected = $('#areaUl li input');
        var ids      = [];
        if(selected.is(':checked')){
            for(var i=0;i<selected.length;i++){
                if(selected[i].checked){
                    if (selected[i].getAttribute('id') !== '') {
                        ids[i] = selected[i].getAttribute('id');
                    }
                }
            }
            districtIds=ids;
        }
        districtIds=$("#areaUl li input[type=checkbox]:checked").length==0?[]:ids;
    }

    // 选择全部
    $('#wholeProvince').on('change',function() {
        
        if ($(this).prop('checked')==true) {
            $('#provinceUl').find('li input').prop("checked",true);
            var elDate = {
            Url:'/cost-level/city',
            element:$('#provinceUl li input'),
            data:{province:[]},
            box:$('#cityUl'),
            judge:1

        }
         linkage (elDate);
        }else{
            $('#provinceUl').find('li input').prop("checked",false);
            $('#wholeCity').prop("checked",false);
            $('#wholeArea').prop("checked",false);
            $('#cityUl').html('');
            $('#areaUl').html('');
        }
    })
    $('#wholeCity').on('change',function() {
        if(this.checked){
            $('#cityUl').find('li input').prop("checked",true);
            var elDate = {
                Url:'/cost-level/district',
                element:$('#cityUl li input'),
                data:{citys:[]},
                box:$('#areaUl'),
                judge:2
            }
             linkage (elDate);
        }else{
            $('#cityUl').find('li input').prop("checked",false);
            $('#areaUl').html('');
        }
        
    })
    $('#wholeArea').on('change',function() {
        districtIds = [];
        if($(this).is(':checked')){
            var checkboxInput =$("#areaUl li input");
            for(var i=0;i<checkboxInput.length;i++){
                districtIds[i] =checkboxInput[i].getAttribute('id');
            }
            $('#areaUl').find('li input').prop("checked",true);
        }else{
            districtIds = [];
            $('#areaUl').find('li input').prop("checked",false);
        }
    })
    // 联动点击获取收费项目
    $('.radioLable label').on('click',function () {
        setTimeout(function () {
             dataPull ();
        },100);
    });
    function dataPull () {
        if ($('#level_title').attr('data') != ''){
            levelId =$('#level_title').attr('data');
        }
        var brandId = $('input[name=brand_id]:checked').attr('value');
        var classId = categoryIdArray;
        var classIds= $('#parentIframeContern').attr('data-id');
        if(classIds != '' && classId.length == 0 || classId == ''){
            classIds = classIds.split(",")
            classId = classIds;
        }
        var typeId  = $('input[name=type_id]:checked').attr('value');

        if(brandId!=undefined&& classId!=undefined&&typeId!=undefined){
            $.ajax({
                type: "POST",
                url:'/cost-level/link-age',
                data:{
                    levelId:levelId,
                    brandId:brandId,
                    classId:classId,
                    typeId:typeId
                },
                dataType:'json',
                success: function (data) {
                    if (data.code == 200) {
                        var tbodyContentHtml = '';
                        $.each(data.data,function (i,pro) {
                            levelId = pro.cost_level_id;
                                tbodyContentHtml +='<tr class="chargeStandard-tr1"  data="'+pro.cost_item_id+','+pro.cost_item_name+','+pro.unit+','+pro.price+'"><td>'+pro.cost_item_name+'</td><td>'+pro.unit+'</td><td>'+pro.price+'</td><td> <a href="javascript:" style="color:#3333ff" onClick="edit_click('+pro.id+')"id="bj'+pro.id+'"></a><a href="javascript:" style="color:#3333ff" onClick="del_click(['+pro.ids+'])">删除</a></td></tr>';
                            $('#tbodyContent').html(tbodyContentHtml);
                            $('#charge-column').show();
                        });
                    }else{
                        $('#tbodyContent').html('');
                        $('#charge-column').hide();
                    }
                }
            });
        }
    }
    // 提交
    var levelId;
    var chargeObj = [];
    function function_name () { 
        // 收费标准名称
        var levelTitle = $('#level_title').val();
        if ($('#level_title').attr('data') != '') {
            levelId =  $('#level_title').attr('data');
        }
        // 城市地址 districtIds
        // 品牌
        var brandId = $('input[name=brand_id]:checked').attr('value');
        // 产品类目
        //var classId = $('input[name=class_id]:checked').attr('value');
        var classId = categoryIdArray;
        var classIds= $('#parentIframeContern').attr('data-id');
        if(classIds != '' && classId.length == 0 || classId == ''){
            classIds = classIds.split(",")
            classId = classIds;
        }
        // 服务类型
        var typeId = $('input[name=type_id]:checked').attr('value');
        //console.log(classId);return false;
        //判断不为空
        if (levelTitle == '') {
            alert('请输入收费标准名称');
            return false;
        }
        if (districtIds == '') {
            alert('请选择服务城市');
            return false;
        }
        
        if (chargeObj == '') {
            alert('请添加收费项目及对应的收费标准');
            return false;
        }
        //ajax传值保存
        $.ajax({
            type: "POST",
            url:'/cost-level/add',
            data:{
                levelId:levelId,
                levelTitle:levelTitle,
                districtIds:districtIds,
                brandId:brandId,
                classId:classId,
                typeId:typeId,
                chargeObj:chargeObj,

            },
            dataType:'json',
            success: function (data) {
                if (data.code == 200) {
                    var tbodyContentHtml = '';
                    $.each(data.data,function (i,pro) {
                        levelId = pro.cost_level_id;
                            tbodyContentHtml +='<tr class="chargeStandard-tr1"  data="'+pro.cost_item_id+','+pro.cost_item_name+','+pro.unit+','+pro.price+'"><td>'+pro.cost_item_name+'</td><td>'+pro.unit+'</td><td>'+pro.price+'</td><td> <a href="javascript:" style="color:#3333ff" onClick="edit_click('+pro.id+')"id="bj'+pro.id+'"></a><a href="javascript:" style="color:#3333ff" onClick="del_click(['+pro.ids+'])">删除</a></td></tr>';
                    });
                    $('#tbodyContent').html(tbodyContentHtml);
                    $('#charge-column').show();
                }else{
                    parent.alert(data.message);
                }
            }
        });
    }
    // 添加收费项目
     var layerIndex = null;
    $('#addCharge').on('click',function() {

        //判断不为空
        var levelTitle = $('#level_title').val();
        if (levelTitle == '') {
            alert('请输入收费标准名称');
            return false;
        }
        if (districtIds == '') {
            alert('请选择服务城市');
            return false;
        }
        
        $('#payService').attr('item_id','');
        layerIndex=layer.open({
            title:false,
            closeBtn: 0,
            type: 1,
            skin: 'yourclass', //加上边框
            area: ['520px', '340px'], //宽高
            content: $('.add-payService')
        });
    });
    $('.weekData').on('click',function() {
        $('.weekData').nextAll('.weekUl').hide();
        $(this).nextAll('.weekUl').is(":hidden")? $(this).nextAll('.weekUl').show():$(this).nextAll('.weekUl').hide();
    });
    // 点击空白处关闭
    $(document).click(function(e){
        var _con = $('.weekData'); 
        (!_con.is (e.target) && _con.has(e.target).length === 0)?$('.weekUl').hide():"";
    });

    $('.weekUl li').on('click',function() {
        $(this).parent('.weekUl').hide();
        $(this).parent('.weekUl').prevAll('.weekData').val($(this).text());
        $(this).parent('.weekUl').prevAll('.weekData').attr('data_id',$(this).attr('value'));
    })
    // 弹窗点击
    $('#cancelPopup').on('click',function() {
       layer.close(layerIndex);
        // alert(1);
    })
    var tbodyContent = $('#tbodyContent');
    var itemId = '';
    $('#okPopup').on('click',function() {
        var payService = $('#payService').val();
        var measurementUnit = $('#measurementUnit').val();
        var unitPrice = $('#unitPrice').val();
        var payServiceId = $('#payService').attr('data_id');
        var measurementUnitId = $('#measurementUnit').attr('data_id');


        if (payService == '') {
            alert('请选择收费项目');
            return false;
        }
        if (measurementUnit == '') {
            alert('请选择计量单位');
            return false;
        }
        if (unitPrice == '') {
            alert('请输入单价');
            return false;
        }
        if (!/^(?!(0[0-9]{0,}$))[0-9]{1,}[.]{0,}[0-9]{0,}$/.test(unitPrice)) {
            alert('单价输入格式不正确（必须为纯数字或两位小数）：请输入正确格式的单价');
            return false;
        }
        
        
        if(payService.length>0&&measurementUnit.length>0&&unitPrice.length>0){
            if ($('#payService').attr('item_id') != '') {
                chargeObj = [payServiceId,payService,measurementUnit,unitPrice,$('#payService').attr('item_id')];
            }else{
                chargeObj = [payServiceId,payService,measurementUnit,unitPrice];
            }
            function_name ();
            layer.close(layerIndex);
        }else{
            alert('请填写内容');
        }
        

    });
    //ajax删除
    function del_click(id) {
        var tbodyContentHtml = '';
        $.ajax({
            type:'POST',
            data:{id:id},
            dataType:'json',
            url:'/cost-level/del',
            success:function(data){
                if (data.code == 200) {
                    dataPull();
                }else{
                    $('#tbodyContent').html('');
                    $('#charge-column').hide();
                }
            }
        });
    }
    //ajax编辑
    function edit_click(id) {
        var editContent = (($('#bj'+id).parent('td').parent('tr')).attr('data')).split(',');
        var payServiceId = editContent[0];
        var payService = editContent[1];
        var company =  editContent[2];
        var unitPrice1 = editContent[3];
        $('#payService').attr('data_id',payServiceId);
        $('#payService').val(payService);
        $('#measurementUnit').val(company);
        $('#unitPrice').val(unitPrice1.trim());
        setTimeout(function() {
            $('#payService').attr('item_id',id);
            layerIndex=layer.open({
                title:false,
                closeBtn: 0,
                type: 1,
                skin: 'yourclass', //加上边框
                area: ['520px', '340px'], //宽高
                content: $('.add-payService')
            });
        },100);
    }
    //收费标准提交
    $('#chargeStandard').on('click',function() {
        var brandId = $('input[name=brand_id]:checked').attr('value');
        // 产品类目
        var classId = categoryIdArray;
        // 服务类型
        var typeId = $('input[name=type_id]:checked').attr('value');

        // 收费标准名称
        var levelTitle = $('#level_title').val();
        if ($('#level_title').attr('data') != '') {
            levelId =  $('#level_title').attr('data');
        }
        if (levelTitle == '') {
            alert('请输入收费标准名称');
            return false;
        }
        if (districtIds == '') {
            alert('请选择服务城市');
            return false;
        }
        if (classId == '') {
            alert('请选择产品类目');
            return false;
        }
        
        //ajax传值保存
        $.ajax({
            type: "POST",
            url:'/cost-level/submit',
            data:{
                levelId:levelId,
                levelTitle:levelTitle,
                districtIds:districtIds,

                brandId:brandId,
                classId:classId,
                typeId:typeId,
                chargeObj:chargeObj,
            },
            dataType:'json',
            success: function (data) {
                if (data.code == 200) {
                    alert(data.message, function(index){
                        location.href='/cost-level/index';
                    });
                }else{
                    alert(data.message);
                }
            }
        });
    });
    function clearNoNum(obj){
        if(obj.value !=''&& obj.value.substr(0,1) == '.'){
            obj.value="";

        }

        obj.value = obj.value.replace(/^0*(0\.|[1-9])/, '$1');
        obj.value = obj.value.replace(/[^\d.]/g,"");
        obj.value = obj.value.replace(/\.{2,}/g,".");
        obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
        obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数
        if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
            if(obj.value.substr(0,1) == '0' && obj.value.length == 2){
                obj.value= obj.value.substr(1,obj.value.length);
            }
        }
    } 
    
    function addCostItem() {
        parent.location.href='/cost-item/index';
    }

    function addServiceArea() {
        window.location.href='/service-area/index';
    }

    //点击添加选择分类
    var index = null;
    $("#parentIframe").click(function () {
        var data = $("#skillId").val();
        var t_id = $("#parentIframe").val(); //技师id
        index = layer.open({
            type: 2,
            title: '添加产品类目',
            area: ['580px', '350px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/cost-level/get-class'

        });
    });
</script>
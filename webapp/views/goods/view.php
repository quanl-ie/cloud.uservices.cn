<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = '商品详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <link rel="stylesheet" href="/css/vueComponent.css"> -->
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/base.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/vue.component.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/goods.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/css/vue-photo-preview.css'); ?>?v=<?php echo time(); ?>">
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">商品详情
    </span>
</div>
<section id="main-content">
    <div id="app" class="panel-body view" v-cloak>
        <section class="section">
            <h5 class="h5">基本信息</h5>
            <hr class="hr">
            <div class="section-content">
                <div class="row flex flex-left">
                    <div class="col flex-item-orig flex" style="width: 100%">
                        <label class="flex-item-orig">商品名称：</label>
                        <div class="text-content">{{goods.goodsName}}</div>
                    </div>
                </div>

                <div class="row flex flex-left">
                    <div class="col flex-item-orig flex">
                        <label class="flex-item-orig">商品类型：</label>
                        <div class="text-content">{{goods.goodsType}}</div>
                    </div>
                    <div class="col flex-item-orig flex">
                        <label class="flex-item-orig">商品品牌：</label>
                        <div class="text-content">{{goods.goodsBrand}}</div>
                    </div>
                    <div class="col flex-item-orig flex">
                        <label class="flex-item-orig">商品类目：</label>
                        <div class="text-content">{{goods.goodsCategory}}</div>
                    </div>
                </div>

                <div class="row flex flex-left">
                    <div class="col flex-item-orig flex">
                        <label class="flex-item-orig">商品型号：</label>
                        <div class="text-content">{{goods.goodsModel==''?'无':goods.goodsModel}}</div>
                    </div>
                    <div class="col flex-item-orig flex">
                        <label class="flex-item-orig">计量单位：</label>
                        <div class="text-content">{{goods.goodsUnit}}</div>
                    </div>
                    <div class="col flex-item-orig flex">
                        <label class="flex-item-orig">质保期：</label>
                        <div class="text-content">{{goods.goodsWarranty==''?'无':goods.goodsWarranty}}</div>
                    </div>
                </div>

                <div class="row flex flex-left">
                    <div class="col flex-item-orig flex">
                        <label class="flex-item-orig">商品属性：</label>
                        <div class="text-content">
                            <span v-if="goods.goodsAttributes.length == 0">无</span>
                            <table class="table" v-if="goods.goodsAttributes.length>0">
                                <thead>
                                <tr>
                                    <th>属性名称</th>
                                    <th>属性值</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="attr in goods.goodsAttributes">
                                    <td>{{ attr.attr_key}}</td>
                                    <td>{{ attr.attr_value}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row flex flex-left">
                    <div class="col flex-item-orig flex" style="width: 100%">
                        <label class="flex-item-orig">商品图片：</label>
                        <div class="text-content flex-item">
                            <select-image :images="goods.files" readonly></select-image>
                            <span v-if="goods.files.length == 0">无</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <h5 class="h5">规格信息</h5>
            <hr class="hr">
            <div class="section-content">
                <div class="row flex flex-left">
                    <div class="col flex-item-orig flex">
                        <label class="flex-item-orig">规格值：</label>
                        <div class="text-content">{{showSpec}}</div>
                    </div>
                </div>
                <div class="row flex flex-left">
                    <div class="col flex-item-orig flex">
                        <label class="flex-item-orig">价格信息：</label>
                        <div class="text-content">
                            <span v-if="goods.specifications.length == 0">无</span>
                            <table v-if="goods.specifications.length > 0" class="table" border="1" cellpadding="0"
                                   cellspacing="0">
                                <thead>
                                <tr>
                                    <th width="80px">规格</th>
                                    <th>商品编号</th>
                                    <th><span class="important">*</span>市场价（元）</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="spec in goods.specifications">
                                    <td class="ellipsis" :title="spec.name">{{spec.name}}</td>
                                    <td>{{spec.goods_no}}</td>
                                    <td>{{spec.market_price}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <h5 class="h5">商品详情</h5>
            <hr class="hr">
            <div class="section-content">
                <div class="row flex flex-left">
                    <div class="col flex-item-orig flex">
                        <label class="flex-item-orig">图片详情：</label>
                        <div class="text-content">
                            <span v-if="goods.goodsDetailsImages.length == 0">无</span>
                            <select-image :images="goods.goodsDetailsImages" readonly vertical></select-image>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
<script type="text/javascript" src="/js/vue/lib/vue.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.mixin.emitter.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.component.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue-photo-preview.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/goods/view.js?v=<?php echo $v; ?>"></script>
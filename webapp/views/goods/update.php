<?php

use yii\helpers\Url;
use webapp\models\BrandQualification;

$this->title = '编辑商品';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <link rel="stylesheet" href="/css/vueComponent.css"> -->
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/base.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/vue.component.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/goods.css'); ?>?v=<?php echo time(); ?>">
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">编辑商品
    </span>
</div>
<section id="main-content">
    <div id="app" class="panel-body">
        <u-form v-cloak>
            <section class="section">
                <h5 class="h5">基本信息</h5>
                <hr class="hr">
                <div class="section-content">
                    <u-form-item required label="商品名称">
                        <u-input type="text"
                                 width="360px"
                                 v-model="goods.goodsName"
                                 rule="UnEmpty::商品名称不能为空！"
                                 placeholder="请输入商品名称"></u-input>
                    </u-form-item>
                    <u-form-item required label="商品类型">
                        <u-select v-model="goods.goodsType" width="360px" rule="UnEmpty::请选择商品类型！">
                            <option value="">请选择商品类型</option>
                            <option v-for="(value,key) in source.goodsTypes" :value="key">{{value}}</option>
                        </u-select>
                    </u-form-item>
                    <u-form-item required label="商品品牌">
                        <u-select v-model="goods.goodsBrand" width="360px" rule="UnEmpty::请选择商品品牌！">
                            <option value="">请选择品牌</option>
                            <option v-for="(value,key) in source.goodsBrands" :value="key">{{value}}</option>
                        </u-select>
                    </u-form-item>
                    <u-form-item required label="商品类目">
                        <u-selection name="goodsCategory" width="360px" v-model="goods.goodsCategory" @load-child="loadChild" :label-value="goods.goodsCategoryDesc">
                            <u-selection-option  v-for="o in source.options"
                                                 :key="o.id"
                                                 :option="o"
                                                 label="class_name"
                                                 value="id"
                                                 child="exist"
                            ></u-selection-option>
                        </u-selection>
                    </u-form-item>
                    <u-form-item label="商品型号">
                        <u-input type="text" width="360px" placeholder="请输入商品型号" v-model="goods.goodsModel"></u-input>
                    </u-form-item>
                    <u-form-item required label="计量单位">
                        <u-select v-model="goods.goodsUnit" width="360px" rule="UnEmpty::请选择计量单位！">
                            <option value="">请选择计量单位</option>
                            <option v-for="(value, key) in source.goodsUnits" :value="key">{{value}}</option>
                        </u-select>
                    </u-form-item>
                    <u-form-item label="质保期">
                        <div class="flex" style="width: 360px">
                            <u-input type="text" margin="0 20px 0 0" width="170px" v-model="goods.goodsWarrantyNum"
                                     placeholder="请输入数字"></u-input>
                            <u-select width="170px" v-model="goods.goodsWarrantyUnit">
                                <option value="">请选择单位</option>
                                <option v-for="(value,key) in source.goodsWarrantyUnits" :value="key">{{value}}</option>
                            </u-select>
                        </div>
                    </u-form-item>
                    <u-form-item label="商品属性">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>属性名称</th>
                                <th>属性值</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="(attr,index) in goods.goodsAttributes">
                                <td>
                                    <u-input type="text"
                                             v-model="attr.attr_key"
                                             height="30px"
                                             :word-count="5"
                                             placeholder="请输入"></u-input>
                                </td>
                                <td>
                                    <u-input type="text"
                                             v-model="attr.attr_value"
                                             height="30px"
                                             :word-count="20"
                                             placeholder="例瓦数属性为10w"></u-input>
                                    <div v-if="index !== 0" class="text-tips" style="position: absolute;margin:-30px 180px"><a
                                                @click.stop.prevent="deleteAttributes(attr,index)">删除</a>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="text-tips">
                            <a @click.stop.prevent="addGoodsAttr">＋添加自定义属性</a>（如不满足您的需求，可添加自定义属性）
                        </div>
                    </u-form-item>
                    <u-form-item label="商品图片">
                        <select-image :images="goods.files"></select-image>
                        <div class="text-tips">首个图片为主图；建议尺寸：750×750</div>
                    </u-form-item>

                </div>
            </section>
            <section class="section">
                <h5 class="h5">规格信息</h5>
                <hr class="hr">
                <div class="section-content">
                    <u-form-item label="规格值">

                        <!--添加/编辑-->
                        <ul class="ul goods-spec">
                            <li class="flex" v-for="(spec, index) in goods.specifications">
                                <u-input type="text"
                                         width="360px"
                                         v-model="spec.name"
                                         placeholder="请输入规格值，如蓝色"></u-input>
                                <div v-if="index !== 0" class="text-tips" style="padding-left: 10px"><a
                                            @click.stop.prevent="deleteSpec(spec, index)">删除</a>
                                </div>
                            </li>
                        </ul>
                        <div class="text-tips"><a @click.prevent.stop="addSpec">+添加规格值</a></div>
                    </u-form-item>
                    <u-form-item label="价格信息">
                        <table class="table" border="1" cellpadding="0" cellspacing="0">
                            <thead>
                            <tr>
                                <th width="80px" v-if="goods.specifications.length >1 || goods.specifications[0].name!=''">规格</th>
                                <th>商品编号</th>
                                <th><span class="important">*</span>市场价（元）</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="(spec, index) in goods.specifications">
                                <td class="ellipsis" :title="spec.name" v-if="goods.specifications.length > 1 || spec.name!=''">{{spec.name}}</td>
                                <td>

                                    <u-input type="text" v-model="spec.goods_no" height="30px"
                                             placeholder=""></u-input>
                                </td>
                                <td>

                                    <u-input type="text" v-model="spec.market_price" height="30px" placeholder=""></u-input>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </u-form-item>
                </div>
            </section>
            <section class="section">
                <h5 class="h5">商品详情</h5>
                <hr class="hr">
                <div class="section-content">
                    <u-form-item label="详情图片">
                        <select-image :images="goods.goodsDetailsImages" vertical></select-image>
                        <div class="text-tips">建议尺寸：宽度750，高度2000以内；上传的图片及描述，将会显示在图文详情中</div>
                    </u-form-item>
                </div>
            </section>
            <section class="section">
                <hr class="hr">
                <div class="section-content">
                    <u-form-item>
                        <u-button width="60px" margin="0 10px 0 0" @click="goback">取消</u-button>
                        <u-button width="72px" margin="0 10px 0 0" v-if="!offlineSubmiting" type="primary" check @click="offlineSubmit">仅保存</u-button>
                        <u-button width="72px" margin="0 10px 0 0" v-if="offlineSubmiting" style="background: #cccccc;border: 1px solid #CCCCCC;" type="primary">仅保存</u-button>
                        <u-button width="96px" margin="0 10px 0 0" v-if="!onlineSubmiting" type="primary" check @click="onlineSubmit">保存并上架</u-button>
                        <u-button width="96px" margin="0 10px 0 0" v-if="onlineSubmiting" style="background: #cccccc;border: 1px solid #CCCCCC;" type="primary">保存并上架</u-button>
                    </u-form-item>
                </div>
            </section>
        </u-form>
    </div>
</section>
<script type="text/javascript" src="/js/vue/lib/vue.js"></script>
<script type="text/javascript" src="/js/vue/lib/axios.min.js"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js"></script>
<script type="text/javascript" src="/js/vue/lib/vue.mixin.emitter.js"></script>
<script type="text/javascript" src="/js/vue/lib/vue.component.js"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js"></script>
<script type="text/javascript" src="/js/vue/controller/goods/update.js"></script>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = '商品列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <link rel="stylesheet" href="/css/vueComponent.css"> -->
 <link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/base.css');?>?v=<?php echo time();?>">
 <link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/vue.component.css');?>?v=<?php echo time();?>">
 <link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/goodslist.css');?>?v=<?php echo time();?>">
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">商品列表</span>
    <?php if(in_array('/goods/add',$selfRoles)): ?>
    <a href="/goods/add" style="color: #fff;"><span class="btn btn-success jn-btn">添加</span></a>
    <?php endif;?>
</div>
<section id="main-content">
    <div class="panel-body" v-cloak>
    	<div class="sousuo-box">	
			<input type="text" v-model="keyword" @keyup.13="search" placeholder="搜索" class="sousuo">
			<i class="icon2-sousuo" @click="search" :style='iColor'>	</i>
    	</div>
    	<!-- tab切换 -->
    	<div class="ct-tab-box">
    		<ul>
                <?php if(in_array('/goods/on-line-list',$selfRoles)): ?>
    			<li  :class="status==1?'active':''" @click='tabGetdata' value='1'>已上架</li>
                <?php endif;?>
                <?php if(in_array('/goods/off-line-list',$selfRoles)): ?>
    			<li   :class="status==2?'active':''"  @click='tabGetdata'  value='2'>已下架</li>
                <?php endif;?>
    		</ul>
    	</div>
    	<!-- 列表内容 -->
    	<table>
    		<thead>
    			<tr>
    				<th>商品名称</th>
    				<th>商品类型</th>
    				<th>商品品牌</th>
    				<th>商品类目</th>
    				<th class="right-border">商品型号</th>
    				<th width="160px" class="center">操作</th>
    			</tr>
    		</thead>
    		<tbody>
    			<tr v-if="dataList.length>0" v-for='(item,index) in dataList' :key='index'>
    				<td>{{item.name}}</td>
    				<td>{{item.type_desc}}</td>
    				<td>{{item.brand_name}}</td>
    				<td>{{item.class_name}}</td>
    				<td class="right-border">{{item.model}}</td>
    				<td class="center">
                        <?php if(in_array('/goods/view',$selfRoles)): ?>
    					<a :href="'/goods/view?goods_id='+item.id" :data-id='item.id'>查看</a>
                        <?php endif;?>
                        <?php if(in_array('/goods/off-line',$selfRoles)): ?>
    					<a href="javascript:" @click="offlineEvent(item.id)"  v-if='status==1'>下架</a>
                        <?php endif;?>
                        <?php if(in_array('/goods/update',$selfRoles)): ?>
    					<a :href="'/goods/update?goods_id='+item.id+'&status='+status" :data-id='item.id' v-if='status==2'>编辑</a>
                        <?php endif;?>
                        <?php if(in_array('/goods/on-line',$selfRoles)): ?>
    					<a href="javascript:" @click="onlineEvent(item.id)" v-if='status==2'>上架</a>
                        <?php endif;?>
    				</td>
    			</tr>
    		</tbody>
    	</table>
    	<div class="mg-top30" v-if='all > 1'>
    		<vue-pagination :cur.sync="cur"  :all.sync="all" v-on:btn-click="listen"></vue-pagination>
    	</div>
    	
	</div>
</section>
<script type="text/javascript" src="/js/vue/lib/vue.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.mixin.emitter.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.component.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/controller/goods/index.js?v=<?php echo $v;?>"></script>
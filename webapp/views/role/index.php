<?php
$this->title = '角色权限管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">角色权限管理</span>
    <?php if(in_array('/role/add',$selfRoles)){?>
        <a href="/role/add" class="btn btn-success jn-btn" style=" color:#FFF;float:right "> 添加</a>
    <?php };?>
</div>

<section id="main-content">
<div class="row">
    <div class="col-md-12">                    
        <div class="panel panel-default">                     
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead bgcolor="#455971">
                            <tr>
                                <th>角色名称</th>
                                <th>创建人</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(isset($data) && $data):?>
                        <?php foreach ($data as $key=>$val):?>
                            <tr>
                                <td><?php echo $val['name'];?></td>
                                <td><?php echo isset($val['create_user_name'])?$val['create_user_name']:'';?></td>
                                <td class="operation">
                                    <?php if(in_array('/role/view',$selfRoles)){?>
                                        <a href="/role/view?id=<?php echo $val['id'];?>">查看</a>
                                    <?php };?>
                                    <?php if( $val['create_user_id'] == $login_user_id){?>
                                        <?php if(in_array('/role/edit',$selfRoles)){?>
                                            <a href="/role/edit?id=<?php echo $val['id'];?>">修改</a>
                                        <?php };?>
                                        <?php if(in_array('/role/delete',$selfRoles)){?>
                                            <a href="/role/delete?id=<?php echo $val['id'];?>" onclick="return myConfirm('您确定要删除吗？',function(obj){ document.location.href = obj.href ;} , this);">删除 </a>
                                        <?php };?>
                                    <?php }?>
                                </td>
                            </tr>
                            <?php endforeach;?>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div> 
       </div>
   </div>
 </div>
</section>
<script>
window.onload = function(){
	 <?php if(Yii::$app->session->hasFlash('message')):?>
	 myAlert('<?php echo Yii::$app->session->getFlash('message'); ?>');
	 <?php endif;?>
}
</script>
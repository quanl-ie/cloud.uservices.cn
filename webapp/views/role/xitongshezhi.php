<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '系统设置';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<style type="text/css">
    body{background: #f1f2f3;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">系统设置</span>
</div> 

<section id="main-content">
    <div class="nav-box-list">
        <h4>用户和权限</h4>
        <div class="nav-content">
            <a href="javascript:" class="nav-content-a">
                <img  src="<?php echo Url::to('/images/sz1.png');?>" alt="优服务">
                <span>用户管理</span>
            </a>
            <a href="javascript:" class="nav-content-a">
                <img  src="<?php echo Url::to('/images/sz2.png');?>" alt="优服务">
                <span>角色管理</span>
            </a>
        </div>
    </div>
    
    <div class="nav-box-list">
        <h4>基础设置<span style="font-size: 14px;color: #666">(<i class="red-i">*</i>为确保系统正常使用，请完成所有基础项设置)</span></h4>
        <div class="nav-content">
            <a href="javascript:" class="nav-content-a">
                <img  src="<?php echo Url::to('/images/sz3.png');?>" alt="优服务">
                <span>收费项目</span>
            </a>
            <a href="javascript:" class="nav-content-a">
                <img src="<?php echo Url::to('/images/sz4.png');?>" alt="优服务">
                <span>收费标准</span>
            </a>
            <a href="javascript:" class="nav-content-a">
                <img src="<?php echo Url::to('/images/sz5.png');?>" alt="优服务">
                <span>服务类型</span>
            </a>
            <a href="javascript:" class="nav-content-a">
                <img src="<?php echo Url::to('/images/sz6.png');?>" alt="优服务">
                <span>服务区域</span>
            </a>
            <a href="javascript:" class="nav-content-a">
                <img src="<?php echo Url::to('/images/sz7.png');?>" alt="优服务">
                <span>服务流程</span>
            </a>
            <a href="javascript:" class="nav-content-a">
                <img src="<?php echo Url::to('/images/sz8.png');?>" alt="优服务">
                <span>产品品牌</span>
            </a>
            <a href="javascript:" class="nav-content-a">
                <img src="<?php echo Url::to('/images/sz9.png');?>" alt="优服务">
                <span>产品类目</span>
            </a>
        </div>
    </div>
    <div class="nav-box-list">
        <h4>仓储设置</h4>
        <div class="nav-content">
            <a href="javascript:" class="nav-content-a">
                <img src="<?php echo Url::to('/images/sz10.png');?>" alt="优服务">
                <span>开启设置</span>
            </a>
        </div>
    </div>
    <div class="nav-box-list">
        <h4>企业设置</h4>
        <div class="nav-content">
            <a href="javascript:" class="nav-content-a">
                <img src="<?php echo Url::to('/images/sz11.png');?>" alt="优服务">
                <span>企业信息</span>
            </a>
           
        </div>
    </div>
</section>
<?php
use yii\bootstrap\ActiveForm;
$this->title = '角色信息查看';
$this->params['breadcrumbs'][] = ['label' => '角色管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
table,td,th{border:1px solid #dddddd;text-align: left;}
td span{width: 180px;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">角色查看</span>
    <a href="/role/index" class="btn btn-success jn-btn" style=" color:#FFF;float:right "> 返回</a>
</div>
<section id="main-content">
	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                  <?php
                    $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'form-horizontal',
                            'id' => 'form2',
                            'name' => 'form1',
                            'enctype' => 'multipart/form-data',
                            
                        ],
                        'fieldConfig' => [
                            'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-sm-6 addinput-list\">{input}{error}</div>",
                            'labelOptions' => ['class' => 'name-title-lable'],
                        ]
                    ]);
                    ?>
                    
                    <?php /*echo $form->field($model, 'description')->input('text',['placeholder'=>$model->getAttributeLabel('description'),'readonly'=>'readonly']);*/?>
                    <div class="information-list">
                      <label class="name-title-lable" for="role-name">角色名称：</label><span class="c_red"></span>
                      <div class="col-sm-6 addinput-list">
                        <input type="text" id="role-name" class="form-control" name="Role[name]" value="<?php echo $model->name;?>" readonly="readonly" disabled="disabled" placeholder="角色名称" aria-required="true">
                        <p class="help-block help-block-error"></p></div>
                    </div>
                    <div class="information-list">
                    <label class="name-title-lable" for="role-data_auth_id">数据权限：</label><span class="c_red"></span>
                    <div class="col-sm-6 addinput-list"><input type="text" id="role-data_auth_id" class="form-control" name="Role[data_auth_id]" value="<?php echo $model->data_auth_id;?>" readonly="readonly" disabled="disabled" placeholder="数据权限" aria-required="true"><p class="help-block help-block-error"></p></div>
                    </div>
                    <div class="information-list">
                    	   <label for="clubuser-qq" class="name-title-lable">权限配置：</label>
                    	   <div class="addinput-list col-sm-10">
                            	<table width="100%" border="0" cellpadding="0" cellspacing="1"  bgcolor="#dddddd" >
                                    <?php foreach ($menu as $val):?>
                                    <tr>
                                        <td bgcolor="#f9f9f9" colspan="2" height="40" valign="middle">
                                      	   <span style="margin-left:10px;"><input class="firstMenu" disabled="disabled" type="checkbox" style="position:relative;top:1px;margin-left:10px;" <?php if(isset($val['checked'])):?>checked="checked"<?php endif;?> /> <label for="<?php echo $val['id'];?>" style="font-weight: normal"><?php echo $val['name'];?><i class="gou-i"></i></label></span>
                                        </td>
                                    </tr>
                                    <?php if(isset($val['sub'])):?>
                                    <?php foreach ($val['sub'] as $v):?>
                                    <tr>
                                      <td width="30%" height="50" bgcolor="#FFFFFF">
                                         <span style="display:block;float:left;margin-right: 15px;margin-left:10px;">
                                         	<input disabled="disabled"  type="checkbox" <?php if(isset($v['checked'])):?>checked="checked"<?php endif;?> style="position:relative;top:1px;margin-left:10px;" /> 
                                         	<label for="<?php echo $v['id'];?>" style="font-weight: normal"><?php echo $v['name'];?><i class="gou-i"></i></label>
                                         </span>
                                      </td>
                                      <td bgcolor="#FFFFFF" style="padding:10px;">
                                      	<?php if(isset($v['sub']) && $v['sub']):?>
                                      		<?php foreach ($v['sub'] as $v2): ?>
                                      		<span style="display:block;float:left;margin-right: 15px;">
                                             	<input disabled="disabled"  type="checkbox" <?php if(isset($v2['checked'])):?>checked="checked"<?php endif;?> style="position:relative;top:1px;margin-left:10px;" /> 
                                             	<label for="<?php echo $v2['id'];?>" style="font-weight: normal"><?php echo $v2['name'];?><i class="gou-i"></i></label>
                                             </span>
                                      		<?php endforeach;?>
                                      	<?php endif;?>
                                      </td>
                                    </tr>
                                    <?php endforeach;?>
                                    <?php endif;?>
                                    <?php endforeach;?>
                                </table>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>               
                 </div>
            </div>                         
        </div>
    </div>                        
</section> 
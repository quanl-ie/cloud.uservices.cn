<?php
use yii\bootstrap\ActiveForm;

$this->title = '修改角色信息';
$this->params['breadcrumbs'][] = ['label' => '角色权限管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
table,td,th{border:1px solid #dddddd;}
td{text-align: left;}
td span{width: 130px;}
td.right-td  span{width: 240px;position: relative;}
.row{background: #f1f2f7;}
td.right-td  span label{width: 100%;height: 26px;display: block;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;}
.hover-right-hint{width: 330px;right: -330px;}
.hover-right-hint p{font-size: 12px;text-align: left;padding:30px 20px;}
.lingdang{left: 140px;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">编辑角色</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">                    
            <div class="panel panel-default">                     
                <div class="panel-body">
                  <?php
                    $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'form-horizontal',
                            'id' => 'form2',
                            'name' => 'form1',
                            'enctype' => 'multipart/form-data',
                            
                        ],
                        'fieldConfig' => [
                            'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-sm-6\">{input}{error}</div>",
                            'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        ]
                    ]);
                    ?>
                    <div class="information-list field-role-name required">
                        <label class="name-title-lable" for="role-name"><span class="c_red">*</span>角色名称：</label><span class="c_red"></span>
                        <div class="addinput-list col-sm-6">
                            <input type="text" id="role-name" class="form-control" name="Role[name]" value="<?php echo $model->name;?>" maxlength="10" placeholder="10字以内" aria-required="true">
                            <p class="help-block help-block-error"></p>
                        </div>
                    </div>
                    <div  class="information-list">
                       <label for="clubuser-qq" class="name-title-lable"><span class="c_red">*</span>数据权限：</label>
                        <div class="ol-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <select id="role-data_auth_id" class="form-control" name="Role[data_auth_id]" aria-required="true">
                                <?php foreach ($dataAuthList as $key=>$val){?>
                                    <option value="<?php echo $key;?>" <?php if($model->data_auth_id == $key){?> selected <?php }?>><?php echo $val;?></option>
                                <?php }?>
                            </select>
                            <div class="hover-right-hint">
                                <div class="hover-right-hintbox">
                                    <i class="lingdang"></i>
                                    <p>1、角色权限：当选中某一角色权限时，该角色即有某项功能<br>
                                          2、数据权限：当选择某一选项时，此角色可以操作此数据范围的功能。<br>
                                          （1）本公司：用户所在公司的数据<br>
                                          （2）本公司及以下：用户所在公司及其以下公司的数据<br>
                                          （3）本部门：用户所在部门的数据<br>
                                          （4）本部门及以下：用户所在部门及其以下部门的数据<br>
                                          （5）本人：用户本人的数据</p>
                                </div>
                            </div>
                            <p class="help-block help-block-error"></p>
                        </div>
                    </div>
                    <div class="information-list" style="height: auto;">
                    	     <label for="clubuser-qq" class="name-title-lable"><span class="c_red">*</span>角色权限：</label>
                    	   <div id="roleListBox" class="addinput-list role-table-box" style="margin-bottom: 20px;height: auto;">
                               <?php echo $this->render( '_form', ['menu' => $menu,]); ?>
                            </div>
                        </div>
                    <p class="xian"></p>
                    <div class="information-list  text-center">
                             <a href="/role/index" style="color: #333"><button type="button" class="btn bg-f7">返回</button></a>
                             <button type="submit" class="btn btn-success mgr20 " style="color:#fff;">提交</button>
                        
                    </div>                                  
                    <?php ActiveForm::end(); ?>               
                 </div>
            </div>                         
        </div>
    </div>                        
</section> 
<script>
window.onload = function(){
	 <?php if(Yii::$app->session->hasFlash('message')):?>
	 myAlert('<?php echo Yii::$app->session->getFlash('message'); ?>');
	 <?php endif;?>

	 <?php  $error = $model->getErrors('menu_ids');?>
	 <?php if(!empty($error)):?>
 myAlert('<?php echo $error[0];?>');
 <?php endif;?>

    $('body').delegate('.firstMenu','click',function(){
	   $('.parent_'+$(this).attr('id')).prop('checked', $(this).is(':checked'));
	});

    $('body').delegate('.secondMenu','click',function(){
		var pid = $(this).attr('p-id');
		//把二级下面的子菜单选上
		$('.subParent_'+$(this).attr('id')).prop('checked', $(this).is(':checked'));
		
		 var checked = $('.parent_'+pid+':checked').length;
		 $("#"+ $(this).attr('p-id') ).prop('checked', checked>0 );
	});
    $('body').delegate('input[name="menu[]"]','change',function() {
      var pid = $(this).attr('p-id');
      var fid = $(this).attr('f-id');
      if($(this).is(':checked')){
        if($(this).attr('f-id')!="undefined"){
          $('#'+pid).prop('checked',true);
          $('#'+fid).prop('checked',true)
        }
      }
      if(fid =='157'){
        $('#'+fid).parents('tr').next('tr').find('input').prop('checked',true);
      }
      if(pid=='157'&& fid== undefined && $(this).attr('id')!="478"){
        $('#'+pid).parents('tr').next('tr').find('input').prop('checked',true);
      }
      
  })
	//表单 提交
	$('form').submit(function(){
		if($('input[name="menu[]"]:checked').length == 0){
			myAlert('请选 择权限配置!');
			return false;
		}
	});

    $('#role-data_auth_id').change(function () {
        var datarole = $(this).val();
        $.getJSON('/role/edit?id=<?php echo $model->id;?>',{'data_role':datarole},function (json) {
            $('#roleListBox').html(json.html);
        })
    });
} 
</script>
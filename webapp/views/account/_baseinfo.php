<?php
use yii\helpers\Url;
?>
<div class="col-md-4 col-lg-3" style="border-right: 1px solid #cdcdcc">
    <div>
        <div>
            <ul class="order-details">
                <li>
                    <div class="headPortrait-letter-left">
                        <img id="headSculpture" src="<?=empty($model->avatar_url) ? Url::to('/images/morentouxiang.png') : $model->avatar_url; ?>">
                    </div>
                </li>
                <li>客户姓名 ：<strong><?= isset($model->account_name) ? $model->account_name :''?></strong>
                    <?php if (isset($model->sex) == 1) :?>
                        先生
                    <?php else:?>
                        女士
                    <?php endif;?>
                </li>
                <li>客户电话 ：<?= isset($model->mobile) ? $model->mobile : ''?></li>
                <li>类型 ：<?php echo $clientType;?></li>
                <li>所属机构 ：<?php echo $departmentName;?></li>
                <li>
                    所在区域：<?= isset($accountAddress->province_name) ? $accountAddress->province_name : '' ?><?= isset($accountAddress->city_name) ? $accountAddress->city_name : ''?><?= isset($accountAddress->district_name) ? $accountAddress->district_name : ''?><?= isset($accountAddress->address) ? $accountAddress->address : ''?>
                        <a href="javascript:" style="color:#0c78ea;" id="showAddress" data-id="<?php echo isset($accountAddress->id) ? $accountAddress->id : ''  ?>">
                            <i class="icon2-didian"></i>
                        </a>
                </li>
                <li>创建人：<?= isset($username) ? $username : ''?></li>
                <li>创建日期：<?= isset($model->create_time) ? date('Y-m-d',$model->create_time) : ''?></li>
            </ul>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#showAddress').click(function () {
            var id = $(this).data('id');
            layer.open({
                type: 2,
                title: '详细地址',
                area: ['700px', '450px'],
                fixed: false, //不固定
                maxmin: false,
                scrollbar: false,
                content: '/account-address/show-coordinate?id='+id,
            });
        });
    });
</script>
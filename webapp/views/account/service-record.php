<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    $this->title = '服务记录列表';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">

<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4{
        padding-left: 0;
    }
    input[type=radio]{
        margin-top: 10px;
    }
    body{
        overflow-x: hidden;
        font-family: "微软雅黑";background: #fff;
    }
    table{
        border: 1px solid #ccc;
        width:380px;
        margin:30px auto;
    }
    th,td{
        border-right: 1px solid #ccc;
        text-align: center;height: 45px;
        font-family: "微软雅黑";
        font-size: 14px;

    }
    td{
        line-height: 2;
    }
    th{

        background: #f6fafe;
    }
    tr{
        border-bottom: 1px solid #ccc;
    }
    .range-td>div{
        border-top: 1px solid #ccc;
    }
    .range-td>div:nth-child(1){
        border-top: 0px solid #ccc;
    }
</style>
<body style="padding: 0px 50px;">
<div class="row popup-boss">
    <table style="width:100%">
        <tr>
            <th width="140px">订单编号</th>
            <th width="90px">服务类型</th>
            <th width="90px">服务流程</th>
            <th width="150px">完成服务时间</th>
            <th>服务备注</th>
        </tr>
        <?php if(!empty($data['data']['list']))
        {
            foreach($data['data']['list'] as $key=>$val)
            {
                $flag = true;
                foreach($val['work_stage_arr'] as $k=>$v) {
                    ?>
                    <tr>
                        <?php if($flag == true){ ?>
                        <td rowspan="<?php echo count($val['work_stage_arr'] );?>">
                            <?php if (in_array('/order/view', $selfRoles)) { ?>
                                <a target="_blank" href="/order/view?order_no=<?php echo $val['order_no']; ?>"><?php echo $val['order_no']; ?></a>
                            <?php } else {
                                ; ?>
                                <?php echo $val['order_no']; ?>
                            <?php } ?>
                        </td>
                        <?php }?>
                        <?php if($flag == true){ ?>
                        <td rowspan="<?php echo count($val['work_stage_arr'] );?>"><?php echo $val['work_type_desc']; ?></td>
                        <?php }?>
                        <td><?php echo $v['work_stage_desc']; ?></td>
                        <td><?php echo date('Y-m-d H:i',$v['finsh_service_time']) ;?></td>
                        <td><?php echo $v['service_record'] ?></td>
                    </tr>
                    <?php $flag = false;
                }
            }
        }else { ?>
            <tr>
                <td colspan="2">暂无数据</td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>

<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };

</script>
</body>
</html>




















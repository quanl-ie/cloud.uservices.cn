<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '客户详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
		border-top:2px solid #2693ff;
	}
	table{
		margin-top: 20px;
		border-right:1px solid #ccc;border-bottom:1px solid #ccc;
	}
	th,td{
		height: 40px;
		text-align: center;
		border-left:1px solid #ccc;border-top:1px solid #ccc
	}
    .layui-layer-setwin .layui-layer-close2,.layui-layer-setwin .layui-layer-close2:hover{right: 15px;width: 20px;height: 20px;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">客户详情</span>
    <a href="/account/index" class="return-a" style="color: #fff">返回</a>
</div> 
<section id="main-content">
<div class="row">
    <div class="col-md-12">                    
        <div class="panel panel-default">                     
            <div class="panel-body" style="font-size: 14px;">
                    <?php echo $this->render( '_baseinfo', [
                        'model'          => $model,
                        'clientType'     => $clientType,
                        'accountAddress' => $accountAddress,
                        'username'       => $username,
                        'departmentName' => $departmentName
                    ]); ?>
				   <div class="col-md-8  col-lg-9">
					   	<div style="width: 100%;" class="tab-group">
					   		<ul id="myTab" class="nav nav-tabs">
                                <?php if(in_array('/account/view',$selfRoles)){?>
                                    <li class="active"><a href="/account/view?account_id=<?php echo $model->id ?>" >客户订单</a></li>
                                <?php };?>
                                <?php if(in_array('/account/get-contract-info',$selfRoles)){?>
                                    <li><a href="/account/get-contract-info?account_id=<?php echo isset($model->id)?$model->id:'' ?>" >合同信息</a></li>
                                <?php };?>
                                <?php if(in_array('/account/get-sale-order',$selfRoles)){?>
                                    <li><a href="/account/get-sale-order?account_id=<?php echo $model->id?>">客户产品</a></li>
                                <?php };?>
                                <?php if(in_array('/account/intention-product',$selfRoles)){?>
                                    <li><a href="/account/intention-product?account_id=<?php echo isset($model->id)?$model->id:'' ?>" >意向产品</a></li>
                                <?php };?>
                                <?php if(in_array('/account/get-address',$selfRoles)){?>
                                    <li><a href="/account/get-address?id=<?php echo $model->id?>">联系地址</a></li>
                                <?php };?>
							</ul>
							<div class="right-btn-group" style="right: 0">
                                <?php if(in_array('/order/add',$selfRoles)){?>
                                    <a href="/order/add?account_id=<?php echo $model->id ?>" >新建订单</a>
                                <?php };?>
                                <?php if(in_array('/sale-order/add',$selfRoles)){?>
                                    <a href="javascript:" onclick="addSoleOrder(<?php echo $model->id ?>)">添加产品</a>
                                <?php };?>
                                <?php if(in_array('/account-address/add',$selfRoles)){?>
                                    <a href="javascript:" onclick="addAddress(<?php echo $model->id ?>)" >添加联系地址</a>
                                <?php };?>
							</div>
						</div>
						
						<div id="myTabContent" class="tab-content">
							<div class="tab-pane fade in active" id="customer-order">
								<table  style="width: 100%;text-align:center;">
									<thead>
										<tr>
											<th>订单编号</th>
                                            <th style="min-width: 80px">所属机构</th>
											<th  style="min-width: 80px">服务类型</th>
											<th style="min-width: 80px">服务产品</th>
											<th style="min-width: 120px">服务时间</th>
											<th width="80px">状态</th>
										</tr>
									</thead>
									<tbody>
                                        <?php if ($data) :?>
                                        <?php foreach ($data as $key=>$val):  ?>
										<tr>
                                            <td><a href="/order/view?order_no=<?php echo isset($val['order_no']) ? $val['order_no'] : ''?>"><?= isset($val['order_no']) ? $val['order_no'] : ''?></a></td>
                                            <td><?= isset($val['department_name']) ? $val['department_name'] : ''?></td>
                                            <td><?= isset($val['work_type_desc']) ? $val['work_type_desc'] : '' ?></td>
                                            <td><?= isset($val['prod_desc']) ? $val['prod_desc'] : '' ?></td>
                                            <td><?= isset($val['plan_time']) ? $val['plan_time'] : '' ?></td>
											<td>
                                                <?php if (isset($val['status']) && $val['status'] == 5 ) :?>
                                                    <span class="state-order state-order-green"><?= $val['status_desc'] ?></span>
                                                <?php elseif (isset($val['status']) && $val['status'] == 6 ) :?>
                                                    <span class="state-order state-order-silver"><?= $val['status_desc'] ?></span> 
                                                <?php else: ?>
                                                    <span class="state-order state-order-orange"><?= $val['status_desc'] ?></span>
                                                <?php endif;?>
											</td>
										</tr>
                                        <?php endforeach;?>
                                    <?php else:?>
                                        <tr>
                                            <tr><td colspan="8" class="no-record">暂无数据</td></tr>
                                        </tr>
                                    <?php endif;?>
									</tbody>
								</table>
							</div>
							<div id="customer-product" class="tab-pane fade" ></div>
							<div id="customer-address" class="tab-pane fade" ></div>
						</div>
                    <div class="col-xs-12 text-center pagination">
                        <?php echo $pageHtml;?>
                    </div>
				   </div>
            </div>
       </div>
   </div>
   
 </div>
</section>
<!-- 取消弹层 -->
<div id="cancelLayer" style="display: none">
    <div class="col-md-12" style="text-align: center;padding:10px">
        <p class="col-md-12">请选择取消订单原因</p>
        <select  class="col-md-12 form-control cancelReason">
            <option value="">请选择</option>
            <option value="客户服务内容变更">客户服务内容变更</option>
            <option value="订单内容填写有误">订单内容填写有误</option>
            <option value="其他原因">其他原因</option>
        </select>
        <div class="col-sm-12 text-cebter" style="margin-top: 30px">
            <button class="btn btn-success mgr20" id="layerSubmit"><i class="fa fa-search"></i>确定</button>
            <button class="btn" id="layerCancel"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>
<script>
    ////提供给弹框调用
    function showAlert(message) {
        alert(message,function () {
            document.location.reload(true);
        });
    }
    
    //添加联系地址 
    function addAddress(id){
        layer.open({
            type: 2,
            title: '添加客户地址',
            area: ['800px', '500px'],
            fixed: false, //不固定
            maxmin: true,
            scrollbar: false,
            content: '/account-address/add?id='+id,
            end:function () {
                location.reload();
            }
        });
    }
    //添加产品
    var addJudge = 2,reload=1;
    function addSoleOrder(id){
       layer.open({
            type: 2,
            title: false,
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: '/sale-order/add?accountId='+id,
           end:function () {
               location.reload();
           }
        });
    }
window.onload = function(){
        
    var layerIndex = null;
    var orderNo = '';
    $(".cancelOrder").click(function () {
        layerIndex = layer.open({
            type: 1,
            title: '提示',
            area: ['300px', '220px'],
            fixed: true,
            maxmin: false,
            scrollbar: false,
            content: $('#cancelLayer').html()
        });
        orderNo = $(this).attr('href');
        return false;
    });
    //关闭弹层
    $("body").delegate("#layerCancel","click",function(){
        if(layerIndex){
            layer.close(layerIndex);
        }
    });
    //提交取消订单
    $("body").delegate("#layerSubmit","click",function(){
        var cancelReason = $(this).parents('div').find('.cancelReason').find('option:selected').val();
        if($.trim(orderNo) == ''){
            alert('订单号不能为空');
            return false;
        }
        if($.trim(cancelReason) == ''){
            alert('请选择取消原因');
            return false;
        }
        $.getJSON('/order/cancel',{'order_no':orderNo,'cancel_reason':cancelReason},function (json) {
            if(json.success == 1){
                if(layerIndex){
                    layer.close(layerIndex);
                }
                alert('取消成功');
                window.location.reload(true);
            }
            else {
                alert(json.message);
            }
        });
    });

}
</script>
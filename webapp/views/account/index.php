<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
$this->title = '客户管理';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<style type="text/css">
    .drop-dw-layerbox{width: calc(100% - 86px)}
    .drop-dw-layerbox>button{width: 100%;position: absolute;left: 0;bottom: 0;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">客户管理</span>
    <div class="right-btn-box">
        <?php if(in_array('/account/add',$selfRoles)):?>
            <span class="btn btn-success" id="addParentIframe"  data-toggle="modal" data-target="#scrollingModal"> 添加</span>
        <?php endif;?>
    </div>

</div> 
<section id="main-content">
<div class="row">
    <div class="col-md-12">                    
        <div class="panel panel-default">                     
            <div class="panel-body">
                <form action="/account/index" class="form-horizontal form-border" id="form">
                    <div class="form-group jn-form-box">
                        <div class="col-sm-12 no-padding-left">
                            <div class="single-search">
                                <label class=" search-box-lable">客户姓名</label>
                                <div class="single-search-kuang1">
                                    <input type="text" class="form-control" name="account_name" value="<?=isset($params['account_name']) ? $params['account_name'] : '' ?>">
                                </div>
                            </div>
                            <div class="single-search">
                                <label class=" search-box-lable">客户电话</label>
                                <div class="single-search-kuang1">
                                    <input type="text" class="form-control" name="mobile" value="<?= isset($params['mobile']) ? $params['mobile'] : '' ?>">
                                </div>
                            </div>
                              <?php if(!empty($department)):?>
                            <div class="single-search region-limit" style="overflow:visible;">
                                <label class=" search-box-lable">所属机构</label>
                                <input type="hidden" id="department_id" value="<?php echo $department_id;?>">
                                <div class="single-search-kuang1" id="de">
                                    <input type="text" class="form-control" value="<?= isset($params['department_name']) ? $params['department_name'] : '' ?>" id="department"  readonly <?php if(!empty($department)){ echo 'onclick="clickCategory()" placeholder="请选择机构"';}?> >
                                    <input type="hidden" class="form-control" value="<?= isset($params['department_ids']) ? $params['department_ids'] : '' ?>" id="department_ids" name="department_ids">
                                    <div class="drop-dw-layerbox" style="display: none;">
                                        <div class="ul-box-h180">
                                             <ul id="depart1" class="depart">
                                                 <?php if (!empty($department)) : ?>
                                                     <?php foreach ($department as $key => $val) : ?>
                                                         <li>
                                                             <span>
                                                                <?php if ($val['exist'] == 1) : ?>
                                                                    <i class="icon2-shixinyou" onclick="getDepartment(this)"></i>
                                                                <?php endif;?>
                                                                   <input type="checkbox" id="a<?=$val['id'] ?>" value="<?=$val['id'] ?>" name="department_id[]" <?php if(isset($params['department_id']) && (in_array($val['id'],explode(",",$params['department_id'])))){ echo "checked=true"; }?>>
                                                                 <label for="a<?=$val['id'] ?>"><?=$val['name'] ?><i class="gou-i"></i></label>
                                                            </span>
                                                         </li>
                                                     <?php endforeach; ?>
                                                 <?php endif; ?>
                                             </ul>
                                        </div>
                                         <button class="btn btn-orange" id="belongedBtn">确定</button>
                                     </div>

                                </div>

                            </div>
                              <?php endif?>
                            <div class="single-search">
                                <label class=" search-box-lable">客户地址</label>
                                <div class="single-search-kuang1">
                                    <input type="text" class="form-control" name="account_address" value="<?= isset($params['account_address']) ? $params['account_address'] : '' ?>">
                                </div>
                            </div>
                            <div class="tow-search">
                                <label class="search-box-lable">创建时间</label>
                                <div class="single-search-kuang2">
                                    <div class="half-search">
                                        <input type="text" id="datetimepicker-create-top" class="form-control" name="create_start_time" value="<?php echo isset($_GET['create_start_time'])?$_GET['create_start_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-create-end\')}'})">
                                        <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-create-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-create-end\')}'})"><i class="icon2-rili"></i></span>
                                        <span class="zhi"> 至</span>
                                    </div>
                                    <div class="half-search">
                                        <input type="text" id="datetimepicker-create-end" class="form-control" name="create_end_time" value="<?php echo isset($_GET['create_end_time'])?$_GET['create_end_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-create-top\')}'})" >
                                        <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-create-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-create-top\')}'})"><i class="icon2-rili"></i></span>
                                    </div>
                                    <p class="city3_fix"></p>
                                </div>
                            </div>
                            <div class="search-confirm"> 
                                <button class="btn btn-success  "><i class="fa fa-search"></i> 搜索</button>
                            </div>
                        </div>
                    </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead bgcolor="#455971">
                            <tr>
                                <th>客户姓名</th>
                                <th>电话</th>
                                <th>订单数</th>
                                <th>产品数</th>
                                <th>所属机构</th>
                                <th>创建时间</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($data):?>
                            <?php foreach ($data as $key=>$val):  ?>
                                <tr>
                                    <td>
                                        <?php if(empty($val['account_name'])):?>
                                        空
                                        <?php else:?>
                                        <?php if(in_array('/account/view',$selfRoles)):?>
                                                <a href="/account/view?account_id=<?php echo $val['id'] ?>"><?=$val['account_name']; ?></a>
                                            <?php else:?>
                                                <?=$val['account_name']; ?>
                                            <?php endif;?>
                                        <?php endif;?>
                                    </td>
                                    <td><?= isset($val['mobile']) ? $val['mobile'] : ''; ?></td>
                                    <td>
                                        <?php if(empty($val['order_number'])):?>
                                            暂无订单
                                        <?php else:?>
                                            <a href="/account/view?account_id=<?php echo $val['id'];?>"><?=$val['order_number']; ?></a>
                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <?php if(empty($val['product_number'])):?>
                                            暂无产品
                                        <?php else:?>
                                            <a href="/account/get-sale-order?account_id=<?php echo $val['id'];?>"><?=$val['product_number']; ?></a>
                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <?php if(empty($val['name'])):?>
                                            无
                                        <?php else:?>
                                            <?=$val['name']; ?>
                                        <?php endif;?>
                                    </td>
                                    <td><?= isset($val['create_time']) ? date('Y-m-d H:i',$val['create_time']) : '' ;?></td>
                                    <td class="operation">
                                        <?php if(in_array('/account/edit',$selfRoles)):?>
                                        <a href="javascript:;" onclick="editChick(<?php echo $val['id']?>)">编辑</a>
                                        <?php endif;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            <?php else:?>
                                <tr>
                                    <tr><td colspan="8" class="no-record">暂无数据</td></tr>
                                </tr>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
                </form>
                <div class="col-xs-12 text-center  pagination">
                    <?php echo $pageHtml;?>
                </div>
            </div> 
       </div>
   </div>
 </div>
</section>
<script>
window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    <?php endif;?>
};
</script>
<script type="text/javascript">
    //添加
    $("#addParentIframe").click(function () {
        layer.open({
            type: 2,
            title: '添加客户',
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: true,
            scrollbar: false,
            content: '/account/add/',
            end:function () {
               location.reload();
            }
        });
    });
    //编辑
    function editChick(id){
        layer.open({
            type: 2,
            title: '编辑客户',
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: true,
            scrollbar: false,
            content: '/account/edit?id='+id,
            end:function () {
               location.reload();
            }
        });
    }
    // 选中内容
    $('#belongedBtn').bind('click', function() {
        //event.preventDefault();
        var id_array= [];
        var data_array=[];
        $('input[name="department_id[]"]:checked').each(function(){
            data_array.push($(this).next('label').text());
            id_array.push($(this).val());//向数组中添加元素
        });
        var idstr=id_array.join(',');//将数组元素连接起来以构建一个字符串
        var datatr = data_array.join('；')
        $('#department').val(datatr);
        $('#department_ids').val(idstr);
        $('.drop-dw-layerbox').hide();
        return false;
    });
    $(document).click(function(event){
        var _con = $('.drop-dw-layerbox');  // 设置目标区域
        if(!_con.is(event.target) && _con.has(event.target).length === 0){ // Mark 1
           //$('#divTop').slideUp('slow');  //滑动消失
           //$('.drop-dw-layerbox').hide(1000);     //淡出消失
        }
   });
    var departmentIdArray = $('#department_ids').val().split(',');
    function getDepartment(el) {
        var id = $(el).next('input').val();
        var _this = $(el).parent('span');
        var auth = <?php echo $auth;?>;
          var company_id = <?php echo $company_id;?>;
          var self_id = $("#department_id").val();
        $.getJSON('/common/ajax-get-department',{'pid':id,"auth":auth,'self_department_id':self_id,"company_id":company_id},function (data) {
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>"
                $(data.data).each(function(i,el) {
                    ulData +="<li><span>";
                    el.exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="getDepartment(this)"></i>':'';
                    ulData+='<input type="checkbox" id="a'+el.id+'" value="'+el.id+'" name="department_id[]" >  <label for="a'+el.id+'">'+el.name+'<i class="gou-i"></i></label></span></li>';
                });
                ulData +="</ul>";
                _this.parent('li').append(ulData);
                $.each(departmentIdArray,function(i,el) {
                    $('#a'+el).prop('checked',true);
                })
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');

            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })
    }
    function  clickCategory(el) {
        //event.stopPropagation();
        $.each(departmentIdArray,function(i,el) {
           $('#a'+el).prop('checked',true);
        })
        $('.drop-dw-layerbox').is(':hidden')? $('.drop-dw-layerbox').show(): $('.drop-dw-layerbox').hide();
    }
     $(document).click(function(event) {
       var region = $('.region-limit');
       if (!region.is(event.target)&&region.has(event.target).length ===0) {
                $('.drop-dw-layerbox').hide();
       }
    });
</script>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '客户详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
		border-top:2px solid #2693ff;
	}
	table{
		margin-top: 20px;
		border-right:1px solid #ccc;border-bottom:1px solid #ccc;
	}
	th,td{
		height: 40px;
		text-align: center;
		border-left:1px solid #ccc;border-top:1px solid #ccc
	}
    .layui-layer-setwin .layui-layer-close2,.layui-layer-setwin .layui-layer-close2:hover{right: 15px;width: 20px;height: 20px;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">客户详情</span>
    <a href="/account/index" class="return-a" style="color: #fff">返回</a>
</div> 
<section id="main-content">
<div class="row">
    <div class="col-md-12">                    
        <div class="panel panel-default">                     
            <div class="panel-body" style="font-size: 14px;">
                    <?php echo $this->render( '_baseinfo', [
                        'model'          => $model,
                        'clientType'     => $clientType,
                        'accountAddress' => $accountAddress,
                        'username'       => $username,
                        'departmentName' => $departmentName
                    ]); ?>
				   <div class="col-md-8  col-lg-9">
				   		<ul id="myTab" class="nav nav-tabs">
                            <?php if(in_array('/account/view',$selfRoles)){?>
                                <li><a href="/account/view?account_id=<?php echo $model->id ?>" >客户订单</a></li>
                            <?php };?>
                            <?php if(in_array('/account/get-contract-info',$selfRoles)){?>
                                <li><a href="/account/get-contract-info?account_id=<?php echo isset($model->id)?$model->id:'' ?>" >合同信息</a></li>
                            <?php };?>

                            <?php if(in_array('/account/get-sale-order',$selfRoles)){?>
                                <li><a href="/account/get-sale-order?account_id=<?php echo $model->id?>">客户产品</a></li>
                            <?php };?>
                            <?php if(in_array('/account/intention-product',$selfRoles)){?>
                                <li><a href="/account/intention-product?account_id=<?php echo isset($model->id)?$model->id:'' ?>" >意向产品</a></li>
                            <?php };?>
                            <?php if(in_array('/account/get-address',$selfRoles)){?>
                                <li class="active"><a href="/account/get-address?id=<?php echo $model->id?>">联系地址</a></li>
                            <?php };?>
								
                        </ul>
                        <div class="right-btn-group">
                            <?php if(in_array('/order/add',$selfRoles)){?>
                                <a href="/order/add?account_id=<?php echo $model->id ?>" >新建订单</a>
                            <?php };?>
                            <?php if(in_array('/sale-order/add',$selfRoles)){?>
                                <a href="javascript:" onclick="addSoleOrder(<?php echo $model->id ?>)">添加产品</a>
                            <?php };?>
                            <?php if(in_array('/account-address/add',$selfRoles)){?>
                                <a href="javascript:" onclick="addAddress(<?php echo $model->id ?>)" >添加联系地址</a>
                            <?php };?>
                        </div>
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade in active" id="customer-order">
                                <table  style="width: 100%;text-align:center;">
                                    <thead>
                                        <tr>
                                            <th>联系人</th>
                                            <th>联系电话</th>
                                            <th>所在区域</th>
                                            <th>详细地址</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($data) :?>
                                        <?php foreach ($data as $key=>$val):  ?>
                                        <tr>
                                            <td><?= isset($val['conact_name']) ? $val['conact_name'] : ''?></td>
                                            <td><?= isset($val['mobile_phone']) ? $val['mobile_phone'] : '' ?></td>
                                            <td><?= isset($val['area']) ? $val['area'] : '' ?></td>
                                            <td><?= isset($val['address']) ? $val['address'] : '' ?></td>
                                        </tr>
                                        <?php endforeach;?>
                                    <?php else:?>
                                        <tr>
                                            <tr><td colspan="4" class="no-record">暂无数据</td></tr>
                                        </tr>
                                    <?php endif;?>
                                    </tbody>
                                </table>
                            </div>
                            <div id="customer-product" class="tab-pane fade" ></div>
                            <div id="customer-address" class="tab-pane fade" ></div>
                        </div>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo $pageHtml;?>
                    </div>
				   </div>
            </div>
       </div>
   </div>
   
 </div>
</section>

<script>

    ////提供给弹框调用
    function showAlert(message) {
        alert(message,function () {
            document.location.reload(true);
        });
    }
    
    //添加联系地址
    function addAddress(id){
        layer.open({
            type: 2,
            title: '添加客户地址',
            area: ['800px', '500px'],
            fixed: false, //不固定
            maxmin: true,
            scrollbar: false,
            content: '/account-address/add?id='+id,
            end:function () {
                location.reload();
            }
        });
    }
    //添加产品
    var addJudge = 2,reload=1;
    function addSoleOrder(id){
       layer.open({
            type: 2,
            title: false,
            area: ['700px', '450px'],
            fixed: false, //不固定
            scrollbar: false,
            content: '/sale-order/add?accountId='+id,
           end:function () {
               location.reload();
           }
        });
    }
</script>
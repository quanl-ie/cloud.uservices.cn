<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '客户详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time();?>">
<style type="text/css">
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{border-top:2px solid #2693ff;}
	table{margin-top: 20px;border-right:1px solid #ccc;border-bottom:1px solid #ccc;}
	th,td{height: 40px;text-align: center;border-left:1px solid #ccc;border-top:1px solid #ccc}
    .right-btn-group{right: 15px;}
     .layui-layer-setwin .layui-layer-close2,.layui-layer-setwin .layui-layer-close2:hover{right: 15px;width: 20px;height: 20px;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">客户详情</span>
    <a href="/account/index" class="return-a" style="color: #fff">返回</a>
</div> 
<section id="main-content">
<div class="row">
    <div class="col-md-12">                    
        <div class="panel panel-default">                     
            <div class="panel-body" style="font-size: 14px;">
                    <?php echo $this->render( '_baseinfo', [
                        'model'          => $model,
                        'clientType'     => $clientType,
                        'accountAddress' => $accountAddress,
                        'username'       => $username,
                        'departmentName' => $departmentName
                    ]); ?>
				   <div class="col-md-8  col-lg-9">
                       <ul id="myTab" class="nav nav-tabs">
                           <?php if(in_array('/account/view',$selfRoles)){?>
                               <li><a href="/account/view?account_id=<?php echo $model->id ?>" >客户订单</a></li>
                           <?php };?>
                           <?php if(in_array('/account/get-contract-info',$selfRoles)){?>
                               <li class="active"><a href="/account/get-contract-info?account_id=<?php echo isset($model->id)?$model->id:'' ?>" >合同信息</a></li>
                           <?php };?>
                           <?php if(in_array('/account/get-sale-order',$selfRoles)){?>
                               <li><a href="/account/get-sale-order?account_id=<?php echo $model->id?>">客户产品</a></li>
                           <?php };?>
                           <?php if(in_array('/account/intention-product',$selfRoles)){?>
                               <li><a href="/account/intention-product?account_id=<?php echo isset($model->id)?$model->id:'' ?>" >意向产品</a></li>
                           <?php };?>
                           <?php if(in_array('/account/get-address',$selfRoles)){?>
                               <li><a href="/account/get-address?id=<?php echo $model->id?>">联系地址</a></li>
                           <?php };?>
                       </ul>
                       <div class="right-btn-group">
                           <?php if(in_array('/order/add',$selfRoles)){?>
                               <a href="/order/add?account_id=<?php echo $model->id ?>" >新建订单</a>
                           <?php };?>
                           <?php if(in_array('/sale-order/add',$selfRoles)){?>
                               <a href="javascript:" onclick="addSoleOrder(<?php echo $model->id ?>)">添加产品</a>
                           <?php };?>
                           <?php if(in_array('/account-address/add',$selfRoles)){?>
                               <a href="javascript:" onclick="addAddress(<?php echo $model->id ?>)" >添加联系地址</a>
                           <?php };?>
                       </div>
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade in active" id="customer-order">
                                <table  style="width: 100%;text-align:center;">
                                    <thead>
                                        <tr>
                                            <th>合同编号</th>
                                            <th>合同主题</th>
                                            <th>合同金额</th>
                                            <th>合同状态</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($data) :?>
                                        <?php foreach ($data as $key=>$val):  ?>
                                        <tr>
                                            <td><a href="/contract/detail?id=<?=$val['id']?>"><?= isset($val['no']) ? $val['no'] : ''?></a></td>
                                            <td><?= isset($val['subject']) ? $val['subject'] : '' ?></td>
                                            <td><?= isset($val['total_amount']) ? $val['total_amount'] : '' ?></td>
                                            <td><?= isset($statusList[$val['status']]) ? $statusList[$val['status']] : '' ?></td>
                                        </tr>
                                        <?php endforeach;?>
                                    <?php else:?>
                                            <tr><td colspan="4" class="no-record">该客户还没有合同
                                                    <?php if(in_array('/contract/add',$selfRoles)){?>
                                                        <a href="/contract/add">添加</a>
                                                    <?php };?>
                                                </td>
                                            </tr>
                                    <?php endif;?>
                                    </tbody>
                                </table>
                            </div>
                            <div id="customer-product" class="tab-pane fade" ></div>
                            <div id="customer-address" class="tab-pane fade" ></div>
                        </div>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo $pageHtml;?>
                    </div>
				   </div>
            </div>
       </div>
   </div>
   
 </div>
</section>

<script>

    ////提供给弹框调用
    function showAlert(message) {
        alert(message,function () {
            document.location.reload(true);
        });
    }
    
    //添加联系地址
    function addAddress(id){
        layer.open({
            type: 2,
            title: '添加客户地址',
            area: ['800px', '500px'],
            fixed: false, //不固定
            maxmin: true,
            scrollbar: false,
            content: '/account-address/add?id='+id,
            end:function(){
                setTimeout(function(){location.reload();},2000);
            }
        });
    }
    //添加产品
    var addJudge = 2,reload=1;
    function addSoleOrder(id){
       layer.open({
            type: 2,
            title: false,
            area: ['700px', '550px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: '/sale-order/add?accountId='+id,
        });
    }
</script>
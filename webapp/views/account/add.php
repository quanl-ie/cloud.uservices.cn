<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = '添加客户';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::cssFile('@web/webuploader/webuploader.css') ?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css'); ?>">

<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4 {
        padding-left: 0;
    }

    input[type=radio] {
        margin-top: 10px;
    }

    body {
        overflow-x: hidden;
        font-family: "微软雅黑";
        background: #fff;
    }

    .information-list {
        padding: 0 80px 0 50px;
    }

    .name-title-lable {
        left: 50px;
    }

    .radio-list {
        margin-top: 8px;
    }
</style>
<body>
<div class="row popup-boss">

    <form action="/account/account-upload" id="avatarUrlUploadImage"
          method="post" enctype="multipart/form-data"
          target="avatarUploadImage">
        <div class="col-md-8 information-list" style="height: 90px;">
            <label class="details-column-title">
                <div class="headPortrait-letter-left">
                    <img id="headSculpture"
                         src="<?= empty($account->avatar_url) ? Url::to('/images/morentouxiang.png') : $account->avatar_url; ?>">
                </div>
            </label>
            <div class="details-column-content" style="padding-top: 10px">
                <div class="change-gallery">
                    <span>上传头像</span>
                    <input type="file" name="avatar_url" onchange="avatarUrlChange()" accept="image/png, image/jpeg"
                           class="filePrew" style="height: 34px;top: 10px;width: 120px;overflow: hidden;">
                </div>
                <p style="margin-top: 10px">非必填，可修改</p>
            </div>
        </div>
    </form>
    <div class="col-md-8 information-list" style="height:0px;margin-top: 0;">
        <iframe name="avatarUploadImage" style="display: none"></iframe>
    </div>


    <form action="/account/add" method="post" id="account-add">
        <input type="hidden" value="<?= isset($account->id) ? $account->id : '' ?>" id="id" name="id">
        <input type="hidden"
               value="<?= empty($account->avatar_url) ? Url::to('/images/morentouxiang.png') : $account->avatar_url; ?>"
               id="avatar_url" name="avatar_url">

        <div class="col-md-8 information-list">
            <label class="name-title-lable">
                <i class="red-i">*</i>客户姓名：
            </label>
            <div class=" addinput-list">
                <input type="text" value="<?= isset($account->account_name) ? $account->account_name : '' ?>"
                       name="account_name" class="form-control inputxt" datatype="*" sucmsg=" " nullmsg="请输入客户姓名"
                       placeholder="请输入客户姓名" max="20"/>
            </div>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>
        <div class="col-md-8 information-list">
            <label class="name-title-lable">
                <i class="red-i">*</i>客户电话：
            </label>
            <div class="addinput-list">
                <input type="text" value="<?= isset($account->mobile) ? $account->mobile : '' ?>" name="mobile"
                       datatype="/^(0[0-9]{2,3}\-*)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$/|/^1\d{10}$/" sucmsg=" "
                       nullmsg="请输入客户电话" placeholder="请输入客户电话(如：139xxxxxxxx/010-88888888-002)" errormsg="请输入正确的客户电话"
                       maxlength="50" class="form-control inputxt"/>
            </div>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>
        <div class="col-md-8 information-list" style="margin-top: 8px">
            <label class="name-title-lable">
                <i class="red-i">*</i>性别：
            </label>
            <div class=" addinput-list">
                <?php if (!empty($account->sex) && $account->sex == 1) : ?>
                    <div class="radio-list">
                        <input type="radio" value="1" name="sex" id="male" class="pr1" checked="checked"/><label
                                for="male">先生</label>
                    </div>
                    <div class="radio-list">
                        <input type="radio" value="2" name="sex" id="female" class="pr1"/><label for="female">女士</label>
                    </div>
                <?php elseif (!empty($account->sex) && $account->sex == 2): ?>
                    <div class="radio-list">
                        <input type="radio" value="1" name="sex" id="male" class="pr1"/><label for="male">先生</label>
                    </div>
                    <div class="radio-list">
                        <input type="radio" value="2" name="sex" id="female" class="pr1" checked="checked"/><label
                                for="female">女士</label>
                    </div>
                <?php else: ?>
                    <div class="radio-list">
                        <input type="radio" value="1" name="sex" id="male" class="pr1" checked="checked"/><label
                                for="male">先生</label>
                    </div>
                    <div class="radio-list">
                        <input type="radio" value="2" name="sex" id="female" class="pr1"/><label for="female">女士</label>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-8 information-list">
            <label class="name-title-lable">
                类型：
            </label>
            <div class="addinput-list">
                <select class="col-md-4 col-sm-4 col-xs-4 form-control" id="client_type" name="client_type">
                    <option value="">请选择类型</option>
                    <?php if (!empty($clientType)) : ?>
                        <?php foreach ($clientType as $key => $val): ?>
                            <option value="<?= $key ?>" <?php if (isset($account->client_type) && $account->client_type == $key) : ?>  selected="selected" <?php endif; ?>><?= $val ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
            <div class="Validform_checktip Validform_wrong" id="mobileError" style="margin-left:110px;"></div>
        </div>
        <div class="col-md-8 information-list  region-limit">
            <label class="name-title-lable" for="workorder-account_id">
                <i class="red-i">*</i> 所属机构：
            </label>
            <span class="c_red"></span>
            <div class=" addinput-list">
                <?php if (isset($id)) { ?>
                    <?php if (!empty($origin_dic_id) && isset($this_direct_company_id) && $origin_dic_id == $this_direct_company_id) { ?>
                        <input type="text" class="form-control"
                               value="<?= isset($department_edit['name']) ? $department_edit['name'] : $define_department['name'] ?>"
                               id="category1" placeholder="请选择上级机构" readonly sucmsg="" nullmsg="请选择机构">
                        <input type="hidden" id="select_department_id"
                               value="<?= isset($department_edit['id']) ? $department_edit['id'] : $define_department['id'] ?>"
                               name="department_id">
                        <i class="icon iconfont icon-shouqi1"></i>
                        <p class="misplacement" style="bottom: -5px;" id="work_type_msg">请选择上级机构</p>
                        <div class="drop-dw-layerbox">
                            <div class="ul-box-h180">
                                <ul>
                                    <?php if (!empty($department)) : ?>
                                        <?php foreach ($department as $key => $val) : ?>
                                            <li>
                                               <span>
                                                  <?php if ($val['exist'] == 1) : ?>
                                                      <i class="icon2-shixinyou" onclick="clickCategory(this) "></i>
                                                      <input type="radio" value="<?= $val['id'] ?>" name="department_id"
                                                             id="<?= $val['id'] ?>" class="pr1"
                                                             onclick="changeCategory(this)">
                                                  <?php else: ?>
                                                      <input type="radio" value="<?= $val['id'] ?>" name="department_id"
                                                             id="<?= $val['id'] ?>" class="pr1"
                                                             onclick="changeCategory(this)">
                                                  <?php endif; ?>
                                                   <label for="<?= $val['id'] ?>"><?= $val['name'] ?></label>
                                              </span>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    <?php } else { ?>
                        <p style="line-height: 34px"><?= isset($department_edit['name']) ? $department_edit['name'] : $define_department['name'] ?></p>
                        <input type="hidden" id="select_department_id"
                               value="<?= isset($department_edit['id']) ? $department_edit['id'] : $define_department['id'] ?>"
                               name="department_id">
                        <i class="icon iconfont icon-shouqi1"></i>
                    <?php } ?>
                <?php } else { ?>
                    <input type="text" class="form-control"
                           value="<?= isset($department_edit['name']) ? $department_edit['name'] : $define_department['name'] ?>"
                           id="category1" placeholder="请选择上级机构" readonly sucmsg="" nullmsg="请选择机构">
                    <input type="hidden" id="select_department_id"
                           value="<?= isset($department_edit['id']) ? $department_edit['id'] : $define_department['id'] ?>"
                           name="department_id">
                    <i class="icon iconfont icon-shouqi1"></i>
                    <p class="misplacement" style="bottom: -5px;" id="work_type_msg">请选择上级机构</p>
                    <div class="drop-dw-layerbox">
                        <div class="ul-box-h180">
                            <ul>
                                <?php if (!empty($department)) : ?>
                                    <?php foreach ($department as $key => $val) : ?>
                                        <li>
                                               <span>
                                                  <?php if ($val['exist'] == 1) : ?>
                                                      <i class="icon2-shixinyou" onclick="clickCategory(this) "></i>
                                                      <input type="radio" value="<?= $val['id'] ?>" name="department_id"
                                                             id="<?= $val['id'] ?>" class="pr1"
                                                             onclick="changeCategory(this)">
                                                  <?php else: ?>
                                                      <input type="radio" value="<?= $val['id'] ?>" name="department_id"
                                                             id="<?= $val['id'] ?>" class="pr1"
                                                             onclick="changeCategory(this)">
                                                  <?php endif; ?>
                                                   <label for="<?= $val['id'] ?>"><?= $val['name'] ?></label>
                                              </span>
                                        </li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
        <div class="col-md-8 information-list" style="height:auto">
            <label class="name-title-lable">
                <i class="red-i">*</i>所在城市：
            </label>
            <div class="addinput-list">
                <!-- 高德地图地址组合jq组件 开始-->
                <div class="us-amap-wrapper" id="address-map">
                    <div class="us-amap-input-content">
                        <div class="us-amap-input-ssq">
                            <input type="text"
                                   placeholder="请选择省市区"
                                   max="50" class="form-control inputxt address input-pua"
                                   value="<?php echo isset($accountAddress->cityLabel) ? $accountAddress->cityLabel : ''; ?>"/>
                            <input type="hidden" name="province_id"
                                   value="<?php echo isset($accountAddress->province_id) ? $accountAddress->province_id : ''; ?>">
                            <input type="hidden" name="city_id"
                                   value="<?php echo isset($accountAddress->city_id) ? $accountAddress->city_id : ''; ?>">
                            <input type="hidden" name="district_id"
                                   value="<?php echo isset($accountAddress->district_id) ? $accountAddress->district_id : ''; ?>">
                            <input type="hidden" name="lng"
                                   value="<?php echo isset($accountAddress->lon) ? $accountAddress->lon : ''; ?>">
                            <input type="hidden" name="lat"
                                   value="<?php echo isset($accountAddress->lat) ? $accountAddress->lat : ''; ?>">
                            <ul class="ul association-list">
                                <li class="tip">
                                    <span>支持中文/拼音/简拼输入</span>
                                    <i class="association-list-close icon2-shanchu"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="us-amap-input-desc">
                            <a class="us-amap-geolocation" href="javascript:void(0)">定位</a>
                            <input type="text"
                                   placeholder="请输入详细地址" style="padding-right: 40px"
                                   name="address"
                                   max="50" class="form-control inputxt address input-desc"
                                   value="<?php echo isset($accountAddress->address) ? $accountAddress->address : ''; ?>"/>
                        </div>
                    </div>
                    <div class="us-amap-map map">
                        <img src="/img/full-screen.png" class="full-screen">
                    </div>
                    <div class="us-amap-shade"><i class="full-close icon2-shanchu"></i></div>
                    <div class="full-map-wrap">
                        <div class="us-amap-map full-map"></div>
                    </div>
                </div>
                <!-- 高德地图地址组合jq组件 结束 -->
            </div>
        </div>
        <hr>
        <div class="col-md-8 information-list text-center">
            <!-- <div class="right-btn"> -->
            <input type="button" id="closeIframe" class="btn bg-f7 mgr20" value="取消">
            <input type="submit" value="提交" class="btn btn-orange">
            <!-- </div> -->
        </div>
    </form>
</div>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script type="text/javascript"
        src="http://webapi.amap.com/maps?v=1.3&key=bbcdabda1427179d77bb95fac7b2b902"></script>
<script src="//webapi.amap.com/ui/1.0/main.js?v=1.0.11"></script>
<script src="/js/plugin/webpack-amap.js"></script>
<script>
    function avatarUrlChange() {
        document.getElementById( 'avatarUrlUploadImage' ).submit();
    }
    
    function showImg( url ) {
        document.getElementById( 'headSculpture' ).src = url;
        document.getElementById( 'avatar_url' ).value = url;
    }
    
    var index = parent.layer.getFrameIndex( window.name );

    //是否点击了定位
    var hasClickLocation = <?php echo $id > 0 ? "true" : "false";?>;
    var map;
    $( function () {
        map = $$map( 'address-map' );
        map.onLocation( function () {
            hasClickLocation = true;
        } );
    } );
    //表单验证
    $( "#account-add" ).Validform( {
        ajaxPost     : true,
        tiptype      : 3,
        beforeCheck  : function () {
            return !map.isInput;
        },
        beforeSubmit : function ( curform ) {
            if ( !map.inputValid() ) {
                alert( '请输入地址！' );
                return false;
            }
            if ( hasClickLocation == false ) {
                alert( '请先点击定位后再重试！' );
                return false;
            }
        },
        callback     : function ( data ) {
            if ( data.status == 'y' ) {
                if ( parent.iframeSrc != 'undefined' && parent.iframeSrc == 'order' ) {
                    parent.fillAccount( data.id, data.name );
                }
                $.Hidemsg();
                parent.alert( data.info );
                parent.layer.close( index );
            } else {
                $.Hidemsg();
                layer.msg( data.info, {
                    offset : ['100px', '300px']
                } )
            }
        }
        
    } );
    
    function clickCategory( el ) {
        var auth = <?php echo $auth;?>;
        var id = $( el ).next( 'input' ).val();
        var _this = $( el ).parent( 'span' );
        $.getJSON( '/common/ajax-get-department', { 'pid' : id, 'auth' : auth, 'type' : 2 }, function ( data ) {
            var ulData = '';
            if ( _this.next( 'ul' ).size() == 0 ) {
                ulData += "<ul>"
                $( data.data ).each( function ( i, el ) {
                    ulData += "<li><span>";
                    el.exist == 1 ? ulData += '<i class="icon2-shixinyou"  onclick="clickCategory(this)"></i>' : '';
                    ulData += '<input type="radio" value="' + el.id + '" name="department_id" id="' + el.id + '" class="pr1" onclick="changeCategory(this)"><label for="' + el.id + '">' + el.name + '</label></span></li>';
                } );
                ulData += "</ul>";
                _this.parent( 'li' ).append( ulData );
                $( el ).removeClass( 'icon2-shixinyou' );
                $( el ).addClass( 'icon2-shixinxia' );
                
            } else {
                if (
                    _this.next( 'ul' ).is( ':hidden' ) ) {
                    $( el ).removeClass( 'icon2-shixinyou' );
                    $( el ).addClass( 'icon2-shixinxia' );
                    _this.next( 'ul' ).show();
                    
                } else {
                    $( el ).addClass( 'icon2-shixinyou' );
                    $( el ).removeClass( 'icon2-shixinxia' );
                    _this.next( 'ul' ).hide();
                }
            }
            return false;
        } )
        
    }
    
    function changeCategory( el ) {
        $( '.drop-dw-layerbox' ).hide();
        $( '#select_department_id' ).val( $( el ).next( 'label' ).attr( "for" ) );
        $( '#category1' ).val( $( el ).next( 'label' ).text() );
    }
    
    // 点击空白处关闭
    $( document ).click( function () {
        var region = $( '.region-limit' );
        if ( !region.is( event.target ) && region.has( event.target ).length === 0 ) {
            $( '.drop-dw-layerbox' ).hide();
        }
    } );
    $( '#category1' ).on( 'click', function ( event ) {
        event.preventDefault();
        /* $("#select_department_id").remove();
         $("#category1").val('');*/
        $( '.drop-dw-layerbox' ).is( ':hidden' ) ? $( '.drop-dw-layerbox' ).show() : $( '.drop-dw-layerbox' ).hide();
        $( '.role' ).val( '' );
        $( '#role_Select' ).val( '' );
        
    } )
    //关闭iframe
    $( '#closeIframe' ).click( function () {
        parent.layer.close( index );
    } );
    //根据开通城市父级id获取市级数据
    $( "#province" ).change( function () {
        $( "#city" ).html( "<option value=''>请选择市</option>" );
        $( "#district" ).html( "<option value=''>请选择区</option>" );
        var pid = $( "#province" ).val();
        if ( pid != '' ) {
            $.get( "/account/get-city", { "province_id" : pid }, function ( data ) {
                $( "#city" ).html( data );
            } );
        }
    } );
    //根据城市获取区县
    $( "#city" ).change( function () {
        var pid = $( "#city" ).val();
        if ( pid != '' ) {
            $.get( "/account/get-district", { "city_id" : pid }, function ( data ) {
                $( "#district" ).html( data );
            } )
        }
    } );

</script>
</body>
</html>
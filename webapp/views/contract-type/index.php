<?php
    use yii\helpers\Url;
    $this->title = '合同分类';
    $this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<style>
    .li-td-left{position: relative;padding-left: 15px;}
    .left-border-d5d5d5{padding-left: 15px;}
    
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">合同分类</span>
        <?php if(in_array('/contract/add',$selfRoles)){?>
            <span class="btn btn-success jn-btn" style=" color:#FFF;float:right;width:auto;" data-toggle="modal" data-target="#scrollingModal" onclick="addClass()">添加</span>
        <?php };?>
</div>
<section id="main-content">
    <div class="panel-body" style="position: relative;min-width: 1000px;">
        <?php if (isset($data) && !empty($data)) : ?>
        <div class="table-box">
            <ul>
                <li>
                    <div class="left-border-d5d5d5"style="padding-left: 0;">
                        <span class="li-tb-title">分类名称</span>
                    </div>
                    <div class="center1-border-d5d5d5">
                        <span class="li-tb-title">排序</span>
                    </div>
                    <div class="center2-border-d5d5d5">
                        <span class="li-tb-title">状态</span>
                    </div>

                    <div class="right-border-d5d5d5">
                        <span class="li-tb-title">操作</span>
                    </div>

                </li>
                <div class="big-box">
                        <?php foreach ($data as $key => $val) : ?>
                            <li>
                                <div class="left-border-d5d5d5">
                                    <span class="li-td-center">
                                        <?=$val['type_name']; ?>
                                    </span>
                                </div>
                                <div class="center1-border-d5d5d5">
                                    <span class="li-td-center"><?=$val['rank']; ?></span>
                                </div>
                                <div class="center2-border-d5d5d5">
                                    <span class="li-td-center">
                                        <?php if($val['status']==1):?>
                                        启用
                                        <?php elseif($val['status']== 2):?>
                                        禁用
                                        <?php endif;?>
                                    </span>
                                </div>

                                <div class="right-border-d5d5d5">
                                    <span class="li-td-center">
                                        <?php if (isset($val['status']) && $val['status'] == 1) : ?>
                                            <?php if(in_array('/contract-type/edit',$selfRoles)){?>
                                                <a href="javascript:;" onclick="editClass(<?=$val['id']; ?>)">修改</a>
                                            <?php };?>
                                            <?php if(in_array('/contract-type/change-status',$selfRoles)){?>
                                                <a href="javascript:;" onclick="disClass(<?=$val['id']; ?>,2)">禁用</a>
                                            <?php };?>
                                        <?php else: ?>
                                            <?php if(in_array('/contract-type/change-status-on',$selfRoles)){?>
                                                <a href="javascript:;" onclick="disClassOn(<?=$val['id']; ?>,1)" >启用</a>
                                            <?php };?>
                                        <?php endif; ?>
                                        
                                    </span>
                                </div>

                            </li>
                        <?php endforeach; ?>
                </div>
                <li>
                    <div class="xian98"></div>
                </li>
            </ul>
        </div>
        <?php else:?>
        <div class="no-data-page">
            <div>
                <div class="kbtp"></div>
                <p class="no-data-zi">您还没有添加过合同分类</p>
                <?php if(in_array('/contract-type/add',$selfRoles)){?>
                    <span class="btn btn-success" onclick="addClass()">添加合同分类</span>
                <?php }?>
            </div>
        </div>
        <?php endif; ?>
    </div>


</section>
<script>

    //添加
    function addClass () {
        layer.open({
            type: 2,
            title: '添加合同分类',
            area: ['580px', '350px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/contract-type/add', 'no'],
            end:function(){
                location.reload();
            }
        });
    }
    //修改
    function editClass (id,pid) {
        layer.open({
            type: 2,
            title: '修改合同分类',
            area: ['580px', '350px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/contract-type/edit?id='+id,'no'],
            end:function(){
               // location.reload();
            }
        });
    }
    
    /**
     * 修改状态  禁用
     * @param id
     * @param status
     */
    function disClass(id,status) {
        if(status == 2){
            var str  = '您确定要禁用该合同分类吗？';
        }else{
            var str  = '您确定要启用该合同分类吗？';
        }
        layer.confirm(str, {
            area:['320px','210px'],
            btn: ['确定','取消'] //按钮
        }, function(){
            ajaxPost(id,status);
        }, function(){
            layer.close();
        });
    }
    /**
     * 修改状态  启用
     * @param id
     * @param status
     */
    function disClassOn(id,status) {
        if(status == 2){
            var str  = '您确定要禁用该合同分类吗？';
        }else{
            var str  = '您确定要启用该合同分类吗？';
        }
        layer.confirm(str, {
            area:['320px','210px'],
            btn: ['确定','取消'] //按钮
        }, function(){
            ajaxPost(id,status);
        }, function(){
            layer.close();
        });
    }

    /**
     * ajax 修改状态 禁用
     * @param id
     * @param status
     */
    function ajaxPost(id,status){
        var url = '/contract-type/change-status';
        $.ajax({
            type: 'POST',
            url: url,
            data: {'id':id,'status':status},
            dataType: 'json',
            success: function(data){
                if (data.code == 200) {
                    layer.alert(data.message, {
                    }, function(){
                        location.reload();
                    });
                }else{
                    alert(data.message);
                }
            }
        });
    }
    /**
     * ajax 修改状态  启用
     * @param id
     * @param status
     */
    function ajaxPost(id,status){
        var url = '/contract-type/change-status-on';
        $.ajax({
            type: 'POST',
            url: url,
            data: {'id':id,'status':status},
            dataType: 'json',
            success: function(data){
                if (data.code == 200) {
                    layer.alert(data.message, {
                    }, function(){
                        location.reload();
                    });
                }else{
                    alert(data.message);
                }
            }
        });
    }

</script>
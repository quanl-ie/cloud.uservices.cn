<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
$this->title = '添加合同分类';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="/plugins/bootstrap/css/bootstrap.min.css?v=<?php echo time()?>"> 
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time()?>">
<script src="<?php echo Url::to('/js/jquery-1.8.3.min.js');?>?v=<?php echo time()?>"></script>
<style>
  body{background: #fff;}
  .addinput-list{padding: 0;}
  li ul{display: block;}
  .drop-dw-layerbox .icon{left: 0;}
  .addinput-list>.drop-dw-layerbox li{padding-left: 0;}
</style>
<div class="col-md-12">
    <?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal',
                'id' => 'class-add',
                'name' => 'form1',
                'enctype' => 'multipart/form-data',
            ]
        ]);
    ?>
    <div class="information-list">
        <label class="name-title-lable" for="workorder-account_id">
           <i class="red-i">*</i> 合同分类名称：
        </label>
        <span class="c_red"></span>
        <div class="col-md-10 addinput-list">
            <input type="text" class="form-control" name="class_name" id="class_name" placeholder="请输入分类名称" maxlength="10">
            <p class="misplacement" style="bottom: -5px;" id="work_type_msg">请输入合同分类名称</p>
        </div>
    </div>
    <div class="information-list">
        <label class="name-title-lable" for="workorder-account_id">
            排序：
        </label>
        <span class="c_red"></span>
        <div class="col-md-10 addinput-list">
            <input type="text" class="form-control" name="sort" id="sort" maxlength="5" placeholder="仅限数字，用于排序，至多5位">
            <p class="misplacement" style="bottom: -5px;" id="sort_msg">仅限数字</p>
        </div>
    </div>
    <p class="height10"></p>
    <hr>
    <div class="information-list" style="padding-top: 10px">
        <div class="btn-center-box">
            <button class="btn bg-f7 mgr10" id="closeIframe">取消</button>
            <button class="btn btn-orange">保存</button>
        </div>
    </div>
    <?php ActiveForm::end() ?>
</div>

<script src="/js/layer/layer.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    var index = parent.layer.getFrameIndex(window.name);
    //表单验证
    $("#class-add").Validform({
        ajaxPost:true,
        tiptype:3,
        beforeSubmit:function(curform){
            $("#sort_msg").hide();
            $("#work_type_msg").hide();
            var reg  = /^[0-9]{1,5}$/;
            if($("#class_name").val() == ''){
                $("#work_type_msg").show();
                return false;
            }
            if ($("#sort").val() != '' && $("#sort").val().search(reg) == -1)
            {
               $("#sort_msg").show();
                return false;
            }

        },
        callback:function(data){
            if (data.code == 200) {
                $.Hidemsg();
                parent.layer.close(index);
                parent.layer.alert(data.message, function(index){
                    parent.location.reload();
                });
            }else{
                $.Hidemsg();
                parent.alert(data.message);
            }
        }
    });

    //关闭iframe
    $('#closeIframe').click(function(){
        parent.layer.close(index);
        return false;
    });
    $(document).click(function(event) {
       var region = $('.region-limit');
       if (!region.is(event.target)&&region.has(event.target).length ===0) {
                $('.drop-dw-layerbox').hide();
       }
    });
</script>
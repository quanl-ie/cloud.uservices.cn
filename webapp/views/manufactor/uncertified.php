<?php
use yii\helpers\Url;
?>
<style type="text/css">
	.uncertified-img{width: 100px;margin:0 auto;}
	.uncertified-img>img{width: 100%;}
	.see-btn{display: inline-block;padding: 10px 20px;background: #2693ff;border-radius: 5px;color: #fff;}
	.un-content{font-size: 14px;margin: 20px auto;width: 350px}
	.un-title{margin-top:20px;font-size: 18px;font-weight: 600 }
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div style="margin-top: 150px;text-align: center;">
                <div class="uncertified-img">
                    <img src="<?php echo Url::to('/img/rzz.png');?>">
                </div>
                <h4 class="un-title">企业信息未认证</h4>
                <p class="un-content">您还未完成企业认证，完成后即可享有所有功能使用权</p>
                <a class="see-btn" href="/manufactor/add" style="color: #fff">立即认证</a>
            </div>
        </div>
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$this->title = '企业信息';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
        border-top:2px solid #2693ff;
    }
    .details-column-title{
        width: 120px;font-weight: 500;font-family: "微软雅黑";
    }
    .riqi1{
        position: relative;
    }
    body{
        background: #f1f2f7;
    }
    .information-nav-a:link{
        display: inline-block;
        color: #323232;
        margin-left: 54px;
        font-size: 14px;
    }
    .information-nav-a:hover{
        text-decoration: none;
        color: #2693ff;
    }
    .filePrew{
        font-size: 20px;
    }
    .details-column-content{
        margin-left: 120px;
    }
    label{font-weight: 100}
    .fbt{font-size: 16px;font-weight: 600;font-family: "微软雅黑";margin:20px 0 0 0;}
    .jn-title>font{font-size: 12px;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">企业信息
        <?php if (isset($model->id) && $model->status == 2 && isset($model->is_main)){ ?>
            <?php if($derect_company_id==$top_id) :?>
                <font color="green">已认证</font>
            <?php endif;?>
        <?php };?>
        <?php if (isset($model->id) && $model->status == 1 && isset($model->is_main)){ ?>
            <font color="#2693ff">待审核</font>
        <?php };?>
        <?php if ((isset($model->id)  && !isset($model->is_main))){ ?>
            <?php if($derect_company_id==$top_id) :?>
                <font color="green">已认证</font>
            <?php endif;?>
        <?php };?>
    </span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <!-- 切换 -->
                    <?php if(isset($model->id) && $model->status == 2):?>
                        <div class="col-md-12">
                            <ul id="myTab" class="nav nav-tabs">
                                <li class="active">
                                    <a href="#essentialInformation" data-toggle="tab" >
                                        基本信息
                                    </a>
                                </li>
                                <?php if($derect_company_id==$top_id) :?>
                                    <li><a href="#credentials" data-toggle="tab">认证信息</a></li>
                                <?php endif;?>
                            </ul>
                        </div>
                    <?php endif;?>

                    <!-- 内容 -->
                    <div id="myTabContent" class="tab-content col-md-12">

                        <?php if ((isset($model->id) && $model->status == 2)   && (($derect_company_id==$top_id) && ($department_id == $derect_company_id))){  ?>
                            <!-- 基本信息 -->
                            <div class="tab-pane fade in active" id="essentialInformation">
                                <?php
                                $form = ActiveForm::begin([
                                    'options' => [
                                        'class' => 'form-horizontal',
                                        'id' => 'manufactor-view',
                                        'name' => 'form1',
                                        'enctype' => 'multipart/form-data',
                                    ]
                                ]);
                                ?>
                                <input type="hidden" value="<?= isset($model->id)?$model->id:'' ?>" name="id">
                                <div class="details-column" style="height: 90px;">
                                    <label class="details-column-title">
                                        <div class="headPortrait-letter-left">
                                            <img id="headSculpture" src="<?php if($model->logo == ''){echo Url::to('/images/morentouxiang.png');}else{ echo $model->logo;}?>">
                                        </div>
                                    </label>
                                    <div class="details-column-content" style="padding-top: 10px">
                                        <div class="change-gallery">
                                            <span>更换logo</span>
                                            <input type="file" name="logo" maxlength="255" accept="image/png, image/jpeg" class="filePrew">
                                        </div>
                                        <p style="margin-top: 10px">logo仅支持jpg、jpeg、png格式，文件最大3MB。</p>
                                    </div>
                                </div>
                                <div class="details-column" style="height: 20px">
                                    <label class="details-column-title">企业名称：</label>
                                    <div class="details-column-content"><?= isset($model->company)?$model->company:'' ?></div>
                                </div>
                                <div class="details-column">
                                    <label class="details-column-title">联系人：</label>
                                    <div class="details-column-content">
                                        <input type="text" value="<?= isset($model->contact)?$model->contact:'' ?>" maxlength="20" name="contact">
                                    </div>
                                </div>
                                <div class="details-column">
                                    <label class="details-column-title">联系电话：</label>
                                    <div class="details-column-content">
                                        <input type="text" name="mobile" maxlength="11" datatype="/^\s*$/|m" errormsg="请输入正确的手机号" sucmsg=" " nullmsg=" " value="<?= isset($model->mobile)?$model->mobile:'' ?>">
                                    </div>
                                </div>
                                <div class="details-column">
                                    <label class="details-column-title">电子邮箱：</label>
                                    <div class="details-column-content">
                                        <input type="text" name="email" maxlength="50" datatype="/^\s*$/|e" errormsg="请输入正确的电子邮箱" sucmsg=" " nullmsg=" "  value="<?= isset($model->email)?$model->email:'' ?>" placeholder="请输入公司联系人邮箱，例：XXX@163.com">
                                    </div>
                                </div>
                                <div class="details-column" style="height: 40px">
                                    <label class="details-column-title"><i class="red-i">*</i>所在区域：</label>
                                    <div class="details-column-content">
                                        <div class="three-linkage">
                                            <div class="linkage-justify-list">
                                                <select class="form-control" id="province" name="province_id" datatype="*" nullmsg="请选择所在区域">
                                                    <?php if (!empty($model->province_id)) : ?>
                                                        <option value="<?=$model->province_id ?>" selected="selected" ><?=$model->province_name ?></option>
                                                        <?php foreach ($province as $key => $val): ?>
                                                            <option value="<?=$val['region_id'] ?>"><?=$val['region_name'] ?></option>
                                                        <?php endforeach;?>
                                                    <?php else :?>
                                                        <option value="">请选择省</option>
                                                        <?php foreach ($province as $key => $val): ?>
                                                            <option value="<?=$val['region_id'] ?>"><?=$val['region_name'] ?></option>
                                                        <?php endforeach;?>
                                                    <?php endif ;?>
                                                </select>
                                            </div>
                                            <div class="linkage-justify-list">
                                                <select class="form-control" id="city" name="city_id" datatype="*" nullmsg="请选择所在区域">
                                                    <?php if (!empty($model->city_id)) :?>
                                                        <option value="<?=$model->city_id ?>" selected="selected" ><?=$model->city_name ?></option>
                                                    <?php else :?>
                                                        <option value="">请选择市</option>
                                                    <?php endif ;?>

                                                </select>
                                            </div>
                                            <div class="linkage-justify-list">
                                                <select class="form-control" id="district" name="district_id" datatype="*" nullmsg="请选择所在区域">
                                                    <?php if (!empty($model->district_id)) :?>
                                                        <option value="<?=$model->district_id ?>" selected="selected" ><?=$model->district_name ?></option>
                                                    <?php else :?>
                                                        <option value="">请选择区</option>
                                                    <?php endif ;?>

                                                </select>
                                            </div>

                                            <div class="justify_fix"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="details-column">
                                    <label class="details-column-title"><i class="red-i">*</i>详细地址：</label>
                                    <div class="details-column-content">
                                        <input type="text" name="address" datatype="*" nullmsg="请输入详细地址" value="<?= isset($model->address)?$model->address:'' ?>" maxlength="200" placeholder="请输入详细地址">
                                    </div>
                                </div>
                                <div class="details-column">
                                    <label class="details-column-title">公司简介：</label>
                                    <div class="details-column-content" style="width: 400px;position: relative;">
                                        <textarea maxlength="200"  id="remarks" name="company_desc"><?= isset($model->company_desc)?$model->company_desc:'' ?></textarea>
                                        <p class="xianzhi"><span id="word">0</span>/200</p>
                                    </div>
                                </div>
                                <div class="details-column">
                                    <div class="details-column-content">
                                        <input type="submit" name="" value="保存" class="details-column-submit">
                                    </div>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                        <?php }else{?>
                            <!-- 基本信息 -->
                            <?php if($top_id == $department_id){?>
                                <div class="tab-pane fade in active" id="essentialInformation">
                                    <!--<div class="details-column" style="height: 90px;">
                                        <label class="details-column-title">
                                            <div class="headPortrait-letter-left">
                                                <img id="headSculpture" src="<?php /*if($model->logo == ''){echo Url::to('/images/morentouxiang.png');}else{ echo $model->logo;}*/?>">
                                            </div>
                                        </label>
                                    </div>
                                    <div class="details-column" style="height: 20px">
                                        <label class="details-column-title">公司全称：</label>
                                        <div class="details-column-content"><?= isset($model->company)?$model->company:'' ?></div>
                                    </div>-->
                                    <!--<div class="details-column">
                                        <label class="details-column-title">联系人：</label>
                                        <div class="details-column-content">
                                            <input readonly disabled="disabled" type="text" value="<?= isset($model->contact)?$model->contact:'' ?>" maxlength="20" name="contact">
                                        </div>
                                    </div>
                                    <div class="details-column">
                                        <label class="details-column-title">联系电话：</label>
                                        <div class="details-column-content">
                                            <input readonly disabled="disabled" type="text" name="mobile" maxlength="11" datatype="/^\s*$/|m" errormsg="请输入正确的手机号" sucmsg=" " nullmsg=" " value="<?= isset($model->mobile)?$model->mobile:'' ?>">
                                        </div>
                                    </div>
                                    <div class="details-column">
                                        <label class="details-column-title">电子邮箱：</label>
                                        <div class="details-column-content">
                                            <input readonly disabled="disabled" type="text" name="email" maxlength="50" datatype="/^\s*$/|e" errormsg="请输入正确的电子邮箱" sucmsg=" " nullmsg=" "  value="<?/*= isset($model->email)?$model->email:'' */?>" placeholder="请输入公司联系人邮箱，例：XXX@163.com">
                                        </div>
                                    </div>-->
                                    <h5 class="fbt">类型</h5>
                                    <div class="details-column">
                                        <span class="details-column-title">公司类型：</span>
                                        <div class="details-column-content">
                                            <?php if (isset($model->user_type)): ?>
                                                <?= $model->user_type == 1 ? '企业' : '个体工商户'  ?>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                    <h5 class="fbt">企业信息</h5>
                                    <div class="details-column">
                                        <span class="details-column-title">证件类型：</span>
                                        <div class="details-column-content">
                                            <?php if (isset($model->document_type)){?>
                                                <?= $model->document_type == 1 ? '普通营业执照（存在独立的组织机构代码证）' : '多证合一营业执照（不存在独立的组织机构代码证）'  ?>
                                            <?php };?>
                                        </div>
                                    </div>
                                    <div class="details-column">
                                        <span class="details-column-title">公司全称：</span>
                                        <div class="details-column-content"><?= isset($model->company ) ?$model->company:'' ?></div>
                                    </div>
                                    <div class="details-column">
                                        <span class="details-column-title">营业执照注册号：</span>
                                        <div class="details-column-content"><?= isset($model->business_licence_num ) ?$model->business_licence_num:'' ?></div>
                                    </div>
                                    <div class="details-column" style="height: 40px">
                                        <label class="details-column-title"><i class="red-i"></i>企业所在地：</label>
                                        <div class="details-column-content">
                                            <div class="three-linkage">

                                                <?=isset($model->province_name)?$model->province_name:''; ?>-<?=isset($model->city_name)?$model->city_name:''; ?>-<?=isset($model->district_name)?$model->district_name:''; ?>
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="details-column">
                                        <label class="details-column-title"><i class="red-i"></i>详细地址：</label>
                                        <div class="details-column-content">
                                            <p class="height34"><?= isset($model->address)?$model->address:'' ?></p>
                                        </div>
                                    </div>
                                    <!--<div class="details-column">
                                        <label class="details-column-title">公司简介：</label>
                                        <div class="details-column-content" style="width: 400px;position: relative;">
                                            <textarea type="text" readonly disabled="disabled" maxlength="200"  id="remarks" name="company_desc"><?/*= isset($model->company_desc)?$model->company_desc:'' */?></textarea>
                                        </div>
                                    </div>-->
                                    <div class="details-column">
                                        <span class="details-column-title">经营范围：</span>
                                        <div class="details-column-content"><?= isset($model->scope_operation ) ?$model->scope_operation:'' ?></div>
                                    </div>
                                    <h5 class="fbt">联系人信息</h5>
                                    <div class="details-column">
                                        <span class="details-column-title">联系人姓名：</span>
                                        <div class="details-column-content"><?= isset($model->contact ) ? $model->contact :'' ?></div>
                                    </div>
                                    <div class="details-column">
                                        <span class="details-column-title">联系人电话：</span>
                                        <div class="details-column-content"><?= isset($model->mobile) ? $model->mobile :'' ?></div>
                                    </div>
                                    <div class="details-column">
                                        <span class="details-column-title">营业执照：</span>
                                        <div class="details-column-content">
                                            <?php if($model->business_licence){?>
                                                <div class="details-column-tupian">
                                                    <img class="enlarge-img" src="<?= isset($model->business_licence ) ?$model->business_licence:'' ?>?x-oss-process=image/resize,w_300/auto-orient,1">
                                                </div>
                                            <?php };?>
                                        </div>
                                    </div>
                                    <div class="details-column">
                                        <span class="details-column-title">资质证明：</span>
                                        <div class="details-column-content">
                                            <?php if($model->code_certificate){?>
                                                <div class="details-column-tupian">
                                                    <img  class="enlarge-img"  src="<?= isset($model->code_certificate ) ?$model->code_certificate:'' ?>?x-oss-process=image/resize,w_300/auto-orient,1">
                                                </div>
                                            <?php };?>
                                        </div>
                                    </div>
                                </div>
                            <?php }else{?>
                                <!-- 经销商可编辑区域-->
                                <div class="tab-pane fade in active" id="essentialInformation">
                                    <?php
                                    $form = ActiveForm::begin([
                                        'options' => [
                                            'class' => 'form-horizontal',
                                            'id' => 'manufactor-view',
                                            'name' => 'form1',
                                            'enctype' => 'multipart/form-data',
                                        ]
                                    ]);
                                    ?>
                                    <div class="details-column" style="height: 90px;">
                                        <label class="details-column-title">
                                            <div class="headPortrait-letter-left">
                                                <img id="headSculpture" src="<?php if($model->logo == ''){echo Url::to('/images/morentouxiang.png');}else{ echo $model->logo;}?>">
                                            </div>
                                        </label>
                                    </div>
                                    <div class="details-column" style="height: 20px">
                                        <input hidden name="department_id" value="<?php echo isset($department_id)?$department_id:'';?>">
                                        <label class="details-column-title">公司全称：</label>
                                        <div class="details-column-content"><?= isset($model->company)?$model->company:'' ?></div>
                                    </div>
                                    <div class="details-column">
                                        <label class="details-column-title">联系人：</label>
                                        <div class="details-column-content">
                                            <input type="text" value="<?= isset($model->contact)?$model->contact:'' ?>" maxlength="20" name="contact">
                                        </div>
                                    </div>
                                    <div class="details-column">
                                        <label class="details-column-title">联系电话：</label>
                                        <div class="details-column-content">
                                            <input type="text" name="phone" maxlength="11" datatype="/^\s*$/|m" errormsg="请输入正确的手机号" sucmsg=" " nullmsg=" " value="<?= isset($model->mobile)?$model->mobile:'' ?>">
                                        </div>
                                    </div>
                                    <div class="details-column">
                                        <label class="details-column-title">电子邮箱：</label>
                                        <div class="details-column-content">
                                            <input type="text" name="email" maxlength="50" datatype="/^\s*$/|e" errormsg="请输入正确的电子邮箱" sucmsg=" " nullmsg=" "  value="<?= isset($model->email)?$model->email:'' ?>" placeholder="请输入公司联系人邮箱，例：XXX@163.com">
                                        </div>
                                    </div>
                                    <div class="details-column" style="height: 40px">
                                        <label class="details-column-title"><i class="red-i"></i>所在区域：</label>
                                        <div class="details-column-content">
                                            <div class="three-linkage">
                                                <div class="linkage-justify-list">
                                                    <select class="form-control" id="province" name="province_id" datatype="*"  sucmsg=" " nullmsg="请选择省份">
                                                        <?php if (!empty($province)) {?>
                                                            <?php foreach ($province as $key=>$val){?>
                                                                <option value="<?php echo $val['region_id'];?>" <?php if($model->province_id == $val['region_id']){?> selected <?php }?>><?php echo $val['region_name'];?></option>
                                                            <?php }?>
                                                        <?php };?>
                                                    </select>
                                                </div>
                                                <div class="linkage-justify-list">
                                                    <select class="form-control" id="city" name="city_id" datatype="*" sucmsg=" " nullmsg="请选择城市">
                                                        <?php if (!empty($model->city_id)) :?>
                                                            <option value="<?=$model->city_id ?>" selected="selected" ><?=$model->city_name ?></option>
                                                        <?php else :?>
                                                            <option value="">请选择市</option>
                                                        <?php endif ;?>
                                                    </select>
                                                </div>
                                                <div class="linkage-justify-list">
                                                    <select class="form-control" id="district" name="district_id" datatype="*" sucmsg=" " nullmsg="请选择区/县">
                                                        <?php if (!empty($model->district_id)) :?>
                                                            <option value="<?=$model->district_id ?>" selected="selected" ><?=$model->district_name ?></option>
                                                        <?php else :?>
                                                            <option value="">请选择区</option>
                                                        <?php endif ;?>
                                                    </select>
                                                </div>
                                                <div class="justify_fix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="details-column">
                                        <label class="details-column-title"><i class="red-i"></i>详细地址：</label>
                                        <div class="details-column-content">
                                            <input type="text" name="address" datatype="*" nullmsg="请输入详细地址" value="<?= isset($model->address)?$model->address:'' ?>" maxlength="200" placeholder="请输入详细地址">
                                        </div>
                                    </div>
                                    <div class="details-column">
                                        <label class="details-column-title">公司简介：</label>
                                        <div class="details-column-content" style="width: 400px;position: relative;">
                                            <textarea type="text" maxlength="200"  id="remarks" name="company_desc"><?= isset($model->company_desc)?$model->company_desc:'' ?></textarea>
                                        </div>
                                        <input type="hidden" name="mark" value="1">
                                    </div>
                                    <div class="details-column">
                                        <div class="details-column-content">
                                            <input type="submit" name="" value="保存" class="details-column-submit">
                                        </div>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            <?php }?>

                        <?php }?>

                        <?php if($derect_company_id==$top_id) :?>
                            <!-- 认证信息 -->
                            <?php if (isset($model->id) && $model->status == 2 && isset($model->is_main)){ ?>
                                <div  class="tab-pane fade" id="credentials">
                                    <h5 class="fbt">类型</h5>
                                    <div class="details-column">
                                        <span class="details-column-title">公司类型：</span>
                                        <div class="details-column-content">
                                            <?php if (isset($model->user_type)): ?>
                                                <?= $model->user_type == 1 ? '企业' : '个体工商户'  ?>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                    <h5 class="fbt">企业信息</h5>
                                    <div class="details-column">
                                        <span class="details-column-title">证件类型：</span>
                                        <div class="details-column-content">
                                            <?php if (isset($model->document_type)){?>
                                                <?= $model->document_type == 1 ? '普通营业执照（存在独立的组织机构代码证）' : '多证合一营业执照（不存在独立的组织机构代码证）'  ?>
                                            <?php };?>
                                        </div>
                                    </div>
                                    <div class="details-column">
                                        <span class="details-column-title">公司全称：</span>
                                        <div class="details-column-content"><?= isset($model->company ) ?$model->company:'' ?></div>
                                    </div>
                                    <div class="details-column">
                                        <span class="details-column-title">营业执照注册号：</span>
                                        <div class="details-column-content"><?= isset($model->business_licence_num ) ?$model->business_licence_num:'' ?></div>
                                    </div>
                                    <div class="details-column">
                                        <span class="details-column-title">企业所在地：</span>
                                        <div class="details-column-content"><?= $model->province_name.'-'.$model->city_name.'-'.$model->district_name ?></div>
                                    </div>
                                    <div class="details-column">
                                        <span class="details-column-title">详细地址：</span>
                                        <div class="details-column-content"><?= isset($model->address ) ?$model->address:'' ?></div>
                                    </div>
                                    <div class="details-column">
                                        <span class="details-column-title">经营范围：</span>
                                        <div class="details-column-content"><?= isset($model->scope_operation ) ?$model->scope_operation:'' ?></div>
                                    </div>
                                    <h5 class="fbt">联系人信息</h5>
                                    <div class="details-column">
                                        <span class="details-column-title">联系人姓名：</span>
                                        <div class="details-column-content"><?= isset($model->contact ) ? $model->contact :'' ?></div>
                                    </div>
                                    <div class="details-column">
                                        <span class="details-column-title">联系人电话：</span>
                                        <div class="details-column-content"><?= isset($model->mobile) ? $model->mobile :'' ?></div>
                                    </div>
                                    <h5 class="fbt">营业执照及组织机构代码证上传</h5>
                                    <div class="details-column">
                                        <span class="details-column-title">营业执照：</span>

                                        <div class="details-column-content">
                                            <?php if($model->business_licence){?>
                                                <div class="details-column-tupian">
                                                    <a target="_blank" href="javascript:">
                                                        <img class="enlarge-img" src="<?= isset($model->business_licence ) ?$model->business_licence:'' ?>?x-oss-process=image/resize,w_300/auto-orient,1">
                                                    </a>
                                                </div>
                                            <?php };?>
                                        </div>

                                    </div>
                                    <div class="details-column">
                                        <span class="details-column-title">资质证明：</span>
                                        <div class="details-column-content">
                                            <?php if($model->code_certificate){?>
                                                <div class="details-column-tupian">
                                                    <a target="_blank" href="javascript:">
                                                        <img  class="enlarge-img"  src="<?= isset($model->code_certificate ) ?$model->code_certificate:'' ?>?x-oss-process=image/resize,w_300/auto-orient,1">
                                                    </a>
                                                </div>
                                            <?php };?>
                                        </div>
                                    </div>
                                </div>
                            <?php };?>
                        <?php endif;?>
                    </div>
                    <!-- 内容end -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- 图片放大 -->
<div class="imgEnlarge-box">
    <div class="hezi">
        <div class="imgEnlarge-chlid">
            <img src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" alt="placeholder+image" id="enlargeImg">
        </div>
    </div>
</div>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
    // 图片放大
    $('.enlarge-img').on('click',function() {
        var imgUrl = $(this).attr('src');
        imgUrl = imgUrl.substring(0,imgUrl.indexOf('?'));
        imgUrl =imgUrl +'?x-oss-process=image/auto-orient,1'
        var imgaa = new Image();
        var winWidht = $(window).width();
        var winHight = $(window).height();
        setTimeout(function() {
            imgaa.src=imgUrl;
            $('.imgEnlarge-chlid img').css('width','auto');
            $('.imgEnlarge-chlid img').css('height','auto');
            if(imgaa.width>winWidht){
                $('.imgEnlarge-chlid img').css('width',winWidht-40+'px');
                $('.imgEnlarge-chlid img').css('height','auto')
            }else{
                $('.imgEnlarge-chlid img').css('width',imgaa.width+'px');
                $('.imgEnlarge-chlid img').css('height','auto');
            }
            if(imgaa.height>winHight){
                $('.imgEnlarge-chlid img').css('height',winHight-40+'px');
                $('.imgEnlarge-chlid img').css('width','auto');
            }else{
                $('.imgEnlarge-chlid img').css('height',imgaa.height+'px');
                $('.imgEnlarge-chlid img').css('width','auto')
            }
        },100)

        $('#enlargeImg').attr('src',imgUrl);
        $('.imgEnlarge-box').show();
    })
    $('.imgEnlarge-box').on('click',function() {
        $('.imgEnlarge-box').hide();
    })
    // 字数限制
    $("#remarks").keyup(function(){
        if($("#remarks").val().length > 200){
            $("#remarks").val( $("#remarks").val().substring(0,200) );
        }
        $("#word").text( $("#remarks").val().length ) ;
    });
    // 图片上传
    $('.filePrew').change(function () {
        var fil = this.files;
        for (var i = 0; i < fil.length; i++) {
            var reader = new FileReader();
            reader.readAsDataURL(fil[i]);
            reader.onload = function()
            {
                $('#headSculpture').attr('src',reader.result);
            };
        }

    });

    $("#manufactor-view").Validform({
        tiptype:3,
        beforeSubmit:function(curform){

        }
    });

    //根据开通城市父级id获取市级数据
    $("#province").change(function() {
        $("#city").html("<option value=''>请选择市</option>");
        $("#district").html("<option value=''>请选择区</option>");
        var pid = $("#province").val();
        if(pid != ''){
            $.get("/account/get-city", { "province_id": pid }, function (data) {
                $("#city").html(data);
            });
        }
    });
    //根据城市获取区县
    $("#city").change(function() {
        var pid = $("#city").val();
        if(pid != ''){
            $.get("/account/get-district", { "city_id": pid }, function (data) {
                $("#district").html(data);
            })
        }
    });

</script>
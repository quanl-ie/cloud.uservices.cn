<?php
use yii\helpers\Url;
?>
<style type="text/css">
	body{color: #444}
	.uncertified-img{width: 100px;margin:0 auto;}
	.uncertified-img>img{width: 100%;}
	.see-btn{display: inline-block;padding: 10px 20px;background: #2693ff;border-radius: 5px;color: #fff;margin-top: 10px}
	.un-content{font-size: 14px;margin: 10px auto;width: 350px}
	.un-title{margin-top:20px;font-size: 18px;font-weight: 600 }
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div style="margin-top: 150px;text-align: center;">
                <div class="uncertified-img">
                    <img src="<?php echo Url::to('/img/tgyz.png');?>">
                </div>
                <h4 class="un-title">提交成功</h4>
                <p class="un-content">我们会在1-3个工作日内为您审核，请耐心等待</p>

                <a href="/manufactor/view" class="btn btn-success" style="color: #fff;background: #2693ff">查看认证信息</a>

            </div>
        </div>
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
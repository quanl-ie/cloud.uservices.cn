<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    $this->title = '填写认证信息';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/js/Validform_v5.3.2/css/demo.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/js/Validform_v5.3.2/css/wp-syntax.css');?>">
<style type="text/css">
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
        border-top:2px solid #2693ff;
    }
    .details-column-content>input{
        width: 500px;
    }
    .three-linkage{
        width: 500px;
        height: 38px;
    }
    .linkage-justify-list{
        width: 150px;
    }
    .details-column-content>textarea{
        width: 500px;
    }
    .details-column-title{
        width: 140px;
    }
    .details-column-content{
        margin-left: 140px;
    }
    .brand-label{
        font-size: 14px;font-weight: 100;
    }
    .fbt{font-size: 16px;font-weight: 600;font-family: "微软雅黑";margin:20px 0 0 0;}
    .filePrew{height: 200px;width: 200px}
    .Validform_wrong, .Validform_right{position: absolute;bottom: -18px;left: 0}
    .jianjie>.details-column-content>.Validform_wrong{bottom: -8px};
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">填写认证信息</span>
    <?php if (isset($model->id) && $model->status == 3): ?>
        <font color="red" style="position: absolute;left: 120px;top: 8px;">审核未通过</font>
    <?php endif;?>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php
                        $form = ActiveForm::begin([
                            'options' => [
                                'class' => 'form-horizontal',
                                'id' => 'manufactor-add',
                                'name' => 'form1',
                                'enctype' => 'multipart/form-data',
                            ]
                        ]);
                    ?>
                    <?php if (isset($model->id) && $model->status == 3): ?>
                        <div style="background-color:#D6D6D6" >
                            <p style="padding:10px">驳回原因：</p>
                            <span><?= isset($model->audit_opinion) ? $model->audit_opinion : '' ?></span>
                        </div>
                    <?php endif;?>
                    <input type="hidden" value="<?= isset($model->id) ? $model->id : ''  ?>" name="id" />
                    <h5 class="fbt">类型</h5>
                    <div class="details-column">
                        <label class="details-column-title">公司类型：</label>
                        <div class="details-column-content">
                            <?php if (isset($model->user_type) && $model->user_type == 1): ?>
                                <span class="radio-span">
                                    <input type="radio" id="enterprise" value="1" name="user_type" checked="checked"/>
    								<label for="enterprise">企业</label>
            					</span>
                                <span  class="radio-span">
                                    <input type="radio" id="Selfemployed" value="2" name="user_type" />
    								<label for="Selfemployed">个体工商户</label>
            					</span>
                            <?php else :?>
                                <span class="radio-span">
                                    <input type="radio" id="enterprise" value="1" name="user_type" checked="checked"/>
    								<label for="enterprise">企业</label>
            					</span>
                                <span  class="radio-span">
                                    <input type="radio" id="Selfemployed" value="2" name="user_type" />
    								<label for="Selfemployed">个体工商户</label>
            					</span>
                            <?php endif;?>
                        </div>
                    </div>
                    <h5 class="fbt">企业信息</h5>
                    <div class="details-column">
                        <label class="details-column-title">
                            证件类型：
                        </label>
                        <div class="details-column-content">
                            <?php if (isset($model->document_type) && $model->document_type == 1): ?>
                                <span class="radio-span">
                                    <input type="radio" id="ordinary" value="1" name="document_type" checked="checked"/>
    								<label for="ordinary">普通营业执照（存在独立的组织机构代码证)</label>
            					</span>

                                <span  class="radio-span">
                                    <input type="radio" id="Multicard" value="2" name="document_type" />
    								<label for="Multicard">多证合一营业执照（不存在独立的组织机构代码证）</label>
            					</span>
                            <?php else:?>
                                <span class="radio-span">
                                    <input type="radio" id="ordinary" value="1" name="document_type" checked="checked"/>
    								<label for="ordinary">普通营业执照（存在独立的组织机构代码证)</label>
            					</span>

                                <span  class="radio-span">
                                    <input type="radio" id="Multicard" value="2" name="document_type" />
    								<label for="Multicard">多证合一营业执照（不存在独立的组织机构代码证）</label>
            					</span>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="details-column">
                        <label class="details-column-title"><i class="red-i">*</i>公司全称：</label>
                        <div class="details-column-content">
                            <input type="text" name="company" value="<?= isset($model->company) ? $model->company : ''  ?>" datatype="*" sucmsg=" " maxlength="70" nullmsg="请输入公司全称" placeholder="请填写营业执照公司名称">
                            <span class="error-span"></span>
                        </div>
                    </div>
                    <div class="details-column">
                        <label class="details-column-title"><i class="red-i">*</i>营业执照注册号：</label>
                        <div class="details-column-content">
                            <input type="text" name="business_licence_num" value="<?= isset($model->business_licence_num) ? $model->business_licence_num : ''  ?>" sucmsg=" " maxlength="70" datatype="*" nullmsg="请输入营业执照注册号" placeholder="请填写营业执照上的注册号">
                            <span class="error-span"></span>
                        </div>
                    </div>
                    <div class="details-column">
                        <label class="details-column-title"><i class="red-i">*</i>企业所在地：</label>
                        <div class="details-column-content">
                            <div class="three-linkage">
                                <div class="linkage-justify-list">
                                    <select class="form-control" id="province" name="province_id" datatype="*" sucmsg=" " nullmsg="请选择省份">
                                        <?php if (!empty($model->province_id)) : ?>
                                            <option value="<?=$model->province_id ?>" selected="selected" ><?=$model->province_name ?></option>
                                            <?php foreach ($province as $key => $val): ?>
                                                <option value="<?=$val['region_id'] ?>"><?=$val['region_name'] ?></option>
                                            <?php endforeach;?>
                                        <?php else :?>
                                            <option value="">请选择省</option>
                                            <?php foreach ($province as $key => $val): ?>
                                                <option value="<?=$val['region_id'] ?>"><?=$val['region_name'] ?></option>
                                            <?php endforeach;?>
                                        <?php endif ;?>
                                    </select>
                                </div>
                                <div class="linkage-justify-list">
                                    <select class="form-control" id="city" name="city_id" datatype="*" sucmsg=" " nullmsg="请选择城市">
                                        <?php if (!empty($model->city_id)) :?>
                                            <option value="<?=$model->city_id ?>" selected="selected" ><?=$model->city_name ?></option>
                                        <?php else :?>
                                            <option value="">请选择市</option>
                                        <?php endif ;?>
                                    </select>
                                </div>
                                <div class="linkage-justify-list">
                                    <select class="form-control" id="district" name="district_id" datatype="*" sucmsg=" " nullmsg="请选择区/县">
                                        <?php if (!empty($model->district_id)) :?>
                                            <option value="<?=$model->district_id ?>" selected="selected" ><?=$model->district_name ?></option>
                                        <?php else :?>
                                            <option value="">请选择区</option>
                                        <?php endif ;?>
                                    </select>
                                </div>

                                <div class="justify_fix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="details-column">
                        <label class="details-column-title"><i class="red-i">*</i>详细地址：</label>
                        <div class="details-column-content">
                            <input type="text" name="address" value="<?= isset($model->address) ? $model->address : ''  ?>" maxlength="200" datatype="*" sucmsg=" " nullmsg="请输入详细地址" placeholder="请填写公司详细地址（具体到门牌号）">
                            <span class="error-span"></span>
                        </div>
                    </div>
                    <div class="details-column">
                        <label class="details-column-title"><i class="red-i">*</i>经营范围：</label>
                        <div class="details-column-content" style="width: 500px;position: relative;height: 150px">

                            <textarea id="remarks" name="scope_operation" datatype="*" maxlength="200" sucmsg=" " nullmsg="请输入经营范围" placeholder="请与营业执照的经营范围保持一致" id="remarks"  ><?= isset($model->scope_operation) ? $model->scope_operation : ''  ?></textarea>
                            <p class="xianzhi"><span  id='word'>0</span>/200</p>
                            <div id="note" class="note">
                                <!-- <font color="#ccc">请与营业执照的经营范围保持一致</font> -->
                            </div>
                            <span class="error-span"></span>
                        </div>
                    </div>

                    <h5 class="fbt">联系人信息</h5>
                    <div class="details-column">
                        <label class="details-column-title"><i class="red-i">*</i>联系人姓名：</label>
                        <div class="details-column-content">
                            <input type="text" name="contact" maxlength="20" sucmsg=" " value="<?= isset($model->contact) ? $model->contact : ''  ?>" datatype="*" nullmsg="请输入联系人姓名" placeholder="">
                            <span class="error-span"></span>
                        </div>
                    </div>
                    <div class="details-column">
                        <label class="details-column-title"><i class="red-i">*</i>联系人电话：</label>
                        <div class="details-column-content">
                            <input type="text" name="mobile" datatype="m" sucmsg=" " maxlength="11" value="<?= isset($model->mobile) ? $model->mobile : ''  ?>" nullmsg="请输入联系人电话" placeholder="">
                            <span class="error-span"></span>
                        </div>
                    </div>
                    <h5 class="fbt">营业执照及组织机构代码证上传</h5>
                    <div class="details-column" style="height: 200px">
                        <label class="details-column-title"><i class="red-i">*</i>营业执照：</label>
                        <div class="details-column-content">
                            <div class="aptitude-div">

                                <img src="<?= isset($model->business_licence) ? $model->business_licence : Url::to('/images/tj.png')  ?>" class="img-aa">
                                <input type="file" name="business_licence" value="<?= isset($model->business_licence) ? $model->business_licence : ''  ?>" maxlength="255" class="filePrew">
                            </div>
                            <div  class="aptitude-div">
                                <img src="<?php echo Url::to('/images/sl.png');?>">
                            </div>
                            <span class="error-span"></span>
                        </div>
                    </div>


                    <div class="details-column"  style="height: 200px">

                        <label class="details-column-title"><i class="red-i">*</i>组织机构代码证：</label>
                        <div class="details-column-content">
                            <div class="aptitude-div">

                                <img src="<?= isset($model->code_certificate) ? $model->code_certificate : Url::to('/images/tj.png')  ?>" class="img-aa">
                                <input type="file" name="code_certificate" value="<?= isset($model->code_certificate) ? $model->code_certificate : ''  ?>" maxlength="255" class="filePrew">
                            </div>
                            <div  class="aptitude-div">
                                <img src="<?php echo Url::to('/images/sl.png');?>">
                            </div>
                            <span class="error-span"></span>
                        </div>
                    </div>
                    <!-- 保存 -->
                    <div class="details-column">

                        <div class="details-column-content">
                            <input type="submit" name="" value="提交认证" class="details-column-submit" style="line-height: 1">
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script type="text/javascript">
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
    // 字数限制
    $("#remarks").keyup(function(){
        $('#note').hide();
        if($("#remarks").val().length > 200){
            $("#remarks").val( $("#remarks").val().substring(0,200) );
        }else if($("#remarks").val().length < 1){
            $('#note').show();
        }
        $("#word").text( $("#remarks").val().length ) ;
    });
    //表单验证
    $("#manufactor-add").Validform({
        tiptype:3
    });

    // 图片上传
    $('.filePrew').change(function () {
        var fil = this.files;
        var self = $(this).prev('.img-aa');
        for (var i = 0; i < fil.length; i++) {
            var reader = new FileReader();
            reader.readAsDataURL(fil[i]);
            reader.onload = function()
            {
                self.attr('src',reader.result);
            };
        }
    })
    // 多选点击事件
    $('.add-tjpp').on('click',function() {
        $('.checkbox-box').show()
    })
    var exhibitionCheckbox = $('.exhibition-checkbox');
    var objInput = $('.checkbox-span').find('input');
    var check_val = [];
    $('#checkboxOk').on('click',function () {
        exhibitionCheckbox.html("");
        for (var i = 0; i < objInput.length; i++) {
            if(objInput[i].checked){
                var data = objInput[i].getAttribute('data');
                check_val.push(data);
                var cc = '<span>'+data+'</span>';
                exhibitionCheckbox.append(cc);
            }
        }
        $('.checkbox-box').hide()
    })
    //根据开通城市父级id获取市级数据
    $("#province").change(function() {
        $("#city").html("<option value=''>请选择市</option>");
        $("#district").html("<option value=''>请选择区</option>");
        var pid = $("#province").val();
        if(pid != ''){
            $.get("/account/get-city", { "province_id": pid }, function (data) {
                $("#city").html(data);
            });
        }
    });
    //根据城市获取区县
    $("#city").change(function() {
        var pid = $("#city").val();
        if(pid != ''){
            $.get("/account/get-district", { "city_id": pid }, function (data) {
                $("#district").html(data);
            })
        }
    });
    // 字数限制
    $("#remarks").keyup(function(){
        if($("#remarks").val().length > 200){
            $("#remarks").val( $("#remarks").val().substring(0,200) );
        }
        $("#word").text( $("#remarks").val().length ) ;
    });
</script>
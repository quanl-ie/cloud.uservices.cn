<?php
use yii\helpers\Url;
?>
<style type="text/css">
	.uncertified-img{width: 100px;margin:0 auto;}
	.uncertified-img>img{width: 100%;}
	.see-btn{display: inline-block;padding: 10px 20px;background: #2693ff;border-radius: 5px;color: #fff;}
	.un-content{font-size: 14px;margin: 10px auto;width: 350px}
	.un-title{margin-top:20px;font-size: 18px;font-weight: 600 }
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div style="margin-top: 150px;text-align: center;">
                <div class="uncertified-img">
                    <img src="<?php echo Url::to('/img/wrz.png');?>">
                </div>
                <h4 class="un-title">等待审核中</h4>
                <p class="un-content">您的认证信息正在进行审核，审核通过后即可享有所有功能使用权，如有疑问请联系客服010-60844544</p>
                <a class="see-btn" href="/manufactor/view" style="color: #fff">查看认证信息</a>
            </div>
        </div>
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
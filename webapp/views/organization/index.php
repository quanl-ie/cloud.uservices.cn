<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
$this->title = '组织架构';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<style>
    ul{margin-left: 20px;}
    .showDepartment{color:#5D5F63;}
    td{position: static;}
    #main-content>.panel-body{height: auto;}
    .text-left>div{height: 100%;position: relative;padding-left: 10px;}
    .text-left>div .icon{position: absolute;left: -5px;top: -2px;}
    li a:hover{color: #333;}
</style>
<div class="jn-title-box no-margin-bottom">
     <span class="jn-title">组织架构</span>
</div>
<section id="main-content">
    <div class="panel-body" style="position: relative;">
        <div class="hrc-left-box">
           <div class="hrc-left-sousuo">
               <input type="text" id="departmentName" class="form-control" style="width: 130px;" placeholder="机构名称">
               <button id="departmentSearch" class="btn btn-orange">搜索</button>
           </div>
           <div class="tree-shape-nav">
               <ul>
                   <?php foreach ($navData as $val): ?>
                   <li class="firstNav">
                       <span><i data-id="<?php echo $val['id'];?>" <?php if($val['sub']==0):?>style="display: none;"<?php endif;?> class="i<?php echo $val['id']; ?> icon2-shixinyou spanNav"></i><a class="showDepartment" href="javascript:void(0);" data-id="<?php echo $val['id'];?>"><?php echo $val['name'];?></a></span>
                       <ul></ul>
                   </li>
                   <?php endforeach; ?>
               </ul>
           </div>
        </div>
        <div class="hrc-right-box">
            <div class="right-top-tab">
                <ul id="myTab" class="nav nav-tabs">
                    <li class="active"><a href="#enable" data-toggle="tab" data-id="1" class="navTabs">启用中</a></li>
                    <li><a href="#disable" data-toggle="tab" data-id="2" class="navTabs">已禁用</a></li>
                </ul>
                <div class="hrc-right-sousuo">
                    <input id="keyword" type="text" class="form-control" style="width: 200px;" placeholder="机构名称/联系人姓名/电话">
                    <button id="btnSearch" class="btn btn-orange">搜索</button>
                </div>
            </div>
            <div id="myTabContent" class="tab-content border-right-auto">
                <div class="tab-pane fade in active fold-tb-box" id="tableList">
                   <?php echo $this->render( '_list', [
                           'data'             => $data,
                            'type'            => $type,
                            'selfDepartmentId'=> $selfDepartmentId,
                            'directCompanyId' => $directCompanyId,
                            'selfRoles'       => $selfRoles
                   ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var departmentFlag = true;
    var selfDepartmentId = '<?php echo $selfDepartmentId;?>';
    $(function() {

        var documentHeight = $(document).height()-200
       documentHeight>$('.hrc-right-box').height()?$('.hrc-right-box').css('height',documentHeight+'px'):$('.hrc-right-box').css('height','auto');
       //导航点击
        var spanNavLength = $('.spanNav').length;
        var textLeftLength = $('.text-left').length;
        $('body').delegate('.spanNav', 'click',{},function () {

            var _self = $(this).parent('span');
            if($.trim(_self.next('ul').html()) == '' || departmentFlag == true)
            {
                departmentFlag = false;
                $.getJSON('/organization/ajax-get-department', {pid:$(this).data('id'),is_search:0}, function (json) {
                    if(json.success == true){
                        var data = json.data;
                        spanNavLength = spanNavLength +data.length;
                        if (spanNavLength-10>textLeftLength) {
                            $('.hrc-right-box').css('height',spanNavLength*40+'px');
                        }
                        var html= '';
                        $.each(data,function(i, elm) {
                            html+='<li>\n' +
                                '      <span>';
                            html+= '<i '+(elm.sub == 1?'':'style="display:none;"')+' data-id="'+elm.id+'" class="i'+elm.id +' icon2-shixinyou spanNav"></i>';
                            html+= '<a class="showDepartment" href="javascript:void(0);" data-id="'+elm.id+'">'+ elm.name+'</a></span>\n' +
                                '      <ul></ul>\n'+
                                '  </li>\n';
                        });
                        if(html == ''){
                            _self.find('i').hide();
                        }
                        else {
                            _self.next('ul').html(html).show();
                            _self.find('i').removeClass('icon2-shixinyou');
                            _self.find('i').addClass('icon2-shixinxia');
                            _self.find('i').show();
                        }

                    }
                });
            }
            else {
                 _self.next('ul').toggle();

                 if(_self.next('ul').is(':hidden')){
                       _self.find('i').removeClass('icon2-shixinxia');
                        _self.find('i').addClass('icon2-shixinyou');
                 }
                 else {
                     _self.find('i').removeClass('icon2-shixinyou');
                     _self.find('i').addClass('icon2-shixinxia');
                 }
            }
            
        });

       //查看备注
       $('body').delegate('.viewRemark','click', {},function () {
            var id = $(this).data('id');
            $.getJSON('/organization/view-remark', {id:id}, function (json) {
                if(json.success == true){
                    alert(json.data == ''?'暂无数据':json.data);
                }
                else{
                    alert(json.message);
                }
            })
       });

       //机构列表展开收起
        $('body').delegate('.toggleOpenClose','click',{},function () {
            // console.log(spanNavLength);
             var id = $(this).data('id');
             var index = $(this).parents('tr').index();
             var _self = $(this);
             var paddingLeft = Number($(this).parent('div').css('marginLeft').match(/(\S*)px/)[1]);
             var childTr = $(this).parents('table').find('.tr'+index);
             if(childTr.length == 0)
             {
                 var trClass = ''
                 if(typeof($(this).parents('tr').attr('class'))!='undefined'){
                     trClass = $(this).parents('tr').attr('class');
                }
                $.getJSON('/organization/index',{pid:id,status:departmentStatus}, function (json) {

                     if(json.success == true){
                         var html = '';
                         var data = json.data;
                         textLeftLength = textLeftLength + data.length;
                         $.each(data,function(i, elm) {
                             html += '<tr class="'+trClass+' tr'+index+'">\n' +
                                '    <td  class="text-left"><div style="margin-left:'+(paddingLeft+16)+'px">';
                                if(elm.sub == 1){
                                 html+=' <a class="toggleOpenClose" data-id="'+elm.id+'" href="javascript:void(0);"><i class="icon2-shixinyou"></i></a>';
                                }
                                html+= elm.name+'</div></td>\n'+
                                '    <td>'+elm.weight+'</td>\n' +
                                '    <td>'+elm.parent_name+'</td>\n' +
                                '    <td>'+elm.type_desc+'</td>\n' +
                                '    <td>'+elm.contacts+'</td>\n' +
                                '    <td>'+elm.address_desc+'</td>\n' +
                                '    <td>'+elm.phone+'</td>\n' +
                                '    <td>'+elm.email+'</td>\n' +
                                '    <td><a href="javascript:" class="viewRemark" data-id="'+elm.id+'">点击查看</a></td>\n' +
                                '    <td>'+elm.create_time+'</td>\n' +
                                '    <td>\n' ;
                                if(elm.status == 1){
                                    <?php if(in_array('/organization/edit-department',$selfRoles)):?>
                                    html+='<a href="/organization/edit-department?id='+elm.id+'">修改</a>\n' ;
                                    <?php endif;?>
                                    <?php if(in_array('/organization/add-department',$selfRoles)):?>
                                    html+='<a href="/organization/add-department?id='+elm.id+'">添加下级</a>\n';
                                    <?php endif; ?>
                                    if(elm.id != window.selfDepartmentId){
                                        <?php if(in_array('/organization/disable',$selfRoles)):?>
                                        html+='<a href="javascript:void(0);" data-type="'+elm.type+'" data-url="/organization/disable?id='+elm.id+'" data-id="'+elm.id+'" class="disable-a btnDisable">禁用</a>\n';
                                        <?php endif; ?>
                                    }
                                    if(elm.type == 1) {
                                        <?php if(in_array('/organization/service-config-view',$selfRoles)):?>
                                        html+='<a href="/organization/service-config-view?id=' + elm.id + '" v-if="item.configure==true">设置服务配置</a>\n' ;
                                        <?php endif; ?>
                                    }
                                }
                                else if(elm.status == 2){
                                    <?php if(in_array('/organization/enable',$selfRoles)):?>
                                    html+='<a href="javascript:void(0);" data-type="'+elm.type+'" data-url="/organization/enable?id='+elm.id+'" class="disable-a btnEnable">启用</a>\n';
                                    <?php endif; ?>
                                    <?php if(in_array('/organization/del',$selfRoles)):?>
                                    html+='<a href="javascript:void(0);" data-type="'+elm.type+'" data-url="/organization/del?id='+elm.id+'" data-id="'+elm.id+'" class="disable-a btnDel">删除</a>\n';
                                    <?php endif; ?>
                                }

                                html+='    </td>\n' +
                                '</tr>';
                         });
                         if(spanNavLength<textLeftLength+10){
                            $('.hrc-right-box').css('height',textLeftLength*70+'px');
                        }
                        documentHeight>$('.hrc-right-box').height()?$('.hrc-right-box').css('height',documentHeight+'px'):$('.hrc-right-box').css('height','auto');
                         _self.parents('tr').after(html);
                         _self.find('i').removeClass('icon2-shixinyou');
                         _self.find('i').addClass('icon2-shixinxia');
                     }
                 });
             }
             else
             {
                 if(_self.find('i').hasClass('icon2-shixinyou')){
                     childTr.show();
                 }
                 else {
                     childTr.hide();
                 }

                 if(childTr.is(':hidden')){
                    _self.find('i').addClass('icon2-shixinyou');
                    _self.find('i').removeClass('icon2-shixinxia');
                 }else{
                    _self.find('i').removeClass('icon2-shixinyou');
                    _self.find('i').addClass('icon2-shixinxia');
                 }
             }
        });
    });

    //导航点击显示机构
    var departmentId = 0;
    var departmentStatus = 1;
    var keyword = '';
    var searchType = 2;
    $('body').delegate('.showDepartment','click',{}, function () {
        departmentId = $(this).data('id');
        $('.showDepartment').css('color','#333');
        $(this).css('color','#428bca');
        searchDepartment(departmentId,departmentStatus,'',2);
    });

    //标签切换
    $('body').delegate('.navTabs', 'click', {}, function () {
        departmentStatus = $(this).data('id');
        searchDepartment(departmentId,departmentStatus,'',2);
    });

    //搜索
    $('#btnSearch').click(function () {
        keyword = $('#keyword').val();
        searchType = 3;
        searchDepartment(departmentId,departmentStatus,keyword,searchType);
    });

    //搜索方法
    function searchDepartment(pid,status,keyword,type) {
        $.getJSON('/organization/index', {pid:pid,type:type,status:status,name:keyword}, function (json) {
            if(json.success == true){
                $('#tableList').html(json.data);
            }
        });
    }

    //回车搜索
    $('#keyword').keyup(function (event) {
         if(event.keyCode == 13){
             $('#btnSearch').click();
         }
    });

    //部门树搜搜
    $('#departmentSearch').click(function () {
         var keywrod = $.trim($('#departmentName').val());
        $.getJSON('/organization/ajax-get-department', {name:keywrod,is_search:2}, function (json) {
            if(json.success == true){
                var data = json.data;

                var html= '';
                $.each(data,function(i, elm) {
                     html+='<li>\n' +
                        '      <span>';
                        if(elm.sub == 1 && keywrod == ''){
                            html+= '<i data-id="'+elm.id+'" class="icon2-shixinyou '+(elm.sub==1?'spanNav':'')+'"></i>';
                        }
                        html+='         <a class="showDepartment" href="javascript:void(0);" data-id="'+elm.id+'">'+ elm.name+'</a></span>\n' +
                        '      <ul></ul>\n'+
                        '  </li>\n';
                });
                if(html == ''){
                    $('.tree-shape-nav ul').html("<li style='margin-top:100px;text-align: center;'>未找到相关机构</li>").show();
                }
                else {
                    $('.tree-shape-nav ul').html(html).show();
                }
            }
        });

    });
    $('#departmentName').keyup(function (event) {
        if(event.keyCode == 13){
            $('#departmentSearch').click();
        }
    });


    //禁用
    $('body').delegate('.btnDisable','click',{},function () {
        var url  = $(this).data('url');
        var type = $(this).data('type');
        var pid  = $(this).data('pid');
        var id   = $(this).data('id');

        var tip = '您确定要禁⽤该经销商吗？禁⽤后该经销商下所有⽤户将不能登录。';
        if(type != 1)
        {
            var hasUser = false;
            $.ajax({
                url:'/organization/ajax-has-department-user',
                data:{id:id},
                async:false,
                dataType:'json',
                success:function (json) {
                    if(json.success == true){
                        if(json.data.res == 1){
                            hasUser = true;
                            tip = '该机构下有员⼯。⽆法禁⽤';
                        }
                        else {
                            tip = '您确定要禁用该机构吗？';
                        }
                    }
                }
            });

            if(hasUser == true){
                alert(tip);
                return false;
            }
        }

        confirm(tip,function (obj) {
            $.getJSON(url,{},function (json) {
                if(json.success == true){
                    alert('禁用成功');
                    searchDepartment(departmentId,departmentStatus,keyword,searchType);

                    $('.i'+pid).click();
                    window.departmentFlag = true;
                    $('.i'+pid).click();
                }
                else {
                    alert(json.message);
                }
            });
        },this);
    });

    //启动
    $('body').delegate('.btnEnable','click',{},function () {
        var url  = $(this).data('url');
        var type = $(this).data('type');
        var pid  = $(this).data('pid');

        var tip = '您确定要启用该经销商吗？启用后恢复至原机构下，且经销商下所有用户可登陆系统。';
        if(type != 1){
            tip = '您确定要启用该机构吗？启用后恢复至原机构下。';
        }

        confirm(tip,function (obj) {
            $.getJSON(url,{},function (json) {
                if(json.success == true){
                    alert('启用成功');
                    searchDepartment(departmentId,departmentStatus,keyword,searchType);

                    $('.i'+pid).click();
                    window.departmentFlag = true;
                    $('.i'+pid).click();
                }
                else {
                    alert(json.message);
                }
            });
        },this);
    });

    //删除
    $('body').delegate('.btnDel','click',{},function () {
        var url  = $(this).data('url');
        var type = $(this).data('type');
        var id   = $(this).data('id');

        var tip = '您确定要删除该经销商吗，删除后将不可恢复，但不影响原数据？';
        if(type != 1){
            tip = '您确定要删除该经机构吗，删除后将不可恢复，但不影响原数据？';
        }
        else {
            $.ajax({
                url:'/organization/ajax-has-sub-department',
                data:{type:1,id:id},
                async:false,
                dataType:'json',
                success:function (json) {
                    if(json.success == true ){
                        if(json.data.res == 1){
                            tip = '您确定要删除该经销商及以下⼦机构吗，删除后将不可恢复，但不影响原数据？';
                        }
                    }
                }
            });
        }

        confirm(tip,function (obj) {
            $.getJSON(url,{},function (json) {
                if(json.success == true){
                    alert('删除成功');
                    searchDepartment(departmentId,departmentStatus,keyword,searchType);
                }
                else {
                    alert(json.message);
                }
            });
        },this);
    });



    <?php if(Yii::$app->session->hasFlash('message')): ?>
    alert('<?php echo Yii::$app->session->getFlash('message');?>');
    <?php endif;?>
</script>
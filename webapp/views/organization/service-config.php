<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
$this->title = '设置服务配置';
$this->params['breadcrumbs'][] = ['url'=>'/organization/index','label'=>'组织架构'];
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<style>
    ul{margin-left: 20px;}
    li>ul{display: block;}
    .region-box-ul{margin-left: 0;}
    td>.region-box{height: 300px;}
    .tb-layerbox li>span>input[type="checkbox"] + label::before{top: 6px;}
    .tb-layerbox li>span>input[type="checkbox"]:checked + label::before{top: 6px;}
    .tb-layerbox li>span{padding-left: 33px;}
    .td-chlid-box{margin:15px 0px;}
</style>
<div class="jn-title-box no-margin-bottom">
     <span class="jn-title">设置服务配置</span>
     <div class="right-btn-box">
            <?php if($srcPage == 1): ?>
                <button class="btn bg-f7 mgr20" onclick="javascript:document.location.href='/organization/index'">取消</button>
            <?php else: ?>
                <button class="btn bg-f7 mgr20" onclick="javascript:history.back();">取消</button>
            <?php endif; ?>
            <input type="hidden" id="srcPage" value="<?php echo $srcPage; ?>" />
            <input type="submit" class="btn btn-orange addBm btnSumit" value="保存" data-type="1" />
            <?php if($hasAddUser == false && $srcPage == 1): ?>
                <?php if(in_array('/organization/add-user',$selfRoles)){?>
                    <input type="submit" class="btn btn-orange addBm btnSumit" value="保存并添加用户" data-type="2" />
                <?php };?>
            <?php endif; ?>
        </div>
</div>
<section id="main-content">
    <div class="panel-body" style="position: relative;">
    	<p class="top-block-hint">
            <i class="red-hint-i"></i>
            <span> 注：已禁用的项目将置灰不可选且将不可使用</span>
        </p>
        <div>
            <?php if($srcPage == 1): ?>
        	 <!-- 步骤样式 -->
			<div class="information-list" style="height: 85px;margin-top: 50px;">
				<ul class="add-step-ul">
					<li>
						<span  style="background: #ccc;">1</span>
						<em>添加机构</em>
					</li>
	                <li>
	                    <span>2<p class="sp-xian"></p></span>
	                    <em>设置服务配置</em>
	                </li>
				</ul>
			</div>
            <?php endif; ?>
			<div class="information-list"  style="height: auto">
				<table>
					<tbody>
						<tr>
							<th width="116px">品牌</th>
							<td>
								<div class="td-chlid-box">
                                    <?php if($brandData) : ?>
                                        <?php foreach ($brandData as $val): ?>
                                        <span>
                                            <input type="checkbox" value="<?php echo $val['id'];?>" <?php if($val['status']==2):?>disabled="disabled"<?php endif;?> <?php if($val['checked'] == 1): ?>checked="checked" <?php endif;?>  name="brand" id="brand<?php echo $val['id'];?>"  <?php if($val['status']==2):?>class="ckb-disabled"<?php endif;?> onclick="changeCategory(this)">
                                                    <label for="brand<?php echo $val['id'];?>"><?php echo $val['name'];?><i class="gou-i"></i></label>
                                        </span>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        上级还未配置品牌，请配置后再次尝试
<!--                                        <a href="/brand/index" target="_blank">去添加品牌</a>-->
                                    <?php endif; ?>
								</div>
							</td>
						</tr>
						<tr>
							<th>收费项目</th>
							<td>
								<div class="td-chlid-box">
                                    <?php if($itemData) : ?>
                                        <?php foreach ($itemData as $val): ?>
                                        <span>
                                            <input type="checkbox" value="<?php echo $val['id'];?>" <?php if($val['status']==2):?>disabled="disabled"<?php endif;?> <?php if($val['checked'] == 1): ?>checked="checked" <?php endif;?> name="paySeervice" id="item<?php echo $val['id'];?>" class="pr1 <?php if($val['status']==2):?>ckb-disabled<?php endif;?>" onclick="changeCategory(this)">
                                                    <label for="item<?php echo $val['id'];?>"><?php echo $val['name'];?><i class="gou-i"></i></label>
                                        </span>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        上级还未配置收费项目，请配置后再次尝试
<!--                                        <a href="/cost-item/index" target="_blank">去添加收费项目</a>-->
                                    <?php endif; ?>
								</div>
							</td>
						</tr>
						<tr>
							<th>服务类型</th>
							<td>
								<div class="td-chlid-box">
                                    <?php if($typeData) : ?>
                                        <?php foreach ($typeData as $val): ?>
                                            <span>
                                            <input type="checkbox" value="<?php echo $val['id'];?>" <?php if($val['status']==2):?>disabled="disabled"<?php endif;?> <?php if($val['checked'] == 1): ?>checked="checked" <?php endif;?> name="type" id="type<?php echo $val['id'];?>" class="pr1 <?php if($val['status']==2):?>ckb-disabled<?php endif;?>" onclick="changeCategory(this)">
                                                    <label for="type<?php echo $val['id'];?>"><?php echo $val['name'];?><i class="gou-i"></i></label>
                                        </span>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        上级还未配置服务类型，请配置后再次尝试
<!--                                        <a href="/type/index" target="_blank">去添加服务类型</a>-->
                                    <?php endif; ?>
								</div>
							</td>
						</tr>
						<tr>
							<th>产品类目</th>
							<td>
								<div class="tb-layerbox">
                                    <?php if($classData) : ?>
									<ul style="margin-left: 0;">
                                        <?php foreach ($classData as $val): ?>
										<li>
			                                <span>
                                                <?php if($val['sub'] == 1): ?>
			                                    <i class="icon2-shixinyou spanNav" data-id="<?php echo $val['id'];?>"></i>
                                                <?php endif; ?>
			                                    <input type="checkbox" value="<?php echo $val['id'];?>" <?php if($val['status']==2):?>disabled="disabled"<?php endif;?> name="class_id" id="class<?php echo $val['id'];?>" <?php if($val['checked'] == 1): ?>checked="checked" <?php endif;?> class="pr1 <?php if($val['status']==2):?>ckb-disabled<?php endif;?>">
			                                    <label for="class<?php echo $val['id'];?>"><?php echo $val['name'];?><i class="gou-i"></i></label>
			                                </span>
			                                <ul>
											</ul>
			                            </li>
                                        <?php endforeach; ?>
									</ul>
                                    <?php else : ?>
                                        上级还未配置产品类目，请配置后再次尝试
<!--                                        <a href="/category/index" target="_blank">去添加产品类目</a>-->
                                    <?php endif; ?>
								</div>
							</td>
						</tr>
						<tr>
							<th>收费标准</th>
							<td>
								<div class="td-chlid-box">
                                    <?php if($levelData) : ?>
                                        <?php foreach ($levelData as $val): ?>
                                            <span>
                                            <input type="checkbox" value="<?php echo $val['id'];?>" <?php if($val['status']==2):?>disabled="disabled"<?php endif;?> <?php if($val['checked'] == 1): ?>checked="checked" <?php endif;?> name="level" id="level<?php echo $val['id'];?>" class="pr1 <?php if($val['status']==2):?>ckb-disabled<?php endif;?>" onclick="changeCategory(this)">
                                                    <label for="level<?php echo $val['id'];?>"><?php echo $val['name'];?><i class="gou-i"></i></label>
                                        </span>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        上级还未配置收费标准，请配置后再次尝试
<!--                                        <a href="/cost-level/index" target="_blank">去添加收费标准</a>-->
                                    <?php endif; ?>
								</div>
							</td>
						</tr>
						<tr>
							<th>服务区域</th>
							<td>
								<div class="region-box">
                                    <p class="region-box-title">选择服务省份</p>
                                    <ul class="region-box-ul" id="province" style="height: 249px">
                                    </ul>
                                </div>
                                <div class="region-box">
                                    <p class="region-box-title">选择服务市</p>
                                    <span class="whole-region-check">
                                        <input type="checkbox" id="wholeCity" name="" class="checkAll" data-type="1">
                                        <label class="brand-label" for="wholeCity"><i class="gou-i"></i>全部</label>
                                    </span>
                                    <ul class="region-box-ul" id="city">
                                    </ul>
                                </div>
                                <div class="region-box">
                                    <p class="region-box-title">选择服务区县</p>
                                    <span class="whole-region-check">
                                        <input type="checkbox" id="wholeArea" name="" class="checkAll" data-type="2">
                                        <label class="brand-label" for="wholeArea"><i class="gou-i"></i>全部</label>
                                    </span> 
                                    <ul class="region-box-ul" id="district">
                                    </ul>
                                </div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
        </div>
        <!-- <p class="xian" style="margin-top: 30px"></p> -->
        
    </div>
</section>
<script>
	function changeCategory(el) {
		var _that = $(el).parent('span');
		console.log(_that.next('ul').size());
		if (_that.next('ul').size()==0) {
			
		}else{
			$(el).is(':checked')?_that.next('ul').find('input[type="checkbox"]').prop('checked',true):_that.next('ul').find('input[type="checkbox"]').prop('checked',false);
		}
		
	}
	function clickCategory(el){
		var _that = $(el).parent('span');
	 	if (_that.next('ul').size()==0) {
	 		//ajax请求
	 	}else{
	 		//
	 		if(_that.next('ul').is(':hidden'))
	 			{
	 				$(el).removeClass('icon2-shixinyou');
                	$(el).addClass('icon2-shixinxia');
	 				_that.next('ul').show()
	 			}else{
	 				$(el).removeClass('icon2-shixinxia');
	 				$(el).addClass('icon2-shixinyou');
	 				_that.next('ul').hide();
	 			}
	 	}
	 }

	 var oldRegion = [];
	 var oldRegionStr = '<?php echo implode(',',$areaData);?>';
	 if(oldRegionStr != ''){
	    oldRegion = oldRegionStr.split(',');
     }

	 //初始化省份
    var provinceInitFlag = true;
    getRegion(1,'province');
    function getRegion(pid,el) {
        $.getJSON('/organization/get-region', {pid:pid,department_id:<?php echo $departmentId;?>} , function (json) {
            if(json.success == true){
                var data = json.data;
                var html = '';
                for(item in data){
                    var v = $('#'+data[item].parent_id).data('val');
                    if($('#'+data[item].parent_id).length > 0 && v!=1) {
                        v = v+'_'+data[item].parent_id;
                    }
                    else {
                        v = data[item].parent_id;
                    }

                    html+='<li class="li'+data[item].parent_id+'">\n' +
                        '       <input type="checkbox" '+(in_array(data[item].region_id,oldRegion)?'checked="checked"':'')+' id="'+data[item].region_id+'" data-val="'+v+'" name="'+el+'[]" value="'+data[item].region_id+'">\n' +
                        '       <label class="brand-label" for="'+data[item].region_id+'"><i class="gou-i"></i>'+data[item].region_name+'</label>\n' +
                        '  </li>';
                }
                $('#'+el).append(html);

                if(el == 'province'){

                    if(html == '' && provinceInitFlag == true){
                        $('#wholeCity').parent().hide();
                        $('#wholeArea').parent().hide();
                        provinceInitFlag = false;

                        var phtml='<li style="text-align:center;margin-top:50px;">\n' +
                                '上级还未配置服务区域，请配置后再次尝试'+
                            // '      <a href="/area/index" target="_blank">去设置服务区域</a> ' +
                            '  </li>';
                        $('#'+el).html(phtml);
                    }

                    var i = 0;
                    $('#'+el).find('input[type="checkbox"]').each(function () {
                        if($(this).prop('checked') == true){
                            var _self = this;
                            setTimeout(function () {
                                 $(_self).change();
                            },(i*150));
                            i++;

                            $('#district').html('');
                        }
                    });
                }
                else if(el == 'city'){
                    setTimeout(function(){
                        var id = $(html).find('input[type="checkbox"]').attr('id');
                        $('#'+id).change();
                    },100);
                }
                else {
                    $('#district').find('input:last').change();
                }
            }
        });
    }

    //省份改变事件
    $('body').delegate('input[name="province[]"]','change',{},function () {
        var id = $(this).val();
        if($(this).prop('checked') == true){
            getRegion(id,'city');
        }
        else {
            $('.li'+id).each(function () {
                $('.li'+$(this).find('input').val()).remove();
            });
            $('.li'+id).remove();
        }
    });
    //城市改变事件
    $('body').delegate('input[name="city[]"]','change',{},function () {
        var id = $(this).val();
        if($(this).prop('checked') == true){
            getRegion(id,'district');
        }
        else {
            $('.li'+id).remove();
        }
    });
    //全选
    $('.checkAll').change(function () {
        $(this).parent().next('ul').find('input').prop('checked',$(this).prop('checked'));
        if($(this).data('type') == 1){
            if($(this).prop('checked') == false){
                $('#district').html('');
            }
            else {
                var cityIds = [];
                $(this).parent().next('ul').find('input').each(function(){
                    if($(this).prop('checked') == true){
                        cityIds.push($(this).val());
                    }
                });
                getRegion(cityIds,'district');
            }
        }
    });

    //地市全选
    $('body').delegate('#city input','change',{},function () {
         var flag = true;
         $('#city input').each(function(){
             if($(this).prop('checked') == false){
                 flag = false;
             }
         });
        $('#city').prev('span').find('input').prop('checked',flag);
    });
    //区县的全选
    $('body').delegate('#district input','change',{},function () {
        var flag = true;
        $('#district input').each(function(){
            if($(this).prop('checked') == false){
                flag = false;
            }
        });
        $('#district').prev('span').find('input').prop('checked',flag);
    });

    //分类点击
    $('body').delegate('.spanNav', 'click',{},function () {
        var departmentId = '<?php echo $departmentId;?>';
        var _self = $(this).parent('span');
        if($.trim(_self.next('ul').html()) == '' || departmentFlag == true)
        {
            departmentFlag = false;
            $.getJSON('/organization/ajax-get-class', {class_id:$(this).data('id'),id:departmentId}, function (json) {
                if(json.success == true){
                    var data = json.data;

                    var html= '';
                    for(item in data){
                        html+='<li>\n' +
                            '  <span>\n' ;
                            if(data[item].sub == 1){
                                html+='    <i class="icon2-shixinyou spanNav" data-id="'+data[item].id+'"></i>\n';
                            }
                        html+='    <input type="checkbox" value="'+data[item].id+'" '+(data[item].status==2?'disabled="disabled"':'')+' name="class_id" id="class'+data[item].id+'" '+(data[item].checked==1?'checked="checked"':'')+' class="pr1">\n' +
                            '    <label for="class'+data[item].id+'">'+data[item].name+'<i class="gou-i"></i></label>\n' +
                            '  </span><ul></ul>\n' +
                            '</li>';
                    }

                    _self.next('ul').html(html).show();
                    _self.find('.spanNav').removeClass('icon2-shixinyou');
                    _self.find('.spanNav').addClass('icon2-shixinxia');
                    // _self.find('i').show();
                }
            });
        }
        else {
            _self.next('ul').toggle();

            if(_self.next('ul').is(':hidden')){
                _self.find('.spanNav').removeClass('icon2-shixinxia');
                _self.find('.spanNav').addClass('icon2-shixinyou');
            }
            else {
                _self.find('.spanNav').removeClass('icon2-shixinyou');
                _self.find('.spanNav').addClass('icon2-shixinxia');
            }
        }
    });

    //保存提交
    $('.btnSumit').click(function () {
        var type = $(this).data('type');

        //品牌
        var brandArr = [];
        $('input[name="brand"]').each(function () {
            if($(this).prop('checked') == true){
                brandArr.push($(this).val());
            }
        });

        //收费项目
        var itemArr = [];
        $('input[name="paySeervice"]').each(function () {
            if($(this).prop('checked') == true){
                itemArr.push($(this).val());
            }
        });

        //服务类型
        var typeArr = [];
        $('input[name="type"]').each(function () {
            if($(this).prop('checked') == true){
                typeArr.push($(this).val());
            }
        });

        //产品类目
        var classArr = [];
        $('input[name="class_id"]').each(function () {
            if($(this).prop('checked') == true){
                classArr.push($(this).val());
            }
        });

        //收费标准
        var levelArr = [];
        $('input[name="level"]').each(function () {
            if($(this).prop('checked') == true){
                levelArr.push($(this).val());
            }
        });

        //省份
        var regionArr = [];
        $('input[name="district[]"]').each(function () {
            var parentIdStr = $(this).data('val');
            parentIds = parentIdStr.split('_');
            if($(this).prop('checked') == true && parentIds.length == 2){
                regionArr.push(parentIdStr+'_'+ $(this).val());
            }
        });

        if(brandArr.length == 0){
            alert('请选择品牌');
            return false;
        }
        if(itemArr.length == 0){
            alert('请选择收费项目');
            return false;
        }
        if(typeArr.length == 0){
            alert('请选择服务类型');
            return false;
        }
        if(classArr.length == 0){
            alert('请选择产品类目');
            return false;
        }
        if(levelArr.length == 0){
            alert('请选择收费标准');
            return false;
        }
        if(regionArr.length == 0){
            alert('请选择服务区域');
            return false;
        }

        var srcPage = $('#srcPage').val();
        var departmentId = '<?php echo $departmentId;?>';
        var data = {
            'brand': brandArr,
            'class': classArr,
            'type': typeArr,
            'area': regionArr,
            'cost_item': itemArr,
            'cost_level': levelArr,
            'department_id': departmentId
        };
        $.post('/organization/service-config?id='+departmentId,data,function (json) {
            if(json.success == true){
                if(srcPage == 1) {
                    if (type == 2) {
                        alert('保存成功', function () {
                            document.location.href = '/organization/add-user?id=' + departmentId;
                        });
                    }
                    else {
                        alert('保存成功', function () {
                            document.location.href = '/organization/index';
                        });
                    }
                }
                else {
                    alert('保存成功',function () {
                        document.location.href = 'service-config-view?id='+departmentId;

                    });
                }
            }
        },'json');


    });

    $('body').delegate('input[name="class_id"]','change',[],function () {
        if($(this).prop('checked') == true){
            var obj = $(this).parent().parent().parent().prev('span').find('input');
            obj.prop('checked',true);
            obj.change();
        }
        else
        {
            var flag = true;
            $(this).parent().next('ul').find('input').each(function () {
                if($(this).prop('checked') == true){
                    flag = false;
                    return false;
                }
            });
            if(flag == false){
                $(this).prop('checked',true);
            }
        }
    });
</script>
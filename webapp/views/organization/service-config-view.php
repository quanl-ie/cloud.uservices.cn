<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
$this->title = '设置服务配置';
$this->params['breadcrumbs'][] = ['url'=>'/organization/index','label'=>'组织架构'];
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<style>
    ul{margin-left: 20px;}
    li>ul{display: block;}
    .region-box-ul{margin-left: 0;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">设置服务配置</span>
    <div class="right-btn-box">
        <?php if(in_array('/organization/service-config',$selfRoles)){?>
            <button class="btn btn-orange mgr20" onclick="location.href='/organization/service-config?src_page=2&id=<?php echo $departmentId;?>'">编辑</button>
        <?php };?>
        <button class="btn btn-orange" onclick="location.href='/organization/index'">返回</button>
    </div>
</div>
<section id="main-content">
    <div class="panel-body" style="position: relative;">

		<div class="information-list"  style="height: auto;margin-top: 20px;">
			<table>
				<tbody>
					<tr>
						<th width="116px">品牌</th>
						<td>
							<div class="td-chlid-box">
								
                                      <?php echo $brandData;?>
		                        
							</div>
						</td>
					</tr>
					<tr>
						<th>收费项目</th>
						<td>
							<div class="td-chlid-box">
								
		                           <?php echo $itemData;?>
		                        
							</div>
						</td>
					</tr>
					<tr>
						<th>服务类型</th>
						<td>
							<div class="td-chlid-box">
								
		                           <?php echo $typeData;?>
		                        
							</div>
						</td>
					</tr>
					<tr>
						<th>产品类目</th>
						<td>
							<div class="td-chlid-box">
								
		                           <?php echo $classData;?>
		                        
							</div>
						</td>
					</tr>
					<tr>
						<th>收费标准</th>
						<td>
							<div class="td-chlid-box">
								
		                           <?php echo $levelData;?>
		                        
							</div>
						</td>
					</tr>
					<tr>
						<th>服务区域</th>
						<td>
							<div class="region-box">
                                <p class="region-box-title">选择服务省份</p>
                                <ul class="region-box-ul" id="provinceUl">
                                    <?php foreach ($areaData['province'] as $val): ?>
                                	<li>
                                        <?php echo $val;?>
                                    </li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                            <div class="region-box">
                                <p class="region-box-title">选择服务市</p>
                               
                                <ul class="region-box-ul" id="provinceUl">
                                    <?php foreach ($areaData['city'] as $val): ?>
                                        <li>
                                            <?php echo $val;?>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                            <div class="region-box">
                                <p class="region-box-title">选择服务区县</p>
                                <ul class="region-box-ul" id="provinceUl">
                                    <?php foreach ($areaData['district'] as $val): ?>
                                        <li>
                                            <?php echo $val;?>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
       
    </div>
</section>
<script>
	 
</script>
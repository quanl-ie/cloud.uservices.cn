<table class="table table-bordered table-striped table-hover" id="enableTable">
    <thead bgcolor="#2693ff">
    <tr>
        <th>名称</th>
        <th width="50px">机构编号</th>
        <th width="120px">上级机构</th>
        <th width="120px">机构类型</th>
        <th width="60px">联系人</th>
        <th width="200px">地址</th>
        <th width="100px">电话</th>
        <th width="100px">邮箱</th>
        <th width="50px">备注</th>
        <th width="100px">创建时间</th>
        <th width="130px">操作</th>
    </tr>
    </thead>
    <tbody>
     <?php if($data): ?>
      <?php foreach ($data as $val): ?>
        <tr>
            <td class="text-left">
                <div>
                    <?php if($val['sub'] == 1 && $type != 3): ?>
                    <a class="toggleOpenClose" data-id="<?php echo $val['id'];?>" href="javascript:void(0);"><i class="icon2-shixinyou"></i></a>
                    <?php endif;?>
                    <?php echo $val['name'];?>
                </div>
            </td>
            <td><?php echo $val['weight'];?></td>
            <td><?php echo $val['parent_name'];?></td>
            <td><?php echo $val['type_desc'];?></td>
            <td><?php echo $val['contacts'];?></td>
            <td><?php echo $val['address_desc'];?></td>
            <td><?php echo $val['phone'];?></td>
            <td><?php echo $val['email'];?></td>
            <td>
                <?php if($val['remark'] == ''):?>
                暂无
                <?php else:?>
                <a href="javascript:" class="viewRemark" data-id="<?php echo $val['id'];?>">点击查看</a>
                <?php endif;?>
            </td>
            <td><?php echo $val['create_time'];?></td>
            <td>
                <?php if($val['status'] == 1): ?>
                    <?php if(in_array('/organization/edit-department',$selfRoles)){?>
                        <a href="/organization/edit-department?id=<?php echo $val['id'];?>">修改</a>
                    <?php };?>
                    <?php if(in_array('/organization/add-department',$selfRoles)){?>
                        <a href="/organization/add-department?id=<?php echo $val['id'];?>">添加下级</a>
                    <?php };?>
                     <?php if($selfDepartmentId != $val['id'] && $directCompanyId!=$val['id']): ?>
                        <?php if(in_array('/organization/disable',$selfRoles)){?>
                            <a href="javascript:void(0);" data-type="<?php echo $val['type'];?>" data-url="/organization/disable?id=<?php echo $val['id'];?>" data-pid="<?php echo $val['parent_id'];?>" data-id="<?php echo $val['id'];?>" class="disable-a btnDisable">禁用</a>
                        <?php };?>
                    <?php endif; ?>
                    <?php if($val['type'] == 1 && $selfDepartmentId != $val['id'] && $directCompanyId!=$val['id']): ?>
                        <?php if(in_array('/organization/service-config-view',$selfRoles)){?>
                            <a href="/organization/service-config-view?id=<?php echo $val['id'];?>" v-if="item.configure==true">设置服务配置</a>
                        <?php };?>
                    <?php endif; ?>
                   
                <?php elseif($val['status'] == 2): ?>
                    <?php if(in_array('/organization/enable',$selfRoles)){?>
                        <a href="javascript:void(0);" data-type="<?php echo $val['type'];?>" data-url="/organization/enable?id=<?php echo $val['id'];?>" data-pid="<?php echo $val['parent_id'];?>" class="disable-a btnEnable">启用</a>
                    <?php };?>
                    <?php if(in_array('/organization/del',$selfRoles)){?>
                        <a href="javascript:void(0);" data-type="<?php echo $val['type'];?>" data-url="/organization/del?id=<?php echo $val['id'];?>" data-id="<?php echo $val['id'];?>" class="disable-a btnDel">删除</a>
                    <?php };?>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach;?>
    <?php else : ?>
    <tr>
        <td colspan="11">未找到相关机构</td>
    </tr>
    <?php endif; ?>
    </tbody>
</table>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
$this->title = '修改机构';
$this->params['breadcrumbs'][] = ['url'=>'/organization/index','label'=>'组织架构'];
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<style>
    ul{margin-left: 20px;}
    .linkage-justify-list{width: 30%;}
    .Validform_loading{padding-left: 0;color: red;}
</style>
<div class="jn-title-box no-margin-bottom">
     <span class="jn-title">修改机构</span> 
</div>
<section id="main-content">
    <div class="panel-body" style="position: relative;">
    	<?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal',
                'id' => 'form2',
                'name' => 'form1',
            ],
        ]);
        ?>
		<div class="information-list">
            <label class="name-title-lable">
               <i class="red-i">*</i> 名称：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list hgt-ov58">
                <input type="text" name="Department[name]" ajaxurl="/organization/ajax-check-department?&id=<?php echo $model->id;?>&pid=<?php echo $model->parent_id;?>" value="<?php echo $model->name;?>" class="form-control" maxlength="30" datatype="*" sucmsg=" " nullmsg="请输入名称"  placeholder="不超过30个字" >
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
               <i class="red-i">*</i> 机构编号：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list hgt-ov58">
                <input type="text" name="Department[weight]" value="<?php echo $model->weight;?>" class="form-control" maxlength="5" datatype="n1-5" sucmsg=" " nullmsg="请输入机构编号" placeholder="仅限数字，用于排序，至多5位" >
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
               <i class="red-i">*</i>上级机构：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list  hgt-ov58">
            	<p class="height34"><?php echo $parentDepartment?$parentDepartment:'无';?></p>
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
               <i class="red-i">*</i>机构类型：
            </label>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list  hgt-ov58">
                <?php if($srcPage == 1):?>
                <p class="height34"><?php echo \webapp\models\Department::getTypeDesc($model->type); ?></p>
                <input type="hidden" name="Department[type]" value="<?php echo $model->type;?>"/>
                <?php else: ?>
                <select name="Department[type]" id="departmentType" class="form-control" datatype="*" sucmsg=" " nullmsg="请选择机构类型">
                    <option value="">请选择机构类型</option>
                    <?php foreach (\webapp\models\Department::getTypeByParentType($parentDepartmentType,$tmpDepartmentId) as $key=>$val): ?>
                        <option value="<?php echo $key; ?>" <?php if($model->type==$key):?>selected="selected"<?php endif;?>><?php echo $val; ?></option>
                    <?php endforeach; ?>
                </select>
                <?php endif; ?>
            </div>


        </div>
        <div class="information-list">
            <label class="name-title-lable">
               联系人：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <input type="text" name="Department[contacts]" value="<?php echo $model->contacts;?>" class="form-control" placeholder="不超过10个字"  maxlength="10">
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
              所在区域：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <div  class="three-linkage" style="height: 40px;width: 100%;">
                	<div class="linkage-justify-list">
                		<select name="Department[province_id]" id="province" class="form-control">
                			<option value="">省</option>
                		</select>
                	</div>
                	<div class="linkage-justify-list">
                		<select name="Department[city_id]" id="city" class="form-control">
                			<option value="">市</option>
                		</select>
                	</div>
                	<div class="linkage-justify-list">
                		<select name="Department[district_id]" id="district" class="form-control">
                			<option value="">区</option>
                		</select>
                	</div>
                	<div class="justify_fix"></div>
                </div>
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
                联系地址：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <input type="text" name="Department[address]" value="<?php echo $model->address; ?>" class="form-control" placeholder="不超过30个字"  maxlength="30">
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
               联系电话：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list  hgt-ov58">
                <input type="text" name="Department[phone]" value="<?php echo $model->phone;?>" class="form-control" datatype="/^\s*$/|/^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$/|/^1\d{10}$/" sucmsg=" " nullmsg="请输入公司联系电话" placeholder="请输入公司联系电话"  maxlength="30">
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
               邮箱：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list  hgt-ov58">
                <input type="text" name="Department[email]" value="<?php echo $model->email;?>" class="form-control" datatype="/^\s*$/|e" sucmsg=" " nullmsg="请正确输入邮箱格式" placeholder="请正确输入邮箱格式"  maxlength="50">
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
               备注：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list"  style="margin-bottom: 30px;">
                <textarea class="ramark-box" id="remarks" maxlength="200" placeholder="至多200字" name="Department[remark]" id="description"  maxlength="200" onkeyup="wordLength(this)"><?php echo $model->remark;?></textarea>
                <p class="xianzhi"><span  id='word'>0</span>/200</p>
            </div>
        </div>
        <div class="information-list">
        	<div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
        		<a class="btn bg-f7 mgr20" href="/organization/index">取消</a>
                <?php if($srcPage == 1): ?>
        		<button class="btn btn-orange">提交</button>
                <input type="hidden" value="提交" name="Department[submitType]" />
                <?php else: ?>
                    <button class="btn btn-orange">下一步</button>
                    <input type="hidden" value="下一步" name="Department[submitType]" />
                <?php endif; ?>
        	</div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</section>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    $("#form2").Validform({
        tiptype:3
    });

    var province = '<?php echo $model->province_id;?>';
    var city = '<?php echo $model->city_id;?>';
    var district = '<?php echo $model->district_id;?>';
    //初始化
    getRegion(1,'省','province');
    function getRegion(pid,prompt,el) {
        $.getJSON('/common/get-region', {pid:pid} , function (json) {
            if(json.success == true){
                var data = json.data;
                var html ='<option value="">'+prompt+'</option>';
                for(item in data)
                {
                    var selected = "";
                    if(data[item].region_id == province || data[item].region_id == city || data[item].region_id == district){
                        selected = "selected = 'selected'";
                    }
                    html+='<option '+selected+' value="'+data[item].region_id+'">'+data[item].region_name+'</option>';
                }
                $('#'+el).html(html);
                setTimeout(function () {
                    $('#'+el).change();
                },100);
            }
            else
            {
                var html ='<option value="">'+prompt+'</option>';
                $('#'+el).html(html);
            }
        });
    }

    //省份改变事件
    $('#province').change(function () {
        getRegion($(this).val(),'市','city');
        $('#district').html('<option value="">区</option>');
    });
    //城市改变事件
    $('#city').change(function () {
        getRegion($(this).val(),'区','district');
    });
    // $('input[name="Department[name]"]').keyup(function() {
    //     console.log($(this).val().length);
    // });
</script>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
$this->title = '添加用户';
$this->params['breadcrumbs'][] = ['url'=>'/organization/index','label'=>'组织架构'];
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<style>
    ul{margin-left: 20px;}
</style>
<div class="jn-title-box no-margin-bottom">
     <span class="jn-title">添加用户</span>
</div>
<section id="main-content">
    <div class="panel-body" style="position: relative;padding-bottom: 150px;">
        <?php
            $form = ActiveForm::begin([
                'options' => [
                    'class' => 'form-horizontal',
                    'id' => 'form2',
                    'name' => 'form1',
                ],
            ]);
        ?>
    	<div class="col-md-12">
            <h5 class="block-h5" style="padding-top: 0">添加用户</h5>
        </div>
    	<div class="information-list">
            <label class="name-title-lable">
               <i class="red-i">*</i> 姓名：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list hgt-ov58">
                <input type="text" name="User[username]" value="" class="form-control" maxlength="10" datatype="*" sucmsg=" " nullmsg="请输入姓名" placeholder="不超过10个字（建议使用真实姓名）" >
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
               <i class="red-i">*</i> 登录手机号：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list hgt-ov58">
                <input type="text" name="User[mobile]" value="" ajaxurl="/organization/ajax-check-user?pid=<?php echo $departmentModel->id;?>" class="form-control" maxlength="11" datatype="m" sucmsg=" " nullmsg="请输正确输入手机号" placeholder="11位数字的手机号（登录时使用）" >
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
               <i class="red-i">*</i> 初始密码：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list hgt-ov58">
                <input type="text" name="User[password]" value="" class="form-control" maxlength="50" datatype="*6-16" sucmsg=" " nullmsg="请输入密码" placeholder="6-16位数字/大小写字母/下划线" >
            </div>
        </div>
		<div class="information-list">
            <label class="name-title-lable">
               所属机构：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list hgt-ov58">
                <p class="height34"><?php echo $departmentModel->name;?></p>
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
               职位：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list hgt-ov58">
                <input type="text" name="User[job_title]" value="" class="form-control" maxlength="50" placeholder="请输入该员工的职位" >
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
                邮箱：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list hgt-ov58">
                <input type="text" name="User[email]" value="" class="form-control" maxlength="50" datatype="/^\s*$/|e" sucmsg=" " nullmsg="请正确输入邮箱格式" placeholder="请正确输入邮箱格式" >
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
               <i class="red-i">*</i> 角色：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list divRole">
                <input type="hidden" name="User[role_ids]" value="" class="form-control" id="role_ids">
                <input type="text" value="" class="form-control" datatype="*" sucmsg=" " nullmsg="请选择角色" placeholder="请选择角色" readonly id="showRoleIds" >
                <i class="icon iconfont icon-shouqi1"></i>
                <p class="misplacement" style="bottom: -5px;" id="work_type_msg">请选择角色</p>
                <div class="drop-dw-rolebox" style="top: 35px;">
                    <?php if($departmentModel->type == 1): ?>
                        <ul class="many-role-ul">
                            <?php foreach ($roleData as $val):?>
                                <li>
                                    <input type="radio" name="role" value="<?php echo $val['id'];?>" id="<?php echo $val['id'];?>">
                                    <label for="<?php echo $val['id'];?>"><?php echo $val['name'];?></label>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    <?php else : ?>
                        <ul class="many-role-ul">
                            <?php foreach ($roleData as $val):?>
                           <li>
                               <input type="checkbox" name="role" value="<?php echo $val['id'];?>" id="<?php echo $val['id'];?>">
                               <label for="<?php echo $val['id'];?>"><?php echo $val['name'];?><i class="gou-i"></i></label>
                           </li>
                            <?php endforeach;?>
                       </ul>
                    <?php endif; ?>
                    <div  class="btn-li">
                        <a href="javascript:" class="bg-f7"  id="cancelRole">取消</a>
                        <a href="javascript:"  class="btn-orange" id="okRole">确定</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="information-list">
        	<div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list hgt-ov58">
        		<a href="/organization/index"  class="btn bg-f7">取消</a>
        		<button class="btn btn-orange">确认</button>
        	</div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</section>
<script src="/js/Validform_v5.3.2/Validform_v5.3.2.js?v=333"></script>
<script>

    $("#form2").Validform({
        tiptype:3
    });
	$('#role').on('click',function() {
		$('.drop-dw-rolebox').is(':hidden')?$('.drop-dw-rolebox').show():$('.drop-dw-rolebox').hide();
        
    })
    // 点击展示角色
    $('.divRole').on('click',function () {
        if ($('.drop-dw-rolebox').is(':hidden')) {
            $('.drop-dw-rolebox').show();
        }
    });

    //取消选择角色
    $('#cancelRole').on('click',function(event) {
        event.stopPropagation();
        $('.drop-dw-rolebox').hide();
    })
    
    //确定选择角色
    $('#okRole').on('click',function(event) {
        event.stopPropagation();

        var roleIds = new Array();
        var roleDesc= new Array();
        $("input[name='role']").each(function() {
            if ($(this).is(':checked')) {
                roleIds.push($(this).val());
                roleDesc.push($(this).next('label').text());
            }
        });

        if (roleIds.length>0) {
            $('.drop-dw-rolebox').hide();
            $('#showRoleIds').val(roleDesc.join('；'));
            $('#role_ids').val(roleIds.toString());
            $('#showRoleIds').focus();
            $('#showRoleIds').blur();
        }else{
            alert('请选择角色');
        }
        
    })
</script>
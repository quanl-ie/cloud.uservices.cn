<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '首页';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">

<style type="text/css">
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
    body{ background: #f1f2f7;}
    .panel-body{ padding: 0;}
    .real-time-a{width: 100%;}
    a>p{color: #666}a>span{color: #999}
    a>p:hover{color: #2693ff}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">首页</span>


    <a href="/account/add" class="add">
    </a>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div>
                <div class="panel-body">
                    <div  class="col-md-8">
                        <div class="home-left-top">
                            <?php if(in_array('/index/real-time',$selfRoles)){?>
                                <h4 class="home-left-title">实时指标</h4>
                                <?php if(in_array('/order/wait-audit',$selfRoles)){?>
                                    <div class="col-md-3 col-xs-3 col-sm-3">
                                        <a href="/order/wait-audit" class="real-time-a">
                                            <p class="real-time-num" style="color: #002aff"><?=isset($orderData['waitAudit']) ? $orderData['waitAudit'] : 0?></p>
                                            <span  class="real-time-name">待审核</span>
                                        </a>
                                    </div>
                                <?php }else{?>
                                    <div class="col-md-3 col-xs-3 col-sm-3">
                                        <a href="#" class="real-time-a">
                                            <p class="real-time-num" style="color: #002aff"><?=isset($orderData['waitAudit']) ? $orderData['waitAudit'] : 0?></p>
                                            <span  class="real-time-name">待审核</span>
                                        </a>
                                    </div>
                                <?php }?>
                                <?php if(in_array('/order/wait-assign',$selfRoles)){?>
                                <div class="col-md-3 col-xs-3 col-sm-3">
                                    <a href="/order/wait-assign" class="real-time-a">
                                        <p class="real-time-num" style="color: #ff5c5e"><?=isset($orderData['waitAssign']) ? $orderData['waitAssign'] : 0?></p>
                                        <span  class="real-time-name">待指派</span>
                                    </a>
                                </div>
                                <?php }else{?>
                                    <div class="col-md-3 col-xs-3 col-sm-3">
                                        <a href="#" class="real-time-a">
                                            <p class="real-time-num" style="color: #ff5c5e"><?=isset($orderData['waitAssign']) ? $orderData['waitAssign'] : 0?></p>
                                            <span  class="real-time-name">待指派</span>
                                        </a>
                                    </div>
                                <?php }?>
                                <?php if(in_array('/order/wait-service',$selfRoles)){?>
                                <div class="col-md-3 col-xs-3 col-sm-3">
                                    <a href="/order/wait-service" class="real-time-a">
                                        <p class="real-time-num" style="color: #ff974e"><?=isset($orderData['waitService']) ? $orderData['waitService'] : 0?></p>
                                        <span  class="real-time-name">待服务</span>
                                    </a>
                                </div>
                                <?php }else{?>
                                    <div class="col-md-3 col-xs-3 col-sm-3">
                                        <a href="#" class="real-time-a">
                                            <p class="real-time-num" style="color: #ff974e"><?=isset($orderData['waitService']) ? $orderData['waitService'] : 0?></p>
                                            <span  class="real-time-name">待服务</span>
                                        </a>
                                    </div>
                                <?php }?>
                                <?php if(in_array('/order/in-service',$selfRoles)){?>
                                <div class="col-md-3 col-xs-3 col-sm-3">
                                    <a href="/order/in-service" class="real-time-a">
                                        <p class="real-time-num" style="color: #01b485"><?=isset($orderData['inService']) ? $orderData['inService'] : 0?></p>
                                        <span  class="real-time-name">服务中</span>
                                    </a>
                                </div>
                                <?php }else{?>
                                    <div class="col-md-3 col-xs-3 col-sm-3">
                                        <a href="#" class="real-time-a">
                                            <p class="real-time-num" style="color: #01b485"><?=isset($orderData['inService']) ? $orderData['inService'] : 0?></p>
                                            <span  class="real-time-name">服务中</span>
                                        </a>
                                    </div>
                                <?php }?>
                                <?php if(in_array('/order/wait-confirm',$selfRoles)){?>
                                <div class="col-md-3 col-xs-3 col-sm-3">
                                    <a href="/order/wait-confirm" class="real-time-a">
                                        <p class="real-time-num" style="color: #01b485"><?=isset($orderData['waitConfirm']) ? $orderData['waitConfirm'] : 0?></p>
                                        <span  class="real-time-name">待验收</span>
                                    </a>
                                </div>
                                <?php }else{?>
                                    <div class="col-md-3 col-xs-3 col-sm-3">
                                        <a href="#" class="real-time-a">
                                            <p class="real-time-num" style="color: #01b485"><?=isset($orderData['waitConfirm']) ? $orderData['waitConfirm'] : 0?></p>
                                            <span  class="real-time-name">待验收</span>
                                        </a>
                                    </div>
                                <?php }?>
                                <?php if(in_array('/order/finish',$selfRoles)){?>
                                <div class="col-md-3 col-xs-3 col-sm-3">
                                <a href="/order/finish" class="real-time-a">
                                    <p class="real-time-num" style="color: #ff974e"><?=isset($orderData['finish']) ? $orderData['finish'] : 0?></p>
                                    <span  class="real-time-name">已完成</span>
                                </a>
                                </div>
                                <?php }else{?>
                                    <div class="col-md-3 col-xs-3 col-sm-3">
                                        <a href="#" class="real-time-a">
                                            <p class="real-time-num" style="color: #ff974e"><?=isset($orderData['finish']) ? $orderData['finish'] : 0?></p>
                                            <span  class="real-time-name">已完成</span>
                                        </a>
                                    </div>
                                <?php }?>
                                <?php if(in_array('/order/service-providers-reject',$selfRoles)){?>
                                <div class="col-md-3 col-xs-3 col-sm-3">
                                    <a href="/order/service-providers-reject" class="real-time-a">
                                        <p class="real-time-num" style="color: #ff5c5e"><?=isset($orderData['serviceProvidersReject']) ? $orderData['serviceProvidersReject'] : 0?></p>
                                        <span  class="real-time-name">被驳回</span>
                                    </a>
                                </div>
                                <?php }else{?>
                                    <div class="col-md-3 col-xs-3 col-sm-3">
                                        <a href="#" class="real-time-a">
                                            <p class="real-time-num" style="color: #ff5c5e"><?=isset($orderData['serviceProvidersReject']) ? $orderData['serviceProvidersReject'] : 0?></p>
                                            <span  class="real-time-name">被驳回</span>
                                        </a>
                                    </div>
                                <?php }?>
                                <?php if(in_array('/order/service-timeout',$selfRoles)){?>
                                    <div class="col-md-3 col-xs-3 col-sm-3">
                                        <a href="/order/service-timeout" class="real-time-a">
                                            <p class="real-time-num" style="color: #002aff"><?=isset($orderData['serviceTimeout']) ? $orderData['serviceTimeout'] : 0?></p>
                                            <span  class="real-time-name">上门超时</span>
                                        </a>
                                    </div>
                                <?php }else{?>
                                    <div class="col-md-3 col-xs-3 col-sm-3">
                                        <a href="#" class="real-time-a">
                                            <p class="real-time-num" style="color: #002aff"><?=isset($orderData['serviceTimeout']) ? $orderData['serviceTimeout'] : 0?></p>
                                            <span  class="real-time-name">上门超时</span>
                                        </a>
                                    </div>
                                <?php }?>
                            <?php }?>
                        </div>
                        <div class="home-left-bottom">
                            <?php if(in_array('/index/shortcuts',$selfRoles)){?>
                                <h4 class="home-left-title">快捷操作</h4>
                                <?php if(in_array('/order/add',$selfRoles)){?>
                                    <div class="col-md-4 col-xs-4 col-sm-4">
                                        <a href="/order/add" class="home-operation">
                                            <img src="<?php echo Url::to('/images/xinjian.png');?>">
                                            <span style="margin-left: 10px">新建售后订单</span>
                                        </a>
                                    </div>
                                <?php };?>
                            <?php };?>
                        </div>
                    </div>
                    <div  class="col-md-4 left-news" style="margin-top: 10px">
                        <?php if(in_array('/index/real-msg',$selfRoles)){?>
                            <div class="home-news-box">
                                <div class="news-box-tit">
                                    <a href="javascrip:" class="real-time-title">
                                        <h4 style="font-size: .22rem;font-weight: 600;;color: #333">实时消息</h4>
                                    </a>

                                </div>
                                <div class="home-right-bottom">
                                    <ul>
                                        <!-- 循环数据 -->
                                        <?php if(!empty($message)) foreach ($message as $key=>$val){?>
                                            <li class="news-list-li">
                                                <a href="<?php echo isset($val['link_info'])?$val['link_info']:'';?>">
                                                    <p style="font-size: .18rem;margin: 0"><?php echo $val['title'];?></p>
                                                    <span style="font-size: .16rem"><?php echo $val['create_time']?></span>
                                                </a>
                                            </li>
                                        <?php }?>
                                    </ul>
                                </div>
                            </div>
                        <?php }?>

                    </div>
                  
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function(){
        fontSize ();
    })
    $(window).resize(function (){
        fontSize ();
    });
    function fontSize () {
        var whdef = 100/1920;
         var wH = window.innerHeight;
         var wW = window.innerWidth;
         var rem = wW * whdef;
         $('html').css('font-size', rem + "px");
    }
    $("#addParentIframe").click(function () {
        layer.open({
            type: 2,
            title: '添加客户',
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: '/account/add/',
            end:function(){
                setTimeout(function(){location.reload();},2000);
            }
        });
    });
</script>
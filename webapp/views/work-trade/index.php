<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    $this->title = '订单交易记录';
    $this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time();?>">
<style type="text/css">
    .search-box-kuang>.col-sm-6{
        padding-left: 0;
        position: relative;
    }
    .icon-rili{position: absolute;right: 17px;top: 1px;height: 34px;line-height: 34px;background: #fff;width: 24px;text-align: center;}
    .calendar-icon{width: 100%;height: 36px;position: absolute;top:0;left: 0;}
    .popover{max-width: 500px !important;}
    .pagination{
        text-align: center;
    }
    .select-fuji{position: relative;height: 36px;width: 100%;}
    .select-fuji>.form-control{position: absolute;top: 0;left: 0;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">订单交易记录</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/work-trade/index" class="form-horizontal form-border" id="form">
                        <!--搜索开始-->
                        <div class="form-group jn-form-box">
                            <div class="col-sm-12 no-padding-left">
                                <div class="single-search">
                                    <label class="search-box-lable">订单编号</label>
                                    <div class="single-search-kuang1">
                                        <input type="text" class="form-control" name="work_no" value="<?php echo isset($_GET['work_no'])?$_GET['work_no']:'';?>">
                                    </div>
                                </div>
    
                                <div class="single-search">
                                    <label class="search-box-lable">收费方式</label>
                                    <div class="single-search-kuang1">
                                        <select class="form-control" name="trade_pay_code">
                                            <option value="-1">请选择</option>
                                            <option value="0" <?php if(isset($_GET['trade_pay_code']) && $_GET['trade_pay_code']==0):?>selected<?php endif;?>>微信</option>
                                            <option value="1" <?php if(isset($_GET['trade_pay_code']) && $_GET['trade_pay_code']==1):?>selected<?php endif;?>>支付宝</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="tow-search">
                                    <label class="search-box-lable">收费时间</label>
                                    <div class="single-search-kuang2">
                                        <div  class="half-search">
                                            <input type="text" id="datetimepicker-create-top" class="form-control" name="start_time" value="<?php echo isset($_GET['start_time'])?$_GET['start_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-create-end\')}'})">
                                            <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-create-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-create-end\')}'})"><i class="icon2-rili"></i></span>
                                            <span class="zhi"> 至</span>
                                        </div>
                                        <div  class="half-search">
                                            <input type="text" id="datetimepicker-create-end" class="form-control" name="end_time" value="<?php echo isset($_GET['end_time'])?$_GET['end_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-create-top\')}'})" >
                                            <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-create-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-create-top\')}'})"><i class="icon2-rili"></i></span>
                                        </div>
                                        <p class="city3_fix"></p>
                                    </div>
                                </div>
                                
                                <div class="search-confirm">
                                    <button class="btn btn-success "><i class=" "></i> 搜索</button>
                                </div>
                            </div>
                        </div>
                        <!--搜索结束-->
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead bgcolor="#2693ff">
                                <tr>
                                    <th>订单编号</th>
                                    <th>收费方式</th>
                                    <th>收费金额</th>
                                    <th>收费时间</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($data) && !empty($data)):?>
                                    <?php foreach ($data as $key=>$val):?>
                                        <tr>
                                            <td>
                                                <?php if(in_array('/order/view',$selfRoles)): ?>
                                                <a href="/order/view?order_no=<?=isset($val['order_no']) ? $val['order_no'] : '';?>"><?=isset($val['order_no']) ? $val['order_no'] : '';?></a>
                                                <?php else : ?>
                                                    <?=isset($val['order_no']) ? $val['order_no'] : '';?>
                                                <?php endif; ?>
                                            </td>
                                            <td><?=isset($val['pay_name']) ? $val['pay_name'] : '';?></td>
                                            <td><?=isset($val['pay_amount']) ? $val['pay_amount'] : '';?></td>
                                            <td><?=isset($val['pay_time']) ? $val['pay_time'] : '';?></td>
                             
                                        </tr>
                                    <?php endforeach;?>
                                <?php else:?>
                                    <tr>
                                    <tr><td colspan="11" class="no-record">暂无数据</td></tr>
                                    </tr>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo $pageHtml;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

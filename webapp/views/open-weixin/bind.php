<?php
$this->title = '小程序授权';
$this->params['breadcrumbs'][] = $this->title;

?>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" id="form">
                        <div style="margin:0 auto; text-align:center; min-height:350px; padding:100px 0">
                            <?php if(Yii::$app->session->hasFlash('message')):?>
                                <div style="color:red;font-weight: bold;padding:20px 0px;"><?php echo Yii::$app->session->getFlash('message');?></div>
                            <?php endif;?>
                            <div>您需要绑定并授权微信小程序权限，才能正常使用小程序功能,请“点击授权微信小程序”进行授权操作；</div>
                            <div class="prompt-icon" style="margin:30px 0px;"><img src="/img/bindweixin.png" /></div>
                            <div>
                                <a target="_blank" href="<?php echo $link;?>" class="btn btn-success" style="color:#FFF; "> 点击授权微信小程序 </a><br /><br />
                                <a target="_blank" href="/商家管理系统小程序部署流程 V1.0.0.pdf"> 查看小程序注册及部署流程 </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

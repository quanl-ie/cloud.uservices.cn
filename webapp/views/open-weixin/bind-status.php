<?php
$this->title = '小程序授权';
$this->params['breadcrumbs'][] = $this->title;

?>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" id="form">
                        <div style="margin:0 auto; text-align:center; min-height:350px; padding:100px 0">
                            <?php if($bindMessage): ?>
                                <div>
                                    <?php if($bindMessage['weixin_audit_status'] == 3): ?>
                                    <img src="/img/wtg.png" style="width:50px;" />
                                    <?php elseif($bindMessage['status'] == 0): ?>
                                    <img src="/img/wait.png" style="width:50px;" />
                                    <?php else: ?>
                                        <img src="/img/pass.png" style="width:50px;" />
                                    <?php endif; ?>
                                </div>
                                <div><h3 style="font-size:20px;<?php if($bindMessage['weixin_audit_status'] == 3):?>color:red<?php endif;?>">
                                    <?php echo urldecode($bindMessage['message']);?>
                                </h3></div>

                                <!-- 重新提交 -->
                                <?php if($bindMessage['weixin_audit_status'] == 3):?>
                                <div>
                                    <input type="button" id="reSubmitAudit" class="btn btn-orange mgr20 btnSubmit" style=" color:#FFF;" value="重新提交审核">
                                </div>
                                <?php endif;?>

                                <?php if($bindMessage['status'] == 0): ?>
                                <div style="text-align: center;margin-top: 30px;">
                                    <p style="color:red">体验二维码，不能用于运营使用！</p>
                                    <img src="/open-weixin/ty-qr-code" style="border:1px solid #cccccc;width: 200px;" />
                                </div>
                                <?php endif; ?>
                            <?php endif;?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if($bindMessage['weixin_audit_status'] == 3):?>
<script>
    $('#reSubmitAudit').click(function () {
        confirm('微信审核约7个工作日，不可以重复提交,您确认要提交吗？',function(){
            $.getJSON('/open-weixin/re-submit-audit',{},function (json) {
                alert(json.message);
                if(json.status == 1){
                    setTimeout(function () {
                        location.replace('/open-weixin/bind');
                    },3000);
                }
            });
        },this);
    });
</script>
<?php endif;?>

<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '添加联系地址';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>">
<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
body{
    font-family: "微软雅黑";
    background: #fff;
}
  .col-sm-4, .col-md-4, .col-lg-4{
    padding-left: 0;
  }
  input[type=radio]{
    margin-top: 10px;
  }
.linkage-justify-list{
    width: 30%;
}
.three-linkage{
    width: 100%;
}
.information-list{padding: 0 40px;}
.name-title-lable{left:40px;}
</style>
<body>
    <form action="/account-address/add" method="post" id="account-address-add">
        <input type="hidden" value="<?= $id ?>" id="account_id" name="account_id">
        <!-- <ul> -->
             <div class="information-list">
                <label class="name-title-lable" ><i class="red-i">*</i> 联系人姓名：</label>
                <div class="addinput-list">
                    <input type="text" value="" name="conact_name" datatype="*" sucmsg=" " nullmsg="请输入联系人姓名"   placeholder="请输入联系人姓名"  max="20"  class="form-control" />
                </div>
                <div class="prompt"></div>
            </div>
             <div class="information-list">
                <label class="name-title-lable" ><i class="red-i">*</i> 联系人电话：</label>
                 <div class="addinput-list">
                    <input type="text" value="" name="mobile_phone" datatype="/^(0[0-9]{2,3}\-*)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$/|/^1\d{10}$/" sucmsg=" " nullmsg="请输入联系人电话"   placeholder="请输入联系人电话" errormsg="手机号格式错误" max="20"  class="form-control" />
                </div>
                <div class="prompt"></div>
            </div>

        <div class="col-md-8 information-list" style="height:auto">
            <label class="name-title-lable">
                <i class="red-i">*</i>所在城市：
            </label>
            <div class="addinput-list">
                <!-- 高德地图地址组合jq组件 开始-->
                <div class="us-amap-wrapper" id="address-map">
                    <div class="us-amap-input-content">
                        <div class="us-amap-input-ssq">
                            <input type="text"
                                   placeholder="请选择省市区"
                                   max="50" class="form-control inputxt address input-pua" value="<?php echo isset($accountAddress->cityLabel)?$accountAddress->cityLabel:'';?>" />
                            <input type="hidden" name="province_id" value="<?php echo isset($accountAddress->province_id)?$accountAddress->province_id:'';?>">
                            <input type="hidden" name="city_id" value="<?php echo isset($accountAddress->city_id)?$accountAddress->city_id:'';?>">
                            <input type="hidden" name="district_id" value="<?php echo isset($accountAddress->district_id)?$accountAddress->district_id:'';?>">
                            <input type="hidden" name="lng" value="<?php echo isset($accountAddress->lon)?$accountAddress->lon:'';?>">
                            <input type="hidden" name="lat" value="<?php echo isset($accountAddress->lat)?$accountAddress->lat:'';?>">
                            <ul class="ul association-list">
                                <li class="tip">
                                    <span>支持中文/拼音/简拼输入</span>
                                    <i class="association-list-close icon2-shanchu"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="us-amap-input-desc">
                            <a class="us-amap-geolocation" href="javascript:void(0)">定位</a>
                            <input type="text"
                                   placeholder="请输入详细地址" style="padding-right: 40px"
                                   name="address"
                                   max="50" class="form-control inputxt address input-desc" value="<?php echo isset($accountAddress->address)?$accountAddress->address:'';?>" />
                        </div>
                    </div>
                    <div class="us-amap-map map">
                        <img src="/img/full-screen.png" class="full-screen">
                    </div>
                    <div class="us-amap-shade"><i class="full-close icon2-shanchu"></i></div>
                    <div class="full-map-wrap">
                        <div class="us-amap-map full-map"></div>
                    </div>
                </div>
                <!-- 高德地图地址组合jq组件 结束 -->
            </div>
        </div>
        <hr>

         <div class="information-list text-center">
            <input type="button" id="closeIframe" class="btn bg-f7 mgr20" value="取消">
            <input type="submit"  class="btn btn-orange" class="btn btn-orange" value="提交">
            
        </div>
	</form>
</body>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>

<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=bbcdabda1427179d77bb95fac7b2b902"></script>
<script src="//webapi.amap.com/ui/1.0/main.js?v=1.0.11"></script>
<script src="/js/plugin/webpack-amap.js"></script>
<script>

    var index = parent.layer.getFrameIndex(window.name);

    //是否点击了定位
    var hasClickLocation = false;
    var map;
    $(function () {
        map = $$map( 'address-map' );
        map.onLocation(function () {
            hasClickLocation = true;
        });
    });

    //表单验证
    $("#account-address-add").Validform({
        ajaxPost:true,
        tiptype:3,
        beforeSubmit:function(curform){
            if ( !map.inputValid() ) {
                alert( '请输入地址！' );
                return false;
            }
            if(hasClickLocation == false){
                alert( '请先点击定位后再重试！' );
                return false;
            }
        },
        callback:function(data){
            if ( data.status == 'y') {
                if(parent.iframeCallbackAddressId != 'undefined'){
                    parent.iframeCallbackAddressId = data.id;
                }
                $.Hidemsg();
                parent.alert(data.info);
                parent.layer.close(index);
            }else{
                $.Hidemsg();
                alert(data.info);
            }
        }
	});
    
    //关闭iframe
    $('#closeIframe').click(function(){
        parent.layer.close(index);
    });
    //根据开通城市父级id获取市级数据
	$("#province").change(function() {
        $("#city").html("<option value=''>请选择市</option>");
        $("#district").html("<option value=''>请选择区</option>");
		var pid = $("#province").val();
		if(pid != ''){
            $.get("/account/get-city", { "province_id": pid }, function (data) {
		        $("#city").html(data);
		    });
		}
	});
	//根据城市获取区县
	$("#city").change(function() {
		var pid = $("#city").val();
		if(pid != ''){
			$.get("/account/get-district", { "city_id": pid }, function (data) {
		        $("#district").html(data);
		    })
		}
	});
</script>
</html>

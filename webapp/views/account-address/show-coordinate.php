<!DOCTYPE html>
<html>
<head>
    <title>显示地图</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/public.css">
    <link rel="stylesheet" type="text/css" href="/css/iconfont.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <script src="/js/jquery-1.10.2.min.js"></script>
    <script src="/js/layer/layer.js"></script>
    <script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
    <script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=bbcdabda1427179d77bb95fac7b2b902"></script>
    <script src="//webapi.amap.com/ui/1.0/main.js?v=1.0.11"></script>
    <script src="/js/plugin/webpack-amap.js"></script>
    <script src="/js/myfunc.js"></script>
</head>
<body style="background: #ffffff;">
<?php if($accountAddress): ?>
    <div class="col-md-8 information-list" style="height:auto;margin-top:0px;">
        <!-- 高德地图地址组合jq组件 开始-->
        <div class="us-amap-wrapper" id="address-map">
            <input type="hidden" name="lng" value="<?php echo isset($accountAddress->lon)?$accountAddress->lon:'';?>">
            <input type="hidden" name="lat" value="<?php echo isset($accountAddress->lat)?$accountAddress->lat:'';?>">
            <div class="us-amap-map map" style="height: 400px;margin-top:0px;">
            </div>
            <div class="us-amap-shade"><i class="full-close icon2-shanchu"></i></div>
            <div class="full-map-wrap">
                <div class="us-amap-map full-map"></div>
            </div>
        </div>
        <!-- 高德地图地址组合jq组件 结束 -->
    </div>
<?php else: ?>
<script>
$(function () {
    var index = parent.layer.getFrameIndex(window.name);
    alert("未找到数据",function () {
        parent.layer.close(index);
    });
});
</script>
<?php endif;?>
</body>
</html>

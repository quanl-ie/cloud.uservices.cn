<!DOCTYPE html>
<html>
<head>
    <title>修正地址坐标</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/public.css">
    <link rel="stylesheet" type="text/css" href="/css/iconfont.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <script src="/js/jquery-1.10.2.min.js"></script>
    <script src="/js/layer/layer.js"></script>
    <script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
    <script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=bbcdabda1427179d77bb95fac7b2b902"></script>
    <script src="//webapi.amap.com/ui/1.0/main.js?v=1.0.11"></script>
    <script src="/js/plugin/webpack-amap.js"></script>
    <script src="/js/myfunc.js"></script>
</head>
<body style="background: #ffffff;">
<?php if($accountAddress): ?>
    <form action="/account-address/change-coordinate" method="post" id="account-address-add">
        <input type="hidden" value="<?= $id ?>" id="address_id" name="address_id">
        <div class="col-md-8 information-list" style="height:auto">
            <label class="name-title-lable">
                <i class="red-i">*</i>所在城市：
            </label>
            <div class="addinput-list">
                <!-- 高德地图地址组合jq组件 开始-->
                <div class="us-amap-wrapper" id="address-map">
                    <div class="us-amap-input-content">
                        <div class="us-amap-input-ssq">
                            <input type="text" disabled="disabled"
                                   placeholder="请选择省市区"
                                   max="50" class="form-control inputxt address input-pua" value="<?php echo isset($accountAddress->cityLabel)?$accountAddress->cityLabel:'';?>" />
                            <input type="hidden" name="lng" value="<?php echo $accountAddress->lon > 0?$accountAddress->lon:'';?>">
                            <input type="hidden" name="lat" value="<?php echo $accountAddress->lat > 0?$accountAddress->lat:'';?>">
                            <ul class="ul association-list">
                                <li class="tip">
                                    <span>支持中文/拼音/简拼输入</span>
                                    <i class="association-list-close icon2-shanchu"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="us-amap-input-desc">
                            <a class="us-amap-geolocation" href="javascript:void(0)">定位</a>
                            <input type="text" style="padding-right: 40px"
                                   placeholder="请输入详细地址" disabled="disabled"
                                   max="50" class="form-control inputxt address input-desc" value="<?php echo isset($accountAddress->address)?$accountAddress->address:'';?>" />
                        </div>
                    </div>
                    <div class="us-amap-map map" style="height: 250px;">
                        <img src="/img/full-screen.png" class="full-screen">
                    </div>
                    <div class="us-amap-shade"><i class="full-close icon2-shanchu"></i></div>
                    <div class="full-map-wrap">
                        <div class="us-amap-map full-map"></div>
                    </div>
                </div>
                <!-- 高德地图地址组合jq组件 结束 -->
            </div>
        </div>
        <div class="information-list text-center">
            <input type="button" id="closeIframe" class="btn bg-f7 mgr20" value="取消">
            <input type="submit"  class="btn btn-orange" class="btn btn-orange" value="提交">

        </div>
    </form>

    <script>
        var index = parent.layer.getFrameIndex(window.name);
        //表单验证
        $("#account-address-add").Validform({
            ajaxPost:true,
            tiptype:3,
            beforeSubmit:function(curform){
                if($('input[name="lng"]').val() == '' || $('input[name="lat"]').val() == ''){
                    alert( '请先点击定位后再重试！' );
                    return false;
                }
            },
            callback:function(data){
                if ( data.status == 'y')
                {
                    $.Hidemsg();
                    parent.layer.close(index);
                    parent.doSubmit();
                }
                else
                {
                    $.Hidemsg();
                    alert(data.info);
                }
            }
        });

        //关闭iframe
        $('#closeIframe').click(function(){
            parent.layer.close(index);
        });
    </script>
<?php else: ?>
<script>
$(function () {
    var index = parent.layer.getFrameIndex(window.name);
    alert("未找到数据",function () {
        parent.layer.close(index);
    });
});
</script>
<?php endif;?>
</body>
</html>

<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = '品牌管理';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>"> -->
<style type="text/css">
    .search-box-kuang>.col-sm-6{
    padding-left: 0;
    position: relative;
    }
    .icons-calendar{
        position: absolute;
        right: 24px;top: 9px;
    }
    .popover{max-width: 500px !important;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">品牌管理
    <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="用于创建收费标准、产品信息、售后订单、技师结算规则等操作时使用"></span>
    </span>
    <?php if (!empty($flag) && $flag == 1): ?>
        <?php if(in_array('/brand/add',$selfRoles)){?>
            <span class="btn btn-success jn-btn" style=" color:#FFF;float:right;width:auto;" id="addParentIframe"  data-toggle="modal" data-target="#scrollingModal" onclick="addBrand()">添加</span>
        <?php };?>
    <?php endif; ?>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php if($data):?>
                    <form action="/sale-order/index" class="form-horizontal form-border" id="form">
                        <div class="form-group jn-form-box">

                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead bgcolor="#455971">
                                <tr>
                                    <th>品牌名称</th>
                                    <th>状态</th>
                                    <?php if (!empty($flag) && $flag == 1): ?>
                                    <th>操作</th>
                                    <?php endif; ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($data as $key=>$val):  ?>
                                        <tr>
                                            <td><?=$val['title']; ?></td>
                                            <td><?=$val['status_desc']; ?></td>
                                            <?php if (!empty($flag) && $flag == 1): ?>
                                            <td>
                                                <?php if (isset($val['status']) && $val['status'] == 1) : ?>
                                                <span class="del">
                                                    <?php if(in_array('/brand/change-status',$selfRoles)){?>
                                                        <a href="javascript:;" onclick="disBranf(<?=$val['brand_id']; ?>,2)">禁用</a>
                                                    <?php };?>
                                                </span>
                                                <?php else: ?>
                                                    <span class="del">
                                                        <?php if(in_array('/brand/change-status-on',$selfRoles)){?>
                                                            <a href="javascript:;" onclick="enaBrand(<?=$val['brand_id']; ?>,1)" >启用</a>
                                                        <?php };?>
                                                </span>
                                                <?php endif; ?>
                                            </td>
                                            <?php endif; ?>
                                        </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo isset($pageHtml)?$pageHtml:'';?>
                    </div>
                    <?php else:?>
                        <div class="no-data-page">
                            <div>
                                <div class="kbtp"></div>
                                <p class="no-data-zi">您还没有添加过经营的品牌</p>
                                <?php if (!empty($flag) && $flag == 1): ?>
                                <span class="btn btn-success" onclick="addBrand()">添加品牌</span>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif;?>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".tip").popover();
    window.onload = function(){
        var  heightKs = ($(window).height())-265+'px';
        $('.no-data-page').height(heightKs);
    };
    $(window).resize(function(){
        var heightKs = ($(window).height())-265+'px';
        $('.no-data-page').height(heightKs);
    });
    //添加
    function addBrand () {
        layer.open({
            type: 2,
            title: '添加品牌',
            area: ['700px', '470px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/brand/add', 'no'],
            end:function(){
                location.reload();
            }
        });
    }
    
    /**
     * 禁用
     * @param id
     * @param status
     */
    function disBranf(id,status) {
        layer.confirm('您确定要禁用该品牌吗？禁用后有此品牌的经销商将不能创建该品牌的新订单，但不影响未完成和已完成订单数据。', {
            area:['320px','210px'],
            btn: ['确定','取消'] //按钮
        }, function(){
            ajaxPost(id,status);
        }, function(){
            layer.close();
        });
    }

    /**
     *  启用
     * @param id
     * @param status
     */
    function enaBrand(id,status) {
        layer.confirm('您确定要启用该品牌吗？启用后原有此品牌的经销商可以创建该品牌的订单。', {
            area:['320px','210px'],
            btn: ['确定','取消'] //按钮
        }, function(){
            ajaxPostOn(id,status);
        }, function(){
            layer.close();
        });
    }
    
    function ajaxPost(id,status){
        var url = '/brand/change-status';
        $.ajax({
            type: 'POST',
            url: url,
            data: {'id':id,'status':status},
            dataType: 'json',
            success: function(data){
                if (data.code == 200) {
                    layer.alert(data.message, {
                    }, function(){
                        location.reload();
                    });
                }else{
                    alert(data.message);
                }
            }
        });
    }

    function ajaxPostOn(id,status){
        var url = '/brand/change-status-on';
        $.ajax({
            type: 'POST',
            url: url,
            data: {'id':id,'status':status},
            dataType: 'json',
            success: function(data){
                if (data.code == 200) {
                    layer.alert(data.message, {
                    }, function(){
                        location.reload();
                    });
                }else{
                    alert(data.message);
                }
            }
        });
    }

</script>
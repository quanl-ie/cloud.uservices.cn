<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '编辑类目';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<!DOCTYPE html>
<html>
<head>
</head>
<body style="background: #fff;">
<style type="text/css">
    .city3{width: 45%;}
    .region-box-ul{height: 240px;}
    .icon-jia,.icon-guanbi{color: #2693ff}
    .icon-jia:before{font-weight: 900;display: inline-block;width: 40px;text-align: right;}
    .region-box-ul>li{padding: 0}
</style>
<section id="main-content">
    <div class="bothEnds" style="width: 550px;margin:30px auto">
        <div class="city3">
            <div class="region-box">
                <p class="region-box-title">请选择品牌：</p>
                <ul class="region-box-ul">
                    <div class="addBrand sort_box">
                        <?php if (!empty($brand_list)) : ?>
                        <?php foreach ($brand_list as $key => $val) :?>
                            <?php if (!empty($brand_ids) && in_array($key,$brand_ids)) : ?>
                                <li>
                                    <span class="brand-Name num_name"><?=$val?></span>
                                    <div class="brand-operation">
                                        <i class="icon2-jia" id="<?=$key;?>" style="display: none"></i>
                                        <i class="added" style="display: block">已添加</i>
                                    </div>
                                </li>
                            <?php else: ?>
                                <li>
                                    <span class="brand-Name num_name"><?=$val?></span>
                                    <div class="brand-operation">
                                        <i class="icon2-jia" id="<?=$key?>"></i>
                                        <i class="added">已添加</i>
                                    </div>
                                </li>
                            <?php endif;?>
                        <?php endforeach; ?>
                        <?php endif;?>
                    </div>
                </ul>
            </div>
        </div>
        <div class="city3">
            <div class="region-box">
                <p class="region-box-title">已添加品牌：</p>
                <ul class="region-box-ul added-box">
                    <?php if (!empty($brand)) : ?>
                    <?php foreach ($brand as $key => $val):?>
                        <li data="<?=$key;?>">
                            <span class="brand-Name num_name"><?=$val;?></span>
                            <div class="brand-operation" data="<?=$key;?>"></div>
                        </li>
                    <?php endforeach;?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <div class="city3_fix"></div>
    </div>
    <p class="xian"></p>
    <div style="text-align: center;">
        <button class="btn bg-f7" id="closeIframe">取消</button>
        <button class="btn btn-orange" id="okBtn">保存</button>
    </div>
             
</section>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script type="text/javascript" src="/js/jquery.charfirst.pinyin.js"></script>
<script type="text/javascript" src="/js/sort.js"></script>
<script>
  var index = parent.layer.getFrameIndex(window.name);
  var okId = [];
   $('.icon2-jia').on('click',function() {
        var brandId = $(this).attr('id');
        var brandName = $(this).parent().siblings('span').text();
        $(this).hide();
        $(this).siblings('i').show();
         var brandLi ='<li data="'+brandId+'"><span class="brand-Name num_name">'+brandName+'</span><div class="brand-operation" data="'+brandId+'"><i class="icon2-shanchu1"></i></div></li>';
          $('.added-box').append(brandLi);
   });
  
   // 删除
     $('.added-box').on('click','li div',function() {
        var ccc = '#'+$(this).attr('data');
        $(this).parent().remove();
        $('.addBrand').find(ccc).show();
        $('.addBrand').find(ccc).siblings('i').hide();
     });
  
  // 保存
  $('#okBtn').on('click',function() {
    getBrandId ();
    $.ajax({
            url:'/brand/add',
            data:{id:okId},
            type:'POST',
            dataType:'json',
            success:function(data){
                if (data.code == 200) {
                    parent.layer.close(index);
                    parent.layer.alert(data.message, function(index){
                        parent.location.reload();
                    });
                }else{
                    alert(data.message);
                }
            }
        });

  });
  
  // 获取品牌id
  function  getBrandId () {
      var brandLi = $('.added-box li');
      for(var i=0;i<brandLi.length;i++){
        if (brandLi[i].getAttribute('data') !== '') {
            okId[i] = brandLi[i].getAttribute('data');
           
        }                    
    }
  }

  //关闭iframe
  $('#closeIframe').click(function(){
      parent.layer.close(index);
  });
</script>
</body>
</html>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = '添加销售退货';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">添加销售退货
      <!--  <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="
            选择客户：请在您所创建的客户中选择，选择客户后，系统会自动将该客户的联系方式及地址进行填充，如客户未创建，请点击右侧添加按钮进行创建<br/><br/>

选择产品：请在您所创建的客户产品中选择，选择产品后，系统会自动将产品类目、品牌、序列号等信息进行填充，如未创建产品，请点击右侧添加按钮进行创建<br/><br/>

质保状态：在创建订单时，请严格确认客户设备的质保状态来选择类型，以免影响服务价格<br/><br/>

派单方式：您可选择指定合作服务商或输入技师电话进行服务，也可通过优服务平台进行派单，服务商需要通过合作申请后方可选择
        "></span>-->
    </span>
</div>
<!--引入CSS-->
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/webuploader/webuploader.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jquery.autocomplete.css');?>">
<!--引入JS-->
<?=Html::jsFile('@web/webuploader/webuploader.js')?>
<?=Html::jsFile('@web/js/bootstrap-select.js')?>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body"><?php
                    $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'form-horizontal',
                            'id' => 'return_goods_add',
                            'name' => 'form1',
                            'enctype' => 'multipart/form-data',
                        ],
                        'fieldConfig' => [
                            'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-xs-6\">{input}{error}</div>",
                            'labelOptions' => ['class' => 'col-xs-2 control-label text-right'],
                        ]
                    ]);
                    ?>
                    <div class="col-md-12">
                        <h5 class="block-h5 block-h5one">基本信息</h5>
                    </div>
                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 关联用户：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <p style="line-height: 32px"><?=$contract['account_name']?></p>
                           <!-- <input type="text" name="" class="form-control" maxlength="30" id="account_name" datatype="*" sucmsg=" " nullmsg="请输入客户姓名/电话……"  placeholder="请输入客户姓名/电话……">-->
                            <input type="hidden" name="account_id" class="form-control" id="account_id"  value="<?=$contract['account_id']?>" accept="">
                        </div>
                    <!--    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-2 add-btn">
                            <span class="btn btn-success  " style=" color:#FFF;float:right;font-size:14px; " id="addAccount"  data-toggle="modal" data-target="#scrollingModal">添加</span>
                        </div>
                        <div class="col-md-offset-2 col-md-6 misplacement" style="display:none" id="account_msg">-->
                    </div>
                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 退货主题：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <input type="text" name="subject" class="form-control" maxlength="20" id="subject"  datatype="*" sucmsg=" " nullmsg="请输入退货主题（20字以内）"  placeholder="请输入退货主题（20字以内）">
                            <p class="misplacement" id="subject_msg"></p>
                        </div>

                        
                    </div>

                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 退货日期：
                        </label>
                        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 addinput-list">
                            <input type="text" id="datetimepicker-pay-top" class="form-control" name="return_date" value="<?php echo isset($_GET['plan_start_time'])?$_GET['plan_start_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd'})">
                                <span class="icon2-rili" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top',dateFmt:'yyyy-MM-dd'})" placeholder="请输入">
                                        </span>
                                <p class="misplacement" id="time_msg"></p>
                        </div>
                    </div>

                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i"></i> 退货原因：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                                <select name="return_reason" class="form-control">
                                  <option value="0">请选择</option>
                                  <?php foreach($contract['reason'] as $k=>$v) :?>
                                      <option value="<?=$k?>"><?=$v?></option>
                                  <?php endforeach;?>
                               </select>
                                <p class="misplacement"  id="return_reason_msg"></p>
                        </div>
                    </div>
                     <h5 class="block-h5">退货明细</h5>


                <div class="information-list"  style="margin-top: 0">
                    <label class="name-title-lable" for="workorder-account_id">
                        <i class="red-i"></i> 关联合同：
                    </label>
                    <span class="c_red"></span>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                        <a href="JavaScript:void(0)"
                           onclick='openPage("/contract/detail?id=<?=$contract['id']?>&contract_id=<?=$contract['id']?>","合同详情")'>
                            <p style="line-height: 32px"><?=$contract['subject']?></p>
                        </a>
                        <input name="contract_id" type="hidden" value="<?=$contract['id']?>" />
                        <p class="misplacement" id="return_reason_msg"></p>
                    </div>
                </div>

                <div class="information-list">
                    <label class="name-title-lable" for="workorder-account_id">
                        <i class="red-i">*</i> 是否入库：
                    </label>
                    <span class="c_red"></span>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                        <div class="roundRadio select-Zhouqi">
                            <input type="radio" class="available_cost_type" name="need_stock_in" value="1" id="available_cost_type1" checked="checked">
                            <label for="available_cost_type1">是</label>
                        </div>
                        <div class="roundRadio  select-Zhouqi">
                            <input type="radio" class="available_cost_type" name="need_stock_in" value="2" id="available_cost_type2">
                            <label for="available_cost_type2">否</label>
                        </div>
                        <p class="misplacement" id="collection_status_msg"></p>
                    </div>
                    
                </div>

                <div class="information-list" style="height: auto">
                    <table>
                        <thead>
                        <tr>
                            <th style="width: 50px;" class="padding15">
                                <input type="checkbox" name="" id="allDetailed">
                                <label for="allDetailed"> <i class="gou-i"></i></label>
                            </th>
                            <th>产品名称</th>
                            <th>产品类目</th>
                            <th>品牌</th>
                            <th>型号</th>
                            <th width="80px">数量</th>
                            <th>单价</th>
                            <th>折扣（%）</th>
                            <th>总价</th>
                            <th>折后总价</th>
                            <th>质保终止日期</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($contractDetail)) :?>
                        <?php foreach($contractDetail as $v) :?>
                            <tr id="<?=$v['id']?>">
                                <td class="padding15">
                                    <input type="checkbox"  name="contractDetail[<?=$v['id']?>][id]"  class="available_cost_type select_product" id="prod_id<?=$v['prod_id']?>"  value="<?=$v['id']?>"> <label for="prod_id<?=$v['prod_id']?>"> <i class="gou-i"></i></label>
                                    <input type="hidden" name="contractDetail[<?=$v['id']?>][prod_id]"    value="<?=$v['prod_id']?>">
                                    </td>
                                <td><?=$v['prod_name']?></td>
                                <td><?=$v['class_name']?></td>
                                <td><?=$v['brand_name']?></td>
                                <td><?=$v['model']?></td>
                                <td ><input name="contractDetail[<?=$v['id']?>][prod_num]" class="prod_num" value="<?=$v['total_num']?>" />
                                    <input type="hidden" class="last_num" value="<?=$v['total_num']?>"/>
                                </td>
                               <!-- <td><?/*=$v['sale_price']*/?></td>
                                <td><?/*=$v['rate']*/?>%</td>
                                <td><?/*=sprintf('%0.2f',$v['sale_price']*$v['prod_num'])*/?></td>
                                <td><?/*=$v['total_amount']*/?></td>
                                <td><?/*=$v['warranty_date']*/?></td>-->
                                <td><?=$v['sale_price']?> <input class="price"     name="contractDetail[<?=$v['id']?>][sale_price]"   type="hidden" size="10" value="<?=$v['sale_price']?>" onkeyup="clearNoNum(this)"/></td>
                                <td><?=$v['rate']?><input class="rate"     type="hidden"  name="contractDetail[<?=$v['id']?>][rate]"  maxlength="3" value="<?=$v['rate']?>"/></td>
                                <td><span class="amount"><?=$v['amount']?></span><input     class="amount_hidden" name="contractDetail[<?=$v['id']?>][amount]"  type="hidden" value="<?=$v['amount']?>"/></td>
                                <td>
                                    <span class="total_amount"><?=$v['total_amount']?></span>
                                    <input class="total_amount_hidden" name="contractDetail[<?=$v['id']?>][total_amount]" type="hidden" value="<?=$v['total_amount']?>"/>
                                </td>
                                <td><input class="warranty_date"  name="contractDetail[<?=$v['id']?>][warranty_date]" type="" value="<?=substr($v['warranty_date'],0,10)?>" maxlength="10" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"  readonly="readonly"  /></td>
                            </tr>
                        <?php endforeach;?>
                        <?php else :?>
                            <td colspan="11">暂无退货数据</td>
                        <?php endif;?>
                        </tbody>
                    </table>
                    <p class="misplacement"   id="product_msg"></p>
                    <input type="hidden" name="total_amount" id="all_total" value="">
                </div>
                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            备注：
                        </label>
                        <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7 addinput-list" style="margin-bottom: 30px;">
                            <textarea class="ramark-box" id="remarks" maxlength="200" placeholder="请输入备注信息（200字以内）" name="express_description" id="express_description"><?php if(isset($data['description'])):?><?=$data['description']?><?php endif;?></textarea>
                            <p class="xianzhi"><span  id='word'>0</span>/200</p>
                        </div>
                    </div>
                    <hr style="clear: both;">
                   <div class="information-list" style="text-align: center;">
                            <input type="submit" class="btn btn-orange mgr20 btnSubmit"  value="提交"  />
                            <input type="button" class="btn btn-orange mgr20 btnSubmiting" style="display: none;" value="提交中..."  />
                            <a href="/contract/executing"><button type="button" class="btn bg-f7">返回</button></a>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- 时间插件/ -->
<?=Html::jsFile('@web/js/WdatePicker/WdatePicker.js')?>
<?=Html::jsFile('@web/js/WdatePicker/time.js')?>
<script type="text/javascript" src="/js/jquery-migrate-1.1.0.js"></script>
<script src="/js/jquery.autocomplete.js" type="text/javascript"></script>
<script>
    function openPage(url,title) {
        window.parent.postMessage(JSON.stringify({
            url: url,
            title: title,
        }), "*")
    }
    // 字数限制
    $("#remarks").keyup(function(){
        if($("#remarks").val().length > 200){
            $("#remarks").val( $("#remarks").val().substring(0,200) );
        }
        $("#word").text( $("#remarks").val().length ) ;
    });
    //单项选择
    $('.radio-li').on('click',function() {
        $(this).find('span').css({'background':'#2693ff','color':'#fff'});
        $(this).siblings('li').find('span').css({'background':'#fff','color':'#333','border-color':'#ccc'});
        $(this).parent('ul').next('input').val($(this).find('span').attr('id'));
    })
    // 选择
    $('.xiangxia').on('click',function() {
        $('.addinput-list-data').hide();
        $(this).next('.addinput-list-data').show();
    })
    //
    $('.addinput-list-data li').on('click',function() {
        $('.addinput-list-data').hide();
        $(this).parent('ul').siblings('.form-control').val($(this).attr('id '));
    })


    //添加客户
    var iframeSrc = '';
    $("#addAccount").click(function () {
        iframeSrc = 'order';
        layer.open({
            type: 2,
            title: '添加客户',
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/account/add/'
        });
    });

    /************************************ajax获取客户相关信息********************************************/
    var iframeCallbackSaleOrderId = 0;

        //模糊查询选中
    var accountId = 0;
    function accountName(id) {

        $("#brand_msg").hide();
        $("#address_msg").hide();
        $("#provide_msg").hide();

        accountId = id;
        $("#account_id").val(id);
        $("#addAccountAddres").attr('title',id);
        $("#addAccountBrand").attr('title',id);
        $("#account_select").hide();
        account_address(id);
        service_brand(id);
    }
    //查询客户对应的地址信息
    function account_address(account_id) {
        $.post('/order/get-address',{account_id:account_id},function(res) {
            $("#account_address").html(res);
            if(iframeCallbackAddressId>0){
                $("#account_address").val(iframeCallbackAddressId);
            }
        })
    }
    //查询客户对应服务产品
    function service_brand(account_id) {
        $.post('/order/get-service-brand',{account_id:account_id},function(res) {
            $("#account_service_brand").html(res);
            if(iframeCallbackSaleOrderId > 0 ){
                $("#account_service_brand").val(iframeCallbackSaleOrderId);
                $("#account_service_brand").change();
            }
        })
    }

    /***********************     END       ************************/
    /**********************   提交信息验证   ************************/



    //获取焦点提示框消失
    $("#subject").focus(function(){
        $("#subject_msg").text("");
        $("#subject_msg").hide();
    })
    $("#datetimepicker-pay-top").focus(function() {
        $("#time_msg").text("");
        $("#time_msg").hide();
    })


    $("#account_name").autocomplete("/account/sreach", {
        minChars: 1,
        matchCase:false,//不区分大小写
        autoFill: false,
        max: 10,
        dataType: 'json',
        width:$('#account_name').outerWidth(true)+'px',
        extraParams:{v:function() { return $('#account_name').val();}},
        parse: function(data) {
            return $.map(eval(data), function(row) {
                return {
                    data: row,
                    value: row.id,    //此处无需把全部列列出来，只是两个关键列
                    result: row.name
                }
            });
        },
        formatItem: function(row, i, max,term) {
            return  row.name;
        },
        formatMatch: function(row, i, max) {
            return row.name;
        },
        formatResult: function(row) {
            return row.name;
        },
        reasultSearch:function(row,v)//本场数据自定义查询语法 注意这是我自己新加的事件
        {
            //自定义在code或spell中匹配
            if(row.data.name.indexOf(v) == 0)
            {
                return row;
            }
            else
                return false;
        }
    }).result(function(event, data, formatted) { //回调
        if(data.id>0){
            accountName(data.id);
            $('#account_name').val(data.name);
        }
        else {
            $('#account_name').val('');
        }
    });



    //填充用户信息
    function fillAccount($accountId,$accountName) {
        $("#account_name").val($accountName);
        $('#account_id').val($accountId);
        accountName($accountId);
    }

    $("#account_address").click(function() {
        if(( $.trim($("#account_address").html()) == '')){
            $("#address_msg").text("请先选择客户");
            $("#address_msg").show();
        }else{
            var accountAddress = $("#account_address").find("option:selected").text();
            var accountName = $("#account_name").val();
            $("#delivery_address").val(accountName+'/'+accountAddress);
        }

    });



    //产品数量验证
    $('body').on('keyup','.prod_num',function(){
        var prod_num = $(this).val();
        if(isNaN(prod_num)){
            alert('请输入数字');
            $(this).val('1');
            return false;
        }
        if(prod_num!='' && prod_num==0){
            $(this).val('1');
        }

        var id = $(this).parents('tr').attr("id");
        getTotal(id);
        getAllTotal();
    })


    $(".select_product").change(function(){
       if(this.checked){
            $("#product_msg").val('');
            $("#product_msg").css('display','none');
        }
    })





    //计算单个产品总额
    function getTotal(id){
        var price      =  $("#"+id).find('.price').val();
        var prod_num   =  $("#"+id).find('.prod_num').val();
        var rate       =  $("#"+id).find('.rate').val();
        var total      = price*prod_num;
        total          = new Number(total);
        total          = total.toFixed(2);
        var total_amount = total*rate/100;
        total_amount = new Number(total_amount);
        total_amount = total_amount.toFixed(2);
        $("#"+id).find('.amount').html(total);
        $("#"+id).find('.amount_hidden').val(total);
        $("#"+id).find('.total_amount').html(total_amount);
        $("#"+id).find('.total_amount_hidden').val(total_amount);

    }


    //计算退货总额
    function getAllTotal(){
        //获取产品明细总价
        var checkboxInput = $(".select_product");
        var sum = 0;
        $(".select_product").each(function(i){
                id = $(this).val();
                var total_amount = $("#"+id).find('.total_amount_hidden').val();
                sum = sum+parseFloat(total_amount);
        });

        sum = new Number(sum);
        sum = sum.toFixed(2);
        $("#total_amount").val(sum);
        $('#all_total').val(sum);
    };



    //价格数据格式化
    function clearNoNum(obj){
        if(obj.value !=''&& obj.value.substr(0,1) == '.'){
            obj.value="";

        }
        obj.value = obj.value.replace(/^0*(0\.|[1-9])/, '$1');
        obj.value = obj.value.replace(/[^\d.]/g,"");
        obj.value = obj.value.replace(/\.{2,}/g,".");
        obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
        obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数
        if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
            if(obj.value.substr(0,1) == '0' && obj.value.length == 2){
                obj.value= obj.value.substr(1,obj.value.length);
            }
        }
    }








    //产品数量验证
    $(".prod_num").keyup(function(){
        var prod_num = $(this).val();
        if(isNaN(prod_num)){
            alert('请输入数字');
            $(this).val('1');
            return false;
        }
        var last_num = $(this).next().val();
        if(parseInt(prod_num)>parseInt(last_num)){
            alert('退货数量不能超出合同限制');
            $(this).focus(function(){
                $(this).val('');
            });
        }
    });


    //表单数据提交验证
    $("#return_goods_add").submit(function(){
        getAllTotal();
        var subject = $("#subject").val();
        var returnTime = $("#datetimepicker-pay-top").val();

        var productLength = 0;
        //计算勾选的产品个数
        var checkboxInput = $(".select_product");
        var sum = 0;
        ids =new Array();
        $(".select_product").each(function(i){
            if(this.checked){
                var check_num = $(this).parents("tr").find(".prod_num").val();
                if(check_num == 0){
                    $("#product_msg").css("display",'block');
                    $("#product_msg").html('产品数量不能为0');
                    return false;
                }
                ids[i] = $(this).val();
            }
        })
        var productLength = ids.length;


        if(!subject){
            $("#subject_msg").css("display",'block');
            $("#subject_msg").html('请输入退货主题');
            // alert(33);
            return false;
        }


        if(!returnTime){
            $("#time_msg").css("display",'block');
            $("#time_msg").html('请输入退货日期');
            // alert(44);
            return false;
        }
        if(!productLength){
            $("#product_msg").css("display",'block');
            $("#product_msg").html('请勾选产品');
            // alert(44);
            return false;
        }
        
        $(".btnSubmit").attr("disabled",true);
    })
    //全选
    $('#allDetailed').on('change',function() {

        $(this).is(':checked')?$(this).parents('table').find('td.padding15 input[type="checkbox"]').prop('checked',true):$(this).parents('table').find('td.padding15 input[type="checkbox"]').prop('checked',false)
        
    })

    //提示
    $(".tip").popover();

    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message');?>');
    <?php endif;?>


</script>


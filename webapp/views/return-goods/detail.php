<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = '退货详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">

</style>

<div class="jn-title-box no-margin-bottom">
    <span class="jn-title" id="no" data-no="<?php $info['no'];?>">退货详情</span>
    <div  class="right-btn-box">
        <?php if($info['audit_status'] == 0 && $info['status'] == 1){?>
        <!--<a href="" class="cancelOrder">
            <button class="btn btn-orange mgr8">打印</button>
        </a>-->
            <?php if(in_array('/return-goods/check-option',$selfRoles)):?>
                <a href="" class="setAudit" data-type="no">
                    <button class="btn btn-orange mgr8" class="checkbtn" id="refused">拒绝</button>
                </a>
                <a href="" class="setAudit" data-type="ok">
                    <button class="btn btn-orange mgr8" class="checkbtn" id="agreen">同意</button>
                </a>
            <?php endif;?>
        <?php }?>
        <?php if($info['audit_status'] == 2 && in_array($info['status'],[2,3]) ){?>
        <!--<a href="" class="cancelOrder">
            <button class="btn btn-orange mgr8">打印</button>
        </a>-->
        <a href="/order/re-assign?work_no=" >
            <button class="btn btn-orange mgr8">入库</button>
        </a>
        <?php }?>
        <?php if($info['audit_status'] == 2 && $info['status'] == 1 ){?>
        <!--<a href="" class="cancelOrder">
            <button class="btn btn-orange mgr8">打印</button>
        </a>-->
        <?php }?>
        <button class="btn bg-f7" onclick="javascript:history.back(-1);">返回</button>
    </div>
</div>

<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body" style="padding:0px 30px 50px 30px;">
                	<h5 class="block-h5" style="margin-top: 0">用户信息</h5>
                    <div class="information-list" style="height: auto;">
                        <div class="col-xs-12	col-sm-12	col-md-4	col-lg-4">
                        	<label  class="left-title80">退货编号：</label>
                            <input type="hidden" value="<?=$info['id']?>" id="id"/>
                        	<div class="right-title3"><span><?=$info['no']?></span></div>
                        </div>
                        <div class="col-xs-12	col-sm-12	col-md-4	col-lg-4">
                        	<label   class="left-title80">退货主题：</label>
                        	<div  class="right-title3"><span><?=$info['subject']?></span></div>
                        </div>
                        <div class="col-xs-12	col-sm-12	col-md-4	col-lg-4">
                        	<label   class="left-title80">关联客户：</label>
							<div  class="right-title3"><span><?=$info['account']?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-4">
                            <label   class="left-title80">退货状态：</label>
                            <div  class="right-title3 2693ff"><span><?=$info['show_status']?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-4">
                            <label  class="left-title80">退货原因：</label>
                            <?php if($info['return_reason']) {?>
                            <div class="right-title3"><span><?=$info['show_reason_status']?></span></div>
                            <?php }else { ?>
                                <div class="right-title3"><span>无</span></div>
                            <?php }?>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-4">
                            <label  class="left-title80">创建人：</label>
                            <div class="right-title3"><span><?=$info['create_user']?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-12    col-lg-12">
                            <label   class="left-title80">关联合同：</label>
                            <div  class="right-title3">
                                <a href="/contract/detail?id=<?=$info['contract_id']?>&contract_id=<?=$info['contract_id']?>">
                                    <span><?=$info['contract_info']?></span>
                                </a>
                            </div>
                        </div>
                        
                        <div class="col-xs-12   col-sm-12   col-md-12    col-lg-12">
                            <label  class="left-title80">创建时间：</label>
                            <div class="right-title3"><span><?=isset($info['create_time'])?$info['create_time']:date("Y-m-d H:i:s",$info['create_time'])?></span></div>
                        </div>
                        <div class="col-xs-12  col-sm-12   col-md-12    col-lg-12">
                                <label  class="left-title80">退货日期：</label>
                                <div class="right-title3"><span><?=substr($info['return_date'],0,10)?></span></div>
                        </div>
                    </div>
                   
					<h5 class="block-h5">产品明细</h5>
					<div class="information-list" style="height: auto">
						<table>
							<thead>
								<tr>
									<th>产品名称</th>
									<th>产品类目</th>
									<th>品牌</th>
									<th>型号</th>
									<th>数量</th>
									<th>单价</th>
									<th>折扣（%）</th>
									<th>总价</th>
									<th>折后总价</th>
								</tr>
							</thead>
							<tbody>
                            <?php foreach($detail as $v) :?>
								<tr>
									<td style="max-width: 180px;"><?=$v['prod_name']?></td>
									<td><?=$v['class_name']?></td>
									<td><?=$v['brand_name']?></td>
									<td><?=$v['model']?></td>
									<td><?=$v['prod_num']?></td>
									<td><?=$v['sale_price']?></td>
									<td><?=$v['rate']?>%</td>
									<td><?=sprintf('%0.2f',$v['sale_price']*$v['prod_num'])?></td>
									<td><?=$v['total_amount']?></td>

								</tr>
                            <?php endforeach;?>
							</tbody>
						</table>
					</div>
                    <?php if($stockInList){?>
                        <h5 class="block-h5">入库单</h5>
                        <div class="information-list" style="height: auto">
                            <table>
                                <thead>
                                <tr>
                                    <th>入库单号</th>
                                    <th>入库状态</th>
                                    <th>申请时间</th>
                                    <th>入库时间</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($stockInList as $key=>$val) {?>
                                    <tr>
                                        <td style="max-width: 180px">
                                            <a href="/stock-in/view?id=<?=isset($val['id'])?$val['id']:"" ?>"><?=$val['no']?></a>
                                        </td>
                                        <td><?=isset($statuslist[$val['audit_status']])?$statuslist[$val['audit_status']]:''?></td>
                                        <td><?=$val['create_time']?></td>
                                        <td><?=$val['audit_date']?></td>
                                    </tr>
                                <?php };?>
                                </tbody>
                            </table>
                        </div>
                    <?php }?>

                </div>
            </div>
        </div>
    </div>
</section>


<!-- 设置同意拒绝弹层 -->
<div id="noLayer" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <div class="select-fuji">
            <textarea placeholder="请填写审批意见，非必填"  onkeyup=" wordsLimit(this)" class="opinionContent" style="width: 100%;height: 100px;"></textarea>
            <p class="xianzhi" style="right: 10px;"><span id="word">0</span>/200</p>
        </div>
        <div style="text-align: center;margin-top:15px;">
            <button class="btn" id="layerCancel" style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search" ></i>取消</button>
            <button class="btn btn-success" id="layerSubmit"  style="padding: 4px 12px" data-type="2"><i class="fa fa-search"></i>拒绝</button>
        </div>
    </div>
</div>
<div id="okLayer" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <div class="select-fuji">
            <textarea placeholder="请填写审批意见，非必填" onkeyup=" wordsLimit(this)" class="opinionContent" style="width: 100%;height: 100px;"></textarea>
            <p class="xianzhi" style="right: 10px;"><span id="word">0</span>/200</p>
        </div>
        <div style="text-align: center;margin-top:15px;">
            <button class="btn" id="layerCancel" style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search" ></i>取消</button>
            <button class="btn btn-success" id="layerSubmit"  style="padding: 4px 12px" data-type="1"><i class="fa fa-search"></i>同意</button>
        </div>
    </div>
</div>


<script>
    var layerIndex = null;
    //no = $(this).attr('data-no');
    $(".setAudit").click(function () {
        var setType = $(this).attr('data-type');
        if(setType == 'ok'){
            var statusType = 2;
            layerIndex = layer.open({
                type: 1,
                title: '确认同意审批退货吗？',
                area: ['400px', '250px'],
                fixed: true,
                maxmin: false,
                content: $('#okLayer').html()
            });
        }else {
            var statusType = 3;
            layerIndex = layer.open({
                type: 1,
                title: '确认拒绝审批退货吗？',
                area: ['400px', '250px'],
                fixed: true,
                maxmin: false,
                content: $('#noLayer').html()
            });
        }
        return false;
    });
    //
    $("body").delegate("#layerSubmit","click",function(){
        var contractNo = $('#no').attr('data-no');
        var auditStatus = $(this).attr('data-type');
        var auditSuggest = $(this).parents('div').find('.opinionContent').val();

        if($.trim(auditStatus) == ''){
            alert('操作状态错误！');
            return false;
        }
        id = $("#id").val();
        $.ajax({
            url:'/return-goods/check-option',
            type:'POST',
            data:{id:id,auditStatus:auditStatus,auditSuggest:auditSuggest},
            datatype:'json',
            success:function (msg) {
                data = eval('('+msg+')');
                if(data.code == 200){
                    if(layerIndex){
                        layer.close(layerIndex);
                    }
                    alert('操作成功');
                    window.location.reload(true);
                }
                else {
                    alert(data.message);
                }
            }

        });
    });
    //关闭弹层
    $("body").delegate("#layerCancel","click",function(){
        if(layerIndex){
            layer.close(layerIndex);
        }
    });
</script>

<?php
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
    use webapp\models\Menu;
    
    $this->title = '拆单设置';
    $this->params['breadcrumbs'][] = ['label' => '系统设置', 'url' => ['sys/index']];
    $this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<style>
    .modal-content {border: none;border-radius: 0px;box-shadow: inherit !important;}
    .ck_role .checkbox{float:left;margin-right:10px;}
    .radio { float: left;margin-top: -5px;}
    .radio label, .checkbox label{padding-left: 0;padding-right: 30px;}
</style>
<section id="main-content">
    <div class="panel-body"><?php
            $form = ActiveForm::begin([
                'options' => [
                    'class' => 'form-horizontal',
                    'id' => 'form2',
                    'name' => 'form1',
                    'enctype' => 'multipart/form-data',
                
                ],
                'fieldConfig' => [
                    'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-xs-6 addinput-list\">{input}{error}</div>",
                    'labelOptions' => ['class' => 'name-title-lable'],
                ]
            ]);
        ?>

        <input type="hidden" name="id" value="<?=isset($data['id']) ? $data['id'] :""; ?>" >

        <div class="form-group">
            <label class="name-title-lable">拆单功能开关：</label>
            <div class="col-xs-6 addinput-list" style="padding-left: 0">
                <?php if (!empty($list)) : ?>
                <?php foreach ($list as $key => $val) : ?>
                <div class="radio">
                    <input type="radio" name="status"  <?php $data['status'] = isset($data['status']) ? $data['status'] : 2; if ($data['status'] == $key) : ?>checked="checked" <?php endif; ?> value="<?=$key ?>" id="<?=$key ?>"><label for="<?=$key ?>"><?=$val ?></label>
                </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        
        <hr>
        <div class="form-group">
            <div class="col-xs-6 addinput-list">
                <input type="submit" class="btn btn-orange mgr20 " style=" color:#FFF;" value="保存" />
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <span style="color: red">注：启用拆单功能后，创建订单且服务类型为“看现场”时，可将所选的多个产品拆分成多个订单，拆分的多个订单可分别指派，每个订单服务互不影响；如不启用，则默认将多个产品合并为同一个订单进行指派并服务。</span>
    </div>
</section>
<script>
    window.onload = function(){
        
        <?php if(Yii::$app->session->hasFlash('dismantling_config_message')):?>
        setTimeout(function () {
            alert('<?php echo Yii::$app->session->getFlash('dismantling_config_message'); ?>');
        },500);
        <?php endif;?>
    }
</script>

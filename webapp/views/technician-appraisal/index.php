<?php

use yii\helpers\Url;
use webapp\models\BrandQualification;

$this->title = '技师评价统计';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/us-icon.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/css/return-purchase/index.css'); ?>?v=<?php echo time(); ?>">
<style type="text/css">
    .us-table-body-empty {
        padding: 10px 0;
    }

    .us-table tbody, .us-table thead, .us-th {
        border: 1px solid rgb(234, 234, 234) !important;
    }

    .bottom-none {
        border-bottom: none !important;
    }

    .us-th-rigth {
        border-right: 1px solid rgb(234, 234, 234) !important;
    }

    .us-table th, .us-table td {
        /*padding: 0 !important;*/
        text-align: left;
        white-space: nowrap;
    }

    .us-table thead th {
        height: 25px !important;
    }

    .us-table thead th .us-table-cell {
        height: 20px !important;
        min-height: auto;
        line-height: 20px;
    }

    .us-table thead tr:first-child th {
        padding-top: 10px !important;
        padding-bottom: 0 !important;
    }

    .us-table th .us-table-cell {
        height: auto;
        line-height: normal;
    }

    .us-table-wrap {
        width: auto !important;
    }

    .us-table {
        width: auto !important;
        max-width: none !important;
        min-height: auto !important;
    }

    #container {
        background: #fff;
    }
</style>
<div id="app" v-cloak>
    <?php if (!in_array('/reporting/technician-order-count', $selfRoles)): ?>
        <div style="text-align: center;font-size:20px; font-weight: bold;">无权限</div>
    <?php else: ?>
    <section id="main-content">
        <div class="panel-body">
            <us-adv-search title="评价统计" placeholder="请输入技师姓名" @on-search="searchData" @on-input-search="inputData"
                           v-model="search.name">
                <div style="float: right;" slot="adv-header">
                    <us-button type="primary" @click="exportBtn()">导出</us-button>
                </div>
                <us-form label-whl="70px" item-whl="300px">
                    <us-form-row>
                        <us-form-item label="技师名称：">
                            <us-ipt-text whl="200px" v-model="search.name"></us-ipt-text>
                        </us-form-item>
                        <us-form-item label="评价日期：" whl="320px">
                            <us-ipt-datetime type="date" whl="200px"
                                             :format="['yyyy-MM']"
                                             v-model="search.month"></us-ipt-datetime>
                        </us-form-item>
                    </us-form-row>
                </us-form>
            </us-adv-search>
            <div class="table-list-wrap">
                <div class="us-table-wrap">
                    <table class="us-table" cellspacing="0">
                        <thead>
                        <tr>
                            <th rowspan="2" class="us-th bottom-none"><span class="us-table-cell">技师</span></th>
                            <th class="us-th bottom-none" rowspan="2"><span class="us-table-cell">综合评分</span>
                            </th>
                            <th class="us-th bottom-none"
                                v-for="item in tagsHeader" :colspan="item.colspan"><span
                                        class="us-table-cell">{{item.lable}}</span>
                            </th>
                        </tr>
                        <tr>
                            <th v-for="(item, index) in tagsList"
                                :class="{ 'us-th-rigth' : (index !=0 && (index === goods_len || index === normal_len || index === bad_len))}">
                                <span class="us-table-cell" :title="item">{{item}}</span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="item in dataList">
                            <td><span class="us-table-cell us-table-cell-ellipsis" :title="item.technician_name">{{item.technician_name}}</span></td>
                            <td><span class="us-table-cell">{{item.composite_score}}</span></td>
                            <td v-for="i in item.ext_json"><span class="us-table-cell">{{i}}</span></td>
                        </tr>
                        </tbody>
                    </table>
                    <div v-if="dataList.length <= 0" class="us-table-body-empty text-center">无数据</div>
                </div>
                <div class="pagination-align">
                    <us-pagination
                            @change="changePage"
                            :page-no="page.pageNo"
                            :total-page="page.totalPage"
                            :total="page.total"></us-pagination>
                </div>
            </div>
        </div>
    </section>
    <?php endif;?>
</div>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/technician-appraisal/index.js?v=<?php echo $v; ?>"></script>
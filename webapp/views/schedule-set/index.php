<?php
    use yii\widgets\LinkPager;
    use yii\helpers\Url;
    use yii\helpers\Html;
    $this->title = '排班管理';
    $this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    td>span{padding:5px 10px;}
    a{cursor: pointer;}
    .table>tbody>tr>td{position: relative;padding: 20px 10px;}
    td>.Validform_checktip{position: absolute;left: 20px;bottom: 0;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">班次设置</span>
    <div class="right-btn-box">
        <?php if(in_array('/schedule-set/add',$selfRoles)):?>
            <button class="btn btn-orange" id="addTd">添加</button>
        <?php endif;?>
        <a href="javascript:" class="btn bg-f7 layerCancel">返回</a>
    </div>
</div>
<section id="main-content">
    <div class="panel-body">

        <form action="/schedule-set/index" class="form-horizontal form-border" method="POST" id="schedule-set-form">
            <div class="table-responsive col-lg-10 col-md-12 col-sm-12" style="overflow: visible;">
                <table class="table table-bordered table-striped table-hover">
                    <thead bgcolor="#455971">
                    <tr>
                        <th width="200px" style="height:80px">排班名称</th>
                        <th width="134px">工作起始时间</th>
                        <th width="134px">工作结束时间</th>
                        <th width="134px">添加方式</th>
                        <th width="134px">背景颜色</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($data)) : ?>
                        <?php foreach ($data as $key => $val) : ?>
                            <tr class="change-time">
                                <td>
                                    <?php if ($val['is_default'] == 1) : ?>
                                        <?=$val['name'] ?>
                                    <?php else: ?>
                                        <input type="text" name="Edit[<?=$val['id']?>][name]" maxlength="5" value="<?=$val['name'] ?>">
                                    <?php endif; ?>
                                    
                                </td>
                                <?php if ($val['name'] != '休息'): ?>
                                    <td>
                                        <input type="hidden" name="Edit[<?=$val['id']?>][id]" value="<?=$val['id']?>">
                                        <input type="text" datatype="*" sucmsg=" " name="Edit[<?=$val['id']?>][start_time]" placeholder="00:00" value="<?=$val['start_time'] ?>" class="start_time"  onchange="timeFormat(this)">
                                    </td>
                                    <td>
                                        <input type="text" datatype="*" sucmsg=" " name="Edit[<?=$val['id']?>][end_time]" placeholder="00:00" value="<?=$val['end_time'] ?>" class="end_time" onchange="timeFormat(this)">
                                    </td>
                                <?php else: ?>
                                    <td>
                                        <input type="text" style="background: #CCCCCC" disabled="disabled" value="<?=$val['start_time'] ?>">
                                    </td>
                                    <td>
                                        <input type="text" style="background: #CCCCCC" disabled="disabled" value="<?=$val['end_time'] ?>" >
                                    </td>
                                <?php endif; ?>

                                <td>
                                    <?php if ($val['is_default'] == 1) : ?>
                                        <?='默认' ?>
                                    <?php else: ?>
                                        <?='自定义' ?>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if ($val['name'] == '休息'): ?>
                                        <span class="border-dedede" style="color: red">背景色</span>
                                    <?php elseif ($val['name'] == '全天班'): ?>
                                        <span class="border-dedede" >背景色</span>
                                    <?php else: ?>
                                        <span style="background: <?=$val['color'] ?>">背景色</span>
                                    <?php endif; ?>
                                    
                                </td>
                            </tr>
                        
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <p class="xian"></p>
            <div class="information-list center">
                <a  href="javascript:"  class="btn bg-f7 mgr20 layerCancel">取消</a>
                <button class="btn btn-orange" id="confirm">确认</button>
            </div>
        </form>
        
    </div>
</section>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    $(".tip").popover();
    var flag = true;
    var mark = false;
    var addName = [1,2,5,6,7,8,9]
    $('#addTd').on('click',function() {
        flag = false;
        mark = true;
        var number = addName[0];
        addName.splice(0,1);
        var addTr ="<tr class='change-time'>"
        +"<td><input type='text' datatype='*' sucmsg='' name='Add["+number+"][name]' maxlength='5' placeholder='5个字以内'></td>"
        + "<td><input type='text' datatype='*' sucmsg='' name='Add["+number+"][start_time]' value='08:00' class='start_time' onchange='timeFormat(this)'></td>"
        +"<td><input type='text' datatype='*' sucmsg='' name='Add["+number+"][end_time]'  value='18:00' class='end_time' onchange='timeFormat(this)'></td>"
        +"<td>自定义<input type='hidden' name='Add["+number+"][is_default]' value='2'></td>"
        +"<td class='custom-td'><div style='position:relative;width:100%;'><span>随机<input type='hidden' name='Add["+number+"][color]'></span><a src='javascript:' class='td-delete' onClick='deleteTr(this)' data-suzi='"+number+"'>删除</a></div></td>"
        +"</tr>";
        $('tbody').children().length>9?alert('最多添加5个'):$('tbody').append(addTr);
    })
    
    // 时间格式不正确,正确的格式为
    
    function timeFormat (el) {
        var number1 = Number($(el).val().slice(0,1));
        var number2 = Number($(el).val().slice(1,2));
        if(!/^[0-2]{1}[0-9]{1}:[0|3]{1}[0]{1}$/.test($(el).val()))
            {
                alert('仅支持输入整点或半点！');
                $(el).val('')
            };
            if(number1==2&&number2>3){
                alert('仅支持输入整点或半点！');
                $(el).val('')
            }
            
　
    }
    //对比前后时间
    $(".change-time").change(function () {
        var start = $(this).find(".start_time").val();
        var end = $(this).find(".end_time").val();

        if (start >= end) {
            alert('班次的结束时间必须大于起始时间，请重新设置后保存！');
            return false;
        }
        if(start.length>0 || end.length>0){
            mark=true;
        }
    });
    
    function deleteTr(el){
        addName.push(Number($(el).attr('data-suzi')));
        console.log(addName);
        $(el).parents('tr').remove();
    }
    function randomHexColor() { //随机生成十六进制颜色
        var hex = Math.floor(Math.random() * 16777216).toString(16);
        while (hex.length < 6) {
            hex = '0' + hex;
        }
        return '#' + hex;
    }


    var oldVal = 0;
    $('body').delegate('.end_time,.start_time','focus',function () {
        oldVal = $(this).val();
    });
    $('body').delegate('.end_time,.start_time','blur',function () {
        if(oldVal != $(this).val()){
            flag =false;
        }
    });

    //表单提交验证
    $("#schedule-set-form").Validform({
        tiptype:3,
        beforeSubmit:function(curform){
            //对比前后时间
            var time = $(".change-time");


            $(".change-time").each(function () {
                var start = $(this).find(".start_time").val();
                var end = $(this).find(".end_time").val();
                
                if (start >= end) {
                    alert('班次的结束时间必须大于起始时间，请重新设置后保存！');
                    return false;
                }
                
            });


            if (flag == false) {
                var index = layer.confirm('新增加的班次不可删除，只能修改。</br>原班次在修改后只更新当前日期之后的排班。</br></br>请谨慎确认是否保存修改？', {
                    btn: ['取消','确认保存'] //按钮
                }, function(){
                    parent.layer.close(index);//关闭弹出的子页面窗口
                }, function(){
                    flag = true;
                    $('#schedule-set-form').submit();
                });
                return false;
            }else{
                if(mark==false){
                    var index = layer.alert('未修改任何内容！');
                    location.href = 'javascript:history.back(-1)';
                    return false;
                }
            }

            $("#confirm").prop("disabled", true);
        },
    });

    //取消按钮
    $(".layerCancel").click(function () {
        if(flag == false)
        {
            layer.confirm('您已修改内容，是否取消保存？', {
                btn: ['取消保存','继续编辑'] //按钮
            }, function(){
                location.href = 'javascript:history.back(-1)';
            }, layer.close());
        }
        else{
            location.href = 'javascript:history.back(-1)';
        }
    });
    
    function shiftName(el){
        console.log($(el).parents('tr').find('span').text($(el).val()));
    }
</script>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$this->title = '产品管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">

<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        th{text-align: center;}
        td{position: relative;text-align: center;}
        td>.Validform_wrong{position: absolute;bottom: -20px;left: 10%;}
        .Validform_checktip{margin-left: 0}
        .drop-dw-layerbox span{padding-left: 4px;}
        td>.Validform_checktip{display: block;}
    </style>
</head>
<body>
<div class="jn-title-box no-margin-bottom">
     <span class="jn-title">查看产品信息</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>基本信息</h4>
                        <!--产品编号-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable" >
                                产品编号：
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                <input type="text" value="<?=isset($data['prod_no']) ? $data['prod_no'] : '' ?>" disabled class="form-control inputxt"/>
                            </div>
                        </div>

                        <!--产品名称-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable" >
                                产品名称：
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                <input type="text" value="<?=isset($data['prod_name']) ? $data['prod_name'] : ''; ?>" disabled class="form-control inputxt"/>
                            </div>
                        </div>

                        <!--产品类型-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable" >
                                产品类型：
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                                <input type="text" value="<?=isset($data['type_name']) ? $data['type_name'] : ''; ?>" disabled class="form-control inputxt"/>
                            </div>
                        </div>

                        <!--产品品牌-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable" >
                                产品品牌：
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                                <input type="text" value="<?=isset($data['brand_name']) ? $data['brand_name'] : ''; ?>" disabled class="form-control inputxt"/>
                            </div>
                        </div>

                        <!--产品分类-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable" >
                                产品类目：
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                                <input type="text" value="<?=isset($data['class_name']) ? $data['class_name'] : ''; ?>" disabled class="form-control inputxt"/>
                            </div>
                        </div>
                    
                        <!--产品型号-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable" >
                                产品型号：
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                <input type="text" value="<?=isset($data['model']) ? $data['model'] : ''; ?>" disabled class="form-control inputxt"/>
                            </div>
                        </div>

                        <!--质保期-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable">
                                质保期：
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                                <div class="three-linkage" style="width: 100%;">
                                    <div class="linkage-justify-list" style="width: 48%;">
                                        <input type="text" value="<?=isset($data['warranty_num']) ? $data['warranty_num'] : ''; ?>" disabled class="form-control inputxt"/>
                                    </div>
                                    <div  class="linkage-justify-list" style="width: 48%;">
                                        <input type="text" value="<?=isset($data['warranty_type_name']) ? $data['warranty_type_name'] : ''; ?>" disabled class="form-control inputxt"/>
                                    </div>
                                    <div class="justify_fix"></div>
                                </div>
                                <span class="error-span"></span>
                            </div>
                        </div>

                        <!--价格体系-->
                        <div class="col-md-12 col-sm-12 col-xs-12 information-list" style="height: auto;">
                            <label class="name-title-lable" >
                                价格体系：
                            </label>
                            <div class="addinput-list">
                                <table>
                                    <tr>
                                        <th width="20%">计量单位</th>
                                        <th width="15%">市场价</th>
                                        <th width="15%">建议进价</th>
                                        <th width="15%">最高进价</th>
                                        <th width="15%">建议售价</th>
                                        <th width="15%">最高售价</th>
                                    </tr>
                                    <tr>
                                        <!--计量单位-->
                                        <td>
                                            <?=isset($data['unit_name']) ? $data['unit_name'] : ''; ?>
                                        </td>
                                        <!--市场价-->
                                        <td>
                                            <?=isset($data['sale_price']) ? $data['sale_price'] : ''; ?>
                                        </td>
                                        <!--建议进价-->
                                        <td>
                                            <?=isset($data['purchase_price']) ? $data['purchase_price'] : ''; ?>
                                        </td>
                                        <!--最高进价-->
                                        <td>
                                            <?=isset($data['purchase_max_price']) ? $data['purchase_max_price'] : ''; ?>
                                        </td>
                                        <!--建议售价-->
                                        <td>
                                            <?=isset($data['suggest_sale_price']) ? $data['suggest_sale_price'] : ''; ?>
                                        </td>
                                        <!--最高售价-->
                                        <td>
                                            <?=isset($data['sale_max_price']) ? $data['sale_max_price'] : ''; ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>
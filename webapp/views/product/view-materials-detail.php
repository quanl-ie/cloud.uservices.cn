<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>"> -->
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/popupStyle.css');?>">
<!DOCTYPE html>
<html>
<head>
</head>
<!-- <style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4{
        padding-left: 0;
    }
    input[type=radio]{
        margin-top: 10px;
    }
    body{
        overflow-x: hidden;
        font-family: "微软雅黑";background: #fff;
    }
    table{
        border: 1px solid #ccc;
        width:90%;
        margin:0 auto;
    }
    th,td{
        border-right: 1px solid #ccc;
        text-align: center;height: 45px;
        font-family: "微软雅黑";
        font-size: 14px;

    }
    td{
        line-height: 2;
    }
    th{

        background: #f6fafe;
    }
    tr{
        border-bottom: 1px solid #ccc;
    }
    .range-td>div{
        border-top: 1px solid #ccc;
    }
    .range-td>div:nth-child(1){
        border-top: 0px solid #ccc;
    }
</style> -->
<body>
<div class="table-box90">
    <table>
         <tr>
            <th>产品编号</th>
            <th>产品名称</th>
            <th>产品类型</th>
            <th>产品品牌</th>
            <th>产品类目</th>
            <th>产品型号</th>
            <th>计量单位</th>
            <th>单价</th>
            <th>数量</th>
            <th>总价</th>
         </tr>
         <?php if(!empty($data['product_list'])){
            foreach($data['product_list'] as $key=>$val){
                ?>
                <tr>
                    <td><?php echo $val['prod_no']?></td>
                    <td><?php echo $val['prod_name']?></td>
                    <td><?php echo $val['type_name']?></td>
                    <td><?php echo $val['brand_name']?></td>
                    <td><?php echo $val['class_name']?></td>
                    <td><?php echo $val['model']?></td>
                    <td><?php echo $val['unit_name']?></td>
                    <td><?php echo $val['price']?></td>
                    <td><?php echo $val['num']?></td>
                    <td><?php echo $val['total_price']?></td>
                </tr>
                <?php
            }
         }else { ?>
            <tr>
                <td colspan="10">暂无数据</td>
            </tr>
            <?php
         }
         ?>
    </table>
    <span class="height34">合计数量：<?php echo $data['total_info']['total_num']?></span><br>
    <span class="height34">合计总价：<?php echo $data['total_info']['total_price']?></span>
</div>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
</body>
</html>
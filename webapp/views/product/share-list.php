<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    $this->title = '产品列表';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">

<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4{ padding-left: 0;}
    body{overflow-x: hidden;background: #fff;font-family: "微软雅黑"}
    table{border-collapse: collapse;border:1px solid #ccc;}
   .table-bordered>tbody>tr>td,.table-bordered>thead>tr>th{border-right:1px solid transparent;overflow: hidden;text-align: center;padding: 10px;font-size: 12px}
   .form-horizontal .form-group{margin:0;}
   /*.table-striped>tbody>tr:nth-child(odd)>td{background: #fff}*/
   input[type="radio"] + label.disabled-label::before{border-color: #ccc;cursor: not-allowed;}
</style>
<body>
<div class="row popup-boss">
    <form action="/product/share-list" class="form-horizontal form-border" id="form">
        <input type="hidden" id="type" value="<?=$type?>">
       <div class="table-responsive" style="width: 90%;margin:15px 0 0 30px;height: 65px">
                <div class="single-search">
                    <label class="search-box-lable">产品名称</label>
                    <div class="single-search-kuang1">
                        <input type="text" class="form-control" name="prod_name" maxlength="20" value="<?=isset($_GET['prod_name']) ? $_GET['prod_name'] : '' ?>">
                    </div>
                </div>
                <div class="single-search">
                    <label class="search-box-lable">产品编号</label>
                    <div class="single-search-kuang1">
                        <input type="text" class="form-control" name="prod_no" maxlength="20" value="<?=isset($_GET['prod_no']) ? $_GET['prod_no'] : '' ?>">
                    </div>
                </div>

                <div class="search-confirm">
                    <button class="btn btn-orange">
                        <i class=" "></i> 查询
                    </button>
                </div>
        </div>
        <div class="table-responsive"  style="width: 90%;margin:0 auto;">
            <table class="table table-bordered table-striped table-hover" style="width:100%;">
                <thead bgcolor="#f8f8f8" style="border-collapse: collapse;">
                <tr>
                    <th></th>
                    <th>产品编号</th>
                    <th>产品名称</th>
                    <th>产品类目</th>
                    <th>品牌</th>
                    <th style="border-right: 1px solid #ddd">产品型号</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($data)) : ?>
                    <?php foreach ($data as $key => $val) : ?>
                        <tr>
                            <td>
                                <input type="radio" name="prod_id" class="prod_id" value="<?=$val['id']?>" class="available_cost_type" id="bh<?=$val['id']?>"/>
                                 <label for="bh<?=$val['id']?>" style="cursor: pointer;"></label>
                            </td>
                            <td><?=isset($val['prod_no'])?$val['prod_no']:'' ?></td>
                            <td><?=isset($val['prod_name'])?$val['prod_name']:'' ?></td>
                            <td><?=isset($val['class_name'])?$val['class_name']:'' ?></td>
                            <td><?=isset($val['brand_name'])?$val['brand_name']:'' ?></td>
                            <td style="border-right: 1px solid #ddd"><?=isset($val['model'])?$val['model']:'' ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr><tr><td colspan="7" class="no-record">暂无数据</td></tr></tr>
                <?php endif; ?>
                </tbody>
            </table>
            <div class="col-xs-12 text-center  pagination">
                <?php echo $pageHtml;?>
            </div>
        </div>
    </form>

</div>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>

<script>
    var parentBox = parent.$('#materielList');
    var loop = parent.loop;//1，循环数据，2，不循环
    var prod_arr = [];
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
        parentBox.find('tr').each(function(i) {
            $('#bh'+$(this).data('id')).attr('disabled',true);
            $('#bh'+$(this).data('id')).next('label').addClass('disabled-label')
            if($(this).data('id')){
                prod_arr.push($(this).data('id'));
            }
            // if($(this).('data-id')!=""){
            //     $('#bh'+$(this).data('id')).attr('disabled',true);
            //     $('#bh'+$(this).data('id')).addClass('disabled-label');
            // }
        })
        parent.chlidMsg = '';
    };
    
    $("input[name='prod_id']").click(function(){
        //console.log(prod_arr); return false;
        var prodId = $(this).val();
        var html = '';
        var type = $("#type").val();
       $.ajax({
            url:'/product/share-detail?id='+prodId+'&type='+type+'&prod_arr='+prod_arr,
            type:'get',
            dataType:'json',
            success:function(msg){   
               if(msg.code==200){
                    parent.chlidMsg = msg.data;
                    var index = parent.layer.getFrameIndex(window.name); //关闭当前弹出层
                    parent.layer.close(index);
                }
            }
        })      
    })
</script>
</body>
</html>
<?php
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\helpers\Url;
    $this->title = '产品管理';
    $this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time();?>">
<style type="text/css">
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
    .search-box-kuang>.col-sm-6{
        padding-left: 0;
        position: relative;
    }
    .icons-calendar{
        position: absolute;
        right: 24px;top: 9px;
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">产品管理</span>
    <div class="right-btn-box">
        <?php if(in_array('/product/add',$selfRoles)){?>
            <a href="/product/add"><span class="btn btn-success" style=" color:#FFF;" >添加</span></a>
        <?php };?>
    </div>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/product/index" class="form-horizontal form-border" id="form">
                        <div class="form-group jn-form-box">
                            <div class="col-sm-12 no-padding-left">
                                <div class="single-search">
                                    <label class=" search-box-lable">产品名称</label>
                                    <div class="single-search-kuang1">
                                        <input type="text" class="form-control" name="prod_name" maxlength="20" value="<?=isset($_GET['prod_name']) ? $_GET['prod_name'] : '' ?>">
                                    </div>
                                </div>
                                <div class="single-search">
                                    <label class=" search-box-lable">产品编号</label>
                                    <div class="single-search-kuang1">
                                        <input type="text" class="form-control" name="prod_no" value="<?=isset($_GET['prod_no']) ? $_GET['prod_no'] : '' ?>">
                                    </div>
                                </div>
                                <div class="single-search">
                                    <label class=" search-box-lable">品牌</label>
                                    <div class="single-search-kuang1">
                                        <select class="form-control" name="brand_id">
                                            <option value="">请选择</option>
                                            <?php if (!empty($brand)) : ?>
                                                <?php foreach ($brand as $key => $val) : ?>
                                                    <option value="<?=$key; ?>" <?php if (isset($_GET['brand_id']) && $_GET['brand_id'] == $key): ?>selected="selected" <?php endif; ?>><?=$val; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>

                                        </select>
                                    </div>
                                </div>

                                <div class="single-search region-limit" style="overflow:visible;">
                                    <label class=" search-box-lable">类目</label>
                                    <div class="single-search-kuang1 region-limit" id="de">
                                        <input type="hidden" name="class_id" id="class_id" value="<?=isset($classId) ? $classId : ''?>">
                                        <input type="text" class="form-control" id="class_name" value="<?=isset($className) ? $className : '' ?>" placeholder="请选择类目" readonly onclick="clickCategory()">
                                        <div class="drop-dw-layerbox" style="display: none;">
                                            <div class="ul-box-h180">
                                                <ul id="depart1" class="depart">
                                                    <?php if (!empty($class)) : ?>
                                                        <?php foreach ($class as $key => $val) : ?>
                                                            <li>
                                                             <span>
                                                                <?php if (isset($val['exist']) && $val['exist'] == 1) : ?>
                                                                    <i class="icon2-shixinyou" onclick="getNextClass(this)"></i>
                                                                <?php endif;?>
                                                                 <input type="checkbox" id="a<?=$val['id'] ?>" value="<?=$val['id'] ?>" name="class_id[]" >
                                                                 <label for="a<?=$val['id'] ?>"><?=$val['class_name'] ?><i class="gou-i"></i></label>
                                                            </span>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                            <button class="btn btn-orange" id="belongedBtn">确定</button>
                                        </div>

                                    </div>

                                </div>
                                <div class="single-search">
                                    <label class=" search-box-lable">产品类型</label>
                                    <div class="single-search-kuang1">
                                        <select class="form-control" name="type_id">
                                            <option value="">不限</option>

                                            <option value="2" <?php if (isset($_GET['type_id']) && $_GET['type_id'] == 2) : ?>selected="selected"<?php endif; ?>>配件</option>
                                            <option value="1" <?php if (isset($_GET['type_id']) && $_GET['type_id'] == 1) : ?>selected="selected"<?php endif; ?>>成品</option>
                                            <option value="3" <?php if (isset($_GET['type_id']) && $_GET['type_id'] == 3) : ?>selected="selected"<?php endif; ?>>物料清单</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="single-search">
                                    <label class=" search-box-lable">状态</label>
                                    <div class="single-search-kuang1">
                                        <select class="form-control" name="status">
                                            <option value="">不限</option>
                                            <option value="1" <?php if (isset($_GET['status']) && $_GET['status'] == 1) : ?>selected="selected"<?php endif; ?>>启用</option>
                                            <option value="2" <?php if (isset($_GET['status']) && $_GET['status'] == 2) : ?>selected="selected"<?php endif; ?>>禁用</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="search-confirm">
                                    <button class="btn btn-success">
                                        <i class=" "></i> 搜索
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead bgcolor="#455971">
                                <tr>
                                    <th>产品编号</th>
                                    <th>产品名称</th>
                                    <th>产品品牌</th>
                                    <th>产品类目</th>
                                    <th>所属机构</th>
                                    <th>产品类型</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($data)) : ?>
                                    <?php foreach ($data as $key => $val) : ?>
                                    <tr>
                                        <td><?=$val['prod_no'] ?></td>
                                        <td><?=$val['prod_name'] ?></td>
                                        <td><?=$val['brand_name'] ?></td>
                                        <td><?=$val['class_name'] ?></td>
                                        <td><?=$val['department_name'] ?></td>
                                        <td><?=$val['type_name'] ?></td>
                                        <td><?=$val['status_name'] ?></td>
                                        
                                        <td>
                                            <?php if(in_array('/product/view',$selfRoles)){?>
                                                <a href="/product/view?id=<?=$val['id']?>">查看</a>
                                            <?php };?>
                                            <?php if ($val['role'] == 1) : ?>
                                            <?php if ($val['status'] == 1): ?>
                                                    <?php if(in_array('/product/edit',$selfRoles)){?>
                                                        <a href="/product/edit?id=<?=$val['id']?>">编辑</a>
                                                    <?php };?>
                                                    <?php if(in_array('/product/ajax-edit-status',$selfRoles)){?>
                                                        <a href="javascript:" onclick="editStatus(<?=$val['id']?>,2,0);" >禁用</a>
                                                    <?php };?>
                                            <?php elseif($val['status'] == 2): ?>
                                                    <?php if(in_array('/product/ajax-edit-status-on',$selfRoles)){?>
                                                        <a href="javascript:" onclick="editStatusOn(<?=$val['id']?>,1,2);" >启用</a>
                                                    <?php };?>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                            
                                        </td>
                                        
                                    </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr><tr><td colspan="8" class="no-record">您还没有添加过产品<a href="/product/add">立即添加</a></td></tr></tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo $pageHtml;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
    
    // 选中内容
    $('#belongedBtn').bind('click', function(event) {
        event.preventDefault();
        var id_array= [];
        var data_array=[];
        $('input[name="class_id[]"]:checked').each(function(){
            data_array.push($(this).next('label').text());
            id_array.push($(this).val());//向数组中添加元素
        });
        var idstr=id_array.join(',');//将数组元素连接起来以构建一个字符串
        var datatr = data_array.join('；');
        $('#class_name').val(datatr);
        $("#class_id").val(idstr);
        $('.drop-dw-layerbox').hide();
    });
    //获取下级分类
    function getNextClass(el) {
        var id = $(el).next('input').val();
        var _this = $(el).parent('span');
        $.getJSON('/common/ajax-get-class',{'pid':id,'type':1},function (json) {
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>"
                var data = json.data;
                delete data.flag;

                for (item in data)
                {
                    ulData +="<li><span>";
                    data[item].exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="getNextClass(this)"></i>':'';
                    ulData+='<input type="checkbox" value="'+data[item].id+'" name="class_id[]" id="class'+data[item].id+'"><label for="class'+data[item].id+'">'+data[item].class_name+'<i class="gou-i"></i></label></span></li>';
                }

                ulData +="</ul>";
                
                _this.parent('li').append(ulData);
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');

            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })
    }
    
    //禁用产品
    function editStatus(id,status,is_off) {
        $.post('/product/ajax-edit-status',{id:id,status:status,is_off:is_off},function(data) {
            var res = $.parseJSON(data);
            if(res.code == 20006){
                alert(res.message);
            }
            if(res.code == 20009){
                confirm('确认要禁用该产品信息吗?',function(){
                    StatusOff(id,status,1);
                },1);
            }
        })
        return false;
    }
    //确定禁用
    function StatusOff(id,status,is_off){
        $.post('/product/ajax-edit-status',{id:id,status:status,is_off:is_off},function(data) {
            var res = $.parseJSON(data);
            alert(res.message);
            setTimeout(function(){location.reload();},2000);
            //window.location.reload();
        })
    }

    //启用产品
    function editStatusOn(id,status,is_off) {
        var html;
        if (status == 1) {
            html = '确认将该产品信息启用吗?';
        }else{
            html = '确认要禁用该产品信息吗?';
        }

        layer.confirm(html, {icon: 3,}, function(index){
            $.ajax({
                type:'post',
                url:'/product/ajax-edit-status-on',
                data:{id:id,status:status,is_off:is_off},
                dataType:'json',
                success:function (data) {
                    if (data.code == 200){
                        // alert(data.message);
                        layer.alert(data.message, function(index){
                            window.location.reload();
                            layer.close(index);
                        });
                    }else{
                        alert(data.message);
                    }
                }
            });
            layer.close(index);
        });
    }

    function  clickCategory(el) {
        event.stopPropagation();
        $('.drop-dw-layerbox').is(':hidden')? $('.drop-dw-layerbox').show(): $('.drop-dw-layerbox').hide();
    }
    $(document).click(function(event) {
       var region = $('.region-limit');
       if (!region.is(event.target)&&region.has(event.target).length ===0) {
                $('.drop-dw-layerbox').hide();
       }
    });
</script>
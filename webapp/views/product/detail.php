<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$this->title = '产品管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">

<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        th{text-align: center;}
        td{position: relative;text-align: center;}
        td>.Validform_wrong{position: absolute;bottom: -20px;left: 10%;}
        .Validform_checktip{margin-left: 0}
        .drop-dw-layerbox span{padding-left: 15px;}
        td>.Validform_checktip{display: block;}
        .row select {
            border: 0;
            background: transparent;
            appearance:none;
            -moz-appearance:none; /* Firefox */
            -webkit-appearance:none; /* Safari 和 Chrome */
        }
        .information-list{margin-top: 0px;}
    </style>
</head>
<body>
<div class="jn-title-box no-margin-bottom">
     <span class="jn-title">查看产品信息</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <!-- <h4>查看产品信息</h4> -->
                    <?php
                    $form = ActiveForm::begin([
                            'options' => [
                                'class' => 'form-horizontal',
                                'id' => 'product-add',
                                'name' => 'form1',
                                'enctype' => 'multipart/form-data',
                            ]
                    ]);
                    ?>
                        <!--产品id-->
                        <input type="hidden" value="<?=isset($data->id) ? $data->id : ''; ?>" id="id" name="id">
                        <!--产品编号-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable" >
                                产品编号：
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8 addinput-list height34">
                                <?=isset($data->prod_no) ? $data->prod_no : '' ?>
                            </div>
                            <div class="Validform_checktip Validform_wrong"></div>
                        </div>

                        <!--产品名称-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable" >
                                产品名称：
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8 addinput-list height34">
                                <?=isset($data->prod_name) ? $data->prod_name : '' ?>
                            </div>
                            <div class="Validform_checktip Validform_wrong"></div>
                        </div>

                        <!--产品类型-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable" >
                                产品类型：
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                                <select style="cursor:default;padding-left:1px" class="col-md-4 col-sm-4 col-xs-4 form-control" disabled="disabled" id="type_id" name="type_id" datatype="*" sucmsg=" " nullmsg="请选择产品类型">
                                    <?php if (!empty($flag) && $flag == 1) : ?>
                                        <option value="2" <?php if (isset($data->type_id) && $data->type_id == 2) : ?>selected="selected"<?php endif; ?>>配件</option>
                                        <option value="1" <?php if (isset($data->type_id) && $data->type_id == 1) : ?>selected="selected"<?php endif; ?>>成品</option>
                                        <option value="3" <?php if (isset($data->type_id) && $data->type_id == 3) : ?>selected="selected"<?php endif; ?>>物料清单</option>
                                    <?php else: ?>
                                        <option value="2" <?php if (isset($data->type_id) && $data->type_id == 2) : ?>selected="selected"<?php endif; ?>>配件</option>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="Validform_checktip Validform_wrong"></div>
                        </div>
                        <!--产品品牌-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable" >
                                产品品牌：
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list height34">
                                <?php echo isset($brand[$data->brand_id])?$brand[$data->brand_id]:'';?>
                            </div>
                            <div class="Validform_checktip Validform_wrong"></div>
                        </div>
                        <!--产品分类-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable" for="workorder-account_id">
                                产品类目：
                            </label>
                            <span class="c_red"></span>
                            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list region-limit height34">
                                <?=$class_name ?>
                            </div>
                        </div>

                        <!--成品、配件-->
                        <div id="endProduct" style="display: block;">
                            <!--产品型号-->
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                                <label class="name-title-lable" >
                                    产品型号：
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-8 addinput-list height34">
                                    <?=isset($data->model) ? $data->model : '' ?>
                                </div>
                                <div class="Validform_checktip Validform_wrong"></div>
                            </div>

                            <!-- 关联物料清单 -->
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list auto-height">
                                <label class="name-title-lable" >
                                    关联物料清单：
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                    <ul id="relationMateriel">
                                        <?php if (isset($materials) && !empty($materials)) : ?>
                                        <?php foreach ($materials as $key => $val) : ?>
                                        <li class="line-height34">
                                            <div class="right-item-shanchu">
                                                <a href="javascript:" class="viewProductBag" id-data="<?=$key?>"><?=$val ?></a>
                                            </div>
                                        </li>
                                        <?php endforeach; ?>
                                        <?php endif; ?>

                                    </ul>
                                </div>
                                <div class="Validform_checktip Validform_wrong"></div>
                            </div>

                            <!--质保期-->
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                                <label class="name-title-lable">
                                    质保期：
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                                    <div class="three-linkage" style="width: 100%;">
                                        <div class="linkage-justify-list height34" style="width: 48%;">
                                            <?=isset($data->warranty_num) ? $data->warranty_num : '' ?><?=isset($warranty[$data->warranty_type]) ? $warranty[$data->warranty_type] : '' ?>
                                        </div>
                                        <div class="justify_fix"></div>
                                    </div>
                                    <span class="error-span"></span>
                                    <div class="Validform_checktip Validform_wrong"></div>
                                </div>
                            </div>

                            <!--价格体系-->
                            <div class="col-md-12 col-sm-12 col-xs-12 information-list" style="height: auto;">
                                <label class="name-title-lable" >
                                    价格体系：
                                </label>
                                <div class="addinput-list">
                                    <table>
                                        <tr>
                                            <th width="20%">计量单位</th>
                                            <th width="15%">市场价</th>
                                            <th width="15%">建议进价</th>
                                            <th width="15%">最高进价</th>
                                            <th width="15%">建议售价</th>
                                            <th width="15%">最高售价</th>
                                        </tr>
                                        <tr id="trPrice">
                                            <!--计量单位-->
                                            <td class="td-pst-relative">
                                                <?=isset($unit[$data->unit_id]) ? $unit[$data->unit_id] : '' ?>
                                            </td>
                                            <!--市场价-->
                                            <td class="td-pst-relative">
                                                <?=isset($data->sale_price) ? $data->sale_price : '' ?>
                                            </td>
                                            <!--建议进价-->
                                            <td class="td-pst-relative">
                                                <?=isset($data->purchase_price) ? $data->purchase_price : '' ?>
                                            </td>
                                            <!--最高进价-->
                                            <td class="td-pst-relative">
                                                <?=isset($data->purchase_max_price) ? $data->sale_price : '' ?>
                                            </td>
                                            <!--建议售价-->
                                            <td class="td-pst-relative">
                                                <?=isset($data->suggest_sale_price) ? $data->suggest_sale_price : '' ?>
                                            </td>
                                            <!--最高售价-->
                                            <td class="td-pst-relative">
                                                <?=isset($data->sale_max_price) ? $data->sale_max_price : '' ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="Validform_checktip Validform_wrong"></div>
                            </div>
                        </div>

                        <!--物料清单-->
                        <div id="materielBill" style="display: none;">
                            <!-- 计量单位 -->
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                                <label class="name-title-lable" >
                                    计量单位：
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-8 addinput-list height34">
                                    <?php echo isset($unit[$data->unit_id])?$unit[$data->unit_id]:'';?>
                                </div>
                                <div class="Validform_checktip Validform_wrong"></div>
                            </div>
                            <!-- 关联合同 -->
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list auto-height">
                                <label class="name-title-lable" >
                                    关联合同：
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                    <ul id="relationContract">
                                        <?php if (isset($contract) && !empty($contract)) : ?>
                                        <?php foreach ($contract as $key => $val) : ?>
                                        <li class="line-height34">
                                            <div class="right-item-shanchu"><a href="javascript:viod(0)" class="viewContractDetail" id-data="<?=$key ?>"><?=$val ?></a></div>
                                        </li>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <div class="Validform_checktip Validform_wrong"></div>
                            </div>
                            <!-- 物料列表 -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 information-list auto-height">
                                <label class="name-title-lable" >
                                    物料列表：
                                </label>
                                <div class="addinput-list">
                                    <table>
                                        <tr>

                                            <td>产品名称</td>
                                            <td>产品编号</td>
                                            <td>产品类型</td>
                                            <td>产品品牌</td>
                                            <td>产品类目</td>
                                            <td>产品型号</td>
                                            <td width="78px">计量单位</td>
                                            <td>单价</td>
                                            <td width="60px;">数量</td>
                                            <td>总价</td>
                                        </tr>
                                        <tbody id="materielList">
                                            <?php if (isset($prod) && !empty($prod)) : ?>
                                            <?php foreach ($prod as $key => $val) : ?>
                                            <tr class="tr1" id="<?=$val['id'] ?>" data-id='<?=$val['id'] ?>'>
                                                <td><?=isset($val['prod_name']) ? $val['prod_name'] : '' ?></td>
                                                <td><?=isset($val['prod_no']) ? $val['prod_no'] : '' ?></td>
                                                <td><?=isset($val['type_name']) ? $val['type_name'] : '' ?></td>
                                                <td><?=isset($val['brand_name']) ? $val['brand_name'] : '' ?></td>
                                                <td><?=isset($val['class_name']) ? $val['class_name'] : '' ?></td>
                                                <td><?=isset($val['model']) ? $val['model'] : '' ?></td>
                                                <td><?=isset($val['unit_name']) ? $val['unit_name'] : '' ?></td>
                                                <td class="sale-price"><?=isset($val['sale_price']) ? $val['sale_price'] : '' ?></td>
                                                <td><input style="border: none" disabled type="text" value="<?=isset($val['prod_num']) ? $val['prod_num'] : '' ?>"</td>
                                                <td class="total"><?=isset($val['total_price']) ? $val['total_price'] : '' ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                            <?php else: ?>
                                            <tr class="tr1">
                                                <td colspan="11">无</td>
                                            </tr>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 height34" style="padding-left: 0">
                                <label class="name-title-lable" >
                                    合计数量：
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                   <span class="height34" id='totalNum'></span>
                                </div>

                            </div>
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 height34" style="padding-left: 0">
                                <label class="name-title-lable" >
                                    合计总价：
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                    <span class="height34" id="totalPrice"></span>

                                </div>
                            </div>
                        </div>
                        <div  class="col-md-12" style="padding: 0"><hr style="padding-bottom: 0"></div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--<script src="/js/layer/layer.js"></script>-->
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
        if ($('#type_id').val() != 3) {
            $("#materielBill").hide();
            $("#endProduct").show();
        }else{
            $("#endProduct").hide();
            $("#materielBill").show();
        }
        calculation();
    }
    
    //价格数据格式化
    $(document).click(function(event) {
       var region = $('.region-limit');
       if (!region.is(event.target)&&region.has(event.target).length ===0) {
                $('.drop-dw-layerbox').hide();
       }
    });

    //失去焦补全
    $('#trPrice input[type="text"]').blur(function() {
        if($(this).val().length>0){
            var val = new Number($(this).val());
            $(this).val(val.toFixed(2));
        }
    });
    // 
    function calculation() {
        var totalNum = 0,total=0;
        $('#materielList tr').each(function() {

            var price=Number($(this).find('.sale-price').text());
            var num = Number($(this).find('input[type="text"]').val());
            $(this).find('.total').text(new Number(price*num).toFixed(2));
            total+=price*num
            totalNum +=num;
        })
        $('#totalPrice').text(total.toFixed(2));
        $('#totalNum').text(totalNum);
    }
    
    //查看配件包，物料清单
    $(".viewProductBag").click(function(){
        var prodId = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['查看物料清单', 'font-size:18px;'],
            area: ['800px', '500px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/product/materials-detail?id='+prodId,
        });
        return false;
    });

    //查看合同
    $(document).on('click','.viewContractDetail',function(e) {
        var id = $(this).attr("id-data");
        window.open('/contract/detail?id='+id);
    });

</script>
</body>
</html>
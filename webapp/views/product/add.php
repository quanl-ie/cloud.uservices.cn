<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$this->title = '产品管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">

<!DOCTYPE html>
<html> 
<head>
    <style type="text/css">
        th{text-align: center;}
        td{position: relative;text-align: center;}
        td>.Validform_wrong{position: absolute;bottom: -20px;left: 10%;}
        .Validform_checktip{margin-left: 0}
        .drop-dw-layerbox span{padding-left: 15px;}
        td>.Validform_checktip{display: block;}
    </style>
</head>
<body>
<div class="jn-title-box no-margin-bottom">
     <span class="jn-title">添加产品信息</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>基本信息</h4>
                    <?php
                    $form = ActiveForm::begin([
                            'options' => [
                                'class' => 'form-horizontal',
                                'id' => 'product-add',
                                'name' => 'form1',
                                'enctype' => 'multipart/form-data',
                            ]
                    ]);
                    ?>
                        <!--产品编号-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable" >
                                产品编号：
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                <input type="text" value="" name="prod_no" id="prod_no"  placeholder="请输入产品编号（50个字符）" maxlength="50" class="form-control inputxt" />
                            </div>
                            <div class="Validform_checktip Validform_wrong"></div>
                        </div>

                        <!--产品名称-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable" >
                                <i class="red-i">*</i>产品名称：
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                <input type="text" value="" name="prod_name" id="prod_name"  placeholder="请输入产品名称（30字以内）" sucmsg=" " nullmsg="请输入产品名称" maxlength="30" class="form-control inputxt" datatype="*" errormsg="不允许输入特殊字符"/>
                            </div>
                            <div class="Validform_checktip Validform_wrong"></div>
                        </div>
                    
                        <!--产品类型-->
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                            <label class="name-title-lable" >
                                <i class="red-i">*</i>产品类型：
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                                <select class="col-md-4 col-sm-4 col-xs-4 form-control" id="type_id" value="" name="type_id" datatype="*" sucmsg=" " nullmsg="请选择产品类型">
                                    <?php if (!empty($flag) && $flag == 1) : ?>
                                    <option value="">请选择产品类型</option>
                                    <option value="2">配件</option>
                                    <option value="1">成品</option>
                                    <option value="3">物料清单</option>
                                    <?php else: ?>
                                    <option value="2">配件</option>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="Validform_checktip Validform_wrong"></div>
                        </div>
                    <!--产品品牌-->
                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                        <label class="name-title-lable" >
                            <i class="red-i">*</i>产品品牌：
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                            <select class="col-md-4 col-sm-4 col-xs-4 form-control" id="brand_id" value="" name="brand_id">
                                <option value="">请选择品牌</option>
                                <?php if (!empty($brand)) : ?>
                                    <?php foreach ($brand as $key => $val) : ?>
                                        <option value="<?=$key; ?>"><?=$val; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="Validform_checktip Validform_wrong"></div>
                    </div>

                    <!--产品分类-->
                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i>产品类目：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-md-8 col-sm-8 col-xs-8  addinput-list region-limit">
                            <input type="text" class="form-control inputxt" placeholder="请选择产品类目" id="category1">
                            <p class="input-zhanwei"  id="categoryP"></p>
                            <i class="icon iconfont icon-shouqi1"></i>
                            <p class="misplacement" style="bottom: -5px;" id="work_type_msg">请选择产品类目</p>
                            <div class="drop-dw-layerbox">
                                <ul style="padding-left: 0">
                    
                                    <?php if (!empty($class)) : ?>
                                        <?php foreach ($class as $key => $val) : ?>
                                            <li>
                                                <span>
                                                    <?php if ($val['exist'] == 1) : ?>
                                                        <i class="icon2-shixinyou"  onclick="clickCategory(this) "></i>
                                                        <input type="radio" value="<?=$val['id'] ?>" name="class_id" id="<?=$val['id'] ?>" class="pr1" onclick="changeCategory(this)">
                                                    <?php else:?>
                                                        <input type="radio" value="<?=$val['id'] ?>" name="class_id" id="<?=$val['id'] ?>" class="pr1" onclick="changeCategory(this)">
                                                    <?php endif;?>
                                                    <label for="<?=$val['id'] ?>"><?=$val['class_name'] ?></label>
                                                </span>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                        <div id="endProduct" style="display: block;">
                            <!--产品型号-->
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                                <label class="name-title-lable" >
                                    产品型号：
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                    <input type="text" value="" name="model" id="model" placeholder="请输入产品型号" maxlength="50" class="form-control inputxt"  />
                                </div>
                                <div class="Validform_checktip Validform_wrong"></div>
                            </div>



                            <!-- 关联物料清单 -->
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list"  style="height: auto;">
                                <label class="name-title-lable" >
                                    关联物料清单：
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                    <ul id="relationMateriel" style="margin-bottom: 0;"></ul>
                                    <a href="javascript:" class="btn btn-orange get-materials" style="color: #fff;">点击添加</a>
                                </div>
                                <div class="Validform_checktip Validform_wrong"></div>
                            </div>
                            
                            <!--质保期-->
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                                <label class="name-title-lable">
                                    质保期：
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                                    <div class="three-linkage" style="width: 100%;">
                                        <div class="linkage-justify-list" style="width: 48%;">
                                            <input type="number" value="" name="warranty_num" id="warranty_num" placeholder="请输入整数" class="form-control inputxt" />
                                        </div>
                                        <div  class="linkage-justify-list" style="width: 48%;">
                                            <select class="col-md-4 col-sm-4 col-xs-4 form-control" id="warranty_type" value="" name="warranty_type" >
                                                <option value="">选择单位</option>
                                                <?php if (!empty($warranty)) : ?>
                                                    <?php foreach ($warranty as $key => $val) : ?>
                                                        <option value="<?=$key; ?>"><?=$val; ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                        <div class="justify_fix"></div>
                                    </div>
                                    <span class="error-span"></span>
                                    <div class="Validform_checktip Validform_wrong"></div>
                                </div>
                            </div>
                        
                            <!--价格体系-->
                            <div class="col-md-12 col-sm-12 col-xs-12 information-list" style="height: auto;">
                                <label class="name-title-lable" >
                                    价格体系：
                                </label>
                                <div class="addinput-list">
                                    <table>
                                        <tr>
                                            <th width="20%"><i class="red-i">*</i>计量单位</th>
                                            <th width="15%"><i class="red-i">*</i>市场价</th>
                                            <th width="15%">建议进价</th>
                                            <th width="15%">最高进价</th>
                                            <th width="15%">建议售价</th>
                                            <th width="15%">最高售价</th>
                                        </tr>
                                        <tr id="trPrice">
                                            <!--计量单位-->
                                            <td class="td-pst-relative">
                                                <select class="unit_id" value="" id="unit_id1" name="unit_id">
                                                    <option value="">请选择</option>
                                                        <?php if (!empty($unit)) : ?>
                                                            <?php foreach ($unit as $key => $val) : ?>
                                                                <option value="<?=$key; ?>"><?=$val; ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                </select>
                                            </td>
                                            <!--市场价-->
                                            <td class="td-pst-relative">
                                                <input type="text" value="" name="sale_price" id="sale_price" datatype="*" placeholder="请输入" sucmsg=" " errormsg="请输入市场价" nullmsg="请输入市场价" maxlength="11" onfocus="clearNoNum(this)"  onkeyup="clearNoNum(this)" />
                                            </td>
                                            <!--建议进价-->
                                            <td class="td-pst-relative">
                                                <input type="text" value="" name="purchase_price" id="purchase_price" placeholder="请输入" sucmsg=" " maxlength="11" onfocus="clearNoNum(this)"  onkeyup="clearNoNum(this)" />
                                            </td>
                                            <!--最高进价-->
                                            <td class="td-pst-relative">
                                                <input type="text" value="" name="purchase_max_price" id="purchase_max_price" placeholder="请输入" sucmsg=" " maxlength="11" onfocus="clearNoNum(this)"  onkeyup="clearNoNum(this)" />
                                            </td>
                                            <!--建议售价-->
                                            <td class="td-pst-relative">
                                                <input type="text" value="" name="suggest_sale_price" id="suggest_sale_price" placeholder="请输入" sucmsg=" " maxlength="11" onfocus="clearNoNum(this)"  onkeyup="clearNoNum(this)" />
                                            </td>
                                            <!--最高售价-->
                                            <td class="td-pst-relative">
                                                <input type="text" value="" name="sale_max_price" id="sale_max_price" placeholder="请输入" sucmsg=" " maxlength="11" onkeyup="clearNoNum(this)"/>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="Validform_checktip Validform_wrong"></div>
                            </div>
                        </div>
                        <div id="materielBill" style="display: none;">
                            <!-- 计量单位 -->
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                                <label class="name-title-lable" >
                                    <i class="red-i">*</i>计量单位：
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                    <select class="form-control unit_id" value="" id="unit_id2" name="unit_id">
                                        <option value="">请选择</option>
                                        <?php if (!empty($unit)) : ?>
                                            <?php foreach ($unit as $key => $val) : ?>
                                                <option value="<?=$key; ?>"><?=$val; ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                                <div class="Validform_checktip Validform_wrong"></div>
                            </div>
                            <!-- 关联合同 -->
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list"   style="height: auto;">
                                <label class="name-title-lable" >
                                    关联合同：
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                    <ul id="relationContract" style="margin-bottom: 0;">
                                        
                                    </ul>
                                   <a href="javascript:" class="btn btn-orange get-contract">点击添加</a>
                                </div>
                                <div class="Validform_checktip Validform_wrong"></div>
                            </div>
                            <!-- 物料列表 -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 information-list auto-height">
                                <label class="name-title-lable" >
                                    <i class="red-i">*</i>物料列表：
                                </label>
                                <div class="addinput-list">
                                    <table>
                                        <tr>
                                            <td class="padding15">
                                                <input type="checkbox" id="checkAll" disabled="disabled">
                                                <label for="checkAll"><i class="gou-i"></i></label>
                                            </td>
                                            <td>产品名称</td>
                                            <td>产品编号</td>
                                            <td>产品类型</td>
                                            <td>产品品牌</td>
                                            <td>产品类目</td>
                                            <td>产品型号</td>
                                            <td>计量单位</td>
                                            <td>单价</td>
                                            <td>数量</td>
                                            <td>总价</td>
                                        </tr>
                                        <tbody id="materielList">
                                            <tr class="tr1">
                                                <td colspan="11">请选择产品<a href="javascript:" class="get-share-list">立即添加</a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div id="productBtnBox" class="margin-top30" style="display: none">
                                        <a href="javascript:" class="btn btn-orange get-share-list mrg20">添加</a>
                                        <a href="javascript:" class="btn bg-f7" id="deleteProduct">删除</a>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 height34" style="padding-left: 0">
                                <label class="name-title-lable" >
                                    合计数量：
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                   <span class="height34" id='totalNum'></span>
                                </div>
                                
                            </div>
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 height34" style="padding-left: 0">
                                <label class="name-title-lable" >
                                    合计总价：
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                                    <span class="height34" id="totalPrice"></span>
                                    
                                </div>
                            </div>
                        </div>
                       
                        <div  class="col-md-12" style="padding: 0"><hr style="padding-bottom: 0"></div>
                        <div  class="information-list" style="margin-top:0;text-align: center;padding-bottom: 50px;">
                            <a href="/product/index"><button type="button" class="btn bg-f7 mgr20">取消</button></a>
                            <input type="button" id="btnSt" value="提交" class="btn btn-orange ">
                        </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--<script src="/js/layer/layer.js"></script>-->
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
        if ($('#type_id') != 3) {
            $("#materielBill").hide();
            $("#endProduct").show();
        }else{
            $("#endProduct").hide();
            $("#materielBill").show();
        }
    }
    
    $("#type_id").change(function () {
        if ($(this).val() == 3) {
            $("#materielBill").show();
            $("#endProduct").hide();
        }else{
            $("#endProduct").show();
            $("#materielBill").hide();
        }
    });
    
    //验证信息
    // $('#prod_no').blur(function() {
    //     var reg = /^[A-Za-z0-9-_\/\.]*$/;
    //     var str = $(this).val();
    //     if(str){
    //         if(!reg.test(str)){
    //             alert('产品编号格式不正确');
    //             $('#prod_no').val('');
    //             return false;
    //         }
    //     }
    // });
    $('#warranty_num').blur(function() {
        var reg = /^[0-9]*$/;
        var str = $(this).val();
        if(str){
            if(!reg.test(str)){
                alert('请输入正整数');
                $('#warranty_num').val('');
                return false;
            }
        }
    });
    
    $(".unit_id").blur(function () {
        if ($(this).val() == '') {
            alert('请选择计量单位');
            return false;
        }
    });

    $("#brand_id").blur(function () {
        if ($(this).val() == '') {
            alert('请选择品牌');
            return false;
        }
    });
    
    $("#class_id").blur(function () {
        if ($(this).val() == '') {
            alert('请选择产品类目');
            return false;
        }
    });
    
    //表单提交验证
    // $("#product-add").Validform({
    //     tiptype:3,
    //     beforeSubmit:function(curform){
    //         $("#btnSt").prop("disabled", true);
    //     },
    // });
    $("#btnSt").on('click',function(e) {
        e.preventDefault()
        var data = {
            prod_no:$('#prod_no').val(),//产品编号
            prod_name:$('#prod_name').val(),//产品名称
            type_id:$('#type_id').val(),//产品类型
            brand_id:$('#brand_id').val(),//品牌
            class_id:$('#category1').attr('data-id'),//类目
        }
        if(data.type_id==3){
            data.unit_id=$('#unit_id2').val();//计量单位
            data.contract=[];//关联合同
            data.prod=[];//物料列表
            $('#relationContract').find('.right-delete-a').each(function() {
                data.contract.push($(this).data('id'));
            });
            $('#materielList').find('tr').each(function() {
                if($(this).attr('class')!='tr1'){
                    var obj = {
                        prod_id:$(this).data('id'),
                        prod_num:$(this).find('input[type="text"]').val(),
                        prod_sale_price:$(this).find('.sale-price').text()
                    }
                    data.prod.push(obj);//产品
                }
            })
        }else{
            var newData = {
                model:$('#model').val(),//型号
                materials:[],//物料清单
                warranty_num :$('#warranty_num').val(),//质保期
                warranty_type:$('#warranty_type').val(),//质保单位
                unit_id:$('#unit_id1').val(),//计量单位
                sale_price:$('#sale_price').val(),//市场价
                purchase_price:$('#purchase_price').val(),//建议进价
                purchase_max_price:$('#purchase_max_price').val(),//最高采购价
                suggest_sale_price:$('#suggest_sale_price').val(),//建议售价
                sale_max_price:$('#sale_max_price').val(),//最高销售价
            }
            $('#relationMateriel').find('.right-delete-a').each(function() {
                newData.materials.push($(this).data('id'));
            });
            data = $.extend(data,newData);
        }
        $.ajax({
            url:'/product/add',
            type:'post',
            data:data,
            dataType:'json',
            success:function(data) {
                if (data.code == 200) {
                    alert(data.message);
                    location.href='/product/index';
                }else{
                    alert(data.message);
                    return false;
                }
            }
        })
    })
    //价格数据格式化
    function  clickCategory(el) {
        var id = $(el).next('input').val();
        var _this = $(el).parent('span')
        $.getJSON('/common/ajax-get-class',{'pid':id},function (json) {
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>";

                var data = json.data;
                delete data.flag;
                $.each(data,function(i,elm) {
                    ulData +="<li><span>";
                    elm.exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="clickCategory(this)"></i>':'';
                    ulData+='<input type="radio" value="'+elm.id+'" name="class_id" id="'+elm.id+'" class="pr1" onclick="changeCategory(this)"><label for="'+elm.id+'">'+elm.class_name+'</label></span></li>';
                })
                ulData +="</ul>";
                _this.parent('li').append(ulData);
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');

            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })

    }

    function changeCategory(el) {
        $('.drop-dw-layerbox').hide();
        $('#category1').val($(el).next('label').text());
        $('#category1').attr('data-id',$(el).attr('id'));
    }

    $('#categoryP').on('click',function() {
        $('.drop-dw-layerbox').is(':hidden')?$('.drop-dw-layerbox').show():$('.drop-dw-layerbox').hide();

    })
    $(document).click(function(event) {
       var region = $('.region-limit');
       if (!region.is(event.target)&&region.has(event.target).length ===0) {
                $('.drop-dw-layerbox').hide();
       }
    });

    //获取合同
    $(".get-contract").click(function () {
        layer.open({
            type: 2,
            title: '选择合同',
            area: ['800px', '450px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: '/contract/get-contract/',
            end:function () {
                //回调方法

            }
        });
    });

    //获取产品列表
    var chlidMsg='';
    $(document).on('click',".get-share-list",function () {
        layer.open({
            type: 2,
            title: '选择产品',
            area: ['800px', '450px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: '/product/share-list/',
            end:function () {
                if(chlidMsg!=""){
                    var html="";
                    var data =chlidMsg;
                    var trHtml ='<tr id="tr'+data.id+'" data-id="'+data.id+'"><td class="padding15"><input type="checkbox" id="product'+data.id+'" value="'+data.id+'"><label for="product'+data.id+'"><i class="gou-i"></i></label></td><td>'+data.prod_name+'</td><td>'+data.prod_no+'</td><td>'+data.type_name+'</td><td>'+data.brand_name+'</td><td>'+data.class_name+'</td><td>'+data.model+'</td><td>'+data.unit_name+'</td><td class="sale-price">'+data.sale_price+'</td><td><input type="text" value="1" onblur="calculation()"></td><td class="total"></td></tr>';
                    $('#materielList').find('.tr1').remove();
                    $('#productBtnBox').show();
                    $('#materielList').append(trHtml);
                    $("#checkAll").attr("disabled",false);
                    calculation();
                }
                
            }
        });
    });

    //获取物料清单列表
    var materielData = '';
    $(document).on('click',".get-materials",function () {
        layer.open({
            type: 2,
            title: '选择物料清单',
            area: ['800px', '450px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: '/product/materials/?type=',
            end:function () {
                //回调方法
                if(materielData!=''){
                    var ahtml = '<li class="line-height34"><div class="right-item-shanchu"><a href="javascript:void(0)" class="viewProductBag" id-data="'+materielData.id+'">'+materielData.prod_name+'</a><a href="javascript:" class="right-delete-a" data-id="'+materielData.id+'">删除</a></div></li>'
                    $('#relationMateriel').append(ahtml);
                }

            }
        });
    });
    //查看配件包，物料清单
    $(document).on('click','.viewProductBag',function(e) {
        var prodId = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['查看物料清单', 'font-size:18px;'],
            area: ['800px', '500px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/product/materials-detail?id='+prodId,
        });
        return false;
    })

    // 删除合同
    $(document).on('click','.right-delete-a',function(e) {
        $(this).parents('.line-height34').remove();
        // $(this).parents('ul').find('tr').
        $('#relationContract').hide();
        $('.get-contract').text('点击添加');  
    })
    $('.right-delete-a').each(function (i) {
        // body...
    })
    // 全选产品
    $('#checkAll').on('click',function () {
        var _that = $(this);
        $('#materielList tr').each(function() {
            if(_that.is(':checked')){
                $('#product'+$(this).data('id')).prop('checked',true);
            }else{
                $('#product'+$(this).data('id')).prop('checked',false);
            }
            
        })
    })
    //删除产品
    $("#deleteProduct").click(function(){
        ids = new Array();
        $("#materielList input[type='checkbox']").each(function(i){
            if( $(this).is(':checked')){
                var id = $(this).val();
                if(id>0){
                    ids[i] = id;
                }
            }
        })
        if(ids.length>0){
            confirm('确认删除产品吗?',function(){
                for(j=0;j<=ids.length;j++){
                    $("#tr"+ids[j]).remove();
                }
                if($('#materielList tr').length==0){$('#materielList').html('<tr class="tr1"><td colspan="11">请选择产品<a href="javascript:" class="get-share-list">立即添加</a></td></tr>');$('#productBtnBox').hide()}
                
                $("#checkAll").attr({"disabled":true,"checked":false});
                calculation();
            },this)
        }else{
            alert('请选择需要删除的产品');
        }
    })
    //失去焦补全
    $('#trPrice input[type="text"]').blur(function() {
        if($(this).val().length>0){
            var val = new Number($(this).val());
            $(this).val(val.toFixed(2));
        }
    });
    // 
    function calculation() {
        var totalNum = 0,total=0;
        $('#materielList tr').each(function() {
            if($(this).attr('class')!='tr1'){
                var price=Number($(this).find('.sale-price').text());
                var num = Number($(this).find('input[type="text"]').val());
                $(this).find('.total').text(new Number(price*num).toFixed(2));
                total+=price*num
                totalNum +=num;
            }else{
                total=0;
                totalNum =0;
            }
                
        })            
        
        $('#totalPrice').text(new Number(total).toFixed(2));
        $('#totalNum').text(totalNum);
    }
    
    //查看合同
    $(document).on('click','.viewContractDetail',function(e) {
        var id = $(this).attr("id-data");
        window.open('/contract/detail?id='+id);
    });
</script>
</body>
</html>
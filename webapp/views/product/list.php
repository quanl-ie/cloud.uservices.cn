<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    $this->title = '产品列表';
    $this->params['breadcrumbs'][] = $this->title;
?>

<!DOCTYPE html>
<html>
<head>
    <link href="/webuploader/webuploader.css" rel="stylesheet"><link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/public.css">
    <link rel="stylesheet" type="text/css" href="/css/iconfont.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/public/css/mystyle.css">

    <style type="text/css">
        .col-sm-4, .col-md-4, .col-lg-4{ padding-left: 0;}
        body{overflow-x: hidden;background: #fff;font-family: "微软雅黑"}
        table{border-collapse: collapse;border:1px solid #ccc;}
        .table-bordered>tbody>tr>td,.table-bordered>thead>tr>th{border-right:1px solid transparent;overflow: hidden;text-align: center;padding: 10px;font-size: 12px}
        .form-horizontal .form-group{margin:0;}
        /*.table-striped>tbody>tr:nth-child(odd)>td{background: #fff}*/
    </style>
</head>

<body>
<div class="row popup-boss">
    <form action="/product/get-list" class="form-horizontal form-border" id="form">
       <div class="table-responsive" style="width: 90%;margin:15px 0 0 30px;height: 65px">
                <div class="single-search">
                    <label class="search-box-lable">产品名称</label>
                    <div class="single-search-kuang1">
                        <input type="text" class="form-control" name="prod_name" maxlength="20" value="<?=isset($_GET['prod_name']) ? $_GET['prod_name'] : '' ?>">
                    </div>
                </div>
                <div class="single-search">
                    <label class="search-box-lable">产品编号</label>
                    <div class="single-search-kuang1">
                        <input type="text" class="form-control" name="prod_no" maxlength="20" value="<?=isset($_GET['prod_no']) ? $_GET['prod_no'] : '' ?>">
                    </div>
                </div>

                <div class="search-confirm">
                    <button class="btn btn-orange">
                        <i class=" "></i> 查询
                    </button>
                </div>
        </div>
        <div class="table-responsive"  style="width: 90%;margin:0 auto;">
            <table class="table table-bordered table-striped table-hover" style="width:100%;">
                <thead bgcolor="#f8f8f8" style="border-collapse: collapse;">
                <tr>
                    <th></th>
                    <th>产品编号</th>
                    <th>产品名称</th>
                    <th>产品类目</th>
                    <th>品牌</th>
                    <th style="border-right: 1px solid #ddd">产品型号</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($data)) : ?>
                    <?php foreach ($data as $key => $val) : ?>
                        <tr>
                            <td>
                                <input type="radio" name="prod_id" class="prod_id" value="<?=$val['id']?>" class="available_cost_type" id="bh<?=$val['id']?>"/>
                                 <label for="bh<?=$val['id']?>" style="cursor: pointer;"></label>
                            </td>
                            <td><?=isset($val['prod_no'])?$val['prod_no']:'' ?></td>
                            <td><?=isset($val['prod_name'])?$val['prod_name']:'' ?></td>
                            <td><?=isset($val['class_name'])?$val['class_name']:'' ?></td>
                            <td><?=isset($val['brand_name'])?$val['brand_name']:'' ?></td>
                            <td style="border-right: 1px solid #ddd"><?=isset($val['model'])?$val['model']:'' ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr><tr><td colspan="7" class="no-record">暂无数据</td></tr></tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </form>
    <div class="col-xs-12 text-center pagination">
        <?php if (!empty($pageHtml)) {echo $pageHtml;};?>
    </div>
</div>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>

<script>

    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
    

    $(".prod_id").click(function(){
        var prodId = $(this).val();
        var html = '';

        $.ajax({
            url:'/product/get-detail?id='+prodId,
            type:'get',
            success:function(msg){
                data = eval("("+msg+")");
                if(data.success==true){
                    parent.$("#product_hide").html(msg); //父页面接收json
                    var index = parent.layer.getFrameIndex(window.name); //关闭当前弹出层
                    parent.layer.close(index);
                }else{

                }
                alert('没有该产品');

            }
        })
    })
</script>
</body>
</html>




















<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = '换货详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
	.information-list {margin-top: 0;}
</style>

<div class="jn-title-box no-margin-bottom">
    <div class="dts-top">
        <span class="jn-title" id="no" data-no="<?php $info['no'];?>">换货详情</span>
        <div  class="right-btn-box">
            <!--<a href="" class="cancelOrder">
                <button class="btn btn-orange mgr8">打印</button>
            </a>-->
            <button class="btn bg-f7" onclick="javascript:history.back(-1);">返回</button>
        </div>
    </div>
</div>

<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body" style="padding: 0 30px 30px 50px">
                	<h5 class="block-h5" style="margin-top: 0">用户信息</h5>
                    <div class="information-list">
                        <div class="col-xs-12	col-sm-12	col-md-4	col-lg-3">
                        	<label  class="left-title80">换货编号：</label>
                            <input type="hidden" value="<?=$info['id']?>" id="id"/>
                        	<div class="right-title3"><span><?=$info['no']?></span></div>
                        </div>
                        <div class="col-xs-12	col-sm-12	col-md-4	col-lg-3">
                        	<label   class="left-title80">换货主题：</label>
                        	<div  class="right-title3"><span><?=$info['subject']?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">换货状态：</label>
                            <div  class="right-title3 2693ff"><span><?=$info['show_status']?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">换货原因：</label>
                            <?php if($info['exchange_reason']) :?>
                            <div class="right-title3"><span><?=$info['show_reason_status']?></span></div>
                            <?php else:?>
                            <div class="right-title3"><span>无</span></div>
                            <?php endif;?>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">创建人：</label>
                            <div class="right-title3"><span><?=$info['create_user']?></span></div>
                        </div>
                        <div class="col-xs-12  col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">换货日期：</label>
                            <div class="right-title3"><span><?=substr($info['exchange_date'],0,10)?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4   col-lg-3">
                            <label  class="left-title80">创建时间：</label>
                            <div class="right-title3"><span><?=$info['create_time']?></span></div>
                        </div>
                         <div class="col-xs-12  col-sm-12   col-md-12    col-lg-12">
                            <label   class="left-title80">关联客户：</label>
                            <div  class="right-title3"><span><?=$info['account']?></span></div>
                        </div>
                         <div class="col-xs-12  col-sm-12   col-md-12    col-lg-12">
                            <label   class="left-title80">关联合同：</label>
                            <div  class="right-title3">
                                <a href="/contract/detail?id=<?=$info['contract_id']?>&contract_id=<?=$info['contract_id']?>">
                                    <span><?=$info['contract_info']?></span>
                                </a>
                            </div>
                        </div>

                    </div>
					<h5 class="block-h5" style="margin-bottom: 20px">退货明细</h5>
					<div class="information-list" style="height: auto">
						<table>
							<thead>
								<tr>
									<th>产品名称</th>
									<th>产品类目</th>
									<th>品牌</th>
									<th>型号</th>
									<th>数量</th>
									<th>单价</th>
									<!--<th>折扣（%）</th>-->
									<th>总价</th>
									<th>折后总价</th>
								</tr>
							</thead>
							<tbody>
                            <?php foreach($detail as $v) :?>
                                <?php if($v['type']==1) :?>
								<tr>
									<td><?=$v['prod_name']?></td>
									<td><?=$v['class_name']?></td>
									<td><?=$v['brand_name']?></td>
									<td><?=$v['model']?></td>
									<td><?=$v['prod_num']?></td>
									<td><?=$v['sale_price']?></td>
									<!--<td><?/*=$v['rate']*/?>%</td>-->
									<td><?=sprintf('%0.2f',$v['sale_price']*$v['prod_num'])?></td>
									<td><?=$v['total_amount']?></td>

								</tr>
                                    <?php endif;?>
                            <?php endforeach;?>
							</tbody>
						</table>
					</div>

                    <h5 class="block-h5" style="margin-bottom: 20px">换货明细</h5>
                    <div class="information-list" style="height: auto">
                        <table>
                            <thead>
                            <tr>
                                <th>产品名称</th>
                                <th>产品类目</th>
                                <th>品牌</th>
                                <th>型号</th>
                                <th>数量</th>
                                <th>单价</th>
                                <!--<th>折扣（%）</th>-->
                                <th>总价</th>
                                <th>折后总价</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($detail as $v) :?>
                                <?php if($v['type']==2) :?>
                                    <tr>
                                        <td  style="max-width: 180px"><?=$v['prod_name']?></td>
                                        <td><?=$v['class_name']?></td>
                                        <td><?=$v['brand_name']?></td>
                                        <td><?=$v['model']?></td>
                                        <td><?=$v['prod_num']?></td>
                                        <td><?=$v['sale_price']?></td>
                                        <!--<td><?/*=$v['rate']*/?>%</td>-->
                                        <td><?=sprintf('%0.2f',$v['sale_price']*$v['prod_num'])?></td>
                                        <td><?=$v['total_amount']?></td>

                                    </tr>
                                <?php endif;?>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    <?php if($inList){?>
                        <h5 class="block-h5" style="margin-bottom: 20px">入库单</h5>
                        <div class="information-list" style="height: auto">
                            <table>
                                <thead>
                                <tr>
                                    <th>入库单号</th>
                                    <th>入库状态</th>
                                    <th>申请时间</th>
                                    <th>入库时间</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($inList as $key=>$val) {?>
                                    <tr>
                                        <td style="max-width: 180px">
                                            <a href="/stock-in/view?id=<?=isset($val['id'])?$val['id']:"" ?>"><?=$val['no']?></a>
                                        </td>
                                        <td><?=$val['status_desc']?></td>
                                        <td><?=$val['create_time']?></td>
                                        <td><?=$val['audit_date']?></td>
                                    </tr>
                                <?php };?>
                                </tbody>
                            </table>
                        </div>
                    <?php }?>
                    <?php if($outList){?>
                        <h5 class="block-h5" style="margin-bottom: 20px">出库单</h5>
                        <div class="information-list" style="height: auto">
                            <table>
                                <thead>
                                <tr>
                                    <th>出库单号</th>
                                    <th>出库状态</th>
                                    <th>出库产品数</th>
                                    <th>申请时间</th>
                                    <th>出库时间</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($outList as $key=>$val) {?>
                                    <tr>
                                        <td style="max-width: 180px">
                                            <a href="/stock-out/view?id=<?=isset($val['id'])?$val['id']:"" ?>"><?=$val['no']?></a>
                                        </td>
                                        <td><?=$val['status_desc']?></td>
                                        <td><?=$val['prod_sum']?></td>
                                        <td><?=$val['create_time']?></td>
                                        <td><?=$val['audit_date']?></td>
                                    </tr>
                                <?php };?>
                                </tbody>
                            </table>
                        </div>
                    <?php }?>
                    <?php if($sendList){?>
                        <h5 class="block-h5" style="margin-bottom: 20px">发货单</h5>
                        <div class="information-list" style="height: auto">
                            <table>
                                <thead>
                                <tr>
                                    <th>发货单号</th>
                                    <th>发货状态</th>
                                    <th>发货产品数</th>
                                    <th>申请时间</th>
                                    <th>发货时间</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($sendList as $key=>$val) {?>
                                    <tr>
                                        <td style="max-width: 180px">
                                            <a href="/send-out/view?id=<?=isset($val['id'])?$val['id']:"" ?>"><?=$val['send_no']?></a>
                                        </td>
                                        <td><?=$val['send_status_desc']?></td>
                                        <td><?=$val['prod_sum']?></td>
                                        <td><?=$val['create_time']?></td>
                                        <td><?=$val['rel_rec_date']?></td>
                                    </tr>
                                <?php };?>
                                </tbody>
                            </table>
                        </div>
                    <?php }?>


                    <h5 class="block-h5">物流信息</h5>
                    <div class="information-list">
                        <div class="col-xs-12	col-sm-12	col-md-8	col-lg-8">
                        	<label  class="left-title80">换货方式：</label>
                            <?php if($info['express_type']) :?>
                        	<div class="right-title3"><span><?=$info['show_express_type']?></span></div>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="information-list">
                        <div class="col-xs-12	col-sm-12	col-md-8	col-lg-8">
                        	<label  class="left-title80">换货地址：</label>
                        	<div class="right-title3"><span><?=$info['delivery_address']?></span></div>
                        </div>
                    </div>

                    <div class="information-list">
                        <div class="col-xs-12	col-sm-12	col-md-8	col-lg-8">
                        	<label  class="left-title80">备注：</label>
                        	<div class="right-title3"><span><?=$info['description']?></span></div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</section>


<!-- 设置同意拒绝弹层 -->
<div id="noLayer" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <p style="margin:0 0 10px 0;text-align: center;">确认拒绝审批换货吗？</p>
        <div class="select-fuji">
            <textarea placeholder="请填写审批意见，非必填" class="opinionContent" style="width: 100%;height: 100px;" onkeyup=" wordsLimit(this)"></textarea>
             <p class="xianzhi" style="right: 10px;"><span id="word">0</span>/200</p>
        </div>
        <div style="text-align: center;margin-top:15px;">
            <button class="btn" id="layerCancel" style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search" ></i>取消</button>
            <button class="btn btn-success" id="layerSubmit"  style="padding: 4px 12px" data-type="2"><i class="fa fa-search"></i>拒绝</button>
        </div>
    </div>
</div>
<div id="okLayer" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <p style="margin:0 0 10px 0;text-align: center;">确认同意审批换货吗？</p>
        <div class="select-fuji">
            <textarea placeholder="请填写审批意见，非必填" class="opinionContent" style="width: 100%;height: 100px;" onkeyup=" wordsLimit(this)"></textarea>
             <p class="xianzhi" style="right: 10px;"><span id="word">0</span>/200</p>
        </div>
        <div style="text-align: center;margin-top:15px;">
            <button class="btn" id="layerCancel" style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search" ></i>取消</button>
            <button class="btn btn-success" id="layerSubmit"  style="padding: 4px 12px" data-type="1"><i class="fa fa-search"></i>同意</button>
        </div>
    </div>
</div>


<script>
    var layerIndex = null;
    //no = $(this).attr('data-no');
    $(".setAudit").click(function () {
        var setType = $(this).attr('data-type');
        if(setType == 'ok'){
            var statusType = 2;
            layerIndex = layer.open({
                type: 1,
                title: '提示',
                area: ['400px', '300px'],
                fixed: true,
                maxmin: false,
                content: $('#okLayer').html()
            });
        }else {
            var statusType = 3;
            layerIndex = layer.open({
                type: 1,
                title: '提示',
                area: ['400px', '300px'],
                fixed: true,
                maxmin: false,
                content: $('#noLayer').html()
            });
        }
        return false;
    });
    //
    $("body").delegate("#layerSubmit","click",function(){
        var contractNo = $('#no').attr('data-no');
        var auditStatus = $(this).attr('data-type');
        var auditSuggest = $(this).parents('div').find('.opinionContent').val();

        if($.trim(auditStatus) == ''){
            alert('操作状态错误！');
            return false;
        }
        id = $("#id").val();
        $.ajax({
            url:'/exchange-goods/check-option',
            type:'POST',
            data:{id:id,auditStatus:auditStatus,auditSuggest:auditSuggest},
            datatype:'json',
            success:function (msg) {
                data = eval('('+msg+')');
                if(data.code == 200){
                    if(layerIndex){
                        layer.close(layerIndex);
                    }
                    alert('操作成功');
                    window.location.reload(true);
                }
                else {
                    alert(data.message);
                }
            }

        });
    });
    //关闭弹层
    $("body").delegate("#layerCancel","click",function(){
        if(layerIndex){
            layer.close(layerIndex);
        }
    });
</script>

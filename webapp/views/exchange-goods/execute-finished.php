<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '销售换货列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">销售换货列表</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body" style="font-size: 14px;">
                    <div class="col-md-12  col-lg-12">
                        <div style="width: 100%;" class="tab-group">
                            <ul id="myTab" class="nav nav-tabs">
                                <?php if(in_array('/exchange-goods/wait-check',$selfRoles)):?>
                                    <li><a href="/exchange-goods/wait-check">待审批</a></li>
                                <?php endif;?>
                                <?php if(in_array('/exchange-goods/executing',$selfRoles)):?>
                                    <li><a href="/exchange-goods/executing">执行中</a></li>
                                <?php endif;?>
                                <?php if(in_array('/exchange-goods/execute-finished',$selfRoles)):?>
                                    <li class="active"><a href="/exchange-goods/execute-finished">执行完毕</a></li>
                                <?php endif;?>
                                <?php if(in_array('/exchange-goods/unchecked',$selfRoles)):?>
                                    <li><a href="/exchange-goods/unchecked">审批未通过</a></li>
                                <?php endif;?>
                                <?php if(in_array('/exchange-goods/terminated',$selfRoles)):?>
                                    <li><a href="/exchange-goods/terminated">已关闭</a></li>
                                <?php endif;?>
                            </ul>
                        </div>

                        <div id="myTabContent" class="tab-content">
                            <!--搜索开始-->
                            <div class="form-group jn-form-box">
                                <form action="/exchange-goods/execute-finished" class="form-horizontal form-border" id="form">
                                    <div class="form-group jn-form-box">
                                        <!-- 搜索栏 -->
                                        <div class="col-sm-12 no-padding-left">

                                            <div class="single-search">
                                                <label class="search-box-lable">换货编号</label>
                                                <div class="single-search-kuang1">
                                                    <input type="text" class="form-control" name="no" value="<?php echo isset($_GET['no'])?$_GET['no']:'';?>">
                                                </div>
                                            </div>

                                            <div class="single-search">
                                                <label class="search-box-lable">换货主题</label>
                                                <div class="single-search-kuang1">
                                                    <input type="text" class="form-control" name="subject" value="<?php echo isset($_GET['subject'])?$_GET['subject']:'';?>">
                                                </div>
                                            </div>

                                            <div class="single-search">
                                                <label class="search-box-lable">客户姓名</label>
                                                <div class="single-search-kuang1">
                                                    <input type="text" class="form-control" name="account_name" value="<?php echo isset($_GET['account_name'])?$_GET['account_name']:'';?>">
                                                </div>
                                            </div>

                                            <div class="single-search">
                                                <label class="search-box-lable">客户电话</label>
                                                <div class="single-search-kuang1">
                                                    <input type="text" class="form-control" name="account_mobile" value="<?php echo isset($_GET['account_mobile'])?$_GET['account_mobile']:'';?>">
                                                </div>
                                            </div>
                                            <?php if(!empty($department)):?>
                                                <div class="single-search region-limit" style="overflow:visible;">
                                                    <label class=" search-box-lable">所属机构</label>
                                                    <input type="hidden" id="department_id" value="<?php echo $department_id;?>">
                                                    <div class="single-search-kuang1" id="de">
                                                        <input type="text" class="form-control" value="<?=isset($department_name) ? $department_name : '' ?>"  id="department"  readonly <?php if(!empty($department)){ echo 'onclick="clickCategory()" placeholder="请选择机构"';}?> >
                                                        <input type="hidden" class="form-control" value="<?=isset($params['department_id']) ? $params['department_id'] : '' ?>" id="department_ids" name="department_ids">
                                                        <div class="drop-dw-layerbox" style="display: none;">
                                                            <div class="ul-box-h180">
                                                                <ul id="depart1" class="depart">
                                                                    <?php if (!empty($department)) : ?>
                                                                        <?php foreach ($department as $key => $val) : ?>
                                                                            <li>
                                                             <span>
                                                                <?php if ($val['exist'] == 1) : ?>
                                                                    <i class="icon2-shixinyou" onclick="getDepartment(this)"></i>
                                                                <?php endif;?>
                                                                 <input type="checkbox" id="a<?=$val['id'] ?>" value="<?=$val['id'] ?>" name="department_id[]" <?php if(isset($params['department_id']) && (in_array($val['id'],explode(",",$params['department_id'])))){ echo "checked=true"; }?>>
                                                                 <label for="a<?=$val['id'] ?>"><?=$val['name'] ?><i class="gou-i"></i></label>
                                                            </span>
                                                                            </li>
                                                                        <?php endforeach; ?>
                                                                    <?php endif; ?>
                                                                </ul>
                                                            </div>
                                                            <button class="btn btn-orange" id="belongedBtn">确定</button>
                                                        </div>

                                                    </div>

                                                </div>
                                            <?php endif?>
                                             <div class="search-confirm">
                                                <button class="btn btn-success"><i class="icon iconfont icon-sousuo2"></i> 搜索</button>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--搜索结束-->
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead bgcolor="#455971">
                                    <tr>
                                        <th>换货编号</th>
                                        <th>换货主题</th>
                                        <th>所属机构</th>
                                        <th>客户姓名</th>
                                        <th>电话</th>
                                        <th>换货金额</th>
                              <!--          <th>操作</th>-->
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($data):?>

                                        <?php foreach ($data as $key=>$val):  ?>
                                            <tr>
                                                <td>
                                                    <?php if(in_array('/exchange-goods/detail',$selfRoles)){?>
                                                        <a href="/exchange-goods/detail?id=<?=$val['id']?>"><?=$val['no'] ? $val['no'] : '空'; ?></a>
                                                    <?php }else{?>
                                                        <?=$val['no'] ? $val['no'] : '空'; ?>
                                                    <?php }?>
                                                </td>
                                                <td><?=$val['subject'] ? $val['subject'] : '空'; ?></td>
                                                <td><?=$val['department_name'] ? $val['department_name'] : '空'; ?></td>
                                                <td><?=$val['account_name'] ? $val['account_name'] : '空'; ?></td>
                                                <td><?=$val['account_mobile'] ? $val['account_mobile'] : '空'; ?></td>
                                                <td><?=$val['total_amount']?></td>
                                            </tr>
                                        <?php endforeach;?>
                                    <?php else:?>
                                        <tr>
                                        <tr><td colspan="6" class="no-record">暂无执行完毕换货</td></tr>
                                        </tr>
                                    <?php endif;?>
                                    </tbody>
                                </table>
                            </div>

                            <div id="customer-product" class="tab-pane fade" ></div>
                            <div id="customer-address" class="tab-pane fade" ></div>
                        </div>

                    </div>
                    <div class="col-xs-12 text-center pagination">
                        <?php echo $pageHtml;?>
                    </div>
                </div>

            </div>

        </div>

    </div>
</section>
<script src="<?php echo Url::to('/js/mechanismChoice.js');?>"></script>
<script>
    $(".deletebtn").click(function () {
        var id = $(this).attr("data");
        confirm('确认删除吗',function(){
            $.ajax({
                type:'POST',
                url:'/exchange-goods/update-status',
                data:{id:id},
                success:function(msg){
                    str = data = eval('('+msg+')');
                    if(str.success==true){
                        alert('删除成功');
                        window.location.reload();
                    }else{
                        alert('删除失败');
                    }
                }
            })
        },this);

    })


    var departmentIdArray = $('#department_ids').val().split(',');
    function getDepartment(el) {
        var id = $(el).next('input').val();
        var _this = $(el).parent('span');
        var auth = <?php echo $auth;?>;
        var company_id = <?php echo $company_id;?>;
        var self_id = $("#department_id").val();
        $.getJSON('/common/ajax-get-department',{'pid':id,"auth":auth,'self_department_id':self_id,"company_id":company_id},function (data) {
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>"
                $(data.data).each(function(i,el) {
                    ulData +="<li><span>";
                    el.exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="getDepartment(this)"></i>':'';
                    ulData+='<input type="checkbox" id="a'+el.id+'" value="'+el.id+'" name="department_id[]" >  <label for="a'+el.id+'">'+el.name+'<i class="gou-i"></i></label></span></li>';
                });
                ulData +="</ul>";
                _this.parent('li').append(ulData);
                $.each(departmentIdArray,function(i,el) {
                    $('#a'+el).prop('checked',true);
                })
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');

            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })
    }
</script>
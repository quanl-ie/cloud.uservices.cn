<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = '添加销售换货';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">添加销售换货
<!--        <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="-->
<!--            选择客户：请在您所创建的客户中选择，选择客户后，系统会自动将该客户的联系方式及地址进行填充，如客户未创建，请点击右侧添加按钮进行创建<br/><br/>-->
<!---->
<!--选择产品：请在您所创建的客户产品中选择，选择产品后，系统会自动将产品类目、品牌、序列号等信息进行填充，如未创建产品，请点击右侧添加按钮进行创建<br/><br/>-->
<!---->
<!--质保状态：在创建订单时，请严格确认客户设备的质保状态来选择类型，以免影响服务价格<br/><br/>-->
<!---->
<!--派单方式：您可选择指定合作服务商或输入技师电话进行服务，也可通过优服务平台进行派单，服务商需要通过合作申请后方可选择-->
<!--        "></span>-->
    </span>
</div>
<!--引入CSS-->
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/webuploader/webuploader.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jquery.autocomplete.css');?>">
<!--引入JS-->
<?=Html::jsFile('@web/webuploader/webuploader.js')?>
<?=Html::jsFile('@web/js/bootstrap-select.js')?>
<style>
    .datexz-wap{
        top: 37px;
    }
    .form-control[disabled], .form-control[readonly]{
        background: #fff;border: 1px solid  #ccc;cursor:pointer;
    }
    .popover{max-width: 500px !important;}
    .radio-li-boss{height: 37px}ul{margin-bottom: 0}
    h4{font-size: 20px;font-family: "微软雅黑"}
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body"><?php
                    $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'form-horizontal',
                            'id' => 'exchange_goods_add',
                            'name' => 'form1',
                            'enctype' => 'multipart/form-data',
                        ],
                        'fieldConfig' => [
                            'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-xs-6\">{input}{error}</div>",
                            'labelOptions' => ['class' => 'col-xs-2 control-label text-right'],
                        ]
                    ]);
                    ?>
                    <div class="col-md-12">
                        <h5 class="block-h5" style="padding-top: 10px">用户信息</h5>
                    </div>
                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 关联用户：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <span class="line-height34"><?=$contract['account_name']?></span>
                            <!-- <input type="text" name="" class="form-control" maxlength="30" id="account_name" datatype="*" sucmsg=" " nullmsg="请输入客户姓名/电话……"  placeholder="请输入客户姓名/电话……">-->
                            <input type="hidden" class="form-control" id="account_name"   value="<?=$contract['account'].'/'.$contract['mobile']?>" accept="">
                            <input type="hidden" name="account_id" class="form-control" id="account_id"  value="<?=$contract['account_id']?>" accept="">
                            <input type="hidden" name="contract_id" class="form-control" id="contract_id"  value="<?=$contract['id']?>" accept="">
                            <p class="misplacement"  id="account_msg"></p>
                        </div>


                        
                    </div>
                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 换货主题：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <input type="text" name="subject" class="form-control" maxlength="20" id="subject"  datatype="*" sucmsg=" " nullmsg="请输入换货主题（20字以内）"  placeholder="请输入换货主题（20字以内）">
                            <p class=" misplacement"  id="subject_msg"></p>
                        </div>
                        
                    </div>

                    <div class="information-list" id="dataReceipt">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 换货日期：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <input type="text" name="exchange_date" class="form-control enYMD"  readonly id="exchangeDate">
                            <i class="icon2-rili riqi1"></i>
                            <p class="misplacement"  id="collection_date_msg"></p>
                        </div>


                    </div>


                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i"></i> 换货原因：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <select name="exchange_reason" class="form-control">
                                <option value="0">请选择</option>
                                <?php foreach($contract['reason'] as $k=>$v) :?>
                                    <option value="<?=$k?>"><?=$v?></option>
                                <?php endforeach;?>
                            </select>
                            <p class=" misplacement"  id="return_reason_msg"></p>
                        </div>
                        
                    </div>

                    <div class="col-md-12">
                         <h5 class="block-h5">退货产品</h5>
                    </div>
                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i"></i> 关联合同：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <a href="JavaScript:void(0)"
                               onclick='openPage("/contract/detail?id=<?=$contract['id']?>&contract_id=<?=$contract['id']?>","合同详情")'>
                                <span class="line-height34"><?=$contract['subject']?></span>
                            </a>
                            <input name="contract_id" type="hidden" value="<?=$contract['id']?>" />
                            <p class=" misplacement"  id="return_reason_msg"></p>
                        </div>
                    </div>

                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 是否需要入库：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <div class="roundRadio select-Zhouqi">
                                <input type="radio" class="available_cost_type" name="need_stock_in" value="1" id="available_cost_type1" checked="checked">
                                <label for="available_cost_type1">是</label>
                            </div>
                            <div class="roundRadio  select-Zhouqi">
                                <input type="radio" class="available_cost_type" name="need_stock_in" value="2" id="available_cost_type2">
                                <label for="available_cost_type2">否</label>
                            </div>
                            <p class=" misplacement"  id="collection_status_msg"></p>
                        </div>
                    </div>

                    <div class="information-list" style="height: auto">
                        <div class="col-xs-12 addinput-list" style="margin-left: 0;margin-bottom: 20px;">
                            <table>
                                <thead>
                                <tr>
                                    <th style="width: 50px;"  class="padding15">
                                        <input type="checkbox" name="" id="allDetailed1">
                                        <label for="allDetailed1"> <i class="gou-i"></i></label>
                                    </th>
                                    <th>产品名称</th>
                                    <th>产品类目</th>
                                    <th>品牌</th>
                                    <th>型号</th>
                                    <th>数量</th>
                                    <th>单价</th>
                                    <th>折扣（%）</th>
                                    <th>总价</th>
                                    <th>折后总价</th>
                                    <th>质保终止日期</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($contractDetail)) :?>
                                <?php foreach($contractDetail as $v) :?>
                                    <tr id="<?=$v['id']?>">
                                        <td class="padding15">
                                            <input type="checkbox"  name="contractDetail[<?=$v['id']?>][id]"  class="available_cost_type return_product" id="contractDetail<?=$v['prod_id']?>" value="<?=$v['id']?>">
                                            <label for="contractDetail<?=$v['prod_id']?>"> <i class="gou-i"></i></label>
                                            <!--<input type="hidden"    name="contractDetail[<?/*=$v['id']*/?>][id]"    value="<?/*=$v['id']*/?>">-->
                                            <input type="hidden"    name="contractDetail[<?=$v['id']?>][prod_id]"    value="<?=$v['prod_id']?>">
                                        </td>
                                        <td style="max-width: 180px"><?=$v['prod_name']?></td>
                                        <td><?=$v['class_name']?></td>
                                        <td><?=$v['brand_name']?></td>
                                        <td><?=$v['model']?></td>
                                        <td><input name="contractDetail[<?=$v['id']?>][prod_num]" class="prod_num" value="<?=$v['total_num']?>" />
                                            <input type="hidden" class="last_num" value="<?=$v['total_num']?>"/>
                                        </td>
                                        <td><?=$v['sale_price']?> <input class="price"     name="contractDetail[<?=$v['id']?>][sale_price]"   type="hidden" size="10" value="<?=$v['sale_price']?>" onkeyup="clearNoNum(this)"/></td>
                                        <td><?=$v['rate']?><input class="rate"     type="hidden"  name="contractDetail[<?=$v['id']?>][rate]"  maxlength="3" value="<?=$v['rate']?>"/></td>
                                        <td><span class="amount"><?=$v['amount']?></span><input     class="amount_hidden" name="contractDetail[<?=$v['id']?>][amount]"  type="hidden" value="<?=$v['amount']?>"/></td>
                                        <td><span class="total_amount"><?=$v['total_amount']?></span><input     class="total_amount_hidden" name="contractDetail[<?=$v['id']?>][total_amount]"   type="hidden"  value="<?=$v['total_amount']?>"/></td>
                                        <td>

                                            <input type="text" name="contractDetail[<?=$v['id']?>][warranty_date]" class="form-control enYMD warranty_date"  readonly >

                                        </td>
                                        <input type="hidden" name="contractDetail[<?=$v['id']?>][type]" value="1" />
                                    </tr>
                                <?php endforeach;?>
                                    <?php else :?>
                                    <td colspan="11">暂无可换产品</td>
                                <?php endif;?>
                                </tbody>
                            </table>
                            <p class=" misplacement"  id="return_msg"></p>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <h5 class="block-h5">换货产品</h5>
                    </div>
                    <div class="information-list">
                        <div class="col-xs-12 addinput-list" style="margin-left: 0;margin-bottom: 20px;">
                            <table style="width: 100%;font-size: 12px;">
                                <thead>
                                    <tr style="font-size: 14px;">
                                       <th style="width: 50px;" class="padding15">
                                            <input type="checkbox" name="" id="allDetailed">
                                            <label for="allDetailed"> <i class="gou-i"></i></label>
                                        </th>
                                       <th style="width: 10%;">产品名称</th>
                                       <th>产品类目</th>
                                       <th style="min-width: 80px;">品牌</th>
                                       <th style="width: 10%;">型号</th><th style="width: 60px;">数量</th><th>单价</th><th style="width: 80px;">折扣(%)</th><th style="min-width: 50px;">总价</th><th style="width: 80px;">折后总价</th><th>质保终止日期</th>
                                    </tr>
                                </thead>
                                <tfoot  id="show_prodcut"  >
                                    <!-- <tr><td colspan="11">请选择产品<a id="addBtn">立即添加</a></td></tr> -->
                                </tfoot>
                                <div id="product_hide" style="display:none;"></div>
                            </table>
                            <div class="bottom-btn-box">
                                 <a href="javascript:" class="btn btn-orange mgr8" id="addBtn" style="color: #fff;">添加</a>
                                 <a class="btn bg-f7 delet-btn" href="javascript:">删除</a>
                            </div>
                        </div>
                        
                        <div class="col-md-offset-2 col-md-6 misplacement"  style="display:none" id="product_msg">
                        </div>

                    </div>


                    <div class="col-md-12">
                        <h5 class="block-h5">收货信息</h5>
                    </div>

                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i"></i> 收货方式：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                            <select name="express_type" class="form-control">
                                <option value=null>请选择收货方式</option>
                                <option value="1" <?php if($contract['express_type']==1){echo 'selected';}?>>带货安装</option>
                                <option  value="2"  <?php if($contract['express_type']==2){echo 'selected';}?>>客户自提</option>
                                <option  value="3"  <?php if($contract['express_type']==3){echo 'selected';}?>>物流运输</option>
                            </select>
                            <p class="misplacement" style="display:none" id="subjectct_msg"></p>
                        </div>

                        
                    </div>
                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i"></i> 收货地址：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-xs-6 addinput-list">
                            <select type="text" class="form-control" name="address_id" id="account_address">
                                <?php if(isset($address)):?>
                                <?php else: ?>
                                    <option value="0">请选择客户</option>
                                <?php endif;?>
                                <?php if(isset($address)):?>
                                <?=$address?>
                                <?php endif;?>
                            </select>
                            <input name="delivery_address" id="delivery_address" type="hidden" value=" "/>
                        </div>
                        <div class="col-lg-1 col-md-2 col-sm-2 col-xs-2 add-btn">
                            <span class="btn btn-success  " style=" color:#FFF;float:right;font-size:14px; " id="addAccountAddres"  data-toggle="modal" data-target="#scrollingModal" title="<?php if(isset($contract['account_id'])) echo $contract['account_id'];?>">添加</span>
                        </div>
                        <div class="col-md-offset-2 col-md-6 misplacement" style="display:none"  id="address_msg">
                        </div>
                    </div>

                        <input  type="hidden" name="total_amount" id="all_total" value="">

                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            备注：
                        </label>
                        <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7 addinput-list" style="margin-bottom: 30px;">
                            <textarea class="ramark-box" id="remarks" maxlength="200" placeholder="请输入备注信息（200字以内）" name="express_description" id="express_description"><?php if(isset($data['description'])):?><?=$data['description']?><?php endif;?></textarea>
                            <p class="xianzhi"><span  id='word'>0</span>/200</p>
                        </div>
                    </div>
                    <hr style="clear: both;">
                    <div class="information-list" style="text-align: center;">
                                <input type="submit" class="btn btn-orange mgr20 btnSubmit"  value="提交"  />
                                <input type="button" class="btn btn-orange mgr20 btnSubmiting" value="提交中..."  style="display: none" />
                                <a href="javascript:void(0);" onclick="history.back();"><button type="button" class="btn bg-f7 ">返回</button></a>
                           
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- 时间插件/ -->
<?=Html::jsFile('@web/js/WdatePicker/WdatePicker.js')?>
<?=Html::jsFile('@web/js/WdatePicker/time.js')?>
<script type="text/javascript" src="/js/jquery-migrate-1.1.0.js"></script>
<script src="/js/jquery.autocomplete.js" type="text/javascript"></script>
<script>
    function openPage(url,title) {
        window.parent.postMessage(JSON.stringify({
            url: url,
            title: title,
        }), "*")
    }
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    }

    var myDate = new Date();
    var myMonth = myDate.getMonth()<10?"0"+(myDate.getMonth()+1):myDate.getMonth();
    var myDay = myDate.getDate()<9?'0'+myDate.getDate():myDate.getDate();
    var minDate = myDate.getFullYear()+'-'+myMonth+'-'+myDay;


    jeDate('#exchangeDate', {
        format: 'YYYY-MM-DD',
        maxDate: '2099-06-16 23:59:59'
    })

    $('body').on('click','.warranty_date',function(){
        jeDate('.warranty_date', {
            format: 'YYYY-MM-DD',
            maxDate: '2099-06-16 23:59:59'
        })

    })



    // 字数限制
    $("#remarks").keyup(function(){
        if($("#remarks").val().length > 200){
            $("#remarks").val( $("#remarks").val().substring(0,200) );
        }
        $("#word").text( $("#remarks").val().length ) ;
    });
    //单项选择
    $('.radio-li').on('click',function() {
        $(this).find('span').css({'background':'#2693ff','color':'#fff'});
        $(this).siblings('li').find('span').css({'background':'#fff','color':'#333','border-color':'#ccc'});
        $(this).parent('ul').next('input').val($(this).find('span').attr('id'));
    })
    // 选择
    $('.xiangxia').on('click',function() {
        $('.addinput-list-data').hide();
        $(this).next('.addinput-list-data').show();
    })
    //
    $('.addinput-list-data li').on('click',function() {
        $('.addinput-list-data').hide();
        $(this).parent('ul').siblings('.form-control').val($(this).attr('id '));
    })


    //添加客户
    var iframeSrc = '';
    $("#addAccount").click(function () {
        iframeSrc = 'order';
        layer.open({
            type: 2,
            title: '添加客户',
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/account/add/'
        });
    });
    //添加客户地址
    var iframeCallbackAddressId = 0;
    $("#addAccountAddres").click(function() {
        var id = $(this).attr('title');
        if(id==null || id==undefined || id=='' || isNaN(parseInt(id))) {
            $("#address_msg").text("请选择客户");
            $("#address_msg").show();
        }else {
            layer.open({
                type: 2,
                title: '添加客户地址',
                area: ['700px', '450px'],
                fixed: false, //不固定
                maxmin: false,
                content: '/account-address/add?id='+id,
                end  : function(){
                    account_address(id)
                }
            });
            account_address(id)
        }
    });
/*    //添加服务产品
    var iframeCallbackSaleOrderId = 0;
    $("#addAccountBrand").click(function() {
        var id = $(this).attr('title');
        if(id==null || id==undefined || id=='' || isNaN(parseInt(id))) {
            $("#brand_msg").text("请选择客户");
            $("#brand_msg").show();
        }else {
            layer.open({
                type: 2,
                title: '添加服务产品',
                area: ['700px', '600px'],
                fixed: false, //不固定
                maxmin: false,
                content: '/sale-order/add?accountId='+id,
                end  : function(){
                    service_brand(id)
                }
            });
            service_brand(id)
        }
    });*/

    /************************************ajax获取客户相关信息********************************************/
    var iframeCallbackSaleOrderId = 0;

        //模糊查询选中
    var accountId = 0;
    function accountName(id) {

        $("#brand_msg").hide();
        $("#address_msg").hide();
        $("#provide_msg").hide();

        accountId = id;
        $("#account_id").val(id);
        $("#addAccountAddres").attr('title',id);
        $("#addAccountBrand").attr('title',id);
        $("#account_select").hide();
        account_address(id);
        service_brand(id);
    }
    //查询客户对应的地址信息
    function account_address(account_id) {
        $.post('/order/get-address',{account_id:account_id},function(res) {
            $("#account_address").html(res);
            if(iframeCallbackAddressId>0){
                $("#account_address").val(iframeCallbackAddressId);
            }
        })
    }
    //查询客户对应服务产品
    function service_brand(account_id) {
        $.post('/order/get-service-brand',{account_id:account_id},function(res) {
            $("#account_service_brand").html(res);
            if(iframeCallbackSaleOrderId > 0 ){
                $("#account_service_brand").val(iframeCallbackSaleOrderId);
                $("#account_service_brand").change();
            }
        })
    }

    /***********************     END       ************************/
    /**********************   提交信息验证   ************************/



    //获取焦点提示框消失

    $("#subject").focus(function(){
        $("#subject_msg").text("");
        $("#subject_msg").hide();
    })
    $("#exchangeDate").focus(function() {
        $("#time_msg").text("");
        $("#time_msg").hide();
    })


    $(".return_product").change(function(){
        if(this.checked){
            $("#return_msg").css("display",'none');
            $("#return_msg").html('');

        }
    })






    //填充用户信息
    function fillAccount($accountId,$accountName) {
        $("#account_name").val($accountName);
        $('#account_id').val($accountId);
        accountName($accountId);
    }

    $("#account_address").click(function() {
        if(( $.trim($("#account_address").html()) == '')){
            $("#address_msg").text("请先选择客户");
            $("#address_msg").show();
        }else{
            var accountAddress = $("#account_address").find("option:selected").text();
            var accountName = $("#account_name").val();
            $("#delivery_address").val(accountName+'/'+accountAddress);
        }

    });



  //添加服务产品
$("#addBtn").click(function(){
    layer.open({
        type: 2,
        title: '添加服务产品',
        area: ['800px', '400px'],
        fixed: false, //不固定
        maxmin: false,
        content: '/product/get-list',
      end:function(){
            var str = $("#product_hide").html();
            data = eval("("+str+")");
           prod_id  =data.data.id;
            html = //'<td>'+data.data.id+'<td>'+
                '<td class="padding15"><input type="checkbox" class="available_cost_type select_product" id="prod_id'+prod_id+'"  value="'+prod_id+'"> <label for="prod_id'+prod_id+'"> <i class="gou-i"></label> ' +
                '<input type="hidden" name="prod['+prod_id+'][prod_id]"    value="'+prod_id+'">'+
                '</td>'+
                 '<td>'+data.data.prod_name+'</td>'+
                '<td>'+data.data.class_name+'</td>'+
                '<td>'+data.data.brand_name+'</td>'+
                '<td>'+data.data.model+'</td>'+
                '<td><input class="prod_num"  name="prod['+prod_id+'][prod_num]"  type="" maxlength="3"  value="1"/></td>'+
                '<td><input class="price"     name="prod['+prod_id+'][sale_price]"   type="" size="10" value="'+data.data.sale_price+'"  onkeyup="clearNoNum(this)"/></td>'+
                '<td><input class="rate"     type=""    name="prod['+prod_id+'][rate]"   maxlength="3" value="100"/></td>'+
                '<td ><span class="amount">'+data.data.sale_price+'</span> ' +
                '<input     class="amount_hidden" name="prod['+prod_id+'][amount]"  type="hidden" value="'+data.data.sale_price+'"/></td>'+
                '<td ><span class="total_amount">'+data.data.sale_price+'</span> ' +
                '<input     class="total_amount_hidden" name="prod['+prod_id+'][total_amount]"   type="hidden"  value="'+data.data.sale_price+'"/></td>'+
                '<td><input class="warranty_date"    name="prod['+prod_id+'][warranty_date]" type="" maxlength="10"  onclick="WdatePicker({dateFmt:\'yyyy-MM-dd\'})"  readonly="readonly"/></td>'+
               '<input type="hidden" name="prod['+prod_id+'][type]" value="2" />'
            ;
          var show_product = $("#show_prodcut").html();
          if(show_product.length>0){
              var haveExist = $("#"+data.data.id).find("#prod_id"+prod_id).val();
              if(!haveExist){  //勾选重复的产品不会显示
                  $("#show_prodcut").append('<tr id="'+data.data.id+'">'+html+'</tr>');
                  getAllTotal();
              }
          }
            $("#product_msg").html('');
            $("#product_msg").hide();

      }
    });
})

/*

    //产品数量验证
    $(".prod_num").keyup(function(){
        var prod_num = $(this).val();
        if(isNaN(prod_num)){
            alert('请输入数字');
            $(this).val('1');
            return false;
        }
        var last_num = $(this).next().val();
        if(parseInt(prod_num)>parseInt(last_num)){
            alert('退货数量不能超出合同限制');
            $(this).focus(function(){
                $(this).val('');
            });
        }
    });
*/







    //产品数量验证
    $('body').on('keyup','.prod_num',function(){
        var prod_num = $(this).val();
        if(isNaN(prod_num)){
            alert('请输入数字');
            $(this).val('1');
            return false;
        }
        if(prod_num!='' && prod_num==0){
            $(this).val(1);
        }
        var last_num = $(this).next().val();
        if(parseInt(prod_num)>parseInt(last_num)){
            alert('退货数量不能超出合同限制');
                $(this).val('');
        }

        var id = $(this).parents('tr').attr("id");
        getTotal(id);
        getAllTotal();
    })


    $('body').on('keyup','.rate',function(){
        var rate = $(this).val();
        if(isNaN(rate)){
            alert('请输入数字');
            $(this).val(1);
            return false;
        }
        if(rate!='' && rate==0){
            $(this).val('1');
            return false;
        }
        var id = $(this).parents('tr').attr("id");
        getTotal(id);
        getAllTotal();
    });



    $(".select_product").change(function(){
        if(this.checked){
            $("#product_msg").val('');
            $("#product_msg").css("display",'none');
        }
    })





    //计算单个产品总额
    function getTotal(id){
        var price      =  $("#"+id).find('.price').val();
        var prod_num   =  $("#"+id).find('.prod_num').val();
        var rate       =  $("#"+id).find('.rate').val();
        var total      = price*prod_num;
        total          = new Number(total);
        total          = total.toFixed(2);
        var total_amount = total*rate/100;
        total_amount = new Number(total_amount);
        total_amount = total_amount.toFixed(2);
        $("#"+id).find('.amount').html(total);
        $("#"+id).find('.amount_hidden').val(total);
        $("#"+id).find('.total_amount').html(total_amount);
        $("#"+id).find('.total_amount_hidden').val(total_amount);

    }


    //计算退货总额
    function getAllTotal(){
        //获取产品明细总价
        var checkboxInput = $(".select_product");
        var sum = 0;
        $(".select_product").each(function(i){
            id = $(this).val();
            var total_amount = $("#"+id).find('.total_amount_hidden').val();
            sum = sum+parseFloat(total_amount);
        });

        sum = new Number(sum);
        sum = sum.toFixed(2);
        $("#total_amount").val(sum);
        $('#all_total').val(sum);
    };



    /*
        //全选计算合同总额
        $('#allDetailed').on('change',function () {
            var amountText =$('.total_amount');

            var checkboxInput = $('tfoot input[type=checkbox]');
            var sum=0,sum1=0;

            if( $(this).is(':checked')){
                checkboxInput.prop("checked",true);
               amountText.each(function() {
                sum += Number($(this).text());
                })
                sum1 = sum + Number($('#otherExpenses').val());
               $("#product_msg").html('');
               $("#product_msg").hide();

            }else{
                sum1 = Number($('#otherExpenses').val());
                checkboxInput.prop("checked",false);
            }
            $('#allCost').text(sum1)
        });
    */




    //价格数据格式化
    function clearNoNum(obj){
        if(obj.value !=''&& obj.value.substr(0,1) == '.'){
            obj.value="";

        }
        obj.value = obj.value.replace(/^0*(0\.|[1-9])/, '$1');
        obj.value = obj.value.replace(/[^\d.]/g,"");
        obj.value = obj.value.replace(/\.{2,}/g,".");
        obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
        obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数
        if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
            if(obj.value.substr(0,1) == '0' && obj.value.length == 2){
                obj.value= obj.value.substr(1,obj.value.length);
            }
        }
    }




  //删除产品
    $(".delet-btn").click(function(){
        ids = new Array();
        $(".select_product").each(function(i){
            if( $(this).is(':checked')){
                var id = $(this).val();
                if(id>0){
                  ids[i] = id;
                }
            }
        })
        if(ids.length>0){
            confirm('确认删除产品吗?',function(){
                for(j=0;j<=ids.length;j++){
                    $("#"+ids[j]).remove();
                }
                $("#allDetailed").attr("checked",false);
            },this)
        }else{
            alert('请选择需要删除的产品');
        }
    })


    //全选删除按钮
    $('#allDetailed').on('change',function () {
        var checkboxInput = $('tfoot input[type=checkbox]');
        var sum=0,sum1=0;
        if( $(this).is(':checked')){
            checkboxInput.prop("checked",true);
        }else{
            checkboxInput.prop("checked",false);
        }
    });
    //全选
    $('#allDetailed1').on('change',function() {

        $(this).is(':checked')?$(this).parents('table').find('td.padding15 input[type="checkbox"]').prop('checked',true):$(this).parents('table').find('td.padding15 input[type="checkbox"]').prop('checked',false)
        
    })


    //表单数据提交验证
    $("#exchange_goods_add").submit(function(){
        var subject = $("#subject").val();
        var signTime = $("#exchangeDate").val();

        //计算所退的产品个数
            return_ids =new Array();
            $(".return_product").each(function(i){
                if(this.checked){
                    return_ids[i] = $(this).val();
                }
            })
            var returnLength = return_ids.length;



        //计算所换的产品个数
        var prodcutHidden  = $("#product_hide").html();
        var productLength = 0;
        if(prodcutHidden.length>0){
            var checkboxInput = $(".select_product");
            var sum = 0;
            ids =new Array();
            $(".select_product").each(function(i){
               // if(this.checked){
                    ids[i] = $(this).val();
                //}
            })
            var productLength = ids.length;
        }


        var accountAddress = $("#account_address").val();
        if(!accountAddress){
            $("#delivery_address").val('');
        }else{
            var accountAddress = $("#account_address").find("option:selected").text();
            var accountName = $("#account_name").val();
            $("#delivery_address").val(accountName+'/'+accountAddress);
        }



        if(!subject){
            $("#subject_msg").css("display",'block');
            $("#subject_msg").html('请输入换货主题');
            return false;
        }
        if(!signTime){
            $("#time_msg").css("display",'block');
            $("#time_msg").html('请选择换货日期');
            return false;
        }

        if(returnLength==0){
            $("#return_msg").css("display",'block');
            $("#return_msg").html('请勾选退货产品');
            return false;
        }

        if(!productLength){
            $("#product_msg").css("display",'block');
            $("#product_msg").html('请添加所换产品');
            return false;
        }


        $(".btnSubmit").attr("disabled",true);


    })


    //提示
    $(".tip").popover();

    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message');?>');
    <?php endif;?>


</script>


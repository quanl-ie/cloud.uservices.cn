<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = '新建发货单';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <link rel="stylesheet" href="/css/vueComponent.css"> -->
<!-- 时间插件引入元素 start -->
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/normalize.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/default.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/daterangepicker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/base.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/vue.component.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/css/send/send-goods.css'); ?>?v=<?php echo time(); ?>">

<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">新建发货单</span>
</div>
<section id="main-content">
    <div class="panel-body">
        <div id="app" v-cloak>
            <u-form>
                <section class="section base-info">
                    <h5 class="h5">基本信息</h5>
                    <hr class="hr">
                    <div>
                        <div class="row">
                            <div class="col flex">
                                <u-form-item label="发货单号：" mini label-width="110px">{{send.send_no}}</u-form-item>
                                <u-form-item label="关联出库单：" mini label-width="110px"><a @click.stop.prevent="openTab">{{send.stock_no}}</a>
                                </u-form-item>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col flex">
                                <u-form-item label="发货主题：" required mini label-width="110px">
                                    <u-input type="text" v-model="send.send_title"
                                             :rule="{ rule : /^\S{1,30}$/, msg : '请正确填写发货主题' }"
                                             width="160px" height="32px" placeholder="发货主题不超过30个字"></u-input>
                                </u-form-item>
                                <u-form-item label="发货类型：" mini label-width="110px">{{send.send_type_name}}
                                </u-form-item>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col flex">
                                <u-form-item mini widht="100%" label="申请人：" label-width="110px">{{send.apply_name}}
                                </u-form-item>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col flex">
                                <u-form-item mini width="100%" label="备注：" height="auto" label-vertical-align="top"
                                             label-width="110px">
                                    <u-textarea v-model="send.remark" width="523px"
                                                height="120px"></u-textarea>
                                </u-form-item>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="section">
                    <h5 class="h5">发货信息</h5>
                    <hr class="hr">
                    <div>
                        <div class="row">
                            <div class="col flex">
                                <u-form-item label="发货方式：" required mini label-width="110px">
                                    <u-select v-model="send.send_method"
                                              rule="UnEmpty::请选择发货方式"
                                              width="160px" height="32px">
                                        <option value="">请选择发货方式</option>
                                        <option v-for="opt in source.sendMethods" :value="opt.dict_enum_id">
                                            {{opt.dict_enum_value}}
                                        </option>
                                    </u-select>
                                </u-form-item>
                                <u-form-item v-show="send.send_method ===state.methodThird" label="物流公司：" mini
                                             label-width="110px">
                                    <u-select v-model="send.apll_id"
                                              :rule="checkLogistics"
                                              :disabled="send.send_method !== state.methodThird"
                                              width="160px" height="32px">
                                        <option value="">请选择物流公司</option>
                                        <option v-for="l in source.logistics" :value="l.id">{{l.name}}</option>
                                    </u-select>
                                </u-form-item>
                                <u-form-item v-show="send.send_method ===state.methodThird" label="运单编号：" mini
                                             label-width="110px">
                                    <u-input type="text"
                                             :rule="checkSn"
                                             placeholder="请输入运单编号"
                                             v-model="send.apll_no"
                                             width="160px" height="32px"></u-input>
                                </u-form-item>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col flex">
                                <u-form-item
                                        v-show="send.send_method ===state.methodThird|| send.send_method ===state.methodCarry"
                                        label="收货人姓名：" mini label-width="110px">
                                    <u-input type="text" placeholder="请输入收货人姓名" v-model="send.rec_name" width="160px"
                                             height="32px"></u-input>
                                </u-form-item>
                                <u-form-item
                                        v-show="send.send_method ===state.methodThird|| send.send_method ===state.methodCarry"
                                        label="收货人电话：" mini label-width="110px">
                                    <u-input type="text"
                                             placeholder="请输入收货人电话"
                                             v-model="send.rec_mobile"
                                             rule="?telephone::电话号码不正确"
                                             width="160px" height="32px"></u-input>
                                </u-form-item>
                                <u-form-item label="预计到货时间：" mini label-width="110px">
                                    <vue-date width='160px' height="32px"
                                              @riqi="changeDate"
                                              placeholder="请选择"
                                              :date="send.rec_date"
                                              margin="0 5px 0 0"></vue-date>
                                </u-form-item>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col flex">
                                <u-form-item
                                        v-show="send.send_method ===state.methodThird|| send.send_method ===state.methodCarry"
                                        label="收货地址：" width="100%" mini label-width="110px">
                                    <div class="flex">
                                        <u-select v-model="send.province_id" width="160px" height="32px"
                                                  margin="0 15px 0 0">
                                            <option value="">请选择省</option>
                                            <option v-for="o in source.province" :value="o.region_id">
                                                {{o.region_name}}
                                            </option>
                                        </u-select>
                                        <u-select v-model="send.city_id" width="160px" height="32px"
                                                  margin="0 15px 0 0">
                                            <option value="">请选择市</option>
                                            <option v-for="o in source.city" :value="o.region_id">{{o.region_name}}
                                            </option>
                                        </u-select>
                                        <u-select v-model="send.district_id" width="160px" height="32px"
                                                  margin="0 15px 0 0">
                                            <option value="">请选择区</option>
                                            <option v-for="o in source.district" :value="o.region_id">
                                                {{o.region_name}}
                                            </option>
                                        </u-select>
                                        <u-input type="text"
                                                 placeholder="请输入详细地址"
                                                 width="363px" height="32px"
                                                 v-model="send.rec_address"></u-input>
                                    </div>
                                </u-form-item>
                            </div>
                        </div>

                    </div>
                </section>
                <section class="section">
                    <h5 class="h5">提货人信息</h5>
                    <hr class="hr">
                    <div>
                        <div class="row">
                            <div class="col flex">
                                <u-form-item label="提货人：" label-width="110px">
                                    <div>
                                        <u-select @change="selectConsignee"
                                                  v-model="send.consignee"
                                                  width="320px" height="32px">
                                            <option value="">请选择提货人</option>
                                            <option v-for="(opt, index) in source.consignee"
                                                    :value="opt.id">{{createOption(opt)}}
                                            </option>
                                        </u-select>
                                        <u-button width="80px" @click="openDialog">编辑</u-button>
                                    </div>
                                </u-form-item>
                            </div>
                        </div>
                    </div>

                </section>
                <section class="section">
                    <h5 class="h5">发货产品明细</h5>
                    <hr class="hr">
                    <div>
                        <div class="row">
                            <div class="col flex">
                                <table class="table" cellpadding="0" cellspacing="0" border="1">
                                    <thead>
                                    <tr>
                                        <th>产品名称</th>
                                        <th>出库单号</th>
                                        <th>产品编号</th>
                                        <th>产品品牌</th>
                                        <th>产品型号</th>
                                        <th>计量单位</th>
                                        <th>数量</th>
                                        <th>序列号</th>
                                        <th>库房</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-if="!source.product || !source.product.length ">
                                        <td colspan="9">暂无产品可选择，请确认是否已出库</td>
                                    </tr>
                                    <tr v-else v-for="p in source.product">
                                        <td>{{p.prod_name}}</td>
                                        <td><a @click.stop.prevent="openTab">{{p.stock_no}}</a></td>
                                        <td>{{p.prod_no}}</td>
                                        <td>{{p.brand_name}}</td>
                                        <td>{{p.model}}</td>
                                        <td>{{p.dict_enum_value}}</td>
                                        <td>{{p.prod_num}}</td>
                                        <td>{{p.serial_number}}</td>
                                        <td>{{p.depot_name}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
                <hr class="hr">
                <div class="row" style="margin-bottom: 20px">
                    <div class="col flex flex-zz-center">
                        <u-button margin="0 20px 0 0" @click="goBack">取消</u-button>
                        <u-button type="primary" @click="submit" :disabled="state.submit" check>确认</u-button>
                    </div>
                </div>
            </u-form>
        </div>
    </div>
</section>
<script type="text/javascript" src="/js/vue/lib/vue.js"></script>
<script type="text/javascript" src="/js/vue/lib/axios.min.js"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js"></script>
<script type="text/javascript" src="/js/vue/lib/vue.mixin.emitter.js"></script>
<script type="text/javascript" src="/js/vue/lib/vue.component.js"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js"></script>
<script type="text/javascript" src="/js/vue/controller/send-out/add.js"></script>

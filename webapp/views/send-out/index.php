<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = '发货管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <link rel="stylesheet" href="/css/vueComponent.css"> -->
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/base.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/vue.component.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/goodslist.css'); ?>?v=<?php echo time(); ?>">
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">发货管理</span>
</div>
<?php if (!in_array('/send-out/sending', $selfRoles) && !in_array('/send-out/sended', $selfRoles)): ?>
    <div style="text-align: center;font-size:20px; font-weight: bold;">无权限</div>
<?php else: ?>
    <section id="main-content">
        <div class="panel-body" v-cloak>
            <!-- tab切换 -->
            <div class="ct-tab-box">
                <ul>
                    <?php if (in_array('/send-out/sending', $selfRoles)): ?>
                        <li v-init-prop="[state,'state1',true]" :class="status==1?'active':''" @click='tabGetdata'
                            value='1'>运输中
                        </li>
                    <?php endif; ?>
                    <?php if (in_array('/send-out/sended', $selfRoles)): ?>
                        <li v-init-prop="[state,'state2',true]" :class="status==2?'active':''" @click='tabGetdata'
                            value='2'>已签收
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
            <!-- 搜索框 -->
            <div class="flex flex-wrap">
                <u-form-item label="发货单号" mini width='240px' label-width="80px" padding='5px'>
                    <u-input type="text"
                             width="160px"
                             v-model="search.send_no"
                             placeholder="请输入单号"></u-input>
                </u-form-item>
                <u-form-item label="发货方式" label-width="80px" mini width='240px'>
                    <u-select width="160px" height="32px" v-model="search.send_method">
                        <option value="">请选择发货方式</option>
                        <option v-for="item in source.sendMethodList" :value="item.id">{{item.name}}</option>
                    </u-select>
                </u-form-item>
                <u-form-item label="发货类型" label-width="80px" mini width='240px'>
                    <u-select width="160px" height="32px" v-model="search.send_type">
                        <option value="" selected>请选择发货类型</option>
                        <option v-for="item in source.sendTypeList" :value="item.id">{{item.name}}</option>
                    </u-select>
                </u-form-item>
                <u-form-item label="创建时间" label-width="80px" mini width='430px'>
                    <div class="flex">
                        <vue-date width='160px' margin="0 5px 0 0" @riqi="changeDate" plural='start_create_time' :date='search.start_create_time'></vue-date>
                        至
                        <vue-date width='160px' margin="0 0 0 5px"   @riqi="changeDate" plural='end_create_time' :date='search.end_create_time'></vue-date>
                    </div>
                </u-form-item>
                <u-form-item label-width="0" mini width='82px'>
                    <button class="btn btn-search" @click="searchEvent">搜索</button>
                </u-form-item>
            </div>
            <!-- 列表内容 -->
            <table>
                <thead>
                <tr>
                    <th width="50px">序号</th>
                    <th>发货单号</th>
                    <th>发货类型</th>
                    <th>发货方式</th>
                    <th width="80px">产品数量</th>
                    <th :class="status==1?'right-border':''">创建时间</th>
                    <th v-if='status==2' class="right-border">收货时间</th>
                    <th width="160px" class="center">操作</th>
                </tr>
                </thead>
                <tbody>
                <tr v-if="dataList.length>0" v-for='(item,index) in dataList' :key='index'>
                    <td class="center">{{index+1}}</td>
                    <td>{{item.send_no}}</td>
                    <td>{{item.send_type_desc}}</td>
                    <td>{{item.send_method_desc}}</td>
                    <td class="center">{{item.product_num}}</td>
                    <td :class="status==1?'right-border':''">{{item.create_time}}</td>
                    <td v-if='status==2' class="right-border">{{item.rel_rec_date}}</td>
                    <td class="center">
                        <?php if (in_array('/send-out/view', $selfRoles)): ?>
                            <a :href="'/send-out/view?id='+item.id" :data-id='item.id' class="mgr10">查看详情</a>
                        <?php endif; ?>
                        <?php if (in_array('/send-out/confirm', $selfRoles)): ?>
                            <a href="javascript:" @click="showCollectGoodsEvent(item.id,item.rec_name)" v-if='status==1'>确认收货</a>
                        <?php endif; ?>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="mg-top30">
                <vue-pagination :cur.sync="cur" :all.sync="all" v-on:btn-click="listen"></vue-pagination>
            </div>
            <vue-layer width="350px" v-on:show='hideCollectGoodsEvent' ly-title='确认收货' v-if='showCollectGoods' show-name='showCollectGoods'>
                <p style="font-weight: bold;font-size: 14px;margin-bottom:30px;">
                    请填写收货人姓名，确认收货后，本发货单将不可修改
                </p>
                <u-input v-model="confirmCollectGoodsData.rec_name"></u-input>
                <div class="u-btn-box">
                    <button type="button" class="btn bg-f7 mgr20" @click='hideCollectGoodsEvent'>取消</button></a>
                    <button type="button" class="btn btn-orange" @click="confirmCollectGoodsEvent">确定</button></a>
                </div>
            </vue-layer>
        </div>
    </section>
<?php endif; ?>

<script type="text/javascript" src="/js/vue/lib/vue.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.mixin.emitter.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.component.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/controller/send-out/index.js?v=<?php echo $v;?>"></script>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = '发货单详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <link rel="stylesheet" href="/css/vueComponent.css"> -->
 <link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/base.css');?>?v=<?php echo time();?>">
 <link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/vue.component.css');?>?v=<?php echo time();?>">
 <style>
 	th{background: #f8f8f8;}
 	p.status{font-size: 15px;color: #000;padding-left: 10px;}
 	p.status>span{margin-left: 20px;font-weight: 600;}
 	.ipt-explain{
 		left: 0;
     	top: 110px;}
 	@media print{
 		.top-btn-box{display: none;}
 	}
 </style>
 <div class="jn-title-box no-margin-bottom">
    <span class="jn-title">发货单详情</span>
</div>
<section id="main-content"> 
    <div class="panel-body" v-cloak>
    	<div class="top-btn-box">
    		<button class="btn btn-orange mgr8" onclick="window.print();">打印</button>
            <?php if (in_array('/send-out/confirm', $selfRoles)): ?>
    		    <button class="btn btn-orange mgr8" @click='edit("show4")' v-if="status!='2'">确认收货</button>
            <?php endif; ?>
    		<a href="javascript:history.back(-1)" class="btn bg-f7" style="color: #333;">返回</a>
    	</div>
    	<p class="status">当前发货状态<span :style="status=='2'?'color:#44bea3':'color:#ff6f11'">{{status=='2'?'已完成':'运输中'}}</span></p>
    	<h5 class="u-title-h5">物流信息</h5>
    	<div class="flex">
    		<u-form-item label="发货方式：" label-width="100px" mini>
				{{dataobj.send_method_desc}}
    		</u-form-item>
    		<u-form-item label="物流公司："  label-width="100px" mini  v-if="dataobj.send_method==3">
				{{dataobj.apll_name}}
    		</u-form-item>
    		<u-form-item label="运单编号："  label-width="100px" mini  v-if="dataobj.send_method==3">
				{{dataobj.apll_no}}
    		</u-form-item>
    	</div>
    	<u-form-item label="物流跟踪：" label-height="10px" label-width="100px" mini width='100%' height='auto'>
            <vue-step :setp-data='apll_detail' lfct='update_time' ctct='content'  send-status>
                <?php if (in_array('/send-out/add-apll-detail', $selfRoles)): ?>
				    <a class="u-oper-btn" href="javascript:" @click='edit("show1")' v-if="status!='2'">更新进度</a>
                <?php endif; ?>
			</vue-step>
		</u-form-item>
		<h5 class="u-title-h5">发货单信息</h5>
		<div class="flex">
    		<u-form-item label="发货单号：" height='17px' label-height="20px"  label-width="100px" mini>
				{{invoiceData.send_no}}
    		</u-form-item>
    		<u-form-item label="关联出库单：" height='17px' label-height="20px"  label-width="100px" mini>
				{{invoiceData.stock_no}}
    		</u-form-item>
    		<u-form-item label="发货主题："  height='17px' label-height="20px"  label-width="100px" mini>
				{{invoiceData.send_title}}
    		</u-form-item>
    	</div>
    	<div class="flex">
    		<u-form-item label="发货类型：" height='17px' label-height="20px"  label-width="100px" mini>
				{{invoiceData.type}}
    		</u-form-item>
    		<u-form-item label="发货方式：" height='17px' label-height="20px"  label-width="100px" mini>
				{{invoiceData.method}}
    		</u-form-item>
    	</div>
    	<div class="flex">
    		<u-form-item label="申请人：" height='17px' label-height="20px"  label-width="100px" mini>
				{{invoiceData.apply_name}}
    		</u-form-item>
    		<u-form-item label="创建时间：" height='17px' label-height="20px"  label-width="100px" mini>
				{{invoiceData.create_time}}
    		</u-form-item>
    	</div>
    	<div class="flex">
    		<u-form-item label="备注：" height='auto' label-height="20px"  label-width="100px" mini width="100%">
				{{invoiceData.remark}}
    		</u-form-item>
    	</div>
    	<div v-if="dataobj.send_method==3">
			<h5 class="u-title-h5">收货人信息
                <?php if (in_array('/send-out/edit-rec', $selfRoles)): ?>
                    <a href="javascript:" class="bianji" @click='edit("show2")' v-if="status!='2'"><i class="icon2-bainji-1"></i>编辑</a>
                <?php endif; ?>
                </h5>
			<div class="flex">
	    		<u-form-item label="收货人姓名：" height='17px' label-height="20px"  label-width="100px" mini>
					{{consigneeData.name}}
	    		</u-form-item>
	    		<u-form-item label="收货人电话：" height='17px' label-height="20px"  label-width="100px" mini>
					{{consigneeData.mobile}}
	    		</u-form-item>
	    	</div>
	    	<div class="flex">
	    		<u-form-item label="收货地址：" height='17px' label-height="20px"  label-width="100px" mini>
					{{consigneeData.address}}
	    		</u-form-item>
	    	</div>
	    </div>
	    <div v-if="dataobj.send_method!=2">
	    	<h5 class="u-title-h5">提货人信息
                <?php if (in_array('/send-out/edit-pick', $selfRoles)): ?>
                    <a href="javascript:" class="bianji" @click='edit("show3")' v-if="status!='2'"><i class="icon2-bainji-1"></i>编辑</a>
                <?php endif; ?>
                </h5>
			<div class="flex">
	    		<u-form-item label="提货人姓名：" height='17px' label-height="20px"  label-width="100px" mini>
					{{takeData.name}}
	    		</u-form-item>
	    		<u-form-item label="身份证号：" height='17px' label-height="20px"  label-width="100px" mini>
					{{takeData.card}}
	    		</u-form-item>
	    	</div>
	    	<div class="flex">
	    		<u-form-item label="提货人电话：" height='17px' label-height="20px"  label-width="100px" mini>
					{{takeData.mobile}}
	    		</u-form-item>
	    		<u-form-item label="车牌号：" height='17px' label-height="20px"  label-width="100px" mini>
					{{takeData.license}}
	    		</u-form-item>
	    	</div>
	    </div>
		<h5 class="u-title-h5">发货产品明细</h5>
		<div>
			<table>
				<thead>
					<tr>
						<th>产品名称</th>
						<th>出库单号</th>
						<th>产品编号</th>
						<th>产品品牌</th>
						<th>产品型号</th>
						<th>计量单位</th>
						<th>数量</th>
						<th>序列号</th>
						<th>库房</th>
					</tr>
				</thead>
				<tbody>
					<tr v-for='(item,index) in product' :key='index'>
						<td>{{item.prod_name}}</td>
						<td>{{item.stock_no}}</td>
						<td>{{item.prod_no}}</td>
						<td>{{item.brand_name}}</td>
						<td>{{item.model}}</td>
						<td>{{item.dict_enum_value}}</td>
						<td>{{item.prod_num}}</td>
						<td>{{item.serial_number}}</td>
						<td>{{item.depot_name}}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- 更新物流进度弹层 -->
		<vue-layer width="500px" v-on:show='layershow' ly-title='更新物流进度' v-if='show1' show-name='show1'>
			<u-form-item  label="更新时间：" label-width="80px" mini width='100%' height='46px'>
				<vue-date v-if='show1' width='100%' margin="0 5px 0 0" placeholder='请选择日期' @riqi="changeDate" timehsm :date='date'></vue-date>
				<p class="u-tips-p" v-if="apllDetail.update_time==''">请选择日期</p>
			</u-form-item>
			<u-form-item mini width="100%" label="进度摘要：" label-width="80px" label-height='10px' height="auto" label-vertical-align="top"
                                     label-width="110px">
                <u-textarea  placeholder='请输入进度摘要' width="100%" height="100px" rule="UnEmpty::请输入进度摘要" v-model='apllDetail.content' ></u-textarea>
                <p class="u-tips-p" v-if="apllDetailCont" @input="apllDetail.content!=''?apllDetailCont=false:''">请输入进度摘要</p>
            </u-form-item>
            <div style="text-align: center;" class="u-btn-box">
	            <button type="button" class="btn bg-f7 mgr20" @click='edit("show1")'>返回</button></a>
	            <button type="button" class="btn btn-orange" @click='submit("show1")'>提交</button></a>
	        </div>
		</vue-layer>
		<!-- 编辑收货人信息弹层 -->
		<vue-layer width="580px" v-on:show='layershow' ly-title='编辑收货人信息' v-if='show2' show-name='show2'>
			<div class="flex">
				<u-form-item label="收货人姓名：" width="292px" mini label-width="90px" margin='0 10px 10px 0'>
					<u-input  v-model='recInfo.rec_name'  placeholder='请输入收货人姓名'></u-input>
				</u-form-item>
				<u-form-item label="收货人电话：" width="292px" mini label-width="90px"  margin='0 0 10px 10px'>
					<u-input  v-model='recInfo.rec_mobile'  placeholder='请输入收货人电话'></u-input>
				</u-form-item>
			</div>

			<u-form-item label="收货地址：" width="100%" mini label-width="90px">
                    <div class="flex">
                    	<u-select  width="160px" height="32px" margin="0 15px 0 0" v-model='recInfo.province_id' placeholder='省'>
                            <option value="">请选择</option>
                            <option v-for="(v,i) in province" :value="v.region_id">{{v.region_name}}</option>
                        </u-select>
                        <u-select  width="160px" height="32px" margin="0 15px 0 0" v-model='recInfo.city_id' placeholder='市'>
                            <option value="">请选择</option>
                            <option v-for="(v,i) in city" :value="v.region_id">{{v.region_name}}</option>
                        </u-select>
                        <u-select  width="160px" height="32px" margin="0" v-model='recInfo.district_id'  placeholder='区'>
                            <option value="">请选择</option>
                            <option v-for="(v,i) in district" :value="v.region_id">{{v.region_name}}</option>
                        </u-select>
                    </div>
            </u-form-item>
            <u-form-item label="详细地址：" width="520px" mini label-width="90px">
            	<u-input  v-model='recInfo.rec_address' placeholder='请输入详细地址'></u-input>
			</u-form-item>
			<div class="u-btn-box">
	            <button type="button" class="btn bg-f7 mgr20" @click='edit("show2")'>返回</button></a>
	            <button type="button" class="btn btn-orange" @click='submit("show2")'>提交</button></a>
	        </div>
			
		</vue-layer>
		<!-- 编辑提货人信息弹层 -->
		<vue-layer width="400px" height='340px' v-on:show='layershow' ly-title='编辑提货人信息' v-if='show3' show-name='show3'>
			<u-form-item label="提货人姓名：" width="100%" mini label-width="100px" >
            	<u-input v-model='pickInfo.pick_name' placeholder="请输入提货人姓名"></u-input>
			</u-form-item>
			<u-form-item label="身份证号：" width="100%" mini label-width="100px" >
            	<u-input v-model='pickInfo.pick_card'  placeholder="请输入提货人身份证号"></u-input>
			</u-form-item>
			<u-form-item label="提货人电话：" width="100%" mini label-width="100px" >
            	<u-input v-model='pickInfo.pick_mobile'  placeholder="请输入提货人电话"></u-input>
			</u-form-item>
			<u-form-item label="车牌号：" width="100%" mini label-width="100px" >
            	<u-input v-model='pickInfo.pick_license'  placeholder="请输车牌号"></u-input>
			</u-form-item>
			<div class="u-btn-box">
	            <button type="button" class="btn bg-f7 mgr20" @click='edit("show3")'>返回</button></a>
	            <button type="button" class="btn btn-orange" @click='submit("show3")'>提交</button></a>
	        </div>
		</vue-layer>
		<!-- 确认收货 -->
		<vue-layer width="350px" height='220px' v-on:show='layershow' ly-title='确认收货' v-if='show4' show-name='show4'>
            <p style="font-size: 14px;margin-bottom:10px;text-align: center;">
                请填写收货人姓名，确认收货后，本发货单将不可修改
            </p>
          
            	<u-input width="100%" v-model="confirmCollectGoodsData.rec_name"></u-input>
       
            
            <div class="u-btn-box">
                <button type="button" class="btn bg-f7 mgr20" @click='edit("show4")'>取消</button></a>
                <button type="button" class="btn btn-orange" @click='confirmCollectGoodsEvent'>确定</button></a>
            </div>
        </vue-layer>













    </div>
</section>
<!-- <div class="u-layer-box">
	<div class="u-layer-centered">
		<div class="u-layer-body">
			<p class="u-layer-title">编辑收货人信息<i class="icon2-cha1"></i></p>
			<div class="u-layer-content">
				
			</div>
		</div>
	</div>
</div> -->

<script type="text/javascript" src="/js/vue/lib/vue.js"></script>
<script type="text/javascript" src="/js/axios.min.js"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js"></script>
<script type="text/javascript" src="/js/vue/lib/vue.mixin.emitter.js"></script>
<script type="text/javascript" src="/js/vue/lib/vue.component.js"></script>
<script type="text/javascript" src="/js/vue/controller/send-out/view.js"></script>
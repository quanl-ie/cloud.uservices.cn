<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
$this->title = '修改服务类型';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="/plugins/bootstrap/css/bootstrap.min.css?v=<?php echo time()?>"> 
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time()?>">
<script src="<?php echo Url::to('/js/jquery-1.8.3.min.js');?>?v=<?php echo time()?>"></script>
<style>
  body{background: #fff;}
  li ul{display: block;}
   .drop-dw-layerbox .icon{left: 0;}
   .addinput-list>.drop-dw-layerbox li{padding-left: 0;}
</style>
<div class="col-md-12">
    <?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal',
                'id' => 'class-edit',
                'name' => 'form1',
                'enctype' => 'multipart/form-data',
            ]
        ]);
    ?>
    <div class="information-list">
        <label class="name-title-lable" for="workorder-account_id">
           <i class="red-i">*</i> 上级分类：
        </label>
        <span class="c_red"></span>
        <div class="col-md-10 addinput-list region-limit">
            <input type="hidden" name="class_id" id="class_id" value="<?=$data['id'] ? $data['id'] : '' ?>">
            <input type="hidden" name="class_oid" value="<?=$data['pid'] != 0 ? $data['pid'] : -1 ?>">
            <input type="hidden" name="class_pid" value="<?=$data['pid'] != 0 ? $data['pid'] : 0 ?>">
            <input type="text" class="form-control" value="<?=isset($data['pid_name']) ? $data['pid_name'] : '无' ?>" id="category1" placeholder="请选择上级分类" readonly>
            <i class="icon iconfont icon-shouqi1"></i>
            <p class="misplacement" style="bottom: -5px;" id="work_type_msg">请选择上级分类</p>
            <div class="drop-dw-layerbox">
                <ul style="padding-top: 10px;">
                    <li>
                        <span>
                            <input type="radio" value="-1" name="class_pid" id="-1" class="pr1" onclick="changeCategory(this)">
                            <label for="-1">无</label>
                        </span>
                    </li>
                        <?php if (!empty($classList)) : ?>
                        <?php foreach ($classList as $key => $val) : ?>
                             <li>
                                <span>
                                    <?php if ($val['exist'] == 1) : ?>
                                    <i class="icon2-shixinyou"  onclick="clickCategory(this) "></i>
                                    <?php endif;?>
                                    <?php if ($val['id'] == $data['pid']) : ?>
                                    <input type="radio" value="<?=$val['id'] ?>" name="class_pid" id="<?=$val['id'] ?>" class="pr1" onclick="changeCategory(this)">
                                    <?php else:?>
                                    <input type="radio" value="<?=$val['id'] ?>" name="class_pid" id="<?=$val['id'] ?>" class="pr1" onclick="changeCategory(this)">
                                    <?php endif;?>
                                    <label for="<?=$val['id'] ?>"><?=$val['class_name'] ?></label>
                                </span>
                            </li>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    
                </ul>
            </div>
        </div>
    </div>
    <div class="information-list">
        <label class="name-title-lable" for="workorder-account_id">
           <i class="red-i">*</i> 分类名称：
        </label>
        <span class="c_red"></span>
        <div class="col-md-10 addinput-list">
            <input type="text" class="form-control" name="class_name" value="<?=isset($data['class_name']) ? $data['class_name'] : '' ?>" id="class_name" placeholder="请输入分类名称" maxlength="10">
            <p class="misplacement" style="bottom: -5px;" id="work_type_msg">请选择上级分类</p>
        </div>
    </div>
    <div class="information-list">
        <label class="name-title-lable" for="workorder-account_id">
            排序：
        </label>
        <span class="c_red"></span>
        <div class="col-md-10 addinput-list">
            <input type="text" class="form-control" maxlength="5" name="sort" value="<?=isset($data['sort']) ? $data['sort'] : '' ?>" id="sort" placeholder="仅限数字，用于排序，至多5位">
            <p class="misplacement" style="bottom: -5px;" id="work_type_msg"></p>
        </div>
    </div>
    <p class="height10"></p>
    <hr>
    <div class="information-list" style="padding-top: 15px">
        <div class="btn-center-box">
            <button class="btn bg-f7 mgr10" id="closeIframe">取消</button>
            <button class="btn btn-orange">保存</button>
        </div>
    </div>
    <?php ActiveForm::end() ?>
</div>

<script src="/js/layer/layer.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    function  clickCategory(el) {
        var class_id = $("#class_id").val();
        var id = $(el).next('input').val();
        var _this = $(el).parent('span')
        $.getJSON('/common/ajax-get-class',{'pid':id,class_id:class_id},function (json) {
                var ulData = '';
               if (_this.next('ul').size()==0) {
                    ulData +="<ul>";

                   var data = json.data;
                   delete data.flag;
                   $.each(data,function(i,elm) {
                        ulData +="<li><span>";
                       elm.exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="clickCategory(this)"></i>':'';
                       ulData+='<input type="radio" value="'+elm.id+'" name="class_pid" id="'+elm.id+'" class="pr1" onclick="changeCategory(this)"><label for="'+elm.id+'">'+elm.class_name+'</label></span></li>';
                   })
                    ulData +="</ul>";
                    _this.parent('li').append(ulData);
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');

               }else{
                if(
                    _this.next('ul').is(':hidden')){
                        $(el).removeClass('icon2-shixinyou');
                        $(el).addClass('icon2-shixinxia');
                        _this.next('ul').show();
                        
                    }else{
                        $(el).addClass('icon2-shixinyou');
                        $(el).removeClass('icon2-shixinxia');
                        _this.next('ul').hide();
                    }
               }
                return false;
        })
        
    }
    
    function changeCategory(el) {
        $('.drop-dw-layerbox').hide();
        $('#category1').val($(el).next('label').text());
    }
    
    $('#category1').on('click',function() {
        $('.drop-dw-layerbox').is(':hidden')?$('.drop-dw-layerbox').show():$('.drop-dw-layerbox').hide();
        
    })
    
    var index = parent.layer.getFrameIndex(window.name);
    //表单验证
    $("#class-edit").Validform({
        ajaxPost:true,
        tiptype:3,
        beforeSubmit:function(curform){
            var reg  = /^[0-9]{1,5}$/;
            if ($("#sort").val() != '' && $("#sort").val().search(reg) == -1)
            {
                layer.alert('仅限数字');
                return false;
            }
        },
        callback:function(data){
            if (data.code == 200) {
                $.Hidemsg();
                parent.layer.close(index);
                parent.layer.alert(data.message, function(index){
                    parent.location.reload();
                });
            }else{
                $.Hidemsg();
                parent.alert(data.message);
            }
        }
    });

    //关闭iframe
    $('#closeIframe').click(function(){
        parent.layer.close(index);
        return false;
    });
    $(document).click(function(event) {
       var region = $('.region-limit');
       if (!region.is(event.target)&&region.has(event.target).length ===0) {
                $('.drop-dw-layerbox').hide();
       }
    });
</script>
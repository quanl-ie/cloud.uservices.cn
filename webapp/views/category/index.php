<?php
    use yii\helpers\Url;
    $this->title = '产品类目';
    $this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<style>
    /*.li-td-left .icon{margin-right: 10px;position: absolute;left: 0;top: 7px;}*/
    .li-td-left{position: relative;padding-left: 15px;}
    .left-border-d5d5d5{padding-left: 15px;}
    
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">产品类目</span>
    <?php if (!empty($flag) && $flag == 1): ?>
        <?php if(in_array('/category/add',$selfRoles)){?>
            <span class="btn btn-success jn-btn" style=" color:#FFF;float:right;width:auto;" data-toggle="modal" data-target="#scrollingModal" onclick="addClass()">添加</span>
        <?php };?>
    <?php endif; ?>
</div>
<section id="main-content">
    <div class="panel-body" style="position: relative;min-width: 1000px;">
        <?php if (isset($data) && !empty($data)) : ?>
        <div class="table-box">
            <ul>
                <li>
                    <div class="left-border-d5d5d5"style="padding-left: 0;<?php if (!empty($flag) && $flag != 1) {echo 'width: 78%';} ?>">
                        <span class="li-tb-title">类目名称</span>
                    </div>
                    <div class="center1-border-d5d5d5">
                        <span class="li-tb-title">排序</span>
                    </div>
                    <div class="center2-border-d5d5d5">
                        <span class="li-tb-title">状态</span>
                    </div>
                    <?php if (!empty($flag) && $flag == 1): ?>
                        <div class="right-border-d5d5d5">
                            <span class="li-tb-title">操作</span>
                        </div>
                    <?php endif; ?>
                </li>
                <div class="big-box">
                        <?php foreach ($data as $key => $val) : ?>
                            <li>
                                <?php if ($val['exist'] == 1) : ?>
                                <div class="left-border-d5d5d5" style="<?php if (!empty($flag) && $flag != 1) {echo 'width: 78%';} ?>" onClick="clickCategory(this,<?=$val['class_id'] ?>) ">
                                    <span class="li-td-left">
                                    <i class="icon2-shixinyou"></i><?=$val['title']; ?></span>
                                </div>
                                <?php else: ?>
                                <div class="left-border-d5d5d5" style="<?php if (!empty($flag) && $flag != 1) {echo 'width: 78%';} ?>">
                                    <span class="li-td-left">
                                    <?=$val['title']; ?></span>
                                </div>
                                <?php endif; ?>
                                
                                <div class="center1-border-d5d5d5">
                                    <span class="li-td-center"><?=$val['sort']; ?></span>
                                </div>
                                <div class="center2-border-d5d5d5">
                                    <span class="li-td-center"><?=$val['status_desc']; ?></span>
                                </div>
                                <?php if (!empty($flag) && $flag == 1): ?>
                                <div class="right-border-d5d5d5">
                                    <span class="li-td-center">
                                        <?php if (isset($val['status']) && $val['status'] == 1) : ?>
                                            <?php if(in_array('/category/add-next',$selfRoles)){?>
                                                <a href="javascript:;" onclick="addNextClass(<?=$val['class_id']; ?>)">添加下级</a>
                                            <?php };?>
                                            <?php if(in_array('/category/edit',$selfRoles)){?>
                                                <a href="javascript:;" onclick="editClass(<?=$val['class_id']; ?>,<?=$val['parent_id']; ?>)">修改</a>
                                            <?php };?>
                                            <?php if(in_array('/category/change-status',$selfRoles)){?>
                                                <a href="javascript:;" onclick="disClass(<?=$val['class_id']; ?>,2)">禁用</a>
                                            <?php };?>
                                        <?php else: ?>
                                            <?php if(in_array('/category/change-status-on',$selfRoles)){?>
                                                <a href="javascript:;" onclick="enaClass(<?=$val['class_id']; ?>,1)" >启用</a>
                                            <?php };?>
                                        <?php endif; ?>
                                        
                                    </span>
                                </div>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                </div>
                <li>
                    <div class="xian98"></div>
                </li>
            </ul>
        </div>
        <?php else:?>
        <div class="no-data-page">
            <div>
                <div class="kbtp"></div>
                <p class="no-data-zi">您还没有添加过产品类目</p>
                <?php if (!empty($flag) && $flag == 1): ?>
                    <span class="btn btn-success" onclick="addClass()">添加产品类目</span>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
    

</section>
<script>

    //添加
    function addClass () {
        layer.open({
            type: 2,
            title: '添加产品类目',
            area: ['580px', '350px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/category/add', 'no'],
            end:function(){
                location.reload();
            }
        });
    }

    //添加下级
    function addNextClass (id) {
        layer.open({
            type: 2,
            title: '添加下级',
            area: ['580px', '350px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/category/add-next?id='+id+'&type='+2, 'no'],
            end:function(){
                location.reload();
            }
        });
    }

    //修改
    function editClass (id,pid) {
        layer.open({
            type: 2,
            title: '修改产品类目',
            area: ['580px', '350px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/category/edit?id='+id+'&pid='+pid+'&type='+1, 'no'],
            end:function(){
                location.reload();
            }
        });
    }
    
    /**
     * 禁用
     * @param id
     * @param status
     */
    function disClass(id,status) {
        layer.confirm('您确定要禁用该产品类目吗？禁用后有此产品类目的 经销商将不能创建此产品类目的新订单，但不影响未完成和已完成订单数据。', {
            area:['320px','210px'],
            btn: ['确定','取消'] //按钮
        }, function(){
            ajaxPost(id,status);
        }, function(){
            layer.close();
        });
    }

    /**
     *  启用
     * @param id
     * @param status
     */
    function enaClass(id,status) {
        layer.confirm('您确定要启用该产品类目吗？启用后有此产品类目的 经销商将可创建此产品类目的新订单，但不影响未完成和已完成订单数据。', {
            area:['320px','210px'],
            btn: ['确定','取消'] //按钮
        }, function(){
            ajaxPostOn(id,status);
        }, function(){
            layer.close();
        });
    }

    /**
     * ajax 修改状态
     * @param id
     * @param status
     */
    function ajaxPost(id,status){
        var url = '/category/change-status';
        $.ajax({
            type: 'POST',
            url: url,
            data: {'id':id,'status':status},
            dataType: 'json',
            success: function(data){
                if (data.code == 200) {
                    layer.alert(data.message, {
                    }, function(){
                        location.reload();
                    });
                }else{
                    alert(data.message);
                }
            }
        });
    }
    function ajaxPostOn(id,status){
        var url = '/category/change-status-on';
        $.ajax({
            type: 'POST',
            url: url,
            data: {'id':id,'status':status},
            dataType: 'json',
            success: function(data){
                if (data.code == 200) {
                    layer.alert(data.message, {
                    }, function(){
                        location.reload();
                    });
                }else{
                    alert(data.message);
                }
            }
        });
    }


    //获取下级分类列表
    function  clickCategory(el,id) {
        
        var _self = $(el);
        var marginLeft =Number(_self.find('span').css('marginLeft').match(/(\S*)px/)[1]);

        var ulData = '';
        if (_self.siblings('ul').size()==0)
        {
            $.getJSON('/common/ajax-get-class',{'pid':id,'type':2},function (json) {
                ulData +="<ul>"
                
                var data = json.data;
                var flag = data.flag;
                delete data.flag;
                console.log(flag);
                $.each(data,function(index, elm) {
                    ulData +="<li>";
                    var nextClass ='<div class="left-border-d5d5d5"';
                        if (flag == 2){
                            nextClass +='style="width:78%"';
                        }
                        nextClass+='onclick="clickCategory(this,'+elm.id+')"><span class="li-td-left" style="margin-left:'+(marginLeft+16)+'px"><i class="icon icon2-shixinyou"></i>'+elm.class_name+'</span></div>';
                    var nonClass  ='<div class="left-border-d5d5d5"';
                     if (flag == 2){
                            nonClass +='style="width:78%"';
                        }

                     nonClass+='><span class="li-td-left" style="margin-left:'+(marginLeft+16)+'px">'+elm.class_name+'</span></div>';
                    if (elm.exist == 1) {
                        ulData+= nextClass;
                    }else{
                        ulData+= nonClass;
                    }
                    ulData+='<div class="center1-border-d5d5d5"><span class="li-td-center">'+elm.sort+'</span></div>\n' +
                        '<div class="center1-border-d5d5d5"><span class="li-td-center">'+elm.status_desc+'</span></div>\n';
                        if (flag == 1)
                        {
                            ulData+='<div class="right-border-d5d5d5"><span class="li-td-center">';

                            if (elm.status == 1) {
                                
                                ulData += '<a href="javascript:" onclick="addNextClass('+elm.id+')">添加下级</a>' +
                                    '<a href="javascript:" onclick="editClass('+elm.id+','+elm.parent_id+')">修改</a>'+
                                    '<a href="javascript:;" onclick="disClass('+elm.id+',2)">禁用</a>';
                            }else{
                                ulData += '<a href="javascript:;" onclick="disClass('+elm.id+',1)">启用</a>';
                            }

                            ulData+='</span></div>';
                        }

                        ulData+='</li>';

                });
                ulData +="</ul>";
                $(el).parent('li').append(ulData);
                 _self.find('i').removeClass('icon2-shixinyou');
                _self.find('i').addClass('icon2-shixinxia');
            })
        }else{
            if(_self.siblings('ul').is(':hidden')){
                _self.find('i').removeClass('icon2-shixinyou');
                _self.find('i').addClass('icon2-shixinxia');
                _self.siblings('ul').show();

            }else{
                _self.find('i').addClass('icon2-shixinyou');
               _self.find('i').removeClass('icon2-shixinxia');
                _self.siblings('ul').hide();
            }
        }

    }

</script>
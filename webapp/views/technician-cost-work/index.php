<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = '结算管理';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
body{background: #f1f2f7}
body,h4,h5,p,div{font-family: "微软雅黑"}
a:hover{text-decoration: none;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">结算管理</span>
    <!-- <span class="btn btn-orange"><i class="fa fa-plus icon-tianjiajiahaowubiankuang icon iconfont"></i>&nbsp;<a href="/cost/apply" style="color: #fff">申请结算</a></span> -->
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
                <div class="col-md-12 col-sm-12" style="margin-bottom: 30px">
                    <h4>商家结算</h4>
                    <div  class="settlement-box col-md-5 col-xs-5">
                           <h5 class="cost-title">商家结算单</h5>
                           <p class="cost-jinge">已结算金额：<span>10000.00</span></p>
                           <div style="margin-top: 20px;">
                               <div class="col-md-4 col-sm-4">
                                    <a href="javascript:" class="numberbox">
                                        <p style="color: #00a2ff">9</p>
                                        <span>待对账</span>
                                    </a>
                                   
                               </div>
                               <div class="col-md-4 col-sm-4">
                                    <a href="javascript:" class="numberbox numberbox-auto">
                                        <p style="color: #ff5c5e">9</p>
                                        <span>待审核</span>
                                    </a>
                               </div>
                               <div class="col-md-4 col-sm-4">
                                    <a href="javascript:" class="numberbox numberbox-right">
                                        <p style="color: #ffa94e">9</p>
                                        <span>已结算</span>
                                    </a>
                               </div>
                           </div>
                    </div>
                    <div class="settlement-box col-md-5 col-xs-5 col-md-offset-1 col-xs-offset-1">
                        <h5 class="cost-title">商家订单结算</h5>
                           <p class="cost-jinge">已结算金额：<span>10000.00</span></p>
                           <div style="margin-top: 20px;">
                               <div class="col-md-4 col-sm-4">
                                    <a  href="javascript:" class="numberbox">
                                        <p style="color: #01b485">9</p>
                                        <span>待对账</span>
                                    </a>
                                   
                               </div>
                               <div class="col-md-4 col-sm-4">
                                    <a  href="javascript:" class="numberbox numberbox-auto">
                                        <p style="color: #ffa94e">9</p>
                                        <span>待审核</span>
                                    </a>
                               </div>
                               <div class="col-md-4 col-sm-4">
                                    <a  href="javascript:" class="numberbox numberbox-right">
                                        <p style="color: #ff5c5e">9</p>
                                        <span>已结算</span>
                                    </a>
                               </div>
                           </div>
                    </div>
                    <div class="col-md-12 col-sm-12" style="padding-left: 0;margin-top: 30px;">
                           <a href="/cost/apply" class="btn btn-orange" style="color: #fff">申请结算</a>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12"><p class="xian"></p></div>
                <div class="col-md-12 col-sm-12" style="margin-top: 30px">
                    <h4>技师结算</h4>
                    <div  class="settlement-box col-md-5 col-xs-5">
                           <h5 class="cost-title">技师结算单</h5>
                           <p class="cost-jinge">已结算金额：<span>10000.00</span></p>
                           <div style="margin-top: 20px;">
                               <div class="col-md-4  col-sm-4">
                                    <a  href="javascript:" class="numberbox">
                                        <p style="color: #00a2ff">9</p>
                                        <span>待对账</span>
                                    </a>
                                   
                               </div>
                               <div class="col-md-4 col-sm-4">
                                    <a  href="javascript:" class="numberbox numberbox-auto">
                                        <p style="color: #ff5c5e">9</p>
                                        <span>待审核</span>
                                    </a>
                               </div>
                               <div class="col-md-4 col-sm-4">
                                    <a  href="javascript:" class="numberbox numberbox-right">
                                        <p style="color: #ffa94e">9</p>
                                        <span>已结算</span>
                                    </a>
                                   
                               </div>
                           </div>
                           
                    </div>
                    <div class="settlement-box col-md-5 col-xs-5 col-md-offset-1 col-xs-offset-1">
                           <h5 class="cost-title">技师工单结算</h5>
                           <p class="cost-jinge">已结算金额：<span>10000.00</span></p>
                           <div style="margin-top: 20px;">
                               <div class="col-md-4  col-sm-4">
                                    <a  href="javascript:" class="numberbox">
                                        <p style="color: #01b485">9</p>
                                        <span>待对账</span>
                                    </div>
                                   
                               </a>
                               <div class="col-md-4 col-sm-4">
                                    <a  href="javascript:" class="numberbox numberbox-auto">
                                        <p style="color: #ffa94e">9</p>
                                        <span>待审核</span>
                                    </a>
                               </div>
                               <div class="col-md-4 col-sm-4">
                                    <a  href="javascript:" class="numberbox numberbox-right">
                                        <p style="color: #ff5c5e">9</p>
                                        <span>已结算</span>
                                    </a>
                                   
                               </div>
                           </div>
                    </div>
                    <div class="col-md-12  col-sm-12" style="padding-left: 0;margin: 30px 0 60px;">
                           <a href="javascript:" class="btn btn-orange" style="color: #fff">申请结算</a>
                    </div>
                </div>
        </div>
    </div>
</section>
<script>
       $(function(){
        fontSize ();
        })
        $(window).resize(function (){
            fontSize ();
        });
        function fontSize () {
            var whdef = 100/1920;
             var wH = window.innerHeight;
             var wW = window.innerWidth;
             var rem = wW * whdef;
             $('html').css('font-size', rem + "px");
        }
</script>
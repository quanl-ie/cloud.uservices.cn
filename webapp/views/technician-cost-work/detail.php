<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$this->title = '结算单详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    th{text-align: center;}
    .details-column-title{font-weight: 600;width: 120px}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">结算单详情 </span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <!-- 内容 -->
                    <div id="" class="tab-content col-md-12">
                        <div class="details-column">
                            <span class="details-column-title">商家名称：</span>
                            <div class="details-column-content">
                                <?php echo $data['company'];?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">结算时间段：</span>
                            <div class="details-column-content">
                                <?php echo date('Y-m-d',$data['cost_start_time'])-date('Y-m-d',$data['cost_end_time']);?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">状态：</span>
                            <div class="details-column-content">
                                <?php if($data['status']==1) :?>
                                    <span class="red-i">
                                        待对账
                                    </span>
                                <?php elseif($data['status']==2):?>
                                    <span class="2693ff">
                                        通过对账
                                    </span>
                                <?php elseif($data['status']==3):?>
                                    <span class="red-i">
                                        生成中
                                    </span>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">收费项目金额：</span>
                            <div class="details-column-content">
                                <div class="dts-list-xq">
                                    <table>
                                        <thead>
                                            <tr style="background: #f8f8f8">
                                                <th>收费项目</th>
                                                <th>收费标准</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>调试费</td>
                                                <td>2元</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">对账明细： </span>
                            <div class="details-column-content">
                                <a href="javascript:" class="btn btn-orange" style="color: #fff">
                                    导出
                                </a>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">创建时间： </span>
                            <div class="details-column-content">
                                <?=date('Y-m-d H:i:s',$data['create_time'])?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">总计金额： </span>
                            <div class="details-column-content">
                                <?=sprintf('%0.2f',$data['order_amount'])?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">预计结算金额： </span>
                            <div class="details-column-content">
                                <?=sprintf('%0.2f',$data['expect_cost_amount'])?>
                            </div>
                        </div>
                         <div class="details-column">
                            <span class="details-column-title">备注： </span>
                            <div class="details-column-content">
                                <!-- 备注内容 -->
                                <div>
                                    急啊立刻就嘎考虑卡拉胶哈快乐和啊快乐哈可理解哈快乐就好可垃圾啊会立刻就会急啊立刻就嘎考虑卡拉胶哈快乐和啊快乐哈可理解哈快乐就好可垃圾啊会立刻就会
                                    急啊立刻就嘎考虑卡拉胶哈快乐和啊快乐哈可理解哈快乐就好可垃圾啊会立刻就会急啊立刻就嘎考虑卡拉胶哈快乐和啊快乐哈可理解哈快乐就好可垃圾啊会立刻就会
                                </div>
                                <!-- 下载内容 -->
                                <div class="download">
                                    <div class="download-title">
                                        <i class=""></i>
                                        附件（1个）
                                    </div>
                                    <div class="download-list">
                                        <div  class="download-list-left">
                                            <img src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" alt="placeholder+image">
                                        </div>
                                        <div  class="download-list-right">
                                            <span>异常订单列表.pdf</span>
                                            <a href="javascript:">下载</a>
                                        </div>
                                    </div>
                                    <div class="download-list">
                                        <div  class="download-list-left">
                                            <img src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" alt="placeholder+image">
                                        </div>
                                        <div  class="download-list-right">
                                            <span>异常订单列表.pdf</span>
                                            <a href="javascript:">下载</a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
    <img src="" id="enlargeImg" style="display: none">
</section>
<script>

    // 图片放大
    $('.enlarge-img').on('click',function() {
        var aa = $(this).attr('src');
        var bb = aa.substring(0,aa.indexOf('?'));
        $('#enlargeImg').attr('src',bb);
        setTimeout(function() {
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: '516px',
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#enlargeImg')
            },300);
        })

    })
    //查看技能
    $(".viewSkill").click(function(){
        var id = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['技能信息', 'font-size:18px;'],
            area: ['700px', '400px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/technician/get-skill?id='+id,
        });
        return false;
    });
    //查看实时位置
    $(".viewLocation").click(function(){
        var id = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['技师位置', 'font-size:18px;'],
            area: ['800px', '500px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/technician/view-location?id='+id,
        });
        return false;
    });
    var index = parent.layer.getFrameIndex(window.name);
    //修改技师位置
    $(".EditArea").click(function(){
        var id = $(this).attr("id-data");
        $.ajax({
            url:'/technician/verify',
            data:{id:id},
            type:'POST',
            dataType:'json',
            success:function(data){
                if (data.success == false) {
                    layer.confirm(data.message, {
                        btn: ['立即设置'] //按钮
                    }, function(){
                        location.href='/technician/edit-area?id='+id;
                    });
                }else{
                    location.href='/technician/edit-area?id='+id;
                }
            }
        });
        return false;
    });
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use webapp\models\BrandQualification;
    use yii\helpers\ArrayHelper;
    /* @var $this yii\web\View */
    /* @var $model webapp\models\BrandQualification */
    $this->title = '出库单详情';
    $this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>"
      xmlns="http://www.w3.org/1999/html">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .information-list {margin-top: 0;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title" id="no" data-no="">出库单详情</span>
    <div class="right-btn-box">
        <?php if(in_array('/send-out/add',$selfRoles)):?>
            <?php if(isset($stockInfo['isship']) && $stockInfo['isship'] == 1 && $ship_sys == 1 && $stockInfo['audit_status'] == 2 && !in_array($stockInfo['stock_type'],[2,4,5])){?>
                <a href="/send-out/add?id=<?=isset($stockInfo['id']) ? $stockInfo['id'] : ""?>">
                    <button class="btn btn-orange" >发货</button>
                </a>
            <?php }?>
        <?php endif;?>
         <button class="btn bg-f7" onclick="javascript:location.href='/stock-out/wait-check';" >返回</button>
    </div>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h5 class="block-h5 block-h5one">出库单信息</h5>
                    <div class="information-list" style="height: auto;">
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">出库单单号：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['no']) ? $stockInfo['no'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">出库主题：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['subject']) ? $stockInfo['subject'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">出库类型：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['stock_type_desc']) ? $stockInfo['stock_type_desc'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">出库状态：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['audit_status_desc']) ? $stockInfo['audit_status_desc'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">申请人：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['apply_user_name']) ? $stockInfo['apply_user_name'] : "" ?></span></div>
                        </div>
                        
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">申请时间：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['create_time']) ? $stockInfo['create_time'] : "" ?></span></div>
                        </div>
                        <?php if (isset($stockInfo['audit_user_id'])): ?>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">审批人：</label>
                            <div  class="right-title3 2693ff"><?=isset($stockInfo['audit_user_name']) ? $stockInfo['audit_user_name'] : "" ?></div>
                        </div>
                        
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">审批时间：</label>
                            <div class="right-title3"><?=isset($stockInfo['audit_date']) ? $stockInfo['audit_date'] : "" ?></div>
                        </div>
                        <?php endif; ?>
                        <div class="col-xs-12	col-sm-12	col-md-12	col-lg-12">
                            <label  class="left-title80">备注：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['description']) ? $stockInfo['description'] : "" ?></span></div>
                        </div>
                        <?php if (isset($stockInfo['audit_user_id'])): ?>

                        <div class="col-xs-12	col-sm-12	col-md-12	col-lg-12">
                            <label  class="left-title80">审批意见：</label>
                            <div class="right-title3"><?=isset($stockInfo['audit_suggest']) ? $stockInfo['audit_suggest'] : "" ?></div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <h5 class="block-h5 block-h5one">提货信息</h5>
                    <div class="information-list" style="height: auto;">
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">提货人姓名：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['pick_name']) ? $stockInfo['pick_name'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">提货人电话：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['pick_mobile']) ? $stockInfo['pick_mobile'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">身份证号：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['pick_card']) ? $stockInfo['pick_card'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">车牌号：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['pick_license']) ? $stockInfo['pick_license'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">提货归属部门：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['pick_department_name']) ? $stockInfo['pick_department_name'] : "" ?></span></div>
                        </div>
                    </div>
                    <h5 class="block-h5" style="padding-bottom: 10px;">产品明细</h5>
                    <div class="information-list" style="height: auto">
                        <table>
                            <thead>
                            <tr>
                                <th>产品名称</th>
                                <th>产品品牌</th>
                                <th>库房</th>
                                <th>计量单位</th>
                                <th>数量</th>
                                <th>序列号</th>
<!--                                <th>生产日期</th>-->
<!--                                <th>有效日期</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($stockDetail)) : ?>
                                <?php foreach ($stockDetail as $val) : ?>
                                    <tr>
                                        <td><?=isset($val['prod_name'])?$val['prod_name']:"" ?></td>
                                        <td><?=isset($val['brand_name'])?$val['brand_name']:"" ?></td>
                                        <td><?=isset($val['depot_name'])?$val['depot_name']:"" ?></td>
                                        <td><?=isset($val['unit_name'])?$val['unit_name']:"" ?></td>
                                        <td><?=isset($val['prod_num'])?$val['prod_num']:"" ?></td>
                                        <td><?=isset($val['serial_number'])?$val['serial_number']:"" ?></td>
<!--                                        <td>--><?//=isset($val['prod_date'])?$val['prod_date']:"" ?><!--</td>-->
<!--                                        <td>--><?//=isset($val['effective_date'])?$val['effective_date']:"" ?><!--</td>-->
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <?php if(isset($stockInfo['isship']) && !empty($shipInfo) || $stockInfo['isship']=='2' || $stockInfo['isship']=='3'){?>
                        <h5 class="block-h5" style="padding-bottom: 10px;">发货单</h5>
                        <div class="information-list" style="height: auto">
                            <table>
                                <thead>
                                <tr>
                                    <th>发货单号</th>
                                    <th>发货状态</th>
                                    <th>发货产品数</th>
                                    <th>发货时间</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <a href="/send-out/view?id=<?=isset($shipInfo['id'])?$shipInfo['id']:"" ?>"><?php echo isset($shipInfo['send_no'])?$shipInfo['send_no']:"";?></a>
                                    </td>
                                    <td><?=isset($shipInfo['send_status_desc'])?$shipInfo['send_status_desc']:"" ?></td>
                                    <td><?=isset($shipInfo['send_prod_num'])?$shipInfo['send_prod_num']:"" ?></td>
                                    <td><?=isset($shipInfo['create_time'])?$shipInfo['create_time']:"" ?></td>
                                </tr>
                                </tbody>
                        </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    }
</script>
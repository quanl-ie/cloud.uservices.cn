<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use webapp\models\BrandQualification;
    use yii\helpers\ArrayHelper;
    /* @var $this yii\web\View */
    /* @var $model webapp\models\BrandQualification */
    $this->title = '审批';
    $this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .information-list {margin-top: 0;}
</style>


<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">审批</span>
    <div class="right-btn-box">
        <!--<a href="javascript:" class="cancelOrder">
            <button class="btn btn-orange bg-f7">打印</button>
        </a>-->
        <a href="javascript:" class="confirmRefuseVisit" data-id="<?=isset($stockInfo['id'])?$stockInfo['id']:'' ;?>" data-status="3">
            <button class="btn btn-orange bg-f7">拒绝</button>
        </a>
        <a href="javascript:" class="confirmAgreeVisit" data-id="<?=isset($stockInfo['id'])?$stockInfo['id']:'' ;?>" data-status="2">
            <button class="btn btn-orange bg-f7">同意</button>
        </a>
        <button class="btn bg-f7" onclick="javascript:location.href='/stock-out/wait-check';" >返回</button>
    </div>
</div>

<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h5 class="block-h5 block-h5one" style="margin-top: 0">出库单信息</h5>
                    <div class="information-list">
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">出库单单号：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['no'])?$stockInfo['no']:"" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">出库主题：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['subject']) ? $stockInfo['subject'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">出库类型：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['stock_type_desc'])?$stockInfo['stock_type_desc']:"" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">出库状态：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['audit_status_desc'])?$stockInfo['audit_status_desc']:"" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">申请人：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['apply_user_name'])?$stockInfo['apply_user_name']:"" ?></span></div>
                        </div>

                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">申请时间：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['create_time'])?$stockInfo['create_time']:"" ?></span></div>
                        </div>
                        <?php if (isset($stockInfo['audit_user_id'])): ?>
                            <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                                <label   class="left-title80">审批人：</label>
                                <div  class="right-title3 2693ff"><span><?=isset($stockInfo['audit_user_name'])?$stockInfo['audit_user_name']:"" ?></span></div>
                            </div>

                            <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                                <label  class="left-title80">审批时间：</label>
                                <div class="right-title3"><span><?=isset($stockInfo['audit_date'])?$stockInfo['audit_date']:"" ?></span></div>
                            </div>
                        <?php endif; ?>
                        <div class="col-xs-12	col-sm-12	col-md-12	col-lg-12">
                            <label  class="left-title80">备注：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['description'])?$stockInfo['description']:"" ?></span></div>
                        </div>
                    </div>
                    <h5 class="block-h5 block-h5one">提货信息</h5>
                    <div class="information-list" style="height: auto;">
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">提货人姓名：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['pick_name']) ? $stockInfo['pick_name'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">提货人电话：</label>
                            <div class="right-title3"><span><?=isset($stockInfo['pick_mobile']) ? $stockInfo['pick_mobile'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">身份证号：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['pick_card']) ? $stockInfo['pick_card'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">车牌号：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['pick_license']) ? $stockInfo['pick_license'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label   class="left-title80">提货归属部门：</label>
                            <div  class="right-title3"><span><?=isset($stockInfo['pick_department_name']) ? $stockInfo['pick_department_name'] : "" ?></span></div>
                        </div>
                    </div>
                    <?php if (isset($stockInfo['audit_user_id'])): ?>
                        <div class="information-list">
                            <div class="col-xs-12	col-sm-12	col-md-12	col-lg-12">
                                <label  class="left-title80">审批意见：</label>
                                <div class="right-title3"><span><?=isset($stockInfo['audit_suggest'])?$stockInfo['audit_suggest']:"" ?></span></div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <h5 class="block-h5" style="padding-bottom: 15px;">产品明细</h5>
                    <div class="information-list" style="height: auto">
                        <table>
                            <thead>
                            <tr>
                                <th>产品名称</th>
                                <th>产品品牌</th>
                                <th>库房</th>
                                <th>计量单位</th>
                                <th>数量</th>
                                <th>可用库存</th>
                                <th style="width: 160px">序列号</th>
                                <!--                                <th>生产日期</th>-->
                                <!--                                <th>有效日期</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($stockDetail)) : ?>
                                <?php foreach ($stockDetail as $val) : ?>
                                    <tr>
                                        <td><?=isset($val['prod_name'])?$val['prod_name']:"" ?></td>
                                        <td><?=isset($val['brand_name'])?$val['brand_name']:"" ?></td>
                                        <td>
                                            <select class="depot_id" name="prod[<?=$val['prod_id']?>][depot_id]" onchange="changeDepot(this,<?=$val['prod_id']?>)" data-id="<?=$val['id']?>">
                                                <option value="-1">请选择</option>
                                                <?php if (!empty($depot)) : ?>
                                                    <?php foreach ($depot as $k => $v) : ?>
                                                        <option value="<?=$k ?>" <?php if ($val['depot_id'] == $k) : ?> selected="selected" <?php endif; ?> ><?=$v ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </td>
                                        <td><?=isset($val['unit_name'])?$val['unit_name']:"" ?></td>
                                        <td class="prod_num"><?=isset($val['prod_num'])?$val['prod_num']:"" ?></td>
                                        <td class="depot_num"><?=isset($val['depot_num'])?$val['depot_num']:"0" ?></td>
                                        <td style="width: 160px">
                                            <input class="serial_number" name="prod[<?=$val['prod_id']?>][serial_number]" value="<?=isset($val['serial_number'])?$val['serial_number']:"" ?>" type="text" maxlength="10" />
                                        </td>
                                        <!--                                        <td>--><?//=isset($val['prod_date'])?$val['prod_date']:"" ?><!--</td>-->
                                        <!--                                        <td>--><?//=isset($val['effective_date'])?$val['effective_date']:"" ?><!--</td>-->
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- 审批拒绝弹层 -->
<div id="refuse" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <!-- <p style="margin:0 0 10px 0;text-align: left;"></p> -->
        <div class="select-fuji">
            <textarea placeholder="请填写审批意见，必填" id="auditSuggest" style="width: 100%;height: 100px;" onkeyup=" wordsLimit(this)"></textarea>
            <p class="xianzhi" style="right: 10px;"><span id="word">0</span>/200</p>
        </div>
        <div style="text-align: center;margin-top:15px;">
            <button class="btn btn-orange mgr20" id="layerSubmit"><i class="fa fa-search"></i>确定</button>
            <button class="btn bg-f7" id="layerCancel"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>
<!--审批同意弹层-->
<div id="agree" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <!-- <p style="margin:0 0 10px 0;text-align: left;">确认同意出库申请吗？</p> -->
        <div class="select-fuji">
            <textarea placeholder="请填写审批意见，非必填" id="auditSuggest" style="width: 100%;height: 100px;" onkeyup=" wordsLimit(this)"></textarea>
            <p class="xianzhi" style="right: 10px;"><span id="word">0</span>/200</p>
        </div>
        <div style="text-align: center;margin-top:15px;">
            <button class="btn btn-orange mgr20" id="layerSubmit"><i class="fa fa-search"></i>确定</button>
            <button class="btn bg-f7" id="layerCancel"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>

<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>

        //提示
        $(".tip").popover();
    }

    var layerIndex = null;
    var id = '';
    var auditStatus = '';
    var requestArray = [];//产品详细id数组

    //拒绝弹框
    $(".confirmRefuseVisit").click(function () {
        layerIndex = layer.open({
            type: 1,
            title: '拒绝出库申请？',
            area: ['400px', '250px'],
            fixed: true,
            maxmin: false,
            content: $('#refuse').html()
        });
        id = $(this).attr('data-id');
        auditStatus = $(this).attr('data-status');
        return false;
    });

    //同意弹框
    $(".confirmAgreeVisit").click(function () {
        id = $(this).attr('data-id');
        auditStatus = $(this).attr('data-status');
        var productDetailed = $('tbody tr');
        for (var i = 0; i < productDetailed.length; i++) {
            if($(productDetailed[i]).find('.depot_id').val()=='-1'){
                alert('请选择库房');
                return false;
            }

            var depotNum = $(productDetailed[i]).find('.depot_num').text();
            var prodNum = $(productDetailed[i]).find('.prod_num').text();
            if(depotNum - prodNum < 0){
                alert('库存不足请重新选择库房');
                return false;
            }
            requestArray[i] = {
                detail_id:$(productDetailed[i]).find('.depot_id').attr('data-id'),
                depot_id:$(productDetailed[i]).find('.depot_id').val(),
                serial_number:$(productDetailed[i]).find('.serial_number').val()

            };
        };
        //修改库房与序列号
        $.ajax({
            url:'/stock-out/edit',
            type:'POST',
            data:{id:id,depot:requestArray},
            dataType:'json',
            success:function (data) {
                if(data.code != 200){
                    alert(data.message);
                    return false;
                }
                //修改成功后弹出
                layerIndex = layer.open({
                    type: 1,
                    title: '确认同意出库申请吗？',
                    area: ['400px', '250px'],
                    fixed: true,
                    maxmin: false,
                    content: $('#agree').html()
                });
                return false;
            }
        })
    });

    //提交审核数据
    $("body").delegate("#layerSubmit","click",function(){
        var auditSuggest = $(this).parents('div').find('#auditSuggest').val();
        if($.trim(id) == ''){
            alert('出库单号获取失败');
            return false;
        }
        if($.trim(auditStatus) == ''){
            alert('审批状态获取失败');
            return false;
        }

        if(auditStatus == 3 && $.trim(auditSuggest) == ''){
            alert('请填写审批意见');
            return false;
        }
        $.ajax({
            url:'/stock-out/examine',
            type:'POST',
            data:{id:id,auditStatus:auditStatus,auditSuggest:auditSuggest},
            dataType:'json',
            success:function (data) {
                if(data.code == 200){
                    if(layerIndex){
                        layer.close(layerIndex);
                    }
                    alert(data.message);
                    setTimeout(function(){location.href = '/stock-out/view?id='+id},1000);
                }else{
                    alert(data.message);
                    return false;
                }
            }

        });

    });
    //关闭弹层
    $("body").delegate("#layerCancel","click",function(){
        if(layerIndex){
            layer.close(layerIndex);
        }
    });
    //动态获取可用库存
    function changeDepot(obj,prodId) {
        var depotId = obj.value;
        $.getJSON('/stock-out/get-depot-num',{depotId:depotId,prodId:prodId},function (data) {
            if (data.code == 200) {
                $(obj).parents('tr').find('.depot_num').html(data.data);
            }
        });
    }
</script>
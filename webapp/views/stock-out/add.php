<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = '创建出库单';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">创建出库单</span>
</div>
<!--引出CSS-->
<?= Html::cssFile('@web/webuploader/webuploader.css') ?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/webuploader/webuploader.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jquery.autocomplete.css'); ?>">
<!--引出JS-->
<?= Html::jsFile('@web/webuploader/webuploader.js') ?>
<?= Html::jsFile('@web/js/bootstrap-select.js') ?>
<section id="main-content">
    <div class="panel-body"><?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal',
                'id' => 'stockOut',
                'name' => 'form1',
                'enctype' => 'multipart/form-data',
            ],
            'fieldConfig' => [
                'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-xs-6\">{input}{error}</div>",
                'labelOptions' => ['class' => 'col-xs-2 control-label text-right'],
            ]
        ]);
        ?>
        <div class="col-md-12">
            <h5 class="block-h5 block-h5one">出库单信息</h5>
        </div>

        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
                出库单号：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                <input type="hidden" name="type" value="<?= isset($data['type']) ? $data['type'] : "" ?>">
                <input type="text" name="no" class="form-control" id="no"
                       value="<?= isset($data['no']) ? $data['no'] : "" ?>" readonly="readonly" placeholder="">
            </div>
            <div class="col-md-offset-2 col-md-6 misplacement" style="display:none" id="subjectct_msg">
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
                出库主题：
            </label>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                <input type="text" name="subject"  class="form-control" maxlength="30" id="subject"
                       value="<?= isset($data['rel_name']) ? $data['rel_name'] : ''; ?>" placeholder="">
            </div>
        </div>

        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
                <i class="red-i">*</i> 出库类型：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                <select name="stock_type" id="stock_type" datatype="*"
                        <?php if (isset($data['stock_type'])) : ?>disabled="disabled" <?php endif; ?>
                        class="form-control" sucmsg=" " nullmsg="请选择出库类型" errormsg="请选择出库类型">
                    <option value="">选择类型</option>
                    <?php if (!empty($stockType)) : ?>
                        <?php foreach ($stockType as $key => $val) : ?>
                            <option value="<?= $key ?>" <?php if (isset($data['stock_type']) && $data['stock_type'] == $key) : ?> selected="selected" <?php endif; ?>><?= $val ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>

            <div class="col-md-offset-2 col-md-6 misplacement" style="display:none" id="subjectct_msg">
            </div>
        </div>

        <?php if (isset($data['rel_type'])) : ?>
            <div class="information-list">
                <label class="name-title-lable" for="workorder-account_id">
                    关联单：
                </label>
                <span class="c_red"></span>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                    <input type="hidden" name="rel_type"
                           value="<?= isset($data['rel_type']) ? $data['rel_type'] : "" ?>">
                    <input type="hidden" name="rel_id" value="<?= isset($data['rel_id']) ? $data['rel_id'] : "" ?>">
                    <input type="hidden" name="rel_name"
                           value="<?= isset($data['rel_name']) ? $data['rel_name'] : "" ?>">
                    <?php if ($data['rel_type'] == 4) { ?>
                        <a href="JavaScript:void(0)"
                           onclick='openPage("/exchange-goods/detail?id=<?= $data['rel_id']; ?>","换货详情")'
                           class="line-height34"><?= isset($data['rel_name']) ? $data['rel_name'] : "" ?></a>
                    <?php } elseif ($data['rel_type'] == 2) { ?>
                        <a href="JavaScript:void(0)"
                           onclick='openPage("/contract/detail?id=<?= $data['rel_id']; ?>&contract_id=<?= $data['rel_id']; ?>","合同详情")'
                           class="line-height34"><?= isset($data['rel_name']) ? $data['rel_name'] : "" ?></a>
                    <?php } elseif ($data['rel_type'] == 5) { ?>
                        <a href="JavaScript:void(0)"
                           onclick='openPage("/tech-prod-stock/detail?id=<?= $data['rel_id']; ?>&contract_id=<?= $data['rel_id']; ?>","详情")'
                           class="line-height34"><?= isset($data['rel_no']) ? $data['rel_no'] : "" ?></a>
                    <?php } elseif ($data['rel_type'] == 7) { ?>
                        <a href="JavaScript:void(0)"
                           onclick='openPage("/return-purchase/view?id=<?= $data['rel_id']; ?>","详情")'
                           class="line-height34"><?= isset($data['rel_no']) ? $data['rel_no'] : "" ?></a>
                    <?php } ?>

                </div>

                <div class="col-md-offset-2 col-md-6 misplacement" style="display:none" id="subjectct_msg">
                </div>
            </div>
        <?php endif; ?>


        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
                申请人：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                <input type="hidden" name="apply_user_id"
                       value="<?= isset($data['apply_user_id']) ? $data['apply_user_id'] : "" ?>">
                <input type="text" name="apply_user_name"
                       value="<?= isset($data['apply_user_name']) ? $data['apply_user_name'] : "" ?>"
                       class="form-control" readonly="readonly" id="apply_user_name">
            </div>

            <div class="col-md-offset-2 col-md-6 misplacement" style="display:none" id="subjectct_msg">
            </div>
        </div>

        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
                备注：
            </label>
            <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7 addinput-list">
                <textarea class="ramark-box" id="ramark" maxlength="200" placeholder="请输出备注信息（200字以内）"
                          name="description" id="description" onkeyup=" wordsLimit(this)"></textarea>
                <p class="xianzhi" style="right: 20px"><span id='word'>0</span>/200</p>
            </div>
        </div>
        <div class="col-md-12">
            <h5 class="block-h5 block-h5one">提货信息</h5>
        </div>
        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
                提货人：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list">
                <select class="form-control" id="pick_user_id" value="" name="pick_user_id" sucmsg=" " nullmsg="">
                    <?php if (!empty($pickList)) foreach ($pickList as $key => $val) { ?>
                        <option value="<?php echo $val['id'] ?>"><?php echo $val['pick_name'] . '/' . $val['pick_mobile'] . '/' . $val['pick_card'] . '/' . $val['pick_license'] ?></option>
                    <?php } else { ?>
                        <option value="">请选择提货人</option>
                    <?php } ?>
                </select>
                <p class="misplacement" id="pick_user_id_msg"></p>
            </div>
            <div class="col-lg-1 col-md-2 col-sm-2 col-xs-2 add-btn padding-left0" style="margin-left: 10px">
                <span class="btn btn-success  " style=" color:#FFF;font-size:14px; " id="pick_user_info"
                      data-toggle="modal" data-target="#scrollingModal"
                      title="<?php if (isset($data['account']['id'])) echo $data['account']['id']; ?>">添加</span>
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
                <i class="red-i"></i> 提货归属部门：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 addinput-list region-limit">
                <input type="text" class="form-control"
                       value="<?= isset($department_edit['name']) ? $department_edit['name'] : $define_department['name'] ?>"
                       id="category1" placeholder="请选择上级机构" readonly sucmsg="" nullmsg="请选择机构">
                <input type="hidden" id="select_department_id"
                       value="<?= isset($department_edit['id']) ? $department_edit['id'] : $define_department['id'] ?>"
                       name="department_id">
                <?php if ($auth != 1) { ?>
                    <i class="icon iconfont icon-shouqi1"></i>
                    <p class="misplacement" style="bottom: -5px;" id="work_type_msg">请选择上级机构</p>
                    <div class="drop-dw-layerbox">
                        <div class="ul-box-h180">
                            <ul>
                                <?php if (!empty($department)) : ?>
                                    <?php foreach ($department as $key => $val) : ?>
                                        <li>
                                           <span>
                                              <?php if ($val['exist'] == 1) : ?>
                                                  <i class="icon2-shixinyou" onclick="clickCategory(this) "></i>
                                                  <input type="radio" value="<?= $val['id'] ?>"
                                                         name="department_id" id="<?= $val['id'] ?>" class="pr1"
                                                         onclick="changeCategory(this)">
                                              <?php else: ?>
                                                  <input type="radio" value="<?= $val['id'] ?>"
                                                         name="department_id" id="<?= $val['id'] ?>" class="pr1"
                                                         onclick="changeCategory(this)">
                                              <?php endif; ?>
                                               <label for="<?= $val['id'] ?>"><?= $val['name'] ?></label>
                                          </span>
                                        </li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="col-md-12">
            <h5 class="block-h5">产品明细</h5>
        </div>

        <div class="information-list">
            <div class="col-xs-12" style="margin-bottom: 30px">
                <table style="width: 100%;font-size: 12px;">
                    <thead>
                    <tr style="font-size: 14px;">
                        <th style="width: 50px;" class="padding15">
                            <input type="checkbox" name="" id="allDetailed">
                            <label for="allDetailed"> <i class="gou-i"></i></label>
                        </th>
                        <th>产品名称</th>
                        <th>产品品牌</th>
                        <th>库房</th>
                        <th>计量单位</th>
                        <th width="80px">数量</th>
                        <th min-width="80px">可用库存</th>
                        <th width="160px">序列号</th>
                        <!--                                    <th>生产日期</th>-->
                        <!--                                    <th>有效日期</th>-->
                    </tr>
                    </thead>
                    <tfoot id="show_prodcut">

                    <?php if (!empty($product)) : ?>
                        <?php foreach ($product as $key => $val): ?>
                            <?php if ((isset($val['type']) && $val['type'] == 2) || !isset($val['type'])): ?>
                                <tr id="<?= $val['prod_id']; ?>">

                                    <td class="padding15">
                                        <input type="checkbox" class="available_cost_type select_product"
                                               name="prod[<?= $val['prod_id'] ?>][prod_id]"
                                               id="prod_id<?= $val['prod_id'] ?>">
                                        <label for="prod_id<?= $val['prod_id'] ?>"> <i class="gou-i"></label>
                                    </td>
                                    <td style="max-width: 180px;">
                                        <input class="prod_id" value="<?= $val['prod_id'] ?>"
                                               name="prod[<?= $val['prod_id'] ?>][prod_id]"
                                               type="hidden"/><?= $val['prod_name'] ?>
                                    </td>
                                    <td><?= $val['brand_name'] ?></td>
                                    <td>
                                        <select class="depot_id" name="prod[<?= $val['prod_id'] ?>][depot_id]"
                                                onchange="changeDepot(this,<?= $val['prod_id'] ?>)" datatype="*"
                                                sucmsg=" " nullmsg="请选择库房" errormsg="请选择库房">
                                            <option value="">请选择</option>
                                            <?php if (!empty($depot)) : ?>
                                                <?php foreach ($depot as $k => $v) : ?>
                                                    <option value="<?= $k ?>"><?= $v ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                    <td><?= $val['unit_name'] ?></td>
                                    <td>
                                        <input class="prod_num" name="prod[<?= $val['prod_id'] ?>][prod_num]"
                                               type="text" datatype="n" sucmsg=" " nullmsg="请输入数量" errormsg="请输入正整数"
                                               value="<?= $val['prod_num'] ?>" maxlength="10" <?php if($data['stock_type'] == 9){ echo "readonly='true'";}?>/>
                                    </td>
                                    <td class="depot_num">0</td>
                                    <td>
                                        <input class="serial_number" name="prod[<?= $val['prod_id'] ?>][serial_number]"
                                               type="text"
                                               value="<?= isset($val['serial_number']) ? $val['serial_number'] : "" ?>"
                                               maxlength="10"/>
                                    </td>

                                    <!--                                        <td>-->
                                    <!--                                            <input class="prod_date" name="prod[-->
                                    <? //=$val['prod_id']?><!--][prod_date]" readonly="readonly" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" type="text" />-->
                                    <!--                                        </td>-->
                                    <!--                                        <td>-->
                                    <!--                                            <input class="effective_date" name="prod[-->
                                    <? //=$val['prod_id']?><!--][effective_date]" readonly="readonly" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" type="text"/>-->
                                    <!--                                        </td>-->
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <div id="stock_in_msg" class="Validform_checktip Validform_wrong" style="display:none;"></div>
                    </tfoot>
                    <div id="product_hide" style="display:none;">333</div>
                </table>
                <?php if (isset($data['stock_type']) == false || (isset($data['stock_type']) && ($data['stock_type'] != 2))): ?>
                    <div class="bottom-btn-box">
                        <a href="javascript:" class="btn btn-orange mgr8" id="addBtn">添加</a>
                        <a class="btn ignore delet-btn" href="javascript:">删除</a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-offset-2 col-md-6 misplacement" style="display:none" id="brand_msg">
            </div>
        </div>

        <hr style="clear: both;">
        <div class="information-list" style="text-align: center;">
            <a href="/stock-out/wait-check">
                <button type="button" class="btn ignore mgr20 ">返回</button>
            </a>
            <input type="submit" class="btn btn-orange btnSubmit" id="btnSt" style=" color:#FFF;" value="提交"/>
            <input type="button" class="btn btn-orange btnSubmiting" style="display: none;" value="提交中..."/>


        </div>
        <?php ActiveForm::end(); ?>
    </div>

</section>
<!-- 时间插件/ -->
<?= Html::jsFile('@web/js/WdatePicker/WdatePicker.js') ?>
<?= Html::jsFile('@web/js/WdatePicker/time.js') ?>
<script type="text/javascript" src="/js/jquery-migrate-1.1.0.js"></script>
<script src="/js/jquery.autocomplete.js" type="text/javascript"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>

    function openPage(url,title) {
        window.parent.postMessage(JSON.stringify({
            url: url,
            title: title,
        }), "*")
    }

    window.onload = function () {
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    }
    //单项选择
    $('.radio-li').on('click', function () {
        $(this).find('span').css({'background': '#2693ff', 'color': '#fff'});
        $(this).siblings('li').find('span').css({'background': '#fff', 'color': '#333', 'border-color': '#ccc'});
        $(this).parent('ul').next('input').val($(this).find('span').attr('id'));
    })
    // 选择
    $('.xiangxia').on('click', function () {
        $('.addinput-list-data').hide();
        $(this).next('.addinput-list-data').show();
    })
    //
    $('.addinput-list-data li').on('click', function () {
        $('.addinput-list-data').hide();
        $(this).parent('ul').siblings('.form-control').val($(this).attr('id '));
    })


    $("#addBtn").click(function () {
        var type = $("#stock_type").val();
        layer.open({
            type: 2,
            title: '选择产品',
            area: ['800px', '400px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/product/get-list?stock_type='+type,
            end: function () {
                var optionStr = '<option value="">请选择</option>';
                <?php if (!empty($depot)) : ?>
                <?php foreach ($depot as $key => $val) : ?>
                optionStr += '<option value="<?=$key ?>"><?=$val ?></option>';
                <?php endforeach; ?>
                <?php endif; ?>
                var str = $("#product_hide").html();
                data = eval("(" + str + ")");
                prod_id = data.data.id;
                html = '<td  class="padding15"><input type="checkbox" class="available_cost_type select_product" name="prod[' + prod_id + '][prod_id]"  id="prod_id' + prod_id + '" > ' +
                    '<label for="prod_id' + prod_id + '"> <i class="gou-i"></label> ' +
                    '</td>' +
                    '<td><input class="prod_id" value="' + prod_id + '" name="prod[' + prod_id + '][prod_id]"  type="hidden"/>' + data.data.prod_name + '</td>' +
                    '<td>' + data.data.brand_name + '</td>' +
                    '<td>' +
                    '<select class="depot_id" name="prod[' + prod_id + '][depot_id]" onchange="changeDepot(this,' + prod_id + ')" datatype="*"  sucmsg=" " nullmsg="请选择库房" errormsg="请选择库房">' + optionStr + '</select>' +
                    '</td>' +
                    '<td>' + data.data.unit_name + '</td>' +
                    '<td><input class="prod_num" datatype="n"  sucmsg=" " nullmsg="请输入数量" errormsg="请输入正整数" value="1" name="prod[' + prod_id + '][prod_num]" /></td>' +
                    '<td class="depot_num">' + data.data.depot_num + '</td>' +
                    '<td><input  class="serial_number"  name="prod[' + prod_id + '][serial_number]"   type="" maxlength="10" /></td>'
                // '<td><input class="prod_date" readonly="readonly" name="prod['+prod_id+'][prod_date]" id="datetimepicker-pay-top" onclick="WdatePicker({dateFmt:\'yyyy-MM-dd HH:mm:ss\'})" type="" maxlength="10" /></td>'+
                // '<td><input class="effective_date" readonly="readonly" name="prod['+prod_id+'][effective_date]" type="" id="datetimepicker-pay-end" onclick="WdatePicker({dateFmt:\'yyyy-MM-dd HH:mm:ss\'})" maxlength="10" /></td>'
                ;
                var show_product = $("#show_prodcut").html();

                if (show_product.length > 0) {
                    var haveExist = $("#" + data.data.id).find("#prod_id" + prod_id).val();
                    //勾选重复的产品不会显示
                    if (!haveExist) {
                        $("#show_prodcut").append('<tr id="' + data.data.id + '">' + html + '</tr>');
                    } else {
                        alert('产品重复添加');
                    }
                }
                $("#stock_in_msg").css("display", 'none');
                $(".prod_num").keyup(function () {
                    var prod_num = $(this).val();
                    if (isNaN(prod_num)) {
                        alert('请输入数字');
                        $(this).val(1);
                        return false;
                    }
                });
            }
        });
    })

    $('body').on('keyup', '.prod_num', function () {
        var prod_num = $(this).val();
        if (isNaN(prod_num)) {
            alert('请输入数字');
            $(this).val(1);
            return false;
        }
    });
    $('#allDetailed').on('change', function () {
        var amountText = $('.amount');
        var checkboxInput = $('tfoot input[type=checkbox]');
        var sum = 0, sum1 = 0;

        if ($(this).is(':checked')) {
            checkboxInput.prop("checked", true);
            amountText.each(function () {
                sum += Number($(this).text());
            })
            sum1 = sum + Number($('#otherExpenses').val());

        } else {
            sum1 = Number($('#otherExpenses').val());
            checkboxInput.prop("checked", false);
        }
        $('#allCost').text(sum1);
    })
    //表单提交验证
    $("#stockOut").Validform({
        tiptype: 3,
        // ajaxPost:true,
        beforeSubmit: function (curform) {
            var prodcutHidden = $("#show_prodcut").html();
            var productLength = 0;
            if (prodcutHidden.length > 0) {
                var checkboxInput = $(".select_product");
                var sum = 0;
                ids = new Array();
                $(".select_product").each(function (i) {
                    ids[i] = $(this).val();
                })
                var productLength = ids.length;
                if (productLength <= 0) {
                    $("#stock_in_msg").css("display", 'block');
                    $("#stock_in_msg").html('请选择产品');
                    return false;
                }
            }
            $("#stock_type").removeAttr('disabled');
            $("#btnSt").prop("disabled", true);

        },
        // callback:function(data){
        //     if (data.code == 200) {
        //         alert(data.message);
        //         setInterval(function () {
        //             location.href = '/stock-out/wait-check';
        //         },2000);
        //     }else{
        //         $.Hidemsg();
        //         alert(data.message);
        //     }
        // }
    });

    var deleteIds = '';
    //删除产品
    $(".delet-btn").click(function () {
        deleteIds = new Array();
        $(".select_product").each(function (i) {
            if ($(this).is(':checked')) {
                var id = $(this).parent().parent().attr('id');
                if (id > 0) {
                    deleteIds[i] = id;
                }
            }
        })
        if (deleteIds.length > 0) {
            confirm('确认删除产品吗?', function () {
                for (j = 0; j <= deleteIds.length; j++) {
                    $("#" + deleteIds[j]).remove();
                }
                $("#allDetailed").attr("checked", false);
            }, this)
        } else {
            alert('请选择需要删除的产品');
        }
    })

    //全选删除按钮
    $('#allDetailed').on('change', function () {
        var checkboxInput = $('tfoot input[type=checkbox]');
        var sum = 0, sum1 = 0;
        if ($(this).is(':checked')) {
            checkboxInput.prop("checked", true);
        } else {
            checkboxInput.prop("checked", false);
        }
    });

    //动态获取可用库存
    function changeDepot(obj, prodId) {
        var depotId = obj.value;
        var stockType = $("#stock_type").val();
        $.getJSON('/stock-out/get-depot-num', {depotId: depotId, prodId: prodId,stockType:stockType}, function (data) {
            if (data.code == 200) {
                $(obj).parents('tr').find('.depot_num').html(data.data);
            }
        });
    }

    //提示
    $(".tip").popover();

    //选择门店
    function clickCategory(el) {
        var auth = <?php echo $auth;?>;
        var id = $(el).next('input').val();
        var _this = $(el).parent('span');
        $.getJSON('/common/ajax-get-department', {'pid': id, 'auth': auth, 'type': 2}, function (data) {
            var ulData = '';
            if (_this.next('ul').size() == 0) {
                ulData += "<ul>"
                $(data.data).each(function (i, el) {
                    ulData += "<li><span>";
                    el.exist == 1 ? ulData += '<i class="icon2-shixinyou"  onclick="clickCategory(this)"></i>' : '';
                    ulData += '<input type="radio" value="' + el.id + '" name="department_id" id="' + el.id + '" class="pr1" onclick="changeCategory(this)"><label for="' + el.id + '">' + el.name + '</label></span></li>';
                });
                ulData += "</ul>";
                _this.parent('li').append(ulData);
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');

            } else {
                if (
                    _this.next('ul').is(':hidden')) {
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                } else {
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })

    }

    function changeCategory(el) {
        $('.drop-dw-layerbox').hide();
        $('#select_department_id').val($(el).next('label').attr("for"));   //父级机构id
        $('#category1').val($(el).next('label').text());
        //选择机构部门后获取用户列表
        /*$("#sale_user_id").html("<option value=''>请选择销售人</option>");
        var SelectDepartmentId = $('#select_department_id').val();
        if (SelectDepartmentId != '') {
            $.get("/user/get-user-list", {"pid": SelectDepartmentId}, function (data) {
                $("#sale_user_id").html(data);
            });
        }*/

    }

    // 点击空白处关闭
    $(document).click(function (event) {
        var region = $('.region-limit');
        if (!region.is(event.target) && region.has(event.target).length === 0) {
            $('.drop-dw-layerbox').hide();
        }
    });
    $('#category1').on('click', function (event) {
        event.preventDefault();
        /* $("#select_department_id").remove();
         $("#category1").val('');*/
        $('.drop-dw-layerbox').is(':hidden') ? $('.drop-dw-layerbox').show() : $('.drop-dw-layerbox').hide();
        $('.role').val('');
        $('#role_Select').val('');

    })
    //添加提取人地址
    var iframeCallbackAddressId = 0;
    $("#pick_user_info").click(function () {
        layer.open({
            type: 2,
            title: '编辑提货人',
            area: ['880px', '350px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/pick-user/index',
            end: function () {
                //请求获取最新的收货人列表信息
                get_pick_user_list()
            }
        });

        //获取最新的收货人列表信息
        function get_pick_user_list() {
            $.post("/pick-user/index", {}, function (data) {
                $("#pick_user_id").html(data);
            })
        }
    });
</script>


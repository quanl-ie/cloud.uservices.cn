<?php
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
    use webapp\models\Menu;
    
    $this->title = '评论设置';
    $this->params['breadcrumbs'][] = ['label' => '系统设置', 'url' => ['/sys/index']];
    $this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<style>
    .modal-content {border: none;border-radius: 0px;box-shadow: inherit !important;}
    .ck_role .checkbox{float:left;margin-right:10px;}
    .radio { float: left;margin-top: -5px;}
    .radio label, .checkbox label{padding-left: 0;padding-right: 30px;}
</style>
<section id="main-content">
    <div class="panel-body"><?php
            $form = ActiveForm::begin([
                'options' => [
                    'class' => 'form-horizontal',
                    'id' => 'appraisal-set',
                    'name' => 'appraisal-set',
                    'enctype' => 'multipart/form-data',
                
                ],
                'fieldConfig' => [
                    'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-xs-6 addinput-list\">{input}{error}</div>",
                    'labelOptions' => ['class' => 'name-title-lable'],
                ]
            ]);
        ?>
        <input type="hidden" name="id" value="<?=isset($data->id) ? $data->id :""; ?>" >
        <div  class="form-group" id="show_div"  style="">
             <label class="name-title-lable" style="height: 32px;line-height: 32px;">服务完成：</label>
            <div class="addinput-list" style="line-height: 30px;">
                 <input name="num" id="days_num" size="10" value="<?php echo isset($data->num)?$data->num:'7';?>" style="border-radius: 4px;height: 30px;text-align: center;">
                天后，技师方可查看评价（建议为7天，以免客户在给出差评后，技师通过评价时间辨认客户身份）
            </div>
        </div>
        
        <hr>
        <div class="form-group">
            <div class="col-xs-6 addinput-list">
                <input type="submit" id="commitBtn" class="btn btn-orange mgr20 " style=" color:#FFF;" value="保存" />
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</section>
<script>
    window.onload = function(){

        <?php if(Yii::$app->session->hasFlash('appraisal_set_message')):?>
        setTimeout(function () {
            alert('<?php echo Yii::$app->session->getFlash('appraisal_set_message'); ?>');
        },500);
        <?php endif;?>
    }
    $("#days_num").keyup(function(){
        var daysNum = $(this).val();
        if(isNaN(daysNum)){
            alert('仅支持大于或等于0的整数！');
            $(this).val('0');
        }
    })

    $("#appraisal-set").submit(function(){
        var daysNum = $("#days_num").val();
        if(daysNum == ''){
            alert('请输入设置天数！');
            return false;
        }
        if(isNaN(daysNum)||daysNum<0){
            alert('仅支持大于或等于0的整数！');
            return false;
        }
    })
</script>

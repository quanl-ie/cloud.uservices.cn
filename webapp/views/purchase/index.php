<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
$this->title = '采购管理列表';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time()?>">
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">采购管理列表</span>
    <?php if(in_array('/purchase/add',$selfRoles)){?>
        <a href="/purchase/add" style="color: #fff;"><span class="btn btn-success jn-btn" style=" color:#FFF;float:right;width:auto;"  data-toggle="modal" data-target="#scrollingModal">创建采购单</span></a>
    <?php };?>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div style="width: 100%;" class="tab-group">
                        <ul id="myTab" class="nav nav-tabs">
                            <?php if(in_array('/purchase/index',$selfRoles)){?>
                                <li class="active"><a href="/purchase/index" >待审批</a></li>
                            <?php };?>
                            <?php if(in_array('/purchase/wait-check-in',$selfRoles)){?>
                                <li><a href="/purchase/wait-check-in">待入库</a></li>
                            <?php };?>
                            <?php if(in_array('/purchase/finished',$selfRoles)){?>
                                <li><a href="/purchase/finished">入库完毕</a></li>
                            <?php };?>
                            <?php if(in_array('/purchase/not-pass',$selfRoles)){?>
                                <li><a href="/purchase/not-pass">未通过</a></li>
                            <?php };?>
                            <?php if(in_array('/purchase/cancelled',$selfRoles)){?>
                                <li><a href="/purchase/cancelled">已取消</a></li>
                            <?php };?>
                            <?php if(in_array('/purchase/invalid',$selfRoles)){?>
                                <li><a href="/purchase/invalid">已作废</a></li>
                            <?php };?>
                        </ul>
                    </div>
                    <form action="/purchase/index" class="form-horizontal form-border" id="form">
                        <div class="form-group jn-form-box">
                            <!-- 搜索栏 -->
                            <div class="col-sm-12 no-padding-left">
                                <div class="single-search">
                                    <label class="search-box-lable">采购编号</label>
                                    <div class="single-search-kuang1">
                                        <input type="text" class="form-control" name="no" value="<?php echo isset($_GET['no'])?$_GET['no']:'';?>">
                                    </div>
                                </div>
                                <div class="single-search">
                                    <label class="search-box-lable">采购主题</label>
                                    <div class="single-search-kuang1">
                                        <input type="text" class="form-control" name="subject" value="<?php echo isset($_GET['subject'])?$_GET['subject']:'';?>">
                                    </div>
                                </div>
                                <div class="single-search">
                                    <label class="search-box-lable">供货商</label>
                                    <div class="single-search-kuang1">
                                        <input type="text" class="form-control" name="supplier_name" value="<?php echo isset($_GET['supplier_name'])?$_GET['supplier_name']:'';?>">
                                    </div>
                                </div>
                                <div class="single-search">
                                    <label class="search-box-lable">采购类型</label>
                                    <div class="single-search-kuang1">
                                        <select class="form-control" name="type">
                                            <option value="">请选择</option>
                                            <?php foreach ($purchaseTypeList as $key=>$val):?>
                                                <option value="<?php echo $key; ?>" <?php if(isset($_GET['type']) && $_GET['type']==$key):?>selected<?php endif; ?>><?php echo $val;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <?php if($params['audit_status'] =='2'){?>
                                    <div class="single-search">
                                        <label class="search-box-lable">采购状态</label>
                                        <div class="single-search-kuang1">
                                            <select class="form-control" name="type">
                                                <option value="">请选择</option>
                                                <?php foreach ($purchaseStatusList as $key=>$val):?>
                                                    <option value="<?php echo $val['dict_enum_id']; ?>" <?php if(isset($_GET['type']) && $_GET['type']==$val['dict_enum_id']):?>selected<?php endif; ?>><?php echo $val['dict_enum_value'];?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                <?php }?>

                                <div class="single-search">
                                    <label class="search-box-lable">采购人员</label>
                                    <div class="single-search-kuang1">
                                        <input type="text" class="form-control" name="purchase_user_name" value="<?php echo isset($_GET['purchase_user_name'])?$_GET['purchase_user_name']:'';?>">
                                    </div>
                                </div>
                                <div class="tow-search" >
                                    <label class="search-box-lable">申请时间</label>
                                    <div class="single-search-kuang2">
                                        <div class="half-search">
                                            <input type="text" id="datetimepicker-pay-top" class="form-control" name="start_time" value="<?php echo isset($_GET['start_time'])?$_GET['start_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})">
                                            <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})"><i class="icon2-rili"></i></span>
                                            <span class="zhi"> 至</span>
                                        </div>
                                        <div class="half-search">
                                            <input type="text" id="datetimepicker-pay-end" class="form-control" name="end_time" value="<?php echo isset($_GET['end_time'])?$_GET['end_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})" >
                                            <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})"><i class="icon2-rili"></i></span>
                                        </div>
                                        <p class="city3_fix"></p>
                                    </div>
                                </div>
                                <div class="search-confirm"> 
                                    <button class="btn btn-success "><i class=" "></i> 搜索</button>
                                </div>

                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead bgcolor="#455971">
                                <tr>
                                    <!--<th>序号</th>-->
                                    <th>采购编号</th>
                                    <th>采购主题</th>
                                    <th>供货商</th>
                                    <th>采购金额</th>
                                    <th>采购类型</th>
                                    <th>采购人员</th>
                                    <th>状态</th>
                                    <th>申请时间</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($data){?>
                                    <?php foreach ($data as $key=>$val){?>
                                        <tr>
                                            <!--<td><?php /*echo isset($val['id']) ? $val['id'] : ''; */?></td>-->
                                            <td>
                                                <?php echo $val['no']; ?>
                                            </td>
                                            <td>
                                                <?php echo $val['subject']; ?>
                                            </td>
                                            <td>
                                                <?php echo $val['supplier_name']; ?>
                                            </td>
                                            <td>
                                                <?php echo isset($val['total_amount']) ? $val['total_amount'] : ''; ?>
                                            </td>
                                            <td>
                                                <?php echo isset($val['type'])?$purchaseTypeList[$val['type']]: ''; ?>
                                            </td>
                                            <td>
                                                <?php echo isset($val['purchase_user_name']) ? $val['purchase_user_name'] : ''; ?>
                                            </td>
                                            <td>
                                                <?php if($val['audit_status'] == '1'){echo '待审批';}?>
                                            </td>
                                            <td>
                                                <?php echo isset($val['purchase_date']) ? $val['purchase_date'] : ''; ?>
                                            </td>
                                            <td class="operation">
                                                <?php if(in_array('/purchase/detail',$selfRoles)){?>
                                                    <a href="/purchase/detail?no=<?php echo $val['no'];?>&t=view" class="update">查看</a>
                                                <?php };?>

                                                <?php if(in_array('/purchase/cancel',$selfRoles)){?>
                                                    <a href="<?php echo $val['no'];?>" class="setCancel">取消</a>
                                                <?php };?>
                                                <?php if(in_array('/purchase/edit',$selfRoles)){?>
                                                    <a href="/purchase/edit?no=<?php echo $val['no'];?>" class="update">编辑</a>
                                                <?php };?>
                                                <?php if(in_array('/purchase/set-audit-status',$selfRoles)){?>
                                                    <a href="/purchase/detail?no=<?php echo $val['no'];?>" class="update">审批</a>
                                                <?php };?>
                                            </td>
                                        </tr>
                                    <?php }?>
                                <?php }else{?>
                                    <tr><td colspan="11" class="no-record">暂无待审批采购单</td></tr>
                                <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo $pageHtml;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- 取消弹层 -->
<div id="cancelLayer" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <p style="margin:0 0 10px 0;text-align: center;">确认取消采购吗？</p>
        <div class="select-fuji">
            <textarea class="textarea100"  onkeyup=" wordsLimit(this)" placeholder="请填写取消意见" id="opinionContent" style="width: 100%;height: 100px;"></textarea>
            <p class="xianzhi"><span  id='word'>0</span>/200</p>
        </div>
        <div style="text-align: center;margin-top:15px;">
            <button class="btn btn-success" id="layerSubmit"  style="padding: 4px 12px"><i class="fa fa-search"></i>确定</button>
            <button class="btn" id="layerCancel" style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>
<script>
    var layerIndex = null;
    var purchaseNo = '';
    $(".setCancel").click(function () {
        layerIndex = layer.open({
            type: 1,
            title: '提示',
            area: ['400px', '300px'],
            fixed: true,
            maxmin: false,
            content: $('#cancelLayer').html()
        });
        purchaseNo = $(this).attr('href');
        return false;
    });
    //提交取消订单
    $("body").delegate("#layerSubmit","click",function(){
        var opinion_content = $(this).parents('div').find('#opinionContent').val();
        if($.trim(purchaseNo) == ''){
            alert('采购编号不能为空');
            return false;
        }
        if($.trim(opinion_content) == ''){
            alert('请填写取消意见');
            return false;
        }
        $.getJSON('/purchase/cancel',{'no':purchaseNo,'audit_suggest':opinion_content},function (json) {
            //console.log(json);return false;
            if(json.success == 1){
                if(layerIndex){
                    layer.close(layerIndex);
                }
                alert(json.message);
                setTimeout(function(){location.reload();},2000);
                //window.location.reload(true);
            }
            else {
                alert(json.message);
            }
        });
    });
    //关闭弹层
    $("body").delegate("#layerCancel","click",function(){
        if(layerIndex){
            layer.close(layerIndex);
        }
    });
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
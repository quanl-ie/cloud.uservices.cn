<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = isset($data['purchaseInfo']['id']) ? '编辑采购单' : '创建采购单';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title"><?php echo isset($data['purchaseInfo']['id']) ? '编辑采购单' : '创建采购单' ?></span>
</div>
<!--引入CSS-->
<?= Html::cssFile('@web/webuploader/webuploader.css') ?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/webuploader/webuploader.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jquery.autocomplete.css'); ?>">
<!--引入JS-->
<?= Html::jsFile('@web/webuploader/webuploader.js') ?>
<?= Html::jsFile('@web/js/bootstrap-select.js') ?>
<style>
    .information-list > .col-xs-12 {
        margin-bottom: 10px
    }
</style>
<section id="main-content">
    <div class="panel-body"><?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal',
                'id' => 'purchaseIn',
                'name' => 'form1',
                'enctype' => 'multipart/form-data',
            ],
            'fieldConfig' => [
                'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-xs-6\">{input}{error}</div>",
                'labelOptions' => ['class' => 'col-xs-2 control-label text-right'],
            ]
        ]);
        ?>
        <div class="col-md-12">
            <h5 class="block-h5 block-h5one">创建采购单</h5>
        </div>
        <div class="information-list">
            <div class="col-xs-12   col-sm-12   col-md-6    col-lg-3">
                <input type="hidden" name="id"
                       value="<?php echo isset($data['purchaseInfo']['id']) ? $data['purchaseInfo']['id'] : '' ?>">
                <label class="left-title80">
                    <i class="red-i">*</i>采购主题：
                </label>
                <span class="c_red"></span>
                <div class="right-title3">
                    <input type="text" name="subject" class="form-control" id="subject"
                           value="<?php echo isset($data['purchaseInfo']['subject']) ? $data['purchaseInfo']['subject'] : '' ?>"
                           placeholder="请输入采购主题" maxlength="30">
                    <p class="Validform_checktip Validform_wrong" id="subjectct_msg"></p>
                </div>

            </div>
            <div class="col-xs-12   col-sm-12   col-md-6  col-lg-3">
                <label class="left-title80">
                    采购编号：
                </label>
                <div class="right-title3">
                    <input type="text" name="no" id="no"
                           value="<?php echo isset($data['purchaseInfo']['no']) ? $data['purchaseInfo']['no'] : '' ?>"
                           readonly="readonly" placeholder="" style="border: none;outline:none">
                    <p class="misplacement" id="no_msg"></p>
                    <p class="Validform_checktip Validform_wrong" id="subjectct_msg"></p>
                </div>

            </div>
            <div class="col-xs-12   col-sm-12   col-md-6  col-lg-3">
                <label class="left-title80">
                    <i class="red-i">*</i> 采购类型：
                </label>
                <div class="right-title3">
                    <select name="type" class="form-control" id="type">
                        <option value="">选择类型</option>
                        <?php if ($purchaseTypeList) : ?>
                            <?php foreach ($purchaseTypeList as $key => $val) : ?>
                                <option value="<?php echo $key; ?>"
                                        <?php if (isset($data['purchaseInfo']['type']) && $data['purchaseInfo']['type'] == $key): ?>selected<?php endif; ?>><?php echo $val; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <p class="Validform_checktip Validform_wrong" id="type_msg"></p>
                </div>

            </div>
            <div class="col-xs-12   col-sm-12   col-md-6    col-lg-3">
                <label class="left-title80" for="workorder-account_id"><i class="red-i">*</i>供货商：</label>
                <div class="right-title3">
                    <input type="hidden" name="supplier_id" id="supplier_id"
                           value="<?php echo isset($data['purchaseInfo']['supplier_id']) ? $data['purchaseInfo']['supplier_id'] : '' ?>">
                    <?php if (!isset($data['purchaseInfo']['id'])) { ?>
                        <!--<span id="choiceSupplier">点击选择</span>-->
                        <input value="点击选择" type="text" name="supplier_name" id="supplier_name"
                               class="form-control choiceSupplier"
                               style="border: none;outline: none;cursor: pointer;color: #2693ff;overflow: hidden;text-overflow: ellipsis;padding: 0">
                    <?php } else { ?>
                        <input value="<?php echo isset($data['purchaseInfo']['supplier_name']) ? $data['purchaseInfo']['supplier_name'] : '' ?>"
                               type="text" name="supplier_name" id="supplier_name" class="form-control"
                               title="<?php echo isset($data['purchaseInfo']['supplier_name']) ? $data['purchaseInfo']['supplier_name'] : '' ?>"
                               style="border: none;outline: none;overflow: hidden;text-overflow: ellipsis;padding: 0">
                    <?php } ?>
                    <p class="Validform_checktip Validform_wrong" id="supplier_msg"></p>
                </div>
            </div>
            <div class="col-xs-12   col-sm-12   col-md-3    col-lg-3">
                <label class="left-title80" for="workorder-account_id">采购人：</label>
                <span class="c_red"></span>
                <div class="right-title3">
                    <input type="hidden" name="purchase_user_id"
                           value="<?php echo isset($data['purchaseInfo']['purchase_user_id']) ? $data['purchaseInfo']['purchase_user_id'] : '' ?>">
                    <input name="purchase_user_name"
                           value="<?php echo isset($data['purchaseInfo']['purchase_user_name']) ? $data['purchaseInfo']['purchase_user_name'] : '' ?>"
                           id="purchase_user_name" style="border: none;outline: none">
                </div>
            </div>
            <?php if (isset($data['purchaseInfo']['id'])) { ?>
                <div class="col-xs-12   col-sm-12   col-md-5    col-lg-5">
                    <label class="left-title80">申请时间：</label>
                    <span class="c_red"></span>
                    <div class="right-title3">
                        <span><?php echo isset($data['purchaseInfo']['purchase_date']) ? $data['purchaseInfo']['purchase_date'] : '' ?></span>
                    </div>
                </div>

            <?php } ?>
            <div class="col-xs-12   col-sm-12   col-md-12    col-lg-12">

                <label class="left-title80">备注：</label>
                <div class="right-title3 col-xs-10   col-sm-10   col-md-6    col-lg-6" style="padding:0">

                    <textarea class="ramark-box" id="remarks" maxlength="200" placeholder="请输入备注信息（200字以内）"
                              name="description"><?php echo isset($data['purchaseInfo']['description']) ? $data['purchaseInfo']['description'] : ''; ?></textarea>
                    <p class="xianzhi" style="right: 22px;"><span id='word'>0</span>/200</p>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <h5 class="block-h5">采购产品明细</h5>
        </div>

        <div class="information-list" style="height: auto;">
            <table style="width: 100%;font-size: 12px;">
                <thead>
                <tr style="font-size: 14px;">
                    <th style="width: 50px;" class="padding15">
                        <input type="checkbox" name="" id="allDetailed">
                        <label for="allDetailed"> <i class="gou-i"></i></label>
                    </th>
                    <th>产品名称</th>
                    <th>产品编号</th>
                    <th>产品品牌</th>
                    <th>产品型号</th>
                    <th>计量单位</th>
                    <th width="100px">数量</th>
                    <th>建议进价</th>
                    <th width="100px">采购价</th>
                    <th width="120px">总价</th>
                </tr>
                </thead>
                <tfoot id="show_prodcut">
                <?php if ($data['purchaseDetail']) foreach ($data['purchaseDetail'] as $key => $val) { ?>
                    <tr id="<?php echo $val['prod_id']; ?>">
                        <td class="padding15">
                            <input type="checkbox" class="available_cost_type select_product"
                                   name="<?php echo 'prod' . '[' . $val['prod_id'] . '][prod_id]'; ?>"
                                   id="<?php echo 'prod_id' . $val['prod_id']; ?>"
                                   value="<?php echo $val['prod_id']; ?>">
                            <label for="<?php echo 'prod_id' . $val['prod_id']; ?>"> <i class="gou-i"></label>
                        </td>
                        <td>
                            <input class="prod_id" value="<?php echo $val['prod_id']; ?>"
                                   name="<?php echo 'prod' . '[' . $val['prod_id'] . '][prod_id]'; ?>" type="hidden"/>
                            <?php echo $val['prod_name']; ?>
                        </td>
                        <td><?php echo $val['prod_no']; ?></td>
                        <td><?php echo isset($val['brand_name']) ? $val['brand_name'] : ''; ?></td>
                        <td><?php echo $val['model']; ?></td>
                        <td><?php echo isset($val['unit_name']) ? $val['unit_name'] : ''; ?></td>
                        <td><input class="prod_num" name="<?php echo 'prod' . '[' . $val['prod_id'] . '][prod_num]'; ?>"
                                   onkeyup="value=value.replace(/[^\d]/g,'') " ng-pattern="/[^a-zA-Z]/" type=""
                                   maxlength="9" value="<?php echo $val['prod_num']; ?>"/></td>
                        <td><?php echo isset($val['purchase_price']) ? $val['purchase_price'] : ''; ?></td>
                        <td>
                            <input class="purchase_price"
                                   name="<?php echo 'prod' . '[' . $val['prod_id'] . '][price]'; ?>" type=""
                                   maxlength="10" value="<?= isset($val['price']) ? $val['price'] : "" ?>"
                                   onkeyup="clearNoNum(this)" onblur="supplement(this)"/>
                        </td>
                        <td class="amount">
                            <span class="amount_sum"><?= isset($val['total_amount']) ? $val['total_amount'] : "" ?></span>
                        </td>
                    </tr>
                <?php } ?>
                </tfoot>
                <div id="product_hide" style="display:none;">333</div>
            </table>
            <div class="Validform_checktip Validform_wrong" id="products_msg" style="display: none"></div>
            <div class="information-list" style="margin-top: 0;height: 28px">
                <label class="name-title-lable" for="workorder-account_id" style="text-align: left;height: 28px">总计总额：</label>
                <div class="addinput-list" style="margin-left: 70px">
                    <input type="hidden" name="total_all_amount" class="form-control" maxlength="30"
                           id="total_all_amount">
                    <p id="allCost"
                       class="height34"><?php echo isset($data['purchaseInfo']['total_amount']) ? $data['purchaseInfo']['total_amount'] : ''; ?></p>
                    <p class="misplacement" id="total_all_amount_msg"></p>
                </div>
            </div>
            <div class="bottom-btn-box" style="margin-top: 5px">
                <a href="javascript:" class="btn btn-orange " id="addBtn" style="color: #fff;">选择产品</a>
                <a class="btn ignore delet-btn" href="javascript:" style="margin-left: 30px;width: 84px;">删除</a>
            </div>

            <div class="col-md-offset-2 col-md-6 misplacement" style="display:none" id="brand_msg"></div>
        </div>

        <hr style="clear: both;">
        <div class="information-list">
            <div style="padding:0px;text-align: center;">
                <a href="/purchase/index">
                    <button type="button" style="border: 1px solid #cccccc;" class="btn ignore mgr20">取消</button>
                </a>
                <input type="submit" class="btn btn-orange btnSubmit" style=" color:#FFF;" value="确认"/>
                <input type="button" class="btn btn-orange btnSubmiting"
                       style="margin-right:10px;display: none; color:#FFF;background: #cccccc;border:1px solid #cccccc;"
                       value="提交中..."/>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</section>
<!-- 时间插件/ -->
<script type="text/javascript" src="/js/jquery-migrate-1.1.0.js"></script>
<script src="/js/jquery.autocomplete.js" type="text/javascript"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    window.onload = function () {
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert( '<?php echo Yii::$app->session->getFlash('message'); ?>' );
        <?php endif;?>
    }
    // 字数限制
    $( "#remarks" ).keyup( function () {
        if ( $( "#remarks" ).val().length > 200 ) {
            $( "#remarks" ).val( $( "#remarks" ).val().substring( 0, 200 ) );
        }
        $( "#word" ).text( $( "#remarks" ).val().length );
    } );
    //单项选择
    $( '.radio-li' ).on( 'click', function () {
        $( this ).find( 'span' ).css( { 'background' : '#2693ff', 'color' : '#fff' } );
        $( this ).siblings( 'li' ).find( 'span' ).css( {
            'background'   : '#fff',
            'color'        : '#333',
            'border-color' : '#ccc'
        } );
        $( this ).parent( 'ul' ).next( 'input' ).val( $( this ).find( 'span' ).attr( 'id' ) );
    } )
    //点击选择供货商弹窗
    $( ".choiceSupplier" ).click( function () {
        layer.open( {
            type    : 2,
            title   : '选择供货商',
            area    : ['800px', '400px'],
            fixed   : false, //不固定
            maxmin  : false,
            content : '/supplier/get-list',
            end     : function () {
            }
        } );
        $( "#supplier_msg" ).text( "" );
        $( "#supplier_msg" ).hide();
    } )
    //点击选择产品弹窗
    $( "#addBtn" ).click( function () {
        layer.open( {
            type    : 2,
            title   : '选择产品',
            area    : ['800px', '400px'],
            fixed   : false, //不固定
            maxmin  : false,
            content : '/product/get-list',
            end     : function () {
                var str = $( "#product_hide" ).html();
                data = eval( "(" + str + ")" );
                prod_id = data.data.id;
                model = data.data.model;
                if(model == null){
                    model = '';
                }
                html = '<td class="padding15"><input type="checkbox" class="available_cost_type select_product" name="prod[' + prod_id + '][prod_id]"  id="prod_id' + prod_id + '" value="' + prod_id + '"> <label for="prod_id' + prod_id + '"> <i class="gou-i"></label> ' +
                    '</td>' +
                    '<td><input class="prod_id" value="' + prod_id + '" name="prod[' + prod_id + '][prod_id]"  type="hidden"/>' + data.data.prod_name + '</td>' +
                    '<td>' + data.data.prod_no + '</td>' +
                    '<td>' + data.data.brand_name + '</td>' +
                    '<td>' + model + '</td>' +
                    '<td>' + data.data.unit_name + '</td>' +
                    '<td><input class="prod_num" name="prod[' + prod_id + '][prod_num]" type="" maxlength="9" value="1"/></td>' +
                    '<td>' + data.data.purchase_price + '</td>' +
                    '<td><input class="purchase_price"  name="prod[' + prod_id + '][price]" type="" maxlength="10" value="' + data.data.purchase_price + '"  onfocus="clearNoNum(this)"  onkeyup="clearNoNum(this)" /></td>' +
                    '<td class="amount"><span class="amount_sum">' + data.data.purchase_price + '</span>' +
                    '<input class="amount_sum_hidden" name="prod[' + data.id + '][amount]" type="hidden" value="' + data.data.purchase_price + '"/></td>';
                
                var show_product = $( "#show_prodcut" ).html();
                //var show_product = $("#show_prodcut").append('<tr id="'+data.data.id+'">'+html+'</tr>');
                if ( show_product.length > 0 ) {
                    var haveExist = $( "#" + data.data.id ).find( "#prod_id" + prod_id ).val();
                    if ( !haveExist ) {  //勾选重复的产品不会显示
                        $( "#show_prodcut" ).append( '<tr id="' + data.data.id + '">' + html + '</tr>' );
                    }
                }

                calculator.refresh();
                /*
                                //获取价格数据进行计算
                                $( ".prod_num" ).keyup( function () {
                                    var prod_num = $( this ).val();
                                    if ( isNaN( prod_num ) ) {
                                        alert( '请输入数字' );
                                        return false;
                                    }
                                    var id = $( this ).parents( 'tr' ).attr( "id" );
                                    getTotal( id );
                                    getAllTotal();
                                } )
                                $( ".purchase_price" ).blur( function () {
                                    var price = $( this ).val();
                                    if ( /^(?!(0[0-9]{0,}$))[0-9]{1,}[.]{0,}[0-9]{0,}$/.test( price ) ) {
                                        price = new Number( price );
                                        price = price.toFixed( 2 );
                                        if ( price != 0.00 ) {
                                            $( this ).val( price );
                                        }
                                    }
                                    var id = $( this ).parents( 'tr' ).attr( "id" );
                                    getTotal( id );
                                    getAllTotal();
                                } )
                                */

                //计算总额
             /*   function getTotal( id ) {
                    var price = $( "#" + id ).find( '.purchase_price' ).val();
                    var prod_num = $( "#" + id ).find( '.prod_num' ).val();
                    var total = price * prod_num;
                    total = new Number( total );
                    total = total.toFixed( 2 );
                    $( "#" + id ).find( '.amount_sum' ).html( total );
                    $( "#" + id ).find( '.amount_sum_hidden' ).html( total );
                }*/
            }
        } );
    } )
    
    //计算采购单总额
    function getAllTotal() {
        //获取产品明细总价
        var checkboxInput = $( ".select_product" );
        var sum = 0;
        $( "td .amount_sum" ).each( function ( i ) {
            // id = $(this).text();
            // var total_amount = $("#"+id).find('.total_amount_hidden').val();
            var total_amount = $( this ).text();
            sum = sum + parseFloat( total_amount );
        } );
        sum = new Number( sum );
        sum = sum.toFixed( 2 );
        if ( !isNaN( sum ) ) {
            $( "#total_all_amount" ).val( sum );
            $( '#allCost' ).text( sum );
        }
        /*if(sum != 'NaN' && sum >0){
            $("#total_all_amount").val(sum);
            $('#allCost').text(sum);
        }*/
    };

    // 计算器
    function createCalculator() {
        // 列表
        var list,
            allTotal = $( "#allCost" ),
            isArray = function ( s ) {
                return s && s.length;
            },
            // 返回一个金额
            toMoney = function ( num ) {
                return num.toFixed( 2 );
            },
            // 返回一个获取数量的函数
            toNumber = function ( val ) {
                return val && parseFloat( val ) || 0;
            },
            // 是不是数字
            isNumeric = function ( s ) {
                return $.isNumeric( s );
            },
            // 重新获取计算单元
            repeatUnits = function () {
                // 清空数组
                if ( isArray( list ) ) list.splice( 0, list.length );
                // 重新获取列表
                list = $( "#show_prodcut > tr" ).map( function () {
                    var that = $( this ),
                        // 单价输入框
                        price = that.find( "input.purchase_price" ),
                        // 数量输入框
                        number = that.find( "input.prod_num" ),
                        // 总价
                        total = that.find( 'td.amount' ),
                        // 计算总价
                        count = function count() {
                            var p = toMoney( toNumber( price.val().replace( /[^\d.]/g, '' ) ) ),
                                n = toNumber( number.val() ),
                                res = toMoney( n * p );
                            price.val( p );
                            total.text( res );
                            return res;
                        };
                    price.off( 'blur' ).on( 'blur', compute );
                    number.off( 'keyup' ).on( 'keyup', function () {
                        var ipt = this;
                        // 如果不是数字
                        if ( !isNumeric( ipt.value ) ) {
                            return alert( '请输入数字', function () {
                                ipt.value = ipt.value.replace( /[^\d]/g, '' );
                            } );
                        }
                        compute();
                    } );
                    // 返回一个计算总额的函数
                    return count;
                } );
            },
            // 计算
            compute = function () {
                var fn, i, l, all = 0;
                if ( isArray( list ) ) {
                    i = 0;
                    l = list.length;
                    // 循环
                    for ( ; i < l; i++ ) {
                        fn = list[i];
                        all += toNumber( fn() );
                    }
                }
                allTotal.text( toMoney( all ) );
                return all;
            },
            self = {
                // 重新获取列表并计算
                refresh  : function () {
                    repeatUnits();
                    return compute();
                },
                // 重新计算
                computed : function () {
                    return compute();
                }
            };
        self.refresh();
        return self;
    }

    var calculator = createCalculator();

    //===start===获取价格数据进行计算(用于编辑时的数据计算)
    /*    $( ".prod_num" ).keyup( function () {
            var id = $( this ).parents( 'tr' ).attr( "id" );
            getTotal( id );
            getAllTotal();
        } )
        $( ".purchase_price" ).keyup( function () {
            var id = $( this ).parents( 'tr' ).attr( "id" );
            getTotal( id );
            getAllTotal();
        } )*/
    
    //计算总额
    function getTotal( id ) {
        var price = $( "#" + id ).find( '.purchase_price' ).val();
        var prod_num = $( "#" + id ).find( '.prod_num' ).val();
        var total = price * prod_num;
        total = new Number( total );
        total = total.toFixed( 2 );
        $( "#" + id ).find( '.amount' ).html( total );
    }
    
    //===end===
    /*$('#allDetailed').on('change',function () {
        var amountText =$('.amount');
        var checkboxInput = $('tfoot input[type=checkbox]');
        var sum=0,sum1=0;

        if( $(this).is(':checked')){
            checkboxInput.prop("checked",true);
            amountText.each(function() {
                sum += Number($(this).text());
            })
            sum1 = sum + Number($('#otherExpenses').val());

        }else{
            sum1 = Number($('#otherExpenses').val());
            checkboxInput.prop("checked",false);
        }
        $('#allCost').text(sum1);
    })*/
    //删除产品
    $( ".delet-btn" ).click( function () {
        ids = new Array();
        $( ".select_product" ).each( function ( i ) {
            if ( $( this ).is( ':checked' ) ) {
                var id = $( this ).val();
                if ( id > 0 ) {
                    ids[i] = id;
                }
            }
        } )
        if ( ids.length > 0 ) {
            confirm( '确认删除产品吗?', function () {
                for ( j = 0; j <= ids.length; j++ ) {
                    $( "#" + ids[j] ).remove();
                    // getAllTotal();
                    calculator.refresh();
                }
                $( "#allDetailed" ).attr( "checked", false );
            }, this )
        } else {
            alert( '请选择需要删除的产品' );
        }
    } );
    //全选删除按钮
    $( '#allDetailed' ).on( 'change', function () {
        var checkboxInput = $( 'tfoot input[type=checkbox]' );
        var sum = 0, sum1 = 0;
        if ( $( this ).is( ':checked' ) ) {
            checkboxInput.prop( "checked", true );
        } else {
            checkboxInput.prop( "checked", false );
        }
    } );
    /**********************   提交信息验证   ************************/
    $( ".btnSubmit" ).click( function ( e ) {
        //console.log(333);
        if ( checkAll() ) {
            $( '.btnSubmit' ).hide();
            $( '.btnSubmiting' ).show();
            $( 'form' ).submit();
        }
        else {
            $( '.btnSubmit' ).show();
            $( '.btnSubmiting' ).hide();
        }
        return false;
    } );
    
    function checkAll() {
        //验证客户是否选中
        var flag = true;
        var subject = $( "#subject" ).val();//采购主题
        var type = $( "#type" ).val();//采购类型
        var supplier_id = $( "#supplier_id" ).val();//供货商
        var supplier_name = $( "#supplier_name" ).val();//供货商名字
        var products = $( "#show_prodcut" ).find( 'tr' );
        //console.log(products.length);return false;
        //验证
        if ( subject == '' ) {
            flag = false;
            $( "#subjectct_msg" ).text( "请输入采购主题" );
            $( "#subjectct_msg" ).show();
        }
        if ( type == '' || type == 0 ) {
            flag = false;
            $( "#type_msg" ).text( "请选择采购类型" );
            $( "#type_msg" ).show();
        }
        if ( supplier_id == '' ) {
            flag = false;
            $( "#supplier_msg" ).text( "请选择供货商" );
            $( "#supplier_msg" ).show();
        }
        if ( products.length == 0 ) {
            flag = false;
            $( "#products_msg" ).text( "请选择采购产品" );
            $( "#products_msg" ).show();
        }
        //获取焦点提示框消失
        $( "#subject" ).focus( function () {
            $( "#subjectct_msg" ).text( "" );
            $( "#subjectct_msg" ).hide();
        } )
        $( "#type" ).click( function () {
            $( "#type_msg" ).text( "" );
            $( "#type_msg" ).hide();
        } )
        $( "#supplier_id" ).click( function () {
            $( "#supplier_msg" ).text( "" );
            $( "#supplier_msg" ).hide();
        } )
        $( "#addBtn" ).click( function () {
            $( "#products_msg" ).text( "" );
            $( "#products_msg" ).hide();
        } )
        return flag;
    }
    
    //提示
    $( ".tip" ).popover();
    <?php if(!empty($data['purchaseInfo']['audit_status'])){?>
    <?php if($data['purchaseInfo']['audit_status'] > 1 ) {?>
    alert( '该采购单已通过审批，不可修改' );
    setTimeout( 'window.location.href="/purchase/index";', 1500 );
    <?php }?>
    <?php }?>
    //表单提交验证
    /*$("#purchaseIn").Validform({
        ajaxPost:true,
        callback:function(data){
            if (data.code == 200) {
                alert(data.message);
                setInterval(function () {
                    location.href = '/purchase/index';
                },1000);
            }else{
                $.Hidemsg();
                alert(data.message);
            }
        }
    });*/
    
    //价格数据格式化
    function clearNoNum( obj ) {
        if ( obj.value != '' && obj.value.substr( 0, 1 ) == '.' || obj.value == '0.00' ) {
            obj.value = "";
            
        }
        obj.value = obj.value.replace( /^0*(0\.|[1-9])/, '$1' );
        obj.value = obj.value.replace( /[^\d.]/g, "" );
        obj.value = obj.value.replace( /\.{2,}/g, "." );
        obj.value = obj.value.replace( ".", "$#$" ).replace( /\./g, "" ).replace( "$#$", "." );
        obj.value = obj.value.replace( /^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3' );//只能输入两个小数
        if ( obj.value.indexOf( "." ) < 0 && obj.value != "" ) {//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
            if ( obj.value.substr( 0, 1 ) == '0' && obj.value.length == 2 ) {
                obj.value = obj.value.substr( 1, obj.value.length );
            }
        }
    }

</script>


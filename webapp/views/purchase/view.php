<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */
$this->title = '采购单详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css'); ?>">
<style type="text/css">
    .information-list {
        margin-top: 0;
    }
</style>

<div class="jn-title-box no-margin-bottom">

    <span class="jn-title" id="no" data-no="<?php echo $data['purchaseInfo']['no']; ?>">采购单详情</span>
    <div class="right-btn-box">
        <?php if ($data['purchaseInfo']['audit_status'] == 1 && $data['purchaseInfo']['status'] == 3) { ?>
            <!--<a href="" class="cancelOrder">-->
            <!--<button class="btn bg-f7" id="btnPrint" onclick="javascript:window.print();">打印</button>-->
            <!--<button class="btn btn-orange" id="btnPrint" onclick="preview(1)">打印</button>-->
            <!--</a>-->
            <?php if (in_array('/purchase/set-audit-status', $selfRoles) && $type != 'view') { ?>
                <a href="" class="setAudit" data-type="no">
                    <button class="btn btn-orange">拒绝</button>
                </a>
                <a href="" class="setAudit" data-type="ok">
                    <button class="btn btn-orange">同意</button>
                </a>
            <?php }; ?>
        <?php } ?>
        <?php if(in_array('/stock-in/add',$selfRoles)){?>
            <?php if(isset($data['purchaseInfo']['isship']) && $data['purchaseInfo']['isship']=='1' && $type == 'w'){?>
                <a href="/purchase-review/add?id=<?php echo $data['purchaseInfo']['id'].'&source=1';?>">
                    <button class="btn btn-orange">收货</button>
                </a>
            <?php }?>
            <?php if($data['purchaseInfo']['status'] == '3' && $type == 'w'){?>
                <a href="/purchase/set-stop?id=<?php echo $data['purchaseInfo']['id'];?>&status=5" onclick="return confirm('您确定要作废吗？',function(obj){ document.location.href = obj.href ;} , this);">
                    <button class="btn btn-orange">作废</button>
                </a>
            <?php }?>
            <?php if($data['purchaseInfo']['status'] == '2' && $data['purchaseInfo']['isship']=='3' && $type == 'w'){?>
                <a href="/purchase/set-stop?id=<?php echo $data['purchaseInfo']['id'];?>&status=6" onclick="return confirm('您确定要终止吗？',function(obj){ document.location.href = obj.href ;} , this);">
                    <button class="btn btn-orange">终止</button>
                </a>
            <?php }?>
        <?php }?>
        <?php if(isset($data['purchaseInfo']['is_return']) && $data['purchaseInfo']['is_return'] == 1 && $type == 'f' ){?>
            <a href="/return-purchase/add?id=<?php echo $data['purchaseInfo']['id'];?>">
                <button class="btn btn-orange">退货</button>
            </a>
        <?php }?>
        <?php if(in_array('/stock-in/add',$selfRoles) && $data['purchaseInfo']['isship']=='3' && $type == 'w'){?>
            <a href="/stock-in/add?rel_id=<?php echo $data['purchaseInfo']['id'] . '&rel_type=1'; ?>">
                <button class="btn btn-orange">入库</button>
            </a>
        <?php };?>
        <?php if ($data['purchaseInfo']['audit_status'] == 2 && $data['purchaseInfo']['status'] == 1) { ?>
            <!--<a href="" class="cancelOrder">
                <button class="btn btn-orange">打印</button>
            </a>-->
        <?php } ?>
        <button class="btn bg-f7" onclick="javascript:history.back(-1);">返回</button>
    </div>

</div>

<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <!--startprint1-->
                <div class="panel-body">
                    <h5 class="block-h5 block-h5one">采购单信息</h5>
                    <div class="information-list">
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label class="left-title80">采购主题：</label>
                            <div class="right-title3">
                                <span><?php echo isset($data['purchaseInfo']['subject']) ? $data['purchaseInfo']['subject'] : '' ?></span>
                            </div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label class="left-title80">采购编号：</label>
                            <div class="right-title3">
                                <span><?php echo isset($data['purchaseInfo']['no']) ? $data['purchaseInfo']['no'] : '' ?></span>
                            </div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label class="left-title80">采购类型：</label>
                            <div class="right-title3">
                                <span><?php echo isset($data['purchaseInfo']['purchase_type_name']) ? $data['purchaseInfo']['purchase_type_name'] : '' ?></span>
                            </div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label class="left-title80">采购状态：</label>
                            <div class="right-title3">
                                <?php if ($data['purchaseInfo']['audit_status'] == 1 && $data['purchaseInfo']['status'] == 3) { ?>
                                    <span>待审批</span>
                                <?php } ?>
                                <?php if ($data['purchaseInfo']['audit_status'] == 2 && $data['purchaseInfo']['status'] == 2) { ?>
                                    <span>部分入库</span>
                                <?php } ?>
                                <?php if ($data['purchaseInfo']['audit_status'] == 2 && $data['purchaseInfo']['status'] == 3) { ?>
                                    <span>待入库</span>
                                <?php } ?>
                                <?php if ($data['purchaseInfo']['audit_status'] == 2 && $data['purchaseInfo']['status'] == 1) { ?>
                                    <span>入库完毕</span>
                                <?php } ?>
                                <?php if ($data['purchaseInfo']['audit_status'] == 3) { ?>
                                    <span>未通过</span>
                                <?php } ?>
                                <?php if ($data['purchaseInfo']['status'] == 4) { ?>
                                    <span>已取消</span>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label class="left-title80">发货状态：</label>
                            <div class="right-title3">
                                <span><?php echo isset($shipStatusList[$data['purchaseInfo']['isship']])?$shipStatusList[$data['purchaseInfo']['isship']]:''?></span>
                            </div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label class="left-title80">供货商：</label>
                            <div class="right-title3">
                                <span><?php echo isset($data['purchaseInfo']['supplier_name']) ? $data['purchaseInfo']['supplier_name'] : '' ?></span>
                            </div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label class="left-title80">采购人：</label>
                            <div class="right-title3">
                                <span><?php echo isset($data['purchaseInfo']['purchase_user_name']) ? $data['purchaseInfo']['purchase_user_name'] : '' ?></span>
                            </div>
                        </div>

                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label class="left-title80">申请时间：</label>
                            <div class="right-title3">
                                <span><?php echo isset($data['purchaseInfo']['purchase_date']) ? $data['purchaseInfo']['purchase_date'] : '' ?></span>
                            </div>
                        </div>
                        <?php if (isset($data['purchaseInfo']['audit_user_id'])): ?>
                            <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                                <label class="left-title80">审批人：</label>
                                <div class="right-title3 2693ff">
                                    <span><?php echo isset($data['purchaseInfo']['audit_user_name']) ? $data['purchaseInfo']['audit_user_name'] : '' ?></span>
                                </div>
                            </div>

                            <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                                <label class="left-title80">审批时间：</label>
                                <div class="right-title3">
                                    <span><?php echo isset($data['purchaseInfo']['audit_date']) ? $data['purchaseInfo']['audit_date'] : '' ?></span>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-xs-12   col-sm-12   col-md-12    col-lg-12">
                            <label class="left-title80">备注：</label>
                            <div class="right-title3">
                                <span><?php echo isset($data['purchaseInfo']['description']) ? $data['purchaseInfo']['description'] : '' ?></span>
                            </div>
                        </div>
                    </div>
                    <?php if (isset($data['purchaseInfo']['audit_user_id'])): ?>
                        <div class="information-list">
                            <div class="col-xs-12	col-sm-12	col-md-12	col-lg-12">
                                <label class="left-title80">审批意见：</label>
                                <div class="right-title3">
                                    <span><?php echo isset($data['purchaseInfo']['audit_suggest']) ? $data['purchaseInfo']['audit_suggest'] : '' ?></span>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-12"><h5 class="block-h5">采购产品明细</h5></div>
                    <div class="information-list" style="height: auto">
                        <table>
                            <thead>
                            <tr>
                                <th>产品名称</th>
                                <th>产品编号</th>
                                <th>产品品牌</th>
                                <th>产品型号</th>
                                <th>计量单位</th>
                                <th>采购数量</th>
                                <th>建议进价</th>
                                <th>采购价</th>
                                <th>总价</th>
                                <th>待入库数量</th>
                                <th>已入库数量</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($data['purchaseDetail']) { ?>
                                <?php foreach ($data['purchaseDetail'] as $val) : ?>
                                    <tr>
                                        <td><?= $val['prod_name'] ?></td>
                                        <td><?= $val['prod_no'] ?></td>
                                        <td><?php echo isset($val['brand_name']) ? $val['brand_name'] : '' ?></td>
                                        <td><?= $val['model'] ?></td>
                                        <td><?php echo isset($val['unit_name']) ? $val['unit_name'] : '' ?></td>
                                        <td><?= isset($val['prod_num']) ? $val['prod_num'] : '' ?></td>
                                        <td><?= isset($val['purchase_price']) ? $val['purchase_price'] : '' ?></td>
                                        <td><?= isset($val['price']) ? $val['price'] : '' ?></td>
                                        <td><?= isset($val['total_amount']) ? $val['total_amount'] : '' ?></td>
                                        <td><?= $val['prod_num'] - $val['finish_num'] ?></td>
                                        <td><?php echo isset($val['finish_num']) ? $val['finish_num'] : 0 ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="11" class="no-record">暂无产品明细</td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div class="information-list">
                            <label class="left-title80" style="text-align: left;left: 0">总计金额：</label>
                            <div class="right-title3" style="margin-left: 70px"><span><?= $data['purchaseInfo']['total_amount'] ?></div>
                        </div>
                    </div>
                    <div class="col-md-12"><h5 class="block-h5">收货记录</h5></div>
                    <div class="information-list" style="height: auto">
                        <table>
                            <thead>
                            <tr>
                                <th>收货单号</th>
                                <th>收货状态</th>
                                <th>收货产品数</th>
                                <th>申请时间</th>
                                <th>收货时间</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($data['sendRecord']) { ?>
                                <?php foreach ($data['sendRecord'] as $key=>$val) : ?>
                                    <tr>
                                        <td style="max-width: 180px">
                                            <a href="/send-out/view?id=<?= isset($val['id']) ? $val['id'] : "" ?>"><?= isset($val['send_no']) ? $val['send_no'] : '' ?></a>
                                        </td>
                                        <td><?php echo isset($sendStatusList[$val['send_status']]) ? $sendStatusList[$val['send_status']] : ''; ?></td>
                                        <td><?php echo isset($data['purchaseInfo']['total_prod_num']) ? $data['purchaseInfo']['total_prod_num'] : ''; ?></td>
                                        <td><?php echo isset($val['create_time']) ? $val['create_time'] : ''; ?></td>
                                        <td><?php echo isset($val['rel_rec_date']) ? $val['rel_rec_date'] : ''; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="11" class="no-record">暂无退货记录</td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12"><h5 class="block-h5">入库记录</h5></div>
                    <div class="information-list" style="height: auto">
                        <table>
                            <thead>
                            <tr>
                                <!--<th>序号</th>-->
                                <th>入库单号</th>
                                <th>入库状态</th>
                                <th>申请时间</th>
                                <th>入库时间</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($data['storeRecord'] && $data['purchaseInfo']['audit_status'] == 2) { ?>
                                <?php foreach ($data['storeRecord'] as $val) : ?>
                                    <tr>
                                        <td style="max-width: 180px">
                                            <a href="/stock-in/view?id=<?= isset($val['id']) ? $val['id'] : "" ?>"><?= isset($val['no']) ? $val['no'] : '' ?></a>
                                        </td>
                                        <?php if ($data['purchaseInfo']['status'] == 1 && $data['purchaseInfo']['audit_status'] == 2) { ?>
                                            <td>已入库</td>
                                        <?php } else { ?>
                                            <td><?php if ($val['audit_status'] == 1) {
                                                    echo '待审批';
                                                } else {
                                                    echo '已入库';
                                                } ?></td>
                                        <?php } ?>
                                        <td><?php echo isset($val['create_time']) ? $val['create_time'] : ''; ?></td>
                                        <td><?php echo isset($val['audit_date']) ? $val['audit_date'] : ''; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="11" class="no-record">暂无入库记录</td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php if ($data['returnRecord']) { ?>
                    <div class="col-md-12"><h5 class="block-h5">退货记录</h5></div>
                    <div class="information-list" style="height: auto">
                        <table>
                            <thead>
                            <tr>
                                <th>采购退货单号</th>
                                <th>退货状态</th>
                                <th>退货产品数</th>
                                <th>申请时间</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($data['returnRecord']) { ?>
                                <?php foreach ($data['returnRecord'] as $val) : ?>
                                    <tr>
                                        <td style="max-width: 180px">
                                            <a href="/return-purchase/view?id=<?= isset($val['id']) ? $val['id'] : "" ?>"><?= isset($val['no']) ? $val['no'] : '' ?></a>
                                        </td>
                                        <td><?php echo isset($returnStatusList[$val['status']]) ? $returnStatusList[$val['status']] : ''; ?></td>
                                        <td><?php echo isset($val['prod_num']) ? $val['prod_num'] : ''; ?></td>
                                        <td><?php echo isset($val['create_time']) ? $val['create_time'] : ''; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="11" class="no-record">暂无退货记录</td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                <?php }?>
            </div>
                <!--endprint1-->
            </div>
        </div>
    </div>
</section>
<!-- 设置同意拒绝弹层 -->
<div id="noLayer" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <div class="select-fuji">
            <textarea placeholder="请填写审批意见，非必填" id="opinionContent" style="width: 100%;height: 100px;"
                      onkeyup=" wordsLimit(this)"></textarea>
            <p class="xianzhi" style="right: 10px;"><span id="word">0</span>/200</p>
        </div>
        <div style="text-align: center;margin-top:15px;">
            <button class="btn bg-f7 mgr8" id="layerCancel"><i class="fa fa-search"></i>取消</button>
            <button class="btn btn-orange" id="layerSubmit" style="padding: 4px 12px" data-type="3"><i
                        class="fa fa-search"></i>拒绝
            </button>
        </div>
    </div>
</div>
<div id="okLayer" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <div class="select-fuji">
            <textarea placeholder="请填写审批意见，非必填" id="opinionContent" style="width: 100%;height: 100px;"
                      onkeyup=" wordsLimit(this)"></textarea>
            <p class="xianzhi" style="right: 10px;"><span id="word">0</span>/200</p>
        </div>
        <div style="text-align: center;margin-top:15px;">
            <button class="btn bg-f7 mgr8" id="layerCancel"><i class="fa fa-search"></i>取消</button>
            <button class="btn btn-orange" id="layerSubmit" data-type="2"><i class="fa fa-search"></i>同意</button>
        </div>
    </div>
</div>
<script>
    var layerIndex = null;
    //var purchaseNo = $(this).attr('data-no');
    $( ".setAudit" ).click( function () {
        var setType = $( this ).attr( 'data-type' );
        if ( setType == 'ok' ) {
            var statusType = 2;
            layerIndex = layer.open( {
                type    : 1,
                title   : '确认同意采购吗？',
                area    : ['400px', '250px'],
                fixed   : true,
                maxmin  : false,
                content : $( '#okLayer' ).html()
            } );
        } else {
            var statusType = 3;
            layerIndex = layer.open( {
                type    : 1,
                title   : '确认拒绝采购吗？',
                area    : ['400px', '250px'],
                fixed   : true,
                maxmin  : false,
                content : $( '#noLayer' ).html()
            } );
        }
        return false;
    } );
    //提交取消订单
    $( "body" ).delegate( "#layerSubmit", "click", function () {
        var purchaseNo = $( '#no' ).attr( 'data-no' );
        var statusType = $( this ).attr( 'data-type' );
        var opinion_content = $( this ).parents( 'div' ).find( '#opinionContent' ).val();
        //var statusType = statusType;
        //alert(statusType);return false;
        if ( $.trim( purchaseNo ) == '' ) {
            alert( '采购编号不能为空1' );
            return false;
        }
        if ( $.trim( statusType ) == '' ) {
            alert( '操作状态错误！' );
            return false;
        }
        $.getJSON( '/purchase/set-audit-status', {
            'no'            : purchaseNo,
            'status'        : statusType,
            'audit_suggest' : opinion_content
        }, function ( json ) {
            //console.log(json);return false;
            if ( json.success == 1 ) {
                if ( layerIndex ) {
                    layer.close( layerIndex );
                }
                alert( json.message );
                setTimeout( function () {location.reload();}, 1000 );
                //window.location.reload(true);
            }
            else {
                alert( json.message );
            }
        } );
    } );
    //关闭弹层
    $( "body" ).delegate( "#layerCancel", "click", function () {
        if ( layerIndex ) {
            layer.close( layerIndex );
        }
    } );

    //打印
    function preview( oper ) {
        if ( oper < 10 ) {
            bdhtml = window.document.body.innerHTML;//获取当前页的html代码
            sprnstr = "<!--startprint" + oper + "-->";//设置打印开始区域
            eprnstr = "<!--endprint" + oper + "-->";//设置打印结束区域
            prnhtml = bdhtml.substring( bdhtml.indexOf( sprnstr ) + 18 ); //从开始代码向后取html
            prnhtml = prnhtml.substring( 0, prnhtml.indexOf( eprnstr ) );//从结束代码向前取html
            window.document.body.innerHTML = prnhtml;
            window.print();
            window.document.body.innerHTML = bdhtml;
        } else {
            window.print();
        }
    }
</script>

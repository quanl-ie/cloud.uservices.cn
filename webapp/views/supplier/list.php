<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\LinkPager;
    $this->title = '库房列表';
    $this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time()?>">
<style>
    body{background: #fff;}
</style>
<section id="main-content">                 
    <div class="panel-body">
        <form action="/supplier/get-list" class="form-horizontal form-border" id="form">
            <div class="table-responsive">
                <div class="single-search" style="width: 300px">
                    <label class="search-box-lable" style="width: 100px;">供货商名称</label>
                    <div class="single-search-kuang1" style="margin-left: 110px;">
                        <input type="text" class="form-control" name="name" maxlength="20" value="<?=isset($_GET['name']) ? $_GET['name'] : '' ?>">
                    </div>
                </div>
                <div class="search-confirm" style="width: 92px">
                    <a href="/supplier/add" target="_parent"  class="btn btn-orange">添加供货商</a>
                </div>
                <div class="search-confirm" style="margin-left: 20px;">
                    <button  class="btn btn-orange">
                        <i class=" "></i>搜索
                    </button>
                </div>

            </div>
            <div class="table-responsive"   style="width: 90%;margin:0 auto;padding-top: 10px">
                <table class="table table-bordered table-striped table-hover">
                    <thead bgcolor="#f7f7f7">
                    <tr>
                        <th></th>
                        <th>供货商编号</th>
                        <th>供货商名称</th>
                        <th>联系人姓名</th>
                        <th style="border-right: 1px solid #ddd;">联系电话</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($data)) : ?>
                        <?php foreach ($data as $key => $val) : ?>
                            <tr>
                                <td style="width: 50px">
                                    <input type="radio" name="prod_id" class="id" value="<?=$val['id']?>" data-name="<?php echo isset($val['name'])?$val['name']:''?>" class="available_cost_type" id="bh<?=$val['id']?>"/>
                                    <label for="bh<?=$val['id']?>" style="margin-top: 5px;cursor: pointer;"></label>
                                </td>
                                <td><?=$val['no'] ?></td>
                                <td><?=$val['name'] ?></td>
                                <td><?=$val['contact_name'] ?></td>
                                <td style="border-right: 1px solid #ddd;"><?=$val['contact_mobile'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr><tr><td colspan="7" class="no-record" style="text-align: center;">暂无数据</td></tr></tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </form>
        <div class="col-xs-12 text-center pagination">
            <?php echo $pageHtml;?>
        </div>
    </div>
</section>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>
<script>
    $(".id").click(function(){
        var Id = $(this).val();
        var Name = $(this).attr('data-name');
        parent.$("#supplier_id").val(Id); //父页面接收json
        parent.$("#supplier_name").val(Name); //父页面接收json
        parent.$("#supplier_name").attr('title',Name);
        var index = parent.layer.getFrameIndex(window.name); //关闭当前弹出层
        parent.layer.close(index);
    })
</script>
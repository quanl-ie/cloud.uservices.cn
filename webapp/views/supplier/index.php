<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
$this->title = '供货商列表';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time()?>">
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">供货商管理列表</span>
    <?php if(in_array('/supplier/add',$selfRoles)){?>
        <a href="/supplier/add" style="color: #fff;"><span class="btn btn-success jn-btn" style=" color:#FFF;float:right;width:auto;"  data-toggle="modal" data-target="#scrollingModal">添加</span></a>
    <?php };?>
</div>
<section id="main-content">
    <div class="panel-body">
        <form action="/supplier/index" class="form-horizontal form-border" id="form">
            <div class="form-group jn-form-box">
                <div class="col-sm-12 no-padding-left">
                    <div class="single-search">
                        <label class="search-box-lable">供货商编号</label>
                        <div class="single-search-kuang1">
                            <input type="text" class="form-control" name="no" maxlength="20" value="<?php echo isset($_GET['no'])?$_GET['no']:'';?>">
                        </div>
                    </div>
                    <div class="single-search">
                        <label class="search-box-lable">供货商名称</label>
                        <div class="single-search-kuang1">
                            <input type="text" class="form-control" name="name" maxlength="11" value="<?php echo isset($_GET['name'])?$_GET['name']:'';?>">
                        </div>
                    </div>
                    <div class="single-search">
                        <label class="search-box-lable">等级</label>
                        <div class="single-search-kuang1">
                            <select class="form-control" name="level">
                                <option value="">请选择</option>
                                <?php foreach ($levelSelect as $key=>$val){?>
                                    <option value="<?php echo $val['dict_enum_id']; ?>" <?php if(isset($_GET['level']) && $_GET['level']==$val['dict_enum_id']):?>selected<?php endif; ?>><?php echo $val['dict_enum_value'];?></option>
                                <?php };?>
                            </select>
                        </div>
                    </div>
                    <div class="single-search">
                        <label class="search-box-lable">状态</label>
                        <div class="single-search-kuang1">
                            <select class="form-control" name="status">
                                <option value="">全部</option>
                                <option value="0" <?php if($params['status']=='0'){echo 'selected="selected"';}?>>正常</option>
                                <option value="1" <?php if($params['status']=='1'){echo 'selected="selected"';}?>>禁用</option>
                            </select>
                        </div>
                    </div>
                    <div class="search-confirm">   
                        <button class="btn btn-success">
                            <i class=" "></i> 搜索
                        </button>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead bgcolor="#455971">
                    <tr>
                        <!--<th>序号</th>-->
                        <th>供货商编号</th>
                        <th>供货商名称</th>
                        <th>联系人姓名</th>
                        <th>联系人电话</th>
                        <th>联系地址</th>
                        <th>等级</th>
                        <th>状态</th>
                        <th>创建时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if($data){?>
                        <?php foreach ($data as $key=>$val){?>
                            <tr>
                                <!--<td><?php /*echo isset($val['id']) ? $val['id'] : ''; */?></td>-->
                                <td>
                                    <?php echo $val['no']; ?>
                                </td>
                                <td>
                                    <?php echo $val['name']; ?>
                                </td>
                                <td>
                                    <?php echo $val['contact_name']; ?>
                                </td>
                                <td>
                                    <?php echo isset($val['contact_mobile']) ? $val['contact_mobile'] : ''; ?>
                                </td>
                                <td>
                                    <?php echo isset($val['full_address']) ? $val['full_address'] : ''; ?>
                                </td>

                                <td>
                                    <?php echo isset($val['level_name']) ? $val['level_name'] : ''; ?>
                                </td>
                                <td>
                                    <?php if($val['status']==0){echo '正常';}else{echo '禁用';}; ?>
                                </td>
                                <td>
                                    <?php echo isset($val['create_time']) ? $val['create_time'] : ''; ?>
                                </td>
                                <td class="operation">
                                    <?php if(in_array('/supplier/edit',$selfRoles)){?>
                                        <a href="/supplier/edit?id=<?php echo $val['id'];?>" class="update">编辑</a>
                                    <?php };?>
                                    <?php if($val['status']==1){?>
                                        <?php if(in_array('/supplier/set-status',$selfRoles)){?>
                                            <a href="/supplier/set-status?id=<?php echo $val['id'];?>&t=1" onclick="return confirm('您确定要启用吗？',function(obj){ document.location.href = obj.href ;} , this);">启用</a>
                                        <?php };?>
                                    <?php }else{?>
                                        <?php if(in_array('/supplier/set-status-off',$selfRoles)){?>
                                            <a href="/supplier/set-status-off?id=<?php echo $val['id'];?>&t=0" onclick="return confirm('您确定要禁用吗？',function(obj){ document.location.href = obj.href ;} , this);">禁用</a>
                                        <?php };?>
                                    <?php }?>
                                </td>
                            </tr>
                        <?php }?>
                    <?php }else{?>
                        <tr><td colspan="11" class="no-record">没有供货商数据
                                <?php if(in_array('/supplier/add',$selfRoles)){?>
                                    <span><a href="/supplier/add">立即添加</a></span>
                                <?php };?>
                            </td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
        </form>
        <div class="col-xs-12 text-center  pagination">
            <?php echo $pageHtml;?>
        </div>
    </div>
          
</section>
<script>
    //删除库房
    function delChick(id){
        confirm('删除该库房将不能恢复！',function(){
            Del(id);
        },1);
        return false;
    }
    //删除操作
    function Del(id){
        $.post('/depot/del',{id:id},function(data) {
            var res = $.parseJSON(data);
            alert(res.message);
            setTimeout(function(){location.reload();},2000);
            //window.location.reload();
        })
    }
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
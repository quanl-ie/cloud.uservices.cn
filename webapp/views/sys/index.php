<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '系统设置';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<style type="text/css">
    body{background: #f1f2f3;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">系统设置</span>
</div> 

<section id="main-content">
    <div class="nav-box-list">
        <?php if(in_array('/user/index',$selfRoles) || in_array('/organization/index',$selfRoles) || in_array('/role/index',$selfRoles)){?>
            <h4>用户和权限</h4>
            <div class="nav-content">
                <?php if(in_array('/user/index',$selfRoles)){?>
                    <a href="/user/index" class="nav-content-a">
                        <img  src="<?php echo Url::to('/images/sz1.png');?>" alt="优服务">
                        <span>用户管理</span>
                    </a>
                <?php };?>
                <?php if(in_array('/organization/index',$selfRoles)){?>
                    <a href="/organization/index" class="nav-content-a">
                        <img src="<?php echo Url::to('/images/zuzhijiagou.png');?>" alt="优服务">
                        <span>组织架构</span>
                    </a>
                <?php };?>
                <?php if(in_array('/role/index',$selfRoles)){?>
                    <a href="/role/index" class="nav-content-a">
                        <img  src="<?php echo Url::to('/images/sz2.png');?>" alt="优服务">
                        <span>角色管理</span>
                    </a>
                <?php };?>
            </div>
        <?php }?>
    </div>
    
    <div class="nav-box-list">
        <h4>基础设置<span style="font-size: 14px;color: #666">(<i class="red-i">*</i>为确保系统正常使用，请完成所有基础项设置)</span></h4>
        <div class="nav-content">
            <?php if(in_array('/brand/index',$selfRoles)){?>
                <a href="/brand/index" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/sz8.png');?>" alt="优服务">
                    <span>品牌管理</span>
                </a>
            <?php };?>
            <?php if(in_array('/category/index',$selfRoles)){?>
                <a href="/category/index" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/sz9.png');?>" alt="优服务">
                    <span>类目管理</span>
                </a>
            <?php };?>
            <?php if(in_array('/product/index',$selfRoles)){?>
                <a href="/product/index" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/chanpingguanli.png');?>" alt="优服务">
                    <span>产品管理</span>
                </a>
            <?php };?>
            <?php if(in_array('/type/index',$selfRoles)){?>
                <a href="/type/index" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/sz5.png');?>" alt="优服务">
                    <span>服务类型</span>
                </a>
            <?php };?>
            <?php if(in_array('/area/index',$selfRoles)){?>
                <a href="/area/index" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/sz6.png');?>" alt="优服务">
                    <span>服务区域</span>
                </a>
            <?php };?>
            <?php if(in_array('/flow/index',$selfRoles)){?>
                <a href="/flow/index" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/sz7.png');?>" alt="优服务">
                    <span>服务流程</span>
                </a>
            <?php };?>
            <?php if(in_array('/cost-item/index',$selfRoles)){?>
                <a href="/cost-item/index" class="nav-content-a">
                    <img  src="<?php echo Url::to('/images/sz3.png');?>" alt="优服务">
                    <span>收费项目</span>
                </a>
            <?php };?>
            <?php if(in_array('/cost-level/index',$selfRoles)){?>
                <a href="/cost-level/index" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/sz4.png');?>" alt="优服务">
                    <span>收费标准</span>
                </a>
            <?php };?>
            <?php if(in_array('/contract-type/index',$selfRoles)){?>
                <a href="/contract-type/index" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/contract.png');?>" alt="优服务">
                    <span>合同分类</span>
                </a>
            <?php };?>

            <?php if(in_array('/sigin-setting/set-pay',$selfRoles)){?>
                <a href="/sigin-setting/set-pay" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/zhifushezhi.jpg');?>" alt="优服务">
                    <span>支付设置</span>
                </a>
            <?php };?>
            <?php if(in_array('/sigin-setting/set-sign',$selfRoles)){?>
                <a href="/sigin-setting/set-sign" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/yonghuqianming.png');?>" alt="优服务">
                    <span>客户签名设置</span>
                </a>
            <?php };?>
            <?php if(in_array('/fee/setting',$selfRoles)){?>
                <a href="/fee/setting" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/sz3.png');?>" alt="优服务">
                    <span>确认收费设置</span>
                </a>
            <?php };?>
            <?php /*if($data['departmentId'] == $data['directCompanyId']) :*/?><!--
                <?php /*if(in_array('/sigin-setting/index',$selfRoles)){*/?>
                    <a href="/sigin-setting/index" class="nav-content-a">
                        <img src="<?php /*echo Url::to('/images/qiandaoshezhi.png');*/?>" alt="优服务">
                        <span>签到设置</span>
                    </a>
                <?php /*};*/?>
            --><?php /*endif;*/?>
            <?php if(in_array('/sigin-setting/index',$selfRoles)){?>
                <a href="/sigin-setting/index" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/qiandaoshezhi.png');?>" alt="优服务">
                    <span>签到设置</span>
                </a>
            <?php };?>
            <?php if(Yii::$app->params['chaidan']['directCompanyId'] == $data['directCompanyId']){?>
                <a href="/dismantling-config/index" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/chaidan.png');?>" alt="优服务">
                    <span>拆单设置</span>
                </a>
            <?php };?>

            <?php if(in_array('/goods/on-line-list',$selfRoles)){?>
                <a href="/goods/index?status=1" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/goods.png');?>" alt="商品管理">
                    <span>商品管理</span>
                </a>
            <?php } elseif(in_array('/goods/off-line-list',$selfRoles)){?>
                <a href="/goods/index?status=2" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/goods.png');?>" alt="商品管理">
                    <span>商品管理</span>
                </a>
            <?php };?>
            <?php if(in_array('/appraisal-set/index',$selfRoles)){?>
                <a href="/appraisal-set/index" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/pingjia.png');?>" alt="优服务">
                    <span>评价设置</span>
                </a>
            <?php };?>
        </div>
    </div>
    <!--<div class="nav-box-list">
        <h4>仓储设置</h4>
        <div class="nav-content">
            <a href="javascript:" class="nav-content-a">
                <img src="<?php echo Url::to('/images/sz10.png');?>" alt="优服务">
                <span>开启设置</span>
            </a>
        </div>
    </div>-->
    <?php if(in_array('/open-weixin/bind',$selfRoles)){?>
        <div class="nav-box-list">
            <h4>小程序设置</h4>
            <div class="nav-content">
                <a href="/open-weixin/bind" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/sz13.png');?>" alt="优服务">
                    <span>开通小程序</span>
                </a>
            </div>
        </div>
    <?php }?>
    <?php if(in_array('/manufactor/view',$selfRoles)){?>
    <div class="nav-box-list">
        <h4>企业设置</h4>
        <div class="nav-content">
                <a href="/manufactor/view" class="nav-content-a">
                    <img src="<?php echo Url::to('/images/sz11.png');?>" alt="优服务">
                    <span>企业信息</span>
                </a>
        </div>
    </div>
    <?php };?>
</section>
<?php
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
    use webapp\models\Menu;

$this->title = '客户签名设置';
$this->params['breadcrumbs'][] = ['label' => '系统设置', 'url' => ['sys/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<style>
    .modal-content {border: none;border-radius: 0px;box-shadow: inherit !important;}
    .ck_role .checkbox{float:left;margin-right:10px;}
    .radio { float: left;margin-top: -5px;}
    .radio label, .checkbox label{padding-left: 0;padding-right: 30px;}
</style>
<section id="main-content">
    <div class="panel-body"><?php
            $form = ActiveForm::begin([
                'options' => [
                    'class' => 'form-horizontal',
                    'id' => 'sigin-setting',
                    'name' => 'sigin-setting',
                    'enctype' => 'multipart/form-data',
                
                ],
                'fieldConfig' => [
                    'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-xs-6 addinput-list\">{input}{error}</div>",
                    'labelOptions' => ['class' => 'name-title-lable'],
                ]
            ]);
        ?>

        <input type="hidden" name="id" value="<?=isset($list->id) ? $list->id :""; ?>" >

        <div class="form-group">
            <label class="name-title-lable">客户签名开关：</label>
            <div class="col-xs-6 addinput-list" style="padding-left: 0">
                <?php if (!empty($option)) : ?>
                <?php foreach ($option as $key => $val) : ?>
                <div class="radio">
                    <?php if(isset($list->id)):?>
                    <input type="radio" name="sigin_status" <?php if (isset($list->sigin_status) && $list->sigin_status == $key) : ?>checked="checked" <?php endif; ?> value="<?=$key ?>" id="<?=$key ?>" class="setting"><label for="<?=$key ?>"><?=$val ?></label>
                    <?php else:?>
                        <input type="radio" name="sigin_status" <?php if ($key==2) : ?>checked="checked" <?php endif; ?> value="<?=$key ?>" id="<?=$key ?>" class="setting"><label for="<?=$key ?>"><?=$val ?></label>
                    <?php endif;?>
                </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        
        <hr>
        <div class="form-group">
            <div class="col-xs-6 addinput-list">
                <input type="submit" id="commitBtn" class="btn btn-orange mgr20 " style=" color:#FFF;" value="保存" />
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <span style="color: red">注：<br/>技师端APP提交收费信息时由客户签名确认。<br/>如启用，则技师必须在提交收费时请客户签名；如不启用，则无需客户签名。</span>
    </div>
</section>
<script>
    window.onload = function(){
        
        <?php if(Yii::$app->session->hasFlash('sys_config_message')):?>
        setTimeout(function () {
            alert('<?php echo Yii::$app->session->getFlash('sys_config_message'); ?>');
        },500);
        <?php endif;?>
    }
</script>

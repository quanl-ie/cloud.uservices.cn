<?php
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
    use webapp\models\Menu;
    
    $this->title = '支付开关';
    $this->params['breadcrumbs'][] = ['label' => '系统设置', 'url' => ['sys/index']];
    $this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<style>
    .modal-content {border: none;border-radius: 0px;box-shadow: inherit !important;}
    .ck_role .checkbox{float:left;margin-right:10px;}
    .radio { float: left;margin-top: -5px;}
    .radio label, .checkbox label{padding-left: 0;padding-right: 30px;}
</style>
<section id="main-content">
    <div class="panel-body"><?php
            $form = ActiveForm::begin([
                'options' => [
                    'class' => 'form-horizontal',
                    'id' => 'sigin-setting',
                    'name' => 'sigin-setting',
                    'enctype' => 'multipart/form-data',
                
                ],
                'fieldConfig' => [
                    'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-xs-6 addinput-list\">{input}{error}</div>",
                    'labelOptions' => ['class' => 'name-title-lable'],
                ]
            ]);
        ?>

        <input type="hidden" name="id" value="<?=isset($list->id) ? $list->id :""; ?>" >

        <div class="form-group">
            <label class="name-title-lable">支付功能开关：</label>
            <div class="col-xs-6 addinput-list" style="padding-left: 0">
                <?php if (!empty($option)) : ?>
                <?php foreach ($option as $key => $val) : ?>
                <div class="radio">
                    <?php if(isset($list->id)):?>
                    <input type="radio" name="pay_status" <?php if (isset($list->pay_status) && $list->pay_status == $key) : ?>checked="checked" <?php endif; ?> value="<?=$key ?>" id="<?=$key ?>" class="setting"><label for="<?=$key ?>"><?=$val ?></label>
                    <?php else:?>
                        <input type="radio" name="pay_status" <?php if ($key==2) : ?>checked="checked" <?php endif; ?> value="<?=$key ?>" id="<?=$key ?>" class="setting"><label for="<?=$key ?>"><?=$val ?></label>
                    <?php endif;?>
                </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        
        <hr>
        <div class="form-group">
            <div class="col-xs-6 addinput-list">
                <input type="submit" id="commitBtn" class="btn btn-orange mgr20 " style=" color:#FFF;" value="保存" />
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <span style="color: red">注：<br/>1.启用支付设置后，支持微信、支付宝等收费方式，技师收款后，公司需要找平台进行结算；<br/>2.关闭支付设置，只提供技师现金收费方式，由技师自行收取费用后上交给公司。<br/>3.每次开启或关闭支付功能时，建议退出账号重新登录，方可与平台正常结算。</span>
    </div>
</section>
<script>
    window.onload = function(){
        
        <?php if(Yii::$app->session->hasFlash('sys_config_message')):?>
        setTimeout(function () {
            alert('<?php echo Yii::$app->session->getFlash('sys_config_message'); ?>');
        },500);
        <?php endif;?>
    }

    $(".setting").click(function(){
        var id = $(this).val();
        //显示设置范围
        if(id==1){
            $("#show_div").css('display','block');
        }else{
            $("#show_div").css('display','none');
        }
    })
</script>

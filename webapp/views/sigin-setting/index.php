<?php
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
    use webapp\models\Menu;
    
    $this->title = '签到设置';
    $this->params['breadcrumbs'][] = ['label' => '系统设置', 'url' => ['/sys/index']];
    $this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<style>
    .modal-content {border: none;border-radius: 0px;box-shadow: inherit !important;}
    .ck_role .checkbox{float:left;margin-right:10px;}
    .radio { float: left;margin-top: -5px;}
    .radio label, .checkbox label{padding-left: 0;padding-right: 30px;}
</style>
<section id="main-content">
    <div class="panel-body"><?php
            $form = ActiveForm::begin([
                'options' => [
                    'class' => 'form-horizontal',
                    'id' => 'sigin-setting',
                    'name' => 'sigin-setting',
                    'enctype' => 'multipart/form-data',
                
                ],
                'fieldConfig' => [
                    'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-xs-6 addinput-list\">{input}{error}</div>",
                    'labelOptions' => ['class' => 'name-title-lable'],
                ]
            ]);
        ?>

        <input type="hidden" name="id" value="<?=isset($list->id) ? $list->id :""; ?>" >

        <div class="form-group">
            <label class="name-title-lable">签到开关：</label>
            <div class="col-xs-6 addinput-list" style="padding-left: 0">
                <?php if (!empty($option)) : ?>
                <?php foreach ($option as $key => $val) : ?>
                <div class="radio">
                    <?php if(isset($list->id)):?>
                    <input type="radio" name="sigin_status" <?php if (isset($list->sigin_status) && $list->sigin_status == $key) : ?>checked="checked" <?php endif; ?> value="<?=$key ?>" id="<?=$key ?>" class="setting"><label for="<?=$key ?>"><?=$val ?></label>
                    <?php else:?>
                        <input type="radio" name="sigin_status" <?php if ($key==2) : ?>checked="checked" <?php endif; ?> value="<?=$key ?>" id="<?=$key ?>" class="setting"><label for="<?=$key ?>"><?=$val ?></label>
                    <?php endif;?>
                </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div  class="form-group" id="show_div"  style="padding-left: 0;<?php if(isset($list->id)&&$list->sigin_status==1){echo 'display:block;';}else{echo 'display: none;'; }?>">
             <label class="name-title-lable" style="height: 32px;line-height: 32px;">偏差范围 :允许</label>
            <div class="col-xs-12 addinput-list" style="line-height: 30px;">

                 <input name="service_radius" id="service_radius" size="10" value="<?php if(isset($list->service_radius)){echo $list->service_radius;}?>" style="border-radius: 4px;height: 30px;text-align: center;">m 内的距离偏差（上门服务地址与技师开始服务地址的距离大于允许偏差值，将上报异常）。
            </div>
        </div>
        
        <hr>
        <div class="form-group">
            <div class="col-xs-6 addinput-list">
                <input type="submit" id="commitBtn" class="btn btn-orange mgr20 " style=" color:#FFF;" value="保存" />
            </div>
        </div>
        <?php ActiveForm::end(); ?>
<!--        <span style="color: red">注：启用后，技师只能在设置的范围以内点击开始服务。</span>-->
    </div>
</section>
<script>
    window.onload = function(){
        
        <?php if(Yii::$app->session->hasFlash('sys_config_message')):?>
        setTimeout(function () {
            alert('<?php echo Yii::$app->session->getFlash('sys_config_message'); ?>');
        },500);
        <?php endif;?>
    }

    $(".setting").click(function(){
        var id = $(this).val();
        //显示设置范围
        if(id==1){
            $("#show_div").css('display','block');
        }else{
            $("#show_div").css('display','none');
            $("#service_radius").val('0');
        }
    })
    $("#service_radius").keyup(function(){
        var radius = $(this).val();
        if(isNaN(radius)){
            alert('仅支持大于0的整数！');
            $(this).val('0');
        }
    })

    $("#sigin-setting").submit(function(){
        var status = $(".setting:checked").val();
        if(status==1){
            var radius = $("#service_radius").val();
            if(isNaN(radius)||radius<=0){
                alert('仅支持大于0的整数！');
                return false;
            }
        }
    })
</script>

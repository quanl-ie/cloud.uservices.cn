<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '添加售后商品';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jquery.autocomplete.css');?>">
<!-- 日期插件-->

<script src="/js/My97DatePicker/WdatePicker.js"></script>
<script src="/js/jquery-1.10.2.min.js"></script>
<!DOCTYPE html>
<html>
<head>
</head> 
<body>
<style type="text/css">
      body{background: #fff;display: none;}
      .three-linkage{width: 100%;}
      .linkage-justify-list{width: 48.5%;font-size: 14px;font-family: "微软雅黑"}
      select,.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control{cursor: pointer;}
      .information-list{padding: 0 100px 0 60px;}
      .name-title-lable{left: 60px;}
      .pop-title100{position: fixed;z-index: 999;background: #fff;margin-top: 0;padding-top: 25px;}
      .pop-title100 span{position: absolute;}
      #intentionProduct{left:244px;}
      #customereProduct{left:348px;}
</style>
<div class="pop-title100">
    <div style="position: relative;width: 680px">
      <span id="intentionProduct" data-type="1">意向产品</span>
      <span id="customereProduct" data-type="2">客户产品</span>
    </div>
    

    <p class="position-left" style="top: 12px">添加产品信息</p>
</div>
<!-- <form action="/sale-order/add" method="post" id="sale-order"> -->
<form action="" method="post" id="sale-order" style="padding-top: 60px;">
      <!--   <input type=hidden" value="0"  id="sale-id" name="id" />-->
      <div class="col-md-8 information-list" style="margin-top: 0;">
            <label class="name-title-lable" > 客户：</label>
            <div class="addinput-list">
<!--                <input type="text" id="account_name" value="--><?//= isset($account['account_name']) ? $account['account_name']:'' ?><!--" --><?php //if(isset($account['account_name'])){echo 'readonly';}?><!-- name="account_name" datatype="*" sucmsg=" " nullmsg="请输入客户姓名"   placeholder="请输入客户姓名" maxlength="20" class="form-control inputxt" />-->
                <p class="height34"><?= isset($account['account_name']) ? $account['account_name']:'' ?></p>
                
                <input type="hidden" id="account_id" value="<?= isset($account['account_id']) ? $account['account_id'] :'' ?>" name="account_id">
                  <ul id="account_select">
                  </ul>
            </div>
      </div>
      <!-- 产品 -->
      <!--  -->
      <div class="col-md-8 information-list"  style="margin-top: 10px;display: none;">
            <label class="name-title-lable" ><span class="red-i">*</span> 服务类型：</label>
            <div class="addinput-list" id="showType">
                  <?php foreach($data['typeList'] as $k=>$v){
                        ?>
                        <div  class="radio-list">
                              <input type="radio" checked class="type_id" value="<?=$k?>" name="type_id" id="type_id_<?=$k?>" class="pr1" /><label for="type_id_<?=$k?>" id="type_value_<?=$k?>" style="font-family: '微软雅黑';font-weight: 100;font-size: 14px"><?=$v?></label>
                        </div>
                  <?php } ?>
                  <input type="hidden" value="" name="type_name" id="type_name" />
            </div>
      </div>
      <div class="col-md-8 information-list">
            <label class="name-title-lable" ><span class="red-i">*</span> 产品名称：</label>
            <div class="addinput-list">
                  <span id="prodNameSpan" style="position: absolute;top: 7px;margin: 0px 0px 0px 15px;color: #999999;">输入产品编码或名称</span>
                  <input type="hidden" id="sale_id" />
                  <input type="text"   id="prod_name_show" name="prod_name_show" value="<?= isset($account->prod_name) ? $account->prod_name :'' ?>"datatype="*" sucmsg=" " nullmsg="输入产品编码或名称"   maxlength="50" class="form-control inputxt" />
                  <input type="hidden" id="prod_name" name="prod_name" value="">
                  <input type="hidden" name="prod_id" class="form-control" id="prod_id" value="">
                  <input type="hidden" name="code" class="form-control" id="code" value="">
                  <p class="misplacement" id="prod_msg"></p>
            </div>
      </div>
      <div class="col-md-8 information-list" style="margin-top: 20px">
            <label class="name-title-lable" ><span class="red-i">*</span> 品牌：</label>
            <div class="addinput-list">
                  <select name="brand_id" id="brand_id" class="form-control" datatype="*" sucmsg=" " nullmsg="请选择品牌">
                        <option value="">请选择一品牌</option>
                        <?php foreach($data['brandList'] as $k=>$v){
                              echo '<option value="'.$k.'">'.$v.'</option>';
                        }?>
                  </select>
                  <input type="hidden" id="brand_name" name="brand_name"/>
                  <input type="hidden" id="brand_check_id" name="brand_check_id"/>
                  <p class="misplacement"  id="brand_msg"></p>
            </div>
      </div>
      <!--产品分类-->
      <div class="col-md-8 information-list">
            <label class="name-title-lable" for="workorder-account_id">
                  <i class="red-i">*</i> 产品类目：
            </label>
            <span class="c_red"></span>
            <div class="addinput-list region-limit">
                  <input type="hidden" id="class_id" name="class_id" value="">
                  <input type="text" class="form-control" id="category1" placeholder="请选择产品类目" readonly>
                  <i class="icon iconfont icon-shouqi1"></i>
                  <p class="misplacement"  id="work_type_msg">请选择产品类目</p>
                  <div class="drop-dw-layerbox">
                        <ul>
                              <?php if (!empty($data['classList'])) : ?>
                                    <?php foreach ($data['classList'] as $key => $val) : ?>
                                          <li>
                                            <span>
                                                <?php if ($val['exist'] == 1) : ?>
                                                      <i class="icon2-shixinyou"  onclick="clickCategory(this) "></i>
                                                      <input type="radio" value="<?=$val['id'] ?>" name="class_id" id="<?=$val['id'] ?>" class="pr1" onclick="changeCategory(this)">
                                                <?php else:?>
                                                      <input type="radio" value="<?=$val['id'] ?>" name="class_id" id="<?=$val['id'] ?>" class="pr1" onclick="changeCategory(this)">
                                                <?php endif;?>
                                                  <label for="<?=$val['id'] ?>"><?=$val['class_name'] ?></label>
                                            </span>
                                          </li>
                                    <?php endforeach; ?>
                              <?php endif; ?>
                        </ul>
                  </div>
            </div>
      </div>
      <div id="switch" style="display: none">
            <div class="col-md-8 information-list">
                  <label class="name-title-lable" > 商品型号：</label>
                  <div class="addinput-list">
                        <input type="hidden" id="prod_series" value="<?= isset($account->prod_series) ? $account->prod_series :'' ?>" name="prod_series"    placeholder="请输入商品型号"  maxlength="30" class="form-control inputxt limit no-border"/>
                        <p class="height34" id="prod_series_show"><?= isset($account->prod_series) ? $account->prod_series :'' ?></p>
                  </div>
            </div>
            <div class="col-md-8 information-list">
                  <label class="name-title-lable" > 商品序列号：</label>
                  <div class="addinput-list">
                        <input type="text" id="serial_number" value="<?= isset($account->serial_number) ? $account->serial_number :'' ?>" name="serial_number"   placeholder="请输入商品序列号"   maxlength="30" class="form-control inputxt limit" />
                  </div>
            </div>
            <div class="col-md-8 information-list">
                  <label class="name-title-lable" > 出厂日期：</label>
                  <div class="addinput-list">
                        <input type="text" id="datetimepicker-pay-top" class="form-control" name="produce_time" value="<?php echo isset($_GET['produce_time'])?$_GET['produce_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})"  placeholder="请选择出厂日期">
                        <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top',dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})"><i class="icon2-rili"></i></span>
                  </div>
            </div>
            <div class="col-md-8 information-list">
                  <label class="name-title-lable" > 购买日期：</label>
                  <div class="addinput-list">

                        <input type="text" id="datetimepicker-pay-end" class="form-control" name="buy_time" value="<?php echo isset($_GET['buy_time'])?$_GET['buy_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})"   placeholder="请选择购买日期">
                        <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-end',dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})"><i class="icon2-rili"></i></span>
                  </div>
            </div>
            <!--质保期-->
            <div class="col-md-8 information-list">
                  <label class="name-title-lable">
                        质保期：
                  </label>
                  <div class="col-md-8 addinput-list">
                        <div  class="three-linkage" style="width: 100%;">
                              <div class="linkage-justify-list" style="width: 48%;">
                                    <input type="text" value="" name="warranty_num" id="warranty_num" placeholder="请输入整数" class="form-control inputxt" onkeyup="this.value=this.value.replace(/\D/g,'')" onblur="this.value=this.value.replace(/\D/g,'')" />
                              </div>
                              <div class="linkage-justify-list" style="width: 48%;">
                                    <select id="warranty_type" name="warranty_type" class="col-md-4 col-sm-4 col-xs-4 form-control">
                                          <option value="0" >选择单位</option>
                                          <?php if (!empty($warranty)) : ?>
                                                <?php foreach ($warranty as $key => $val) : ?>
                                                      <option value="<?=$key; ?>"><?=$val; ?></option>
                                                <?php endforeach; ?>
                                          <?php endif; ?>
                                    </select>
                              </div>
                              <div class="justify_fix"></div>
                        </div>
                        <span class="error-span"></span>
                        <div class="Validform_checktip Validform_wrong"></div>
                  </div>
            </div>
      </div>
      <!--  <div class="col-md-8 information-list">
          <label class="name-title-lable" > 质保期：</label>
           <div class="addinput-list">
               <input type="text" id="warranty" value="" name="warranty" sucmsg=" "   placeholder="请输入质保日期数字，单位为天" maxlength="50" class="form-control inputxt" onkeyup="this.value=this.value.replace(/\D/g,'')" />
           </div>
       </div>-->
      <div class="col-md-8 information-list">
            <label class="name-title-lable" > 备注：</label>
            <div class="addinput-list">
                  <input type="text" id="info" value="<?= isset($account->info) ? $account-> info:'' ?>" name="info"   maxlength="50" class="form-control inputxt"   placeholder="请填写备注" />
            </div>
      </div>
      <div class="information-list" style="padding-top: 15px">
            <hr>
      </div>
      
      <div class="col-md-8 information-list" style="text-align: center;padding-bottom: 50px">
            <input type="button" class="btn bg-f7 mgr20" style="margin-right: 15px;" id="closeIframe" value="取消">
            <input type="submit" value="提交"  class="btn btn-orange" id="submit">
      </div>
</form>


<script src="/js/layer/layer.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="/js/jquery-migrate-1.1.0.js"></script>
<script src="/js/jquery.autocomplete.js" type="text/javascript"></script>
<script src="/js/myfunc.js"></script>
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=bbcdabda1427179d77bb95fac7b2b902&plugin=AMap.Geocoder"></script>
<script>
    var intention_type = 1;
    //addJudge=1 代表意向产品，reload=1 代表刷新
    $(function() {
        $('body').show();
        if(parent.addJudge==2){
            intention_type = 1;
            $('#customereProduct').addClass('bottom-orangexian');
            $('#switch').show();
        }else if(parent.addJudge==1){
            intention_type = 2;
            $('#intentionProduct').addClass('bottom-orangexian');  
        }else if(parent.addJudge==3){
           $('#switch').show();
           $('#customereProduct').addClass('bottom-orangexian');
           $('#customereProduct').hide();
           $('#intentionProduct').hide();
        }
        parent.callbackJudge = 0;

        
    })
    $('.pop-title100').find('span').on('click',function() {
        $('#brand_id').val('');
        $('.class_id').val('');
        $('#prod_name_show').val('');
        $('#category1').val('');
        $('.pop-title100').find('span').removeClass('bottom-orangexian');
        $(this).addClass('bottom-orangexian');
        $(this).attr('id')!="intentionProduct"?$('#switch').show():$('#switch').hide();
        $('.ac_results').remove();
    })

      var index = parent.layer.getFrameIndex(window.name);
      // 限制格式
      $('.limit').keyup(function () {
            var str = $(this).val();
            // var limitFormat =str.replace(/[^\a-\z\A-\Z0-9]/g,'');
            // $(this).val(limitFormat)
            if (str.length>30) {
                  $(this).val(limitFormat.substring(0,30));
                  layer.alert('请正确输入');
            };
      })
      
    //表单提交
    $('#submit').on('click',function(e) {
        e.preventDefault();
        var source = "<?php echo $source;?>";
        if($('#prod_name_show').val()!=""&& $('#class_id').val()!="" && $('#brand_id').val()!=""){
            var data = {
                source:source,
                account_id:$('#account_id').val(),
                prod_id:$('#prod_id').val(),
                class_id:$('#class_id').val(),
                brand_id:$('#brand_id').val(),
                info:$('#info').val(),
                intention_type:intention_type
            };
            

            var url = '/sale-order/add';
            if($('.bottom-orangexian').attr('id')=="customereProduct"){
                data.prod_series=$('#prod_series').val();
                data.serial_number=$('#serial_number').val();
                data.produce_time=$('#datetimepicker-pay-top').val();
                data.buy_time=$('#datetimepicker-pay-end').val();
                data.warranty_num=$('#warranty_num').val();
                data.warranty_type = $('#warranty_type').val();
                url = '/sale-order/add';
            }
            else if($('.bottom-orangexian').attr('id')=="intentionProduct"){
                url = '/intention-product/add';
            }

            var prodId = $("#prod_id").val();
            if(!prodId){
                $("#prod_msg").text("未找到该产品");
                $("#prod_msg").show();
                return false;
            }
            $.ajax({
                url:url,
                type:'POST',
                data:data,
                dataType:'json',
                success:function(data) {
                      console.log(data.data.type);
                    if (data.code == 200) {
                        if(parent.iframeCallbackSaleOrderId != 'undefined'){
                            parent.iframeCallbackSaleOrderId = data.data.id;
                        }
                        parent.showAlert(data.message);
                        parent.layer.close(index);
                        parent.callbackJudge = data.data.type;
                        parent.reload==1?window.parent.location.reload():'';
                    }else{
                        alert(data.message);
                    }
                    
                }
            })
        }else{
            if($('#prod_name_show').val()==""){
                $('#prod_msg').text('产品名称不能为空');
                $('#prod_msg').show();
                return false;
            }
            if($('#brand_id').val()==""){
                $('#brand_msg').text('品牌不能为空');$('#brand_msg').show();
                $('#work_type_msg').show();
                return false;
            }
            if($('#class_id').val()==""){
                $('#work_type_msg').text('请选择产品类目');
                $('#work_type_msg').show();
                return false;
            }
        }
    })


      //关闭iframe
      $('#closeIframe').click(function(){
            parent.layer.close(index);
      });

      $(".type_id").click(function(){
            var typeId = $(this).val();
            var value  = $("#type_value_"+typeId).text();
            $("#type_name").val(value);
      })

      $(function () {
            //用户联想
            $("#account_name").autocomplete("/sale-order/get-account", {
                  minChars: 1,
                  matchCase:false,//不区分大小写
                  autoFill: false,
                  max: 10,
                  dataType: 'json',
                  //width:'',
                  extraParams:{v:function() { return $('#clubmember-compay').val();}},
                  parse: function(data) {
                        return $.map(eval(data), function(row) {
                              $("#account_id").val(row.id);
                              return {
                                    data: row,
                                    value: row.id,    //此处无需把全部列列出来，只是两个关键列
                                    result: row.account_name
                              }
                        });
                  },
                  formatItem: function(row, i, max,term) {
                        var v = $("#clubmember-compay").val();
                        return  row.account_name;
                        if(row.name.indexOf(v) == 0)
                        {
                              return  row.account_name;
                        }
                        else
                              return false;
                  },
                  formatMatch: function(row, i, max) {
                        return row.account_name;
                  },
                  formatResult: function(row) {
                        return row.account_name;
                  },
                  reasultSearch:function(row,v)//本场数据自定义查询语法 注意这是我自己新加的事件
                  {
                        //自定义在code或spell中匹配
                        if(row.data.account_name.indexOf(v) == 0)
                        {
                              return row;
                        }
                        else
                              return false;
                  }
            });
            /*autocomplete("#prod_name","/sale-order/get-product",function (data) {
             $("#sale_id").val(data.id);
             $("#prod_name").focus();
             });*/
      });

      //选择完品牌，一级分类，二级分类后，获取相关的产品  2018-5-14
      //搜索产品信息开始
      //产品id带出产品信息


      $("#prod_name_show").keyup(function () {
            if($(this).val() == ''){
                  setTimeout(function () {
                        $("#prod_name_show").val(' ');
                        $("#prod_name_show").keydown();
                  },100);
            }
            if($.trim($(this).val()) != ''){
                  $('#prodNameSpan').hide();
            }
            else {
                  $('#prodNameSpan').html('输入产品编码或名称').show();

            }
      });
      $('#prodNameSpan').click(function () {
            $("#prod_name_show").focus();
      });

      setInterval(function () {
          if($('.ac_results').length >1){
              var len = $('.ac_results').length;
              $('.ac_results').each(function (i) {
                  if(i < len-1){
                      $(this).remove();
                  }
              });
          }
      },100)

      var productFlag = true;
      var search_data = '';
      $("#prod_name_show").focus(function(){
          console.log(window.productFlag);
            if(window.productFlag == false){
                return false;
            }
            window.productFlag = false;

            var type = $(".bottom-orangexian").attr("data-type");
            var brand_id = $('#brand_id').val();
            var class_id = $('#class_id').val();
                  $("#prod_msg").hide();
                  search_data = {
                        brand_id:brand_id,
                        class_id:class_id,
                        type:type
                  }
                  if($.trim($("#prod_name_show").val()) == '')
                  {
                        setTimeout(function () {
                              $('#prodNameSpan').html('请选择产品名称').show();
                              $("#prod_name_show").val(' ');
                              $("#prod_name_show").keydown();
                        },100);
                  }

          $('.ac_results').remove();
          $("#prod_name_show").autocomplete("/product/index", {
                        minChars: 1,
                        matchCase:false,//不区分大小写
                        autoFill: false,
                        max: 10,
                        dataType: 'json',
                        width:$('#prod_name_show').outerWidth(true)+'px',
                        //extraParams:{v1:function() { return $('#prod_name').val();},},
                        extraParams:search_data,
                        parse: function(data) {
                              return $.map(eval(data), function(row) {
                                    if(row.id == 0){
                                          $('#prod_name').val('');
                                          $('#prod_id').val('');
                                          $('#code').val('');
                                          $('#prod_series').val('');
                                          $('#prod_series_show').text('');
                                          $("#prod_msg").text("未找到该产品");
                                          $("#prod_msg").show();
                                    }else {
                                          return {
                                                data: row,
                                                value: row.id,    //此处无需把全部列列出来，只是两个关键列
                                                result: (row.prod_no == ''?'':(row.prod_no+'/'))+row.prod_name
                                          }
                                    }
                              });
                        },
                        formatItem: function(row, i, max,term) {
                              return  (row.prod_no == ''?'':(row.prod_no+'/'))+row.prod_name;
                        },
                        formatMatch: function(row, i, max) {
                              return row.prod_name;
                        },
                        formatResult: function(row) {
                              return row.prod_name;
                        }
                  }).result(function(event, data, formatted) {    //回调填充产品信息显示
                        if(data.id>0){
                              $('#brand_msg').hide();
                              $('#work_type_msg').hide();
                              $("#prod_msg").hide();
                              $("#prod_name").val(data.prod_name);
                              $("#prod_series").val(data.model);
                              $('#prod_series_show').text(data.model);
                              $('#prod_id').val(data.id);
                              $('#code').val(data.prod_no);
                              $('#prodNameSpan').hide();
                              $('#warranty_num').val(data.warranty_num);
                              $('#warranty_type').val(data.warranty_type);
                              if(brand_id == '' || class_id == ''){
                                    //根据产品获取品牌和类目
                                    $.ajax({
                                          url:'/product/get-detail',
                                          type:'GET',
                                          data:{id:data.id},
                                          datatype:'json',
                                          success:function(msg){
                                                var dataObj=eval("("+msg+")");//转换为json对象
                                               $('#brand_id').val(dataObj.data.brand_id);
                                               $('#class_id').val(dataObj.data.class_id);
                                                $('#category1').val(dataObj.data.class_name);
                                          }
                                    })
                              }
                        }
                        else {
                              $('#prod_name').val('');
                              $('#prod_name_show').val('');
                              $('#prod_series').val('');
                              $('#prod_series_show').text('');
                              $('#prod_id').val('0');
                              $('#code').val('');
                              $('#warranty_num').val('');
                              $('#warranty_type').val('');
                        }
                  }).blur(function () {
                          window.timeoutvar = setTimeout(function () {
                              $('.ac_results').remove();
                              window.productFlag = true;
                          },300);
                  });
            //#
            //搜索产品信息结束
            var productId = $("#sale_id").val();
            if(productId){
                  $.ajax({
                        url:'/sale-order/get-product',
                        type:'POST',
                        data:{productId:productId},
                        datatype:'json',
                        success:function(msg){
                              if(msg){
                                    var dataObj=eval("("+msg+")");//转换为json对象
                                    $("#prod_id").val(dataObj.prod_id);
                                    var prod_name = dataObj.prod_name+'/'+dataObj.prod_series+'/'+dataObj.serial_number;
                                    $("#prod_name").val(prod_name);
                                    $("#type_name").val(dataObj.type_name);
                                    $("#brand_id").val(dataObj.brand_id);
                                    $("#brand_name").val(dataObj.brand_name);
                                    $("#class_id").val(dataObj.class_id);
                                    $("#class_name").val(dataObj.class_name);
                                    $("#prod_series").val(dataObj.prod_series);
                                    $("#serial_number").val(dataObj.serial_number);
                                    var typeId = dataObj.type_id;
                                    $(".type_id").each(function(){
                                          var id = $(this).val();
                                          if(parseInt(id) == parseInt(typeId)){
                                                $(this).attr("selected",true);
                                          }
                                    })

                              }
                        }
                  })

            }
      })
      var intervalObj = null;
      $("#brand_id").change(function() {
            $("#prod_name").val('');
            $("#prod_name_show").val('');
            $("#prod_series").val('');
            $("#prod_series_show").text('');
            if($(this).val() == -1){
                  $(this).val('');
                  parent.location.href="/brand/index";
                  intervalObj = setInterval(function(){
                        var saleOrderId = $.trim($('#account_service_brand').val());
                        var addressId = $('#account_address').val();
                        var workType  = $('#service_type').val();
                        service_provider(accountId,saleOrderId,addressId,workType);
                  },10000);
            }
            else {
                  if(intervalObj!=null){
                        clearInterval(intervalObj);
                  }
            }
      });
      var intervalClassObj = null;
      function  clickCategory(el) {
            var id = $(el).next('input').val();
            var _this = $(el).parent('span')
            $.getJSON('/common/ajax-get-class',{'pid':id},function (json) {
                  var ulData = '';
                  if (_this.next('ul').size()==0) {
                        ulData +="<ul>";

                        var data = json.data;
                        delete data.flag;
                        for (item in data)
                        {
                              ulData +="<li><span>";
                              data[item].exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="clickCategory(this)"></i>':'';
                              ulData+='<input type="radio" value="'+data[item].id+'" name="class_id" id="'+data[item].id+'" class="pr1" onclick="changeCategory(this)"><label for="'+data[item].id+'">'+data[item].class_name+'</label></span></li>';
                        }

                        ulData +="</ul>";
                        _this.parent('li').append(ulData);
                        $(el).removeClass('icon2-shixinyou');
                        $(el).addClass('icon2-shixinxia');

                  }else{
                        if(
                              _this.next('ul').is(':hidden')){
                              $(el).removeClass('icon2-shixinyou');
                              $(el).addClass('icon2-shixinxia');
                              _this.next('ul').show();

                        }else{
                              $(el).addClass('icon2-shixinyou');
                              $(el).removeClass('icon2-shixinxia');
                              _this.next('ul').hide();
                        }
                  }
                  return false;
            })

      }
      function changeCategory(el) {
            $("#prod_name_show").val('');
            $("#prod_name").val('');
            $("#prod_id").val('');
            $('.drop-dw-layerbox').hide();
            $('#class_id').val($(el).next('label').attr("for"));
            $('#category1').val($(el).next('label').text());
      }
      $('#category1').on('click',function() {
            $('.drop-dw-layerbox').is(':hidden')?$('.drop-dw-layerbox').show():$('.drop-dw-layerbox').hide();

      })
      $(document).click(function(event) {
       var region = $('.region-limit');
       if (!region.is(event.target)&&region.has(event.target).length ===0) {
                $('.drop-dw-layerbox').hide();
       }
    });
 
</script>
</body>
</html>
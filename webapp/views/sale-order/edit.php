<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
$this->title = '添加售后商品';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>">
<!-- 日期插件-->

<script src="/js/My97DatePicker/WdatePicker.js"></script>
<section id="main-content">
    <div class="panel-body">
        <form action="/sale-order/edit" method="post" id="sale-order-edit">
            <input type="hidden" value="<?=$info->id?>" id="sale-id" name="id" />
            <input type="hidden" id="account_id" value="<?=$info->account_id?>" name="account_id">

                 <div class="col-md-8 information-list">
                   <label class="name-title-lable" ><span class="red-i">*</span> 客户：</label>
                   <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                        <input type="text" id="account_name" value="<?=$info->account_name?>" readonly datatype="*" nullmsg="请输入客户姓名"  max="20" class="form-control inputxt" />
                    </div>
                    <div class="col-xs-5 fuzzy-query">

                        <ul style="display: none;width: 545px;min-height: 30px;border: 1px solid #000" id="account_select"></ul>
                    </div>
                </div>
                 <div class="col-md-8 information-list">
                   <label class="name-title-lable" ><span class="red-i">*</span> 产品名称：</label>
                   <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                        <input type="text"  readonly  id="prod_name" value="<?=$info->prod_name?>" datatype="*" nullmsg="请输入产品名称"  max="50" class="form-control inputxt" />
                    </div>
                    <div class="col-xs-5 fuzzy-query">

                        <ul style="display: none;width: 545px;min-height: 30px;border: 1px solid #000" id="product_select"></ul>
                    </div>
                </div>
                 <div class="col-md-8 information-list" style="display: none;">
                    <label class="name-title-lable" ><span class="red-i">*</span> 服务类型：</label>
                    <div class="col-md-8 col-sm-8 col-xs-8  addinput-list" id="showType">
                        <?php foreach($typeList as $k=>$v){
                            if($k==$info->type_id){
                                $checkStatus =  'checked';
                            }else{
                                $checkStatus = '';
                            }

                            ?>
                        <div  class="radio-list">
                            <input type="radio" class="type_id" value="<?=$k?>" name="type_id" id="type_id_<?=$k?>" class="pr1" <?=$checkStatus?>/><label for="type_id_<?=$k?>" id="type_value_<?=$k?>"><?=$v?></label>
                        </div>

                        <?php } ?>
                        <input type="hidden" value="" name="type_name" id="type_name"  max="50" class="form-control inputxt"/>
                    </div>

                </div>

                 <div class="col-md-8 information-list">
                    <label class="name-title-lable" ><span class="red-i">*</span> 品牌：</label>
                     <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                        <select name="brand_id" id="brand_id"  class="form-control" datatype="*"  nullmsg="请选择品牌">
                            <?php foreach($brandList as $k=>$v){

                                if($v['brand_id']==$info->brand_id){
                                   $status = 'selected="selected"';
                                }else{
                                    $status='';
                                }

                                echo '<option value="'.$v['brand_id'].'"  '.$status.'>'.$v['brand_name'].'</option>';
                            }?>
                        </select>
                        <input type="hidden" id="brand_name" name="brand_name"/>
                        </div>

                    </div>
                </div>

            <div class="col-md-8 information-list" id="class_div">
                <label class="name-title-lable" ><span class="red-i">*</span> 分类：</label>

                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list" id="show_class">
                    <div class="three-linkage">
                        <div class="linkage-justify-list">
                            <select  id="first_class_id" class="form-control" datatype="*"  nullmsg="请选择一级类目">
                        <?php foreach($classList['firstClass'] as $k=>$v){
                            if($k==$classInfo->pid){
                                $status = 'selected="selected"';
                            }else{
                                $status='';
                            }
                            echo '<option value="'.$k.'" '.$status.'>'.$v.'</option>';
                        }?>
                    </select>
                        </div>
                        <div class="linkage-justify-list">
                            <select name="class_id" id="class_id" class="form-control" datatype="*"  nullmsg="请选择二级类目">
                           <?php foreach($classList['secondClass'] as $k=>$v){
                               if($k==$classInfo->id){
                                   $status = 'selected="selected"';
                               }else{
                                   $status='';
                               }
                            echo '<option value="'.$k.'"  '.$status.'>'.$v.'</option>';
                        }?>
                    </select>
                        </div>
                    <input type="hidden"  name="class_name" id="class_name"/>
                    </div>
                </div>
            </div>

                 <div class="col-md-8 information-list">
                   <label class="name-title-lable" > 商品型号：</label>
                   <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                        <input type="text" id="prod_series" value="<?= isset($info->prod_series) ? $info->prod_series :'' ?>" name="prod_series"   max="50" class="form-control inputxt" />
                    </div>
                </div>
                 <div class="col-md-8 information-list">
                   <label class="name-title-lable" > 商品序列号：</label>
                   <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                        <input type="text" id="serial_number" value="<?= isset($info->serial_number) ? $info->serial_number :'' ?>" name="serial_number"   max="50" class="form-control inputxt" />
                    </div>
                </div>
                 <div class="col-md-8 information-list">
                    <label class="name-title-lable" > 出厂日期：</label>
                    <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                            <input type="text" id="datetimepicker-pay-top" class="form-control" name="produce_time" value="<?php echo isset($_GET['produce_time'])?$_GET['produce_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})">
                            <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})"><i class="icons-calendar"></i></span>
                    </div>
                </div>
                 <div class="col-md-8 information-list">
                   <label class="name-title-lable" > 购买日期：</label>
                   <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">

                           <input type="text" id="datetimepicker-pay-end" class="form-control" name="buy_time" value="<?php echo isset($_GET['buy_time'])?$_GET['buy_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})">
                           <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})"><i class="icons-calendar"></i></span>
                    </div>
                </div>
                 <div class="col-md-8 information-list">
                   <label class="name-title-lable" > 质保期：</label>
                    <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                        <input type="text" value="<?= isset($info->warranty) ? $info->warranty :'' ?>" name="warranty" datatype="n1-2" errormsg="请输入正确的质保日期数字，单位为天！"   max="50" class="form-control inputxt" />
                    </div>
                </div>
                 <div class="col-md-8 information-list">
                    <label class="name-title-lable" > 备注：</label>
                    <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                        <input type="text" value="<?= isset($info->info) ? $info-> info:'' ?>" name="info" max="50" class="form-control inputxt" />
                    </div>
                </div>
            <div class="col-md-8 information-list">
                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                    <div class="col-md-2 col-xs-4" style="padding-left: 0">
                         <input type="button" class="btn ignore jn-btn" id="closeIframe" value="取消">
                    </div>
                   <div class="col-md-2  col-xs-4" style="padding-left: 0">
                       <input type="submit" value="提交" class="btn btn-orange jn-btn">
                   </div>
                    
                </div>
            </div>
        </form>
    </div>
</section>

<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=bbcdabda1427179d77bb95fac7b2b902&plugin=AMap.Geocoder"></script>
<script>

    var index = parent.layer.getFrameIndex(window.name);




    //表单验证
    $("#sale-order-edit").Validform({
        ajaxPost:true,
        tiptype:3,
        callback:function(data){
            if (data.code == 200) {
                parent.alert(data.message);
                parent.layer.close(index);
            }else{
                alert(data.message);
            }
        }

    });



    //关闭iframe
    $('#closeIframe').click(function(){
        parent.layer.close(index);
    });




  $(".type_id").click(function(){
      var typeId = $(this).val();
      var value  = $("#type_value_"+typeId).text();
      $("#prod_type").val(value);
  })



    //品牌带出分类
    $("#brand_id").change(function(){
        var brandId = $(this).val();
        var brandName = $(this).find("option:checked").text();
        $("#brand_name").val(brandName);
        $.ajax({
            url:'/sale-order/get-class',
            type:'POST',
            data:{brandId:brandId},
            datatype:'json',
            success:function(msg){
                var dataObj=eval("("+msg+")");//转换为json对象
                str = '';
                $.each(dataObj,function(idx,item){
                    str += '<option value="'+item.id+'">'+item.title+'</option>';
                })
                $("#first_class_id").html(str);
            }
        })
    })



    //二级分类带出分类
    $("#first_class_id").change(function(){
        var classId = $(this).val();
        $.ajax({
            url:'/sale-order/get-second-class',
            type:'POST',
            data:{classId:classId},
            datatype:'json',
            success:function(msg){
                var dataObj=eval("("+msg+")");//转换为json对象
                //console.log(dataObj);
                str = '';
                $.each(dataObj,function(idx,item){
                    console.log(item);
                    str += '<option value="'+idx+'">'+item+'</option>';
                })
                $("#class_id").html(str);
            }
        })
    })

    $("#class_id").change(function(){
        var className = $(this).find("option:checked").text();
        $("#class_name").val(className);
    })


</script>
<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = '售后产品管理';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
   /* .search-box-kuang>.col-sm-6{
        padding-left: 0;
        position: relative;
    }
    .icons-calendar{
        position: absolute;
        right: 24px;top: 9px;
    }
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }*/
    .search-box-kuang>.col-sm-6{
    padding-left: 0;
    position: relative;
    }
    .icons-calendar{
        position: absolute;
        right: 24px;top: 9px;
    }
    .popover{max-width: 500px !important;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">售后产品管理</span>
    <span class="btn btn-success jn-btn" style=" color:#FFF;float:right;width:auto " id="addParentIframe"  data-toggle="modal" data-target="#scrollingModal">
        <i class="fa fa-plus icon-tianjiajiahaowubiankuang icon iconfont"></i>&nbsp;添加</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/sale-order/index" class="form-horizontal form-border" id="form">
                        <div class="form-group jn-form-box">
                            <div class="form-group jn-form-box">
                                <!-- 搜索栏 -->
                                <div class="col-sm-12 no-padding-left">

                                    <div class="single-search">
                                        <label class=" search-box-lable">客户姓名</label>
                                        <div class="single-search-kuang1">
                                            <input type="text" class="form-control" name="account_name" value="<?php echo isset($_GET['account_name'])?$_GET['account_name']:'';?>">
                                        </div>
                                    </div>
                                    <div class="single-search">
                                        <label class=" search-box-lable">品牌</label>
                                        <div class="single-search-kuang1">
                                            <select name="brand_id" id="brand_id" class="form-control">
                                                <option>全部</option>
                                                <?php
                                                $brandId = yii::$app->request->get('brand_id','');
                                                foreach($brandList as $k=>$v){
                                                    if($brandId == $k){
                                                        $status = 'check="checked"';
                                                    }
                                                    echo '<option value="'.$k.'" $status>'.$v.'</option>';
                                                }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 search-box" >
                                        <label class="control-label search-box-lable">销售时间</label>
                                        <div class="search-box-kuang row">
                                            <div class="col-sm-6">
                                                <input type="text" id="datetimepicker-pay-top" class="form-control" name="plan_start_time" value="<?php echo isset($_GET['plan_start_time'])?$_GET['plan_start_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})">
                                                <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})"><i class="icons-calendar"></i></span>
                                                <span class="zhi"> 至</span>
                                            </div>
                                            <div  class="col-sm-6">
                                                <input type="text" id="datetimepicker-pay-end" class="form-control" name="plan_end_time" value="<?php echo isset($_GET['plan_end_time'])?$_GET['plan_end_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})" >
                                                <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})"><i class="icons-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="search-confirm"> 
                                        <button class="btn btn-success"><i class="icon iconfont icon-sousuo2"></i> 搜索</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead bgcolor="#455971">
                                <tr>
                                    <th>产品名称</th>
                                    <th>客户</th>
                                    <th>品牌</th>
                                    <th>商品型号</th>
                                    <th>商品序列号</th>
                                    <th>销售时间</th>
                                    <th>保修截止日期</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($data):?>

                                    <?php foreach ($data as $key=>$val):  ?>
                                        <tr>
                                            <td><?=$val['prod_name'] ? $val['prod_name'] : '空'; ?></td>
                                            <td><a href="/account/view?account_id=<?php echo $val['account_id'] ?>"><?=$val['account_name'] ? $val['account_name'] : ' 暂无'; ?></a></td>
                                            <td><?=$val['brand_name'] ? $val['brand_name'] : '  暂无'; ?></td>
                                            <td><?=$val['prod_series']?></td>
                                            <td><?=$val['serial_number']?></td>
                                            <td><?=date('Y-m-d H:i:s',$val['buy_time'])?></td>
                                            <td><?=$val['warranty']?></td>
                                           <td class="operation">
                                               <a class="clickEdit" href="javascript:void(0);" id-data="<?=$val['id']?>">编辑</a>
                                               <a class="clickDelete" href="javascript:void(0);" id-data="<?=$val['id']?>">删除</a>

                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php else:?>
                                    <tr>
                                    <tr><td colspan="9" class="no-record">暂无数据</td></tr>
                                    </tr>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo isset($pageHtml)?$pageHtml:'';?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
<script type="text/javascript">
    $("#addParentIframe").click(function () {
        layer.open({
            type: 2,
            title: '添加售后产品',
            area: ['700px', '600px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/sale-order/add'
        });
    });



    $(".clickDelete").click(function(){
        var id = $(this).attr("id-data");
        var status = $(this).attr("status-data");
        var html = $(this).html();
        layer.confirm('确认”<b>删除</b>“操作吗?', {icon: 3, title:'售后产品管理'}, function(index){
             $.ajax({
                 url:'/sale-order/change-status',
                 type:'POST',
                 data:{id:id,status:status},
                 success:function(msg){
                     var dataObj=eval("("+msg+")");
                     if(dataObj.success == true){
                         layer.alert('操作成功！', function(index){
                             window.location.reload();
                             layer.close(index);
                         });

                     }

                 }
             })
            layer.close(index);
        });
    })

    //更改商品
    $(".clickEdit").click(function(){
        var id = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['修改售后商品', 'font-size:18px;'],
            area: ['700px', '600px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/sale-order/edit?id='+id,
            //end:function(){
            //location.reload();
            // }
        });
        //return false;
    })

</script>
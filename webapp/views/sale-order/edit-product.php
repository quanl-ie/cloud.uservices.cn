<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$this->title = '编辑商品';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/webuploader/webuploader.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>?v=<?php echo time();?>">
<?=Html::jsFile('@web/webuploader/webuploader.js')?>
<?=Html::jsFile('@web/js/bootstrap-select.js')?>
<!-- 日期插件-->
<script src="/js/My97DatePicker/WdatePicker.js"></script>
<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
.input-group{margin-top: 10px;}
.form-horizontal .form-group{margin-left: 0;margin-right: 0;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">编辑商品</span>
</div>
<section id="main-content">
    <div class="panel-body">

    <?php
    $form = ActiveForm::begin([
        'options' => [
            'class' => 'form-horizontal',
            'id' => 'sale-order-edit',
            'name' => 'sale-order-edit',
            'enctype' => 'multipart/form-data',
        ],
        'fieldConfig' => [
            'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-lg-8 col-md-12 col-sm-12 col-xs-12 addinput-list\">{input}{error}</div>",
            'labelOptions' => ['class' => 'name-title-lable'],
        ]
    ]);
    ?>
    <input type="hidden" value="<?=$info->id?>" id="sale-id" name="id" />
    <input type="hidden" value="<?=$orderNo?>" id="order_no" name="order_no" />
    <div class="information-list">
        <label class="name-title-lable" > 产品名称：</label>
        <div class="col-lg-5 col-md-8 col-sm-8 col-xs-8 addinput-list">
            <p class="height34"><?=$info->prod_name?></p>
        </div>
    </div>
    <div class="information-list" style="margin-top: 0">
        <label class="name-title-lable" > 品牌：</label>
        <div class="col-lg-5 col-md-8 col-sm-8 col-xs-8 addinput-list">
            <p class="height34"><?=$info->brand_name?></p>
        </div>
    </div>
    <div class="information-list" style="margin-top: 0">
        <label class="name-title-lable" > 产品型号：</label>
        <div class="col-lg-5 col-md-8 col-sm-8 col-xs-8 addinput-list">
            <p class="height34"><?= isset($info->prod_series) ? $info->prod_series :'' ?></p>
        </div>
    </div>
    <div class="information-list" style="margin-top: 10px">
        <label class="name-title-lable" > 序列号：</label>
        <div class="col-lg-5 col-md-8 col-sm-8 col-xs-8 addinput-list">
            <input type="text" id="serial_number" value="<?= isset($info->serial_number) ? $info->serial_number :'' ?>" name="serial_number"   max="50" class="form-control inputxt" />
            <span class="red-i" style="font-size: 12px"><?php if($info->serial_number){ echo '原序列号【'.$info->serial_number .'】，请核对无误后提交。';}?></span>
        </div>
    </div>
    <div class="information-list">
        <label class="name-title-lable" > 出厂日期：</label>
        <div class="col-lg-5 col-md-8 col-sm-8 col-xs-8 addinput-list">
            <input type="text" id="datetimepicker-pay-top" class="form-control" name="produce_time" value="<?php if($info->produce_time){echo date('Y-m-d',$info->produce_time);};?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})"  placeholder="请选择出厂日期">
            <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top',dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})"><i class="icon2-rili"></i></span>
        </div>
    </div>
    <div class="information-list">
        <label class="name-title-lable" > 购买日期：</label>
        <div class="col-lg-5 col-md-8 col-sm-8 col-xs-8 addinput-list">

            <input type="text" id="datetimepicker-pay-end" class="form-control" name="buy_time" value="<?php if($info->buy_time){echo date('Y-m-d',$info->buy_time);};?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})"  placeholder="请选择购买日期">
            <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-end',dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})"><i class="icon2-rili"></i></span>
        </div>
    </div>
    <div class="information-list">
        <label class="name-title-lable">
            质保期：
        </label>
        <div class="col-lg-5 col-md-8 col-sm-8 col-xs-8 addinput-list">
            <div  class="three-linkage" style="width: 100%;">
                <div class="linkage-justify-list" style="width: 58%;">
                    <input type="text" readonly value="<?= isset($info->warranty_num) ? $info-> warranty_num:'' ?>" name="warranty_num" id="warranty_num" placeholder="请输入整数" class="form-control inputxt" onkeyup="this.value=this.value.replace(/\D/g,'')" onblur="this.value=this.value.replace(/\D/g,'')" />
                </div>
                <div class="linkage-justify-list" style="width: 38%;">
                    <select class="col-md-4 col-sm-4 col-xs-4 form-control" disabled="disabled" id="warranty_type" value="" name="warranty_type" >
                        <option value="">选择单位</option>
                        <?php if (!empty($warranty)) {?>
                            <?php foreach($warranty as $key=>$val){
                                if($key==$info->warranty_type){
                                    $status = 'selected="selected"';
                                }else{
                                    $status='';
                                }
                                echo '<option value="'.$key.'"  '.$status.'>'.$val.'</option>';
                            }?>
                        <?php }?>
                    </select>
                </div>
                <div class="justify_fix"></div>
            </div>
            <span class="error-span"></span>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>
    </div>
    <div class="information-list">
        <label class="name-title-lable" > 质保状态：</label>
        <div class="col-lg-5 col-md-8 col-sm-8 col-xs-8 addinput-list">
            <select class="form-control" id="is_scope" value="" name="is_scope">
                <!--<option value="">质保状态</option>-->
                <option value="1" <?= ($info->is_scope == 1)? 'selected="selected"': '' ?>>保内</option>
                <option value="2" <?= ($info->is_scope == 2)? 'selected="selected"': '' ?>>保外</option>
            </select>
        </div>
    </div>
    <?= $form->field($model, 'scope_img')->widget('manks\FileInput', [
            'clientOptions'=>[
                'server' => Url::to('../upload/upload?source=fws'),
                'pick'=>[
                    'multiple'=>true,
                ],
                'fileNumLimit'=>'5',
                'duplicate'=>true,
                'fileSingleSizeLimit'=>'2097152', //限制上传大小为2M
            ],
        ]
    ); ?>

    <div class="information-list">
        <label class="name-title-lable" > 备注：</label>
        <div class="col-lg-5 col-md-8 col-sm-8 col-xs-8 addinput-list">
            <input type="text" value="<?= isset($info->info) ? $info-> info:'' ?>" name="info" max="50" class="form-control inputxt" />
        </div>
    </div>
    <hr>
    <div class="information-list text-center">
        <a href="javascript:history.back(-1)"><button type="button" style="border: 1px solid #cccccc;color: #333" class="btn bg-f7">取消</button></a>

        <!--<input type="button" class="btn bg-f7" id="closeIframe" value="取消">-->
        <input type="submit" value="提交" class="btn btn-orange">
    </div>
    <?php ActiveForm::end(); ?>
</div>
</section>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    var index = parent.layer.getFrameIndex(window.name);
    //表单验证
    $("#sale-order-edit").Validform({
        ajaxPost:true,
        tiptype:2,
        /*beforeSubmit:function(curform){
            if(confirm("是否确认提交产品信息？")){
                return true;
            }
            else{
                parent.layer.close(index);
                return false;
            }

        },*/
        callback:function(data){
            //console.log(data);return false;
            if (data.code == 200) {
                parent.alert(data.message);
                location.href='javascript:history.back(-1)'
                //parent.layer.close(index);
                //parent.location.reload();
            }else{
                alert(data.message);
            }
        }
    });
    //关闭iframe
    $('#closeIframe').click(function(){
        parent.layer.close(index);
    });
</script>
</html>
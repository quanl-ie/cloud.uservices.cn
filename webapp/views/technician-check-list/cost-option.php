<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$this->title = '结算单详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    th{text-align: center;}
    .details-column-title{font-weight: 600;width: 120px}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">结算单详情 </span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/technician-check-list/cost" method="post" id="check">
                    <!-- 内容 -->
                    <div id="" class="tab-content col-md-12">
                        <div class="details-column">
                            <span class="details-column-title">技师姓名：</span>
                            <div class="details-column-content">
                                <?php echo $info['technician_name'];?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">技师电话：</span>
                            <div class="details-column-content">
                                <?php echo $info['mobile'];?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">结算时间段：</span>
                            <div class="details-column-content">
                                <?php echo date("Y-m-d",$info['begin_time']).'——'.date('Y-m-d',$info['end_time']);?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">结算费用：</span>
                            <div class="details-column-content">
                                <div class="dts-list-xq">
                                    <table>
                                        <thead>
                                            <tr style="background: #f8f8f8">
                                                <th>费用类型</th>
                                                <th>应结算金额</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($costItemList as $v) :?>
                                            <tr>
                                                <td><?=$v['cost_name']?></td>
                                                <td><?=sprintf('%0.2f',$v['amount'])?>元</td>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">工单明细：</span>
                            <div class="details-column-content">
                                <div class="dts-list-xq">
                                    <table>
                                        <thead>
                                        <tr style="background: #f8f8f8">
                                            <th>工单编号</th>
                                            <th>服务内容</th>
                                            <th>完成服务时间</th>
                                            <th>结算金额</th>
                                            <th style="min-width: 100px;">备注</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($workList as $v) :?>
                                            <tr>
                                                <td><?=$v['work_no']?></td>
                                                <td><?=$v['service_content']?></td>
                                                <td><?=$v['finish_time']?></td>
                                                <td><?=sprintf('%0.2f',$v['amount'])?></td>
                                                <td><?=$v['remark']?></td>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">提交人： </span>
                            <div class="details-column-content">
                                <?=$info['report_name']?>
                            </div>
                        </div>

                        <div class="details-column">
                            <span class="details-column-title">提交时间： </span>
                            <div class="details-column-content">
                                <?=date('Y-m-d H:i:s',$info['create_time'])?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">对账人： </span>
                            <div class="details-column-content">
                                <?=$info['check_name']?>
                            </div>
                        </div>

                        <div class="details-column">
                            <span class="details-column-title">对账时间： </span>
                            <div class="details-column-content">
                                <?=date('Y-m-d H:i:s',$info['check_time'])?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">应结算总额： </span>
                            <div class="details-column-content" id="total">
                                <?=sprintf('%0.2f',$info['amount'])?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">实际结算金额： </span>
                            <div class="details-column-content" id="amount_real_total">
                                <?=sprintf('%0.2f',$info['amount_real'])?>
                            </div>
                            <input name="amount_real" id="amount_real" type="hidden" value="<?=$info['amount_real']?$info['amount_real']:$info['amount']?>"/>
                        </div>

                        <!--带出数据结束 -->
                        <hr>
                        <div class="information-list">
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <a href="javascript:history.back(-1)" class="btn ignore jn-btn" style="border:1px solid #ccc;margin-right: 20px;width: 100px">取消</a>
                                <input id="checklist_id" name="checklist_id" type="hidden" value="<?=$info['id']?>"/>
                                <button class="btn btn-orange jn-btn id="submit" style="width: 100px">确认结算</button>
                            </div>
                        </div>
                        </form>


                </div>
            </div>
        </div>
    </div>
    <img src="" id="enlargeImg" style="display: none">
</section>
<script>

    $(".amount").blur(function(){
        //var amount_total = $("#amount_real_total").html();
        var amount = 0,amount_total=0;
        $.each($('.amount'),function(){
            var amount = $(this).val();
            if(amount){
                amount_total = amount_total+parseFloat(amount,2);
            }
        });
        $("#amount_real_total").html(amount_total.toFixed(2));
        $("#amount_real").val(amount_total);

    })

    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
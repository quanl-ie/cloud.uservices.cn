<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
$this->title = '申请结算';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style>
    .form-control[readonly]{cursor: pointer;}
    .icon-rili{position: absolute;right: 20px;top: 8px}
    .zhi{position: absolute;right: -5px;top: 8px}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">申请结算</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/technician-check-list/apply" method="post" id="technician_check_list_apply">
                        <div class="information-list">
                            <label class="name-title-lable" ><span class="red-i">*</span> 结算时间段：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="padding-left: 0">
                                    <input type="text" id="datetimepicker-pay-top" class="form-control" name="cost_start_time" value="<?php echo isset($_GET['plan_start_time'])?$_GET['plan_start_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})">
                                     <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})"><i class="icon2-rili"></i></span>
                                    <span class="zhi">至</span>
                                </div>
                               
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    <input type="text" id="datetimepicker-pay-end" class="form-control" name="cost_end_time" value="<?php echo isset($_GET['plan_end_time'])?$_GET['plan_end_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})" >
                                    <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})"><i class="icon2-rili"></i></span>
                                </div>
                                
                                <a class="getData btn btn-orange" value="查询" id="getData">查询</a>
                            </div>
                        </div>

                        <!--这里开始需要带出的数据-->


                        <!--这里开始需要带出的数据-->
                        <div class="information-list" id="show_div">



                        </div>
                            <!--带出数据结束 -->
                        <div class="information-list">   
                            <hr>
                        </div>
                        <div class="information-list">
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <a href="/cost/index" class="btn bg-f7 jn-btn" style="border:1px solid #ccc;margin-right: 20px;color: #444">取消</a>
                                <input id="checklist_ids" name="checklist_ids" type="hidden" value=""/>
                                <input id="set_data" value='0' type="hidden" />
                                <button class="btn btn-orange jn-btn" id="submit">提交</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $("#sj_id").change(function(){
        var company = $(this).find("option:selected").text();
        $("#company").val(company);
    })



    function p(s) {
        return s < 10 ? '0' + s: s;
    }


    $(".getData").click(function(){
        var start_time = $("#datetimepicker-pay-top").val();
        var end_time   = $("#datetimepicker-pay-end").val();

        var myDate = new Date();
        //获取当前年
        var year=myDate.getFullYear();
        //获取当前月
        var month=myDate.getMonth()+1;
        //获取当前日
        var date=myDate.getDate();
        var h=myDate.getHours();       //获取当前小时数(0-23)
        var m=myDate.getMinutes();     //获取当前分钟数(0-59)
        var s=myDate.getSeconds();

        var now=year+'-'+p(month)+"-"+p(date)+" "+p(h)+':'+p(m)+":"+p(s);

        if(!start_time){
            alert('请选择开始时间段');
            return false;
        }
        if(!end_time){
            alert('请选择结束时间段');
            return false;
        }
        if(start_time>now) {
            alert('开始时间不能大于当前时间');
            return false;
        }
        if(end_time>now) {
            alert('结束时间不能大于当前时间');
            return false;
        }
        if(start_time>end_time) {
            alert('开始时间不能大于结束时间');
            return false;
        }
        $.ajax({
            url:'/technician-check-list/apply/',
            type:'POST',
            data:{mark:true,startTime:start_time,endTime:end_time},
            success:function(msg){
                var dataObj=eval("("+msg+")");//转换为json对象
                var tech_str = '';
                $.each(dataObj,function(idx,item){
                    tech_str +=   '<tr> <td><input type="checkbox" value="'+item.checklist_id+'" id="p'+item.checklist_id+'" class="check_id"/><label class="brand-label" for="p'+item.checklist_id+'"><i class="gou-i"></i></label></td>'+
                                '<td>'+item.tech_name+'</td>'+
                                '<td>'+item.work_number+'</td>'+

                                '<td id="'+item.checklist_id+'">'+item.work_amount+'</td>'+
                                '<td><a href="/technician-check-list/detail?id='+item.checklist_id+'">查看结算明细<a/></td>'+
                                '</tr>';

                })
                str =   '<div id="get-data">'+
                    '<label class="name-title-lable" ><span class="red-i">*</span> 技师工单明细：</label>'+
                    '<div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">'+
                    '<div class="dts-list-xq" style="margin-bottom: 30px;">'+
                    '<table>'+
                    '<thead>'+
                    '<tr style="background: #f8f8f8"> <th width="50px"></th>'+
                    '<th>技师姓名</th>'+
                    '<th>工单数</th>'+
                    '<th>待结算金额</th>'+
                    '<th>操作</th>'+
                    '</tr>'+
                    '</thead>'+
                    '<tbody>'+
                    tech_str+
                    '<tr>'+
                    '<td colspan="5">   已选择<span id="technician_count">0</span>人，合计金额<span id="work_total_amount">0.00</span>元</td>'+
                    '</tr>'+
                    '</tbody>'+
                    '</table>'+

                    ' </div>'+
                    '</div>'+
                    ' </div>';
                $("#show_div").html(str);
                $(".check_id").click(function(){
                    var checklist_id = '';
                    var i = 0,amount = 0,money=0;
                    $.each($('.check_id:checked'),function(){
                        var id = $(this).val();
                        if(id){
                            checklist_id = checklist_id+id+',';
                            i++;
                            money = $("#"+id).html();
                            amount = amount+parseFloat(money);
                        }
                    });
                    $("#technician_count").html(i);
                    $("#work_total_amount").html(amount);
                    $("#checklist_ids").val(checklist_id);
                    $("#set_data").val('1');
                })
            }
        })
    })

    $("#technician_check_list_apply").submit(function(){
        var technician_count = $("#technician_count").html();
        var set_data = $("#set_data").val();
        //console.log(technician_count.length);return false;
        if(parseInt(technician_count)==0){
            alert('该结算单没有选择技师，请选择');
            return false;
        }
        if(parseInt(set_data)==0){
            alert('该结算单没有结算数据，请选择');
            return false;
        }

/*        layer.confirm("确定要将此信息通过审核吗？", {
                offset: ['160px', '350px'],
                btn: ['确定', '取消'],
                title: '提示',
                icon: 3
            },
            function () {
                return true;
            },
            this
        );*/

        // return true;



    })

</script>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$this->title = '结算单审核';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    th{text-align: center;}
    .details-column-title{font-weight: 600;width: 120px}
    td>input{margin: 0 5px;border-radius: 3px;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">结算单审核 </span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/technician-check-list/check" method="post" id="check">
                    <!-- 内容 -->
                    <div id="" class="tab-content col-md-12">
                        <div class="details-column">
                         <!--    <span class="details-column-title" style="width: 100%;text-align: left;">  </span>
                            <div class="details-column-content">

                            </div> -->
                            <h5 class="block-h5 block-h5one"><?php echo $info['technician_name'];?>技师的结算单</h5>
                        </div>
                        <div class="details-column" style="margin-top: 0;">
                            <span class="details-column-title">结算时间段：</span>
                            <div class="details-column-content">
                                <?php echo date("Y-m-d",$info['begin_time']).'——'.date('Y-m-d',$info['end_time']);?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">结算费用：</span>
                            <div class="details-column-content">
                                <div class="dts-list-xq">
                                    <table>
                                        <thead>
                                            <tr style="background: #f8f8f8">
                                                <th>费用类型</th>
                                                <th>应结算金额</th>      
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($costItemList as $v) :?>
                                            <tr>
                                                <td><?=$v['cost_name']?></td>
                                                <td><?=sprintf('%0.2f',$v['amount'])?>元</td>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="details-column" style="width: 800px;">
                            <span class="details-column-title">工单明细：</span>
                            <div class="details-column-content">
                                <div class="dts-list-xq">
                                    <table>
                                        <thead>
                                        <tr style="background: #f8f8f8">
                                            <th>工单编号</th>
                                            <th>服务内容</th>
                                            <th>完成服务时间</th>
                                            <th>结算金额</th>
                                            <th>备注</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($workList as $v) :?>
                                            <tr>
                                                <td><?=$v['work_no']?></td>
                                                <td><?=$v['service_content']?></td>
                                                <td><?=$v['finish_time']?></td>
                                                <td><input name="detail[<?=$v['id']?>][amount]" class="amount" value="<?=sprintf('%0.2f',$v['amount'])?>" size="10"  onkeyup="clearNoNum(this)" /></td>
                                                <td><input name="detail[<?=$v['id']?>][remark]" class="remark"/></td>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="details-column">
                            <span class="details-column-title">创建时间： </span>
                            <div class="details-column-content">
                                <?=date('Y-m-d H:i:s',$info['create_time'])?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">应结算总额： </span>
                            <div class="details-column-content" id="total">
                                <?=sprintf('%0.2f',$info['amount'])?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">实际结算金额： </span>
                            <div class="details-column-content" id="amount_real_total">
                                <?=sprintf('%0.2f',$info['amount'])?>
                            </div>
                            <input name="amount_real" id="amount_real" type="hidden" value="<?=$info['amount_real']?sprintf('%0.2f',$info['amount_real']):sprintf('%0.2f',$info['amount'])?>"/>
                        </div>

                        <!--带出数据结束 -->
                        <hr>
                        <div class="information-list">
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <a href="javascript:history.back(-1)" class="btn ignore jn-btn" style="border:1px solid #ccc;margin-right: 20px;width: 100px">取消</a>
                                <input id="checklist_id" name="checklist_id" type="hidden" value="<?=$info['id']?>" />
                                <button class="btn btn-orange jn-btn" id="submit"  style="width: 100px">确认审核</button>
                            </div>
                        </div>
                        </form>


                </div>
            </div>
        </div>
    </div>
    <img src="" id="enlargeImg" style="display: none">
</section>
<script>

    $(".amount").blur(function(){
        //var amount_total = $("#amount_real_total").html();
        var amount = 0,amount_total=0;
        var reg = /.*\..*/;
        reg.test( $(this).val())==false?$(this).val($(this).val()+'.00'):'';
        $.each($('.amount'),function(){
            var amount = $(this).val();
            if(amount){
                amount_total = amount_total+parseFloat(amount,2);
            }
        });
        //$(this).val(Number(amount).toFixed(2));
        $("#amount_real_total").html(amount_total.toFixed(2));
        $("#amount_real").val(amount_total);

    })

    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
     function clearNoNum(obj){  
        //修复第一个字符是小数点 的情况.  
        if(obj.value !=''&& obj.value.substr(0,1) == '.'){  
            obj.value="";  
        }  
        obj.value = obj.value.replace(/^0*(0\.|[1-9])/, '$1');//解决 粘贴不生效  
        obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符  
        obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个.清除多余的       
        obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");      
        obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数       
        if(obj.value.indexOf(".")< 0 && obj.value !=""){// 
            if(obj.value.substr(0,1) == '0' && obj.value.length == 2){
                obj.value= obj.value.substr(1,obj.value.length);

            }

        } 
    }


    $("#submit").click(function(){
        //alert(333);return false;
        //var amount = $(".amount");
        var amount = Number($(".amount").val());
        amount = amount.toFixed(2);
            if(amount<='0.00'){
                alert('请确保所有工单结算金额填写完成后再提交');
                return false;
            }
    })
</script>
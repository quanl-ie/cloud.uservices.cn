<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$this->title = '结算单详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    th{text-align: center;}
    .details-column-title{font-weight: 600;width: 120px}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">结算单详情 </span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <!-- 内容 -->
                    <div id="" class="tab-content col-md-12">
                        <div class="details-column">
                            <span class="details-column-title">技师姓名：</span>
                            <div class="details-column-content">
                                <?php echo $info['technician_name'];?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">技师电话：</span>
                            <div class="details-column-content">
                                <?php echo $info['mobile'];?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">结算时间段：</span>
                            <div class="details-column-content">
                                <?php echo date("Y-m-d",$info['begin_time']).'——'.date('Y-m-d',$info['end_time']);?>
                            </div>
                        </div>

                        <div class="details-column" style="width: 100%;">
                            <span class="details-column-title">结算费用：</span>
                            <div class="details-column-content">
                                <div class="dts-list-xq">
                                    <table style="width: auto;">
                                        <thead>
                                            <tr style="background: #f8f8f8">
                                                <th style="width: 20%">费用类型</th>
                                                <th style="width: 20%">收费金额</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($costItemList as $v) :?>
                                            <tr>
                                                <td><?=$v['cost_name']?></td>
                                                <td><?=sprintf('%0.2f',$v['amount'])?>元</td>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="details-column" style="width: 100%;">
                            <span class="details-column-title">工单明细：</span>
                            <div class="details-column-content">
                                <div class="dts-list-xq">
                                    <table>
                                        <thead>
                                        <tr style="background: #f8f8f8">
                                            <th style="width: 20%">工单编号</th>
                                            <th style="width: 20%">服务内容</th>
                                            <th>完成服务时间</th>
                                            <th>结算金额</th>
                                            <th>备注</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($workList as $v) :?>
                                            <tr>
                                                <td><?=$v['work_no']?></td>
                                                <td><?=$v['service_content']?></td>
                                                <td><?=$v['finish_time']?></td>
                                                <td><?=sprintf('%0.2f',$v['amount'])?></td>
                                                <td>无</td>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <?php if($info['status']>0) :?>
                        <div class="details-column">
                            <span class="details-column-title">提交人： </span>
                            <div class="details-column-content">
                                <?=$info['report_name']?>
                            </div>
                        </div>

                        <div class="details-column">
                            <span class="details-column-title">提交时间： </span>
                            <div class="details-column-content">
                                <?=date('Y-m-d H:i:s',$info['create_time'])?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">对账人： </span>
                            <div class="details-column-content">
                                <?=$info['check_name']?>
                            </div>
                        </div>

                        <div class="details-column">
                            <span class="details-column-title">对账时间： </span>
                            <div class="details-column-content">
                                <?=date('Y-m-d H:i:s',$info['check_time'])?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">结算人： </span>
                            <div class="details-column-content">
                                <?=$info['cost_name']?>
                            </div>
                        </div>

                        <div class="details-column">
                            <span class="details-column-title">结算时间： </span>
                            <div class="details-column-content">
                                <?=date('Y-m-d H:i:s',$info['cost_time'])?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">总计金额： </span>
                            <div class="details-column-content">
                                <?=sprintf('%0.2f',$info['amount'])?>
                            </div>
                        </div>
                        <div class="details-column">
                            <span class="details-column-title">预计结算金额： </span>
                            <div class="details-column-content">
                                <?=sprintf('%0.2f',$info['amount_real'])?>
                            </div>
                        </div>
                    <?php endif;?>


                    </div>

                </div>
            </div>
        </div>
    </div>
    <img src="" id="enlargeImg" style="display: none">
</section>
<script>

    // 图片放大
    $('.enlarge-img').on('click',function() {
        var aa = $(this).attr('src');
        var bb = aa.substring(0,aa.indexOf('?'));
        $('#enlargeImg').attr('src',bb);
        setTimeout(function() {
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: '516px',
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#enlargeImg')
            },300);
        })

    })
    //查看技能
    $(".viewSkill").click(function(){
        var id = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['技能信息', 'font-size:18px;'],
            area: ['700px', '400px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/technician/get-skill?id='+id,
        });
        return false;
    });
    //查看实时位置
    $(".viewLocation").click(function(){
        var id = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['技师位置', 'font-size:18px;'],
            area: ['800px', '500px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/technician/view-location?id='+id,
        });
        return false;
    });
    var index = parent.layer.getFrameIndex(window.name);
    //修改技师位置
    $(".EditArea").click(function(){
        var id = $(this).attr("id-data");
        $.ajax({
            url:'/technician/verify',
            data:{id:id},
            type:'POST',
            dataType:'json',
            success:function(data){
                if (data.success == false) {
                    layer.confirm(data.message, {
                        btn: ['立即设置'] //按钮
                    }, function(){
                        location.href='/technician/edit-area?id='+id;
                    });
                }else{
                    location.href='/technician/edit-area?id='+id;
                }
            }
        });
        return false;
    });
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = '排班管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/js/layer/theme/default/layer.css');?>?v=<?php echo time()?>">
<script src="<?php echo Url::to('/js/layer/layer.js');?>?v=<?php echo time()?>"></script>
<script src="<?php echo Url::to('/js/myfunc.js');?>?v=<?php echo time()?>"></script>
<style type="text/css">
    body{background: #fff;}
    .information-list{padding:0 20px;height: auto;}
    .name-title-lable{left: 20px;width: 85px;}
    .addinput-list{margin-left: 85px;}
    .btn{padding: 8px 20px;cursor: pointer;}
    .xianzhi{right: 5px;bottom: -10px;}
    .xian{margin:34px 0 26px 0;}
    input[type="radio"] + label::before{width: 13px;height: 13px;}
    input[type="radio"]:checked + label::before{width: 9px;height: 9px}
    .pb-technician-checkbox{width: auto;}
</style>
<section id="main-content">
        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
               <i class="red-i">*</i> 选择班次：
            </label>
            <span class="c_red"></span>
            <div class="addinput-list pb-tec-box" style="max-height: 80px">
                <?php if($data) foreach ($data as $key=>$val) {?>
                    <span class="pb-technician-checkbox">
                        <input type="radio" id="<?php echo $key?>"  name="shift">
                        <label class="brand-label" for="<?php echo $key?>"><i class="gou-i"></i><?php echo $val?></label>
                    </span>
                <?php }?>
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
                备注：
            </label>
            <span class="c_red"></span>
            <div class="addinput-list">
                <textarea class="ramark-box" id="remarks" maxlength="200" placeholder="请输入" style="min-height: 72px;" name="description"></textarea>
                <p class="xianzhi"><span id="word">0</span>/200</p>
            </div>
        </div>
        <p class="xian"></p>
        <div class="information-list center">
            <button class="btn bg-f7 mgr20 layerCancel">取消</button>
            <button class="btn btn-orange" id="confirm">确认</button>
        </div>
</section>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script>
    var layerWindow = parent.layer.getFrameIndex(window.name);
    var data = parent.editArray;
    $(function() {
        $('#'+data.shift).prop('checked',true);
        $('#remarks').val(data.note);
    })
    // 字数限制
    $("#remarks").keyup(function(){
        if($("#remarks").val().length > 200){
            $("#remarks").val( $("#remarks").val().substring(0,200) );
        }
        $("#word").text( $("#remarks").val().length ) ;
    });
    $('#confirm').on('click',function() {
        data.shift = $('input[name="shift"]:checked').attr('id');
        data.note = $('#remarks').val();
        console.log(data);
        $.ajax({
            url:'/schedule-set-option/set-tech-schedule',
            type:'POST',
            datatype:'json',
            data:data,
            success:function(data){
                //console.log(data);
                var res = $.parseJSON(data);
                //console.log(res);
                //if(res.code != 200){
                    alert(res.message);
                //}
                parent.location.reload();
                /*setInterval(function () {
                    parent.layer.close(layerWindow);
                },300);*/

            }
        })
    })
    //关闭弹层
    $('.layerCancel').click(function(){
        parent.layer.close(layerWindow);
    })
</script>
<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = '排班管理';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jedate.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time()?>">
<script src="/js/jedate.js"></script>
<style>
    .information-list{width: 600px;}
    .pb-technician-checkbox{position: relative;}
    input[type="checkbox"] + label::before{top: 6px}
    input[type="checkbox"]:checked + label::before{left: 0}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">批量排班</span>
    <div class="right-btn-box">
        <a href="javascript:" class="btn bg-f7 layerCancel">返回</a>
    </div>
</div>
<section id="main-content">
    <div class="panel-body padding0">
        <form action="/schedule-set-option/index" class="form-horizontal form-border" method="POST" id="schedule-set-option-form">

        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
               <i class="red-i">*</i> 选择分组：
            </label>
            <span class="c_red"></span>
            <div class="addinput-list">
                <select class="form-control onchangeSt" onchange="getTech(this)">
                        <option value="">全部</option>
                        <?php if (!empty($group)) : ?>
                        <?php foreach ($group as $key => $val) : ?>
                        <option value="<?=$key ?>"><?=$val ?></option>
                        <?php endforeach; ?>
                        <?php endif; ?>
                </select>
            </div>
        </div>
        <div class="information-list"  style="height: auto;margin-bottom: 40px">
            <label class="name-title-lable" for="workorder-account_id">
               <i class="red-i">*</i> 选择技师：
            </label>
            <span class="c_red"></span>
            <div class="addinput-list pb-tec-box" id="technician">
    
                <?php if (!empty($tech)) : ?>
                    <?php foreach ($tech as $key => $val) : ?>
                        <span class="pb-technician-checkbox">
                            <input type="checkbox" id="tech_id<?=$key ?>" name="tech_id[]" value="<?=$key ?>" class="tech_id onchangeSt" >
                            <label class="brand-label" for="tech_id<?=$key ?>" title="<?=$val ?>"><i class="gou-i"></i><?=$val ?></label>
                        </span>
                    <?php endforeach; ?>
                <?php endif; ?>
                <span class="justify_fix120"></span>
                <span class="justify_fix120"></span>
                <span class="justify_fix120"></span>
            </div>
            <a href="javascript:" class="see-more-a" <?php if(count($tech)<=6):?> style="display: none;"<?php endif;?>>查看更多</a>
            <p class="red-i addinput-list" style="font-size: 12px" id="technician_msg"></p>
        </div>
        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
               <i class="red-i">*</i> 选择班次：
            </label>
            <span class="c_red"></span>
            <div class="addinput-list">
                <select class="form-control onchangeSt" id="schedule_set_id" name="schedule_set_id">
                        <option value="">请选择</option>
                        <?php if (!empty($scheduleSet)) : ?>
                            <?php foreach ($scheduleSet as $key => $val) : ?>
                                <option value="<?=$key ?>"><?=$val ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                </select>
                <p class="red-i" style="font-size: 12px" id="schedule_set_id_msg"></p>
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
               <i class="red-i">*</i> 重复模式：
            </label>
            <span class="c_red"></span>
            <div class="addinput-list">
                <select class="form-control onchangeSt" id="repeate_type" name="repeate_type">
                    <option value="">请选择</option>
                    <?php if (!empty($repeateType)) : ?>
                        <?php foreach ($repeateType as $key => $val) : ?>
                            <option value="<?=$key ?>"><?=$val ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <p class="red-i" style="font-size: 12px" id="repeate_type_msg"></p>
            </div>
        </div>
        <div class="information-list  display-none" id="repeatDate" style="height: auto;min-height: 34px">
            <label class="name-title-lable" id="repeatingName">
               <i class="red-i">*</i> 重复日期：
            </label>
            <span class="c_red"></span>
            <!-- 每周重复日期 -->
            <div class="addinput-list pb-tec-box display-none" id="weekly">
                <span class="pb-technician-checkbox">
                    <input type="checkbox" id="week1" value="1" name="repeate_week[]">
                    <label class="brand-label" for="week1"><i class="gou-i"></i>周一</label>
                </span>
                <span class="pb-technician-checkbox">
                    <input type="checkbox" id="week2" value="2" name="repeate_week[]">
                    <label class="brand-label" for="week2"><i class="gou-i"></i>周二</label>
                </span>
                <span class="pb-technician-checkbox">
                    <input type="checkbox" id="week3" value="3" name="repeate_week[]">
                    <label class="brand-label" for="week3"><i class="gou-i"></i>周三</label>
                </span>
                <span class="pb-technician-checkbox">
                    <input type="checkbox" id="week4" value="4" name="repeate_week[]">
                    <label class="brand-label" for="week4"><i class="gou-i"></i>周四</label>
                </span>
                <span class="pb-technician-checkbox">
                    <input type="checkbox" id="week5" value="5" name="repeate_week[]">
                    <label class="brand-label" for="week5"><i class="gou-i"></i>周五</label>
                </span>
                <span class="pb-technician-checkbox">
                    <input type="checkbox" id="week6" value="6" name="repeate_week[]">
                    <label class="brand-label" for="week6"><i class="gou-i"></i>周六</label>
                </span>
                <span class="pb-technician-checkbox">
                    <input type="checkbox" id="week7" value="7" name="repeate_week[]">
                    <label class="brand-label" for="week7"><i class="gou-i"></i>周日</label>
                </span>
                <p class="red-i" style="font-size: 12px" id="weekly_msg"></p>
            </div>
            <!-- 自定义重复日期 -->
            <div class="addinput-list pb-tec-box display-none" id="customPattern">
                <input type="" name="repeate_date" class="form-control" readonly id="starDate">
                <i class="icon2-rili riqi1"></i>
                <p class="red-i" style="font-size: 12px" id="customPattern_msg"></p>
            </div>
            <!-- 每月重复日期 -->
            <div class="addinput-list month-repeat display-none" id="monthly">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day1" value="1" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day1">1</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day2" value="2" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day2">2</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day3" value="3" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day3">3</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day4" value="4" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day4">4</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day5" value="5" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day5">5</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day6" value="6" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day6">6</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day7" value="7" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day7">7</label>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day8" value="8" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day8">8</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day9" value="9" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day9">9</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day10" value="10" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day10">10</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day11" value="11" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day11">11</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day12" value="12" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day12">12</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day13" value="13" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day13">13</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day14" value="14" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day14">14</label>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day15" value="15" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day15">15</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day16" value="16" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day16">16</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day17" value="17" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day17">17</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day18" value="18" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day18">18</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day19" value="19" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day19">19</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day20" value="20" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day20">20</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day21" value="21" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day21">21</label>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day22" value="22" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day22">22</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day23" value="23" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day23">23</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day24" value="24" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day24">24</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day25" value="25" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day25">25</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day26" value="26" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day26">26</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day27" value="27" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day27">27</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day28" value="28" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day28">28</label>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day29" value="29" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day29">29</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day30" value="30" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day30">30</label>
                                </span>
                            </td>
                            <td>
                                <span class="month-repeat-day">
                                    <input type="checkbox" id="day31" value="31" name="repeate_date[]" onchange="selectDay(this)">
                                    <label class="brand-label" for="day31">31</label>
                                </span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <p class="red-i" style="font-size: 12px" id="monthly_msg"></p>
            </div>

        </div>
        <!-- 截至日期 -->
        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
               截止日期：
            </label>
            <span class="c_red"></span>
            <div class="addinput-list">
                <input type="text" name="end_date" class="form-control enYMD" readonly id="endDate">
                <i class="icon2-rili riqi1"></i>
            </div>
        </div>
        <div class="information-list" style="height: auto;">
            <label class="name-title-lable" for="workorder-account_id">
                备注：
            </label>
            <span class="c_red"></span>
            <div class="addinput-list">
                <textarea class="ramark-box" id="remarks" maxlength="50" placeholder="请输入" name="description" onkeyup="wordLength(this)"></textarea>
                <p class="xianzhi"><span id="word">0</span>/50</p>
            </div>
        </div>
        <p class="xian" style="margin-top: 20px"></p>
        <div class="information-list" style="text-align: center;">
            <a href="javascript:" class="btn bg-f7 layerCancel">取消</a>
            <input type="submit" class="btn btn-orange" value="保存">
        </div>
        </form>
    </div>
</section>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    var myDate = new Date();
    var myMonth = myDate.getMonth()<10?"0"+(myDate.getMonth()+1):myDate.getMonth();
    var myDay = myDate.getDate()<9?'0'+myDate.getDate():myDate.getDate();
    var minDate = myDate.getFullYear()+'-'+myMonth+'-'+myDay;
    $(function() {
    })
    // function enYmd(elem){
    //     jeDate(elem,{
    //         ranDate:'~',
    //         trigger:false,
    //         format: 'YYYY-MM-DD hh:mm:ss',
    //         minDate: minDate, //设定最小日期为当前日期
    //         maxDate: '2099-06-16 23:59:59' //最大日期
    //     });
    // }
    jeDate('#starDate', {
        format: 'YYYY-MM-DD',
        maxDate: '2099-06-16 23:59:59'
    })
    $('#endDate').on('click',function() {
        jeDate('#endDate', {
                trigger:false,
                isToday:false,
                format: 'YYYY-MM-DD',
                minDate: $('#starDate').val().length>0?$('#starDate').val():minDate,
                maxDate: '2099-06-16 23:59:59'
        })
    })
     
    function getTech(obj)
    {
        $.getJSON('/schedule-set-option/get-tech',{id:obj.value},function (data) {
            var html = "";
            if (data.code == 200) {
                var length = 0;
                console.log(data.data.length);
                if (data.data.length == 0) {
                    html += '<span class="pb-technician-checkbox">\n' +
                        '   \t所选分组下无技师\n' +
                        '</span>';
                }
                else
                {
                    for (i in data.data)
                    {
                        length++;
                        html += '<span class="pb-technician-checkbox">\n' +' \t<input type="checkbox" id="tech_id'+i+'" datatype="*" sucmsg=" " nullmsg="请选择技师" name="tech_id[]" value="'+i+'" class="tech_id" >\n' +'\t<label class="brand-label" for="tech_id'+i+'"><i class="gou-i"></i>'+data.data[i]+'</label>\n' +'</span>';
                    };
                    html+='<span class="justify_fix120"></span><span class="justify_fix120"></span><span class="justify_fix120"></span>';
                }

                $('#technician').html(html);


                if(length > 6){
                    $('.see-more-a').show();
                }
                else {
                    $('.see-more-a').hide();
                }
            }
        })
    }
    // 点击查看更多
    $('.see-more-a').on('click',function() {
        if ($('#technician').height()==70) {
            $('#technician').css('max-height','none');
            $(this).css('bottom','-30px');
            $(this).text('收起');
        }else{
            $('#technician').css('max-height','70px');
            $(this).text('查看更多');
            $(this).css('bottom','-20px');
        }
        
    })
    // 选择重复模式切换
    $("select[name='repeate_type']").on('change',function() {
        $("input[name='repeate_week[]']").prop('checked',false);
        $(".month-repeat-day").find('input').prop('checked',false);
        $(".month-repeat-day").parents('td').removeClass('td-orange');
        $("#customPattern").find('input').val("");
        $('#endDate').val('');
        var Judgement = $(this).val();
        switch(Judgement){
            case "2":
                empty(2);
                $('#weekly').show();
            break;
            case "3":
                empty(3);
                $('#monthly').show();
            break;
            case "4":
                empty(4);
                $('#customPattern').show();
            break;
            default:
                $('#repeatDate').hide();
        }
    })
    function empty(a){
        a==4 ? $('#repeatingName').text('开始日期：'):$('#repeatingName').text('重复日期：');
        $('#repeatDate').show();
        $('#repeatDate').find('.addinput-list').hide();
    }
    // 选中天
    function selectDay(el){
        if($(el).prop('checked')==true){
            $(el).parents('td').addClass('td-orange');
        }else{
             $(el).parents('td').removeClass('td-orange');
        }
    }
    //隐藏提示语
    $(".onchangeSt").change(function () {
        $("#technician_msg").hide();
        $("#schedule_set_id_msg").hide();
        $("#repeate_type_msg").hide();
        $("#week_msg").hide();
        $("#monthly_msg").hide();
        $("#customPattern_msg").hide();
        
    });
    var flag = true;

    //表单提交验证
    $("#schedule-set-option-form").Validform({
        beforeSubmit:function(curform){
            //请选择技师
            var technician = $("#technician").find('input:checked').val();

            if (technician == undefined) {
                $("#technician_msg").css("display",'block');
                $("#technician_msg").text('请选择技师');
                flag = false;
                return false;
            }

            //请选择班次
            var schedule_set_id = $("#schedule_set_id").val();
            if (schedule_set_id == '') {
                $("#schedule_set_id_msg").css("display",'block');
                $("#schedule_set_id_msg").text('请选择班次');
                flag = false;
                return false;
            }


            //请选择重复模式
            var repeate_type = $("#repeate_type").val();
            if (repeate_type == '') {
                $("#repeate_type_msg").css("display",'block');
                $("#repeate_type_msg").text('请选择重复模式');
                flag = false;
                return false;
            }

            //请选择重复日期
            var type = $("#repeate_type").val();

            var weekly = $('#weekly').find('input:checked').val();
            var monthly = $('#monthly').find('input:checked').val();
            var customPattern = $('#customPattern').find('input').val();

            if (type == 2 && weekly == undefined) {
                $("#weekly_msg").css("display",'block');
                flag = false;
                $("#weekly_msg").text('请选择重复日期');
                return false;
            }else if (type == 3 && monthly == undefined) {
                $("#monthly_msg").css("display",'block');
                flag = false;
                $("#monthly_msg").text('请选择重复日期');
                return false;
            }else if (type == 4 && customPattern == '') {
                $("#customPattern_msg").css("display",'block');
                flag = false;
                $("#customPattern_msg").text('请选择开始日期');
                return false;
            }

/*

            $('input[type="text"],textarea').each(function () {
                if($(this).val()!=''){
                    flag = false;
                }
            });
            $('input[type="checkbox"]').each(function () {
                if($(this).prop('checked') == true){
                    flag = false;
                }
            });
            $('select').find('option:selected').each(function () {
                if($(this).val()!='') {
                    flag = false;
                }
            });
*/


            if (flag == true) {
                var index = layer.confirm('是否确认保存？', {
                    btn: ['保存','取消'] //按钮
                }, function(){
                     flag = false;
                    //console.log(flag);return false;
                    $('#schedule-set-option-form').submit();
                }, function(){
                    parent.layer.close(index);//关闭弹出的子页面窗口
                });
                return false;
            }
            $("#confirm").prop("disabled", true);
        },
    });


    //取消按钮
    $(".layerCancel").click(function () {
        //请选择技师
        var cansel = false;
        var mark   = false;
        var technician = $("#technician").find('input:checked').val();

        if (technician != '' && technician != undefined) {
            cansel = true;

        }
        var schedule_set_id = $("#schedule_set_id").val();
        if (schedule_set_id != '' && schedule_set_id != undefined) {
            cansel = true;
        }

        //请选择重复模式
        var repeate_type = $("#repeate_type").val();
        if (repeate_type != ''  && repeate_type != undefined) {
            cansel = true;
        }

        //请选择重复日期
        var type = $("#repeate_type").val();

        var weekly = $('#weekly').find('input:checked').val();
        var monthly = $('#monthly').find('input:checked').val();
        var customPattern = $('#customPattern').find('input').val();

        if (type == 2 && weekly != ''  && weekly != undefined) {
            cansel = true;
        }else if (type == 3 && monthly !='' && monthly != undefined) {
            $("#monthly_msg").css("display",'block');
            cansel = true;
        }else if (type == 4 && customPattern != '' && customPattern != undefined) {
            cansel = true;
        }
        //console.log(cansel);return false;
        if(cansel == true)
        {
            layer.confirm('您已修改内容，是否取消保存？', {
                btn: ['取消保存','继续编辑'] //按钮
            }, function(){
                location.href = 'javascript:history.back(-1)';
            }, layer.close());
        }
        else {
            location.href = 'javascript:history.back(-1)';
        }

    });
</script>
<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use webapp\models\BrandQualification;
    use yii\helpers\ArrayHelper;
    $this->title = '编辑产品信息';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>">
<!-- 日期插件-->

<script src="/js/My97DatePicker/WdatePicker.js"></script>
<section id="main-content">
    <div class="panel-body">
        <form action="/intention-product/edit" method="post" id="intention-product-edit">
            <input type="hidden" value="<?=isset($data['id']) ? $data['id'] : '' ?>" name="id" />
            <input type="hidden" value="<?=isset($data['account_id']) ? $data['account_id'] : '' ?>" name="account_id">
            
            <div class="col-md-8 information-list">
                <label class="name-title-lable" > 客户：</label>
                
                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                    <p class="height34"><?=isset($data['account_name']) ? $data['account_name'] : '' ?></p>
                </div>
            </div>
            <div class="col-md-8 information-list">
                <label class="name-title-lable" > 产品名称：</label>
                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                    <p class="height34"><?=isset($data['prod_name']) ? $data['prod_name'] : '' ?></p>
                </div>
            </div>
    
            <div class="col-md-8 information-list">
                <label class="name-title-lable" > 品牌：</label>
                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                    <p class="height34"><?=isset($data['brand_name']) ? $data['brand_name'] : '' ?></p>
                </div>
            </div>
    
            <div class="col-md-8 information-list">
                <label class="name-title-lable" > 类目：</label>
                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                    <p class="height34"><?=isset($data['class_name']) ? $data['class_name'] : '' ?></p>
                </div>
            </div>
    
            <div class="col-md-8 information-list">
                <label class="name-title-lable" > 备注：</label>
                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                    <input type="text" value="<?= isset($data['info']) ? $data['info'] : '' ?>" name="info" max="50" class="form-control inputxt" />
                </div>
            </div>
            
            <div class="col-md-8 information-list">
                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                    <div class="col-md-2 col-xs-4" style="padding-left: 0">
                        <input type="button" class="btn ignore jn-btn" id="closeIframe" value="取消">
                    </div>
                    <div class="col-md-2  col-xs-4" style="padding-left: 0">
                        <input type="submit" value="提交" class="btn btn-orange jn-btn">
                    </div>
                
                </div>
            </div>
        </form>
    </div>
</section>

<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>


    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };

    var index = parent.layer.getFrameIndex(window.name);
    
    //表单验证
    $("#intention-product-edit").Validform({
        ajaxPost:true,
        tiptype:3,
        callback:function(data){
            if (data.code == 200) {
                parent.showAlert(data.message);
                parent.layer.close(index);
            }else{
                alert(data.message);
            }
        }

    });
    
    //关闭iframe
    $('#closeIframe').click(function(){
        parent.layer.close(index);
    });
    

</script>
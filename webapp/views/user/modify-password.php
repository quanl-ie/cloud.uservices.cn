<?php

use yii\bootstrap\ActiveForm;

$this->title = '修改密码';
$this->params['breadcrumbs'][] = $this->title;

use yii\captcha\Captcha;

?>
<link rel="stylesheet" href="/js/vue/static/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/register.password.css">
<style type="text/css">
    .layui-layer-content {
        text-align: center;
    }

    .layui-layer-btn {
        text-align: center;
    }

    .layui-layer-btn0 {
        position: static;
        width: auto;
    }

    .layui-layer-btn a {
        height: auto;
        line-height: 2
    }

    .page-container {
        text-align: center;
        font-size: 15px;
        display: table-cell;
        vertical-align: middle;
    }

    .orange-a {
        display: inline-block;
        padding: 8px 20px;
        border: 1px solid #2693ff;
        border-radius: 5px;
        display: inline-block;
        text-decoration: none;
        color: #fff;
        background: #2693ff;
        font-size: 14px
    }

    .page-container > div > .fa {
        color: #34bd30;
        font-size: 90px;
        width: 120px;
    }

    .page-container > div > p {
        margin: 10px 0;
        line-height: 1.5;
        font-size: 16px;
        font-weight: bold;
        color: #666
    }

    .page-container > div > p.desc {
        font-size: 12px;
        color: #b9b9b9;
        margin-bottom: 20px;
        font-weight: 100;
        letter-spacing: 1px;
    }

    .page-container a.orange-a {
        color: #fff;
    }

    .page-container a.orange-a,
    .page-container a.orange-a:hover,
    .page-container a.orange-a:focus,
    .page-container a.orange-a:active {
        outline: none;
        text-decoration: none;
    }

    span.second {
        color: #ff723f;
        font-weight: bold;
    }

    #container, #main-content, #main-content-wrapper {
        background: #fff !important;
    }
</style>
<?php if (Yii::$app->session->hasFlash('message')): ?>
    <section id="main-content"
             style="display: table;
        width: 100%;
        height: 100%;">
        <div class="page-container">
            <div>
                <i class="fa fa-3x fa-check-circle"></i>
                <p>重置密码成功</p>
                <p class="desc">恭喜，重置密码成功！</p>
                <!--                <a href="/site/login" class="orange-a">重新登录</a>-->
            </div>
        </div>
    </section>
<?php else: ?>
    <section id="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form id="form2" class="form-horizontal col-md-6" name="form1" action="/user/modify-password"
                              method="post">
                            <input type="hidden" name="_csrf"
                                   value="vtL1T-YDpXUU__GLSOe1uXB-bAvbNVWfECrd8bu-s5H9vccJskDuJCDKxNEkrvfUGRAdepRFbMtdHLqz08zrxw==">
                            <div class="form-group ipt-group field-userform-old_pwd required <?php
                            $error = $model->getErrors("old_pwd");
                            if ($error) {
                                echo "ipt-error";
                            } else {
                                echo "";
                            }
                            ?>">
                                <label class="col-sm-3 control-label" for="userform-old_pwd">原密码<span
                                            class="c_red">*</span></label>
                                <div class="col-sm-6"><input type="password" id="userform-old_pwd"
                                                             class="form-control ipt"
                                                             name="UserForm[old_pwd]" placeholder="原密码"
                                                             aria-required="true">
                                </div>
                                <span class="ipt-explain">
                                <?php
                                $error = $model->getErrors("old_pwd");
                                if ($error) {
                                    echo $error[0];
                                }
                                ?>
                            </span>
                            </div>
                            <div class="form-group ipt-group field-userform-new_pwd required <?php
                            $error = $model->getErrors("new_pwd");
                            if ($error) {
                                echo "ipt-error";
                            } else {
                                echo "";
                            }
                            ?>">
                                <label class="col-sm-3 control-label" for="userform-new_pwd">新密码<span
                                            class="c_red">*</span></label>
                                <div class="col-sm-6">
                                    <input type="password" id="userform-new_pwd" class="form-control ipt"
                                           name="UserForm[new_pwd]" placeholder="新密码" aria-required="true">
                                </div>
                                <span class="ipt-explain">
                                <?php
                                $error = $model->getErrors("new_pwd");
                                if ($error) {
                                    echo $error[0];
                                }
                                ?>
                            </span>
                            </div>
                            <div class="form-group ipt-group field-userform-verify_pwd required <?php
                            $error = $model->getErrors("verify_pwd");
                            if ($error) {
                                echo "ipt-error";
                            } else {
                                echo "";
                            }
                            ?>">
                                <label class="col-sm-3 control-label" for="userform-verify_pwd">确认密码<span
                                            class="c_red">*</span></label>
                                <div class="col-sm-6">
                                    <input type="password" id="userform-verify_pwd" class="form-control ipt"
                                           name="UserForm[verify_pwd]" placeholder="确认密码"
                                           aria-required="true">
                                </div>
                                <span class="ipt-explain">
                                <?php
                                $error = $model->getErrors("verify_pwd");
                                if ($error) {
                                    echo $error[0];
                                }
                                ?>
                            </span>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3"> </label>
                                <div class="col-sm-6">
                                    <button type="submit" id="submit-button" class="btn btn-success"
                                            style="color:#fff;width: 100%">确认重置
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/js/register.password.js"></script>
    <script type="text/javascript">
        $( function () {
            var util = jQuery.$$util;
            var isDefined = util.isDefined;
            var isBlank = util.isBlank;
            var form = jQuery( "#form2" );

            function checkPassword( value, check ) {
                var length;
                if ( !isDefined( value ) || isBlank( value ) ) {
                    check( "请输入密码！" );
                } else if ( (length = value.length) < 6 || length > 18 ) {
                    check( "请输入6-18位密码！" );
                } else if ( !/^(?=.*[\d])(?=.*[a-zA-Z])([a-zA-Z\d]{6,18})$/.test( value ) ) {
                    check( "密码必须是数字和字母组合！" );
                }
            }

            var proveHandler = form.proving( {
                //UserForm[old_pwd] UserForm[new_pwd] UserForm[verify_pwd]
                "UserForm[old_pwd]"    : {
                    bind  : "blur",
                    prove : function ( val, check ) {
                        check( isDefined( val ) && !isBlank( val ), "请输入原密码！" );
                    }
                },
                "UserForm[new_pwd]"    : {
                    bind  : "blur",
                    prove : function ( val, check ) {
                        checkPassword( val, check );
                        check( true );
                    }
                },
                "UserForm[verify_pwd]" : {
                    bind  : "blur",
                    prove : function ( value, check, api ) {
                        var ps = api.getValue( "UserForm[new_pwd]" );
                        checkPassword( value, check );
                        check( ps === value, "两次密码输入不一致！" );
                    }
                }
            } );

            $( "#submit-button" ).click( function ( e ) {
                e.preventDefault();
                proveHandler.proveAll().done( function () {
                    form.submit();
                } );
            } );

        } );
    </script>
<?php endif; ?>

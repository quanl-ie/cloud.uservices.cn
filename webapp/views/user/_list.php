
<table class="table table-bordered table-striped table-hover" id="enableTable">
    <thead bgcolor="#2693ff">
    <tr>
    <tr>
        <th style="min-width: 73px">姓名</th>
        <th width="100px">登录手机号</th>
        <th width="80px">职位</th>
        <th width="80px">邮箱</th>
        <th>所属机构</th>
        <th width="50px">角色</th>
        <th>创建时间</th>
        <th width="150px;">操作</th>
    </tr>
    </tr>
    </thead>
    <tbody>
    <?php if($data['list']):?>
        <?php foreach($data['list'] as $key=>$val):?>
                <tr>
                    <td><?php echo $val['username'];?> </td>
                    <td><?php echo $val['mobile'];?></td>
                    <td><?php echo $val['job_title'];?></td>
                    <td><?php echo $val['email'];?></td>
                    <td><?php echo $val['department'];?></td>
                    <td>
                        <a href="javascript:" onclick="viewRole(this);">点击查看</a>
                        <p style="display: none"><?php echo $val['role_name'];?></p>
                    </td>
                    <td><?php echo date("Y-m-d H:i:s",$val['created_at']);?></td>
                    <td>
                        <!-- //判断是否是当前 所属系统的所属登录用户-->
                        <?php if($val['id'] == $uid && $val['department_id'] == $did): ?>
                            <?php if(in_array('/user/reset-pwd',$selfRoles)){?>
                                <a href="javascript:" onclick="reset(<?php echo $val['id'];?>)">重置密码</a>
                            <?php };?>
                        <?php else: ?>
                            <?php if($val['status'] == 1):?>
                                <?php if(in_array('/user/disable',$selfRoles)){?>
                                    <a href="/user/disable?id=<?php echo $val['id'];?>&t=1" onclick="return confirm('您确定要禁用吗？',function(obj){ document.location.href = obj.href ;} , this);">禁用</a>
                                <?php };?>
                                <?php if(in_array('/user/update',$selfRoles)){?>
                                    <a href="/user/update?id=<?php echo $val['id'];?>" class="update">修改</a>
                                <?php };?>
                                <?php if(in_array('/user/reset-pwd',$selfRoles)){?>
                                    <a href="javascript:" onclick="reset(<?php echo $val['id'];?>)">重置密码</a>
                                <?php };?>
                            <?php else:?>
                                <?php if(in_array('/user/enable',$selfRoles)){?>
                                    <a href="/user/enable?id=<?php echo $val['id'];?>&t=2" onclick="return confirm('您确定要启用吗？',function(obj){ document.location.href = obj.href ;} , this);">启用</a>
                                <?php };?>
                                <?php if(in_array('/user/del',$selfRoles)){?>
                                    <a href="/user/del?id=<?php echo $val['id'];?>&d=2" onclick="return confirm('您确定要删除吗？',function(obj){ document.location.href = obj.href ;} , this);">删除</a>
                                <?php };?>
                            <?php endif;?>
                        <?php endif;?>
                </tr>
        <?php endforeach;?>

    <?php else:?>
        <tr>
        <tr><td colspan="8" class="no-record">未找到相关用户</td></tr>
        </tr>
    <?php endif;?>
    </tbody>
</table>
<div class="col-xs-12 text-center pagination">
<?php echo $pageHtml;?> 
</div>
<script>
      // 点击查看职业
      function viewRole(el){
            layer.open({
                  type: 1,
                  title:'角色',
                  area: ['320px', '200px'],
                  content: $(el).next('p').text()
            });
      }
      //重置密码
      function reset(id){
            layer.open({
                  type: 2,
                  title: '重置密码',
                  area: ['400px', '250px'],
                  fixed: false, //不固定
                  maxmin: false,
                  scrollbar: false,
                  content: '/user/reset-pwd?id='+id,

            });
      }
</script>

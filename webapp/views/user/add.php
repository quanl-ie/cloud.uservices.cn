<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\models\Region;
use common\models\Role;
$this->title = '创建新用户';
$this->params['breadcrumbs'][] = ['label' => '用户管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<style>
.drop-dw-layerbox .icon{left: 0;}
.addinput-list{height: 56px;overflow: hidden;}
.addinput-list>.Validform_wrong{position: static;}
</style>
<div class="jn-title-box no-margin-bottom">
     <span class="jn-title">添加用户</span>
</div>
<section id="main-content">
    <div class="panel-body" style="position: relative;">
        <div class="col-md-12">
            <h5 class="block-h5" style="padding-top: 0">添加用户</h5>
        </div>
        <?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-signin',
                'id' => 'form2',
                'name' => 'form1',
                'enctype' => 'multipart/form-data'
            ]
        ]);
        ?>
        <div class="information-list">
            <label class="name-title-lable">
               <i class="red-i">*</i> 姓名：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <input type="text" value="" name="AdminUser[username]" class="form-control" maxlength="10" placeholder="不超过10个字（建议使用真实姓名）"  sucmsg=" " nullmsg="请输入联系人姓名"  >
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
               <i class="red-i">*</i> 登录手机号：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <input type="text" value="" name="AdminUser[mobile]" class="form-control" placeholder="11位数字的手机号（登录时使用）" id="contact_mobile" datatype="/^[1][3,4,5,6,7,8,9][0-9]{9}$/" placeholder="请输入联系人电话" sucmsg=" " nullmsg="请输入联系人电话" errormsg="手机号格式错误" maxlength="11">
            </div>
              <?php if($model->hasErrors('mobile')): ?>
                    <span class="Validform_checktip Validform_wrong"><?php print_r($model->getErrors('mobile')[0]);?></span>
              <?php endif;?>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
               <i class="red-i">*</i> 初始密码：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <input type="text" value="" name="AdminUser[password]" class="form-control" minlength="6" maxlength="16" placeholder="6-16位数字/大小写字母/下划线" datatype="/^[\d_a-zA-Z]{6,16}$/" sucmsg=" " nullmsg="请输入密码" errormsg="密码格式错误" >
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
               <i class="red-i">*</i> 所属机构：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list region-limit" style="height: auto;overflow: visible;">
                <input type="text" class="form-control" id="category1"  placeholder="请选择上级机构" readonly  sucmsg="" nullmsg="请选择机构">
                <input type="hidden" id="select_department_id" value="" name="AdminUser[department_id]">
                <i class="icon iconfont icon-shouqi1"></i>
                <p class="misplacement" style="bottom: -5px;" id="work_type_msg">请选择上级机构</p>
                <div class="drop-dw-layerbox">
                    <ul style="padding-left: 0;padding-top: 10px;">
                        <?php if (!empty($department)) : ?>
                        <?php foreach ($department as $key => $val) : ?>
                        <li>
                         <span>
                            <?php if ($val['exist'] == 1) : ?>
                                <i class="icon2-shixinyou"  onclick="clickCategory(this) "></i>
                                <input type="radio" value="<?=$val['id'] ?>" data-id="<?=$val['id'] ?>" name="department_id" id="jg<?=$val['id'] ?>" class="pr1" onclick="changeCategory(this)">
                            <?php else:?>
                                <input type="radio" value="<?=$val['id'] ?>" data-id="<?=$val['id'] ?>" name="department_id" id="jg<?=$val['id'] ?>" class="pr1" onclick="changeCategory(this)">
                            <?php endif;?>
                             <label for="jg<?=$val['id'] ?>"><?=$val['name'] ?></label>
                        </span>
                        </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
                职位：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <input type="text" value="" name="AdminUser[job_title]" class="form-control" maxlength="50" placeholder="请输入该员工的职位" >
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
               邮箱：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <input type="text" name="AdminUser[email]" value="" class="form-control" maxlength="50" placeholder="请正确输入邮箱格式">
            </div>
              <?php if($model->hasErrors('email')): ?>
                    <span class="Validform_checktip Validform_wrong"><?php print_r($model->getErrors('email')[0]);?></span>
              <?php endif;?>
        </div>
        <input type="hidden" value="<?php echo $department_id;?>" id="department_id">
        <div class="information-list">
            <label class="name-title-lable">
               <i class="red-i">*</i> 角色：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list" style="height: auto;overflow:visible; ">
                <input type="text" value=""  class="form-control" placeholder="请选择角色" readonly id="role_Select" onclick="roleSelect();">
                <input type="hidden" value="" name="AdminUser[role_ids]" id="role_Select_ids">
                <i class="icon iconfont icon-shouqi1"  onclick="roleSelect();"></i>
                <p class="misplacement" style="bottom: -5px;" id="work_type_msg">请选择角色</p>
                <div class="drop-dw-rolebox" style="top: 35px;">
                    <ul class="many-role-ul" id="check" style="display: block;"><!--多选-->
                        <?php if(!empty($roles)):?>
                        <?php foreach($roles as $k=>$v):?>
                        <li>
                            <input class="role" type="checkbox" name="role" id="<?= $k;?>" data-id="<?= $k;?>" text="<?= $v;?>" value="<?= $k;?>">
                            <label for="<?= $k;?>"><?= $v;?><i class="gou-i"></i></label>
                        </li>
                        <?php endforeach;?>
                        <?php endif;?>
                    </ul>
                    <ul class="many-role-ul" id="radio" style="display: none;"><!--单选-->
                        <?php if(!empty($roles)):?>
                            <?php foreach($roles as $k=>$v):?>
                                <li>
                                    <input type="radio" class="role" name="role" id="dx<?= $k;?>" text="<?= $v;?>" data-id="<?= $k;?>" value="<?= $k;?>">
                                    <label for="dx<?= $k;?>"><?= $v;?><i class="gou-i"></i></label>
                                </li>
                            <?php endforeach;?>
                        <?php endif;?>
                    </ul>
                      <input type="hidden" id="isset_mobile" value="0">
                    <div  class="btn-li">
                        <a class="bg-f7"  href="javascript:" id="cancelRole">取消</a>
                        <a class="btn-orange"  href="javascript:" id="okRole">确定</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="information-list">
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <a href="/user/index"><button type="button" class="btn bg-f7  mgr8">取消</button></a>
                <input type="submit" value="确认" id="btnSt" class="btn btn-orange">
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</section>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    $('#role').on('click',function() {
        $('.drop-dw-rolebox').is(':hidden')?$('.drop-dw-rolebox').show():$('.drop-dw-rolebox').hide();
        
    })
    $('#contact_mobile').on('focus',function() {
          $('#category1').val('');
          $('#select_department_id').val('');
          $('.department_id').val('');
          $("input[name='department_id']").prop('checked',false);

    })

    function  clickCategory(el) {
        var auth = <?php echo $auth;?>;
        var directCompanyId = <?php echo $directCompanyId;?>;
        var id = $(el).next('input').val();
        var _this = $(el).parent('span');
        $.getJSON('/common/ajax-get-department',{'pid':id,'auth':auth,'type':1,'company_id':directCompanyId},function (data) {
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>"
                $(data.data).each(function(i,el) {
                    console.log(el);


                    if(el.is_user == 1){
                        ulData +="<li><span>";
                        el.exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="clickCategory(this)"></i>':'';
                        ulData+='<input type="radio" disabled="false" value="'+el.id+'" name="department_id" id="jg'+el.id+'" class="pr1" onclick="changeCategory(this)"><label for="jg'+el.id+'" class="disabled-label">'+el.name+'</label></span></li>';
                    }else{
                        ulData +="<li><span>";
                        el.exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="clickCategory(this)"></i>':'';
                        ulData+='<input type="radio" value="'+el.id+'" name="department_id" id="jg'+el.id+'" class="pr1" onclick="changeCategory(this)"><label for="jg'+el.id+'">'+el.name+'</label></span></li>';

                    }
                });
                ulData +="</ul>";
                _this.parent('li').append(ulData);
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');

            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })
    }
    function changeCategory(el) {

        $('.drop-dw-layerbox').hide();
        $('#select_department_id').val($(el).val());
          var department_id = $(el).val();
          console.log(department_id);
          var mobile = $('#contact_mobile').val();

          $.post("/user/ajax-reg-mobile",{department_id:department_id,mobile:mobile},function(result){
               if(result.success == false){
                     $("#isset_mobile").val(1);
                     alert(result.message);
                     return false;
               }else{
                     $("#isset_mobile").val(0);
               }
          });
        $('#category1').val($(el).next('label').text());
        $('.role').val('');
        $('#role_Select').val('');
    }
    
    $('#category1').on('click',function() {
        $('.drop-dw-layerbox').is(':hidden')?$('.drop-dw-layerbox').show():$('.drop-dw-layerbox').hide();
        $('.drop-dw-rolebox').hide();
        $("input[name='role']").prop('checked',false);

    })

    // 点击展示角色
    function roleSelect() {
        var select_id = $("#select_department_id").val();
        var self_id = '<?php echo $department_ids; ?>';
        if(select_id == ''){
            alert('请选择正确的机构');
            return false;
        }
          self_id_arr = self_id.split(",");
          if($.inArray(select_id, self_id_arr) > -1){
          　　$("#check").show();
              $("#radio").hide();
          }else{
            $("#check").hide();
            $("#radio").show();
        }
        if ($('.drop-dw-rolebox').is(':hidden')) {
            $('.drop-dw-rolebox').show();
        }else{
            $('.drop-dw-rolebox').show();
        }
    }
    var selectRole = '',selectRoleIds='';
    //取消选择角色
    $('#cancelRole').on('click',function() {
        // $('.drop-dw-rolebox')
        selectRole = '';
        $("input[name='role']").prop('checked',false);
        $('#role_Select').val(selectRole);
        $('.drop-dw-rolebox').hide();
    })
    
    //确定选择角色
    $('#okRole').on('click',function() {
        selectRole = '';
        selectRoleIds = '';
        $("input[name='role']").each(function() {
            if ($(this).is(':checked')) {
                selectRole +=$(this).attr('text')+'、'
                selectRoleIds +=$(this).attr('data-id')+';'
            }
        });
        selectRole = selectRole.substring(0,selectRole.length-1);
        if (selectRole!='') {
            $('#role_Select').val(selectRole.replace(/(.*)[;]$/, '$1'));
            $('#role_Select_ids').val(selectRoleIds);
            $('.drop-dw-rolebox').hide();
        }else{
            alert('请选择角色');
        }
        
    })
    //表单验证
    $("#form2").Validform({
        tiptype:3,
        datatype:{
            //"zh1-20":/^[\u4E00-\u9FA5\uf900-\ufa2d]{1,20}$/,
            "zh1a":/^[\u4e00-\u9fa5_a-zA-Z0-9 ]+$/   //只允许数字，字母，汉字，空格
        },
        beforeSubmit:function(curform){
                    if($('#select_department_id').val() == ''){
                          alert("请选择机构");
                          return false;
                    }
                    if($("#isset_mobile").val() == 1){
                          alert("该机构下登录手机号已被占用，请编辑后再次尝试");
                          return false;
                    }
                    if($('#role_Select_ids').val() == ''){
                          alert("请选择角色");
                          return false;
                    }
            $("#btnSt").prop("disabled", true);
        }
    });
    $(document).click(function(event) {
       var region = $('.region-limit');
       if (!region.is(event.target)&&region.has(event.target).length ===0) {
                $('.drop-dw-layerbox').hide();
       }
    });
</script>
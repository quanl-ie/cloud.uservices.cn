<?php
use webapp\models\Region;
$this->title = '成员信息查看';
$this->params['breadcrumbs'][] = ['label' => '团队成员管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="main-content">
            
	<div class="row">
        <div class="col-md-12">                    
            <div class="panel panel-default">                     
                <div class="panel-body">
                  <div class="table-lr">
                    <table class="table table-bordered" style="background:#FFF;">
                        <tbody>

                          <tr>
                            <td  class="col-sm-3 left">姓名</td>
                            <td  class="col-sm-8 right"> <?php echo $model->username;?></td>
                          </tr>
                          <tr>
                            <td class="left">昵称</td>
                            <td class="right"><?php echo $model->nick_name;?></td>
                          </tr>
                          <tr>
                            <td class="left">手机号</td>
                            <td class="right"><?php echo $model->mobile;?></td>
                          </tr>
                          
                          <tr>
                            <td class="left">服务城市</td>
                            <td class="right"><?php echo Region::getCityName($model->server_city_id);?></td>
                          </tr> 
                          
                          <tr>
                            <td class="left">权限角色</td>
                            <td class="right"><?php echo $roles;?></td>
                          </tr>   
                          
                         </tbody>
                    </table> 
                  </div>
                  <div class="text-right btn-green-line"><a href="/user/index"><button class="btn btn-default">返回</button></a></div>
                </div>

            </div>                         
        </div>
    </div>                        
</section>
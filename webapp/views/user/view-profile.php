<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '商家信息';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
		border-top:2px solid #2693ff;
	}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">商家信息</span>
</div>
<section id="main-content">
	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
					<!-- 切换 -->
                	<div class="col-md-12">
                		<ul id="myTab" class="nav nav-tabs">
							<li class="active">
                                <a href="#essentialInformation" data-toggle="tab" >
									基本信息
								</a>
							</li>
							<li><a href="#credentials" data-toggle="tab">认证信息</a></li>
						</ul>
                	</div>
                	<!-- 内容 -->
                	<div id="myTabContent" class="tab-content col-md-12">
                		<!-- 基本信息 -->
                		<!--  -->
	                		<div class="tab-pane fade in active" id="essentialInformation">
                                <form method="post" action="/user/view-profile">
		                			<div class="details-column">
		                				<label class="details-column-title">公司全称：</label>
                                        <div class="details-column-content"><?= isset($model->company)?$model->company:'' ?></div>
		                			</div>
		                			<div class="details-column">
		                				<label class="details-column-title">联系人：</label>
		                				<div class="details-column-content">
                                            <input type="text" value="<?= isset($model->contact)?$model->contact:'' ?>" maxlength="20" name="Manufactor[contact]">
		                				</div>
		                			</div>
		                			<div class="details-column">
		                				<label class="details-column-title">联系电话：</label>
		                				<div class="details-column-content">
                                            <input type="text" name="Manufactor[mobile]" maxlength="11" value="<?= isset($model->mobile)?$model->mobile:'' ?>">
		                				</div>
		                			</div>
		                			<div class="details-column">
		                				<label class="details-column-title">电子邮箱：</label>
		                				<div class="details-column-content">
                                            <input type="text" name="Manufactor[email]" maxlength="50" value="<?= isset($model->email)?$model->email:'' ?>" placeholder="请输入公司联系人邮箱，例：XXX@163.com">
		                				</div>
		                			</div>
		                			<div class="details-column">
		                				<label class="details-column-title"><i class="red-i">*</i>所在区域：</label>
		                				<div class="details-column-content">
		                					<div class="three-linkage">
			                					<div class="linkage-justify-list">
											        <select class="form-control" id="province" name="Manufactor[province_id]" datatype="*" nullmsg="请选择所在区域">
                                                        <?php if (!empty($model->province_id)) : ?>
                                                            <option value="<?=$model->province_id ?>" selected="selected" ><?=$model->province_name ?></option>
                                                            <?php foreach ($province as $key => $val): ?>
                                                                <option value="<?=$val['region_id'] ?>"><?=$val['region_name'] ?></option>
                                                            <?php endforeach;?>
                                                        <?php else :?>
                                                            <option value="">请选择省</option>
                                                            <?php foreach ($province as $key => $val): ?>
                                                                <option value="<?=$val['region_id'] ?>"><?=$val['region_name'] ?></option>
                                                            <?php endforeach;?>                        
                                                        <?php endif ;?>
								                    </select>
			                					</div>
			                					<div class="linkage-justify-list">
			                						<select class="form-control" id="city" name="Manufactor[city_id]" datatype="*" nullmsg="请选择所在区域">
                                                        <?php if (!empty($model->city_id)) :?>
                                                            <option value="<?=$model->city_id ?>" selected="selected" ><?=$model->city_name ?></option>
                                                        <?php else :?>
                                                            <option value="">请选择市</option>
                                                        <?php endif ;?>
                                                        
								                    </select>
			                					</div>
			                					<div class="linkage-justify-list">
			                						<select class="form-control" id="district" name="Manufactor[district_id]" datatype="*" nullmsg="请选择所在区域">
                                                        <?php if (!empty($model->district_id)) :?>
                                                            <option value="<?=$model->district_id ?>" selected="selected" ><?=$model->district_name ?></option>
                                                        <?php else :?>
                                                            <option value="">请选择区</option>
                                                        <?php endif ;?>
                                                        
								                    </select>
			                					</div>

			                					<div class="justify_fix"></div>
		                					</div>
		                				</div>
		                			</div>
		                			<div class="details-column">
		                				<label class="details-column-title">详细地址：</label>
		                				<div class="details-column-content">
                                            <input type="text" name="Manufactor[address]" maxlength="100" value="<?= isset($model->address)?$model->address:'' ?>" placeholder="请输入公司联系人邮箱，例：XXX@163.com">
		                				</div>
		                			</div>
		                			<div class="details-column">
		                				<label class="details-column-title">公司简介：</label>
		                				<div class="details-column-content" style="width: 400px;position: relative;">
                                            <textarea name="Manufactor[company_desc]" maxlength="200"><?= isset($model->company_desc)?$model->company_desc:'' ?></textarea>
		                					 <p class="xianzhi"><span>0</span>/200</p>
		                				</div>
		                			</div>
		                			<div class="details-column">
		                				
		                				<div class="details-column-content">
		                					<input type="submit" name="" value="保存" class="details-column-submit">
		                				</div>
		                			</div>
	                			</form>
	                		</div>
	                	<!-- </form> -->
                		<!-- 认证信息 -->
                		<div  class="tab-pane fade" id="credentials">
                			<div class="details-column">
                				<span class="details-column-title">公司类型：</span>
                				<div class="details-column-content">企业</div>
                			</div>
							<div class="details-column">
                				<span class="details-column-title">证件类型：</span>
                				<div class="details-column-content">普通营业执照（存在独立的组织机构代码证）</div>
                			</div>
							<div class="details-column">
                				<span class="details-column-title">公司全称：</span>
                				<div class="details-column-content">北方智能科技股份有限公司</div>
                			</div>
                			<div class="details-column">
                				<span class="details-column-title">营业执照注册号：</span>
                				<div class="details-column-content">1101010101010100</div>
                			</div>
                			<div class="details-column">
                				<span class="details-column-title">企业所在地：</span>
                				<div class="details-column-content">北京 北京市 东城区</div>
                			</div>
                			<div class="details-column">
                				<span class="details-column-title">详细地址：</span>
                				<div class="details-column-content">雍贵中心C座</div>
                			</div>
                			<div class="details-column">
                				<span class="details-column-title">经营范围：</span>
                				<div class="details-column-content">五金交电，日用百货、针纺织品、洗涤用品、化妆品、食品、保健食品、营养补充食品、家居护理用品、包装材料、橡塑制品、缝纫机服装及辅料、纺织面料、纺织助剂、羽绒制品、工艺品</div>
                			</div>
                			<div class="details-column">
                				<span class="details-column-title">经营类目：</span>
                				<div class="details-column-content">燃气壁挂炉</div>
                			</div>
                			<div class="details-column">
                				<span class="details-column-title">经营品牌：</span>
                				<div class="details-column-content">威能、博世</div>
                			</div>
							<div class="details-column">
                				<span class="details-column-title">营业执照：</span>
                				<div class="details-column-content">
                					<div class="details-column-tupian">
                						<img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1514029991690&di=38b419eb4702febec99ef628127c7303&imgtype=0&src=http%3A%2F%2Fimg.jdzj.com%2FUserDocument%2F2014c%2Ffutovo%2FPicture%2F2014520154558.jpg">
                					</div>
                				</div>
                			</div>
                			<div class="details-column">
                				<span class="details-column-title">组织机构代码证：</span>
                				<div class="details-column-content">
                					<div class="details-column-tupian">
                						<img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1514029991690&di=38b419eb4702febec99ef628127c7303&imgtype=0&src=http%3A%2F%2Fimg.jdzj.com%2FUserDocument%2F2014c%2Ffutovo%2FPicture%2F2014520154558.jpg">
                					</div>
                				</div>
                			</div>
                		</div>

 
                	</div>
                	<!-- 内容end -->

                </div>
            </div>
        </div>
    </div>
</section>
<script>

    //根据开通城市父级id获取市级数据
	$("#province").change(function() {
        $("#city").html("<option value=''>请选择市</option>");
        $("#district").html("<option value=''>请选择区</option>");
		var pid = $("#province").val();
		if(pid != ''){
            $.get("/account/get-city", { "province_id": pid }, function (data) {
		        $("#city").html(data);
		    });
		}
	});
	//根据城市获取区县
	$("#city").change(function() {
		var pid = $("#city").val();
		if(pid != ''){
			$.get("/account/get-district", { "city_id": pid }, function (data) {
		        $("#district").html(data);
		    })
		}
	});
</script>
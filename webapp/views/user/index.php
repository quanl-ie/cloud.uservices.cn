<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
$this->title = '用户管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<style>
    ul{margin-left: 20px;}
    .showDepartment{color:#5D5F63;}
    li a:hover{color: #333;}
</style>
<div class="jn-title-box no-margin-bottom">
     <span class="jn-title">用户管理</span>
        <div class="right-btn-box">
              <?php if($auth !== 1):?>
                  <?php if(in_array('/user/add',$selfRoles)){?>
                      <a href="/user/add" class="add">
                          <span class="btn btn-success  icon iconfont"   data-toggle="modal" data-target="#scrollingModal"><i class="fa fa-plus"></i>&nbsp;添加</span>
                      </a>
                  <?php };?>
              <?php endif ?>
        </div>
</div>
<section id="main-content">
    <div class="panel-body" style="position: relative;">
        <div class="hrc-left-box">
           <div class="hrc-left-sousuo">
               <input type="text" id="departmentName" class="form-control" style="width: 130px;" placeholder="机构名称">
               <button id="departmentSearch" class="btn btn-orange">搜索</button>
           </div>
           <div class="tree-shape-nav">
               <ul>
               <?php if(!empty($navData)):?>
                     <?php foreach ($navData as $val): ?>
                           <li class="firstNav">
                                 <span><i data-id="<?php echo $val['id'];?>" <?php if($val['sub']==0):?>style="display: none;"<?php endif;?> class="i<?php echo $val['id']; ?> icon2-shixinyou spanNav"></i><a class="showDepartment" href="javascript:void(0);" data-id="<?php echo $val['id'];?>"><?php echo $val['name'];?></a></span>
                                 <ul></ul>
                           </li>
                     <?php endforeach; ?>
               <?php else:?>
                     <li class="firstNav">
                           <span>未找到相关机构</span>
                     </li>
               <?php endif?>
               </ul>
           </div>
        </div>
        <div class="hrc-right-box">
            <div class="right-top-tab">
                <ul id="myTab" class="nav nav-tabs">
                    <li <?php if($status == 1){ echo 'class="active"';}?>><a href="#enable" data-toggle="tab" data-id="1" class="navTabs">启用中</a></li>
                    <li <?php if($status == 2){ echo 'class="active"';}?>><a href="#disable" data-toggle="tab" data-id="2" class="navTabs">已禁用</a></li>
                </ul>
                <div class="hrc-right-sousuo">
                    <input id="keyword" type="text" value="<?php echo $searchValue ?>" class="form-control" style="width: 200px;" placeholder="姓名/登录手机号">
                    <button id="btnSearch" class="btn btn-orange">搜索</button>
                </div>
            </div>
            <div id="myTabContent" class="tab-content border-right-auto" style="border-color: transparent;padding:0;">
                <div class="tab-pane fade in active fold-tb-box" id="tableList" style="width: 100%;">
                    <?php echo $this->render( '_list', ['data'=>$data,'uid'=>$userId,'did'=>$department_id,'type'=>$type,'selfDepartmentId'=>$department_id,'pageHtml'=>$pageHtml,'pageSize'=>$pageSize,'selfRoles'=>$selfRoles]);?>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var departmentFlag = true;
    var selfDepartmentId = '<?php echo $department_id;?>';
    $(function() {
        var documentHeight = $(document).height()-200
       documentHeight>$('.hrc-right-box').height()?$('.hrc-right-box').css('height',documentHeight+60+'px'):$('.hrc-right-box').css('height','auto');
    });
    //导航点击
    $('body').delegate('.spanNav', 'click',{},function () {
        if ($('.hrc-left-box').height()>$('.hrc-right-box').height()) {
            $('.hrc-right-box').css('height',$('.hrc-left-box').height()+100+'px');
        }

        var _self = $(this).parent('span');
        if($.trim(_self.next('ul').html()) == '' || departmentFlag == true)
        {
            departmentFlag = false;
            $.getJSON('/organization/ajax-get-department', {pid:$(this).data('id'),is_search:0}, function (json) {
                if(json.success == true){
                    var data = json.data;

                    var html= '';
                    for(item in data){
                        html+='<li>\n' +
                            '      <span>';
                        html+= '<i '+(data[item].sub == 1?'':'style="display:none;"')+' data-id="'+data[item].id+'" class="i'+data[item].id +' icon2-shixinyou spanNav"></i>';
                        html+= '<a class="showDepartment" href="javascript:void(0);" data-id="'+data[item].id+'">'+ data[item].name+'</a></span>\n' +
                            '      <ul></ul>\n'+
                            '  </li>\n';
                    }

                    if(html == ''){
                        _self.find('i').hide();
                    }
                    else {
                        _self.next('ul').html(html).show();
                        _self.find('i').removeClass('icon2-shixinyou');
                        _self.find('i').addClass('icon2-shixinxia');
                        _self.find('i').show();
                    }
                }
            });
        }
        else {
             _self.next('ul').toggle();

             if(_self.next('ul').is(':hidden')){
                   _self.find('i').removeClass('icon2-shixinxia');
                    _self.find('i').addClass('icon2-shixinyou');
             }
             else {
                 _self.find('i').removeClass('icon2-shixinyou');
                 _self.find('i').addClass('icon2-shixinxia');
             }
        }
    });

   //查看备注
   $('body').delegate('.viewRemark','click', {},function () {
        var id = $(this).data('id');
        $.getJSON('/organization/view-remark', {id:id}, function (json) {
            if(json.success == true){
                alert(json.data == ''?'暂无数据':json.data);
            }
            else{
                alert(json.message);
            }
        })
   });

   //机构列表展开收起
    $('body').delegate('.toggleOpenClose','click',{},function () {
         var id = $(this).data('id');
         var index = $(this).parents('tr').index();
         var _self = $(this);

         var childTr = $(this).parents('table').find('.tr'+index);
         if(childTr.length == 0)
         {
             var trClass = ''
             if(typeof($(this).parents('tr').attr('class'))!='undefined'){
                 trClass = $(this).parents('tr').attr('class');
             }
         }
         else
         {
             if(_self.find('i').hasClass('icon2-shixinyou')){
                 childTr.show();
             }
             else {
                 childTr.hide();
             }

             if(childTr.is(':hidden')){
                _self.find('i').addClass('icon2-shixinyou');
                _self.find('i').removeClass('icon2-shixinxia');
             }else{
                _self.find('i').removeClass('icon2-shixinyou');
                _self.find('i').addClass('icon2-shixinxia');
             }
         }
    });
    //导航点击显示机构
    var departmentId = '<?php echo $department_id;?>';
    var departmentStatus = 1;
    var keyword = '';
    var searchType = 2;
    $('body').delegate('.showDepartment','click',{}, function () {
        $('.showDepartment').css('color','#333');
        $(this).css('color','#428bca');
        departmentId = $(this).data('id');
        keyword = $('#keyword').val();
        searchDepartment(departmentId,departmentStatus,keyword,2);
    });

    //标签切换
    $('body').delegate('.navTabs', 'click', {}, function () {
        departmentStatus = $(this).data('id');
        keyword = $('#keyword').val();
        searchDepartment(departmentId,departmentStatus,keyword,2);
    });

    //搜索
    $('#btnSearch').click(function () {
        keyword = $('#keyword').val();
        searchType = 3;
        searchDepartment(departmentId,departmentStatus,keyword,searchType);
    });

    //搜索方法
    function searchDepartment(pid,status,keyword,type) {
        $.getJSON('/user/index', {pid:pid,type:type,status:status,searchValue:keyword}, function (json) {
            if(json.success == true){
                $('#tableList').html(json.data);
            }
        });
    }

    //回车搜索
    $('#keyword').keyup(function (event) {
         if(event.keyCode == 13){
             $('#btnSearch').click();
         }
    });

    //部门树搜搜
    $('#departmentSearch').click(function () {
         var keywrod = $.trim($('#departmentName').val());
        $.getJSON('/organization/ajax-get-department', {name:keywrod,is_search:2}, function (json) {
            if(json.success == true){
                var data = json.data;
                var html= '';
                for(item in data){
                    html+='<li>\n' +
                        '      <span>';
                        if(data[item].sub == 1 && keywrod == ''){
                            html+= '<i data-id="'+data[item].id+'" class="icon2-shixinyou '+(data[item].sub==1?'spanNav':'')+'"></i>';
                        }
                        html+='         <a class="showDepartment" href="javascript:void(0);" data-id="'+data[item].id+'">'+ data[item].name+'</a></span>\n' +
                        '      <ul></ul>\n'+
                        '  </li>\n';
                }
                if(html == ''){
                    $('.tree-shape-nav ul').html("<li style='margin-top:100px;text-align: center;'>未找到相关机构</li>").show();
                }
                else {
                    $('.tree-shape-nav ul').html(html).show();
                }
            }
        });

    });
    $('#departmentName').keyup(function (event) {
        if(event.keyCode == 13){
            $('#departmentSearch').click();
        }
    });

    <?php if(Yii::$app->session->hasFlash('message')): ?>
    alert('<?php echo Yii::$app->session->getFlash('message');?>');
    <?php endif;?>
</script>
<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use webapp\assets\AppAsset;
use yii\helpers\Url;
$this->title = '忘记密码';
AppAsset::addCss($this, '/plugins/bootstrap/css/bootstrap.min.css');
$this->registerJsFile('/js/jquery-1.10.2.min.js', ['position'=>1]);

?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>

<style>
    body{
        font-family: "微软雅黑";
    }
.register-title{width: 100%;padding: 15px 0 10px 0;}
.register-title-box>div{display: inline-block;height: 43px;overflow: hidden;}
.register-title-left>img{height: 43px;}
.register-title-right>p{margin-left: 10px;line-height: 43px;font-size: 28px;font-weight: 500;}
.login-logo{ height: 100px;line-height: 100px;overflow: hidden;}
.login-boss{width: 100%;height: 550px;}
.box-1200{width: 1200px;border-radius: 4px;margin:0 auto;  }
.wj-h5{padding:15px 30px;margin-top: 0;background: #f8f8f8;font-size: 16px;font-weight: 600}
.details-column{position: relative;margin-top: 30px;width: 700px;}
.details-column-title{text-align: right;position: absolute;width: 100px;height: 30px;line-height: 30px;font-size: 14px;}
.details-column-content{ min-height: 30px;margin-left: 120px;line-height: 30px;font-size: 14px;position: relative;}
.login-boss>.box-1200{border: 1px solid #e8e8e8;}
.three-linkage{width:316px;text-align:justify;}
.linkage-justify-list{display: inline-block;width: 140px;text-align: left;vertical-align: top; height: 40px;}
.linkage-justify-list>input,.linkage-justify-list>a{ width: 100%;border-radius: 4px;height: 42px;}
.linkage-justify-list>input{border: 1px solid #ccc;padding-left: 15px}
.justify_fix{display:inline-block; width:100%; height:0; overflow:hidden;}
.column-input{display: inline-block;padding-left: 15px;width:316px;height: 42px;border: 1px solid #ccc;border-radius: 4px;}
.wj-form{padding: 20px 0 50px 0;}.ok-btn{width: 140px;height: 42px}
.btn-orange:hover,a:hover{color: #fff;}
.errorMsg{color:red;margin-left:120px;}
</style>
<!-- <div class="login-box"> -->
<div class="register-title">
    <div class="box-1200 register-title-box">
        <div class="register-title-left">
            <img src="<?php echo Url::to('@web/img/logo2.png');?>">
        </div>
        <div class="register-title-right">
            <p>优服务-管理系统</p>
        </div>
    </div>
</div>
<div class="login-boss">
    <div class="box-1200" style="position: relative;padding-bottom: 100px;">
        <h5 class="wj-h5">忘记密码</h5>
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'class'=>'wj-form','enableClientValidation' => false]); ?>
            <div class="details-column">
                <label class="details-column-title">手机号</label>
                <div class="details-column-content">
                    <input type="text" value="<?php echo $model->mobile;?>" maxlength="11" name="AdminUser[mobile]" id="mobile" onKeypress="return (/[\d.]/.test(String.fromCharCode(event.keyCode)))" class="column-input">
                </div>
                <?php if($model->hasErrors('mobile')): ?>
                    <p class="errorMsg"><?php echo $model->getErrors('mobile')[0];?></p>
                <?php endif;?>
            </div>
            <div class="details-column" style="height: 40px">
                <label class="details-column-title">验证码</label>
                <div class="details-column-content">
                    <div class="three-linkage">
                        <div class="linkage-justify-list" >
                            <input type="text" name="AdminUser[vcode]" id="vcode" maxlength="6">
                        </div>
                        <div class="linkage-justify-list">
                            <a href="javascript:"  class="btn btn-orange ok-btn btnGetSmsCode" style="line-height: 28px" >获取验证码</a>
                        </div>
                        <?php if($model->hasErrors('vcode')): ?>
                            <p class="errorMsg" style="margin-left: 0px;"><?php echo $model->getErrors('vcode')[0];?></p>
                        <?php endif;?>
                        <div class="justify_fix"></div>
                    </div>
                </div>
            </div>
            <div class="details-column department_id"  style="display: none">
                <label class="details-column-title">选择企业</label>
                <div class="details-column-content">
                    <select id="department_id" class="column-input" name="AdminUser[department_id]">
                    </select>
                </div>
                <p class="errorMsg"></p>
            </div>
            <div class="details-column">
                <label class="details-column-title">设置新密码</label>
                <div class="details-column-content">
                    <input type="text" name="AdminUser[password]" onfocus="this.type='password'" id="password" maxlength="50" class="column-input">
                </div>
                <?php if($model->hasErrors('password')): ?>
                    <p class="errorMsg"><?php echo $model->getErrors('password')[0];?></p>
                <?php endif;?>
            </div>
            <div class="details-column">
                <label class="details-column-title">验证新密码</label>
                <div class="details-column-content">
                    <input type="text" name="AdminUser[verify_pwd]" id="repassword" onfocus="this.type='password'" maxlength="50" class="column-input">
                </div>
                <?php if($model->hasErrors('verify_pwd')): ?>
                <p class="errorMsg"><?php echo $model->getErrors('verify_pwd')[0];?></p>
                <?php endif;?>
            </div>
            <div class="details-column">
                <label class="details-column-title"></label>
                <div class="details-column-content">
                    <div class="linkage-justify-list">
                        <button class="btn btn-orange ok-btn" id="ok-btn">确定重置</button>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<!-- </div> -->

<script type="text/javascript">
    var countdown=60;
    function settime(obj)
    {
        if (countdown <=0) {
            obj.removeAttr("disabled");
            obj.html("获取验证码");
            countdown = 60;
            return false;
        } else {
            obj.attr("disabled", true);
            obj.html("重新发送(" + countdown + ")");
            countdown--;
        }

        setTimeout(function() {
            settime(obj)
        },1000);
    }

    function getCode(obj)
    {
        var mobile = $("#mobile").val();
        if(!(/^1[3456789]\d{9}$/).test(mobile))
        {
            alert("手机号格式不正确");
            return false;
        }
        else
        {
            var sms_token = '<?php echo $sms_token;?>';
            $.ajax({
                url: '/user/send-sms',
                type: 'POST',
                dataType: 'json',
                async:false,
                data: {
                    'sms_token' : sms_token,
                    'mobile': mobile,
                },
                success: function(data){
                    alert(data.message);
                    if(data.status == 1){
                        settime(obj);
                    }
                }
            });
        }
    }

    $('.btnGetSmsCode').click(function () {
        getCode($(this));
    });

    //登陆账号失去焦点后查询有多少个账号
    $("#mobile").blur(function() {
        var mobile = $("#mobile").val();
        if(mobile != '' ){
            if(!(/^1[3456789]\d{9}$/).test(mobile))
            {
                alert("手机号格式不正确");
                return false;
            }
            $.post("/site/check-dep", { "mobile": mobile}, function (data) {
                if(data){
                    $(".department_id").show();
                    $("#department_id").html(data);
                }else {
                    $(".department_id").hide();
                    $("#department_id").html('');
                }
            });
        }
    })
    //提交验证
    $("#ok-btn").click(function() {
        var mobile = $("#mobile").val();
        var department_id = $("#department_id").val();
        var vcode         = $("#vcode").val();
        var repassword = $("#repassword").val();
        var password = $("#password").val();
        if(mobile == ''){
            alert('请输入手机号');
            return false;
        }
        if(vcode == ''){
            alert('请输入验证码');
            return false;
        }
        if(repassword == '' || password == ''){
            alert('请输入密码');
            return false;
        }
        if(department_id != null && department_id == ''){
            alert('公司不能为空');
            return false;
        }
    })


    <?php if(Yii::$app->session->hasFlash('alertMsg')):?>
        alert('<?php echo Yii::$app->session->getFlash('alertMsg');?>');
    <?php endif; ?>

</script>
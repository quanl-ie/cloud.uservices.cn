<?php
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use backstage\assets\AppAsset;
use yii\helpers\Url;
$this->title = '添加用户';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <?php
            $form = ActiveForm::begin([
                'options' => [
                    'class' => 'form-signin',
                    'id' => 'form2',
                    'name' => 'form1',
                    'enctype' => 'multipart/form-data'
                ]
            ]);
            ?>
            <fieldset>
                  <div class="box-body col-xs-5">
                        <div class="form-group">
                            <?= $form->field($model, 'username')->input('text',['maxlength'=>20]);?>
                        </div>
                        <div class="form-group">
                            <?= $form->field($model, 'email')->input('text',['maxlength'=>50]);?>
                        </div>
                        <div class="form-group">
                            <?= $form->field($model, 'mobile')->input('text',['maxlength'=>11]);?>
                        </div>
                        <div class="form-group">
                            <?= $form->field($model, 'password')->passwordInput(['maxlength'=>20]);?>
                        </div>
                        <div class="form-group">
                            <?= $form->field($model, 'g_id')->dropDownList(backstage\models\UserRegister::getGroup());?>
                        </div>
                  </div>

            </fieldset>
            <fieldset>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </fieldset>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
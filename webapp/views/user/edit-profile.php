<?php
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$this->title = '帐户信息';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
	.riqi1{
		position: relative;
	}
	body{
		background: #f1f2f7;
	}
	.information-nav-a:link{
		display: inline-block;
		color: #323232;
		margin-left: 54px;
		font-size: 14px;
	}
	.information-nav-a:hover{
		text-decoration: none;
		color: #2693ff;
	}
	.filePrew{
		font-size: 20px;
	}
	.details-column-content{
		margin-left: 130px;
	}
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">                    
            <div class="" style="position: relative;">
            	<div style="background: #fff;padding-bottom: 50px">
            		<div class="right-box-title">
            			<h4 style="font-size: 26px;color: #333">帐户信息</h4>
            		</div>
            		<div>
                        <?php
                        $form = ActiveForm::begin([
                            'options' => [
                                'class' => 'form-horizontal',
                                'id' => 'form2',
                                'name' => 'form1',
                                'enctype' => 'multipart/form-data',
                            ]
                        ]);
                        ?>
    						<div class="details-column" style="height: 90px;">
                				<label class="details-column-title">
                					<div class="headPortrait-letter-left">
			            				<img id="headSculpture" src="<?php echo $model->avatar==''? Url::to('/images/morentouxiang.png'):$model->avatar;?>">
			            			</div>
                				</label>
                				<div class="details-column-content" style="padding-top: 10px">
                					<div class="change-gallery">
	            						<span>更换头像</span>
                                        <input type="file" name="file" maxlength="255" accept="image/png, image/jpeg" class="filePrew">
	            					</div>
	            					<p style="margin-top: 10px">头像仅支持 JPG、PNG 格式，文件最大3 MB。</p>
                				</div>
                			</div>
    						<div class="details-column">
                				<label class="details-column-title">手机号</label>
                				<div class="details-column-content">
                					<span><?php echo $model->mobile;?></span>
                				</div>

                			</div>
                			<div class="details-column">
                				<label class="details-column-title">
                                    <i class="red-i">*</i>
                					姓名
                				</label>
                				<div class="details-column-content">
                                    <input type="text" value="<?php echo $model->username;?>" name="AdminUser[username]" placeholder="请输入您的真实姓名（20字以内）" maxlength="20" />
                                    <?php if($model->hasErrors('username')): ?>
                                    <span class="error-span">姓名不能为空</span>
                                    <?php endif;?>
                				</div>
                			</div>
							<div class="details-column">
                				<label class="details-column-title">性别</label>
                				<div class="details-column-content">
                					<div  class="radio-list">
					                    <input type="radio" value="1" name="AdminUser[sex]" id="male" class="pr1" <?php if($model->sex==1):?>checked="checked"<?php endif; ?> /><label for="male">先生</label>
					                </div>
					                <div  class="radio-list">
					                    <input type="radio" value="2" name="AdminUser[sex]" id="female" class="pr1" <?php if($model->sex==2):?>checked="checked"<?php endif; ?> /><label for="female">女士</label>
					                </div>
                				</div>
                			</div>
                            <div class="details-column">
                                <label class="details-column-title">
                                    邮箱
                                </label>
                                <div class="details-column-content">
                                    <input type="text" value="<?php echo $model->email;?>" name="AdminUser[email]" maxlength="50" placeholder="请输入您的邮箱（例：xxx@163.com）" />
                                    <?php if($model->hasErrors('email')): ?>
                                        <span class="error-span"><?php echo $model->getErrors('email')[0];?></span>
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="details-column">
                                <label class="details-column-title">
                                    职位
                                </label>
                                <div class="details-column-content">
                                    <input type="text" value="<?php echo $model->job_title;?>" name="AdminUser[job_title]" maxlength="100" placeholder="请输入您的职位，以便管理员能够清晰识别" />
                                </div>
                            </div>

                			<div class="details-column">
                				
                				<div class="details-column-content">
                					<input type="submit" name="" value="保存" class="details-column-submit">
                				</div>
                			</div>
                        <?php ActiveForm::end(); ?>
            		</div>
            	</div>
            </div>
	</div>
    </div>
</section>
<script type="text/javascript">
		// 图片上传
	$('.filePrew').change(function () {
		var fil = this.files;
        for (var i = 0; i < fil.length; i++) {
            var reader = new FileReader();
		    reader.readAsDataURL(fil[i]);
		    reader.onload = function()
		    {
		       $('#headSculpture').attr('src',reader.result);
		    };
        }

	});

	<?php if(Yii::$app->session->hasFlash('message')):?>
    setTimeout(function () {
        alert('<?php echo Yii::$app->session->getFlash('message');?>');
    },200);
    <?php endif;?>
</script>
<?php
use yii\bootstrap\ActiveForm;
use backstage\assets\AppAsset;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;


$this->title = '企业信息修改';
$this->params['breadcrumbs'][] = ['label' => '企业信息', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
.modal-content {border: none;border-radius: 0px;box-shadow: inherit !important;}
.ck_role .checkbox{float:left;margin-right:10px;}
 .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">                    
            <div class="panel panel-default" style="padding-top:35px; padding-bottom: 130px;"> 
            <?php
            $form = ActiveForm::begin([
                'options' => [
                    'class' => 'form-horizontal',
                    'id' => 'form2',
                    'name' => 'form1',
                    'enctype' => 'multipart/form-data',
                ],
                'fieldConfig' => [
                    'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-sm-6\">{input}{error}</div>",
                    'labelOptions' => ['class' => 'col-sm-2 control-label'],
                ]
            ]);
            ?>
            <?php echo $form->field($model, 'company')
                    ->input('text',['placeholder'=>$model->getAttributeLabel('company')])
                    ->label($model->getAttributeLabel('商家名称').'<span class="c_red">*</span>');
            ?>
            <?php echo $form->field($model, 'contact')->input('text',['placeholder'=>$model->getAttributeLabel('contact')]);?>
            <?php echo $form->field($model, 'mobile')
                    ->input('text',['placeholder'=>$model->getAttributeLabel('mobile')])
                    ->label($model->getAttributeLabel('电话').'<span class="c_red">*</span>');
            ?>
            <?php echo $form->field($model, 'email')
                    ->input('text',['placeholder'=>$model->getAttributeLabel('email')])
                    ->label($model->getAttributeLabel('电子邮箱').'<span class="c_red">*</span>');
            ?>

             <?php echo $form->field($model, 'logo')->widget('manks\FileInput', [
                    'clientOptions'=>[
                        'server' => Url::to('../upload/upload?source=logo'),
                        'fileSingleSizeLimit'=>'2097152', //限制上传大小为2M
                    ]
                ]) ?>
              <?php echo $form->field($model, 'business_licence')->widget('manks\FileInput', [
                    'clientOptions'=>[
                        'server' => Url::to('../upload/upload?source=business_licence'),
                        'fileSingleSizeLimit'=>'2097152', //限制上传大小为2M
                    ]
                ]) ?>
                <?php echo $form->field($model, 'address')
                    ->input('text',['placeholder'=>$model->getAttributeLabel('address')])
                    ->label($model->getAttributeLabel('address').'<span class="c_red">*</span>');
                ?>
                
                <?php echo $form->field($model, 'company_desc')
                    ->input('text',['placeholder'=>$model->getAttributeLabel('company_desc')])
                    ->label($model->getAttributeLabel('company_desc').'<span class="c_red">*</span>');
                ?>

               <?= $form->field($model, 'class_id')->dropDownList(ArrayHelper::map($data['goodsClass'],'id','title'),['prompt'=>'请选择']) ?>

                <div class="form-group">
                <label class="col-sm-3"> </label>
                <div class="col-sm-9 text-right" style="margin-right:30px;">
                     <input type="submit" class="btn btn-success  mgr20" style=" color:#FFF;" value="提交" />
                     <a type="button" class="btn btn-green-line-min btnClose" href="/user/view-profile">返回</a>
                </div>
            </div>                                  
            <?php ActiveForm::end(); ?>
            </div>
	</div>
    </div>
</section>

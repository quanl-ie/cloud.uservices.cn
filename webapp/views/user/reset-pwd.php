<?php
use yii\helpers\Url;
$this->title = '重置密码';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    body{background: #fff;}
    .information-list{padding:0 30px;}
    .name-title-lable{left: 30px;}
</style>
        <div class="information-list">
            <label class="name-title-lable" for="userform-new_pwd"><span class="red-i">*</span>新密码：</label>
            <div class="addinput-list"><input type="password" id="userform-new_pwd" class="form-control" name="UserForm[new_pwd]" placeholder="新密码" aria-required="true"><p class="help-block help-block-error"></p></div>
        </div>
        <div class="information-list">
            <label class="name-title-lable" for="userform-verify_pwd"><span class="red-i">*</span>确认密码：</label></span>
            <div class="addinput-list"><input type="password" id="userform-verify_pwd" class="form-control" name="UserForm[verify_pwd]" placeholder="确认密码" aria-required="true"><p class="help-block help-block-error"></p></div>
        </div>
        <hr>
        <input type="hidden" id='user_id' value="<?php echo $id;?>">
        <div class="information-list text-center" >
            <a href="javascript:" class="btn bg-f7 mgr10" id="closeIframe">取消</a>
            <button type="submit" class="btn btn-orange" style="color:#fff;" id="conservation">保存</button>
        </div>
<script src="<?php echo Url::to('/js/jquery-1.8.3.min.js');?>"></script>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    }
    $('#conservation').on('click',function() {
          var pwd   = $("#userform-new_pwd").val();
          var repwd = $("#userform-verify_pwd").val();
          var uid   = $("#user_id").val();
          if(pwd == ''){
                alert("请输入新密码");
                $("#userform-new_pwd").focus();
                return false;
          }
          if(repwd == ''){
                alert("请再次输入新密码");
                $("userform-verify_pwd").focus();
                return false;
          }
          var reg = /^[\u4e00-\u9fa5_a-zA-Z0-9]{6,12}$/;
          if (!reg.test(pwd)){
                alert("密码格式不正确！");
                $("#userform-new_pwd").focus();
                return false;
          }
          if(pwd !== repwd){
                alert('<?php echo Yii::$app->session->getFlash('message',"密码不一致！"); ?>');
                return false;
          }
        $.post('/user/reset-pwd',{password:pwd,"user_id":uid},function(msg) {
            if(msg.success == true){
                  alert("编辑成功");
                  window.parent.location.href="/user/index";
              }
        })
    })
    $('#closeIframe').click(function(){
        parent.layer.close(parent.layer.index);
    });
</script>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\models\Region;
use common\models\Role;
$this->title = '修改用户信息';
$this->params['breadcrumbs'][] = ['label' => '用户管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<style>
    ul{margin-left: 20px;}
    .addinput-list{height: 56px;overflow: hidden;}
    .addinput-list>.Validform_wrong{position: static;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">用户管理</span>
</div>
<section id="main-content">
    <div class="panel-body" style="position: relative;">
        <div class="col-md-12">
            <h5 class="block-h5" style="padding-top: 0">修改用户</h5>
        </div>
        <?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-signin',
                'id' => 'form2',
                'name' => 'form1',
                'enctype' => 'multipart/form-data'
            ]
        ]);
        ?>
        <div class="information-list">
            <label class="name-title-lable">
                <i class="red-i">*</i> 姓名：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <input type="text" value="<?= $user_info['username']?>" name="AdminUser[username]" class="form-control" maxlength="10" placeholder="不超过10个字（建议使用真实姓名）"  sucmsg=" " nullmsg="请输入联系人姓名">
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
                <i class="red-i">*</i> 登录手机号：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <input type="text" value="<?= $user_info['mobile']?>" name="AdminUser[mobile]" class="form-control" placeholder="11位数字的手机号（登录时使用）" id="contact_mobile" datatype="/^[1][3,4,5,6,7,8,9][0-9]{9}$/" placeholder="请输入联系人电话" sucmsg=" " nullmsg="请输入联系人电话" errormsg="手机号格式错误" maxlength="11">
            </div>
        </div>
       <!--   <div class="information-list">
                <label class="name-title-lable">
                      <i class="red-i">*</i> 初始密码：
                </label>
                <span class="c_red"></span>
                <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                      <input type="password" value="" name="AdminUser[password]" class="form-control" minlength="6" maxlength="16" placeholder="6-16位数字/大小写字母/下划线" datatype="/^\s*$/|/^\w{6,16}$/" sucmsg=" " nullmsg="请输入密码" errormsg="密码格式错误" >
                </div>
          </div>-->
        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
                <i class="red-i">*</i> 所属机构：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <input type="text" class="form-control" value="<?= $department_name ?>"  readonly >
                <input type="hidden" id="select_department_id" value="<?= $user_info['department_id']?>" name="AdminUser[department_id]">
                <i class="icon iconfont icon-shouqi1"></i>
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
                职位：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <input type="text" value="<?= $user_info['job_title']?>" name="AdminUser[job_title]" class="form-control" maxlength="50" placeholder="请输入该员工的职位" >
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable">
                邮箱：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <input type="text" name="AdminUser[email]" value="<?= $user_info['email']?>" class="form-control" maxlength="50" placeholder="请正确输入邮箱格式" >
            </div>
        </div>
        <input type="hidden" value="<?php echo $department_id;?>" id="department_id">
        <div class="information-list">
            <label class="name-title-lable">
                <i class="red-i">*</i> 角色：
            </label>
            <span class="c_red"></span>
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list region-limit" style="overflow:visible; ">
                <input type="text" value="<?php if($select_roles) echo implode(',',$select_roles['name'])?>" data-value ="<?php if($select_roles) echo implode(',',$select_roles['name'])?>"  class="form-control" placeholder="请选择角色" readonly id="role_Select" onclick="roleSelect();">
                <input type="hidden" value="<?php if($select_roles) echo implode(',',$select_roles['ids']); ?>" name="AdminUser[role_ids]" id="role_Select_ids"  data-id = "<?php if($select_roles) echo implode(',',$select_roles['ids']); ?>">
                <i class="icon iconfont icon-shouqi1"  onclick="roleSelect();"></i>
                <p class="misplacement" style="bottom: -5px;" id="work_type_msg">请选择角色</p>
                <div class="drop-dw-rolebox" style="top: 35px;">
                    <ul class="many-role-ul" id="check" style="display: none;"><!--多选-->
                        <?php if(!empty($roles)):?>
                          <?php $role_ids = explode(",",$user_info['role_ids']);?>
                            <?php foreach($roles as $k=>$v):?>
                                <li>
                                    <input class="role" type="checkbox" <?php if(in_array($k,$role_ids)){ echo "checked='true'";} ?>name="roleMany" id="<?= $k;?>" data-id="<?= $k;?>" text="<?= $v;?>" value="<?= $k;?>">
                                    <label for="<?= $k;?>"><?= $v;?><i class="gou-i"></i></label>
                                </li>
                            <?php endforeach;?>
                        <?php endif;?>
                    </ul>
                    <ul class="many-role-ul" id="radio" style="display: none;"><!--单选-->
                        <?php if(!empty($roles)):?>
                            <?php $role_ids = explode(",",$user_info['role_ids']);?>
                            <?php foreach($roles as $k=>$v):?>
                                <li>
                                    <input class="role" type="radio" name="role" id="dx<?= $k;?>"  data-id="<?= $k;?>"  text="<?= $v;?>" value="<?= $k;?>" <?php if(in_array($k,$role_ids)){ echo "checked='true'";} ?> >
                                    <label for="dx<?= $k;?>"><?= $v;?><i class="gou-i"></i></label>
                                </li>
                            <?php endforeach;?>
                        <?php endif;?>

                    </ul>
                    <div  class="btn-li">
                        <a class="bg-f7"  href="javascript:" id="cancelRole">取消</a>
                        <a class="btn-orange"  href="javascript:" id="okRole">确定</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="information-list">
            <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                <a href="/user/index"><button type="button" class="btn bg-f7  mgr8">取消</button></a>
                <input type="submit" value="确认" id="btnSt" class="btn btn-orange">
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</section>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    $('#role').on('click',function() {
        $('.drop-dw-rolebox').is(':hidden')?$('.drop-dw-rolebox').show():$('.drop-dw-rolebox').hide();

    })

    function  clickCategory(el) {
        var id = $(el).next('input').val();
        var _this = $(el).parent('span');
        $.getJSON('/common/ajax-get-department',{'pid':id},function (data) {
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>"
                $(data.data).each(function(i,el) {
                    ulData +="<li><span>";
                    el.exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="clickCategory(this)"></i>':'';
                    ulData+='<input type="radio" value="'+el.id+'" name="department_id" id="'+el.id+'" class="pr1" onclick="changeCategory(this)"><label for="'+el.id+'">'+el.name+'</label></span></li>';
                });
                ulData +="</ul>";
                _this.parent('li').append(ulData);
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');

            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })

    }
    function changeCategory(el) {
        $('.drop-dw-layerbox').hide();
        $('#select_department_id').val($(el).next('label').attr("for"));
        $('#category1').val($(el).next('label').text());
    }

    $('#category1').on('click',function() {
        $('.drop-dw-rolebox').is(':hidden')?$('.drop-dw-rolebox').show():$('.drop-dw-rolebox').hide();
    })

    // 点击展示角色
    var select_id = $("#select_department_id").val();
    var self_id = '<?php echo $department_ids; ?>';
    function roleSelect() {
        // $("input[name='role']").removeAttr("checked");//取消选中
        // $("input[name='role']").prop('checked',false);
        if(select_id == ''){
            alert('请选择正确的机构');
            return false;
        }
          self_id_arr = self_id.split(",");
          if($.inArray(select_id, self_id_arr) > -1){
          　　$("#check").show();
                $("#radio").hide();
          }else{
            $("#radio").show();
            $("#check").hide();
        }
        $('.drop-dw-rolebox').is(':hidden')?$('.drop-dw-rolebox').show():$('.drop-dw-rolebox').hide();
    }
    var selectRole = '';
    //取消选择角色
    $('#cancelRole').on('click',function() {
        var cancelId = $('#role_Select_ids').attr('data-id');
          if($.inArray(select_id, self_id_arr) > -1){
            $('input[name="roleMany"]').prop('checked',false);
            cancelId = cancelId.split(',');
            $.each(cancelId,function(i,el) {
                $('#'+el).prop('checked',true);
            })
        }else{
            //单选
            $('input[name="role"]').prop('checked',false);
            $('#dx'+cancelId).prop('checked',true);
            selectRole = $('#dx'+cancelId).next('label').text();
            
            $('#role_Select_ids').val(cancelId);

        }
        $('#role_Select').val(selectRole);
        $('.drop-dw-rolebox').hide();
    })

    //确定选择角色
    $('#okRole').on('click',function() {
        selectRole = '';
        selectRoleIds = '';
          if($.inArray(select_id, self_id_arr) > -1){
            var bb = $("input[name='roleMany']");
            bb.each(function() {
                if ($(this).is(':checked')) {
                    selectRole +=$(this).attr('text')+';'
                    selectRoleIds +=$(this).attr('data-id')+','
                }
            });
        }else{
            var bb = $("input[name='role']");
            bb.each(function() {
                if ($(this).is(':checked')) {
                    selectRole +=$(this).attr('text')+';'
                    selectRoleIds +=$(this).attr('data-id')+','
                }
            });
        }
        if (selectRole!='') {
            selectRoleIds = selectRoleIds.substring(0,selectRoleIds.length - 1);
            selectRole = selectRole.substring(0,selectRole.length - 1);
            $('#role_Select').val(selectRole);
            $('#role_Select_ids').val(selectRoleIds);
            $('#role_Select_ids').attr('data-id',selectRoleIds);
            $('.drop-dw-rolebox').hide();
        }else{
            alert('请选择角色');
        }

    })
    //表单验证
    $("#form2").Validform({
        tiptype:3,
        datatype:{
            //"zh1-20":/^[\u4E00-\u9FA5\uf900-\ufa2d]{1,20}$/,
            "zh1a":/^[\u4e00-\u9fa5_a-zA-Z0-9 ]+$/, //只允许数字，字母，汉字，空格
        },
        beforeSubmit:function(curform){

            if($('#role_Select_ids').val() == ''){
                alert("请选择角色");
                return false;
            }
            $("#btnSt").prop("disabled", true);
        }
    });
    $(document).click(function(event) {
       var region = $('.region-limit');
       if (!region.is(event.target)&&region.has(event.target).length ===0) {
                $('.drop-dw-rolebox').hide();
       }
    });
</script>
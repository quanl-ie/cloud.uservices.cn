<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\widgets\Breadcrumbs;
use webapp\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
$v = isset(Yii::$app->params['v']) ? Yii::$app->params['v'] : "";
$departmentId = isset(Yii::$app->params['departmentId']) ? Yii::$app->params['departmentId'] : 0;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="/css/public.css?v=<?php echo $v; ?>">
    <link rel="stylesheet" href="/css/menu.css?v=<?php echo $v; ?>">
    <link rel="stylesheet" href="/css/iconfont.css?v=<?php echo $v; ?>">
    <link rel="stylesheet" href="/js/layer/theme/default/layer.css?v=<?php echo $v; ?>">
    <script src="/js/jquery-1.10.2.min.js?v=<?php echo $v; ?>"></script>
    <script src="/js/myfunc.js?v=<?php echo $v; ?>"></script>
    <script src="/js/main.js?v=<?php echo $v; ?>"></script>
    <script src="/js/layer/layer.js?v=<?php echo $v; ?>"></script>
    <title><?php echo isset(Yii::$app->params['manufactor']['company']) ? Yii::$app->params['manufactor']['company'] : ''; ?>
        售后管理系统</title>
    <style type="text/css">
        .ifm-tab-nav, .ifm-tab-nav > li {
            -webkit-transition: all .3s;
            -moz-transition: all .3s;
            -ms-transition: all .3s;
            -o-transition: all .3s;
            transition: all .3s;
        }
    </style>
</head>
<body style="overflow: hidden;">
<header id="header" data-id="<?php echo $departmentId; ?>"
        data-address="<?php echo Yii::$app->params['backurl']; ?>"
        data-backurl="<?php echo(Yii::$app->params['backurl'] == '' ? 1 : 0); ?>">
    <div class="header-left">
        <img class="logo" src="<?php if (!empty(Yii::$app->params['manufactor']['logo'])) {
            echo Yii::$app->params['manufactor']['logo'];
        } else {
            echo Url::to('@web/img/logo1.png');
        } ?>"" alt="">
        <h5 class="header-title"><?php echo isset(Yii::$app->params['manufactor']['company']) ? Yii::$app->params['manufactor']['company'] : ''; ?>
            售后管理系统</h5>
    </div>
    <div class="header-right">
        <div id="personal">
            <img class="touxiang" src="<?php echo Yii::$app->params['avatar']; ?>" alt=""/>
            <p><span><?php echo Yii::$app->params['username']; ?></span><i class="icon2-shixinxia"></i></p>
            <ul>
                <li class="link-nav" data-src="/user/edit-profile"><i class="icon2-zhanghu"></i><a href="javascript:">账户信息</a>
                </li>
                <li class="link-nav" data-src="/user/modify-password"><i class="icon2-quanxian"></i><a
                            href="javascript:">修改密码</a></li>
                <!--<li class="link-nav" data-src="/technician/add"><i class="icon2-lingdang"></i><a href="javascript:">系统消息</a></li>-->
                <li class="margin-top60" id="logOff" style="position: absolute;bottom: 10px;"><i
                            style="padding-left: 12px"></i><a href="javascript:">退出登录</a></li>
            </ul>
        </div>
        <!-- 预留以后去快捷用 -->
        <ul class="herder-nav-right">
            <li class="link-nav" data-src="/order/add"><a href="javascript:">创建订单</a></li>
            <li class="link-nav" data-src="/order/wait-assign"><a href="javascript:">指派订单</a></li>
            <li class="link-nav" data-src="/account/index"><a href="javascript:">添加客户</a></li>
        </ul>

    </div>
</header>
<nav id="navigation">
    <ul class="top-nav-box">
        <li class="left-nav-list link-nav" data-src='/index'>
            <a href="javascript:">
                <i class="icon2-shouye"></i>
                首页
            </a>
        </li>
        <?php foreach (Yii::$app->params['menu'] as $val): ?>
            <?php if ($val['url'] != '/sys/index'): ?>
                <li class="left-nav-list">
                    <a href="javascript:">
                        <i class="<?php echo $val['class'] == '' ? 'icons-home' : $val['class']; ?>"></i>
                        <?php echo $val['label'] ?>
                    </a>
                    <?php if (isset($val['items']) && $val['items']): ?>
                        <ul class="hover-nav">
                            <?php foreach ($val['items'] as $v): ?>
                                <li class="link-nav"
                                    data-src="<?php echo trim($v['url']) == '#' ? '/user/no-authority' : $v['url']; ?>">
                                    <a href="javascript:"><?php echo $v['label']; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
    <?php foreach (Yii::$app->params['menu'] as $val): ?>
        <?php if ($val['url'] == '/sys/index'): ?>
            <div class="left-nav-list">
                <li class="link-nav left-nav-list" data-src='/sys/index'>
                    <a href="javascript:">
                        <i class="icon2-shangpin2"></i>
                        设置
                    </a>
                </li>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
</nav>
<section id="content">
    <div class="top-nav-box" id="tabNavBox">
        <div class="page-left" id="navPageLeft"></div>
        <div class="page-right" id="navPageRight"></div>
        <ul class="ifm-tab-nav" id="ifrmTabNav">
            <li data-src='/index' id="index-nav-li"><span>主页</span></li>
            <?php /*if (Yii::$app->params['backurl'] != ''): */?><!--
                <li data-src='<?php /*echo Yii::$app->params['backurl']; */?>' class="active"></li>
            --><?php /*endif; */?>
        </ul>
    </div>
    <div class="abc">
        <!-- <iframe src="/contract/edit?id=47" frameborder="0"></iframe> -->
    </div>
    <div class="ifrm-box" id="ifrmBox">
        <div id="index-page-div"
             class="ifrmbody-list <?php if (Yii::$app->params['backurl'] == ''): ?>ifrmbody-show<?php endif; ?>"
             data-src="/index">
            <iframe id="aa" src="/index/index" frameborder="0"></iframe>
        </div>
       <!-- <?php /*if (Yii::$app->params['backurl'] != ''): */?>
            <div class="ifrmbody-list ifrmbody-show" data-src="<?php /*echo Yii::$app->params['backurl']; */?>">
                <iframe id="aa" src="<?php /*echo Yii::$app->params['backurl']; */?>" frameborder="0"></iframe>
            </div>
        --><?php /*endif; */?>
    </div>
</section>
</body>
</html>
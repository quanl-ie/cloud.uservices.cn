<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\widgets\Breadcrumbs;
use webapp\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
AppAsset::register($this);
$v = isset(Yii::$app->params['v'])?Yii::$app->params['v']:"";
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name=”renderer” content=”webkit”>
    <meta http-equiv=”X-UA-Compatible” content=”IE=edge,chrome=1”/>
    <meta name="renderer" content="webkit">
    <title><?php echo  Html::encode($this->title) ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="/plugins/bootstrap/css/bootstrap.min.css?v=<?php echo $v;?>">
    <link rel="stylesheet" href="/public/css/bootstrap-datetimepicker.css?v=<?php echo $v;?>">

    <!-- CSS Animate -->
    <link rel="stylesheet" href="/css/animate.css?v=<?php echo $v;?>">
    <!-- Custom styles for this theme -->
    <link rel="stylesheet" href="/css/main.css??v=<?php echo time();?>">
    <!-- ToDos  -->
    <link rel="stylesheet" href="/plugins/todo/css/todos.css?v=<?php echo $v;?>">
    <!--自己新增样式-->
    <link rel="stylesheet" href="/public/css/mystyle.css?v=<?php echo $v;?>">
    <!-- jedate时间插件     -->
    <link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jedate.css');?>?v=<?php echo time()?>">
    <!-- 新增icon库     -->
    <!-- <link rel="stylesheet" href="/public/myfonts/css/fonts.css?v=<?php echo $v;?>"> -->
    <link rel="stylesheet" href="/css/iconfont.css?v=<?php echo $v;?>">
    <script src="/js/jedate.js"></script>

    <!--Global JS-->
    <script src="/js/jquery-1.10.2.min.js?v=<?php echo $v;?>"></script>
    <script src="/js/modernizr-2.6.2.min.js?v=<?php echo $v;?>"></script>
    <!-- 日期插件-->
    <script src="/js/datetimepicker/bootstrap-datetimepicker.js?v=<?php echo $v;?>"></script>
    <script src="/js/datetimepicker/bootstrap-datetimepicker.zh-CN.js?v=<?php echo $v;?>"></script>
    <script src="/plugins/bootstrap/js/bootstrap.min.js?v=<?php echo $v;?>"></script>
    <script src="/plugins/waypoints/waypoints.min.js?v=<?php echo $v;?>"></script>
    <script src="/js/application.js?v=<?php echo $v;?>"></script>
    <script src="/plugins/todo/js/todos.js?v=<?php echo $v;?>"></script>
    <script src="/js/myfunc.js?v=<?php echo $v;?>"></script>
    <script src="/js/My97DatePicker/WdatePicker.js?v=<?php echo $v;?>"></script>
    <script src="/js/layer/layer.js?v=<?php echo $v;?>"></script>
    <script>
    $(function(){
        lnitHeight ();
        fontSize ();
    })
    $(window).resize(function (){
        lnitHeight ();
        fontSize ();
    });
    function lnitHeight () {
        $('.tooltips').tooltip();
        var height = $(window).height();
        $('.sidebar').css('height',height-60)
        setNoActiveMenu();
    }
    function fontSize () {
        var whdef = 100/1920;
         var wH = window.innerHeight;
         var wW = window.innerWidth;
         var rem = wW * whdef;
         $('html').css('font-size', rem + "px");
    }
    // 字数限制
    function wordLength(el) {
        if($(el).val().length > 200){
            $(el).val( $(el).val().substring(0,200) );
        }
            $(el).next("p").find('#word').text( $(el).val().length ) ;
    }
    function wordsLimit(el) {
        if($(el).val().length > 200){
            $(el).val( $(el).val().substring(0,200) );
        }
            $(el).next("p").find('#word').text( $(el).val().length ) ;
    }
    </script>
    <style>
        .main-content-wrapper{padding: 0px;margin: 0px;}
    </style>
    <?php $this->head() ?>
</head>
<body>
    <section id="container">
        <section class="main-content-wrapper">
            <?php $this->beginBody() ?>
            <?php echo $content;?>
            <?php $this->endBody() ?>
        </section>
    </section>
<!-- 消息推送js 不能删除！！！！ -->
<script>
 //关闭后将此消息设置为忽略不在提醒
function setNoticeStatus(msg_id){
	 $.post("/message/set-msg-notice-status", {"msg_id": msg_id}, function (data) {
		 var res = $.parseJSON(data);
		 if(res.success){
			 }
		 });
};
//点击查看后将此消息设置为已读状态
function setReadStatus(msg_id){
	 $.post("/message/set-msg-read-status", {"msg_id": msg_id}, function (data) {
		 var res = $.parseJSON(data);
		 if(res.success){
			 //console.log(msg_id)
			 //alert(msg_id);
			 }
		 });
};
$('.sub-menu .a-li').on('click',function() {//.pull-right
    $(this).next('.second-menu').is(':hidden')?$(this).find('.pull-right').addClass('icon-up'):$(this).find('.pull-right').removeClass('icon-up');
})
    if(top.location == self.location){
         //document.location.href = "/index/main?backurl="+ encodeURIComponent(self.location);
    }
</script>
</body>
</html>
<?php $this->endPage() ?>

<?php
use yii\helpers\Html;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="no-js">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo  Html::encode($this->title) ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/public/css/bootstrap-datetimepicker.css">
    <!-- CSS Animate -->
    <link rel="stylesheet" href="/css/animate.css">
    <!-- Custom styles for this theme -->
    <link rel="stylesheet" href="css/main.css">
    <!-- ToDos  -->
    <link rel="stylesheet" href="/plugins/todo/css/todos.css">
    <!--自己新增样式-->
    <link rel="stylesheet" href="/public/css/mystyle.css">
	<!-- 新增icon库     -->
    <link rel="stylesheet" href="/public/myfonts/css/fonts.css"> 
    
    <script src="/js/jquery-1.10.2.min.js"></script>
    <script src="/js/modernizr-2.6.2.min.js"></script> 
    
    <!-- 日期插件-->
    <script src="/js/datetimepicker/bootstrap-datetimepicker.js"></script>
    <script src="/js/datetimepicker/bootstrap-datetimepicker.zh-CN.js"></script>

    <script src="/js/My97DatePicker/WdatePicker.js"></script>
    <?php $this->head() ?>
</head>

<body style="background:#FFFFFF;">
<?php $this->beginBody() ?>
    	<?php echo $content;?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
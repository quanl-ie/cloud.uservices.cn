<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = '排班管理';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jedate.css');?>?v=<?php echo time()?>">
<script src="/js/jedate.js"></script>
<style>
    .tb-width-100 td{cursor: pointer;}
    .tb-width-100 td:hover{background: #f8f8f8;}
    .xian{margin:10px 0;border-color: #dfdfdf;}
    .drop-dw-layerbox{width: calc(100% - 86px)}
    .drop-dw-layerbox>button{width: 100%;position: absolute;left: 0;bottom: 0;}
    .tb-width-100 td{position: static;}
    .tb-width-100 td>div{width: 100%;height: 100%;position: relative;display: table;line-height: 37px;}
    td{height: 39px;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">排班管理
    <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="点击排班日历表可对技师班次进行编辑"></span>
    </span>
    <div class="right-btn-box">
        <?php if(in_array('/schedule-set/index',$selfRoles)){?>
            <a  href="/schedule-set/index"><button class="btn btn-orange">班次设置</button></a>
        <?php };?>
        <?php if(in_array('/schedule-set-option/index',$selfRoles)){?>
            <a  href="/schedule-set-option/index"><button class="btn btn-orange">批量排班</button></a>
        <?php };?>
        <?php if(in_array('/schedule-manage/export-data',$selfRoles)){?>
            <button class="btn btn-orange daochu">导出</button>
        <?php };?>
    </div>
</div>
<section id="main-content">
    <div class="panel-body padding0">
        <form action="/schedule-manage/index" class="form-horizontal form-border" id="form">
        <!-- 搜索栏 -->
        <div class="col-sm-12 no-padding-left margin-top20">
            <div class="single-search">
                <label class=" search-box-lable">选择月份</label>
                <div class="single-search-kuang1">
                    <input type="hidden" value="<?= isset($_GET['date']) ? $_GET['date'] : '' ?>">
                    <input type="text" class="form-control" id="month"  name="date" value="<?= isset($_GET['date']) ? $_GET['date'] : '' ?>">
                </div>
            </div>

            <?php if(!empty($department)):?>
                <div class="single-search region-limit" style="overflow:visible;">
                    <label class=" search-box-lable">所属机构</label>
                    <input type="hidden" id="department_id" name="department_id" value="<?php echo $department_id;?>">
                    <div class="single-search-kuang1" id="de">
                        <input type="text" class="form-control" value="<?=isset($departpment_name) ? $departpment_name : '' ?>" id="department"  readonly <?php if(!empty($department)){ echo 'onclick="clickCategory()" placeholder="请选择机构"';}?> >
                        <input type="hidden" class="form-control" value="<?=isset($params['department_id']) ? $params['department_id'] : '' ?>" id="department_ids" name="department_ids">
                        <div class="drop-dw-layerbox" style="display: none;">
                            <div class="ul-box-h180">
                                <ul id="depart1" class="depart">
                                    <?php if (!empty($department)) : ?>
                                        <?php foreach ($department as $key => $val) : ?>
                                            <li>
                                                 <span>
                                                    <?php if ($val['exist'] == 1) : ?>
                                                        <i class="icon2-shixinyou" onclick="getDepartment(this)"></i>
                                                    <?php endif;?>
                                                     <input type="checkbox" id="a<?=$val['id'] ?>" value="<?=$val['id'] ?>" name="department_id[]" <?php if(isset($params['department_id']) && (in_array($val['id'],explode(",",$params['department_id'])))){ echo "checked=true"; }?>>
                                                     <label for="a<?=$val['id'] ?>"><?=$val['name'] ?><i class="gou-i"></i></label>
                                                </span>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <button class="btn btn-orange" id="belongedBtn">确定</button>
                        </div>

                    </div>

                </div>
            <?php endif?>

            <div class="single-search">
                <label class=" search-box-lable">技师分组</label>
                <div class="single-search-kuang1">
                    <select class="form-control" onchange="getTech(this)" name="grouping_id" id="grouping_id">
                        <option value="">请选择机构</option>
                    </select>
                </div>
            </div>
            <div class="single-search">
                <label class=" search-box-lable">技师</label>
                <div class="single-search-kuang1">
                    <select class="form-control" name="tech_id" id="tech_id">
                        <option value="">请选择分组</option>
              
                    </select>
                </div>
            </div>
            <div class="search-confirm">   
                 <button class="btn btn-success"><i class=" "></i> 搜索</button>
            </div>
        </div>
        <hr>
        <?php if (!empty($data)) : ?>
        <div class="loading-img">
            <div>
                <div class="loading-bg">
                    
                </div>
            </div>
        </div>
        <!-- 排班日历 -->
        <div class="pb-general-box" style="display: none;">
            <!-- 固定 -->
            <table  class="pt-left">
                <tr>
                    <th width="106px" height="99px">技师</th>
                    <th width="106px" height="99px">所属机构</th>
                    <th width="106px" height="99px">组别</th>
                    <th width="106px" height="99px">职位</th>
                </tr>
                <tbody style="background: #fff;">
                    <?php foreach ($data as $key => $val) : ?>
                        <tr>
                            <td><?=isset($val['name']) ? $val['name'] : '' ?></td>
                            <td><?=isset($val['store_name']) ? $val['store_name'] : '' ?></td>
                            <td><?=isset($val['grouping_name']) ? $val['grouping_name'] : '' ?></td>
                            <td><?=isset($val['job_name']) ? $val['job_name'] : '' ?></td>
                        </tr>
                    <?php endforeach; ?>
                
                </tbody>
            </table>
            <div class="pb-calendar-dbox">
                <div class="pb-calendar-box" id="calendarBox">
                    <!-- 内容 -->
                    <table class="tb-width-100" style="width: auto;">
                        <thead>
                            <tr class="title" id="dataScheduling">
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($data as $key => $val) : ?>
                                <tr>
                                    <td><?=isset($val['name']) ? $val['name'] : '' ?></td>
                                    <td><?=isset($val['store_name']) ? $val['store_name'] : '' ?></td>
                                    <td><?=isset($val['grouping_name']) ? $val['grouping_name'] : '' ?></td>
                                    <td><?=isset($val['job_name']) ? $val['job_name'] : '' ?></td>
                                    <?php foreach ($val['child'] as $k => $v) : ?>
                                    <?php if ($v['schedule_set_name'] != '休息') : ?>
                                        <td style="background: <?=$v['color'] ?>" tec-id="<?=$val['id'] ?>" data-shift = '<?=$v['schedule_set_id'] ?>' data-time='<?=$v['date'] ?>' onclick="modifyShift(this)">
                                            <div>
                                                <?=$v['schedule_set_name'] ?>
                                                <?php if (!empty($v['description'])) : ?>
                                                <i class="tip icon2-bangzhu" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="<?=$v['description'] ?>"></i>
                                                <?php endif; ?>
                                            </div>
                                        </td>
                                        <?php else: ?>
                                            <td style="color: red" tec-id="<?=$val['id'] ?>" data-shift = '<?=$v['schedule_set_id'] ?>' data-time='<?=$v['date'] ?>' onclick="modifyShift(this)">
                                               <div>
                                                <?=$v['schedule_set_name'] ?>
                                                <?php if (!empty($v['description'])) : ?>
                                                   
                                                        <i class="tip icon2-bangzhu" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="<?=$v['description'] ?>"></i>
                                                    </td>
                                                <?php endif; ?>
                                                </div> 
                                            </td>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </tr>
                            <?php endforeach; ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </form>
        <div class="col-xs-12 text-center pagination">
            <?php echo isset($pageHtml)?$pageHtml:'';?>
        </div>
        <?php else: ?>
            <div class="no-data-page">
                <div>
                    <div class="kbtp"></div>
                    <p class="no-data-zi">所选条件下暂无技师</p>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
<script>
    var curDate, nian,curMonth,yue,ri;//获取当月多少天
    $(function() {
        
        <?php if(Yii::$app->session->hasFlash('message')):?>
        setTimeout(function () {
            alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        },500);
        <?php endif;?>
		
		if($('#month').val().length==0){
			var tmpDate = new Date();
			ri = (new Date(tmpDate.getYear(),(tmpDate.getMonth()+1),0)).getDate();
		}
		else {
			var yearMonth = $('#month').val();
			var yearMonthArr = yearMonth.split('-');
			var d = new Date(yearMonthArr[0],yearMonthArr[1],0);
			ri = d.getDate();
		}
        
        $('#month').val().length==0?curDate = new Date():curDate = new Date(($('#month').val()+'-22').replace("-", "/").replace("-", "/"));
        /* 获取当前月份 */
        nian = curDate.getFullYear();
        curMonth = curDate.getMonth();
        yue = curMonth+1;
        
        //搜索框赋值
        if ($('#month').val() == '') {
            if (yue < 10) {
                $('#month').val(nian+'-'+'0'+yue);
            }else{
                $('#month').val(nian+'-'+yue);
            }
        }
        
        curDate.setMonth(curMonth + 1);
        curDate.setDate(0);
        
        $(".tip").popover();
        $('.pb-calendar-box').css('width',ri*139+'px');
        $('.loading-img').hide();
        $('.pb-general-box').show(100);
        $('.pagination').show(100);
        ergodicDate();
    });
  // 计算当月有几天
    function ergodicDate(){
        //var thDate = `<th>技师</th><th>组别</th><th>职位</th>`;
        var thDate = "<th>技师</th><th>所属机构</th><th>组别</th><th>职位</th>";
        for (var i = 1; i < ri+1; i++) {
            var cc = yue+'-'+i
            var w1 = getMyDay(new Date((nian+"-"+yue+"-"+i).replace("-", "/").replace("-", "/")).getDay());
            thDate += "<th>"+yue+'月'+i+"日<p class='xian'></p>"+w1+"</th>";
        };
        $('#dataScheduling').html(thDate);
    };
    function getMyDay(date){
        var week;
        if(date==0)          week="周日"
        if(date==1)          week="周一"
        if(date==2)          week="周二"
        if(date==3)          week="周三"
        if(date==4)          week="周四"
        if(date==5)          week="周五"
        if(date==6)          week="周六"
        return week;
    };
    // 修改排班
    var editArray ='';
    function modifyShift(el){

        $.getJSON('/schedule-set-option/set-tech-schedule',{time:$(el).attr('data-time')},function (data) {
            
            if (data.success == false) {
                alert(data.message);
                return false;
            }
            editArray = {
                tecId:$(el).attr('tec-id'),
                shift:$(el).attr('data-shift'),
                time:$(el).attr('data-time'),
                note:$(el).find('i').attr('data-content')
            }
            layerIndex = layer.open({
                type: 2,
                title: '修改班次',
                area: ['521px', '300px'],
                fixed: true,
                maxmin: false,
                content: '/schedule-set-option/get-tech-schedule'
            });
            
        });
    };
    // 
    jeDate('#month',{
        format:'YYYY-MM'
    });

    //导出
    $('.daochu').on('click',function() {
        var Etime = $('#month').val();
        var EGroup= $('#grouping_id option:selected') .val();//选中的值
        var ETech = $('#tech_id option:selected') .val();//选中的值
        var EMark = '1';
        var url ='/schedule-manage/export-data?export='+EMark+'&date='+Etime+'&grouping_id='+EGroup+'&tech_id='+ETech;
        setTimeout(function() {
            window.location.href=url;
        },300);
    });

    function getTech(obj) {
        $.getJSON('/schedule-set-option/get-tech',{id:obj.value},function (data) {
            var html = "<option value='' >请选择</option>";
            if (data.code == 200) {
                for (i in data.data) {
                    html+='<option value="'+i+'}" >'+data.data[i]+'</option>';
                };
                $('#tech_id').html(html);
            }
        })
    }


    var departmentIdArray = $('#department_ids').val().split(',');
    function getDepartment(el) {
        var id = $(el).next('input').val();
        var _this = $(el).parent('span');
        var auth = <?php echo $auth;?>;
        var company_id = <?php echo $company_id;?>;
        var self_id = $("#department_id").val();
        $.getJSON('/common/ajax-get-department',{'pid':id,"auth":auth,'self_department_id':self_id,"company_id":company_id},function (data) {
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>"
                $(data.data).each(function(i,el) {
                    ulData +="<li><span>";
                    el.exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="getDepartment(this)"></i>':'';
                    ulData+='<input type="checkbox" id="a'+el.id+'" value="'+el.id+'" name="department_id[]" >  <label for="a'+el.id+'">'+el.name+'<i class="gou-i"></i></label></span></li>';
                });
                ulData +="</ul>";
                _this.parent('li').append(ulData);
                $.each(departmentIdArray,function(i,el) {
                    $('#a'+el).prop('checked',true);
                })
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');

            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })
    }

    function  clickCategory(el) {
        //event.stopPropagation();
        $.each(departmentIdArray,function(i,el) {
            $('#a'+el).prop('checked',true);
        })
        $('.drop-dw-layerbox').is(':hidden')? $('.drop-dw-layerbox').show(): $('.drop-dw-layerbox').hide();
    }

    // 选中内容
    $('#belongedBtn').bind('click', function() {
        //event.preventDefault();
        var id_array= [];
        var data_array=[];
        $('input[name="department_id[]"]:checked').each(function(){
            data_array.push($(this).next('label').text());
            id_array.push($(this).val());//向数组中添加元素
        });
        var idstr=id_array.join(',');//将数组元素连接起来以构建一个字符串
        var datatr = data_array.join('；')
        $('#department').val(datatr);
        $('#department_ids').val(idstr);
        $('.drop-dw-layerbox').hide();

        //动态获取分组
        $.getJSON('/schedule-manage/get-group',{department_ids:idstr},function (data) {
            var html = "<option value='' >请选择</option>";
            if (data.code == 200) {
                for (i in data.data) {
                    html+='<option value="'+i+'" >'+data.data[i]+'</option>';
                };
                $('#grouping_id').html(html);
                $('#tech_id').html("<option value='' >请选择分组</option>");
            }
        })
        
        return false;
    });

    $(document).click(function(event) {
        var region = $('.region-limit');
        if (!region.is(event.target)&&region.has(event.target).length ===0) {
            $('.drop-dw-layerbox').hide();
        }
    });
</script>
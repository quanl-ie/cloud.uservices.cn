<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;

$this->title = '服务产品统计';
$this->params['breadcrumbs'][] = $this->title;
?>

<?=Html::jsFile('@web/echarts/echarts.min.js')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/reportForm.css');?>">
<?=Html::jsFile('@web/echarts/echarts.min.js')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/reportForm.css');?>">
<!-- 时间插件引入元素 start -->
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/normalize.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/default.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/daterangepicker.css');?>">
<?=Html::jsFile('@web/js/daterangepickers/moment.min.js')?>
<?=Html::jsFile('@web/js/daterangepickers/jquery.daterangepicker.js')?>
<style type="text/css">
body{
    font-size: 14px;
}
    .btn-success{background: #2693ff;border-color: #2693ff;}
    .btn-success:hover{background: #2693ff;border-color: #2693ff;}
    .pagination{ text-align: center;}
    #xiala{position: absolute;right: 8px;top: 4px;}
    .brand-box{
        /*min-height: 58px;*/
    }
    .drop-down-ul{z-index: 9;}
    .search-ok{padding: 15px;}
    .dingwei{
        position: absolute;
        top: 16px;
        z-index: 99;
    }
    .brand-ul>li{line-height: 1.5}
</style>
<!-- 时间插件引入元素 end -->
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title"><?= Html::encode($this->title) ?></span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                <form action="/statistics/service-product?class_type=" method="post" class="form-horizontal form-border" id="form">
                    <div class="time_pick">
                             <input id="date-range102" size="30" value="<?php echo $params['start_time'].'至'.$params['end_time'];?>" readonly>
                             <input type="hidden" id="start_time" name="start_time" value="<?php echo $params['start_time'];?>">
                             <input type="hidden" id="end_time" name="end_time" value="<?php echo $params['end_time'];?>">
                             <i  class="icon iconfont icon-shouqi1"></i>
                     </div>
                    <!-- 产品分类显示 -->
                    <div class="">
                        <ul id="tabs" class="nav nav-tabs">
                            <li id="li_bus_msg" name="class_type" class="switch <?php echo $params['class_type']==''?' active':'';?>" value="1">
                                <a href="/statistics/service-product?class_type="  aria-expanded="">一级类目统计</a>
                            </li>
                            <li id="li_sys_msg" name="class_type" class="switch <?php echo $params['class_type']==2?' active':'';?>" value="2">
                                <a href="/statistics/service-product?class_type=2" aria-expanded="">二级类目统计</a>
                                <input type="hidden" name="class_type" id="class_type" value="<?php echo $params['class_type'];?>">
                            </li>
                        </ul>
                     </div>
                     
                    <div class="pinpai">
                        <span class="left-tit-cp">产品</span>
                          <!-- 显示分类多选项目 -->
                            <?php if($params['class_type']==2){?>
                            <div class="dingwei">
                                <div class="one-drop-down">
                                    <input type="text" name="" class="one-drop-input" id="onedrop" placeholder="" readonly="readonly" value="<?php if(!empty($class_type1) && !empty($params['class_p_id'])){echo $class_type1[$params['class_p_id']];}?>">
                                    <i class="icon iconfont icon-shouqi1" id="xiala"></i>
                                    <ul class="drop-down-ul">
                                        <?php if(!empty($class_type1)) foreach ($class_type1 as $key=>$val){?>
                                            <li class="<?php if($key == $params['class_p_id']){echo "selected-radio-li";}?>" >
                                                <input type="radio" id="<?php echo $key;?>" name="type" value="<?php echo $key;?>" <?php if($key == $params['class_p_id']){echo "checked='checked'";}?>/>
                                                <label style="font-weight: 100;font-size: 14px" for="<?php echo $key;?>"><?php echo $val;?></label>
                                            </li>
                                        <?php }?>
                                    </ul>
                                </div>
                            </div>
                            <?php }?>
                        <div class="class_type_list" id="class_type_list">
                            <ul class="brand-ul" id="brand-ul">
                                <?php if($params['class_type']==2){?>
                                <li class="checkbox-span" style="height: 20px;"></li>
                                <?php }?>

                                <?php if(!empty($class_list)) foreach ($class_list as $key=>$val){?>
                                    <li class="checkbox-span">
                                        <input type="checkbox" class="products_id" name="products_id[]" id="<?php echo $key;?>" value="<?php echo $key;?>" data ="<?php echo $params['products_id'];?>" <?php if(in_array($key,explode(',',$params['products_id']))){ echo "checked";}?>>
                                           <label  class="brand-label" for="<?php echo $key;?>" style="font-weight: 100;"><i class="abcdef"></i>
                                               <?php echo $val?>
                                           </label>
                                    </li>
                                    <?php }?> 
                            </ul>
                            <span class="shrink">展开<i class="icon iconfont icon-shouqi1"></i></span>
                            <span class="stop">收起<i class="icon iconfont icon-shouqi"></i></span>
                            <div class="search-ok">
                                <span>提示：最多选择5个一级类目</span>
                                <button class="search-ok-btn">确认选择</button>
                            </div>
                        </div>
                    </div>
                    <!-- 售后订单盒子 -->
                    <div class="cus-ser-box">
                        <!-- 报表显示，为 ECharts 准备一个具备大小（宽高）的 DOM -->
                       <div id="main" style="margin-right:200px;height:400px;"></div>
                       <!-- 一定时间段内的总数据分类数据占比 -->
                       <div class="cus-ser-bili">
                            <div class="aaaa">
                            <ul class="cus-ser-ul">
                            <?php if(!empty($type_total)) foreach ($type_total as $key=>$val){?>
                                <li><span><?php echo $val['name'].'&nbsp&nbsp';?></span><span>(<?php echo $val['total'].'&nbsp&nbsp'.$val['percent']?>)</span></li>
                            <?php }?>
                            </ul>
                            </div>
                       </div>
                    </div>
                   <!-- 列表显示 -->
                   <div class="table-responsive" style="margin-top: 40px;">
                    <table class="table table-bordered table-striped table-hover">
                        <!-- 导出数据按钮 -->
                        <div class="report-bottom-title">
                            <p style="float: left;" class="reportForm-title">数据报表</p>
                            <div class="btn btn-success daochu" id="btn-export"><i class="icon iconfont icon-daochushuju"></i> 导出数据</div>
                            <input type="hidden" name="export" id="export" class="" data="1">
                        </div>
                        <thead bgcolor="#455971">
                            <tr>
                                <th>日期</th>
                                <?php if(!empty($products_info)) foreach ($products_info as $key=>$val){?>
                                <th><?php echo $val['name']?></th>
                                <th><?php echo $val['name'].'占比'?></th>
                                <?php }?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($data_list):?>
                            <?php foreach ($data_list as $key=>$val){?>
                                <tr>
                                    <td><?php echo $key; ?></td>
                                    <?php foreach ($products_info as $k => $v):?>
                                        <td><?php echo $val['data'][$k]['numb'];?></td>
                                        <td><?php echo $val['data'][$k]['percent'];?></td>
                                    <?php endforeach;?>
                                </tr>
                            <?php }?>
                            <?php else:?>
                                <tr>
                                    <tr><td colspan="11" class="no-record">暂无数据</td></tr>
                                </tr>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
                   <!-- 分页 --> 
                    <div class="col-xs-12 text-center  pagination">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    $(function(){
        $('#onedrop').val()==""?$('#onedrop').val($('.drop-down-ul li label').eq(0).text()): $('#onedrop').val();
        
    })
        // 样式js
    var checkboxOk = "";
    $('.shrink').on('click',function () {
        $(this).hide();
        $('.stop').show();
        $('.class_type_list').css('height','auto');
        })
    $('.stop').on('click',function () {
        $(this).hide();
        $('.shrink').show();
        $('.class_type_list').css('height','56px');
    })
     $('.products_id').change(function(){
        if ($("input[type=checkbox]:checked").length > 5 ) {
            alert("最多能选5个");
            $(this).attr('checked',false);
        }
        checkboxOk = $("input[type=checkbox]:checked");
    });
// ===================
    // 点击样式
        var jobContent = $('.drop-down-ul');
        $('#onedrop').on('click',function() {
          if (jobContent.is(":hidden")){
               $('.drop-down-ul').show();
               $('#xiala').removeClass('icon-shouqi1');
               $('#xiala').addClass('icon-shouqi');
          }else{
               $('.drop-down-ul').hide();
               $('#xiala').removeClass('icon-shouqi');
               $('#xiala').addClass('icon-shouqi1');
          }
          
        })
       $('.drop-down-ul li input').on('click',function() {
            $('.drop-down-ul li').removeClass('selected-radio-li');
            $(this).parent('li').addClass('selected-radio-li');
            $('#onedrop').val($(this).next('label').text());
            $('#xiala').removeClass('icon-shouqi');
            $('#xiala').addClass('icon-shouqi1');
            $('.drop-down-ul').hide();
            ////切换到二级类目时，根据产品类型一级获取二级数据
            var p_id = $(this).val();
            var odiv = $("#brand-ul");
            var str = '';
            var ccc = [];
            var class_type = $("#class_type").val();
        	$.post("/statistics/get-classes/", {"pid": p_id}, function (data) {
        		var res = $.parseJSON(data);
        		if(res){
            			odiv.html('');
            			$.each(res, function(i) {
            				var id = i;
            				var name = res[i];
            				str = `<li class="checkbox-span">
                                    <input type="checkbox" class="products_id" name="products_id[]" id="${id}" value="${id}" data ="<?php echo $params['products_id'];?>" <?php if(in_array($key,explode(',',$params['products_id']))){ echo "checked";}?>>
                                       <label  class="brand-label" for="${id}" style="font-weight: 100;"><i class="abcdef"></i>
									   		${name}
                                       </label>
                                	</li>`;
                        			// odiv.append(str);
                                    ccc.push(str);
            			});
                        cc =  (ccc.join(',')).replace(/,/g,"");
                        for (var i = 0.,len=ccc.length ; i <len; i++) {
                            var ddd = ccc.slice(i)
                        };
                        odiv.append('<li class="checkbox-span" style="height: 20px;"></li>'+cc);
        			}
            });
            //alert(pid);

       })

    // ======================================
    function get_show_data(){
    	var start_time = $("#start_time").val();
        var end_time = $("#end_time").val();
    	var class_type = $("#class_type").val();
		var products_ids = [];
    	$("input[name='products_id[]']").each(function(){
                if($(this).attr("checked")){
                    a = $(this).val();
                    products_ids.push(a)
                }
            });
        //console.log(products_ids);
    	$.post("/statistics/service-product", {"products_id[]":products_ids,"class_type":class_type,"start_time":start_time,"end_time":end_time},function (data) {
    		var res = $.parseJSON(data);
    		//console.log(res);
        	if(res){
        		var show_time  = res.dataTime;   //时间维度
        			show_datas = res.dataList;	//数据列表
        			show_type  = res.dataType;	//所选类目列表
                 	//console.log(res);
/////////////////////报表开始/////////////////////////////////////////////////
                	// 基于准备好的dom，初始化echarts实例
                    var myChart = echarts.init(document.getElementById('main'));
                    //console.log(shows); 
                    // 指定图表的配置项和数据
                    var option = {
                    	    title: {
                    	        text: '售后订单趋势',
                                textStyle:{
                                    fontFamily: 'Microsoft YaHei',
                                    fontSize:'18',
                                    fontWeight:'600'
                                }
                    	    },
                    	    tooltip: {
                    	        trigger: 'axis'
                    	    },
                    	    legend: {
                    	    	data:show_type
                    	    },
                    	    grid: {
                    	        left: '3%',
                    	        right: '4%',
                    	        bottom: '3%',
                    	        containLabel: true
                    	    },
                    	    toolbox: {
                    	        feature: {
                    	            saveAsImage: {}
                    	        }
                    	    },
                    	    xAxis: {
                    	        type: 'category',
                    	        boundaryGap: false,
                    	        data: show_time
                    	    },
                    	    yAxis: {
                    	        type: 'value'
                    	    },
                    	    series: show_datas
                    	};
                    // 使用刚指定的配置项和数据显示图表。
                    myChart.setOption(option);
/////////////////////报表结束////////////////////////////////////	
            }
        });
    };
    get_show_data();
</script>
</section>
<script type="text/javascript">
//时间控件元素
$('#date-range102').dateRangePicker(
		{
			shortcuts : 
			{
				'prev-days': [1,7,30,60],
				//'prev': ['week','month'],
				'next-days':null,
				'next':null
			}
});
$('.apply-btn').on('click',function(){
    var startDay = $('.start-day').text();
    var endDay = $('.end-day').text();
    var class_type = $("#class_type").val();
    var product_ids = [];
    if (checkboxOk=="") {
        $("input[type=checkbox]:checked").each(function(){
            if($(this).attr("checked")){
                a = $(this).val();
                product_ids.push(a)
            }
        });
    }else{
        checkboxOk.each(function(){
                a = $(this).val();
                product_ids.push(a)
        });
    }
    /* console.log(startDay);
    console.log(endDay);
    console.log(class_type);
    console.log(product_ids);return false; */
    var product_id = product_ids.join(',');
    var url ='/statistics/service-product?start_time='+startDay+'&end_time='+endDay+'&product_ids='+product_id+'&class_type='+class_type;
    setTimeout(function() {
		  window.location.href=url;
	  },300);
});
//执行导出
$("#btn-export").click(function() {
	var  ex = $('#export').attr("data"), //导出标识
	     startDay = $('#start_time').val(),
         endDay = $('#end_time').val(),
         class_type = $("#class_type").val(),
         product_ids = [];
	$("input[name='products_id[]']").each(function(){
		if($(this).attr("checked")){
            a = $(this).val();
            product_ids.push(a)
        }
    });
	//console.log(product_ids);return false;
	var product_id = product_ids.join(',');
	var url ='/statistics/service-product?start_time='+startDay+'&end_time='+endDay+'&products_id='+product_id+'&class_type='+class_type+'&export='+ex;
    setTimeout(function() {
		  window.location.href=url;
	},300);
	/* $.post("/statistics/service-brand", {"export":ex,"brand_ids[]":brand_ids,"start_time":startDay,"end_time":endDay},function (data) {
    	//console.log(ex);
    }); */
});
//切换到二级类目时，根据产品类型一级获取二级数据
/* var odiv = $("#class_type_list");
var str = '';
$("#class_type1_id").change(function() {
	var p_id  = $('#class_type1_id').val();
	$.post("/statistics/get-classes/", {"pid": p_id}, function (data) {
		var res = $.parseJSON(data);
		if(res){
    			odiv.html('');
    			$.each(res, function(i) {
    				var id = i;
    				var name = res[i];
    				str = `<span class="checkbox-span">
                				<input type="checkbox" name="products_id[]" value="${i}">
                			   		<label class="">${name}</label>
                			</span>`;
                			odiv.append(str);
    			});
			}
    });
}); */

window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    window.parent.document.location.reload();
    <?php endif;?>
}
</script>
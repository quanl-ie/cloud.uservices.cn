<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;

$this->title = '服务类型统计';
$this->params['breadcrumbs'][] = $this->title;
?>

<?=Html::jsFile('@web/echarts/echarts.min.js')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/reportForm.css');?>">
<!-- 时间插件引入元素 start -->
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/normalize.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/default.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/daterangepicker.css');?>">
<?=Html::jsFile('@web/js/daterangepickers/moment.min.js')?>
<?=Html::jsFile('@web/js/daterangepickers/jquery.daterangepicker.js')?>
<!-- 时间插件引入元素 end -->
<style type="text/css">
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
    .pagination{
        text-align: center;
    }
    .abcdef{
        top:6px;
    }
    .brand-label{
        line-height: 0;
    }
    .brand-ul>li{
        width: 130px;
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title"><?= Html::encode($this->title) ?></span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                <form action="/statistics/service-type" method="post" class="form-horizontal form-border" id="form">
                    <!-- 时间控件位置 -->
                   <div class="time_pick">
                             <input id="date-range102" size="30" value="<?php echo $params['start_time'].'至'.$params['end_time'];?>">
                             <input type="hidden" id="start_time" name="start_time" value="<?php echo $params['start_time'];?>">
                             <input type="hidden" id="end_time" name="end_time" value="<?php echo $params['end_time'];?>">
                             <i  class="icon iconfont icon-shouqi1"></i>
                    </div>
                    <div class="pinpai">
                        <span class="left-tit-cp">类型</span>
                        <div class="class_type_list" id="class_type_list">
                            <ul class="brand-ul">
                                <?php if(!empty($type_list)) foreach ($type_list as $key=>$val){?>
                                <li>
                                <input type="checkbox" class="brand_id" name="type_ids[]" id="<?php echo $key;?>" value="<?php echo $key;?>" <?php if(in_array($key,explode(',',$params['type_ids']))){ echo "checked";}?>>
                                   <label  class="brand-label" for="<?php echo $key;?>">
                                       <?php echo $val?>
                                       <i class="abcdef"></i>
                                   </label>
                                </li>
                            <?php }?>
                            </ul>
                            <span class="shrink">展开<i class="icon iconfont icon-shouqi1"></i></span>
                            <span class="stop">收起<i class="icon iconfont icon-shouqi"></i></span>
                            <div class="search-ok">
                                <span>提示：最多选择5个类目</span>
                                <button class="search-ok-btn">确认选择</button>
                            </div>
                        </div>

                    </div>
                   <!-- 报表显示，为 ECharts 准备一个具备大小（宽高）的 DOM -->
                    <div class="cus-ser-box">
                        <div id="main" style="margin-right:200px;height:400px;"></div>
                        <div class="cus-ser-bili">
                            <div class="aaaa">
                                <ul class="cus-ser-ul">
                                    <?php if(!empty($type_total)) foreach ($type_total as $key=>$val){?>
                                        <li><span><?php echo $val['name'].'&nbsp&nbsp';?></span><span>(<?php echo $val['total'].'&nbsp&nbsp'.$val['percent']?>)</span></li>
                                       <?php }?>
                                </ul>
                            </div>
                        </div>
                    </div>
                   <!-- 一定时间段内的总数据分类数据占比 -->
                   <!-- 列表显示 -->
                   <div class="table-responsive" style="margin-top: 40px">
                   <!-- 导出数据按钮位置 -->
                    <div class="report-bottom-title">
                        <p style="float: left;" class="reportForm-title">数据报表</p>
                        <div class="btn btn-success daochu" id="btn-export"><i class="icon iconfont icon-daochushuju"></i> 导出数据</div>
                        <input type="hidden" name="export" id="export" class="" data="1">
                    </div>
                    <table class="table table-bordered table-striped table-hover">
                        <thead bgcolor="#455971">
                            <tr>
                                <th>日期</th>
                                <?php if(!empty($type_infos)) foreach ($type_infos as $key=>$val){?>
                                <th><?php echo $val['name']?></th>
                                <th><?php echo $val['name'].'占比'?></th>
                                <?php }?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($data_list):?>
                            <?php foreach ($data_list as $key=>$val){?>
                                <tr>
                                    <td><?php echo $key; ?></td>
                                    <?php foreach ($type_infos as $k => $v):?>
                                        <td><?php echo $val['data'][$k]['numb'];?></td>
                                        <td><?php echo $val['data'][$k]['percent'];?></td>
                                    <?php endforeach;?>
                                </tr>
                            <?php }?>
                            <?php else:?>
                                <tr>
                                    <tr><td colspan="7" class="no-record">暂无数据</td></tr>
                                </tr>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
                   <!-- 分页 --> 
                    <div class="col-xs-12 text-center  pagination">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
// 样式js
 var checkboxOk="";
    $('.shrink').on('click',function () {
        $(this).hide();
        $('.stop').show();
        $('.class_type_list').css('height','auto');
        })
    $('.stop').on('click',function () {
        $(this).hide();
        $('.shrink').show();
        $('.class_type_list').css('height','56px');
    })
    $('.brand_id').change(function(){
        if ($("input[type=checkbox]:checked").length > 5 ) {
            alert("最多能选5个");
            $(this).attr('checked',false);

        }
        checkboxOk = $("input[type=checkbox]:checked");
    });
// ===================
    function get_show_data(){
    	var start_time = $("#start_time").val();
        var end_time = $("#end_time").val();
    	var type_ids = [];
    	$("input[name='type_ids[]']").each(function(){
                if($(this).attr("checked")){
                    a = $(this).val();
                    type_ids.push(a)
                }
            });
    	$.post("/statistics/service-type", {"type_ids[]":type_ids,"start_time":start_time,"end_time":end_time},function (data) {
    		var res = $.parseJSON(data);
        	if(res){
        		var show_time = res.dataTime;   //时间维度
        			show_datas = res.dataList;	//数据列表
        			show_type = res.dataType;	//所选类目列表
                 	//console.log(res);
/////////////////////报表开始/////////////////////////////////////////////////
                	// 基于准备好的dom，初始化echarts实例
                    var myChart = echarts.init(document.getElementById('main'));
                    //console.log(shows); 
                    // 指定图表的配置项和数据
                    var option = {
                    	    title: {
                    	        // text: '售后订单趋势'
                    	    },
                    	    tooltip: {
                    	        trigger: 'axis'
                    	    },
                    	    legend: {
                    	    	data:show_type
                    	    },
                    	    grid: {
                    	        left: '3%',
                    	        right: '4%',
                    	        bottom: '3%',
                    	        containLabel: true
                    	    },
                    	    toolbox: {
                    	        feature: {
                    	            saveAsImage: {}
                    	        }
                    	    },
                    	    xAxis: {
                    	        type: 'category',
                    	        boundaryGap: false,
                    	        data: show_time
                    	    },
                    	    yAxis: {
                    	        type: 'value'
                    	    },
                    	    series: show_datas
                    	};
                    // 使用刚指定的配置项和数据显示图表。
                    myChart.setOption(option);
/////////////////////报表结束////////////////////////////////////	
            }
        });
    };
    get_show_data();
    </script>
</section>
<script type="text/javascript">
//时间控件元素
$('#date-range102').dateRangePicker(
		{
			shortcuts : 
			{
				'prev-days': [1,7,30,60],
				//'prev': ['week','month'],
				'next-days':null,
				'next':null
			}
});
$('.apply-btn').on('click',function(){
    var startDay = $('.start-day').text();
    var endDay = $('.end-day').text();
    var type_ids = [];
    if (checkboxOk=="") {
        $("input[type=checkbox]:checked").each(function(){
            if($(this).attr("checked")){
                a = $(this).val();
                type_ids.push(a)
            }
        });
    }else{
        checkboxOk.each(function(){
                a = $(this).val();
                type_ids.push(a)
        });
    }
    var type_id = type_ids.join(',');
    var url ='/statistics/service-type?start_time='+startDay+'&end_time='+endDay+'&type_ids='+type_id;
    setTimeout(function() {
		  window.location.href=url;
	  },300);
});
//执行导出
$("#btn-export").click(function() {
	var  ex = $('#export').attr("data"), //导出标识
	     startDay = $('#start_time').val(),
         endDay = $('#end_time').val(),
         type_ids = [];
	$("input[name='type_ids[]']").each(function(){
            if($(this).attr("checked")){
                a = $(this).val();
                type_ids.push(a)
            }
    });
	var type_id = type_ids.join(',');
	console.log(startDay);
    //console.log(brand_ids);
	var url ='/statistics/service-type?start_time='+startDay+'&end_time='+endDay+'&type_ids='+type_id+'&export='+ex;
    setTimeout(function() {
		  window.location.href=url;
	},300);
});
window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    window.parent.document.location.reload();
    <?php endif;?>
}
</script>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;

$this->title = '异常订单统计';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
    .pagination{
        text-align: center;
    }
</style>
<?=Html::jsFile('@web/echarts/echarts.min.js')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/reportForm.css');?>">
<!-- 时间插件引入元素 start -->
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/normalize.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/default.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/daterangepicker.css');?>">
<?=Html::jsFile('@web/js/daterangepickers/moment.min.js')?>
<?=Html::jsFile('@web/js/daterangepickers/jquery.daterangepicker.js')?>
<!-- 时间插件引入元素 end -->
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title"><?= Html::encode($this->title) ?></span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                <form action="/statistics/abnormal-orders" method="get" class="form-horizontal form-border" id="form">
                   <div class="time_pick">
                             <input id="date-range102" size="30" value="<?php echo $params['start_time'].'至'.$params['end_time'];?>">
                             <input type="hidden" id="start_time" name="start_time" value="<?php echo $params['start_time'];?>">
                             <input type="hidden" id="end_time" name="end_time" value="<?php echo $params['end_time'];?>">
                             <i  class="icon iconfont icon-shouqi1"></i>
                    </div>
                    <div class="cus-ser-box">
                       <!-- 报表显示，为 ECharts 准备一个具备大小（宽高）的 DOM -->
                        <div id="main" style="margin-right: 200px;height:400px;"></div>

                        <div class="cus-ser-bili">
                            <div class="aaaa">
                                <ul class="cus-ser-ul">
                                    <?php if(!empty($type_total)) foreach ($type_total as $key=>$val){?>
                                    <li><span><?php echo $val['name'].'&nbsp&nbsp';?></span><span>(<?php echo $val['total'].'&nbsp&nbsp'.$val['percent']?>)</span></li>
                                   <?php }?>
                                </ul>
                            </div>
                        </div>
                    </div>
                   <!-- 一定时间段内的总数据分类数据占比 -->
                   <!-- 列表显示 -->
                   <div class="table-responsive" style="margin-top: 40px">
                       <div class="report-bottom-title">
                                <p style="float: left;" class="reportForm-title">订单数据列表</p>
                                <button class="btn btn-success daochu" style="float: right;"><i class="icon iconfont icon-daochushuju"></i> 导出数据</button>
                                <input type="hidden" name="export" class="" value="1">
                        </div>
                    <table class="table table-bordered table-striped table-hover">
                        <thead bgcolor="#455971">
                            <tr>
                                <th>日期</th>
                                <?php if(!empty($type_infos)) foreach ($type_infos as $key=>$val){?>
                                <th><?php echo $val['name']?></th>
                                <th><?php echo $val['name'].'占比'?></th>
                                <?php }?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($data_list):?>
                            <?php foreach ($data_list as $key=>$val){?>
                                <tr>
                                    <td><?php echo $key; ?></td>
                                    <?php foreach ($type_infos as $k => $v):?>
                                        <td><?php echo $val['data'][$k]['numb'];?></td>
                                        <td><?php echo $val['data'][$k]['percent'];?></td>
                                    <?php endforeach;?>
                                </tr>
                            <?php }?>
                            <?php else:?>
                                <tr>
                                    <tr><td colspan="9" class="no-record">暂无数据</td></tr>
                                </tr>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
               <!-- 分页 --> 
                <div class="col-xs-12 text-center  pagination">
                </div>
                </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    function get_show_data(){
    	var start_time = $("#start_time").val();
        var end_time = $("#end_time").val();
    	$.post("/statistics/abnormal-orders", {"start_time":start_time,"end_time":end_time},function (data) {
    		var res = $.parseJSON(data);
        	if(res){
        		var show_time = res.dataTime;   //时间维度
        			show_datas = res.dataList;	//数据列表
        			show_type = res.dataType;	//所选类目列表
                 	//console.log(res);
/////////////////////报表开始/////////////////////////////////////////////////
                	// 基于准备好的dom，初始化echarts实例
                    var myChart = echarts.init(document.getElementById('main'));
                    //console.log(shows); 
                    // 指定图表的配置项和数据
                    var option = {
                    	    title: {
                    	        text: '售后订单趋势'
                    	    },
                    	    tooltip: {
                    	        trigger: 'axis'
                    	    },
                    	    legend: {
                    	    	data:show_type
                    	    },
                    	    grid: {
                    	        left: '3%',
                    	        right: '4%',
                    	        bottom: '3%',
                    	        containLabel: true
                    	    },
                    	    toolbox: {
                    	        feature: {
                    	            saveAsImage: {}
                    	        }
                    	    },
                    	    xAxis: {
                    	        type: 'category',
                    	        boundaryGap: false,
                    	        data: show_time
                    	    },
                    	    yAxis: {
                    	        type: 'value'
                    	    },
                    	    series: show_datas
                    	};
                    // 使用刚指定的配置项和数据显示图表。
                    myChart.setOption(option);
/////////////////////报表结束////////////////////////////////////	
            }
        });
    };
    get_show_data();
    	
        
    </script>
</section>
<script type="text/javascript">
//时间控件元素
$('#date-range102').dateRangePicker(
		{
			shortcuts : 
			{
				'prev-days': [1,7,30,60],
				//'prev': ['week','month'],
				'next-days':null,
				'next':null
			}
});
$('.apply-btn').on('click',function(){
    var startDay = $('.start-day').text();
    var endDay = $('.end-day').text();
    var url ='/statistics/abnormal-orders?start_time='+startDay+'&end_time='+endDay;
    setTimeout(function() {
		  window.location.href=url;
	  },300);
});    
window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    window.parent.document.location.reload();
    <?php endif;?>
}
</script>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;

$this->title = '售后订单统计';
$this->params['breadcrumbs'][] = $this->title;
$status = ['0'=>'待审核','1'=>'已上架','2'=>'审核未通过','3'=>'已下架'];
?>
<style type="text/css">
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
    .pagination{
        text-align: center;
    }
</style>
<?=Html::jsFile('@web/echarts/echarts.min.js')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/reportForm.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<!-- 时间插件引入元素 start -->
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/normalize.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/default.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/daterangepickers/daterangepicker.css');?>">
<?=Html::jsFile('@web/js/daterangepickers/moment.min.js')?>
<?=Html::jsFile('@web/js/daterangepickers/jquery.daterangepicker.js')?>
<!-- 时间插件引入元素 end -->
<?php $typeName=['all'=>'新增订单','finish'=>'已完成订单','cancel'=>'已取消订单'];?>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title"><?= Html::encode($this->title) ?></span>
    
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/statistics/index" method="get" class="form-horizontal form-border" id="form">
                        <div class="time_pick">
                             <input id="date-range102" size="30" value="<?php echo $params['start_time'].'至'.$params['end_time'];?>">
                             <input type="hidden" id="start_time" name="start_time" value="<?php echo $params['start_time'];?>">
                             <input type="hidden" id="end_time" name="end_time" value="<?php echo $params['end_time'];?>">
                             <i  class="icon iconfont icon-shouqi1"></i>
                        </div>
                    <!--  <div style="width: 200px;height:40px;">
                        <?php if($data['showlist']['total']) foreach ($data['showlist']['total'] as $key=>$val){?>
                            <li class=""><span><?php echo $typeName[$key];?></span><span>(<?php echo $val;?>)</span></span></li>
                        <?php }?>
                        
                    </div> -->
                    <div>
                    </div>
                   <!-- 报表显示，为 ECharts 准备一个具备大小（宽高）的 DOM -->
                   <div id="main" style="width: 100%;height:400px;"></div>
                   <!-- 列表显示 -->
                   <!-- 导出数据按钮 -->
                  <!--   <div class="search-confirm">    
                       <button class="btn btn-success"><i class="fa fa-search"></i>导出数据</button>
                    </div> -->

                   <div class="table-responsive" style="margin-top: 30px">
                        <div class="report-bottom-title">
                            <p style="float: left;" class="reportForm-title">订单数据列表</p>
                            <button class="btn btn-success daochu" style="float: right;"><i class="icon iconfont icon-daochushuju"></i> 导出数据</button>
                            <input type="hidden" name="export" class="" value="1">
                        </div>
                        <table class="table table-bordered table-striped table-hover" style="margin-top: 30px;width: 100%">
                            <thead bgcolor="#455971">
                                <tr>
                                    <th>日期</th>
                                    <th>新增订单数</th>
                                    <th>已完成订单数</th>
                                    <th>已取消订单数</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($data['showlist']['list']):?>
                                <?php foreach ($data['showlist']['list'] as $key=>$val):  ?>
                                    <tr>
                                        <td><?php echo $key; ?></td>
                                        <td><?php echo $val['all']; ?></td>
                                        <td><?php echo $val['finish']; ?></td>
                                        <td><?php echo $val['cancel']; ?></td>
                                    </tr>
                                <?php endforeach;?>
                                <?php else:?>
                                    <tr>
                                        <tr><td colspan="6" class="no-record">暂无数据</td></tr>
                                    </tr>
                                <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                   <!-- 分页 --> 
                    <div class="col-xs-12 text-center  pagination">
                    </div>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    function get_show_data(){
        var start_time = $("#start_time").val();
        var end_time = $("#end_time").val();
        /* var start_time = $('.start-day').text();
        var end_time = $('.end-day').text(); */
        //alert(start_time);
    	$.post("/statistics/index",{"start_time":start_time,"end_time":end_time}, function (data) {
    		var res = $.parseJSON(data);
        	if(res){
        		var show_time = res.dataTime;   //时间维度
        			show_data = res.dataList;	//数据列表
                 	//console.log(res);
                 	var arr = [];
                 	if(show_data){
                 		for(var i=0;i < show_data.length;i++){
                         	var obj = {};
                         	obj.name = show_data[i].name;
                         	obj.type = 'line';
                         	obj.stack = '总量';
                         	obj.data = show_data[i].data;
                         	arr.push(obj)
                         	}
                    }
                 	var show_datas = arr;
                	///////报表开始/////////////////////////////////////////////////
                	// 基于准备好的dom，初始化echarts实例
                    var myChart = echarts.init(document.getElementById('main'));
                    //console.log(shows); 
                    // 指定图表的配置项和数据
                    var option = {
                    	    title: {
                    	        text: '售后订单趋势',
                                textStyle:{
                                    fontFamily: 'Microsoft YaHei',
                                    fontSize:'18',
                                    fontWeight:'600'
                                }
                                
                    	    },
                    	    tooltip: {
                    	        trigger: 'axis'
                    	    },
                    	    legend: {
                    	    	data:['all','finish','cancel']
                    	    },
                    	    grid: {
                    	        left: '3%',
                    	        right: '4%',
                    	        bottom: '3%',
                    	        containLabel: true
                    	    },
                    	    toolbox: {
                    	        feature: {
                    	            saveAsImage: {}
                    	        }
                    	    },
                    	    xAxis: {
                    	        type: 'category',
                    	        boundaryGap: false,
                    	        data: show_time
                    	    },
                    	    yAxis: {
                    	        type: 'value'
                    	    },
                    	    series: show_datas
                    	};
                    // 使用刚指定的配置项和数据显示图表。
                    myChart.setOption(option);
                /////报表结束////////////////////////////////////	
            }
        	
        });
    };
    get_show_data();
    	
        
    </script>
</section>
<script>
//时间控件元素
$('#date-range102').dateRangePicker(
		{
			shortcuts : 
			{
				'prev-days': [1,7,30,60],
				//'prev': ['week','month'],
				'next-days':null,
				'next':null
			}
});
$('.apply-btn').on('click',function(){
    var startDay = $('.start-day').text();
    var endDay = $('.end-day').text();
    var url ='/statistics?start_time='+startDay+'&end_time='+endDay;
    setTimeout(function() {
		  window.location.href=url;
	  },300);
    /* var form = new FormData();
        form.append("start_time",startDay);
        form.append("end_time",endDay);
       　$.ajax({
           url:"/statistics/index",
           type:"post",
           data:form,
           processData:false,
           contentType:false,
           success:function(data){
               //window.clearInterval(timer);
               console.log("over..");
           }
	}); */    
    /* $.post("/statistics/index", {"start_time":startDay,"end_time":endDay},function (data) {
		alert('dd');
    	//console.log(ex);
    }); */
    //console.log(startDay+'至于'+endDay);
});
window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    window.parent.document.location.reload();
    <?php endif;?>
}
</script>
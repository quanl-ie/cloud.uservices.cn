<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;
if($data['info']['stock_type'] ==1){
    $this->title = '配件申请详情';
}else{
    $this->title = '配件返还详情';
}
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .information-list {margin-top: 0;}
</style>

 
<div class="jn-title-box no-margin-bottom">
        <?php if($data['info']['stock_type'] == 1 ){?>
            <span class="jn-title" id="no" data-no="<?php echo $data['info']['id'];?>">备件申请详情</span>
        <?php }else{?>
            <span class="jn-title" id="no" data-no="<?php echo $data['info']['id'];?>">备件返还详情</span>
        <?php }?>
        <div class="right-btn-box">
            <?php if($data['info']['audit_status'] == 1 ){?>
                <?php if(in_array('/tech-prod-stock-return/set-audit',$selfRoles)){?>
                    <a href="" class="setAudit" data-type="no"><button class="btn btn-orange">拒绝</button></a>
                    <a href="" class="setAudit" data-type="ok"><button class="btn btn-orange">同意</button></a>
                <?php };?>
            <?php }?>
            <?php if($data['info']['audit_status'] == 2 ){?>
                <?php if($data['info']['stock_type'] == 1 ){?>
                    <?php if(in_array('/stock-out/add',$selfRoles)){?>
                        <a href="/stock-out/add?rel_id=<?php echo $data['info']['id'].'&rel_type=5';?>" >
                            <button class="btn btn-orange">出库</button>
                        </a>
                    <?php };?>
            <?php }else{?>
                    <?php if(in_array('/stock-in/add',$selfRoles)){?>
                        <a href="/stock-in/add?rel_id=<?php echo $data['info']['id'].'&rel_type=6';?>" >
                            <button class="btn btn-orange">入库</button>
                        </a>
                    <?php };?>
                <?php }?>
            <?php }?>
            <button class="btn bg-f7" onclick="javascript:history.back(-1);">返回</button>
        </div>

</div>

<section id="main-content">
    <div class="panel-body">
        <h5 class="block-h5 block-h5one">基本信息</h5>
        <div class="information-list">
            <?php if($data['info']['stock_type'] == 1 ){?>
                <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                    <label   class="left-title80">申请编号：</label>
                    <div  class="right-title3"><span><?php echo isset($data['info']['no'])?$data['info']['no']:''?></span></div>
                </div>
            <?php }else{?>
                <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                    <label   class="left-title80">返还编号：</label>
                    <div  class="right-title3"><span><?php echo isset($data['info']['no'])?$data['info']['no']:''?></span></div>
                </div>
                <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                    <label   class="left-title80">返还类型：</label>
                    <div  class="right-title3"><span><?php echo $data['info']['return_type'] == 1 ? '良品':'非良品'?></span></div>
                </div>
            <?php }?>
            <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                <label   class="left-title80">申请状态：</label>
                <div  class="right-title3">
                    <span><?php echo isset($data['info']['status_name'])?$data['info']['status_name']:''?></span>
                </div>
            </div>
            <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                <label   class="left-title80">申请技师：</label>
                <div  class="right-title3">
                    <span><?php echo isset($data['info']['tech_name'])?$data['info']['tech_name']:''?></span>
                </div>
            </div>
        
            <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                <label   class="left-title80">创建时间：</label>
                <div  class="right-title3">
                    <span><?php echo isset($data['info']['apply_time'])?$data['info']['apply_time']:''?></span>
                </div>
            </div>
        </div>
       
        <?php if($data['info']['stock_type'] == 1 ){?>
            <h5 class="block-h5">申请备件</h5>
        <?php }else{?>
            <h5 class="block-h5">返还备件</h5>
        <?php }?>

        <div class="information-list" style="height: auto">
            <table>
                <thead>
                <tr>
                    <th>配件名称</th>
                    <th>产品类目</th>
                    <th>品牌</th>
                    <th>型号</th>
                    <?php if($data['info']['stock_type'] == 1 ){?>
                        <th>申请数量</th>
                        <th>库存数量</th>
                    <?php }else{?>
                        <th>数量</th>
                    <?php }?>
                    <th>市场单价</th>
                    <th>总价</th>
                </tr>
                </thead>
                <tbody>
                <?php if ($data['detail']) {?>
                    <?php foreach ($data['detail'] as $val) : ?>
                        <tr>
                            <td><?= isset($val['prod_name'])?$val['prod_name']:'' ?></td>
                            <td><?= isset($val['class_name'])?$val['class_name']:'' ?></td>
                            <td><?= isset($val['brand_name'])?$val['brand_name']:'' ?></td>
                            <td><?= isset($val['model'])?$val['model']:'' ?></td>
                            <?php if($data['info']['stock_type'] == 1 ){?>
                                <td><?= isset($val['prod_num'])?$val['prod_num']:'' ?></td>
                                <td><?= isset($val['total_num'])?$val['total_num']:'' ?></td>
                            <?php }else{?>
                                <td><?= isset($val['prod_num'])?$val['prod_num']:'' ?></td>
                            <?php }?>
                            <td><?= isset($val['price'])?$val['price']:'' ?></td>
                            <td><?= isset($val['total_amount'])?$val['total_amount']:'' ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php }else{?>
                    <tr><td colspan="11" class="no-record">暂无数据</td></tr>
                <?php }?>
                </tbody>
            </table>
        </div>
        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
            <label   class="left-title80">备注：</label>
            <div  class="right-title3">
                <span><?php echo isset($data['info']['description'])?$data['info']['description']:''?></span>
            </div>
        </div>
    </div>
</section>
<!-- 设置同意拒绝弹层 -->
<div id="noLayer" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <div class="select-fuji">
            <textarea placeholder="请填写打回原因" id="opinionContent" style="width: 100%;height: 100px;" onkeyup=" wordsLimit(this)"></textarea>
            <p class="xianzhi" style="right: 10px;"><span id="word">0</span>/200</p>
        </div>
        <div style="text-align: center;margin-top:15px;">
            <button class="btn btn-orange mgr8" id="layerSubmit"  data-type="4"><i class="fa fa-search"></i>确定</button>
            <button class="btn bg-f7" id="layerCancel"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>
<div id="okLayer" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <p style="margin:0 0 10px 0;text-align: center;">确定要将此信息审批通过吗？</p>
        <div style="text-align: center;margin-top:30px;">
            <button class="btn btn-orange mgr8" id="layerSubmit" data-type="2"><i class="fa fa-search"></i>确定</button>
            <button class="btn bg-f7" id="layerCancel"><i class="fa fa-search" ></i>取消</button>

        </div>
    </div>
</div>
<script>
    var layerIndex = null;
    //var purchaseNo = $(this).attr('data-no');
    $(".setAudit").click(function () {
        var setType = $(this).attr('data-type');
        if(setType == 'ok'){
            var statusType = 2;  //通过，设定为待出入库状态
            layerIndex = layer.open({
                type: 1,
                title: '提示',
                area: ['300px', '180px'],
                fixed: true,
                maxmin: false,
                content: $('#okLayer').html()
            });
        }else {
            var statusType = 4;  //未通过
            layerIndex = layer.open({
                type: 1,
                title: '请输入拒绝原因',
                area: ['400px', '250px'],
                fixed: true,
                maxmin: false,
                content: $('#noLayer').html()
            });
        }
        return false;
    });
    //备件申请返还审核操作
    $("body").delegate("#layerSubmit","click",function(){
        var prodStockNO = $('#no').attr('data-no');
        var statusType = $(this).attr('data-type');
        var opinion_content = $(this).parents('div').find('#opinionContent').val();
        //var statusType = statusType;
        if($.trim(prodStockNO) == ''){
            alert('单号为空，操作失败');
            return false;
        }
        if($.trim(statusType) == ''){
            alert('操作状态错误！');
            return false;
        }
        if(statusType == 4){
            if($.trim(opinion_content) == ''){
                alert('打回原因不能为空！');
                return false;
            }
        }
        $.getJSON('/tech-prod-stock-return/set-audit',{'no':prodStockNO,'status':statusType,'audit_suggest':opinion_content},function (json) {
            //console.log(json);return false;
            if(json.success == 1){
                if(layerIndex){
                    layer.close(layerIndex);
                }
                alert(json.message);
                setTimeout(function(){location.reload();},1000);
                //window.location.reload(true);
            }
            else {
                alert(json.message);
            }
        });
    });
    //关闭弹层
    $("body").delegate("#layerCancel","click",function(){
        if(layerIndex){
            layer.close(layerIndex);
        }
    });
</script>

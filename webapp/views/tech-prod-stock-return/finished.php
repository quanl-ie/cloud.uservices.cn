<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
if($action == 'apply'){
    $this->title = '技师备件申请';
}else{
    $this->title = '技师备件返还';
}
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
    .search-box-kuang>.col-sm-6{padding-left: 0;position: relative;}
    .icon-rili{position: absolute;right: 17px;top: 1px;height: 34px;line-height: 34px;background: #fff;width: 24px;text-align: center;}
    .calendar-icon{width: 100%;height: 36px;position: absolute;top:0;left: 0;cursor: pointer;}
    .pagination{text-align: center;}
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
        border-top:2px solid #2693ff;
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <?php if($action == 'apply'){?>
        <span class="jn-title">技师备件申请</span>
    <?php }else{?>
        <span class="jn-title">技师备件返还</span>
    <?php }?>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div style="width: 100%;" class="tab-group">
                        <ul id="myTab" class="nav nav-tabs">
                            <?php if($action == 'apply'){?>
                                <?php if(in_array('/tech-prod-stock/index',$selfRoles)){?>
                                    <li><a href="/tech-prod-stock/apply" >待审批</a></li>
                                <?php };?>
                                <?php if(in_array('/tech-prod-stock/wait-check',$selfRoles)){?>
                                    <li><a href="/tech-prod-stock/wait-check?type=apply">待出库</a></li>
                                <?php };?>
                                <?php if(in_array('/tech-prod-stock/finished',$selfRoles)){?>
                                    <li class="active"><a href="/tech-prod-stock/finished?type=apply">已完成</a></li>
                                <?php };?>
                                <?php if(in_array('/tech-prod-stock/not-pass',$selfRoles)){?>
                                    <li><a href="/tech-prod-stock/not-pass?type=apply">未通过</a></li>
                                <?php };?>
                                <?php if(in_array('/tech-prod-stock/cancelled',$selfRoles)){?>
                                    <li><a href="/tech-prod-stock/cancelled?type=apply">已取消</a></li>
                                <?php };?>
                            <?php }else{?>
                                <?php if(in_array('/tech-prod-stock-return/refund',$selfRoles)){?>
                                    <li><a href="/tech-prod-stock-return/refund" >待审批</a></li>
                                <?php };?>
                                <?php if(in_array('/tech-prod-stock-return/wait-check',$selfRoles)){?>
                                    <li><a href="/tech-prod-stock-return/wait-check?type=refund">待入库</a></li>
                                <?php };?>
                                <?php if(in_array('/tech-prod-stock-return/finished',$selfRoles)){?>
                                    <li class="active"><a href="/tech-prod-stock-return/finished?type=refund">已完成</a></li>
                                <?php };?>
                                <?php if(in_array('/tech-prod-stock-return/not-pass',$selfRoles)){?>
                                    <li><a href="/tech-prod-stock-return/not-pass?type=refund">未通过</a></li>
                                <?php };?>
                                <?php if(in_array('/tech-prod-stock-return/cancelled',$selfRoles)){?>
                                    <li><a href="/tech-prod-stock-return/cancelled?type=refund">已取消</a></li>
                                <?php };?>
                            <?php }?>
                        </ul>
                    </div>
                    <?php if($action == 'apply'){?>
                        <form action="/tech-prod-stock/finished?type=apply" class="form-horizontal form-border" id="form">
                    <?php }else{?>
                        <form action="/tech-prod-stock-return/finished?type=refund" class="form-horizontal form-border" id="form">
                    <?php }?>
                            <div class="form-group jn-form-box">
                                <!-- 搜索栏 -->
                                <div class="col-sm-12 no-padding-left">
                                    <div class="single-search">
                                        <label class=" search-box-lable">技师姓名：</label>
                                        <div class="single-search-kuang1">
                                            <input type="text" class="form-control" name="tech_name" value="<?php echo isset($_GET['tech_name'])?$_GET['tech_name']:'';?>">
                                        </div>
                                    </div>
                                    <div class="single-search">
                                        <label class=" search-box-lable">技师电话：</label>
                                        <div class="single-search-kuang1">
                                            <input type="text" class="form-control" name="tech_mobile" value="<?php echo isset($_GET['tech_mobile'])?$_GET['tech_mobile']:'';?>">
                                        </div>
                                    </div>
                                    <input type="text" name="type" hidden="hidden" value="<?php echo isset($_GET['type'])?$_GET['type']:'';?>">
                                    <div class="search-confirm">
                                        <button class="btn btn-success "><i class=" "></i> 搜索</button>
                                    </div>

                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead bgcolor="#455971">
                                    <tr>
                                        <?php if($action == 'apply'){?>
                                            <th>申请编号</th>
                                        <?php }else{?>
                                            <th>返还编号</th>
                                        <?php }?>
                                        <th>技师姓名</th>
                                        <th>技师电话</th>
                                        <th>产品数量</th>
                                        <th>创建时间</th>
                                        <!-- <th>审批时间</th>-->
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($data){?>
                                        <?php foreach ($data as $key=>$val){?>
                                            <tr>
                                                <td>
                                                    <?php if(in_array('/tech-prod-stock-return/detail',$selfRoles)){?>
                                                        <a href="/tech-prod-stock-return/detail?id=<?php echo $val['id'];?>"> <?php echo $val['no']; ?> </a>
                                                    <?php }else{?>
                                                        <?php echo $val['no']; ?>
                                                    <?php }?>
                                                </td>
                                                <td>
                                                    <?php echo $val['tech_name']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $val['tech_mobile']; ?>
                                                </td>
                                                <td>
                                                    <?php echo isset($val['total_num']) ? $val['total_num'] : ''; ?>
                                                </td>
                                                <td>
                                                    <?php echo isset($val['apply_time'])?$val['apply_time']: ''; ?>
                                                </td>
                                                <td class="operation">
                                                    <?php if(in_array('/tech-prod-stock-return/detail',$selfRoles)){?>
                                                        <a href="/tech-prod-stock-return/detail?id=<?php echo $val['id'];?>" class="update">查看</a>
                                                    <?php };?>
                                                </td>
                                            </tr>
                                        <?php }?>
                                    <?php }else{?>
                                        <?php if($action == 'apply'){?>
                                            <tr><td colspan="11" class="no-record">暂无已完成申请信息</td></tr>
                                        <?php }else{?>
                                            <tr><td colspan="11" class="no-record">暂无已完成返还信息</td></tr>
                                        <?php }?>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                        <div class="col-xs-12 text-center  pagination">
                            <?php echo $pageHtml;?>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- 取消弹层 -->
<div id="cancelLayer" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <p style="margin:0 0 10px 0;text-align: center;">确认取消采购吗？</p>
        <div class="select-fuji">
            <textarea placeholder="请填写取消意见" id="opinionContent" style="width: 100%;height: 100px;"></textarea>
        </div>
        <div style="text-align: right;margin-top:30px;">
            <button class="btn btn-success" id="layerSubmit"  style="padding: 4px 12px"><i class="fa fa-search"></i>确定</button>
            <button class="btn" id="layerCancel" style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>
<script>
    var layerIndex = null;
    var purchaseNo = '';
    $(".setCancel").click(function () {
        layerIndex = layer.open({
            type: 1,
            title: '提示',
            area: ['400px', '300px'],
            fixed: true,
            maxmin: false,
            content: $('#cancelLayer').html()
        });
        purchaseNo = $(this).attr('href');
        return false;
    });
    //提交取消订单
    $("body").delegate("#layerSubmit","click",function(){
        var opinion_content = $(this).parents('div').find('#opinionContent').val();
        if($.trim(purchaseNo) == ''){
            alert('采购编号不能为空');
            return false;
        }
        if($.trim(opinion_content) == ''){
            alert('请填写取消意见');
            return false;
        }
        $.getJSON('/purchase/cancel',{'no':purchaseNo,'audit_suggest':opinion_content},function (json) {
            //console.log(json);return false;
            if(json.success == 1){
                if(layerIndex){
                    layer.close(layerIndex);
                }
                alert(json.message);
                setTimeout(function(){location.reload();},2000);
                //window.location.reload(true);
            }
            else {
                alert(json.message);
            }
        });
    });
    //关闭弹层
    $("body").delegate("#layerCancel","click",function(){
        if(layerIndex){
            layer.close(layerIndex);
        }
    });
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
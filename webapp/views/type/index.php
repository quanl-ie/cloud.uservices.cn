<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = '服务类型管理';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?=Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?=Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?=Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4{
        padding-left: 0;
    }
    input[type=radio]{
        margin-top: 10px;
    }
    body{
        overflow-x: hidden;
        font-family: "微软雅黑";
    }
    table{
        border: 1px solid #ccc;
        width:380px;
        margin:30px auto;
    }
    th,td{
        border-right: 1px solid #ccc;
        text-align: center;height: 45px;
        font-family: "微软雅黑";
        font-size: 14px;

    }
    td{
        line-height: 2;
    }
    th{

        background: #f6fafe;
    }
    tr{
        border-bottom: 1px solid #ccc;
    }
    .range-td>div{
        border-top: 1px solid #ccc;
    }
    .range-td>div:nth-child(1){
        border-top: 0px solid #ccc;
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">服务类型管理
    <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="用于创建收费标准、售后订单、技师结算规则等操作时使用"></span>
    </span>
    <?php if (!empty($flag) && $flag == 1): ?>
        <?php if(in_array('/type/add',$selfRoles)){?>
            <span class="btn btn-success jn-btn" style=" color:#FFF;float:right;width:auto;" id="addParentIframe"  data-toggle="modal" data-target="#scrollingModal" onclick="addParentIframe()">添加</span>
        <?php };?>
    <?php endif; ?>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                     <?php if(!empty($data)) : ?>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>服务类型</th>
                                <th>状态</th>
                                <?php if (!empty($flag) && $flag == 1): ?>
                                <th>操作</th>
                                <?php endif; ?>
                            </tr>
                            </thead>
                            <?php foreach ($data as $key=>$val ) : ?>
                                <tr>
                                    <td><?=$val['title'];?></td>
                                    <td><?=$val['status_desc']; ?></td>
                                    <?php if (!empty($flag) && $flag == 1): ?>
                                    <td>
                                        <?php if (isset($val['status']) && $val['status'] == 1) : ?>
                                            <span class="del">
                                                <?php if(in_array('/type/change-status',$selfRoles)){?>
                                                    <a href="javascript:;" onclick="disBranf(<?=$val['type_id']; ?>,2)">禁用</a>
                                                <?php };?>
                                            </span>
                                        <?php else: ?>
                                            <span class="del">
                                                <?php if(in_array('/type/change-status-on',$selfRoles)){?>
                                                    <a href="javascript:;" onclick="enaBrand(<?=$val['type_id']; ?>,1)" >启用</a>
                                                <?php };?>
                                            </span>
                                        <?php endif; ?>
                                    </td>
                                    <?php endif; ?>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                    <div class="col-xs-12 text-center  pagination">

                    </div>
                    <?php else : ?>
                        <div class="no-data-page">
                            <div>
                                <div class="kbtp"></div>
                                <p class="no-data-zi">您还没有添加过服务类型</p>
                                <?php if (!empty($flag) && $flag == 1): ?>
                                <span class="btn btn-success" onclick="addParentIframe()">添加服务类型</span>
                                <?php endif; ?>
                            </div>
                        </div>
                     <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="/js/layer/layer.js"></script>
<script>
$(".tip").popover();
    window.onload = function(){
         var  heightKs = ($(window).height())-265+'px';
        $('.no-data-page').height(heightKs);
    };
    $(window).resize(function(){
        var heightKs = ($(window).height())-265+'px';
        $('.no-data-page').height(heightKs);
    });

    //添加
    function addParentIframe (argument) {
        layer.open({
            type: 2,
            title: '请选择服务类型',
            area: ['400px','250px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/type/add', 'no'],
            end:function(){
                location.reload();
            }
        });
    }

    /**
     * 禁用
     * @param id
     * @param status
     */
    function disBranf(id,status) {
        layer.confirm('您确定要禁用该服务类型吗？禁用后有此服务类型的经销商将不能创建此服务类型的新订单，但不影响未完成和已完成订单数据。', {
            area:['320px','210px'],
            btn: ['确定','取消'] //按钮
        }, function(){
            ajaxPost(id,status);
        }, function(){
            layer.close();
        });
    }
    
    /**
     *  启用
     * @param id
     * @param status
     */
    function enaBrand(id,status) {
        layer.confirm('您确定要启用该服务类型吗？启用后有此服务类型的经销商将可创建此服务类型的新订单，但不影响未完成和已完成订单数据。', {
            area:['320px','210px'],
            btn: ['确定','取消'] //按钮
        }, function(){
            ajaxPostOn(id,status);
        }, function(){
            layer.close();
        });
    }
    
    function ajaxPost(id,status){
        var url = '/type/change-status';
        $.ajax({
            type: 'POST',
            url: url,
            data: {'id':id,'status':status},
            dataType: 'json',
            success: function(data){
                if (data.code == 200) {
                    layer.alert(data.message, {
                    }, function(){
                        location.reload();
                    });
                }else{
                    alert(data.message);
                }
            }
        });
    }

    function ajaxPostOn(id,status){
        var url = '/type/change-status-on';
        $.ajax({
            type: 'POST',
            url: url,
            data: {'id':id,'status':status},
            dataType: 'json',
            success: function(data){
                if (data.code == 200) {
                    layer.alert(data.message, {
                    }, function(){
                        location.reload();
                    });
                }else{
                    alert(data.message);
                }
            }
        });
    }

</script>

<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$this->title = '收费项目';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?=Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?=Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?=Url::to('/css/iconfont.css');?>">

<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    body{background: #fff;}
    .col-sm-4, .col-md-4, .col-lg-4{
        padding-left: 0;
    }
    input[type=radio]{
        margin-top: 10px;
    }
    body{
        overflow-x: hidden;
    }
    label{
        font-weight: 100;font-family: "微软雅黑";font-size: 14px;
    }
</style>
<body>
<div class="row popup-boss">
    <?php
    $form = ActiveForm::begin([
        'options' => [
            'class' => 'form-horizontal',
            'id' => 'type-add',
            'name' => 'form1',
            'enctype' => 'multipart/form-data',
        ]
    ]);
    ?>
    <div class="details-column"   style="width: auto;margin-right: 30px">
        <label class="details-column-title"><i class="red-i">*</i>请选择</label>
        <div class="details-column-content">
            <div class="checkbox-box2" style="height:108px;">

                <?php
                $flag = true;
                foreach ($type_list as $key => $val){
                    if (!in_array($key, $type_ids)){
                        $flag = false;
                        break;
                    }
                }
                ?>
                <span class="checkbox-span">
                    <input id="checkAll" type="checkbox" <?php if($flag):?>checked="checked"  disabled class="ckb-disabled"<?php endif;?>  >
                    <label for="checkAll" class="brand-label">
                        <i class="abcdef"></i>全选
                    </label>
                </span>
                
                <?php if (!empty($type_list)) : ?>

                <?php foreach ($type_list as $key => $val) : ?>
                    
                    <span class="checkbox-span realCk">
                        
                        <?php if (!empty($type_ids) && in_array($key, $type_ids)) : ?>
                            
                            <input type="checkbox" disabled checked="checked" id="<?=$key?>" value="<?=$key?>" data="<?=$val?>" class="ckb-disabled">

                      
                        <?php else : ?>
                            
                            <input type="checkbox" name="type_id[]" id="<?=$key?>" value="<?=$key?>" data="<?=$val?>">
                      
                        <?php endif;?>
                        
                        <label for="<?=$key;?>" class="brand-label">
                            
                            <i class="abcdef"></i>
                            
                            <?=$val;?>
                            
                        </label>
                        
                    </span>
                    
                <?php endforeach;?>
                
                <?php endif; ?>
            </div>
        </div>
    </div>
    <hr style="margin-bottom: 0;">
    <div  class="col-md-8 information-list"  style="margin-top: 13px">
        <div class="right-btn" style="text-align: center;">
            <input type="button" id="closeIframe"  value="取消">
            <input type="submit" value="提交" style="margin-left: 20px" class="btn-orange">
        </div>
    </div>
    <?php ActiveForm::end() ?>
</div>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=bbcdabda1427179d77bb95fac7b2b902&plugin=AMap.Geocoder"></script>
<script>

    var index = parent.layer.getFrameIndex(window.name);
    //表单验证
    $("#type-add").Validform({
        ajaxPost:true,
        tiptype:3,
        callback:function(data){
            if (data.code == 200) {
                parent.layer.close(index);
                parent.layer.alert(data.message, function(index){
                    parent.location.reload();
                });
            }else{
                $.Hidemsg();
                alert(data.message);
            }
        }
    });
    
    //关闭iframe
    $('#closeIframe').click(function(){
        parent.layer.close(index);
    });

    $('#checkAll').click(function () {
        $('.realCk').find('input[type="checkbox"]').prop('checked',$(this).prop('checked'));
    });

</script>
</body>
</html>
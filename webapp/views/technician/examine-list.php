<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
$this->title = '技师认证审核';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
    .search-box-kuang>.col-sm-6{
        padding-left: 0;
        position: relative;
    }
    .icons-calendar{
        position: absolute;
        right: 24px;top: 9px;
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">技师认证审核</span>
</div> 
<section id="main-content">
<div class="row">
    <div class="col-md-12">                    
        <div class="panel panel-default">                     
            <div class="panel-body">
                <form action="/technician/examine-list" class="form-horizontal form-border" id="form">
                    <div class="form-group jn-form-box">
                        <div class="col-sm-12 no-padding-left">
                            <div class="single-search">
                                <label class=" search-box-lable">技师姓名</label>
                                <div class="single-search-kuang1">
                                    <input type="text" class="form-control" name="technician_name" maxlength="20" value="<?=isset($params['technician_name']) ? $params['technician_name'] : '' ?>">
                                </div>
                            </div>
                            <div class="single-search">
                                <label class=" search-box-lable">技师电话</label>
                                <div class="single-search-kuang1">
                                    <input type="text" class="form-control" name="technician_mobile" maxlength="20" value="<?= isset($params['technician_mobile']) ? $params['technician_mobile'] : '' ?>">
                                </div>
                            </div>

                            <div class="single-search">
                                <label class=" search-box-lable">接单状态</label>
                                <div class="single-search-kuang1">
                                    <select class="form-control" name="audit_status">
                                        <option value="0">全部</option>
                                        <?php foreach ($auditStatus as $key=>$val){?>
                                        <option value="<?php echo $key; ?>" <?php if(isset($_GET['audit_status']) && $_GET['audit_status']==$key):?>selected<?php endif; ?>><?php echo $val;?></option>
                                        <?php };?>
                                    </select>
                                </div>
                            </div>
                            <div class="search-confirm"> 
                                <button class="btn btn-success"><i class=" "></i> 搜索</button>
                            </div>
                        </div>
                    </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead bgcolor="#455971">
                            <tr>
                                <th>技师编号</th>
                                <th>技师姓名</th>
                                <th>联系电话</th>
                                <th>审核状态</th>
                                <th>提交日期</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($data){?>
                            <?php foreach ($data as $key=>$val){?>
                                <tr>
                                    <td>
                                        <?php echo isset($val['technician_number'])?$val['technician_number']:''; ?>
                                    </td>
                                    <td>
                                        <?php echo isset($val['name'])?$val['name']:''; ?>
                                    </td>
                                    <td><?php echo isset($val['mobile']) ? $val['mobile'] : ''; ?></td>
                                    <td>
                                        <?php echo isset($auditStatus[$val['audit_status']])?$auditStatus[$val['audit_status']]:'未知';?>
                                    </td>
                                    <td>
                                         <?php echo date("Y-m-d H:i:s",$val['audit_time']);?>
                                    </td>
                                    <td class="operation">
                                        <a href="/technician/detail?technician_id=<?php echo $val['id'] ?>">技师详情</a>
                                        <?php if($val['audit_status'] == '1'){?>
                                          <a href="" class="viewExamine" id-data="<?php echo $val['id'];?>">查看并审核</a>
                                        <?php }elseif ($val['audit_status'] == '3' or $val['audit_status'] == '4'){?>
                                            <a href="" class="viewReason" id-data="<?php echo $val['id'];?>">驳回原因</a>
                                            <a href="/technician/examine-edit?id=<?php echo $val['id'];?>" >修改</a>
                                        <?php }?>
                                    </td>
                                </tr>
                            <?php }}else {?>
                                <tr>
                                    <tr><td colspan="6" class="no-record">暂无数据</td></tr>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
                </form>
                <div class="col-xs-12 text-center  pagination">
                    <?php echo $pageHtml;?>
                </div>
            </div> 
       </div>
   </div>
 </div>
</section>
<script src="/js/layer/layer.js"></script>
<script>
//查看驳回原因
$(".viewReason").click(function(){
   var id = $(this).attr("id-data");
   layer.open({
        type: 2,
        title: ['驳回原因', 'font-size:18px;'],
        area: ['700px', '400px'],
        fixed: false, //不固定
        maxmin: false,
        content: '/technician/reason?id='+id,
    });
   return false;
});
//查看并审核
$(".viewExamine").click(function(){
    var id = $(this).attr("id-data");
    layer.open({
        type: 2,
        title: ['查看并审核', 'font-size:18px;'],
        area: ['1000px', '500px'],
        fixed: false, //不固定
        maxmin: false,
        content: '/technician/to-examine?id='+id,
    });
    return false;
});
//关闭iframe
$('#closeIframe').click(function(){
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
});
window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    <?php endif;?>
};
</script>

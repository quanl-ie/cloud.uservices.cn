<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
  body{
    display: table;
    width: 100%;
    height: 100%;
}
    .page-container{text-align: center;font-size: 15px;display: table-cell;vertical-align: middle;
        
    }
    .page-container>div{
        
    }
    .page-container>a{display: inline-block;padding: 10px 20px;height: 35px;border: 1px solid #2693ff;border-radius: 5px;}
    .page-container>div>img{width: 120px;}
    .page-container>div>p{margin:20px 80px;line-height: 1.8;}
     
</style>
<body>
<div class="page-container">
    <div>
        <img src="<?php echo Url::to('/images/jishiweitongguoshenghe.png');?>">
        <p>该技师的实名认证信息未通过审核！<br/>
            驳回原因：<?php echo isset($reason)?$reason:'' ?>
        </p>
    </div>
</div>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
</body>
</html>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4{
        padding-left: 0;
    }
    input[type=radio]{
        margin-top: 10px;
    }
    body{
        overflow-x: hidden;
        font-family: "微软雅黑";background: #fff;
    }
    table{
        border: 1px solid #ccc;
        width:380px;
        margin:30px auto;
    }
    th,td{
        border-right: 1px solid #ccc;
        text-align: center;height: 45px;
        font-family: "微软雅黑";
        font-size: 14px;

    }
    td{
        line-height: 2;
    }
    th{
       
        background: #f6fafe;
    }
    tr{
         border-bottom: 1px solid #ccc;
    }
    .range-td>div{
        border-top: 1px solid #ccc;
    }
    .range-td>div:nth-child(1){
        border-top: 0px solid #ccc;
    }
</style>
<body>
<div class="row popup-boss">
    <table>
        <tr><th>服务类型</th><th>产品类目</th></tr>
        <?php if(!empty($field)){
            foreach($field as $key=>$val){
            ?>
            <tr>
                <td><?php echo $key?></td>
                <td class="range-td">
                     <?php
                         foreach($val as $v){
                                 echo '<div>'.$v.'</div>';
                         }
                     ?>
                </td>
            </tr>
        <?php
            }

        }else { ?>
            <tr>
                <td colspan="2">暂无数据</td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
</body>
</html>
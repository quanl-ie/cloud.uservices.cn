<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/edit-area.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<!DOCTYPE html>
<html>
<head>
</head>
<script src="http://webapi.amap.com/maps?v=1.4.1&key=bbcdabda1427179d77bb95fac7b2b902&plugin=AMap.Autocomplete,AMap.Geocoder
"></script>

<script type="text/javascript" src="http://cache.amap.com/lbs/static/addToolbar.js"></script>
<style type="text/css">
    #container2{height: 800px;}
    .amap-marker-content{display: none}
    #sousuo,#location-delete,.prohibitModification{cursor: pointer;}
    .layui-layer-content{text-align: center;}
    .layui-layer-btn{text-align: center;}
    .layui-layer-btn0{position: static;width: auto;}
    .layui-layer-btn a{height: auto;line-height: 2}
    @media screen and (max-width: 1920px){
        #container2{
            height: 800px;
        }
    }
     @media screen and (max-width: 1366px){
        #container2{
            height: 500px;
        }
    }
</style>
<body>
<?php {?>

<?php }?>
<div class="popup-boss">
	  <div class="revise-title">
        <div style="width: 500px;height: 55px;display: inline-block;" class="revise-title-btn">
            <button class="conservation" style="cursor:pointer">保存</button>
            <a href="/technician/index"><div style="cursor:pointer">&nbsp;&nbsp;取消&nbsp;&nbsp;</div></a>
       <!--      <label class="jinzhixiug" style="margin:10px"><i class="prohibitModification " onload="zx()"></i><span style="margin-left: 20px;">禁止技师修改接单范围</span></label>-->
        </div>
        
       
    </div>
	<div class="revise-location">
            <div class="revise-location-dizhi">
                <input type="text" name="" id="import-location" maxlength="150" value="<?php echo isset($data['service_address'])?$data['service_address']:'';?>"><i class="icon1-cha" id="location-delete" style="font-size: 16px;color: #ccc;background: #fff"></i><span id="searchBar"><i class="icon iconfont icon-sousuo1-copy" id="sousuo"></i></span>
            </div>
            <div class="revise-location-km">
                <p style="margin-bottom: 0" class="revise-location-p"> 接单范围半径：<span class="receivingRange"><?php echo isset($data['service_radius'])?$data['service_radius']:'5';?></span> km</p>
                <ul class="revise-location-gongli">
                    <?php for($i=5;$i<51;$i++){ ?>
                            <li><?php echo $i; ?> </li>
                    <?php }?>
                   
                </ul>
            </div>
     </div>
     <div>
         <div id="container2"></div>
    </div>
    <input type="hidden" id="technicianId" data-id="<?php echo isset($data['technician_id'])?$data['technician_id']:'';?>" data-lon="<?php echo isset($data['lon'])?$data['lon']:'';?>" data-lat="<?php echo isset($data['lat'])?$data['lat']:'';?>" data-address="<?php echo isset($data['service_address'])?$data['service_address']:'';?>" data-type= "<?php if(empty($data['service_address']) && empty($data['lon']) && empty($data['lat'])){ echo '1';}else{echo '2';};?>" data-radius="<?php echo isset($data['service_radius'])?$data['service_radius']:'';?>" data-Isban = "<?php echo isset($data['is_ban'])?$data['is_ban']:'';?>">
</div>
<script type="text/javascript" src="<?php echo Url::to('/js/edit-area.js');?>"></script>

</body>
</html>
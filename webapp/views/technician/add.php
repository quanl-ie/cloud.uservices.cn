<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\Department;

$this->title = isset($infos['id']) ? '编辑技师' :'新增技师';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">

<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4{
        padding-left: 0;
    }
    input[type=radio]{
        margin-top: 10px;
    }
    body{
        overflow-x: hidden;
        font-family: "微软雅黑"
    }
    .information-list{height: 37px;}
    .greneral-box{background: #f1f2f7;padding: 30px 50px;}
    .cont-box{padding:10px 30px  60px 30px;background: #fff;box-shadow: 0 0 30px #ccc}
    .main-content-title {display: inline-block; margin-right: 15px;font-size: 22px; background: #fff;}
    .fubiaoti {color: #2693ff;}.tup{height: 160px}
    .radio-list{line-height: 30px;}
    .aptitude-div{width: 100px;height: 100px;}
    .name-title-lable{width: 120px}
    .addinput-list{margin-left: 120px;}
    .add-zhaopian {width: 254px;height: 153px;display: inline-block;vertical-align: top;position: relative;}

.add-zhaopian>img{width: 100%;height: 100%;}
.filePrew{width: 100%;height: 100%;overflow: hidden;position: absolute;left: 0;bottom: 16px;z-index: 4;}
.tup:nth-chlid(1){
    height: 165px
}
.img-edit{position: absolute;width: 100px;height: 30px;background: rgba(0,0,0,0.5);z-index: 3;bottom: 15px;text-align: center;line-height: 30px;;color: #fff;display: none;}
.linkage-justify-list{width: 30%;}
@media screen and (max-width:1300px){
    .add-zhaopian{width: 200px;height: 120px;}
    .tup{height: 130px;}
}
@media screen and (max-width:1000px){
    .add-zhaopian{width: 45%;height: 120px;}
    .tup{height: 130px;}
}
#mengcheng{width: 82px;height: 34px;text-align: center;line-height: 32px;background: #ccc;position: absolute;left: 0;top: 0;border-radius: 4px;display: none;}
</style>
<body>
<section id="main-content">
    <div class="panel-body">
        <div>
            <h3 class="main-content-title"><?php echo isset($infos['id']) ? '编辑技师' :'新增技师';?></h3>
            <span class="fubiaoti">请按照上级要求填写相关信息</span></div>
        <?php
        $form = ActiveForm::begin([
                'options' => [
                    'class' => 'form-horizontal',
                    'id' => 'technician-add',
                    'name' => 'form1',
                    'enctype' => 'multipart/form-data',
                ]
        ]);
        ?>
            <input type="hidden" value="<?php echo isset($infos['id']) ? $infos['id'] :'';?>" id="id" name="id">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                <label class="name-title-lable" >
                    <i class="red-i">*</i>登录手机号：
                </label>
                <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                    <input type="text" value="<?php echo isset($infos['mobile']) ? $infos['mobile'] :'';?>" ajaxurl="/technician/ajax-has-repeat?id=<?php echo isset($infos['id'])?$infos['id']:0; ?>" name="mobile" id="mobile" datatype="/^[1][3,4,5,6,7,8,9][0-9]{9}$/" placeholder="请输入手机号" sucmsg=" " nullmsg="请输入手机号" errormsg="手机号格式错误" maxlength="11" class="form-control inputxt" />
                </div>
                <div class="Validform_checktip Validform_wrong"></div>
            </div>
            <?php if(!$infos['id']){?>
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                    <label class="name-title-lable" >
                        <i class="red-i">*</i>初始密码：
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                        <input type="text" value="" onfocus="this.type='password'"  name="password" datatype="*6-18" placeholder="请输入6~18位的密码" sucmsg=" " nullmsg="请输入初始密码" errormsg="密码格式错误" maxlength="18" class="form-control inputxt" />
                    </div>
                    <div class="Validform_checktip Validform_wrong"></div>
                </div>
            <?php }?>
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                <label class="name-title-lable" >
                    <i class="red-i">*</i>技师姓名：
                </label>
                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                    <input type="text" value="<?php echo isset($infos['name']) ? $infos['name'] :'';?>" name="name" class="form-control inputxt" datatype="*" placeholder="请输入技师姓名" sucmsg=" " nullmsg="请输入技师姓名" maxlength="20" />
                </div>
                <div class="Validform_checktip Validform_wrong"></div>
            </div>


        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list" style="width: ">
            <label class="name-title-lable" for="workorder-account_id">
                <i class="red-i">*</i> 所属机构：
            </label>
            <span class="c_red"></span>
            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                <?php if($countNoFinshWork == 0):?>
                <input type="text" class="form-control" id="category1" value="<?php echo isset($defult_department) ? $defult_department : '';?>" placeholder="请选择上级机构" readonly  sucmsg="" nullmsg="请选择机构">
                <input type="hidden" id="select_department_id" value="<?php echo isset($infos['store_id']) ? $infos['store_id'] : $defult_department_id; ?>" name="store_id" datatype="*" nullmsg="请选择机构">
                <i class="icon iconfont icon-shouqi1"></i>
                <p class="misplacement" style="bottom: -5px;" id="work_type_msg">请选择上级机构</p>
                <div class="drop-dw-layerbox">
                    <ul>
                        <?php if (!empty($department)) : ?>
                            <?php foreach ($department as $key => $val) : ?>
                                <li>
                         <span class="is_department">
                            <?php if ($val['exist'] == 1) : ?>
                                <i class="icon2-shixinyou"  onclick="clickCategory(this) "></i>
                                <input type="radio" value="<?=$val['id'] ?>" name="department_id" id="<?=$val['id'] ?>" class="pr1" onclick="changeCategory(this)">
                            <?php else:?>
                                <input type="radio" value="<?=$val['id'] ?>" name="department_id" id="<?=$val['id'] ?>" class="pr1" onclick="changeCategory(this)">
                            <?php endif;?>
                             <label for="<?=$val['id'] ?>"><?=$val['name'] ?></label>
                        </span>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
                <?php else:?>
                    <span class="form-control" id="noEdit"><?php echo Department::getDepartmentName($infos['store_id']);?></span>
                    <input type="hidden" name="store_id"  value="<?php echo $infos['store_id']; ?>" />
                    <span style="color:red;display: none;" id="noEditSpan">该技师还有未完成的订单，不可修改所属机构</span>
                <?php endif;?>
            </div>
        </div>

            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list" style="height: 27px">
                <label class="name-title-lable">
                性别：
                </label>
                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                    <?php if ( !empty($infos['sex']) && $infos['sex'] == 1) :?>
                        <div  class="radio-list">
                            <input type="radio" value="1" name="sex" id="male" class="pr1" checked="checked" /><label for="male">男</label>
                        </div>
                        <div  class="radio-list">
                            <input type="radio" value="2" name="sex" id="female" class="pr1" /><label for="female">女</label>
                        </div>

                    <?php elseif (!empty($infos['sex']) && $infos['sex'] == 2): ?>
                        <div  class="radio-list">
                            <input type="radio" value="1" name="sex" id="male" class="pr1" /><label for="male">男</label>
                        </div>
                        <div  class="radio-list">
                            <input type="radio" value="2" name="sex" id="female" class="pr1" checked="checked" /><label for="female">女</label>
                        </div>
                    <?php else:?>
                        <div  class="radio-list">
                            <input type="radio" value="1" name="sex" id="male" class="pr1" checked="checked" /><label for="male">男</label>
                        </div>
                        <div  class="radio-list">
                            <input type="radio" value="2" name="sex" id="female" class="pr1" /><label for="female">女</label>
                        </div>
                    <?php endif;?>
                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list" style="height: 105px">
                <label class="name-title-lable" >
                    头像：
                </label>
                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                    <div class="aptitude-div" style="height: 125px">
                        <img src="<?php echo isset($infos['icon']) ? $infos['icon'].'?x-oss-process=image/resize,m_fixed,h_100,w_100' : Url::to('/images/shangchuantouxiang.png'); ?>" class="img-aa" style="height: 100px">
                        <input type="file" name="icon" class="filePrew" id="touxiang" value="<?php echo isset($infos['icon']) ? $infos['icon'] : ''; ?>" <?php echo isset($infos['id'])?'':'' ;?> sucmsg=" " maxlength="255" accept="image/gif,image/GIF,image/png,image/PNG,image/jpg,image/JPG,image/jpeg,image/JPEG" style="height: 100px;">

                        <p class="img-edit">编辑</p>
                    </div>
                    <span class="error-span"></span>
                </div>
                <div class="Validform_checktip Validform_wrong"></div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                <label class="name-title-lable">

                所在城市：
                </label>
                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                    <div class="linkage-justify-list">
                        <select class="col-md-4 col-sm-4 col-xs-4 form-control" id="province" value="<?php echo isset($infos['province'])?$infos['province']:'';?>" name="province" sucmsg=" " nullmsg="请选择所在区域">
                            <option value="">请选择省</option>
                            <?php foreach ($province as $key => $val){?>
                                <option value="<?php echo $val['region_id'] ?>" <?php if(isset($infos['province']) && $infos['province']==$val['region_id']){?>selected<?php };?>><?=$val['region_name'] ?></option>
                            <?php };?>
                        </select>
                    </div>
                    <div class="linkage-justify-list">
                        <select class="form-control" id="city" value="<?php echo isset($infos['city'])?$infos['city']:'';?>" name="city" sucmsg=" " nullmsg="请选择所在区域">
                            <?php if (!empty($infos['city'])) :?>
                                <option value="<?=$infos['city'] ?>" selected="selected" ><?php echo $infos['city_name'];?></option>
                            <?php else :?>
                                <option value="">请选择市</option>
                            <?php endif ;?>
                        </select>
                    </div>
                    <div class="linkage-justify-list">
                        <select class="form-control" id="district" value="<?php echo isset($infos['district'])?$infos['district']:'';?>" name="district"  sucmsg=" " nullmsg="请选择所在区域">
                            <?php if (!empty($infos['district'])) :?>
                                <option value="<?php echo $infos['district'] ?>" selected="selected" ><?php echo $infos['district_name'] ?></option>
                            <?php else :?>
                                <option value="">请选择区</option>
                            <?php endif ;?>
                        </select>
                    </div>
                    <div class="justify_fix"></div>
                    <span class="error-span"></span>
                    <div class="Validform_checktip Validform_wrong"></div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                <label class="name-title-lable" >
                    身份证号码：
                </label>
                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                    <input type="text" id="identity_card" <?php echo isset($infos['id'])?'readonly="true"':'' ;?> value="<?php echo isset($infos['identity_card']) ? $infos['identity_card'] :'';?>" name="identity_card" class="form-control inputxt" ignore="ignore" datatype="/^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/" placeholder="请输入技师身份证号" sucmsg=" " maxlength="18"  errormsg="身份证号格式不正确" />
                </div>
                <div class="Validform_checktip Validform_wrong"></div>
            </div>
            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12 information-list tup" style="padding-top: 5px;">
                <label class="name-title-lable" >
                    身份证正面照片：
                </label>
                <div class="details-column-content">
                    <div class="add-zhaopian">
                        <img src="<?php echo isset($infos['identity_front']) ? $infos['identity_front'].'?x-oss-process=image/resize,m_fixed,h_150,w_250' : Url::to('/images/zhengmian.png'); ?>" class="img-aa">
                        <input type="file" name="identity_front" class="filePrew" value="<?php echo isset($infos['identity_front']) ? $infos['identity_front']:'' ;?>" <?php echo isset($infos['id'])?'':'' ;?> sucmsg=" " maxlength="255" nullmsg="请上传身份证正面照片" accept="image/gif,image/GIF,image/png,image/PNG,image/jpg,image/JPG,image/jpeg,image/JPEG">
                    </div>
                    <div  class="add-zhaopian" style="margin-left: 30px">
                        <img src="<?php echo Url::to('/images/zhegnmianshili.png');?>">
                    </div>
                    <span class="error-span"></span>
                </div>
                <div class="Validform_checktip Validform_wrong"></div>
            </div>
            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12  information-list tup" >
                <label class="name-title-lable" >
                    身份证反面照片：
                </label>
                <div class="details-column-content">
                    <div class="add-zhaopian">
                        <img src="<?php echo isset($infos['identity_behind']) ? $infos['identity_behind'].'?x-oss-process=image/resize,m_fixed,h_150,w_250' : Url::to('/images/fanmian.png'); ?>" class="img-aa">
                        <input type="file" name="identity_behind" maxlength="255" class="filePrew" value="<?php echo isset($infos['identity_behind']) ? $infos['identity_behind'] : ''; ?>" <?php echo isset($infos['id'])?'':'' ;?> sucmsg=" " nullmsg="请上传身份证反面照片" accept="image/gif,image/GIF,image/png,image/PNG,image/jpg,image/JPG,image/jpeg,image/JPEG">
                    </div>
                    <div   class="add-zhaopian" style="margin-left: 30px">
                        <img src="<?php echo Url::to('/images/fanmianshili.png');?>">
                    </div>
                    <span class="error-span"></span>
                </div>
                <div class="Validform_checktip Validform_wrong"></div>
            </div>
            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12 information-list tup">
                <label class="name-title-lable" >
                    手持身份证照片：
                </label>
                <div class="details-column-content">
                    <div  class="add-zhaopian">
                        <img src="<?php echo isset($infos['identity_hold']) ? $infos['identity_hold'].'?x-oss-process=image/resize,m_fixed,h_150,w_250' : Url::to('/images/shouchishenfenzheng.png'); ?>" class="img-aa">
                        <input type="file" name="identity_hold" maxlength="255" class="filePrew" value="<?php echo isset($infos['identity_hold']) ? $infos['identity_hold'] : ''; ?>" <?php echo isset($infos['id'])?'':'' ;?> sucmsg=" " nullmsg="请上传手持身份证照片" accept="image/gif,image/GIF,image/png,image/PNG,image/jpg,image/JPG,image/jpeg,image/JPEG">
                    </div>
                    <div   class="add-zhaopian" style="margin-left: 30px">
                        <img src="<?php echo Url::to('/images/shouchishenfenzhengshili.png');?>">
                    </div>
                    <span class="error-span"></span>
                </div>
                <div class="Validform_checktip Validform_wrong"></div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 information-list">
                <label class="name-title-lable">技能：</label>
                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                    <button type="button" class="btn border-e8 radius" id="parentIframe" value="<?php echo isset($infos['id'])?$infos['id']:''?>">编辑技能</button>
                    <input type="hidden" name="skill" id="skillId" value="<?php echo isset($infos['skill'])?$infos['skill']:''?>"  sucmsg=" " nullmsg="编辑技能">
                </div>
            </div>
            <div class="col-md-8 information-list" style="height: auto;">
                <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                    <div id="skill">
                        <?php if($infos['skill_name']) foreach ($infos['skill_name'] as $key =>$val){?>
                            <td class="td2">
                                <p><?php echo $val;?></p>
                            </td>
                        <?php }?>
                    </div>
                </div>
            </div>





            <div  class="col-md-8 information-list">
                <label class="name-title-lable"></label>
                <div class="col-md-10 col-sm-10 col-xs-10  addinput-list">
                    <div class="right-btn" style="position: relative;">
                        <?php if($option):?>
                        <input type="submit" id="btnSubmit" value="提交" class="btn btn-orange mgr20">
                        <p id="mengcheng">提交中</p>
                        <?php else:?>
                            <input type="button" id="cannotsubmit" value="提交" class="btn btn-orange mgr20">
                        <?php endif;?>
                        <a href="/technician/index"><button type="button"  class="btn bg-f7" style="width: 82px">取消</button></a>
                    </div>

                </div>

            </div>

        <?php ActiveForm::end(); ?>
    </div>
</section>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>

<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=bbcdabda1427179d77bb95fac7b2b902&plugin=AMap.Geocoder"></script>
<script>
    //点击添加技能
    var index = null;
    $("#parentIframe").click(function () {
        var data = $("#skillId").val();
        var t_id = $("#parentIframe").val(); //技师id
        index = layer.open({
            type: 2,
            title: '添加技能',
            area: ['550px', '350px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/technician/get-skills?id='+t_id
        });
    });

    // 图片上传
    $('.filePrew').change(function () {
        var fil = this.files;
        if(fil.length >0)
        {
            var self = $(this).prev('.img-aa');
            var file_type = fil[0].name;
            var size = fil[0].size;
            if(size >= 1*1024*1024){
                alert('图片不能大于1M!');
                return false;
            }
            if(file_type){
                if(checkImgType(file_type)){
                    for (var i = 0; i < fil.length; i++) {
                        var reader = new FileReader();
                        reader.readAsDataURL(fil[i]);
                        reader.onload = function()
                        {
                            self.attr('src',reader.result);
                            if($('#touxiang').prev('img').attr('src')!='/images/shangchuantouxiang.png'){
                                $('.img-edit').show();
                            }
                        };
                    }
                }
            }
        }
        else
        {
            if($(this).attr('name') == 'identity_front'){
                $(this).prev('.img-aa').attr('src','/images/zhengmian.png');
            }
            else if($(this).attr('name') == 'identity_behind'){
                $(this).prev('.img-aa').attr('src','/images/fanmian.png');
            }
            else if($(this).attr('name') == 'identity_hold'){
                $(this).prev('.img-aa').attr('src','/images/shouchishenfenzheng.png');
            }
        }
    });
    //验证图片格式
    function checkImgType(ths){
        if (ths == "") {
            alert("请上传图片");
            return false;
        } else {
            if (!/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/.test(ths)) {
                alert("图片类型必须是.jpg,png,gif中的一种");
                ths = "";
                return false;
            }
        }
        return true;
    };
    $('#identity_card').blur(function() {
        var str = $(this).val();
        var type = 'i';
        str = $.trim(str);
        if(str !== ''){
            Vinfos(str,type);
        }
    });
    function Vinfos(str,type){
        $.post('/technician/verify-data',{str:str,type:type},function(data) {
            var res = $.parseJSON(data);
            if(res.code != 200){
                alert(res.msg);
            }
        })
    }
    //表单验证
    $("#technician-add").Validform({
        tiptype:3,
    });

    //关闭iframe
    $('#closeIframe').click(function(){
        parent.layer.close(index);
    });
    //根据开通城市父级id获取市级数据
    $("#province").change(function() {
        $("#city").html("<option value=''>请选择市</option>");
        $("#district").html("<option value=''>请选择区</option>");
        var pid = $("#province").val();
        if(pid != ''){
            $.get("/technician/get-city", { "province_id": pid }, function (data) {
                $("#city").html(data);
            });
        }
    });
    //根据城市获取区县
    $("#city").change(function() {
        var pid = $("#city").val();
        if(pid != ''){
            $.get("/technician/get-district", { "city_id": pid }, function (data) {
                $("#district").html(data);
            })
        }
    });
    //转换经纬度
    $('#address').blur(function(){
        //地理编码,返回地理编码结果
        var province = $("#province").find('option:selected').html();
        var city = $("#city").find('option:selected').html();
        var district = $("#district").find('option:selected').html();
        var address = $('#address').val();
        var address_all = province + city + district + address;
        var geocoder = new AMap.Geocoder({
            city: "010", //城市，默认：“全国”
            radius: 1000 //范围，默认：500
        });
        geocoder.getLocation(address_all, function(status, result) {
            if (status === 'complete' && result.info === 'OK') {
                var arr = result.geocodes[0].location;
                $('#lon').val(arr.lng);
                $('#lat').val(arr.lat);
            }
        });
    });
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };


    function  clickCategory(el) {
        var auth = 1;
        var id = $(el).next('input').val();
        var _this = $(el).parent('span');
        $.getJSON('/common/ajax-get-department',{'pid':id,'auth':auth,'type':1},function (data) {
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>"
                $(data.data).each(function(i,el) {
                    ulData +="<li><span>";
                    el.exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="clickCategory(this)"></i>':'';
                    ulData+='<input type="radio" value="'+el.id+'" name="department_id" id="'+el.id+'" class="pr1" onclick="changeCategory(this)"><label for="'+el.id+'">'+el.name+'</label></span></li>';
                });
                ulData +="</ul>";
                _this.parent('li').append(ulData);
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');

            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })

    }

    $('#category1').on('click',function() {
        var lenth = $('.is_department').text();
        if(lenth.length != 0) {
            $('.drop-dw-layerbox').is(':hidden')?$('.drop-dw-layerbox').show():$('.drop-dw-layerbox').hide();
            $('.drop-dw-rolebox').hide();
            $('.role').val('');
            $('#role_Select').val('');
        }
    })

    function changeCategory(el) {
        $('.drop-dw-layerbox').hide();
        $('#select_department_id').val($(el).next('label').attr("for"));
        $('#category1').val($(el).next('label').text());
    }
    $('#noEdit').click(function () {
        $('#noEditSpan').show();
        setTimeout(function () {
            $('#noEditSpan').hide();
        },3000);
    });

    $("#cannotsubmit").click(function(){
        alert('该权限下无法新增技师，如有问题，请联系管理员');
    })
</script>
</body>
</html>
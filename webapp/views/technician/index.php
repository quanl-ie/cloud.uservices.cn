<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use webapp\models\Department;

$this->title = '技师';
$this->params['breadcrumbs'][] = $this->title;

?> 
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time();?>">
<style type="text/css">
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
    .search-box-kuang>.col-sm-6{
        padding-left: 0;
        position: relative;
    }
    .icons-calendar{
        position: absolute;
        right: 24px;top: 9px;
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">技师列表</span>
</div> 
<section id="main-content">
<div class="row">
    <div class="col-md-12">                    
        <div class="panel panel-default">                     
            <div class="panel-body">
                <form action="/technician/index" class="form-horizontal form-border" id="form">
                    <div class="form-group jn-form-box">
                        <div class="col-sm-12 no-padding-left">

                            <?php if(!empty($department)):?>
                                <div class="single-search region-limit" style="overflow:visible;">
                                    <label class=" search-box-lable">所属机构</label>
                                    <input type="hidden" id="department_id" name="department_id" value="<?php echo $department_id;?>">
                                    <div class="single-search-kuang1" id="de">
                                        <input type="text" class="form-control" value="<?=isset($department_name) ? $department_name : '' ?>" id="department"  readonly <?php if(!empty($department)){ echo 'onclick="clickCategory()" placeholder="请选择机构"';}?> >
                                        <input type="hidden" class="form-control" value="<?=isset($params['department_id']) ? $params['department_id'] : '' ?>" id="department_ids" name="department_ids">
                                        <div class="drop-dw-layerbox" style="display: none;">
                                            <div class="ul-box-h180">
                                                <ul id="depart1" class="depart">
                                                    <?php if (!empty($department)) : ?>
                                                        <?php foreach ($department as $key => $val) : ?>
                                                            <li>
                                                             <span>
                                                                <?php if ($val['exist'] == 1) : ?>
                                                                    <i class="icon2-shixinyou" onclick="getDepartment(this)"></i>
                                                                <?php endif;?>
                                                                 <input type="checkbox" id="a<?=$val['id'] ?>" value="<?=$val['id'] ?>" name="department_id[]" <?php if(isset($params['department_id']) && (in_array($val['id'],explode(",",$params['department_id'])))){ echo "checked=true"; }?>>
                                                                 <label for="a<?=$val['id'] ?>"><?=$val['name'] ?><i class="gou-i"></i></label>
                                                            </span>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                            <button class="btn btn-orange" id="belongedBtn">确定</button>
                                        </div>

                                    </div>

                                </div>
                            <?php endif?>
                            <div class="single-search">
                                <label class=" search-box-lable">技师姓名</label>
                                <div class="single-search-kuang1">
                                    <input type="text" class="form-control" name="technician_name" maxlength="20" value="<?=isset($params['technician_name']) ? $params['technician_name'] : '' ?>">
                                </div>
                            </div>

                            <div class="single-search">
                                <label class=" search-box-lable">技师电话</label>
                                <div class="single-search-kuang1">
                                    <input type="text" class="form-control" name="technician_mobile" maxlength="11" value="<?= isset($params['technician_mobile']) ? $params['technician_mobile'] : '' ?>">
                                </div>
                            </div>

                            <div class="single-search">
                                <label class=" search-box-lable">服务类型</label>
                                <div class="single-search-kuang1">
                                    <select class="form-control" name="service_type">
                                        <option value="">请选择</option>
                                        <?php foreach ($serviceType as $key=>$val){?>
                                        <option value="<?php echo $key; ?>" <?php if(isset($_GET['service_type']) && $_GET['service_type']==$key):?>selected<?php endif; ?>><?php echo $val;?></option>
                                        <?php };?>
                                    </select>
                                </div>
                            </div>
                            <div class="search-confirm">
                                <button class="btn btn-success">
                                    <i class=" "></i> 搜索
                                </button>
                            </div>
                        </div>
                    </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead bgcolor="#455971">
                            <tr>
                                <th>技师编号</th>
                                <th>所属机构</th>
                                <th>技师姓名</th>
                                <th>联系电话</th>
                                <th>技能信息</th>
                                <th>实时位置</th>
                                <!--<th>接单范围</th> -->
                                <th>接单情况</th>
                                <?php if ($sysConfig == 1) : ?>
                                <th>备件信息</th>
                                <?php endif; ?>
                                <th>账号状态</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($data){?>
                            <?php foreach ($data as $key=>$val){?>
                                <tr>
                                    <td>
                                        <?php if(in_array('/technician/detail',$selfRoles)){?>
                                            <a href="/technician/detail?technician_id=<?php echo $val['id'] ?>"><?php echo isset($val['technician_number']) ? $val['technician_number'] : ''; ?></a>
                                        <?php }else{;?>
                                            <?php echo isset($val['technician_number']) ? $val['technician_number'] : ''; ?>
                                        <?php }?>
                                    </td>
                                    <td><?=$val['department_name'] ?></td>
                                    <td><?php echo $val['name']; ?></td>
                                    <td><?php echo isset($val['mobile']) ? $val['mobile'] : ''; ?></td>
                                    <td>
                                        <?php if(in_array('/technician/get-skill',$selfRoles)){?>
                                            <a href="" class="viewSkill" id-data="<?php echo $val['id'];?>">点击查看</a>
                                        <?php };?>
                                    </td>
                                    <td>
                                        <?php if(in_array('/technician/view-location',$selfRoles)){?>
                                            <a href="" class="viewLocation" id-data="<?php echo $val['id'];?>">点击查看</a>
                                        <?php };?>
                                    </td>
                                    <!--
                                    <td>
                                        <?php if(in_array('/technician/edit-area',$selfRoles)){?>
                                            <a href="" class="EditArea" id-data="<?php echo $val['id'];?>">点击修改</a>
                                        <?php };?>
                                    </td>
                                    -->
                                    <td>
                                        <?php if(in_array('/technician/technician-order',$selfRoles)){?>
                                            <a href="/technician/technician-order?technician_id=<?php echo isset($val['id'])?$val['id']:''?>">查看明细</a>
                                        <?php };?>
                                    </td>
                                    <?php if ($sysConfig == 1) : ?>
                                        <td>
                                            <?php if(in_array('/tech-storage/index',$selfRoles)){?>
                                                <a href="/tech-storage?technician_id=<?php echo isset($val['id'])?$val['id']:''?>">点击查看</a>
                                            <?php };?>
                                        </td>
                                    <?php endif; ?>
                                    <td><?php if($val['status'] == 1){echo '正常';}else{echo '禁用';}?></td>
                                    <td class="operation">
                                        <?php if(in_array('/technician/edit-info',$selfRoles)){?>
                                            <a href="/technician/edit-info?id=<?php echo $val['id'];?>" class="update">编辑</a>
                                        <?php };?>
                                        <?php if(in_array('/technician/reset-pwd',$selfRoles)){?>
                                            <a href="javascript:;" onclick="editChick(<?php echo $val['id']?>)">重置密码</a>
                                        <?php };?>
                                        <?php if($val['status']==1){?>
                                            <?php if(in_array('/technician/set-status',$selfRoles)){?>
                                                <a href="/technician/set-status?id=<?php echo $val['id'];?>&t=1&technician_id=<?=$val['id']?>&plan_start_time=<?=date('Y-m-d')?>&check_technician=`" onclick="return confirm('您确定要禁用吗？',function(obj){ document.location.href = obj.href ;} , this);">禁用</a>
                                            <?php };?>
                                        <?php }else{?>
                                            <?php if(in_array('/technician/set-status-on',$selfRoles)){?>
                                                <a href="/technician/set-status-on?id=<?php echo $val['id'];?>&t=2" onclick="return confirm('您确定要启用吗？',function(obj){ document.location.href = obj.href ;} , this);">启用</a>
                                            <?php };?>
                                        <?php }?>
                                    </td>
                                </tr>
                            <?php }}else {?>
                                    <tr><td colspan="11" class="no-record">暂无数据</td></tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
                </form>
                <div class="col-xs-12 text-center pagination">
                    <?php echo $pageHtml;?>
                </div>
            </div> 
       </div>
   </div>
 </div>
</section>
<script>
//查看技能
$(".viewSkill").click(function(){
   var id = $(this).attr("id-data");
   layer.open({
        type: 2,
        title: ['技能信息', 'font-size:18px;'],
        area: ['700px', '400px'],
        fixed: false, //不固定
        maxmin: false,
        content: '/technician/get-skill?id='+id,
    });
   return false;
});
//查看实时位置
$(".viewLocation").click(function(){
    var id = $(this).attr("id-data");
    layer.open({
        type: 2,
        title: ['技师位置', 'font-size:18px;'],
        area: ['800px', '500px'],
        fixed: false, //不固定
        maxmin: false,
        content: '/technician/view-location?id='+id,
    });
    return false;
});
var index = parent.layer.getFrameIndex(window.name);
//修改技师位置
$(".EditArea").click(function(){
    var id = $(this).attr("id-data");
    $.ajax({
        url:'/technician/verify',
        data:{id:id},
        type:'POST',
        dataType:'json',
        success:function(data){
            if (data.success == false) {
                layer.confirm(data.message, {
                    btn: ['立即设置'] //按钮
                }, function(){
                    location.href='/technician/edit-area?id='+id;
                });
            }else{
                location.href='/technician/edit-area?id='+id;
            }
        }
    });
    return false;
});
//重置密码
function editChick(id){
    layer.open({
        type: 2,
        title: '重置密码',
        area: ['400px', '300px'],
        fixed: false, //不固定
        maxmin: false,
        scrollbar: false,
        content: '/technician/reset-pwd?id='+id,
        end:function(){
            location.reload();
        }
    });
}
//设置状态 2018-5-15
//==========js开始==================//
$(".edit-status").click(function(){
    var id = $(this).attr("id-data");
    $.ajax({
        url:'/technician/technician-order',
        type:'POST',
        datatype:'json',
        data:{technician_id:id},
        success:function(data){
            if(data !=''){      //改技师还有未完成的工单，不能禁用
                alert('该技师还有未完成工单，无法禁用！请完成工单后再禁用该技师。');
            }else {             //改技师没有未完成的工单，可以禁用
                set_status(id,1);
            }
        }
    })
    return false;
});
function set_status(id,status) {
    var url="/technician/set-status?id="+id+"&t="+status;
    return confirm('您确定要禁用吗？',function(){
        document.location.href = url ;
        } , this);
}
//==========js结束==================//
window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    <?php endif;?>
};
var departmentIdArray = $('#department_ids').val().split(',');
function getDepartment(el) {
    var id = $(el).next('input').val();
    var _this = $(el).parent('span');
    var auth = <?php echo $auth;?>;
    var company_id = <?php echo $company_id;?>;
    var self_id = $("#department_id").val();
    $.getJSON('/common/ajax-get-department',{'pid':id,"auth":auth,'self_department_id':self_id,"company_id":company_id},function (data) {
        var ulData = '';
        if (_this.next('ul').size()==0) {
            ulData +="<ul>"
            $(data.data).each(function(i,el) {
                ulData +="<li><span>";
                el.exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="getDepartment(this)"></i>':'';
                ulData+='<input type="checkbox" id="a'+el.id+'" value="'+el.id+'" name="department_id[]" >  <label for="a'+el.id+'">'+el.name+'<i class="gou-i"></i></label></span></li>';
            });
            ulData +="</ul>";
            _this.parent('li').append(ulData);
            $.each(departmentIdArray,function(i,el) {
                $('#a'+el).prop('checked',true);
            })
            $(el).removeClass('icon2-shixinyou');
            $(el).addClass('icon2-shixinxia');

        }else{
            if(
                _this.next('ul').is(':hidden')){
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');
                _this.next('ul').show();

            }else{
                $(el).addClass('icon2-shixinyou');
                $(el).removeClass('icon2-shixinxia');
                _this.next('ul').hide();
            }
        }
        return false;
    })
}

function  clickCategory(el) {
    //event.stopPropagation();
    $.each(departmentIdArray,function(i,el) {
        $('#a'+el).prop('checked',true);
    })
    $('.drop-dw-layerbox').is(':hidden')? $('.drop-dw-layerbox').show(): $('.drop-dw-layerbox').hide();
}

// 选中内容
$('#belongedBtn').bind('click', function() {
    //event.preventDefault();
    var id_array= [];
    var data_array=[];
    $('input[name="department_id[]"]:checked').each(function(){
        data_array.push($(this).next('label').text());
        id_array.push($(this).val());//向数组中添加元素
    });
    var idstr=id_array.join(',');//将数组元素连接起来以构建一个字符串
    var datatr = data_array.join('；')
    $('#department').val(datatr);
    $('#department_ids').val(idstr);
    $('.drop-dw-layerbox').hide();
    return false;
});

$(document).click(function(event) {
    var region = $('.region-limit');
    if (!region.is(event.target)&&region.has(event.target).length ===0) {
        $('.drop-dw-layerbox').hide();
    }
});
</script>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$this->title = '技师详情';
$this->params['breadcrumbs'][] = ['label' => '技师列表', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
		border-top:2px solid #2693ff;
	}
    .details-column-title{
        width: 115px;
    }
	.riqi1{
		position: relative;
	}
	body{
		background: #f1f2f7;
	}
	.information-nav-a:link{
		display: inline-block;
		color: #323232;
		margin-left: 54px;
		font-size: 14px;
	}
	.information-nav-a:hover{
		text-decoration: none;
		color: #2693ff;
	}
	.filePrew{
		font-size: 20px;
	}
	.details-column-content{
		margin-left: 130px;
	}
    label{font-weight: 100}
    .details-column{margin-top: 10px}
    .fbt{font-size: 16px;font-weight: 600;font-family: "微软雅黑";margin:20px 0 0 0;}
    .details-column-tupian>a>img{
        width: 100%;height: 100%;
    }
    
</style>
<section id="main-content">
	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                	<!-- 内容 -->
                	<div id="" class="tab-content col-md-12">
                            <h5 class="fbt">技师详情</h5>
                			<div class="details-column">
                				<span class="details-column-title">技师编号：</span>
                                <div class="details-column-content">
                                    <?php echo $data['technician_number'];?>
                                </div>
                			</div>
							<div class="details-column">
                				<span class="details-column-title">技师姓名：</span>
                				<div class="details-column-content">
                                    <?php echo $data['name'];?>
                                </div>
                            </div>
							<div class="details-column">
                				<span class="details-column-title">性别：</span>
                				<div class="details-column-content">
                				    <?php if($data['sex']==1){echo '男';}else {echo '女';}?>
                				</div>
                			</div>
                			<div class="details-column">
                				<span class="details-column-title">联系方式： </span>
                				<div class="details-column-content">
                				    <?php echo $data['mobile'];?>
                				</div>
                			</div>
                			<div class="details-column">
                				<span class="details-column-title">头像：</span>
                				<div class="details-column-content">
                					<div class="details-column-tupian" style="height: 110px">
                                        <a href="#">
                                            <img src="<?php echo isset($data['icon'])?$data['icon'].'?x-oss-process=image/resize,m_fixed,h_100,w_100':Url::to('/images/morentouxiang.png');?>" style="width: 100px;height: 100px;">
                                        </a>
                					</div>
                				</div>
                			</div>
                         <div class="details-column">
                                <span class="details-column-title">工作城市：</span>
                                <div class="details-column-content">
                                    <?php echo isset($data['work_city'])?$data['work_city']:'';?>
                                </div>
                            </div>
                        <?php if($data['audit_status'] == '5'){?>
                            <div class="details-column">
                                <span class="details-column-title">账号状态：</span>
                                <div class="details-column-content">
                                    <?php if($data['status']==1){echo '正常';}else {echo '禁用';}?>
                                </div>
                            </div>
                        <?php }?>

                			<div class="details-column">
                				<span class="details-column-title">身份证号：</span>
                				<div class="details-column-content">
                				<?php echo $data['identity_card'];?>
                				</div>
                			</div>
							<div class="details-column" style="margin-top: 18px">
                				<span class="details-column-title"> 身份证正面照：</span>
                				<div class="details-column-content">
                                    <?php if (isset($data['identity_front']) && $data['identity_front'] != ''): ?>
                                    <div class="details-column-tupian">
                                            <img class="enlarge-img"  src="<?php echo $data['identity_front'].'?x-oss-process=image/resize,m_fixed,h_150,w_250';?>">
                					</div>
                                    <?php endif; ?>
                                </div>
                			</div>
                			<div class="details-column" style="margin-top: 18px">
                				<span class="details-column-title"> 身份证反面照片：</span>
                				<div class="details-column-content">
                                    <?php if (isset($data['identity_behind']) && $data['identity_behind'] != ''): ?>
                                    <div class="details-column-tupian">
                                            <img class="enlarge-img" src="<?php echo $data['identity_behind'].'?x-oss-process=image/resize,m_fixed,h_150,w_250';?>">
                					</div>
                                    <?php endif; ?>
                                </div>
                			</div>
                			<div class="details-column" style="margin-top: 18px">
                				<span class="details-column-title">手持身份证照片：</span>
                				<div class="details-column-content">
                                    <?php if (isset($data['identity_hold']) && $data['identity_hold'] != ''): ?>
                                    <div class="details-column-tupian">
                                            <img class="enlarge-img"  src="<?php echo $data['identity_hold'].'?x-oss-process=image/resize,m_fixed,h_150,w_250';?>">
                					</div>
                                    <?php endif; ?>
                                </div>
                			</div>
                    </div>                        
                    
                </div>
            </div>
        </div>
    </div>
    <!-- <img src="" id="enlargeImg" style="display: none"> -->
</section>
<!-- 图片放大 -->
<div class="imgEnlarge-box">
    <div class="hezi">
        <div class="imgEnlarge-chlid">
             <img src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" alt="placeholder+image" id="enlargeImg">
        </div>
    </div>
</div>
<script>

// 图片放大
$('.enlarge-img').on('click',function() {
   var aa = $(this).attr('src');
   var bb = aa.substring(0,aa.indexOf('?'));
    $('#enlargeImg').attr('src',bb);
    $('.imgEnlarge-box').show();
})
$('.imgEnlarge-box').on('click',function() {
     $('.imgEnlarge-box').hide();
})
//查看技能
$(".viewSkill").click(function(){
   var id = $(this).attr("id-data");
   layer.open({
        type: 2,
        title: ['技能信息', 'font-size:18px;'],
        area: ['700px', '400px'],
        fixed: false, //不固定
        maxmin: false,
        content: '/technician/get-skill?id='+id,
    });
   return false;
});
//查看实时位置
$(".viewLocation").click(function(){
    var id = $(this).attr("id-data");
    layer.open({
        type: 2,
        title: ['技师位置', 'font-size:18px;'],
        area: ['800px', '500px'],
        fixed: false, //不固定
        maxmin: false,
        content: '/technician/view-location?id='+id,
    });
    return false;
});
var index = parent.layer.getFrameIndex(window.name);
//修改技师位置
$(".EditArea").click(function(){
    var id = $(this).attr("id-data");
    $.ajax({
        url:'/technician/verify',
        data:{id:id},
        type:'POST',
        dataType:'json',
        success:function(data){
            if (data.success == false) {
                layer.confirm(data.message, {
                    btn: ['立即设置'] //按钮
                }, function(){
                    location.href='/technician/edit-area?id='+id;
                });
            }else{
                location.href='/technician/edit-area?id='+id;
            }
        }
    });
    return false;
});
window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    <?php endif;?>
};
</script>
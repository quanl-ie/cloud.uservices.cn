<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$this->title = '实名认证信息审核';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
		border-top:2px solid #2693ff;
	}
    .details-column-title{
        width: 115px;
    }
	.riqi1{
		position: relative;
	}
	body{
		background: #f1f2f7;
        font-family: "微软雅黑";
        overflow-x:hidden; 
	}
	.information-nav-a:link{
		display: inline-block;
		color: #323232;
		margin-left: 54px;
		font-size: 14px;
	}
	.information-nav-a:hover{
		text-decoration: none;
		color: #2693ff;
	}
	.filePrew{
		font-size: 20px;
	}
	.details-column-content{
		margin-left: 130px;
	}
    label{font-weight: 100}
    .details-column{margin-top: 10px}
    .fbt{font-size: 16px;font-weight: 600;font-family: "微软雅黑";margin:20px 0 0 0;}
    .details-column-tupian>img{width:100%;height: 100%;}
    
</style>
<section id="main-content">
	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                	<!-- 内容 -->
                    <form class="form form-horizontal" id="form" method="post" action="">
                	<div id="" class="tab-content col-md-12">
                            <h5 class="fbt">实名认证信息审核</h5>
                			<div class="details-column">
                				<span class="details-column-title">技师编号：</span>
                                <div class="details-column-content">
                                    <?php echo $data['technician_number'];?>
                                </div>
                			</div>
							<div class="details-column">
                				<span class="details-column-title">技师姓名：</span>
                				<div class="details-column-content">
                                    <?php echo $data['name'];?>
                                </div>
                            </div>
							<div class="details-column">
                				<span class="details-column-title">性别：</span>
                				<div class="details-column-content">
                				    <?php if($data['sex']==1){echo '男';}else {echo '女';}?>
                				</div>
                			</div>
                			<div class="details-column">
                				<span class="details-column-title">联系方式： </span>
                				<div class="details-column-content">
                				    <?php echo $data['mobile'];?>
                				</div>
                			</div>
                            <div class="details-column">
                                <span class="details-column-title">工作城市：</span>
                                <div class="details-column-content">
                                    <?php echo isset($data['work_city'])?$data['work_city']:'';?>
                                </div>
                            </div>
                			<div class="details-column">
                				<span class="details-column-title">身份证号：</span>
                				<div class="details-column-content">
                				<?php echo $data['identity_card'];?>
                				</div>
                			</div>
							<div class="details-column" style="margin-top: 20px">
                				<span class="details-column-title"> 身份证正面照：</span>
                				<div class="details-column-content">
                					<div class="details-column-tupian">
                                         <a href="<?php echo $data['identity_front'];?>">
                                            <img src="<?php echo $data['identity_front'].'?x-oss-process=image/resize,m_fixed,h_150,w_250';?>">
                                         </a> 
                					</div>
                				</div>
                			</div>
                			<div class="details-column" style="margin-top: 20px">
                				<span class="details-column-title"> 身份证反面照片：</span>
                				<div class="details-column-content">
                					<div class="details-column-tupian">
                                         <a  href="<?php echo $data['identity_behind'];?>">
                                            <img src="<?php echo $data['identity_behind'].'?x-oss-process=image/resize,m_fixed,h_150,w_250';?>">
                                         </a> 
                					</div>
                				</div>
                			</div>
                			<div class="details-column" style="margin-top: 20px">
                				<span class="details-column-title">手持身份证照片：</span>
                				<div class="details-column-content">
                					<div class="details-column-tupian">
                                         <a href="<?php echo $data['identity_hold'];?>">
                                            <img src="<?php echo $data['identity_hold'].'?x-oss-process=image/resize,m_fixed,h_150,w_250';?>">
                                        </a>
                					</div>
                				</div>
                			</div>

                        <div class="details-column">
                            <span class="details-column-title"> 审核意见：</span>
                            <div class="details-column-content">
                                <textarea class="textarea" placeholder="200字以内（请输入审核意见）" maxlength="200" name="reason" id="reason" style="width: 340px;height: 133px"></textarea>
                            </div>
                        </div>
                        <!--操作按钮区域-->
                        <div class="details-column">
                            <div class="details-column-content">
                                <button type="button" class="btn btn-success radius" onclick="examine(<?php echo $data['id'];?>,1);">&nbsp;&nbsp;同意&nbsp;&nbsp;</button>
                                <button type="button" class="btn btn-danger radius ml-20" onclick="examine(<?php echo $data['id'];?>,2);">&nbsp;&nbsp;拒绝&nbsp;&nbsp;</button>
                                <button type="button" class="btn btn-default radius ml-20 closeIframe" >&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
                            </div>
                        </div>
                    </div>                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    function examine(id,type){
        if (type == 1) {
           var index = layer.confirm("确定要将此信息通过审核吗？", {
                    offset: ['160px', '350px'],
                    btn: ['确定', '取消'],
                    title: '提示',
                    icon: 3
                },
                function () {
                    ajaxPost(id,type,index);
                },
                function (index) {
                    layer.close(index);
                });
        }else{
            var index = layer.confirm("确定要将此信息驳回吗？", {
                    offset: ['160px', '350px'],
                    btn: ['确定', '取消'],
                    title: '提示',
                    icon: 3
                },
            function () {
                ajaxPost(id,type,index);
            },
            function (index) {
                layer.close(index);
            });
        }
    };
    function ajaxPost(id,type,index) {
        var reason = $('#reason').val();
        $.ajax({
            url: "/technician/set-examine",
            data: {
                'id': id,
                'type':type,
                'reason':reason,
            },
            type: 'POST',
            success:function (ret){
                var ret = $.parseJSON(ret);
                if (ret.code !=200) {
                    layer.close(index);
                    parent.layer.msg(ret.msg, {icon: 2, time: 2000});
                }else{
                    parent.layer.msg(ret.msg, {icon: 1, time: 2000});
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                    window.parent.location.reload();
                }
            }
        });
    }
    //关闭iframe
    $('.closeIframe').click(function(){
        close();
    });
    function close() {
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    }
</script>
<script>
window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    <?php endif;?>
};
</script>
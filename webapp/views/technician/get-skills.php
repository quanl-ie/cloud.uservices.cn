<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4{
        padding-left: 0;
    }
    input[type=radio]{
        margin-top: 10px;
    }
    body{overflow-x: hidden;font-family: "微软雅黑";background: #fff;}
    label{font-size: 14px;font-weight: 500;}
    .form-label{font-weight: 600;}
    .range-td>div{
        border-top: 1px solid #ccc;
    }
    .range-td>div:nth-child(1){border-top: 0px solid #ccc;}
    .popup-boss{width: 550px;overflow-x:hidden;padding-left: 20px}
    .skill-listbox{height: 200px;overflow-y: auto;}
    .abcdef{top: 0}
    .row{margin-top: 20px;margin-left: 30px;margin-right: 0;}
    input[type='checkbox'] + label{position: relative;}
    input[type="checkbox"] + label::before,input[type="checkbox"]:checked + label::before{left: 0;top: 5px;}
</style>
<body>
<div class="popup-boss" style="background: #fff;">
    <?php if(!empty($service_type)){?>
        <form action="/technician/get-skills" method="post" id="skillForm">
            <div class="skill-listbox">
                <?php foreach ($service_type as $key=>$val){?>
                    <div class="row cl skill-list">
                        <label class="form-label"><?php echo isset($val)?$val:''?></label>
                        <div class="formControls">
                            <input type="checkbox" name="" id="w<?php echo $key?>" class="whole">
                            <label for="w<?php echo $key?>">全部<i class="abcdef"></i></label>
                            <?php if(!empty($class_type)) foreach ($class_type as $k=>$v){?>
                                <input type="checkbox" class="skill" name="skill" id="-<?php echo $key.$v['id'];?>" value="<?php echo $key.'_'.$v['id'];?>" />
                                <label for="-<?php echo $key.$v['id'];?>" class="chlid-label"><?php echo isset($v['class_name'])?$v['class_name']:'';?><i class="abcdef"></i></label>&nbsp;&nbsp;
                            <?php }?>
                        </div>
                        <div class="col-xs-3 col-sm-3"></div>
                    </div>
                <?php }?>
            </div>
            <hr>
            <div  class="col-md-8 information-list" style="text-align: center;margin-bottom: 30px">
                
                        <input type="button" id="closeIframe" class="btn bg-f7 mgr8" value="取消">
                        <button id="transmit" class="btn btn-orange">确定</button>
            </div>
        </form>
    <?php }else{?>
        <div class="no-data-page" style="margin-top: 100px;">
            <div>
                <div class="kbtp"></div>
                <p class="no-data-zi">您还没有配置过服务类型</p>
                <span class="btn btn-success" id="add_link" >配置服务类型</span>
            </div>
        </div>
    <?php }?>
</div>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script>
    var category = [];
    var layerWindow = parent.layer.getFrameIndex(window.name); //获取窗口索引
    $(function () {
        $('.form-label').each(function() {
            var obj = {
                'title':$(this).html(),
                'child':[]
            }
            category.push(obj);
        })
        //注意：parent 是 JS 自带的全局对象，可用于操作父页面
        //获取选中的值
        var data = parent.$('#skillId').val();
        //根据获取的字符串差分为数组
        data = data.split(',');
        //循环选中
        for(var i=0;i<data.length;i++){
            $(".skill").each(function(){
                var id = $(this).val();
                if(data[i] == id){
                    $(this).attr("checked",true);
                    if($(this).prop('checked')==true){
                        var index = $(this).parents('.skill-list').index();
                        category[index].child.push($(this).next().text());
                    }
                }
            });
        }
        //定义返回值变量
        $('#closeIframe').click(function(){
            parent.layer.close(parent.index);
        });
        //无配置数据时，点击跳转
        $('#add_link').click(function(){
            parent.layer.close(layerWindow);
            window.parent.location.href="/type/index";
        });

    })
    // 选中的值
    $('.skill').click(function(){
        var index = $(this).parents('.skill-list').index();
        if($(this).prop('checked')==true){
            category[index].child.push($(this).next().text());
        }else{
            var cancel = $(this).next().text();
            var realChlid = [];
            var tempChlid = category[index].child;
            for (var i = 0; i < tempChlid.length; i++) {
                if(cancel != tempChlid[i]){
                    realChlid.push(tempChlid[i]);
                }
            };
            category[index].child=realChlid;
        }
    })
    // 全选按钮
    $('.formControls').find('.whole').change(function(){
        var index = $(this).parents('.skill-list').index();
        if (this.checked) {
            $(this).parents('.formControls').find('input').prop("checked",true);
            var tempChlid = $(this).nextAll('.chlid-label');
            var realChlid = [];
            $.each(tempChlid,function(i){
                realChlid[i]=($(this).text())
            })
            category[index].child=realChlid;

        }else{
            $(this).parents('.formControls').find('input').prop("checked",false);
            category[index].child=[];
        }
    })
    var getValue = '';
    var getText = '';
    // 确定提交按钮
    $("#transmit").click(function(){
         getValue = '';
        $(".skill").each(function(){
            if ($(this).prop('checked')){
                var bb = $(this).val();
                getValue+=bb+',';
            }
        })
        getValue = getValue.substring(0,getValue.length-1);
        getText = '';
        $.each(category,function(i,pro){
            if(pro.child.length!=0){
                getText+="<p>"+pro.title+"：";
                $.each(this.child,function(i,pro){
                    getText += pro + '、'
                })
                getText = getText.substring(0,getText.length-1);
                getText += "</p>"

            }
        })
        //给父页面传值
        parent.$('#skill').html(getText);
        parent.$('#skillId').val(getValue);
        parent.layer.close(layerWindow);
    });

</script>
</body>
</html>
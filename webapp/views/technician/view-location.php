<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" href="http://cache.amap.com/lbs/static/main1119.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.4.2&key=bbcdabda1427179d77bb95fac7b2b902&plugin=AMap.Geocoder"></script>
<script type="text/javascript" src="http://cache.amap.com/lbs/static/addToolbar.js"></script>
<!DOCTYPE html>
<html>
<head>
</head>
<style>
	.amap-icon>img{width: 100%;height: 100%;}
	.technician-tc{padding-left: 10px}
    .technician-tc>li{list-style-type: none;font-size: 14px;line-height: 2;color: #888;}
     body{
    display: table;
    width: 100%;
    height: 100%;
}
    .page-container{text-align: center;font-size: 15px;display: table-cell;vertical-align: middle;}
    .page-container>a{display: inline-block;padding: 10px 20px;height: 35px;border: 1px solid #2693ff;border-radius: 5px;}
    .page-container>div>img{width: 120px;}
    .page-container>div>p{margin:20px 0;line-height: 1.5;}
</style>
<body>
<div class="popup-boss">
	<div id="container" style="height: 600px;"></div>

</div>
<!-- 判断有无数据 -->
<?php if(empty($data['lon']) && empty($data['lat'])){ ?>
    <div class="page-container">
        <div>
            <img src="<?php echo Url::to('/images/jishiweitongguoshenghe.png');?>">
            <p style="color: #ccc">暂无实时位置</p>
        </div>
    </div>
<?php };?>
<!--  -->
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
</body>
<script type="text/javascript">
	var lon = <?php echo isset($data['lon'])?$data['lon'] : '';?>;
    var lat = <?php echo isset($data['lat'])?$data['lat'] : '';?>;

    var lnglatXY = [lon, lat]; //已知点坐标
    console.log(lnglatXY);
    var map = new AMap.Map("container", {
        resizeEnable: true,
        center:lnglatXY,
        zoom: 10
    });
    // openInfo ();
     var geocoder = new AMap.Geocoder({
        radius: 1000,
        // extensions: "all"
    });
    geocoder.getAddress(lnglatXY, function(status, result) {
        if (status === 'complete' && result.info === 'OK') {
            // geocoder_CallBack(result);
             openInfo (result);
        }
    });
    function openInfo (data) {
        console.log(data);
        technician = new AMap.Marker({
            position:lnglatXY,
            map: map,
            icon: new AMap.Icon({
                size: new AMap.Size(30, 40),  //图标大小
                image: "<?php echo Url::to('../images/jishi.png');?>",
                imageOffset: new AMap.Pixel(0, 0)
            })

        });
        var address = data.regeocode.formattedAddress; //返回地址描述
        contss ='<div style="display: inline-block;width:313px">'+
                '<ul class="technician-tc">'+
	            '    <li style="color:#444"><span style="font-size:20px"><?php echo isset($data['name'])?$data['name']:'';?></span><span style="font-size:16px;margin-left:20px"><?php echo isset($data['mobile'])?$data['mobile']:'';?></span><li>'+
	            '    <li><?php echo isset($data['address'])?$data['address']:'';?></br>最近上传时间：<?php echo isset($data['updated_at'])?$data['updated_at']:''?><li>'+
	            '</ul>'+
                '</div>';
        technician.content =contss;
        technician.on('click', markerClick);//图标点击事件
    }
    function markerClick(e) {
        technicianInfo.setContent(e.target.content);
        technicianInfo.open(map, e.target.getPosition());
    }

    var technicianInfo = new AMap.InfoWindow({offset: new AMap.Pixel(16, -45)});

</script>
</html>
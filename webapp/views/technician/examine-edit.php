<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$this->title = '修改技师';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">

<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4{
        padding-left: 0;
    }
    input[type=radio]{
        margin-top: 10px;
    }
    body{
        overflow-x: hidden;
        font-family: "微软雅黑"
    }
    .greneral-box{background: #f1f2f7;padding: 30px 50px;}
    .cont-box{padding:10px 30px  60px 30px;background: #fff;box-shadow: 0 0 30px #ccc}
    .main-content-title {display: inline-block; margin-right: 15px;font-size: 22px; background: #fff;}
    .fubiaoti {color: #2693ff;}
    .radio-list{line-height: 30px;}
    .aptitude-div{width: 100px;height: 100px;}
    .name-title-lable{width: 120px}
    .add-zhaopian {
        width: 254px;
        height: 153px;
        display: inline-block;
        vertical-align: top;
    }
    .add-zhaopian>img{
        width: 100%;height: 100%;
    }
    .filePrew{width: 244px;height: 100%;overflow: hidden;}
</style>
<body>
<div class="greneral-box">
    <div class="row cont-box">
        <div>
            <h3 class="main-content-title">修改技师</h3>
        <?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal',
                'id' => 'technician-add',
                'name' => 'form1',
                'enctype' => 'multipart/form-data',
            ]
        ]);
        ?>
        <input type="hidden" value="<?php echo isset($infos['id']) ? $infos['id'] :'';?>" id="id" name="id">
        <input type="hidden" value="<?php echo isset($infos['mobile']) ? $infos['mobile'] :'';?>" name="mobile" />
        <div class="col-md-8 information-list">
            <label class="name-title-lable" >
                <i class="red-i">*</i>技师姓名：
            </label>
            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                <input type="text" value="<?php echo isset($infos['name']) ? $infos['name'] :'';?>" name="name" class="form-control inputxt" datatype="*" sucmsg=" " nullmsg="请输入技师姓名"  max="20" />
            </div>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>
        <div class="col-md-8 information-list">
            <label class="name-title-lable" ">
            <i class="red-i">*</i>性别：
            </label>
            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                <?php if ( !empty($infos['sex']) && $infos['sex'] == 1) :?>
                    <div  class="radio-list">
                        <input type="radio" value="1" name="sex" id="male" class="pr1" checked="checked" /><label for="male">男</label>
                    </div>
                    <div  class="radio-list">
                        <input type="radio" value="2" name="sex" id="female" class="pr1" /><label for="female">女</label>
                    </div>

                <?php elseif (!empty($infos['sex']) && $infos['sex'] == 2): ?>
                    <div  class="radio-list">
                        <input type="radio" value="1" name="sex" id="male" class="pr1" /><label for="male">男</label>
                    </div>
                    <div  class="radio-list">
                        <input type="radio" value="2" name="sex" id="female" class="pr1" checked="checked" /><label for="female">女</label>
                    </div>
                <?php else:?>
                    <div  class="radio-list">
                        <input type="radio" value="1" name="sex" id="male" class="pr1" checked="checked" /><label for="male">男</label>
                    </div>
                    <div  class="radio-list">
                        <input type="radio" value="2" name="sex" id="female" class="pr1" /><label for="female">女</label>
                    </div>
                <?php endif;?>
            </div>
        </div>
        <div class="col-md-8    information-list" style="height: 100px">
            <label class="name-title-lable" >
                <i class="red-i">*</i>头像：
            </label>
            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                <div class="aptitude-div">
                    <img src="<?php echo isset($infos['icon']) ? $infos['icon'].'?x-oss-process=image/resize,m_fixed,h_100,w_100' : Url::to('/images/shangchuantouxiang.png'); ?>" class="img-aa">
                    <input type="file" name="icon" class="filePrew" value="<?php echo isset($infos['icon']) ? $infos['icon'] : ''; ?>" <?php echo isset($infos['id'])?'':'datatype="*"' ;?> sucmsg=" " maxlength="255" nullmsg="请上传头像">
                </div>
                <span class="error-span"></span>
            </div>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>
        <div class="col-md-8 information-list">
            <label class="name-title-lable" ">

            <i class="red-i">*</i>工作城市：
            </label>
            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                <div class="col-md-4 col-sm-4 col-xs-4 ">
                    <select class="col-md-4 col-sm-4 col-xs-4 form-control" id="province" value="<?php echo isset($infos['province'])?$infos['province']:'';?>" name="province" datatype="*" sucmsg=" " nullmsg="请选择所在区域">
                        <?php if (!empty($infos['province'])) : ?>
                            <option value="<?php echo $infos['province'];?>" selected="selected" ><?php echo $infos['province_name'];?></option>
                            <?php foreach ($province as $key => $val): ?>
                                <option value="<?=$val['region_id'] ?>"><?=$val['region_name'] ?></option>
                            <?php endforeach;?>
                        <?php else :?>
                            <option value="">请选择省</option>
                            <?php foreach ($province as $key => $val): ?>
                                <option value="<?=$val['region_id'] ?>"><?=$val['region_name'] ?></option>
                            <?php endforeach;?>
                        <?php endif ;?>
                    </select>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 ">
                    <select class="form-control" id="city" value="<?php echo isset($infos['city'])?$infos['city']:'';?>" name="city" datatype="*" sucmsg=" " nullmsg="请选择所在区域">
                        <?php if (!empty($infos['city'])) :?>
                            <option value="<?=$infos['city'] ?>" selected="selected" ><?php echo $infos['city_name'];?></option>
                        <?php else :?>
                            <option value="">请选择市</option>
                        <?php endif ;?>
                    </select>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 ">
                    <select class="form-control" id="district" value="<?php echo isset($infos['district'])?$infos['district']:'';?>" name="district" datatype="*" sucmsg=" " nullmsg="请选择所在区域">
                        <?php if (!empty($infos['district'])) :?>
                            <option value="<?php echo $infos['district'] ?>" selected="selected" ><?php echo $infos['district_name'] ?></option>
                        <?php else :?>
                            <option value="">请选择区</option>
                        <?php endif ;?>
                    </select>
                </div>
                <span class="error-span"></span>
                <div class="Validform_checktip Validform_wrong"></div>
            </div>
        </div>
        <div class="col-md-8 information-list">
            <label class="name-title-lable" >
                <i class="red-i">*</i>身份证号码：
            </label>
            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                <input type="text" id="identity_card" value="<?php echo isset($infos['identity_card']) ? $infos['identity_card'] :'';?>" name="identity_card" class="form-control inputxt" datatype="/^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/" sucmsg=" " maxlength="18" nullmsg="身份证号不能为空" errormsg="身份证号格式不正确" />
            </div>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>
        <div class="col-md-12 information-list" style="height: 160px">
            <label class="name-title-lable" >
                <i class="red-i">*</i>身份证正面照片：
            </label>
            <div class="details-column-content">
                <div class="add-zhaopian">
                    <img src="<?php echo isset($infos['identity_behind']) ? $infos['identity_behind'].'?x-oss-process=image/resize,m_fixed,h_150,w_250' : Url::to('/images/zhengmian.png'); ?>" class="img-aa">
                    <input type="file" name="identity_front" class="filePrew" value="<?php echo isset($infos['identity_behind']) ? $infos['identity_behind']:'' ;?>" <?php echo isset($infos['id'])?'':'datatype="*"' ;?> sucmsg=" " maxlength="255" nullmsg="请上传身份证正面照片">
                </div>
                <div  class="add-zhaopian" style="margin-left: 30px">
                    <img src="<?php echo Url::to('/images/zhegnmianshili.png');?>">
                </div>
                <span class="error-span"></span>
            </div>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>
        <div class="col-md-12 information-list" style="height: 160px;">
            <label class="name-title-lable" >
                <i class="red-i">*</i>身份证反面照片：
            </label>
            <div class="details-column-content">
                <div class="add-zhaopian">
                    <img src="<?php echo isset($infos['identity_behind']) ? $infos['identity_behind'].'?x-oss-process=image/resize,m_fixed,h_150,w_250' : Url::to('/images/fanmian.png'); ?>" class="img-aa">
                    <input type="file" name="identity_behind" class="filePrew" value="<?php echo isset($infos['identity_behind']) ? $infos['identity_behind'] : ''; ?>" <?php echo isset($infos['id'])?'':'datatype="*"' ;?> sucmsg=" " maxlength="255" nullmsg="请上传身份证反面照片">
                </div>
                <div   class="add-zhaopian" style="margin-left: 30px">
                    <img src="<?php echo Url::to('/images/fanmianshili.png');?>">
                </div>
                <span class="error-span"></span>
            </div>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>
        <div class="col-md-12 information-list" style="height: 160px;">
            <label class="name-title-lable" >
                <i class="red-i">*</i>手持身份证照片：
            </label>
            <div class="details-column-content">
                <div  class="add-zhaopian">
                    <img src="<?php echo isset($infos['identity_hold']) ? $infos['identity_hold'].'?x-oss-process=image/resize,m_fixed,h_150,w_250' : Url::to('/images/shouchishenfenzheng.png'); ?>" class="img-aa">
                    <input type="file" name="identity_hold" class="filePrew" value="<?php echo isset($infos['identity_hold']) ? $infos['identity_hold'] : ''; ?>" <?php echo isset($infos['id'])?'':'datatype="*"' ;?> sucmsg=" " maxlength="255" nullmsg="请上传手持身份证照片">
                </div>
                <div   class="add-zhaopian" style="margin-left: 30px">
                    <img src="<?php echo Url::to('/images/shouchishenfenzhengshili.png');?>">
                </div>
                <span class="error-span"></span>
            </div>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>
        <div  class="col-md-8 information-list">
            <label class="name-title-lable" ></label>
            <div class="col-md-10 col-sm-10 col-xs-10  addinput-list">
                <div class="right-btn">
                    <input type="submit" value="提交" class="btn-orange">
                    <a href="/technician/examine-list"><button type="button" style="border: 1px solid #cccccc; height:33px;width:80px;margin-left:30px" class="btn btn-orange-zi ">取消</button></a>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<script src="/js/layer/layer.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=bbcdabda1427179d77bb95fac7b2b902&plugin=AMap.Geocoder"></script>
<script>
    // 图片上传
    $('.filePrew').change(function () {
        var fil = this.files;
        var self = $(this).prev('.img-aa');
        for (var i = 0; i < fil.length; i++) {
            var reader = new FileReader();
            reader.readAsDataURL(fil[i]);
            reader.onload = function()
            {
                self.attr('src',reader.result);
            };
        }

    });
    //表单验证
    $("#technician-add").Validform({
        tiptype:3,
        callback:function(data){
            if (data.code == 200) {
                parent.alert(data.message);
                parent.layer.close(index);
            }else{
                $.Hidemsg();
                alert(data.message);
            }
        }

    });

    //关闭iframe
    $('#closeIframe').click(function(){
        parent.layer.close(index);
    });
    //根据开通城市父级id获取市级数据
    $("#province").change(function() {
        $("#city").html("<option value=''>请选择市</option>");
        $("#district").html("<option value=''>请选择区</option>");
        var pid = $("#province").val();
        if(pid != ''){
            $.get("/technician/get-city", { "province_id": pid }, function (data) {
                $("#city").html(data);
            });
        }
    });
    //根据城市获取区县
    $("#city").change(function() {
        var pid = $("#city").val();
        if(pid != ''){
            $.get("/technician/get-district", { "city_id": pid }, function (data) {
                $("#district").html(data);
            })
        }
    });
    //转换经纬度
    $('#address').blur(function(){
        //地理编码,返回地理编码结果
        var province = $("#province").find('option:selected').html();
        var city = $("#city").find('option:selected').html();
        var district = $("#district").find('option:selected').html();
        var address = $('#address').val();
        var address_all = province + city + district + address;
        var geocoder = new AMap.Geocoder({
            city: "010", //城市，默认：“全国”
            radius: 1000 //范围，默认：500
        });
        geocoder.getLocation(address_all, function(status, result) {
            if (status === 'complete' && result.info === 'OK') {
                var arr = result.geocodes[0].location;
                $('#lon').val(arr.lng);
                $('#lat').val(arr.lat);
            }
        });
    });
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
</body>
</html>
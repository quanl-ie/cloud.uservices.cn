<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = $technicianName;
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .search-box-kuang>.col-sm-6{padding-left: 0; position: relative;}
    .icon-rili{position: absolute;right: 17px;top: 1px;height: 34px;line-height: 34px;background: #fff;width: 24px;text-align: center;}
    .calendar-icon{width: 100%;height: 36px;position: absolute;top:0;left: 0;cursor: pointer;}
    .popover{max-width: 500px !important;}
    .pagination{
        text-align: center;
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title"><?php echo $technicianName?></span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/technician/technician-order" class="form-horizontal form-border" id="form">
                        <div class="form-group jn-form-box">
                            <!-- 搜索栏 -->
                            <div class="col-sm-12 no-padding-left">
                                <input type="hidden" class="form-control" name="technician_id" value="<?php echo isset($_GET['technician_id'])?$_GET['technician_id']:'';?>">
                                <div class="single-search">
                                    <label class=" search-box-lable">服务类型</label>
                                    <div class="single-search-kuang1">
                                        <select class="form-control" name="work_type">
                                            <option value="">请选择</option>
                                            <?php foreach ($workTypeArr as $key=>$val):?>
                                                <option value="<?php echo $key; ?>" <?php if(isset($_GET['work_type']) && $_GET['work_type']==$key):?>selected<?php endif; ?>><?php echo $val;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="tow-search"  >
                                    <label class="search-box-lable">服务时间</label>
                                    <div class="single-search-kuang2">
                                        <div class="half-search">
                                            <input type="text" id="datetimepicker-pay-top" class="form-control" name="plan_start_time" value="<?php echo isset($_GET['plan_start_time'])?$_GET['plan_start_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})">
                                            <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})"><i class="icon2-rili"></i></span>
                                            <span class="zhi"> 至</span>
                                        </div>
                                        <div class="half-search">
                                            <input type="text" id="datetimepicker-pay-end" class="form-control" name="plan_end_time" value="<?php echo isset($_GET['plan_end_time'])?$_GET['plan_end_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})" >
                                            <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})"><i class="icon2-rili"></i></span>
                                        </div>
                                        <p class="city3_fix"></p>
                                    </div>
                                </div>
                                <div class="search-confirm"> 
                                    <button class="btn btn-success "><i class=" "></i> 搜索</button>
                                </div>

                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead bgcolor="#2693ff">
                                <tr>
                                    <th>订单编号</th>
                                    <th>订单状态</th>
                                    <th>客户姓名</th>
                                    <th>客户电话</th>
                                    <th>服务内容</th>
                                    <th>服务地址</th>
                                    <th>服务时间</th>
                                    <!--<th>操作</th>-->
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($data['list']) && $data['list']):?>
                                    <?php foreach ($data['list'] as $key=>$val):?>
                                        <tr>
                                            <td><a href="/order/view?order_no=<?php echo $val['order_no'];?>"><?php echo $val['order_no'];?></a></td>
                                            <td><?php echo $val['status_desc']; ?></td>
                                            <td><?php echo $val['account_name'];?></td>
                                            <td><?php echo $val['account_mobile'];?></td>
                                            <td><?php echo $val['work_type_desc'];?></td>
                                            <td><a href="<?php echo $val['order_no'];?>" class="checkPosition"  ><?php echo $val['address'];?></a></td>
                                            <td><?php echo $val['plan_time']; ?></td>
                                            <!--<td  class="operation">
                                                <?php if($val['status'] == 2):?>
                                                    <a href="/order/assign?order_no=<?php echo $val['order_no']?>" >指派技师</a>
                                                <?php endif; ?>
                                                <?php if($val['status'] == 3): ?>
                                                    <a href="/order/assign?order_no=<?php echo $val['order_no'];?>" class="update">改派技师</a>
                                                <?php endif;?>
                                            </td>-->
                                        </tr>
                                    <?php endforeach;?>
                                <?php else:?>
                                    <tr>
                                    <tr><td colspan="10" class="no-record">暂无数据</td></tr>
                                    </tr>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo $pageHtml;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        window.parent.document.location.reload();
        <?php endif;?>

        //提示
        $(".tip").popover();

        $(".checkPosition").click(function (){
            var orderNo = $(this).attr('href');
            layer.open({
                type: 2,
                title: '订单位置',
                shadeClose: true,
                shade: [0],
                area: ['893px', '600px'],
                anim: 2,
                content: ['/order/position?order_no='+orderNo, 'no'], //iframe的url，no代表不显示滚动条
            });
            return false;
        });
    }
</script>
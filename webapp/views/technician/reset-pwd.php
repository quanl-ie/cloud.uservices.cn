<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = '重置密码';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::cssFile('@web/webuploader/webuploader.css') ?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css'); ?>">

<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4 {
        padding-left: 0;
    }

    input[type=radio] {
        margin-top: 10px;
    }

    body {
        overflow-x: hidden;
        font-family: "微软雅黑", sans-serif
    }
</style>
<body>
<div class="row popup-boss">
    <form action="/technician/reset-pwd" method="post" id="pwd-set">
        <input type="hidden" value="<?php echo $id ?>" id="id" name="id">
        <div class="col-md-8 information-list">
            <label class="name-title-lable">新密码：</label>
            <div class="col-md-8 col-sm-8 col-xs-8  addinput-list">
                <input type="password" value="" name="userpassword" class="form-control inputxt"
                       placeholder="请输入6~18位的密码" datatype="*6-18" sucmsg=" " maxlength="18" errormsg="密码范围在6~18位之间！"/>
            </div>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>
        <div class="col-md-8 information-list">
            <label class="name-title-lable">确认密码：</label>
            <div class="col-md-8 col-sm-8 col-xs-8 addinput-list">
                <input type="password" value="" name="userpassword2" class="form-control inputxt"
                       placeholder="请输入6~18位的密码" datatype="*" sucmsg=" " maxlength="18" recheck="userpassword"
                       errormsg="您两次输入的账号密码不一致！"/>
            </div>
            <div class="Validform_checktip Validform_wrong"></div>
        </div>

        <div class="col-md-8 information-list">
            <label class="name-title-lable"></label>
            <div class="col-md-10 col-sm-10 col-xs-10  addinput-list">
                <div class="right-btn">
                    <input type="button" id="closeIframe" value="取消">
                    <input type="submit" value="提交" class="btn-orange">
                </div>

            </div>

        </div>

    </form>
</div>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    var index = parent.layer.getFrameIndex( window.name );

    //表单验证
    $( "#pwd-set" ).Validform( {
        ajaxPost : true,
        tiptype  : 3,
        callback : function ( data ) {
            if ( data.code == 200 ) {
                parent.alert( data.message );
                parent.layer.close( index );
            } else {
                $.Hidemsg();
                alert( data.message );
            }
        }

    } );

    //关闭iframe
    $( '#closeIframe' ).click( function () {
        parent.layer.close( index );
    } );
</script>
</body>
</html>
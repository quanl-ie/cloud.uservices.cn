<?php

use yii\helpers\Url;

$this->title = '收费确认';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/base.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/vue.component.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/goodslist.css'); ?>?v=<?php echo time(); ?>">
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">收费确认列表</span>
</div>
    <section id="main-content">
        <div class="panel-body" v-cloak>
            <?php if (!in_array('/fee/no-confirm-list', $selfRoles) && !in_array('/fee/confirm-list', $selfRoles) ): ?>
                <div style="text-align: center">无权限</div>
            <?php else: ?>
            <!-- tab切换 -->
            <div class="ct-tab-box">
                <ul>
                    <?php if (in_array('/fee/no-confirm-list', $selfRoles)): ?>
                        <li v-init-prop="[state,'state1',true]" :class="status==0?'active':''" @click='tabGetdata'
                            value='0'>未确认
                        </li>
                    <?php endif; ?>
                    <?php if (in_array('/fee/confirm-list', $selfRoles)): ?>
                        <li v-init-prop="[state,'state2',true]" :class="status==1?'active':''" @click='tabGetdata'
                            value='1'>已确认
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
            <!-- 搜索框 -->
            <div class="flex flex-wrap">
                <u-form-item label="子工单号" mini width='240px' label-width="80px" padding='5px'>
                    <u-input type="text"
                             width="160px"
                             v-model="search.work_no"
                             placeholder="请输入子工单号"></u-input>
                </u-form-item>

                <u-form-item label="技师姓名" mini width='240px' label-width="80px" padding='5px'>
                    <u-input type="text"
                             width="160px"
                             v-model="search.tec_name"
                             placeholder="请输入技师姓名"></u-input>
                </u-form-item>

                <u-form-item label="技师电话" mini width='240px' label-width="80px" padding='5px'>
                    <u-input type="text"
                             width="160px"
                             v-model="search.tec_mobile"
                             placeholder="请输入技师电话"></u-input>
                </u-form-item>

                <u-form-item label="服务类型" label-width="80px" mini width='240px'>
                    <u-select width="160px" height="32px" v-model="search.service_type">
                        <option value="">全部</option>
                        <option v-for="item in source.typeList" :value="item.id">{{item.title}}</option>
                    </u-select>
                </u-form-item>

                <u-form-item label="收费时间" label-width="80px" mini width='430px'>
                    <div class="flex">
                        <vue-date width='160px' margin="0 5px 0 0" @riqi="changeDate" plural='start_fee_time' :date='search.start_fee_time'></vue-date>
                        至
                        <vue-date width='160px' margin="0 0 0 5px"   @riqi="changeDate" plural='end_fee_time' :date='search.end_fee_time'></vue-date>
                    </div>
                </u-form-item>
                <u-form-item label-width="0" mini width='82px'>
                    <button class="btn btn-search" @click="searchEvent">确认</button>
                </u-form-item>
            </div>
            <!-- 列表内容 -->
            <table>
                <thead>
                <tr>
                    <th width="150">子工单号</th>
                    <th width="150">服务技师</th>
                    <th width="100">服务类型</th>
                    <th width="100">收费总额</th>
                    <th width="100">实际收费</th>
                    <th width="150">收费时间</th>
                    <th>备注</th>
                    <th width="160px" class="center">操作</th>
                </tr>
                </thead>
                <tbody>
                <tr v-if="dataList.length>0" v-for='(item,index) in dataList' :key='index'>
                    <td><a :href="'/order/view?order_no='+item.order_no">{{item.work_no}}</a></td>
                    <td>{{item.technician_name}}</td>
                    <td>{{item.type_name}}</td>
                    <td>{{item.cost_amount}}</td>
                    <td>{{item.cost_real_amount}}</td>
                    <td>{{item.create_time}}</td>
                    <td>{{item.fee_remark}}</td>
                    <td class="center">
                        <?php if (in_array('/fee/confirm', $selfRoles)): ?>
                            <a href="javascript:void(0);" @click="confirmFee(item.work_no)" v-if="status == 0" class="mgr10">确认收费</a>
                        <?php endif; ?>
                        <?php if (in_array('/fee/add-remark', $selfRoles)): ?>
                            <a href="javascript:" @click="showRemarkEvent(item.work_no,'')" v-if='!item.fee_remark '>添加备注</a>
                        <?php endif; ?>
                        <?php if (in_array('/fee/update-remark', $selfRoles)): ?>
                            <a href="javascript:" @click="showRemarkEvent(item.work_no,item.fee_remark)" v-if='item.fee_remark'>修改备注</a>
                        <?php endif; ?>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="mg-top30">
                <vue-pagination :cur.sync="cur" :all.sync="all" v-on:btn-click="listen"></vue-pagination>
            </div>
            <vue-layer width="350px" height="240px" v-on:show='hideCollectGoodsEvent' ly-title='添加备注' v-if='showCollectGoods' show-name='showCollectGoods'>
                <u-textarea height="100px" v-model="remark.remark"></u-textarea>
                <div class="u-btn-box">
                    <button type="button" class="btn bg-f7 mgr20" @click='hideCollectGoodsEvent'>取消</button></a>
                    <button type="button" class="btn btn-orange" @click="editRemarkEvent">确定</button></a>
                </div>
            </vue-layer>
        <?php endif; ?>
        </div>
    </section>

<script type="text/javascript" src="/js/vue/lib/vue.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.mixin.emitter.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.component.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v;?>"></script>
<script type="text/javascript" src="/js/vue/controller/fee/index.js?v=<?php echo $v;?>"></script>
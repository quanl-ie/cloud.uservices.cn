<?php
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
    use webapp\models\Menu;
    
    $this->title = '确认收费设置';
    $this->params['breadcrumbs'][] = ['label' => '系统设置', 'url' => ['sys/index']];
    $this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<style>
    .modal-content {border: none;border-radius: 0px;box-shadow: inherit !important;}
    .ck_role .checkbox{float:left;margin-right:10px;}
    .radio { float: left;margin-top: -5px;}
    .radio label, .checkbox label{padding-left: 0;padding-right: 30px;}
</style>
<section id="main-content">
    <div class="panel-body"><?php
            $form = ActiveForm::begin([
                'options' => [
                    'class' => 'form-horizontal',
                    'id' => 'form2',
                    'name' => 'form1',
                    'enctype' => 'multipart/form-data',
                
                ],
                'fieldConfig' => [
                    'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-xs-6 addinput-list\">{input}{error}</div>",
                    'labelOptions' => ['class' => 'name-title-lable'],
                ]
            ]);
        ?>

        <input type="hidden" name="id" value="<?=isset($data['id']) ? $data['id'] :""; ?>" >
        <div class="form-group">
            <label class="name-title-lable">确认收费开关：</label>
            <div class="col-xs-6 addinput-list" style="padding-left: 0">
                <?php if (!empty($list)) : ?>
                <?php foreach ($list as $key => $val) : ?>
                <div class="radio">
                    <input type="radio" name="status"  <?php $data['status'] = isset($data['status']) ? $data['status'] : 2; if ($data['status'] == $key) : ?>checked="checked" <?php endif; ?> value="<?=$key ?>" id="<?=$key ?>"><label for="<?=$key ?>"><?=$val ?></label>
                </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        
        <hr>
        <div class="form-group">
            <div class="col-xs-6 addinput-list">
                <input type="submit" class="btn btn-orange mgr20 " style=" color:#FFF;" value="保存" />
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <span style="color: red">注：启用设置后，可对技师服务的每个子工单进行收费确认及添加相应备注记录。</span>
    </div>
</section>
<script>
    window.onload = function(){
        
        <?php if(Yii::$app->session->hasFlash('fee_setting_config_message')):?>
        setTimeout(function () {
            alert('<?php echo Yii::$app->session->getFlash('fee_setting_config_message'); ?>');
        },500);
        <?php endif;?>
    }
</script>

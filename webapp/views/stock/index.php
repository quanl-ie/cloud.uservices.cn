<?php
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\helpers\Url;
    $this->title = '库存管理';
    $this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time()?>">
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">库存管理</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/stock/index" class="form-horizontal form-border" id="form">
                        <div class="form-group jn-form-box">
                            <div class="col-sm-12 no-padding-left">
                                <div class="single-search">
                                    <label class="search-box-lable">产品名称</label>
                                    <div class="single-search-kuang1">
                                        <input type="text" class="form-control" name="prod_name" maxlength="20" value="<?php echo isset($_GET['prod_name'])?$_GET['prod_name']:'';?>">
                                    </div>
                                </div>
                                <div class="single-search">
                                    <label class="search-box-lable">品牌</label>
                                    <div class="single-search-kuang1">
                                        <select class="form-control" name="brand_id">
                                            <option value="">请选择</option>
                                            <?php if (!empty($brand)) : ?>
                                            <?php foreach ($brand as $key => $val) : ?>
                                                <option value="<?=$key;?>" <?php if(isset($_GET['brand_id']) && $_GET['brand_id']==$key):?>selected<?php endif; ?>><?=$val;?></option>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="single-search" style="overflow:visible;">
                                    <label class=" search-box-lable">类目</label>
                                    <div class="single-search-kuang1 region-limit" id="de">
                                        <input type="hidden" name="class_id" id="class_id">
                                        <input type="text" class="form-control" id="class_name" value="<?=isset($className) ? $className : '' ?>" placeholder="请选择类目" readonly onclick="clickCategory()">
                                        <div class="drop-dw-layerbox" style="display: none;">
                                            <div class="ul-box-h180">
                                                <ul id="depart1" class="depart">
                                                    <?php if (!empty($class)) : ?>
                                                        <?php foreach ($class as $key => $val) : ?>
                                                            <li>
                                                             <span>
                                                                <?php if (isset($val['exist']) && $val['exist'] == 1) : ?>
                                                                    <i class="icon2-shixinyou" onclick="getNextClass(this)"></i>
                                                                <?php endif;?>
                                                                 <input type="checkbox" id="a<?=$val['id'] ?>" value="<?=$val['id'] ?>" name="class_id[]" >
                                                                 <label for="a<?=$val['id'] ?>"><?=$val['class_name'] ?><i class="gou-i"></i></label>
                                                            </span>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                            <button class="btn btn-orange" id="belongedBtn">确定</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-search">
                                    <label class="search-box-lable">库房名称</label>
                                    <div class="single-search-kuang1">
                                        <input type="text" class="form-control" name="depot_name" maxlength="20" value="<?php echo isset($_GET['depot_name'])?$_GET['depot_name']:'';?>">
                                    </div>
                                </div>

                                <div class="search-confirm">   
                                    <button class="btn btn-success">
                                        <i class=" "></i> 搜索
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead bgcolor="#455971">
                                <tr>
                                    <th>产品编号</th>
                                    <th>产品名称</th>
                                    <th>产品品牌</th>
                                    <th>产品分类</th>
                                    <th>计量单位</th>
                                    <th>申请采购数量</th>
                                    <th>采购在途数量</th>
                                    <?php if($ship_sys == 1){?>
                                        <th>销售在途数量</th>
                                    <?php }?>
                                    <th>库存总量</th>
                                    <th>仓储数量</th>
                                    <th>技师领用（未使用）</th>
                                    <th>非良品</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($data)):?>
                                    <?php foreach ($data as $key=>$val):?>
                                        <tr>
                                            <td><?=isset($val['prod_no']) ? $val['prod_no'] : ''; ?></td>
                                            <td><?=isset($val['prod_name']) ? $val['prod_name'] : ''; ?></td>
                                            <td><?=isset($val['brand_name']) ? $val['brand_name'] : '空'; ?></td>
                                            <td><?=isset($val['class_name']) ? $val['class_name'] : '空'; ?></td>
                                            <td><?=isset($val['unit_name']) ? $val['unit_name'] : '空'; ?></td>
                                            <td><?=isset($val['purchase_num']) ? $val['purchase_num'] : ''; ?></td>
                                            <td><?=isset($val['purchase_in_sum']) ? $val['purchase_in_sum'] : ''; ?></td>
                                            <?php if($ship_sys == 1){?>
                                                <td><?=isset($val['on_sale_num']) ? $val['on_sale_num'] : ''; ?></td>
                                            <?php }?>
                                            <td><?=isset($val['sum']) ? $val['sum'] : ''; ?></td>
                                            <td><?=isset($val['num']) ? $val['num'] : ''; ?></td>
                                            <td><?=isset($val['storage_num']) ? $val['storage_num'] : ''; ?></td>
                                            <td><?=isset($val['bad_num']) ? $val['bad_num'] : ''; ?></td>
                                            <td class="operation">
                                                <?php if(in_array('/stock/view',$selfRoles)){?>
                                                    <a href="/stock/view?id=<?php echo $val['prod_id'];?>">查看</a>
                                                <?php };?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else:?>
                                    <tr><td colspan="12" class="no-record">暂无库存产品</td></tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <div class="col-xs-12 text-center pagination">
                        <?php if (!empty($pageHtml)) {echo $pageHtml;};?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
    // 选中内容
    $('#belongedBtn').bind('click', function() {
        event.preventDefault();
        var id_array= [];
        var data_array=[];
        $('input[name="class_id[]"]:checked').each(function(){
            data_array.push($(this).next('label').text());
            id_array.push($(this).val());//向数组中添加元素
        });
        var idstr=id_array.join(',');//将数组元素连接起来以构建一个字符串
        var datatr = data_array.join('；');
        $('#class_name').val(datatr);
        $("#class_id").val(idstr);
        $('.drop-dw-layerbox').hide();
    });

    //获取下级分类
    function getNextClass(el) {
        var id = $(el).next('input').val();
        var _this = $(el).parent('span');
        $.getJSON('/common/ajax-get-class',{'pid':id},function (json) {
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>"
                var data = json.data;
                delete data.flag;

                for (item in data)
                {
                    ulData +="<li><span>";
                    data[item].exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="getNextClass(this)"></i>':'';
                    ulData+='<input type="checkbox" value="'+data[item].id+'" name="class_id[]" id="class'+data[item].id+'"><label for="class'+data[item].id+'">'+data[item].class_name+'<i class="gou-i"></i></label></span></li>';
                }

                ulData +="</ul>";

                _this.parent('li').append(ulData);
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');

            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })
    }

    function  clickCategory(el) {
        //event.stopPropagation();
        $('.drop-dw-layerbox').is(':hidden')? $('.drop-dw-layerbox').show(): $('.drop-dw-layerbox').hide();
    }
    $(document).click(function(event) {
       var region = $('.region-limit');
       if (!region.is(event.target)&&region.has(event.target).length ===0) {
                $('.drop-dw-layerbox').hide();
       }
    });
</script>
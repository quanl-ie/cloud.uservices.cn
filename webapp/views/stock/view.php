<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use webapp\models\BrandQualification;
    use yii\helpers\ArrayHelper;
    /* @var $this yii\web\View */
    /* @var $model webapp\models\BrandQualification */
    $this->title = '库存详情';
    $this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .information-list {margin-top: 0;}
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h5 class="block-h5 block-h5one">基本信息</h5>
                    <div class="information-list">
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">产品编号：</label>
                            <div class="right-title3"><?=isset($data['prod_no']) ? $data['prod_no'] : "" ?></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-4">
                            <label  class="left-title80">产品名称：</label>
                            <div class="right-title3"><span><?=isset($data['prod_name']) ? $data['prod_name'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">产品类型：</label>
                            <div class="right-title3"><span><?=isset($data['type_name']) ? $data['type_name'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">产品品牌：</label>
                            <div class="right-title3"><span><?=isset($data['brand_name']) ? $data['brand_name'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-4">
                            <label  class="left-title80">产品分类：</label>
                            <div class="right-title3"><span><?=isset($data['class_name']) ? $data['class_name'] : "" ?></span></div>
                        </div>
                        <div class="col-xs-12   col-sm-12   col-md-4    col-lg-3">
                            <label  class="left-title80">产品型号：</label>
                            <div class="right-title3"><span><?=isset($data['model']) ? $data['model'] : "" ?></span></div>
                        </div>
                    </div>
                    <h5 class="block-h5" style="padding-bottom: 10px">库存信息</h5>
                    <div class="information-list" style="height: auto;">
                        <table>
                            <thead>
                            <tr>
                                <th style="width: 200px;">所属库房</th>
                                <th>库存数量（个）</th>
                                <th>非良品（个）</th>
                                <th>预出库数量（个）</th>
                                <th>可用数量（个）</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($depot)) : ?>
                            <?php foreach ($depot as $val) : ?>
                            <tr>
                                <td><?=isset($val['depot_name'])?$val['depot_name']:"" ?></td>
                                <td><?=isset($val['total_num'])?$val['total_num']:"" ?></td>
                                <td><?=isset($val['bad_num'])?$val['bad_num']:"" ?></td>
                                <td><?=isset($val['stock_out_num'])?$val['stock_out_num']:"" ?></td>
                                <td><?=isset($val['num'])?$val['num']:"" ?></td>
                                <td class="operation">
                                    <a href="/stock/detail?depotId=<?=$val['depot_id'];?>&prodId=<?=$val['prod_id']; ?>">明细</a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    }
</script>
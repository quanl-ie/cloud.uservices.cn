<?php
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\helpers\Url;
    $this->title = '库存明细';
    $this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
    .search-box-kuang>.col-sm-6{
        padding-left: 0;
        position: relative;
    }
    .icons-calendar{
        position: absolute;
        right: 24px;top: 9px;
    }
    .pagination{text-align: center;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">库存明细</span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead bgcolor="#455971">
                                <tr>
                                    <th>库存数量（个）</th>
                                    <th>非良品数量（个）</th>
                                    <th>可用数量（个）</th>
                                    <th>批号</th>
                                    <th>入库日期</th>
<!--                                    <th>生产日期</th>-->
<!--                                    <th>有效日期</th>-->
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($data)) : ?>
                                <?php foreach ($data as $val) : ?>
                                <tr>
                                    <td><?=$val['total_num'] ?></td>
                                    <td><?=$val['bad_num'] ?></td>
                                    <td><?=$val['num'] ?></td>
                                    <td><?=$val['prod_batch'] ?></td>
                                    <td><?=$val['create_time'] ?></td>
<!--                                    <td>--><?//=$val['prod_date'] ?><!--</td>-->
<!--                                    <td>--><?//=$val['effective_date'] ?><!--</td>-->
                                </tr>
                                <?php endforeach; ?>
                                <?php else:?>
                                    <tr><td colspan="10" class="no-record">暂无数据</td></tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    <div class="col-xs-12 text-center  pagination">
                        <?php if (!empty($pageHtml)) {echo $pageHtml;};?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    };
</script>
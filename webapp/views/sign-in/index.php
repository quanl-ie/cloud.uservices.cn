<?php

use yii\helpers\Url;
use webapp\models\BrandQualification;

$this->title = '技师打卡记录';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/us-icon.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css" href="/css/vue-photo-preview.css"/>


<style type="text/css">

    .us-table-cell-break {
        font-family: "Microsoft YaHei", sans-serif;
        font-size: 12px;
        list-style: none;
        font-weight: 400;
        padding: 0;
        letter-spacing: 0;
        white-space: nowrap;
        color: #8c8e99;
        display: inline-block;
        margin: 0 40px 0 0;
    }

</style>
<div id="app" v-cloak>
    <section id="main-content">
        <div class="panel-body">
            <us-adv-search title="技师打卡记录" placeholder="技师姓名"
                           @on-input-search="inputData"
                           @on-search="searchData"
                           v-model="search.technician_name">
                <us-form label-whl="80px" item-whl="210px" wrap-whl="130px">
                    <us-form-row>
                        <us-form-item label="时间：" whl="400px">
                            <us-ipt-datetime section whl="300px" v-model="search.timeSection"></us-ipt-datetime>
                        </us-form-item>
                        <us-form-item label="所属部门：">
                            <us-ipt-select v-model="search.department_id" @change="changeSearchGrouping">
                                <us-select-option value="" label="不限"></us-select-option>
                                <us-select-option v-for="item in source.departmentList" :value="item.id"
                                                  :label="item.name"></us-select-option>
                            </us-ipt-select>
                        </us-form-item>
                        <us-form-item label="小组：">
                            <us-ipt-select v-model="search.group_id" @change="changeSearchTechnician">
                                <us-select-option value="" label="不限"></us-select-option>
                                <us-select-option v-for="item in source.groupList" :value="item.id"
                                                  :label="item.name"></us-select-option>
                            </us-ipt-select>

                        </us-form-item>
                        <us-form-item label="技师:">
                            <us-ipt-select v-model="search.technician_id">
                                <us-select-option value="" label="不限"></us-select-option>
                                <us-select-option v-for="item in source.technicianList" :value="item.id"
                                                  :label="item.name"></us-select-option>
                            </us-ipt-select>
                        </us-form-item>
                        <us-form-item label="类型:">
                            <us-ipt-select v-model="search.category">
                                <us-select-option value="" selected label="不限"></us-select-option>
                                <us-select-option v-for="item in source.categoryList" :value="item.c_id"
                                                  :label="item.title"></us-select-option>
                            </us-ipt-select>
                        </us-form-item>

                        <!--                        <us-form-item label-whl="0">-->
                        <!--                            <us-button type="primary" @click="exportBtn">导出</us-button>-->
                        <!--                        </us-form-item>-->
                    </us-form-row>
                </us-form>
            </us-adv-search>
            <us-table :data="dataList">
                <us-table-column label="技师" field="technician_name"></us-table-column>
                <us-table-column label="所属部门" field="department_name"></us-table-column>
                <us-table-column label="小组" field="group_name"></us-table-column>
                <us-table-column label="日期" field="date"></us-table-column>
                <us-table-column label="时间" field="time"></us-table-column>
                <us-table-column label="类型" field="category"></us-table-column>
                <us-table-column label="关联工单">
                    <span class="us-table-cell us-table-cell-ellipsis" slot-scope="scope" :title="scope.data.order_title">
                        <?php if (in_array('/order/view', $selfRoles)): ?>
                            <a v-if="scope.data.order_no !=''" :href="'/order/view?order_no='+scope.data.order_no">{{scope.data.order_title}}</a>
                            <span v-if="scope.data.order_no == ''">无</span>
                        <?php endif; ?>
                    </span>
                </us-table-column>
                <us-table-column label="照片">
                    <template slot-scope="scope">
                        <img v-if="scope.data.pic_url.length > 0" v-for="(item,index ) in scope.data.pic_url"
                             :src="item" :preview="scope.data.id" width="20" height="20"
                             style="margin-right:5px;cursor: pointer;"/>
                        <span v-if="scope.data.pic_url.length == 0">无</span>
                    </template>
                </us-table-column>
                <us-table-column break-col>
                    <div slot-scope="scope">
                        <span class="us-table-cell-break" style="white-space: normal">打卡地址：{{scope.data.address}}</span>
                        <span class="us-table-cell-break" style="white-space: normal">备注：{{!scope.data.remark?'无':scope.data.remark}}</span>
                    </div>
                </us-table-column>
            </us-table>
            <div class="text-center">
                <us-pagination
                        @change="changePage"
                        :page-no="page.pageNo"
                        :total-page="page.totalPage"
                        :total="page.total"></us-pagination>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue-photo-preview.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/sign-in/index.js?v=<?php echo $v; ?>"></script>

<?php
use yii\widgets\LinkPager;
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '合作商家';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<style type="text/css">
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">合作商家
    <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="请联系商家向您发起申请，通过后即可建立合作关系"></span>
    </span>

</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/cooporation/index" class="form-horizontal form-border" id="form">
                        <div class="form-group jn-form-box">
                            <div class="col-sm-12 no-padding-left">
                                <div class="single-search">
                                     <label class=" search-box-lable">商家名称</label>
                                    <div class="single-search-kuang1">
                                        <input type="text" class="form-control" name="name" value="<?= isset($_GET['name']) ? $_GET['name'] : '' ?>">
                                    </div>
                                </div>
                                <div class="single-search">
                                   <label class=" search-box-lable">品牌</label>
                                    <div class="single-search-kuang1">
                                        <input type="text" class="form-control" name="brand" value="<?= isset($_GET['brand']) ? $_GET['brand'] : ''?>">
                                    </div>
                                </div>
                                 <div class="tow-search">
                                    <label class=" search-box-lable">商品类目</label>
                                    <div class="single-search-kuang1">
                                        <div class="half-search">
                                            <select class="form-control first_class_id" name="first_class_id">
                                                <option value="">全部一级类目</option>
                                                <?php foreach ($firstClass as $key=>$val):?>
                                                    <option value="<?php echo $key; ?>" <?php if(isset($_GET['first_class_id']) && $_GET['first_class_id']==$key):?>selected<?php endif; ?>><?php echo $val;?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                        <div class="half-search">
                                            <select class="form-control second_class_id" name="second_class_id">
                                                <option value="">全部二级类目</option>
                                                <?php if($secondClass): ?>
                                                    <?php foreach ($secondClass as $key=>$val):?>
                                                        <option value="<?php echo $key; ?>" <?php if(isset($_GET['second_class_id']) && $_GET['second_class_id']==$key):?>selected<?php endif; ?>><?php echo $val;?></option>
                                                    <?php endforeach;?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="single-search">
                                    <label class=" search-box-lable">状态</label>
                                    <div class="single-search-kuang1">
                                        <select class="form-control" name="status">
                                            <option value="">请选择</option>
                                            <?php foreach ($statusList as $key=>$val):?>
                                            <option value="<?php echo $key; ?>" <?php if(isset($_GET['status']) && $_GET['status']==$key):?>selected<?php endif; ?>><?php echo $val;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="search-confirm">  
                                    <button class="btn btn-success "><i class="fa fa-search"></i> 搜索</button>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead bgcolor="#455971">
                                <tr>
                                    <th>商家名称</th>
                                    <th>经营品牌</th>
                                    <th>经营类目</th>
                                    <th>联系人</th>
                                    <th>联系方式</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($list):?>
                                    <?php foreach ($list as $key=>$val):  ?>
                                    <tr>
                                        <td><?=$val['company'] ? $val['company'] : '空'; ?></td>
                                        <td><a href="javascript:void(0)"onclick="getBrand(<?=$val['src_id']?>)">点击查看</a></td>
                                        <td><a href="javascript:void(0)"onclick="getClass(<?=$val['src_id']?>)">点击查看</a></td>
                                        <td><?=$val['contact'] ? $val['contact'] : ' 暂无联系人'; ?></td>
                                        <td><?=$val['mobile'] ? $val['mobile'] : '  暂无联系电话'; ?></td>
                                        <td><?=$val['statusDesc'] ;?></td>
                                        <td class="operation">
                                            <a href="/cooporation/get-cost-rule?id=<?=$val['src_id']?>" >查看结算规则</a>
                                            <a href="/cooporation/get-cost-level?id=<?=$val['src_id']?>" >查看收费标准</a>
                                            <?php if($val['status']==1){?>
                                            <a class="clickButton" href="javascript:void(0)" id-data="<?=$val['id']?>" status-data="2">同意</a>
                                            <a class="clickButton" href="javascript:void(0)" id-data="<?=$val['id']?>" status-data="3">拒绝</a>
                                            <?php }?>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                <?php else:?>
                                    <tr>
                                    <tr><td colspan="7" class="no-record">暂无数据</td></tr>
                                    </tr>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo $pageHtml;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(".tip").popover();
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        window.parent.document.location.reload();
        <?php endif;?>
    };
</script>
<script type="text/javascript">
//类目二级联动
    $('.first_class_id').change(function () {
        var pid = $(this).val();
        var html = '<option value="">全部二级类目</option>';
        if(pid!='' && pid > 0 ){
            $.getJSON('/common/ajax-get-class',{'pid':pid},function (json) {
                if(json.success == true){
                    for(item in json.data.data){
                        html+='<option value="'+item+'">'+json.data.data[item]+'</option>';
                    }
                    $('.second_class_id').html(html);
                }
            })
        }
        else {
            $('.second_class_id').html(html);
        }
    });
    //查看经营品牌
    function getBrand(id){
       layer.open({
            type: 2,
            title: ['查看经营品牌', 'font-size:18px;'],
            area: ['600px', '300px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: '/cooporation/get-brand?id='+id,
        });
       return false;
    }
    //查看经营类目
    function getClass(id){
       layer.open({
            type: 2,
            title: ['查看经营类目', 'font-size:18px;'],
            area: ['600px', '300px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: '/cooporation/get-class?id='+id,
        });
       return false;
    }

    $(".clickButton").click(function(){
        var id = $(this).attr("id-data");
        var status = $(this).attr("status-data");
        var html;
        
        if (status == 2) {
            html = '确定要通过此商家的合作申请吗?';
        }else if (status == 3){
            html = '确定要拒绝此商家的合作申请吗？';
        }
        if(status!=4){
            layer.confirm(html, {icon: 3, title:'服务商管理'}, function(index){
                 $.ajax({
                     url:'/cooporation/change-status',
                     type:'POST',
                     data:{id:id,status:status},
                     success:function(msg){
                         var dataObj=eval("("+msg+")");
                         if(dataObj.success == true){
                             layer.alert('操作成功！', function(index){
                                 window.location.reload();
                                 layer.close(index);
                             });

                         }

                     }
                 })
            });
        }
    });
</script>
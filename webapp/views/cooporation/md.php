<?php
use yii\widgets\LinkPager;
$this->title = '合作门店';
$this->params['breadcrumbs'][] = $this->title;

?>
<style type="text/css">
    .btn-success{
        background: #2693ff;
        border-color: #2693ff;
    }
    .btn-success:hover{
        background: #2693ff;
        border-color: #2693ff;
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">合作门店</span>

</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/account/index" class="form-horizontal form-border" id="form">
                  <!--      <div class="form-group jn-form-box">
                            <div class="col-sm-10 no-padding-left">
                                <div class="col-sm-3">
                                    <label class="control-label">服务商名称</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="service_provider_name" value="<?/*=isset($params['service_provider_name']) ? $params['service_provider_name'] : '' */?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label">联系人</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="mobile" value="<?/*= isset($params['mobile']) ? $params['mobile'] : '' */?>">
                                    </div>
                                </div>
                                <div class="col-sm-2 text-right" style="text-align: left;">
                                    <button href="" class="btn btn-success "><i class="fa fa-search"></i> 搜索</button>
                                </div>
                            </div>
                        </div>-->
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead bgcolor="#455971">
                                <tr>
                                    <th>门店名称</th>
                                    <th>联系人</th>
                                    <th>电话</th>
                                    <th>服务区域</th>
                                    <th>经营范围</th>
                                    <th>状态</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($data):?>

                                    <?php foreach ($data as $key=>$val):  ?>
                                        <tr>
                                            <td><?=$val['company'] ? $val['company'] : '空'; ?></td>
                                            <td><?=$val['contact'] ? $val['contact'] : ' 暂无联系人'; ?></td>
                                            <td><?=$val['mobile'] ? $val['mobile'] : '  暂无联系电话'; ?></td>
                                            <td><a href="" class="serviceArea" id-data="<?=$val['co_id']?>">服务区域</a></td>
                                            <td><a href="" class="serviceField" id-data="<?=$val['co_id'].'&srcType='.$val['src_type']?>">经营范围</a></td>
                                            <td><?=$statusArr[$val['status']];?></td>
                                           <td class="operation">
                                                   <?php if($val['status']==1){?>
                                                    <a class="clickButton" href="javascript:void(0)" id-data="<?=$val['id']?>" status-data="2">同意</a>
                                                    <a class="clickButton" href="javascript:void(0)" id-data="<?=$val['id']?>" status-data="3">拒绝</a>
                                                    <?php }?>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php else:?>
                                    <tr>
                                    <tr><td colspan="7" class="no-record">暂无数据</td></tr>
                                    </tr>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo $pageHtml;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        window.parent.document.location.reload();
        <?php endif;?>
    };
</script>
<script type="text/javascript">




    $(".clickButton").click(function(){
        var id = $(this).attr("id-data");
        var status = $(this).attr("status-data");
        var html = $(this).html();
        if(status!=4){
            layer.confirm('确认”<b>'+html+'</b>“操作吗?', {icon: 3, title:'服务商管理'}, function(index){
                 $.ajax({
                     url:'/cooporation/change-status',
                     type:'POST',
                     data:{id:id,status:status},
                     success:function(msg){
                         var dataObj=eval("("+msg+")");
                         if(dataObj.success == true){
                             layer.alert('操作成功！', function(index){
                                 window.location.reload();
                                 layer.close(index);
                             });

                         }

                     }
                 })
            });
        }
    })




    //查看服务区域
    $(".serviceArea").click(function(){
        var id = $(this).attr("id-data");

       layer.open({
            type: 2,
            title: ['服务区域', 'font-size:18px;'],
            area: ['600px', '300px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/cooporation/service-area?serviceId='+id,
            //end:function(){
                //location.reload();
           // }
        });
       return false;
    })



    //查看经营范围
    $(".serviceField").click(function(){
        var id = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['经营范围', 'font-size:18px;'],
            area: ['450px', '300px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/cooporation/service-field?serviceId='+id,
            //end:function(){
            //location.reload();
            // }
        });
        return false;
    })

</script>
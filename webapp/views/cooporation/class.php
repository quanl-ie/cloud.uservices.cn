<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">

<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4{
        padding-left: 0;
    }
    input[type=radio]{
        margin-top: 10px;
    }
    body{background: #fff;overflow-x: hidden;font-family: "微软雅黑"；}
    table{
        border: 1px solid #ccc;
        width: 450px;
        margin:30px auto;
        font-size: 14px;text-align: center;
    }    
    div{
        padding-left: 20px;
        margin-top: 10px;
    }
    .table-striped>tbody>tr:nth-child(odd)>td{background: #fff;}
</style>
<body>
    <div>
        <!-- <h3>经营类目</h3> -->
         <table class="table table-bordered table-striped table-hover" style="width: 80%;margin-left: 10%;margin-bottom: 40px;">
            <thead>
            <tr>
                <th style="text-align: center;">一级类目</th>
                <th style="text-align: center;">二级类目</th>
            </tr>
            </thead>
            <tbody style="background: #fff">
            <?php if($class):?>
                <?php foreach ($class as $key=>$val):  ?>
                    <tr>
                        <td><?=$val['title']?></td>
                        <td><?=$val['secondClass'] ? $val['secondClass'] : '空'; ?></td>
                    </tr>
                <?php endforeach;?>
            <?php else:?>
                <tr>
                    <td colspan="2" class="no-record" style="align-content: center">暂无数据</td>
                </tr>
            <?php endif;?>
            </tbody>
        </table>
    </div>
</body>
</html>
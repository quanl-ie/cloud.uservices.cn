<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '查看经营品牌';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">

<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4{padding-left: 0;}
    input[type=radio]{
        margin-top: 10px;
    }
    body{background: #fff;
        overflow-x: hidden;
        font-family: "微软雅黑"；
    }
    div{
        padding-left: 20px;
        margin-top: 10px;
    }
    p{
        margin-top: 20px;
    }
</style>
<body>
    <div>
        <!-- <h3>经营品牌</h3> -->
        <?php if (!empty($brand)) : ?>
        <p><?= isset($brand) ? $brand : '' ;?></p>
        <?php else:?>
        <p><?= '暂无数据' ?></p>
        <?php endif; ?>
    </div>
</body>
</html>
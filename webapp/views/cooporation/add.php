<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use webapp\models\BrandQualification;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model webapp\models\BrandQualification */

$this->title = '添加合作服务商';
$this->params['breadcrumbs'][] = ['label' => '合作服务商', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jn-title-box no-margin-bottom" xmlns="http://www.w3.org/1999/html">
    <span class="jn-title">添加合作服务商
    </span>
</div>
<!--引入CSS-->
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/webuploader/webuploader.css');?>">
<!--<link rel="stylesheet" type="text/css" href="<?php /*echo Url::to('/css/bootstrap-select.css');*/?>">-->
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jquery.autocomplete.css');?>">
<!-- <script type="text/javascript" src="http://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>     -->
<!-- <link rel="stylesheet" type="text/css" href="http://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css">   -->
<!--引入JS-->
<?=Html::jsFile('@web/webuploader/webuploader.js')?>
<?=Html::jsFile('@web/js/bootstrap-select.js')?>
<style>
    .datexz-wap{
        top: 37px;
    }
    .form-control[disabled], .form-control[readonly]{
        background: #fff;border: 1px solid  #ccc;cursor:pointer;
    }
    .layui-layer-btn .layui-layer-btn0{
        display: none;
    }
    .text-right{width: 120px}
    .form-group{height: 45px;}
    .popover{max-width: 500px !important;}
    #provider_select{display: none;width: 100%;min-height: 30px;border: 1px solid #ccc;border-top-color:transparent;border-radius: 4px;position: absolute;background: #fff;
    top: 36px;}
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/cooporation/add" method="post" id="cooporation-add"   class = 'form-horizontal'>

                    <div class="form-group field-workorder-account_id">
                        <label class="col-xs-2 control-label text-right" for="workorder-account_id">
                            <i class="red-i">*</i> 服务商名称
                        </label>
                        <span class="c_red"></span>
                        <div class="col-xs-5 fuzzy-query">
                            <!--<input type="text" name="" class="form-control" id="account_name" onkeyup="selectSearch(this.value,event)" onfocus="selectSearch(this.value,event)" onblur="hideSelect()">-->
                            <input type="text" name="service_provider_name" class="form-control" id="service_provider_name" value="" datatype="*" nullmsg="请输入合作服务商" />
                            <input type="hidden" name="service_provider_id" class="form-control" id="service_provider_id" value="" />
                           <!--  <div class="col-xs-5 fuzzy-query"> -->

                                <ul style="" id="provider_select"></ul>
                            <!-- </div> -->
                        </div>

                    </div>

                    <div class="form-group field-workorder-account_id">
                        <label class="col-xs-2 control-label text-right" for="workorder-account_id">
                            <i class="red-i">*</i> 申请信息
                        </label>
                        <span class="c_red"></span>
                        <div class="col-xs-5 fuzzy-query">
                            <input class="form-control" id="info" name="info" datatype="*" nullmsg="请输入申请信息，以便服务商进行核实" />
                            <ul style="display: none;width: 545px;min-height: 30px;border: 1px solid rgb(0, 0, 0);" id="account_select">

                            </ul>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-xs-2" style="width: 120px"> </label>
                        <div class="col-xs-5 fuzzy-query" style="line-height: 34px">
                            <div style="float: left;"><span style="text-align: center;">合作服务商未入驻， <a class="clickButton1" >点此邀请</a></span></div>
                            <div class="col-xs-2">
                                <input type="submit" class="btn btn-orange mgr20 " style=" color:#FFF;" value="提交申请"  />
                            </div>


                        </div>

                    </div>
                </form>
                </div>

            </div>

        </div>
    </div>
</section>
<script type="text/javascript" src="/js/jquery-migrate-1.1.0.js"></script>
<script src="/js/jquery.autocomplete.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=bbcdabda1427179d77bb95fac7b2b902&plugin=AMap.Geocoder"></script>
<script src="/js/clipboard.min.js"></script>
<script>
    //表单验证

    //var index = parent.layer.getFrameIndex(window.name);
    var index = layer.tips();
    console.log(index);
    $("#cooporation-add").Validform({
        ajaxPost:true,
        tiptype:3,
        callback:function(data){
            if (data.code == 200) {
                $.Hidemsg();
                parent.alert(data.message);
                parent.layer.close(index);
                window.location.reload();
            }else{
                $.Hidemsg();
                alert(data.message);

            }
        }

    });


    function accountList(accountId){
        getValue = $("#provider_select_"+accountId).text();
        $("#service_provider_name").val(getValue);
        $("#service_provider_id").val(accountId);
        $("#provider_select").css("display",'none');
    }



    $("#addParentIframe").click(function () {
//        var data = $("#skillId").val();
        layer.open({
            type: 2,
            title: '添加客户',
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: true,
            content: '/account/add/'
        });
    });


    $(".clickButton1").click(function(){
        alert(33);
        var id = $(this).attr("id-data");
        var status = $(this).attr("status-data");
        var html = $(this).html();
        // var str = '<p>方法1：直接将服务商入驻链接发送给合作方<input value="http://www.uservices.cn/index/merchant/register.html" /></p>';
        //     str += '<p>方法2:发送邮件通知合作方入驻 <input value="" id="sendMail"/><input type="button" id="clickButton" value="发送"/></p>'
        var str = ` 
                <div class="">
                   <div class="invite-list">
                       <span>方法1:</span>
                       <div class="invite-list-right">
                           <p>直接将服务商入驻链接发送给合作方</p>
                           <input type="text" class="invite-input" id="invitationLink" readonly="readonly" value="http://www.uservices.cn/index/merchant/register.html">
                           <a href="javascript:" class="btn btn-orange  copy-a" data-clipboard-action="copy" data-clipboard-target="#invitationLink" style="color: #fff" id="copy-a">复制链接</a>
                       </div>
                   </div>
                   <div class="invite-list">
                       <span>方法2:</span>
                       <div class="invite-list-right">
                           <p>发送邮件通知合作方入驻 </p>
                           <input type="text" class="invite-input"  id="sendMail">
                           <input type="button" class="btn btn-orange" id="clickButton" style="width:82px" value="发送"/>
                       </div>
                   </div>
                   
               </div>


          `;
        layer.open({
            title: '邀请服务商入驻'
            ,content: str,area:'590px',
            success:function(index){
                $('#clickButton').click(function(){
                    var mail = $('#sendMail').val();
                    if(mail){
                        $.ajax({
                            url:'/cooporation/send-mail',
                            type:'POST',
                            data:{mail:mail},
                            success:function(msg){
                                //alert(msg);
                                if(msg){
                                    layer.alert('邮件发送成功！', function(index){
                                        window.location.reload();
                                        layer.close(index);
                                    });
                                }
                            }
                        })
                    }else{
                        $("#sendMail").val('请输入邮箱地址');
                        $("#sendMail").focus(function(){
                            $(this).val('');
                        })
                    }
                })
            }
        });

      /*  layer.confirm('确认”<b>'+html+'</b>“操作吗?', {icon: 3, title:'服务商管理'}, function(index){
            $.ajax({
                url:'/cooporation/change-status',
                type:'POST',
                data:{id:id,status:status},
                success:function(msg){
                    var dataObj=eval("("+msg+")");
                    if(dataObj.success == true){
                        layer.alert('操作成功！', function(index){
                            window.location.reload();
                            layer.close(index);
                        });

                    }

                }
            })
            layer.close(index);
        });*/
    })
    var clipboard = new Clipboard('.copy-a');
    clipboard.on('success', function(e) {
        console.log(e);
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });



    $(function () {
        //用户联想
        $("#service_provider_name").autocomplete("/cooporation/get-account", {
            minChars: 1,
            matchCase:false,//不区分大小写
            autoFill: false,
            max: 10,
            dataType: 'json',
            //width:'',
            extraParams:{v:function() { return $('#clubmember-compay').val();}},
            parse: function(data) {
                return $.map(eval(data), function(row) {
                    $("#service_provider_id").val(row.id);
                    //test();
                    return {
                        data: row,
                        value: row.id,    //此处无需把全部列列出来，只是两个关键列
                        result: row.name
                    }
                });
            },
            formatItem: function(row, i, max,term) {
                var v = $("#clubmember-compay").val();
                return  row.name;
                if(row.name.indexOf(v) == 0)
                {
                    return  row.name;
                }
                else
                    return false;
            },
            formatMatch: function(row, i, max) {
                return row.name;
            },
            formatResult: function(row) {
                return row.name;
            },
            reasultSearch:function(row,v)//本场数据自定义查询语法 注意这是我自己新加的事件
            {
                //自定义在code或spell中匹配
                if(row.name.indexOf(v) == 0)
                {
                    return row;
                }
                else
                    return false;
            }
        });

        function test(id){
            alert(id);
        }

    });
</script>

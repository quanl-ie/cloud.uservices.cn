<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '查看服务区域';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">

<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    .col-sm-4, .col-md-4, .col-lg-4{
        padding-left: 0;
    }
    input[type=radio]{
        margin-top: 10px;
    }
    body{
        overflow-x: hidden;
        font-family: "微软雅黑"；
    }
    table{
        border: 1px solid #ccc;
        width: 450px;
        margin:30px auto;
        font-size: 14px;
    }
    th,td{
        border-right: 1px solid #ccc;
        text-align: center;height: 45px;
        font-family: "微软雅黑"；

    }
    th{
       
        background: #f6fafe;
    }
    tr{
         border-bottom: 1px solid #ccc;
    }
</style>
<body>
<!-- <div class="row popup-boss">
    <div class="col-md-8 information-list"> -->
    <!-- <table>
        <tr><th>省份</th><th>城市</th><th>区县</th></tr>
        <tr><td><?=$area['province']?></td><td><?=$area['city']?></td><td><?=$area['district']?></td></tr>
    </table> -->
    <table>
        <thead>
            <tr><th>省份</th><th>城市</th><th>区县</th></tr>
        </thead>
        <tbody>
            <tr><td><?=$area['province']?></td><td><?=$area['city']?></td><td><?=$area['district']?></td></tr>
        </tbody>
    </table>
<!-- 
</div> -->
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=bbcdabda1427179d77bb95fac7b2b902&plugin=AMap.Geocoder"></script>
</body>
</html>
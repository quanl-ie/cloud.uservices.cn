<?php
$this->title = '收费标准详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="/css/iconfont.css">
<style type="text/css">
    .region-box-ul,.region-box-title{margin-bottom: 0;}
    .icon-shouqi1{position: absolute;right: 25px;top: 10px;}
    .form-control[readonly]{cursor: pointer;background: #fff}
    .add-payService>.information-list>.addinput-list{margin-left: 120px;}
    li>input[type="radio"]{position:static;clip:rect(0,0,0,0);opacity:0}
    li>input[type="radio"]:checked+label::before{background-color:#2693ff;background-clip:content-box;padding:2px;width:15px;height:15px}
    li>input[type="radio"]+label::before{content:"\a0";display:inline-block;vertical-align:middle;font-size:18px;width:15px;height:15px;margin-right:10px;border-radius:50%;border:1px solid #2693ff;margin-top:-3px}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">收费标准详情</span>
</div> 
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <!-- <form  method="post" id="cost-rule"> -->
                        <div class="information-list">
                            <label class="name-title-lable" >商家名称：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <input type="text" name="" id="level_title" data="<?= isset($manufactor->id) ? $manufactor->id :'' ?>" value="<?= isset($manufactor->company) ? $manufactor->company :'' ?>" class="form-control" style='border: 0px solid #fff;'>                         
                            </div>
                        </div>
                        <div class="information-list">
                            <label class="name-title-lable" >服务城市：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list bothEnds">
                               <div class="city3">
                                    <div class="region-box">
                                        <p class="region-box-title">选择服务省份</p> 
                                        <ul class="region-box-ul" id="provinceUl">
                                        <?php if (!empty($province)) : ?>
                                        <?php foreach ($province as $key => $val): ?>
                                        <?php if (isset($provinceId) && in_array($val['region_id'], $provinceId)) : ?>
                                        <li>
<!--                                            <input type="radio" id="--><?//=$val['region_id'] ?><!--" checked="checked" name="province">-->
                                            <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                                        </li>
                                        <?php else : ?>
                                        <li>
<!--                                            <input type="radio" id="--><?//=$val['region_id'] ?><!--" name="province">-->
                                            <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                                        </li>
                                        <?php endif; ?>
                                        <?php endforeach ; ?>
                                        <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="city3">
                                    <div class="region-box">
                                        <p class="region-box-title">选择服务城市</p>
                                        <ul class="region-box-ul" id="cityUl">
                                        <?php if (!empty($province)) : ?>
                                        <?php foreach ($city as $key => $val): ?>
                                        <?php if (isset($cityId) && in_array($val['region_id'], $cityId)) : ?>
                                        <li>
<!--                                            <input type="radio" id="--><?//=$val['region_id'] ?><!--" checked="checked" name="city">-->
                                            <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                                        </li>
                                        <?php else : ?>
                                        <li>
<!--                                            <input type="radio" id="--><?//=$val['region_id'] ?><!--" name="city">-->
                                            <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                                        </li>
                                        <?php endif; ?>
                                        <?php endforeach ; ?>
                                        <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="city3">
                                    <div class="region-box">
                                        <p class="region-box-title">选择服务区/县</p>
                                        <ul class="region-box-ul" id="areaUl">
                                        <?php if (!empty($province)) : ?>
                                        <?php foreach ($district as $key => $val): ?>
                                        <?php if (isset($districtId) && in_array($val['region_id'], $districtId)) : ?>
                                        <li area="<?=$val['region_id'] ?>">
<!--                                            <input type="radio" id="--><?//=$val['region_id'] ?><!--" checked="checked" name="area">-->
                                            <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                                        </li>
                                        <?php else : ?>
                                        <li>
<!--                                            <input type="radio" id="--><?//=$val['region_id'] ?><!--" name="area">-->
                                            <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                                        </li>
                                        <?php endif; ?>
                                        <?php endforeach ; ?>
                                        <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="city3_fix"></div>
                            </div>
                        </div>
                        
                        <div class="information-list">
                            <label class="name-title-lable" >品牌：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list bothEnds" id="brand">
                               <div class="radioLable">
                                    <input type="radio" name="brand_id" value="-1" id="brand_id_0">
                                    <label  for="brand_id_0">通用</label>
                                </div>
                                <?php if (!empty($brand)) : ?>
                                <?php foreach ($brand as $key => $val) : ?>
                                <div class="radioLable">
                                    <input type="radio" name="brand_id"  value="<?=$key ?>" id="brand_id_<?=$key ?>">
                                    <label  for="brand_id_<?=$key ?>"> <?=$val ?></label>
                                </div>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                            </div>
                        </div>
                        
                        <div class="information-list">
                            <label class="name-title-lable" >产品类目：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list bothEnds" id="productCategory">
                                <div class="radioLable">
                                    <input type="radio" name="class_id" value="-1" id="class_id_0">
                                    <label  for="class_id_0">通用</label>
                                </div>
                                <?php if (!empty($class)) : ?>
                                <?php foreach ($class as $key => $val) : ?>
                                <div class="radioLable">
                                    <input type="radio" name="class_id" value="<?=$key ?>" id="class_id_<?=$key ?>">
                                    <label  for="class_id_<?=$key ?>"> <?=$val ?></label>
                                </div>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                            </div>
                        </div>
                        
                    
                        <div class="information-list">
                            <label class="name-title-lable" >服务类型：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list bothEnds" id="serviceType">
                                <div class="radioLable">
                                    <input type="radio" name="type_id" value="-1" id="type_id_0">
                                    <label  for="type_id_0">通用</label>
                                </div>
                                <?php if (!empty($type)) : ?>
                                <?php foreach ($type as $key => $val) : ?>
                                <div class="radioLable">
                                    <input type="radio" name="type_id" value="<?=$key ?>" id="type_id_<?=$key ?>">
                                    <label  for="type_id_<?=$key ?>"> <?=$val ?></label>
                                </div>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                                <div class="city3_fix"></div>
                            </div>
                        </div>
                        <div class="information-list">
                            <label class="name-title-lable" >收费项目：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list bothEnds" id="payService">
                                
                            </div>
                        </div>
                        <div class="information-list">
                            <label class="name-title-lable" >价格：</label>
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list" id="price" style="line-height: 2.3"></div>
                        </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        // window.parent.document.location.reload();
        <?php endif;?>
    };
</script>
<script type="text/javascript">
    // var cityId='';
    var manufactorId = $("#level_title").attr('data');
    $(function() {

    })

    // 联动点击
    $('#provinceUl li input').on('change',function() {
        $('#wholeArea').prop("checked",false);
        $('#wholeCity').prop("checked",false);
        $('#areaUl').html('');
        var wholeCheck = $('#provinceUl li input');
        var cityBox = $('#cityUl');
        var Url = '/cooporation/get-city';
        city ($(this).attr('id'),1,cityBox,Url,1);
    });
    $('#cityUl').on('change','li input',function() {
        $('#wholeArea').prop("checked",false);
        var wholeCheck = $('#cityUl li input');
        var cityBox = $('#areaUl');
        var Url = '/cooporation/get-district';
        city ($(this).attr('id'),1,cityBox,Url,1); 
    });
    $('#areaUl').on('change','li input',function() {
        var wholeCheck = $('#areaUl li input');
        cityId = $(this).attr('id');
    })
    function city (id,judge,cityBox,Url,type) {
        var cityBox = cityBox;
        var provinceUlHtml;
        if(judge==1){
            $.ajax({
                    url:Url,
                    data:{id:manufactorId,cityId:id,type:type},
                    type:'POST',
                    dataType:'json',
                    success:function(data){
                        $.each(data,function (i,pro) {
                             provinceUlHtml += `
                                    <li>
                                        <input type="radio" id="${pro.region_id}" name="">
                                        <label class="brand-label" for="${pro.region_id}"><i class="gou-i"></i>${pro.region_name}</label>
                                    </li>
                              `;
                              cityBox.html(provinceUlHtml.replace('undefined',''));
                        });
                    }
                });
        }
    }
    
    // 联动点击获取收费项目
    $('.radioLable label').on('click',function () {
        setTimeout(function () {
            var levelId =$('#level_title').attr('data');
            var brandId = $('input[name=brand_id]:checked').attr('value');
            var classId = $('input[name=class_id]:checked').attr('value');
            var typeId = $('input[name=type_id]:checked').attr('value');
            if(brandId!=undefined&& classId!=undefined&&typeId!=undefined){
                // var city3_fix = `
                // <div class="city3_fix"></div>
                // <div class="city3_fix"></div>
                // <div class="city3_fix"></div>
                // <div class="city3_fix"></div>`;
                $.ajax({
                    type: "POST",
                    url:'/cooporation/link-age',
                    data:{
                        levelId:levelId,
                        brandId:brandId,
                        classId:classId,
                        typeId:typeId,
                        // cityId:cityId
                    },
                    dataType:'json',
                    success: function (data) {
                        if (data.code == 200) {
                            var tbodyContentHtml = '';
                            $.each(data.data,function (i,pro) {

                                levelId = pro.cost_level_id;
                                tbodyContentHtml +=`
                                    <div class="radioLable">
                                        <input type="radio" name="payService" value="" id="cost_item_${pro.id}" data="${pro.price}元/${pro.unit}">
                                        <label  for="cost_item_${pro.id}">${pro.cost_item_name}</label>
                                    </div>
                                    `;
                                

                            });
                            $('#payService').html(tbodyContentHtml);
                            // $('#payService').append(city3_fix);
                            $('#payService').find('input').change(function (argument) {
                                var cc = $(this).attr('data');
                                $('#price').html(cc);
                            })

                        }else{
                            $('#payService').html('');
                            $('#price').html('');
                        }
                    }
                });
            }
        },100);
    });
    
</script>
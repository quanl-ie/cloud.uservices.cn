<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '商家订单结算列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
        border-top:2px solid #2693ff;
    }
    table{
        margin-top: 20px;
        border-right:1px solid #ccc;border-bottom:1px solid #ccc;
    }
    th,td{
        height: 40px;
        text-align: center;
        border-left:1px solid #ccc;border-top:1px solid #ccc
    }

</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">商家订单结算列表
     <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="创建结算单后，系统需要一段时间生成，之后将自动发送给商家"></span>
    </span>
</div>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body" style="font-size: 14px;">
                    <div class="col-md-12  col-lg-12">
                        <div style="width: 100%;" class="tab-group">
                            <ul id="myTab" class="nav nav-tabs">

                                <li  class="active"><a href="/cost-order/wait-cost">待结算</a></li>
                                <li>
                                    <a href="/cost-order/have-costed" >已结算</a>
                                </li>
                            </ul>
                        </div>

                        <div id="myTabContent" class="tab-content">
                            <!--搜索开始-->
                            <div class="form-group jn-form-box">
                                <form action="/cost-order/wait-cost" class="form-horizontal form-border" id="form">
                                <div class="form-group jn-form-box">
                                    <!-- 搜索栏 -->
                                    <div class="col-sm-12 no-padding-left">

                                        <div class="single-search">
                                            <label class=" search-box-lable">订单编号</label>
                                            <div class="single-search-kuang1">
                                                <input type="text" class="form-control" name="order_no" value="<?php echo isset($_GET['order_no'])?$_GET['order_no']:'';?>">
                                            </div>
                                        </div>
                                        <div class="single-search">
                                            <label class=" search-box-lable">服务类型</label>
                                            <div class="single-search-kuang1">
                                                <select name="type_id" id="brand_id" class="form-control">
                                                    <option>全部</option>
                                                    <?php if ($search['typeList']) :?>
                                                    <?php foreach ($search['typeList'] as $key=>$val):  ?>
                                                            <option value="<?=$key?>"><?=$val?></option>
                                                    <?php endforeach;?>
                                                    <?php endif;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="single-search">
                                            <label class=" search-box-lable">服务产品</label>
                                            <div class="single-search-kuang1">
                                                <select name="class_id" id="brand_id" class="form-control">
                                                    <option>全部</option>
                                                    <?php if ($search['classList']) :?>
                                                        <?php foreach ($search['classList'] as $key=>$val):  ?>
                                                            <option value="<?=$val['id']?>"><?=$val['title']?></option>
                                                        <?php endforeach;?>
                                                    <?php endif;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="single-search">
                                            <label class=" search-box-lable">品牌</label>
                                            <div class="single-search-kuang1">
                                                <select name="brand_id" id="brand_id" class="form-control">
                                                    <option>全部</option>
                                                    <?php if ($search['brandList']) :?>
                                                        <?php foreach ($search['brandList'] as $key=>$val):  ?>
                                                            <option value="<?=$val['brand_id']?>"><?=$val['brand_name']?></option>
                                                        <?php endforeach;?>
                                                    <?php endif;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="tow-search" >
                                            <label class="search-box-lable">服务完成时间</label>
                                            <div class="single-search-kuang2">
                                                <div class="half-search">
                                                    <input type="text" id="datetimepicker-pay-top" class="form-control" name="start_time" value="<?php echo isset($_GET['finish_time'])?$_GET['finish_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})">
                                                    <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})"> <i class="icon2-rili"></i></span>
                                                    <span class="zhi"> 至</span>
                                                </div>
                                                <div class="half-search">
                                                    <input type="text" id="datetimepicker-pay-end" class="form-control" name="end_time" value="<?php echo isset($_GET['finish_time'])?$_GET['finish_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})" >
                                                    <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})"> <i class="icon2-rili"></i></span>
                                                </div>
                                                <p class="city3_fix"></p>
                                            </div>
                                        </div>
                                        <div class="search-confirm"> 
                                            <button class="btn btn-success"><i class="icon iconfont icon-sousuo2"></i> 搜索</button>
                                        </div>

                                    </div>
                                </div>
                                </form>
                            </div>
                            <!--搜索结束-->
                            <div class="tab-pane fade in active" id="customer-order">
                                <table  style="width: 100%;text-align:center;">
                                    <thead>
                                    <tr>
                                        <th>订单编号</th>
                                        <th>服务类型</th>
                                        <th>服务产品</th>
                                        <th>服务品牌</th>
                                        <th>订单金额</th>
                                        <th>完成服务时间</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($data) :?>
                                        <?php foreach ($data as $key=>$val):  ?>
                                            <tr>
                                                <td><a href="/order/view?order_no=<?=$val['order_no']?>"><?= isset($val['order_no']) ? $val['order_no'] : ''?></a></td>
                                                <td><?= isset($val['type_name']) ? $val['type_name'] : '' ?></td>
                                                <td><?= isset($val['class_name']) ? $val['class_name'] : '' ?></td>
                                                <td><?= isset($val['brand_name']) ? $val['brand_name'] : '' ?></td>
                                                <td><?= isset($val['amount']) ? sprintf('%0.2f',$val['amount']) : '' ?></td>
                                                <td><?= isset($val['finish_time']) ? date('Y-m-d H:i:s',$val['finish_time']) : '' ?></td>
                                            </tr>
                                        <?php endforeach;?>
                                    <?php else:?>
                                        <tr>
                                        <tr><td colspan="7" class="no-record">暂无待结算订单</td></tr>
                                        </tr>
                                    <?php endif;?>
                                    </tbody>
                                </table>

                            </div>

                            <div id="customer-product" class="tab-pane fade" ></div>
                            <div id="customer-address" class="tab-pane fade" ></div>
                        </div>

                    </div>
                    <div class="col-xs-12 text-center  pagination">
                        <?php echo $pageHtml;?>
                    </div>
                </div>

            </div>

        </div>

    </div>
</section>
<script>
$(".tip").popover();
</script>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '分组管理';
$this->params['breadcrumbs'][] = ['label'=>'分组管理','url'=>'/grouping/index'];
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time()?>">
<style type="text/css">
    .table-striped >tbody >tr:nth-of-type(odd){background: #fff;}
    .table-responsive{width: 100%;}
    .main{
        text-align: center; /*让div内部文字居中*/
        background-color: #fff;
        border-radius: 20px;
        width: 300px;
        height: 350px;
        margin: auto;
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        font-weight:bold
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <p class="jn-title">
       <span id="groupName"><?php echo $groupingName;?></span>的组员
    </p>
    <div class="right-btn-box">
        <input type="hidden" id="groupid" value="<?php echo $groupingId;?>" />
        <?php if(in_array('/grouping/ajax-batch-join-grouping',$selfRoles)){?>
            <a href="javascript:" class="btn btn-orange mgr8" id="addMenberBtn">新增组员</a>
        <?php };?>
        <a href="javascript:history.back(-1)" class="btn bg-f7">返回</a>
    </div>
</div> 
<section id="main-content">
    <div class="panel-body">
       <div class="table-responsive">
           <?php if ($data['list']):?>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>姓名</th>
                        <th>电话</th>
                        <th>职位</th>
                        <th width="350px;">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data['list'] as $val):?>
                    <tr>
                        <td class="technician-name"><?php echo $val['name'];?></td>
                        <td><?php echo $val['mobile'];?></td>
                        <td><?php echo $val['job_title']==2?'组长':'组员';?></td>
                        <td>
                            <?php if(in_array('/grouping/remove',$selfRoles)){?>
                                <a href="javascript:" onclick="removeGrouping(<?php echo $val['id'];?>,<?php echo $groupingId;?>,'<?php echo $val['name'];?>')"></i>移除</a>
                            <?php };?>
                            <?php if(in_array('/grouping/change',$selfRoles)){?>
                                <a href="javascript:" onclick="getDepartmentGroup(<?php echo $val['id'];?>,<?php echo $groupingId;?>,<?=$val['store_id']?>)">修改分组</a>
                            <?php };?>
                            <?php if(in_array('/grouping/change-title',$selfRoles)){?>
                                <a href="javascript:"  onclick="revisePlace(<?php echo $val['id'];?>,'<?php echo $val['name'];?>',<?php echo $val['job_title'];?>,<?php echo $groupingId;?>)">修改职位</a>
                            <?php };?>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
           <?php else:?>
               <div class="main">
                   <span>您还未添加组员</span>
                   <div style=" margin-top: 30px;">
                       <?php if(in_array('/grouping/default-technician',$selfRoles)){?>
                           <a href="javascript:" class="btn btn-orange mgr8" id="addMenberBtn2">新增组员</a>
                       <?php };?>
                   </div>
               </div>
           <?php endif;?>
        </div>
        <div class="col-xs-12 text-center pagination">
            <?php echo $pageHtml;?>
        </div>
    </div> 
</section>
<!-- 修改分组 -->
<div id="reviseGrouping" style="display: none">
    <div class="col-md-12" style="text-align: center;padding:10px">
        <p class="col-md-12" style="margin-bottom: 20px">选择技师分组</p>
        <select  class="col-md-12 form-control" id="optGroupingIds">
            <option value="">请选择</option>
            <?php foreach ($grouping as $val): ?>
                <?php if($val['id']!=$groupingId): ?>
                <option value="<?php echo $val['id'];?>"><?php echo $val['name'];?></option>
                <?php endif; ?>
            <?php endforeach; ?>
        </select>
        <div class="col-sm-12" style="margin-top:28px;">
            <button class="btn btn-orange" id="changeGrouping"><i class="fa fa-search"></i>确定</button>
            <button class="btn bg-f7 layerCancel"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>
<script>
	//提示
    $(".tip").popover();
 
	// 新增组员弹窗  判断无数据 <p style="line-height: 210px">暂无未分组成员可选!</p> 替换ul
    // 新增组员弹窗
    var addNewMemberHtml = '<div id="addMenber">\n' +
        '        <div class="col-md-12">\n' +
        '            <ul class="popup-nr196" id="addMemberUl">\n' +
        '            </ul>\n' +
        '        </div>\n' +
        '        <div class="col-sm-12" style="margin-top:15px;">\n' +
        '            <button class="btn btn-orange mgr8" id="btnAddMember"><i class="fa fa-search"></i>确定</button>\n' +
        '            <button class="btn bg-f7 layerCancel"><i class="fa fa-search" ></i>取消</button>\n' +
        '        </div>\n' +
        '    </div>';
    var addMenberTitle = $('#groupName').text();
 	$('#addMenberBtn,#addMenberBtn2').click(function(){
        var groupid = $("#groupid").val();
 		layerIndex = layer.open({
            type: 1,
            title: '新增【'+addMenberTitle+'】成员',
            area: ['635px', '374px'],
            fixed: true,
            maxmin: false,
            content: addNewMemberHtml
        });

        $.getJSON('/grouping/default-technician',{group_id:groupid},function (json) {
            var memberHtml = '';
            var data = json.list;
            for(i in data)
            {
                memberHtml+='<li class="li33">\n' +
                '                    <input type="checkbox" id="y15'+data[i].id+'" name="technician" value="'+data[i].id+'">\n' +
                '                    <label for="y15'+data[i].id+'"><i class="gou-i"></i>\n' +
                '                    <span>'+data[i].name+'</span>\n' +
                '                    <span>'+data[i].mobile+'</span></label>\n' +
                '            </li>\n';
            }
            if(memberHtml!=''){
                $('#addMemberUl').html(memberHtml);
            }
            else {
                $('#addMemberUl').html(`<div class="no-data-page" style="min-height:auto;margin-top:40px">
                            <div>
                                <div class="kbtp"></div>
                                <p class="no-data-zi">暂无分组成员可选</p>
                            </div>
                        </div>`);
            }
        });
 	})


    //点击请求当前技师的所属机构下的分组
    function getDepartmentGroup(jsId,grouping_id,d_id) {
        $.post("/grouping/get-department-group",{id:d_id},function(data) {
            if(data) {
                $("#optGroupingIds").html(data);
                reviseGrouping(jsId,grouping_id)
            }
        })
    }

 	// 修改分组弹窗
    var technician_id = 0;
    function reviseGrouping (technician_id,grouping_id) {
 	    window.technician_id = technician_id;
        layerIndex = layer.open({
            type: 1,
            title: '提示',
            area: ['300px', '220px'],
            fixed: true,
            maxmin: false,
            content: $('#reviseGrouping').html()
        });
    }

    var jobtitleHtml = '<div id="revisePlace">\n' +
        '        <div style="text-align: left;border-bottom: 1px solid #e8e8e8;margin-top: 20px">\n' +
        '            <div class="radio-w200">\n' +
        '                <input type="radio" name="jobtitle" value="2" id="4">\n' +
        '                <label for="4">\n' +
        '                组长<span id="spanLeader"></span></label>\n' +
        '            </div>\n' +
        '            <div  class="radio-w200">\n' +
        '                <input type="radio" name="jobtitle" value="1" id="3">\n' +
        '                <label for="3">\n' +
        '                组员<span id="spanMember"></span></label>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '        <span style="color: red">注：指定新组长后，原组长将自动调整为组员</span>\n' +
        '        <div class="col-sm-12" style="margin-top:15px;">\n' +
        '            <button class="btn btn-orange" id="btnChangeJobTitle"><i class="fa fa-search"></i>确定</button>\n' +
        '            <button class="btn bg-f7 layerCancel"><i class="fa fa-search" ></i>取消</button>\n' +
        '        </div>\n' +
        '    </div>';

    // 修改职位弹窗
    function revisePlace (id,name,jobtitle,groupid) {
        layerIndex = layer.open({
            type: 1,
            title: '修改【'+name+'】的职位为：',
            area: ['360px', '250px'],
            fixed: true,
            maxmin: false,
            content: jobtitleHtml
        });

        if(jobtitle == 2){
            $('#spanLeader').html('(原职位)');
        }
        else {
            $('#spanMember').html('(原职位)');
        }
        $('input[name="jobtitle"]').each(function () {
            if($(this).val() == jobtitle){
                $(this).prop('checked',true);
            }
        });
        $('#btnChangeJobTitle').attr('data-id',id);
        $('#btnChangeJobTitle').attr('data-groupid',groupid);

    }
    // 移出
    function removeGrouping(technician_id,grouping_id,technician_name) {
 	    this.technician_id = technician_id;
 	    this.grouping_id = grouping_id;
 	    this.technician_name = technician_name;
    	confirm("是否确认将【"+technician_name+"】从该组移除？移除后，该技师将分配到【默认组】",function (obj) {
            $.getJSON('/grouping/remove',{technician_id:obj.technician_id,grouping_id:obj.grouping_id},function (json) {
                if(json.success == true){
                    alert('移除成功！组员分配至【默认组】');
                }
                else {
                    alert(json.message);
                }
                setTimeout(function () {
                    document.location.reload(true);
                },1000);
            })
        },this);
    }
    //关闭弹层
    $("body").delegate(".layerCancel","click",function(){
        if(layerIndex){
            layer.close(layerIndex);
        }
    });
 	//修改分组
    $("body").delegate("#changeGrouping","click",function(){
        var groupId = $(this).parent('div').prev('#optGroupingIds').val();
        $.getJSON('/grouping/change',{technician_id:window.technician_id,grouping_id:groupId},function (json) {
            if(json.success == true){
                alert('修改成功');
            }
            else {
                alert(json.message);
            }
            setTimeout(function () {
                document.location.reload(true);
            },1000);
        })
    });
    //修改职位
    $('body').delegate('#btnChangeJobTitle','click', function () {
        var id = $(this).attr('data-id');
        var groupId = $(this).attr('data-groupid');
        var jobtitle = 0;

        $('input[name="jobtitle"]').each(function () {
            if($(this).prop('checked') == true){
                jobtitle = $(this).val();
            }
        });


        $.getJSON('/grouping/change-title',{technician_id:id,grouping_id:groupId,job_title:jobtitle},function (json) {
            if(json.success == true){
                alert('修改成功',function () {
                    document.location.reload(true);
                });
            }
            else {
                alert(json.message,function () {
                    document.location.reload(true);
                });
            }
        })
    });
    //新增组员
    $('body').delegate('#btnAddMember','click', function () {

        var jsIds = [];
        $('input[name="technician"]').each(function () {
            if($(this).prop('checked') == true){
                jsIds.push($(this).val());
            }
        });

        if(jsIds.length == 0){
            alert('至少要选一名组员,才可以操作!');
            return false;
        }

        $.getJSON('/grouping/ajax-batch-join-grouping',{technician_ids:jsIds,grouping_id:$('#groupid').val()},function (json) {
            if(json.success == true){
                alert('添加成功',function () {
                    document.location.reload(true);
                });
            }
            else {
                alert('添加失败');
            }
        })
   });

    <?php if (!$data['list']):?>
    $('#addMenberBtn2').click();
    <?php endif;?>

</script>
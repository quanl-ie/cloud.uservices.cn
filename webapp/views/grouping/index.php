<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '分组管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time();?>">
<style type="text/css">
    .table-striped >tbody >tr:nth-of-type(odd){background: #fff;}
    .drop-dw-layerbox{width: calc(100% - 86px)}
    .drop-dw-layerbox>button{width: 100%;position: absolute;left: 0;bottom: 0;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">分组管理
        <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-content="您可对技师进行分组管理，其中组长可在技师端APP指派技师上门。"></span>
    </span>
    <div class="right-btn-box">
        <?php if(in_array('/grouping/add',$selfRoles)){?>
            <a href="/grouping/add" style="colro:#ffffff;"><span class="btn btn-orange">新增分组</span></a>
        <?php };?>
    </div>
</div> 
<section id="main-content">
    <div class="panel-body">
        <form action="/grouping/index" class="form-horizontal form-border" id="form">
            <div>
                <!-- 搜索栏 -->
                <?php if(!empty($department)):?>
                    <div class="single-search region-limit" style="overflow:visible;">
                        <label class=" search-box-lable">所属机构</label>
                        <input type="hidden" id="department_id" value="<?php echo $department_id;?>">
                        <div class="single-search-kuang1" id="de">
                            <input type="text" class="form-control" value="<?=isset($department_name) ? $department_name : '' ?>" id="department"  readonly <?php if(!empty($department)){ echo 'onclick="clickCategory()" placeholder="请选择机构"';}?> >
                            <input type="hidden" class="form-control" value="<?=isset($params['department_id']) ? $params['department_id'] : '' ?>" id="department_ids" name="department_ids">
                            <div class="drop-dw-layerbox" style="display: none;">
                                <div class="ul-box-h180">
                                    <ul id="depart1" class="depart">
                                        <?php if (!empty($department)) : ?>
                                            <?php foreach ($department as $key => $val) : ?>
                                                <li>
                                                             <span>
                                                                <?php if ($val['exist'] == 1) : ?>
                                                                    <i class="icon2-shixinyou" onclick="getDepartment(this)"></i>
                                                                <?php endif;?>
                                                                 <input type="checkbox" id="a<?=$val['id'] ?>" value="<?=$val['id'] ?>" name="department_id[]" <?php if(isset($params['department_id']) && (in_array($val['id'],explode(",",$params['department_id'])))){ echo "checked=true"; }?>>
                                                                 <label for="a<?=$val['id'] ?>"><?=$val['name'] ?><i class="gou-i"></i></label>
                                                            </span>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <button class="btn btn-orange" id="belongedBtn">确定</button>
                            </div>

                        </div>

                    </div>


                <div class="search-confirm">
                    <button class="btn btn-success"><i class=" "></i> 搜索</button>
                </div>
                <?php endif?>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>所属机构</th>
                            <th>组名</th>
                            <th>组长</th>
                            <th>全部组员</th>
                            <th>备注</th>
                            <th width="200px">操作</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if(isset($data['list']) && $data['list']):?>
                    <?php foreach ($data['list'] as $key=>$val):?>
                        <tr>
                            <td><?=$val['department_name']?></td>
                            <td class="intercept20" style="height: 50px;"><?php echo $val['name'];?></td>
                            <td><?php echo $val['group_leader_name'];?><br /><?php echo $val['group_leader_mobile'] ? $val['group_leader_mobile'] : '';?></td>
                            <td>
                                <?php if(in_array('/grouping/technician',$selfRoles)){?>
                                    <a href="/grouping/technician?grouping_id=<?php echo $val['id'];?>"><?php if($val['hasMember'] == 1):?>点击查看<?php else:?>添加<?php endif;?></a>
                                <?php };?>
                            </td>
                            <td  class="intercept20"><?php echo $val['remark'];?></td>
                            <td  class="operation">
                                <?php if(in_array('/grouping/del',$selfRoles)){?>
                                    <a href="/grouping/del?id=<?php echo $val['id'];?>" onclick="return confirm('是否确认删除【<?php echo  $val['name']?>】？删除后，所有组员将自动修改分组至默认组',function(obj){document.location.href=obj.href;},this)" class="cancelOrder mgr8">删除</a>
                                <?php };?>
                                <?php if(in_array('/grouping/update',$selfRoles)){?>
                                    <a href="/grouping/update?id=<?php echo $val['id'];?>" >编辑</a>
                                <?php };?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    <?php elseif($dataAuthId == 1) : ?>
                        <tr>
                            <td colspan="6">暂无数据</td>
                        </tr>
                    <?php endif; ?>
                    <?php
                    $page = isset($_GET['page'])?$_GET['page']:1;
                    ?>
                    <?php if(($dataAuthId != 1) && ($total_page == $page)) :?>
                        <tr>
                            <td><?php echo $defaultName; ?></td>
                            <td>默认组</td>
                            <td style="height: 50px;">-</td>
                            <td>
                                <?php if(in_array('/grouping/default-technician',$selfRoles)){?>
                                    <a href="/grouping/default-technician">点击查看</a>
                                <?php };?>
                            </td>
                            <td>默认组包含所有未分组成员</td>
                            <td  class="operation">
                            </td>
                        </tr>
                    <?php endif;?>
                    </tbody>
                </table>
            </div>
        </form>
        <div class="col-xs-12 text-center pagination">
                <?php echo $pageHtml;?>
        </div>
    </div> 
</section>
<script>
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message');?>');
    <?php endif;?>
    $('.intercept20').each(function(){
        var text = ($(this).text().slice(0,20))+'...'
        if($(this).text().length>20){
            $(this).text(text);
        }
    });
    $("[data-toggle='popover']").popover();



    // 选中内容
    $('#belongedBtn').bind('click', function() {
        //event.preventDefault();
        var id_array= [];
        var data_array=[];
        $('input[name="department_id[]"]:checked').each(function(){
            data_array.push($(this).next('label').text());
            id_array.push($(this).val());//向数组中添加元素
        });
        var idstr=id_array.join(',');//将数组元素连接起来以构建一个字符串
        var datatr = data_array.join('；')
        $('#department').val(datatr);
        $('#department_ids').val(idstr);
        $('.drop-dw-layerbox').hide();
        return false;
    });
    $(document).click(function(event){
        var _con = $('.drop-dw-layerbox');  // 设置目标区域
        if(!_con.is(event.target) && _con.has(event.target).length === 0){ // Mark 1
            //$('#divTop').slideUp('slow');  //滑动消失
            //$('.drop-dw-layerbox').hide(1000);     //淡出消失
        }
    });
    var departmentIdArray = $('#department_ids').val().split(',');
    function getDepartment(el) {
        var id = $(el).next('input').val();
        var _this = $(el).parent('span');
        var auth = <?php echo $auth;?>;
        var company_id = <?php echo $company_id;?>;
        var self_id = $("#department_id").val();
        $.getJSON('/common/ajax-get-department',{'pid':id,"auth":auth,'self_department_id':self_id,"company_id":company_id},function (data) {
            var ulData = '';
            if (_this.next('ul').size()==0) {
                ulData +="<ul>"
                $(data.data).each(function(i,el) {
                    ulData +="<li><span>";
                    el.exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="getDepartment(this)"></i>':'';
                    ulData+='<input type="checkbox" id="a'+el.id+'" value="'+el.id+'" name="department_id[]" >  <label for="a'+el.id+'">'+el.name+'<i class="gou-i"></i></label></span></li>';
                });
                ulData +="</ul>";
                _this.parent('li').append(ulData);
                $.each(departmentIdArray,function(i,el) {
                    $('#a'+el).prop('checked',true);
                })
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');

            }else{
                if(
                    _this.next('ul').is(':hidden')){
                    $(el).removeClass('icon2-shixinyou');
                    $(el).addClass('icon2-shixinxia');
                    _this.next('ul').show();

                }else{
                    $(el).addClass('icon2-shixinyou');
                    $(el).removeClass('icon2-shixinxia');
                    _this.next('ul').hide();
                }
            }
            return false;
        })
    }
    function  clickCategory(el) {
        //event.stopPropagation();
        $.each(departmentIdArray,function(i,el) {
            $('#a'+el).prop('checked',true);
        })
        $('.drop-dw-layerbox').is(':hidden')? $('.drop-dw-layerbox').show(): $('.drop-dw-layerbox').hide();
    }
    $(document).click(function(event) {
        var region = $('.region-limit');
        if (!region.is(event.target)&&region.has(event.target).length ===0) {
            $('.drop-dw-layerbox').hide();
        }
    });
</script>
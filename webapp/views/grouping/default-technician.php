<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '查看组员';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<style type="text/css">
    .table-striped >tbody >tr:nth-of-type(odd){background: #fff;}
    .table-responsive{max-width: 1000px;width: 100%;}
    .main{
        text-align: center; /*让div内部文字居中*/
        background-color: #fff;
        border-radius: 20px;
        width: 300px;
        height: 350px;
        margin: auto;
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        font-weight:bold
    }
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">
        默认组的组员
        <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="默认组是系统生成的包含全部未分组成员的分组"></span>
    </span>
    <div class="right-btn-box">
        <a href="/grouping/index" class="btn bg-f7">返回</a>
    </div>
</div> 
<section id="main-content">
    <div class="panel-body">
       <div  style="max-width: 100%" class="table-responsive">
           <?php if($data['list']):?>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>姓名</th>
                        <th>电话</th>
                        <th>职位</th>
                        <th width="150px;">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data['list'] as $val): ?>
                    <tr>
                        <td><?php echo $val['name'];?></td>
                        <td><?php echo $val['mobile'];?></td>
                        <td>组员</td>
                        <td>
                            <!--  class="edit10-btn" -->
                            <a href="javascript:"  onclick="getDepartmentGroup(<?php echo $val['id'];?>,<?=$val['department_top_id'];?>)">修改分组</a></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
           <?php else:?>
               <div class="main">
                   <span>默认组暂无组员</span>
               </div>
           <?php endif;?>
        </div>
        <div class="col-xs-12 text-center pagination">
            <?php echo $pageHtml;?>
        </div>
    </div> 
</section>
<!-- 修改分组 -->
<div id="cancelLayer" style="display: none">
    <div class="col-md-12" style="text-align: center;padding:10px">
        <p class="col-md-12" style="margin-bottom: 20px">选择分组</p>
        <select  class="col-md-12 form-control" id="optHeadman">
            <option value="">请选择</option>
            <?php foreach ($grouping as $val): ?>
            <option value="<?php echo $val['id'];?>"><?php echo $val['name'];?></option>
            <?php endforeach; ?>
        </select>
        <div class="col-sm-12" style="margin-top:28px;">
            <button class="btn btn-orange" id="determineGrouping"><i class="fa fa-search"></i>确定</button>
            <button class="btn bg-f7" id="layerCancel"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>
<script>
    //提示
    $(".tip").popover();

    var technicianId = 0;
    var department_id = 0

    //点击请求当前技师的所属机构下的分组
    function getDepartmentGroup(jsId,d_id) {
        $.post("/grouping/get-department-group",{id:d_id},function(data) {
            if(data) {
                $("#optHeadman").html(data);
                reviseGrouping(jsId)
            }
        })
    }

    function reviseGrouping (jsId) {
        technicianId = jsId;
        layerIndex = layer.open({
            type: 1,
            title: '提示',
            area: ['300px', '220px'],
            fixed: true,
            maxmin: false,
            content: $('#cancelLayer').html()
        });
    }
    //关闭弹层
    $("body").delegate("#layerCancel","click",function(){
        if(layerIndex){
            layer.close(layerIndex);
        }
    });
    $("body").delegate("#determineGrouping","click",function(){
        var groupid = $(this).parent('div').prev('#optHeadman').val();
        if(technicianId == 0){
            alert('请选择组员');
            return false;
        }
        if(!groupid){
            alert('选择分组');
            return false;
        }

        $.getJSON('/grouping/ajax-join-grouping',{technician_id:technicianId,grouping_id:groupid},function (json) {
            if(json.success == true){
                alert('修改成功');
                document.location.reload(true);
            }
            else {
                alert('修改失败');
            }
        })
    })
</script>
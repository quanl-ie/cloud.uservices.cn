<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = $model->isNewRecord?'新增分组':'编辑分组';
$this->params['breadcrumbs'][] = ['label'=>'分组管理','url'=>'/grouping/index'];
$this->params['breadcrumbs'][] = $this->title;
use yii\captcha\Captcha;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<style>
    .name-title-lable{width: 65px;}
    .addinput-list{margin-left: 65px;}
    .form-horizontal{width: 400px;}
</style>
<div style="display:none;">
    <input type="text" />
    <input type="password" />
</div>
<section id="main-content">
    <div class="panel-body">
        <?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-horizontal',
                'id' => 'form2',
                'name' => 'form1',
                'enctype' => 'multipart/form-data',

            ],
            'fieldConfig' => [
                'template' => "{label}<span class=\"c_red\"></span>\n<div class=\"addinput-list\">{input}{hint}{error}</div>",
                'labelOptions' => ['class' => 'name-title-lable'],
            ]
        ]);
        ?>
        <?php echo $form->field($model, 'name')
            ->input('text',['placeholder'=>$model->getAttributeLabel('name').'（20字以内）'])
            ->label('<span class="c_red" >*</span>'.$model->getAttributeLabel('name').'：');
//                        ->label('<span class="c_red">*</span>'.$model->getAttributeLabel('name').'  ：');
        ?>
        <?php if(Yii::$app->params['ca'] != "grouping_update"):?>
        <div class="form-group field-grouping-group_leader">
            <label class="name-title-lable" for="grouping-group_leader">组长：</label><span class="c_red"></span>
            <div class="addinput-list">
                <select id="grouping-group_leader" class="form-control" name="Grouping[group_leader]" placeholder="组长">
                    <option value="">请选择</option>
                    <?php foreach ($technicianArr as $key=>$val):?>
                        <option value="<?php echo $key;?>"><?php echo $val;?></option>
                    <?php endforeach;?>
                </select>
                <p class="help-block help-block-error"></p>
                <div class="hover-right-hint">
                    <div class="hover-right-hintbox">
                        <i class="lingdang"></i>
                        <p>每个组仅有一个组长。组长可管理组下所有技师的工单，包括指派技师、改派技师、修改服务时间、查看组内全部工单。</p>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-left: 35px;">
            <span style="color: red" id="check"></span>
        </div>
        <?php endif;?>
        <?php echo $form->field($model, 'remark')
            ->textarea(['placeholder'=>$model->getAttributeLabel('remark').'（20字以内）'])
            ->label($model->getAttributeLabel('remark').'：');
        ?>
        <div class="form-group">
            <div class="addinput-list">
                <button type="button" class="btn bg-f7 mgr20 " style="color:#333333;" onclick="javascript:document.location.replace('/grouping/index')">取消</button>
                <button type="submit" class="btn btn-orange " style="color:#fff;">保存</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</section>
<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    }
    $("#grouping-group_leader").change(function() {
        var tec_id = $(this).val();
        var group_id = <?php echo isset($id) ? $id : 0;?>;
        $.get('/grouping/check-leader',{technician_id:tec_id,group_id:group_id},function (res) {
            if(!res.success) {
                $("#check").text(res.message);
            }
        })
    });

    <?php if(!$technicianArr):?>
    $('#grouping-group_leader').click(function () {
        $(this).next('p').css({'color':'#a94442'}).html('暂时没有未分组的成员可选!');
        $(this).blur();
    });
    <?php endif;?>
</script>
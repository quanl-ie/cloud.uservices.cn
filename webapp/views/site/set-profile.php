<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use webapp\assets\AppAsset;
use webapp\models\UserInfo;
use yii\helpers\ArrayHelper;

AppAsset::addCss($this, '/plugins/bootstrap/css/bootstrap.min.css');
$this->registerJsFile('/js/jquery-1.10.2.min.js', ['position'=>1]);

$this->title = '用户注册';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>
<link rel="stylesheet" href="/css/register.css">
<style type="text/css">
    .help-block{
        display: inline-block;
        margin-left: 10px;
        font-size: 12px;
        color: #2693ff;
    }
    textarea{
        margin-left:103px; 
    }
    .field-userinfo-company_desc{
        position: relative;
    }
    .field-userinfo-company_desc>label{
        position: absolute;
    }
    .bus_class{
        margin-top: 8px;
    }
</style>
<div class="register-title">
    <div class="box-1200 register-title-box">
        <div class="register-title-left">
            <img src="<?php echo Url::to('@web/img/logo1.png');?>">
        </div>
        <div class="register-title-right">
            <p>优服务-售后管理系统</p>
        </div>
    </div>
</div>
<div class="box-1200" style="margin-top: 100px">
    <div class="register-step">
        <p class="xiantiao"></p>
        <div class="step">
            <span><i class="register-step-i">1</i></span>
            <p>用户注册</p>
        </div>
        <div class="step"><span class="execute-span"><i class="register-step-i">2</i></span> <p class="execute-p">填写入驻信息</p></div>
        <div class="step"><span><i class="register-step-i">3</i></span> <p>等待审核</p></div>
    </div>
</div>
<div class="login-box">
   <!--  <h3 class="register-title">服务商入驻</h3>
    <div class="register-step">
        <p class="xiantiao"></p>
        <span><i class="register-step-i">1</i></span>
        <span class="execute-span"><i class="register-step-i">2</i></span>
        <span><i class="register-step-i">3</i></span>
        
    </div> -->
    <div class="login-box-body">
        <?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'form-signin',
                'id' => 'form2',
                'name' => 'form1',
                'enctype' => 'multipart/form-data'
            ]
        ]);
        ?>
        <fieldset>



                    <div class="form-group field-userinfo-user_type required">
                    <label class="control-label">企业类型</label>
                    <input type="hidden" name="UserInfo[user_type]" value="">
                    <div id="userinfo-user_type" aria-required="true">
                        <div class="radio">
                            <label class="radio-label"><input type="radio" name="UserInfo[user_type]" value="1" checked=""> <i></i>厂家直营</label>

                            <label class="radio-label"><input type="radio" name="UserInfo[user_type]" value="2"><i></i> 代理商</label>
                        </div>
                 <!--    <div class="radio">

                    </div> -->
                    </div>

                    <p class="help-block help-block-error"></p>
                    </div>

                    <?= $form->field($model, 'company')->input('text');?>

                    <?= $form->field($model, 'contact')->input('text');?>

                    <?= $form->field($model, 'mobile')->input('text');?>

                    <?= $form->field($model, 'email')->input('text');?>

                    <?= $form->field($model, 'logo')->widget('manks\FileInput', [
                        'clientOptions'=>[
                            'server' => Url::to('/../upload/upload?source=logo'),
                            'fileSingleSizeLimit'=>'2097152', //限制上传大小为2M
                            'setClass' => 'logo_class',
                        ]
                    ]) ?>

                    

                    <?= $form->field($model, 'address')->input('text');?>

                    <?= $form->field($model, 'company_desc')->textarea(['rows'=>5,'rols'=>'30']);?>

                    <?= $form->field($model, 'class_id')->dropDownList(ArrayHelper::map($data['goodsClass'],'id','title'),['prompt'=>'请选择']) ?>
                    
                    <?= $form->field($model, 'business_licence')->widget('manks\FileInput', [
                        'clientOptions'=>[
                            'server' => Url::to('/../upload/upload?source=business_licence'),
                            'fileSingleSizeLimit'=>'2097152', //限制上传大小为2M
                            'setClass' =>'bus_class',
                        ]
                    ]) ?>

        </fieldset>
        <fieldset>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">下一步</button>
            </div>
        </fieldset>
        <?php ActiveForm::end() ?>
    </div>
</div>
<style>
.has-feedback .form-control-feedback{
    top:-2px !important;
}
</style>
<script type="text/javascript"></script>
<script type="text/javascript">
$("#get_verified_code").click(function(){
    var mobile = $("#registerform-mobile").val();
        if(!(/^1[3456789]\d{9}$/).test(mobile))
    {
        alert("手机号格式不正确");
        return false;
    }else{
        $.ajax({
            type:'POST',
            url:'/site/get-verified-code',
            data:{mobile:mobile},
            success:function(msg){
                alert(msg);
            /*    if（msg == '-1'）{
                    $("#registerform-verified_code").val("获取验证码失败");
                }*/
            }
        });   
    }

})

$('.delImage').on('click',function () {
    console.log(1);
    $(this).parent('.input-group').css('display','none');
})
$('.bus_class button').on('click',function() {
   $('.bus_class').css('display','block');
})
$('.logo_class button').on('click',function(){
    $(".logo_class").css('display','block');
})
</script>
<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use webapp\assets\AppAsset;
use yii\helpers\Url;

AppAsset::addCss($this, '/plugins/bootstrap/css/bootstrap.min.css');
$this->registerJsFile('/js/jquery-1.10.2.min.js', ['position'=>1]);

$this->title = '用户登录';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback icon2-shoujihao' style='color:#999'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback icon2-suo1' style='color:#999'></span>"
];
?>
<!-- <div class="login-logo">
    <div class="box-1200">
        <img src="/img/logo.png">
    </div>
</div> -->
<!-- <link rel="stylesheet" href="/css/register.css"> -->
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time();?>">
<div class="register-title">
    <div class="box-1200 register-title-box">
        <div class="register-title-left">
            <img src="<?php echo Url::to('@web/img/logo2.png');?>">
        </div>
        <div class="register-title-right">
            <p>优服务-售后管理系统</p>
        </div>
    </div>
</div>
<div class="login-boss">
    <div class="box-1200" style="position: relative;">
        <div class="login-box" style="margin:7% 0;float: right;padding:20px;background:#ffffff;width:330px;">
            <div class="login-box-body">
                <div class="login-title">
                    <div class="login-xt">

                    </div>
                    <h5>用户登录</h5>
                </div>
                <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

                <?= $form
                    ->field($model, 'username', $fieldOptions1)
                    ->label(false)
                    ->textInput(['placeholder' => $model->getAttributeLabel('username'),'maxlength' => 11]) ?>
                    <select id="department_id" class="form-control department_id" name="LoginForm[department_id]" style="height: 45px;margin-bottom: 20px;display: none;">
                        <option value=" 1">sfassfdaf</option>
                        <option value=" 1">sfassfdaf</option>
                    </select>
                <?= $form
                    ->field($model, 'password', $fieldOptions2)
                    ->label(false)
                    ->textInput(['placeholder' => $model->getAttributeLabel('password') ,'onfocus'=>"this.type='password'",'maxlength' => 20]) ?>

                <?= $form->field($model, 'verifyCode')->label(false)->widget(Captcha::className(), [
                    'options'=>['placeholder'=>'验证码','class'=>'form-control'],
                    'captchaAction' => 'site/captcha',
                    'imageOptions'=>['style'=>'cursor: pointer;margin-top:-5px'],
                    'template' => '<div class="row"><div class="col-lg-6">{input}</div><div class="col-lg-6">{image}</div></div>',
                ]) ?>

                <div class="row">
                    <div class="col-xs-6">
                        <?= $form->field($model, 'rememberMe')->checkbox(['style'=>'opacity:100;width:inherit;display: block;left: 23px;top: 1px;']) ?>
                       <!--  <a href="/user/forget-pwd">忘记密码</a>
                        <a style="margin-left:30px;" href="/site/register">注册</a> -->
                    </div>
                    <div class="col-xs-6" style="height: 45px;line-height: 45px;text-align: right;">
                        <a href="/user/forget-pwd" style="color: #2693ff">忘记密码？</a>
                    </div>

                </div>
                  <div class="col-xs-12">
                        <?= Html::submitButton('登录', ['class' => 'btn btn-primary btn-block btn-flat', 'id'=>'ok-btn', 'name' => 'login-button']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="login-zi">
            <img src="/img/login-zi.png">
        </div>
    </div>
</div>
<style>
body{
    font-family: "微软雅黑";
}
.register-title{
    width: 100%;
    padding: 15px 0 10px 0;
    border-bottom: 1px solid #e8e8e8;
}
.register-title-box>div{
    display: inline-block;
    height: 43px;
    overflow: hidden;
}
.register-title-left>img{
    height: 43px;
}
.register-title-right>p{
    margin-left: 10px;
    line-height: 43px;
    font-size: 28px;
    font-weight: 500;
}
.login-logo{
    height: 100px;
    line-height: 100px;
    overflow: hidden;
}
.login-boss{
    width: 100%;
    height: 550px;
    background: url('/img/login-bn.jpg');
}
.box-1200{
    width: 1200px;
    /*height: 500px;*/
    margin:0 auto;

}
.has-feedback .form-control-feedback{
	top:-2px !important;
}
.login-title{
    width: 100%;
    position: relative;
    text-align: center;
    height: 50px
}
.login-zi{
    position: absolute;
    top: 220px;

    /*height: 60%;*/
    width: 60%;
    overflow: height;
}
.login-zi>img{
    width: 100%;
}
.login-xt{
    /*position: absolute;*/
    /*top: 10px;
    left: 0;*/
    display: inline-block;
    width: 100%;
    height: 1px;
    /*content: "";*/
    border-bottom: 1px solid #e8e8f8;
}
.login-title>h5{
    width: 100px;
    display: inline-block;
    padding:0 10px;background: #fff;
    position: absolute;
    top: -5px;
    font-size: 15px;
    left: 95px;
}
.form-group{
    margin-bottom: 20px;
}
.form-group>input{
    height: 45px;
}
#loginform-verifycode{
    height: 45px;
}
.has-feedback .form-control-feedback {
    line-height: 50px;color: #2693ff;
}
.btn-primary{
    background: #2693ff;
    border:1px solid #2693ff;
    height: 45px;
    /*box-shadow: 0 0 5px #ccc;*/
}
.btn-primary:hover{
    background: #2693ff;
    border:1px solid #2693ff;
}
.help-block-error{color:red !important;}
</style>
<script type="text/javascript">
    //登陆账号失去焦点后查询有多少个账号
    $("#loginform-username").blur(checkDep);
    $('#loginform-password').focus(function() {
        $(".department_id").is(":hidden")?checkDep():"";
    });
    //登陆账号聚焦后隐藏提示
    $("#loginform-username").blur(function(){
        $(".help-block-error").hide();
    } );
    function checkDep() {
        var mobile = $("#loginform-username").val();
        if(mobile != '' ){
            $.post("/site/check-dep", { "mobile": mobile}, function (data) {
                if(data){
                    $(".department_id").show();
                    $("#department_id").html(data);
                }else {
                    $(".department_id").hide();
                    $("#department_id").html('');
                }
            });
        }
    }
    //提交验证
    $("#ok-btn").click(function() {
        var department_id = $("#department_id").val();
        if(department_id != null && department_id == ''){
            alert('请选择需要登陆的经销商系统');
            return false;
        }
    })
</script>
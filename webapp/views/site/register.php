<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use webapp\assets\AppAsset;
use yii\helpers\Url;


AppAsset::addCss($this, '/plugins/bootstrap/css/bootstrap.min.css');
$this->registerJsFile('/js/jquery-1.10.2.min.js', ['position' => 1]);
$this->registerJsFile('/js/layer/layer.js', ['position' => 1]);
$this->registerJsFile('/js/myfunc.js', ['position' => 1]);

$this->title = '用户注册';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>
<link rel="stylesheet" href="/js/vue/static/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/register.css">
<link rel="stylesheet" href="/css/register.password.css">
<style type="text/css">
    .remind_message {
        font-size: 14px;
        margin-left: 108px;
        font-family: "微软雅黑";
    }

    .has-feedback .form-control-feedback {
        top: -2px !important;
    }

    .help-block, .help-block-error {
        margin-left: 108px;
        color: red;
    }

</style>
<div class="register-title">
    <div class="box-1200 register-title-box">
        <div class="register-title-left">
            <img src="<?php echo Url::to('@web/img/logo2.png'); ?>">
        </div>
        <div class="register-title-right">
            <p>优服务-售后管理系统</p>
        </div>
    </div>
</div>
<div class="box-1200" style="margin-top: 50px">
    <div class="register-step">
        <p class="xiantiao"></p>
        <!--<div class="step">-->
        <!--<span class=" execute-span"><i class="register-step-i">1</i></span>-->
        <!--<p  class="execute-p">用户注册</p>-->
        <!--</div>-->
        <!--        <div class="step"><span><i class="register-step-i">2</i></span> <p>填写入驻信息</p></div>
                <div class="step"><span><i class="register-step-i">3</i></span> <p>等待审核</p></div>-->
    </div>
</div>
<div style="display:none;">
    <input type="text"/>
    <input type="password"/>
</div>

<div class="login-box" style=";">
    <div class="login-box-body">
        <form id="register-form" action="/site/register" method="post">
            <input type="hidden" name="_csrf"
                   value="iBF13BeBA6uKmD3oiKnqoD1cIii4qscMLAvePn6APq78QSC-JblX6evQD4z8n6DaDipPXPX9gVpUYpxqBskHwQ==">
            <div class="form-group ipt-group field-registerform-company required">
                <label class="control-label" for="registerform-company">企业名称<span class="">:</span></label>
                <input type="text" id="registerform-company" class="form-control ipt" name="RegisterForm[company]"
                       maxlength="20" value="<?php echo $model->company ?>" placeholder="请输入公司全称"
                       aria-required="true">
                <span class="ipt-explain"></span>
            </div>
            <div class="form-group ipt-group field-registerform-mobile required">
                <label class="control-label" for="registerform-mobile">手机号<span class="">:</span></label>
                <input type="text" id="registerform-mobile" class="form-control ipt" name="RegisterForm[mobile]"
                       placeholder="请输入登录手机号" value="<?php echo $model->mobile ?>" aria-required="true">
                <span class="ipt-explain"></span>
            </div>

            <div class="form-group ipt-group field-registerform-verified_code required <?php
            $error = $model->getErrors("verified_code");
            if ($error) {
                echo "ipt-error";
            } else {
                echo "";
            }
            ?>">
                <label class="control-label" for="registerform-verified_code">验证码<span class="">:</span></label>
                <div class="ipt-wrapper">
                    <input type="text" id="registerform-verified_code" class="form-control ipt"
                           name="RegisterForm[verified_code]" placeholder="请输入验证码" aria-required="true">
                    <input type="button" id="get_verified_code" class="btn-yzm" value="获取验证码"
                           style="width: 110px;">
                </div>
                <span class="ipt-explain">
                    <?php
                    $error = $model->getErrors("verified_code");
                    if ($error) {
                        echo "<i class=\"fa fa-fw fa-times-circle\"></i>" . $error[0];
                    }
                    ?>
                </span>
            </div>
            <div class="form-group ipt-group field-registerform-password required">
                <label class="control-label" for="registerform-password">密码<span class="">:</span></label>
                <input type="password" id="registerform-password" class="form-control ipt" name="RegisterForm[password]"
                       maxlength="18" placeholder="请输入登录密码（6-18位数字与字母组合）" aria-required="true">
                <span class="ipt-explain"></span>
            </div>
            <div class="form-group ipt-group field-registerform-confirm_password required">
                <label class="control-label" for="registerform-confirm_password">确认密码<span class="">:</span></label>
                <input type="password" id="registerform-confirm_password" class="form-control ipt"
                       name="RegisterForm[confirm_password]" maxlength="18" placeholder="请再次输入密码" aria-required="true">
                <span class="ipt-explain"></span>
            </div>
            <div class="row">
                <div class="btn-register">
                    <button type="submit" id="register_submit" class="btn btn-primary btn-block btn-flat"
                            name="resister-button">注 册
                    </button>
                </div>
            </div>
            <input type="hidden" name="resister-button">
        </form>
    </div>
</div>


<script type="text/javascript">
    ;(function ( window, $, jQuery, UDF ) {
        var REG_BLANK = /^\s*$/;
        var isFunction = jQuery.isFunction;
        var isDefined = function isDefined( d ) {return d !== null && d !== UDF};
        var isArray = function isArray( a ) {return Array.isArray( a );};
        var isString = function isString( s ) {return typeof s === "string";};
        var isBlank = function isBlank( s ) {return REG_BLANK.test( s );};
        var registerForm = jQuery( "#register-form" );
        // 初始化操作。
        var $$proving = (function () {
            var inputs = registerForm.find( ".ipt:input" );
            var inputArray = [];
            var scopeMap = {};
            var collectPromise = jQuery.Callbacks();
            var globalApi = {};
            
            function noopProve( _, check ) {
                check( true );
            }
            
            function noop() {}
            
            function createBind( bind ) {
                return isFunction( bind )
                    ? bind
                    : isString( bind )
                        ? function ( input, api ) { input.on( bind, api.prove ) }
                        : noop;
            }
            
            function isInvalidPromise( promise ) {
                return !promise || promise.state() !== "pending";
            }
            
            function createScope( input, option ) {
                var warp = input.parents( ".ipt-group" );
                var explain = warp.find( ".ipt-explain" );
                var bind = createBind( option.bind );
                var proveFn = isFunction( option.prove ) ? option.prove : noopProve;
                var scopeApi = Object.create( globalApi );
                var promise;
                
                function error( message ) {
                    message = isDefined( message ) ? message : "";
                    warp.removeClass( "ipt-done" ).addClass( "ipt-error" );
                    explain.empty().append( "<i class=\"fa fa-fw fa-times-circle\"></i>" + (message) );
                    return message;
                }
                
                function success() {
                    warp.removeClass( "ipt-error" ).addClass( "ipt-done" );
                    explain.empty().append( "<i class=\"fa fa-fw fa-check-circle\"></i>" );
                }
                
                function prove( list ) {
                    var deferred;
                    if ( isInvalidPromise( promise ) ) {
                        deferred = jQuery.Deferred();
                        promise = deferred.promise();
                        proveFn( input.val(), function ( flag, message ) {
                            message = typeof flag !== "boolean" ? flag : message;
                            flag = flag === true;
                            flag ? deferred.resolve() : deferred.reject( message );
                            return promise;
                        }, scopeApi );
                        promise.done( success ).fail( error );
                    }
                    if ( isArray( list ) ) list.push( promise );
                    return promise;
                }
                
                scopeApi.prove = prove;
                scopeApi.value = function () {
                    return input.val();
                };
                scopeApi.rejected = function ( message ) {
                    promise = jQuery.Deferred( function ( d ) {
                        d.reject( error( message ) );
                    } );
                };
                bind( input, scopeApi );
                collectPromise.add( prove );
                return scopeApi;
            }
            
            // 收集需要校验的元素
            function collect( options ) {
                var inputMap = {};
                inputs.each( function () {
                    var getJquery, input = $( this ),
                        name = input.prop( "name" ),
                        option = options[name];
                    if ( isDefined( name ) && option ) {
                        if ( !inputMap[name] ) {
                            getJquery = inputMap[name] = jQuery();
                            inputArray.push( {
                                name   : name,
                                input  : getJquery,
                                option : option
                            } );
                        }
                        getJquery.push( this );
                    }
                } );
            }
            
            // 初始化绑定并创建验证作用域
            function bindProve() {
                for ( var name, option, i = 0, l = inputArray.length; i < l; i++ ) {
                    option = inputArray[i];
                    name = option.name;
                    scopeMap[name] = createScope( option.input, option.option, name );
                }
            }
            
            function assert( name ) {
                if ( !scopeMap[name] ) throw new Error( "找不到name为" + name + "的表单元素。" );
            }
            
            // 使某个表单验证错误
            globalApi.rejected = function ( name, message ) {
                assert( name );
                (scopeMap[name]).rejected( message );
            };
            //获取value
            globalApi.getValue = function ( name ) {
                assert( name );
                return (scopeMap[name]).value();
            };
            //手动触发校验
            globalApi.proveBy = function ( name ) {
                assert( name );
                return (scopeMap[name]).prove();
            };
            
            //
            return function ( options ) {
                collect( options );
                bindProve();
                return {
                    rejected : globalApi.rejected,
                    getValue : globalApi.getValue,
                    proveBy  : globalApi.proveBy,
                    proveAll : function () {
                        var promises = [];
                        collectPromise.fire( promises );
                        return jQuery.when.apply( null, promises );
                    }
                };
            }
        }());
        var MOBILE_REG = /^[1][3-9]\d{9}$/;
        var jqGetCode = $( "#get_verified_code" );
        
        function codeState( flag ) {
            flag ? jqGetCode.removeClass( "btn-disabled" )
                : jqGetCode.addClass( "btn-disabled" );
        }
        
        function Timer() {
            jqGetCode.attr( "disabled", "disabled" );
            var index = 60;
            var call = function () {
                index--;
                if ( index <= 0 ) {
                    Timer.clear();
                } else {
                    jqGetCode.val( index + "秒后重新获取" );
                }
            };
            call();
            Timer.timer = setInterval( call, 1000 );
        }

        Timer.clear = function () {
            clearInterval( Timer.timer );
            jqGetCode.val( "获取验证码" );
            jqGetCode.removeAttr( "disabled" );
        };

        function checkPassword( value, check ) {
            var length;
            if ( !isDefined( value ) || isBlank( value ) ) {
                check( "请输入密码！" );
            } else if ( (length = value.length) < 6 || length > 18 ) {
                check( "请输入6-18位密码！" );
            } else if ( !/^(?=.*[\d])(?=.*[a-zA-Z])([a-zA-Z\d]{6,18})$/.test( value ) ) {
                check( "密码必须是数字和字母组合！" );
            }
        }

        var registeredMobile;
        // 定义表单验证规则
        var proveHandler = $$proving( {
            // 验证公司名称
            "RegisterForm[company]"          : {
                bind  : "blur",
                prove : function ( value, check ) {
                    check( isDefined( value ) && !isBlank( value ), "请输入公司名称！" );
                }
            },
            // 验证手机号
            "RegisterForm[mobile]"           : {
                bind  : "blur",
                prove : function ( value, check ) {
                    var message;
                    if ( !isDefined( value ) || isBlank( value ) ) {
                        message = "手机号不能为空。"
                    } else if ( !MOBILE_REG.test( value ) ) {
                        message = "手机号格式不正确";
                    } else if ( registeredMobile === value ) {
                        message = "该手机号已注册。";
                    } else {
                        message = true;
                    }
                    check( message ).done( function () {
                        // 手机号输入正确时，移除按钮不可点击样式
                        codeState( true )
                    } ).fail( function () {
                        // 手机号输入不正确时，添加按钮不可点击样式
                        codeState( false )
                    } );
                }
            },
            // 验证码
            "RegisterForm[verified_code]"    : {
                bind  : "blur",
                prove : function ( value, check ) {
                    check( isDefined( value ) && !isBlank( value ), "请输入手机验证码！" );
                }
            },
            // 验证密码
            "RegisterForm[password]"         : {
                bind  : "blur",
                prove : function ( value, check ) {
                    checkPassword( value, check );
                    check( true );
                }
            },
            // 验证确认密码
            "RegisterForm[confirm_password]" : {
                bind  : "blur",
                prove : function ( value, check, api ) {
                    var ps = api.getValue( "RegisterForm[password]" );
                    checkPassword( value, check );
                    check( ps === value, "两次密码输入不一致！" );
                }
            }
        } );

        var mobileName = "RegisterForm[mobile]";
        // 点击获取验证码时，先校验手机号，如果验证通过则获取。
        jqGetCode.click( function () {
            proveHandler.proveBy( mobileName ).done( function () {
                var mobile = proveHandler.getValue( mobileName );
                jQuery.ajax( {
                    type : 'POST',
                    url  : '/site/get-verified-code',
                    data : { mobile : mobile, type : 'register' }
                } ).done( function ( result ) {
                    if ( jQuery.isNumeric( result ) ) result = Number( result );
                    // 已注册
                    if ( result === -1 ) {
                        registeredMobile = mobile;
                        // 触发校验手机号
                        proveHandler.proveBy( mobileName );
                    } else {
                        registeredMobile = "";
                        Timer();
                    }
                } );
            } );
        } );
        
        // 点击注册。
        $( "#register_submit" ).click( function ( e ) {
            e.preventDefault();
            // 验证全部输入项
            proveHandler.proveAll().done( function () {
                // 提交表单
                registerForm.submit();
            } );
        } );

    }( window, jQuery, jQuery ));
</script>
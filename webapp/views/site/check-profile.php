<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use webapp\assets\AppAsset;
use yii\helpers\Url;

AppAsset::addCss($this, '/plugins/bootstrap/css/bootstrap.min.css');
$this->registerJsFile('/js/jquery-1.10.2.min.js', ['position'=>1]);

$this->title = '用户注册';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>
<link rel="stylesheet" href="/css/register.css">
<div class="register-title">
    <div class="box-1200 register-title-box">
        <div class="register-title-left">
            <img src="<?php echo Url::to('@web/img/logo1.png');?>">
        </div>
        <div class="register-title-right">
            <p>优服务-售后管理系统</p>
        </div>
    </div>
</div>
<div class="box-1200" style="margin-top: 100px">
    <div class="register-step">
        <p class="xiantiao"></p>
        <div class="step">
            <span><i class="register-step-i">1</i></span>
            <p>用户注册</p>
        </div>
        <div class="step"><span><i class="register-step-i">2</i></span> <p>填写入驻信息</p></div>
        <div class="step"><span class="execute-span"><i class="register-step-i">3</i></span> <p class="execute-p">等待审核</p></div>
    </div>
</div>

<div class="box-1200">
<div class="login-box" style="margin:30px auto;padding:20px;background:#ffffff;width:700px;border: 1px solid #ccc;text-align: center;">
     <div style="margin:0 auto;height: 300px">
        <div style="height: 65px"></div>
         <div class="check-wancheng"> </div>
         <p style="margin-top: 10px;">
             恭喜，您成功提交资料！<br>
             请等待审核！
         </p>
         <a href="/" class="check-denglu">
             登录
         </a>
     </div>
</div>
</div>
<style>
    .has-feedback .form-control-feedback{
        top:-2px !important;

    .remind_message{
        color: red;
        magin:5px;
        font-size: medium;
    }
</style>
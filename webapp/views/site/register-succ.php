<style type="text/css">
    body {
        display: table;
        width: 100%;
        height: 100%;
        position: fixed;
    }

    .page-container {
        text-align: center;
        font-size: 15px;
        display: table-cell;
        vertical-align: middle;
    }

    .orange-a {
        display: inline-block;
        padding: 8px 20px;
        border: 1px solid #2693ff;
        border-radius: 5px;
        display: inline-block;
        text-decoration: none;
        color: #fff;
        background: #2693ff;
        font-size: 14px
    }

    .page-container > div > .fa {
        color: #34bd30;
        font-size: 90px;
        width: 120px;
    }

    .page-container > div > p {
        margin: 10px 0;
        line-height: 1.5;
        font-size: 16px;
        font-weight: bold;
        color: #666
    }

    .page-container > div > p.desc {
        font-size: 12px;
        color: #b9b9b9;
        margin-bottom: 20px;
        font-weight: 100;
        letter-spacing: 1px;
    }

    span.second {
        color: #ff723f;
        font-weight: bold;
    }
</style>
<link rel="stylesheet" href="/js/vue/static/css/font-awesome.min.css">
<div class="page-container">
    <div>
        <!--    	<img src="/images/zhucechenggong.png">-->
        <i class="fa fa-3x fa-check-circle"></i>
        <p>注册成功</p>
        <p class="desc">系统将在<span class="second">3</span>秒后，自动进入登录界面</p>
        <a href="/site/login" class="orange-a">立即登录</a>
    </div>
</div>
<script type="text/javascript">
    ;(function () {
        var span = document.querySelector( "span.second" );
        var cont = 3;
        var timer = setInterval( function () {
            cont -= 1;
            span.textContent = String( cont );
            if ( cont === 0 ) {
                document.location.href = "/site/login";
                clearInterval( timer );
            }
        }, 1000 );
    }());
</script>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$this->title = '预约下次上门';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">预约下次上门</span>
</div>
<!--引入CSS-->
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/webuploader/webuploader.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jquery.autocomplete.css');?>">
<!--引入JS-->
<?=Html::jsFile('@web/webuploader/webuploader.js')?>
<?=Html::jsFile('@web/js/bootstrap-select.js')?>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default" >
                <div class="panel-body"><?php
                    $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'form-horizontal',
                            'id' => 'form2',
                            'name' => 'form1',
                            'enctype' => 'multipart/form-data',
                        ],
                        'fieldConfig' => [
                            'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-lg-7 col-md-7 col-sm-7 col-xs-7 fuzzy-query\">{input}{error}</div>",
                            'labelOptions' => ['class' => 'name-title-lable'],
                        ]
                    ]);
                    ?>
                    <input type="text" name="order_no" value="<?php echo isset($data['order_no'])?$data['order_no']:'';?>" id="work_no" hidden>
                    <!--<div class="information-list" style="margin-top: 5px">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i"></i> 品牌：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-7 col-md-10 col-sm-10 col-xs-10  addinput-list">
                            <span class="wenzi"><?php /*echo isset($data['brand_name'])?$data['brand_name']:''*/?></span>
                        </div>
                        <div class="col-md-offset-2 col-md-6 misplacement" style="display:none" id="account_msg">
                        </div>
                    </div>
                    <div class="information-list" style="margin-top: 5px">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i"></i> 产品类目：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-7 col-md-10 col-sm-10 col-xs-10  addinput-list">
                             <span class="wenzi"><?php /*echo isset($data['class_name'])?$data['class_name']:''*/?></span>
                        </div>
                        <div class="col-md-offset-2 col-md-6 misplacement" style="display:none" id="account_msg">
                        </div>
                    </div>-->
                    <div class="information-list" style="margin-top: 5px">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i"></i> 服务类型：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-7 col-md-10 col-sm-10 col-xs-10  addinput-list">
                             <span class="wenzi"><?php echo isset($data['word_type_desc'])?$data['word_type_desc']:''?></span>
                        </div>
                        <div class="col-md-offset-2 col-md-6 misplacement" style="display:none" id="account_msg">
                        </div>
                    </div>
                    <div class="information-list" style="margin-top: 10px">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 服务流程：
                        </label>
                        <span class="c_red"></span>
                        <div class="addinput-list col-lg-4 col-md-4">
                            <select name="work_stage" id="work_stage" class="form-control" datatype="*" sucmsg=" " nullmsg="请选择品牌">
                                <option value="">请选择服务流程</option>
                                <?php if($data['service_flow']){?>
                                    <?php foreach($data['service_flow'] as $k=>$v){
                                        echo '<option value="'.$k.'">'.$v.'</option>';
                                    }?>
                                <?php }?>
                            </select>
                            <p class="misplacement" style="display:none" id="work_stage_msg"></p>
                        </div>
                    </div>

                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 服务时间：
                        </label>
                        <div  class="col-lg-7 col-md-10 col-sm-10 col-xs-10  addinput-list">
                            <div class="radio-li-boss">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 radio-li worktype">
                                    <!--日历选择框-->
                                    <input type="text"  class="form-control  planTime" id="selectDate" name="selectDate" value=""  placeholder="请选择日期" readonly>
                                    <i class="icon2-rili riqi1" id="rili" style="right: 17px;"></i>
                                    <p class=" misplacement" id='plan_time_msg' style="line-height: 1;bottom: -25px;">错误提示</p>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 radio-li worktype">
                                    <!--类型选择框-->
                                    <select name="plan_time_type" id="plan_time_type" class="plan_time_type form-control">
                                        <option value="0">请选择时间</option>
                                        <?php foreach($setTimeType as $k=>$v) :?>
                                            <option value="<?=$k?>"><?=$v?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <p class=" misplacement" id='plan_time_type11' style="line-height: 1;bottom: -25px;">错误提示</p>

                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 radio-li worktype">
                                    <!--选择具体时间时才显示setTime这个框-->
                                    <div id="show_setTime" style="display: none"></div>
                                </div>
                                 <div  class="city23_fix"></div>
                            </div>

                            
                        </div>
                    </div>

                    <div class="information-list">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 下次上门原因：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-7 col-md-10 col-sm-10 col-xs-10  addinput-list">
                            <ul class="radio-li-boss">
                                <li  class="col-lg-2 col-md-2 col-sm-2 col-xs-2 radio-li worktype reason" data-id="1">
                                    <span class="radio-li-ct" id="1">技师原因</span>
                                </li>
                                <li  class="col-lg-2 col-md-2 col-sm-2 col-xs-2 radio-li worktype reason" data-id="2">
                                    <span class="radio-li-ct" id="2">客户需求</span>
                                </li>
                                <li  class="col-lg-2 col-md-2 col-sm-2 col-xs-2 radio-li worktype reason" data-id="3">
                                    <span class="radio-li-ct" id="3">其他</span>
                                </li>
                            </ul>
                            <input type="hidden" name="reason" value="" class="determined-val" id="reason">
                            <p class="misplacement" style="display:none;bottom: -4px;" id="reason_msg"></p>
                        </div>
                       
                        <div class="information-list reason_detail" style="display:none;margin-top:15px;">
                            <label class="name-title-lable" for="workorder-account_id">
                            </label>
                            <div class="col-lg-7 col-md-10 col-sm-10 col-xs-10  addinput-list">
                                <textarea class="ramark-box" id="remarks" maxlength="200" placeholder="请填写原因" name="reason_detail" id="reason_detail"></textarea>
                                <p class="xianzhi"><span  id='word'>0</span>/200</p>
                            </div>
                        </div>
                    </div>
                    <div  class="information-list">
                        <div class="col-lg-7 col-md-10 col-sm-10 col-xs-10  addinput-list">
                            <a href="/work/in-service"><button type="button"  class="btn bg-f7">取消</button></a>
                            <input type="button" id="btnSubmit" class="btn btn-orange mgr20 "  value="提交"  />
                        </div>    
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- 时间插件/ -->
<?=Html::jsFile('@web/js/WdatePicker/WdatePicker.js')?>
<?=Html::jsFile('@web/js/WdatePicker/time.js')?>
<script type="text/javascript" src="/js/jquery-migrate-1.1.0.js"></script>
<script src="/js/jquery.autocomplete.js" type="text/javascript"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>

<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    }

    //日历插件调用
    var myDate = new Date();
    var myMonth = myDate.getMonth()<10?"0"+(myDate.getMonth()+1):myDate.getMonth();
    var myDay = myDate.getDate()<9?'0'+myDate.getDate():myDate.getDate();
    var minDate = myDate.getFullYear()+'-'+myMonth+'-'+myDay;

    $('#selectDate').on('click',function() {
        jeDate('#selectDate', {
            trigger:false,
            isToday:false,
            format: 'YYYY-MM-DD',
            maxDate: '2099-06-16 23:59:59',
            donefun:function(){
                $("#plan_time_type").attr('disabled',false);
                var d = new Date();
                var selectDate = new Date($('#selectDate').val());
                if(selectDate.getDate() == d.getDate())
                {
                    $('.plan_time_type').find('option').each(function(){
                        if(d.getHours() >=12 && $(this).val() == 2){
                            $(this).attr('disabled','disabled');
                            $(this).css('background','#f8f8f8');
                        }
                        if(d.getHours() >=18 && $(this).val() == 3){
                            $(this).attr('disabled','disabled');
                            $(this).css('background','#f8f8f8');
                        }
                     });
                }
                else 
                {
                    $('.plan_time_type').find('option').each(function(){
                        $(this).removeAttr('disabled');
                        $(this).removeAttr('style');
                    });
                }
            }
        })
        $(".plan_time_type").val(0);
        $(".plan_time_type").attr('selected');
        $("#show_setTime").html('');
    })

    //服务时间类型为5时候，显示具体时间下拉框
    $("#plan_time_type").click(function(){
        var id = $(this).val();
        var selectDate = $("#selectDate").val();
        if(selectDate==''){
            $("#plan_time_msg").html('请选择日期');
            $("#plan_time_msg").css('display','block');
            $(this).attr('disabled',true);
            return false;
        }else{
            $("#plan_time_msg").hide();
        }
        if(id==5){
            $.ajax({
                url:'/order/set-time',
                type:'POST',
                data:{time_type:id,date:selectDate},
                success:function (msg) {
                    $("#show_setTime").html(msg);
                }
            })
            $("#show_setTime").css('display','block');
        }else{
            $("#show_setTime").css('display','none');
        }
    })


    //单项选择
    $('.radio-li').on('click',function() {
         $('.radio-li').find('span').css({
            'border-color':'#ccc',
            'background':'#fff',
            'color':'#333'
        });
        $(this).find('span').css({
            'border-color':'#2693ff',
            'background':'#2693ff',
            'color':'#fff'
        });
       
        $(this).siblings('li').find('span').css('border-color','#ccc');
        $(this).parent('ul').next('input').val($(this).find('span').attr('id'));
    });
    //下次上门原因，显示输入区域
    $('.reason').on('click',function() {
        var $reason_id = $(this).attr('data-id');
        //alert($reason_id);return false;
        if($reason_id == 3){
            $('.reason_detail').show();
            //$("#fee_scale_note2").hide();
        }else {
            $('.reason_detail').hide();
        }
        /*//选择自定义收费标准时隐藏下拉框
        if($scale_id == 2){
            $('.merchant_select').hide();
            var merchantId = $("#manufactor_id").attr('data-id');  //获取自家id
            check_fee_scale(merchantId,2);
            //alert(merchantId);
        }*/
    });
    /**********************   提交信息验证   ************************/
    $("#btnSubmit").click(function(e){
        var flag = true;
        //验证
        var work_stage              = $("#work_stage").val();//服务流程
        var select_date             = $("#selectDate").val();//选择日期
        var plan_time_type          = $(".plan_time_type").val();//服务时间类型
        var set_time                = $("#setTime").val();  //选择具体时间
        var reason                  = $("#reason").val();  //原因

        //验证服务流程
        if(work_stage == ''){
            flag = false;
            $("#work_stage_msg").text("请选择服务流程");
            $("#work_stage_msg").show();
            return false;
        }
        
        //验证时间
        if(select_date==null || select_date==undefined || select_date==''){
            flag = false;
            $("#plan_time_msg").text("请选择日期");
            $("#plan_time_msg").show();
            return false;
        }
    
        //验证时间
        if(plan_time_type==0 || plan_time_type==undefined || plan_time_type==''){
            flag = false;
            $("#plan_time_type11").text("请选择服务时间类型");
            $("#plan_time_type11").show();
            return false;
        }
    
        //验证时间
        if(plan_time_type==5&&(set_time==null || set_time==undefined || set_time=='')){
            flag = false;
            $("#setTime").text("请选择具体的服务时间");
            $("#plan_time_msg").show();
            return false;
        }
        
        //验证原因
        if(reason == ''){
            flag = false;
            $("#reason_msg").text("请选择原因");
            $("#reason_msg").show();
            return false;
        }
        //询问框
        window.submitFlag = true;
        if(flag===true)
        {
            confirm('确认预约下次上门吗?',function(){
                if(window.submitFlag == true)
                {
                    window.submitFlag = false;
                    $('#form2').submit();
                }
            },1);
        }

    });
    /*function checkAll() {

        return flag;
    }*/
    // 字数限制
    $("#remarks").keyup(function(){
        if($("#remarks").val().length > 200){
            $("#remarks").val( $("#remarks").val().substring(0,200) );
        }
        $("#word").text( $("#remarks").val().length ) ;
    });
    //获取焦点提示框消失
    $("#work_stage").change(function() {
        $("#work_stage_msg").text("");
        $("#work_stage_msg").hide();
    })
    $(".reason").click(function() {
        $("#reason_msg").text("");
        $("#reason_msg").hide();
    });
    $("#plan_time").focus(function() {
        $("#plan_time_msg").text("");
        $("#plan_time_msg").hide();
    });
    $("#selectDate").click(function() {
        $("#plan_time_msg").text("");
        $("#plan_time_msg").hide();
    })
    $("#plan_time_type").change(function() {
        $("#plan_time_msg").text("");
        $("#plan_time_msg").hide();
    })
    $("#set_time").change(function() {
        $("#plan_time_msg").text("");
        $("#plan_time_msg").hide();
    })
    $('.worktype').click(function(){
        //$('#reason').val($(this).text());
        $(this).parents('.addinput-list').find('.misplacement').hide();
    });
</script>


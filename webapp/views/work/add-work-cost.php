<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$this->title = '添加收费项目';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">添加收费项目</span>
</div>
<!--引入CSS-->
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/webuploader/webuploader.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jquery.autocomplete.css');?>">
<!--引入JS-->
<?=Html::jsFile('@web/webuploader/webuploader.js')?>
<?=Html::jsFile('@web/js/bootstrap-select.js')?>
<?=Html::jsFile('@web/webuploader/init.js')?>
<style>
    .datexz-wap{top: 37px;}
    .form-control[disabled], .form-control[readonly]{background: #fff;border: 1px solid  #ccc;cursor:pointer;}
    .popover{max-width: 500px !important;}
    input[name=fault_img]{display:none !important;}
    .help-block,.input-group{margin-bottom: 0};
    .btn-default:hover{color: #fff;}ul{margin-bottom: 0}
    em{font-style: normal;}
    .layui-layer-dialog .layui-layer-content{text-align: center;}
    .layui-layer-btn{text-align: center;}
    .layui-layer-btn0{position: static;width: auto;}
    .layui-layer-btn a{height: auto;line-height: 2}
    .form-group{margin-left: 20px}
    .field-workcostform-voucher_img{position: relative;margin-top: 20px;height: 34px;}
    .form-horizontal .form-group{margin: 0;}
    .misplacement{bottom: -21px;}
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default" >
                <div class="panel-body"><?php
                    $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'form-horizontal',
                            'id' => 'form2',
                            'name' => 'form1',
                            'enctype' => 'multipart/form-data',
                        ],
                        'fieldConfig' => [
                            'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list\">{input}{error}</div>",
                            'labelOptions' => ['class' => 'name-title-lable'],
                        ]
                    ]);
                    ?>
                    <input type="text" name="work_no" value="<?php echo isset($data['work_no'])?$data['work_no']:'';?>" id="work_no" hidden>
                    <input type="text" name="work_id" value="<?php echo isset($data['work_info']['id'])?$data['work_info']['id']:'';?>" id="work_id" hidden>
                    <div class="information-list field-workorder-account_id">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 付费方：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                            <select name="payer_id" id="payer_id" class="form-control" datatype="*" sucmsg=" " nullmsg="请选择付费方">
                                <option value="">请选择</option>
                                <?php if($data['cost_pay_list']) foreach($data['cost_pay_list'] as $kay=>$val){?>
                                        <option value="<?php echo $val['id'];?>" <?php if(isset($data['work_info']['payer_id']) && $data['work_info']['payer_id']==$val['id']){?>selected<?php };?>><?php echo $val['type_name']; ?></option>;
                                <?php }?>
                            </select>
                            <div class="misplacement" id="payer_id_msg">
                        </div>
                        </div>
                        
                    </div>
                    <div class="information-list field-workorder-account_id">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 收费项目：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">

                            <select name="cost_id" id="cost_id" class="form-control" datatype="*" sucmsg=" " nullmsg="请选择收费项目">
                                <option value="">请选择</option>
                                <?php if($data['cost_item_list']){?>
                                    <?php foreach($data['cost_item_list'] as $k=>$v){
                                        /*echo '<option value="'.$v['id'].'">'.$v['cost_item_name'].'</option>';*/
                                        echo '<option value="'.$k.'" data-price="0" data-unit="次">'.$v.'</option>';
                                    }?>
                                <?php }?>
                            </select>
                                <input id="prod_id" name="prod_id" type="hidden" value="">
                            <div class="misplacement" id="cost_id_msg"></div>
                        </div>
                        
                    </div>
                    <div class="information-list field-workorder-account_id" id="beijian" style="display: none;">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i"></i> 备件名称：
                            <input type="hidden" name="technician_id" id="technican" value="<?=isset($data['technician_id'])?$data['technician_id']:''?>" />
                        </label>
                        <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10 addinput-list" id="prod_name" style="line-height: 35px">
                            <a href="javascript:" class="btn addBtn btn-orange" id="addBtn" style="">点击选择</a>
                            <div class="misplacement" id="product_msg"></div>
                        </div>
                        <div id="product_hide" style="display:none;"></div>
                    </div>
                    <input type="hidden" id="cost_type"  name="cost_type" value="1"/>

                    <div class="information-list field-workorder-account_id">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 单价：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list" style="position: relative;">
                            <input type="text" class="form-control" value="<?php echo isset($data['work_info']['cost_price'])?number_format($data['work_info']['cost_price'],2,'.',''):''?>" class="count" name="cost_price" id="cost_price" datatype="n" nullmsg="请输入价格" errormsg="价格错误"  placeholder="价格"  sucmsg=" " max="20"/>
                            <p style="position: absolute;right: -160px;line-height: 34px;top: 0">
                                元/<span id="unit_set"><?php echo isset($data['work_info']['unit'])?$data['work_info']['unit']:''?></span>
                        （参考价<span id="guide_price"><?php echo isset($data['work_info']['guide_price'])?$data['work_info']['guide_price']:''?></span>元）
                            </p>
                            <input type="text" name="guide_price" id="hidden_guide_price" hidden="hidden"  value="<?php echo isset($data['work_info']['guide_price'])?$data['work_info']['guide_price']:''?>" />
                             <div class="Validform_checktip Validform_wrong"></div>
                            <div class="misplacement" id="cost_price_msg"></div>
                        </div>
                    </div>
                    <div class="information-list field-workorder-account_id">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i">*</i> 数量：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                            <input type="text" class="count form-control" onkeyup="this.value=this.value.replace(/\D/g, '')" value="<?php echo isset($data['work_info']['cost_number'])?$data['work_info']['cost_number']:''?>" name="cost_number" id="cost_number" datatype="*" nullmsg="请输入数量" errormsg="数量错误"  placeholder="数量"  sucmsg=" " max="100"/>
                            <input type="hidden" id="storck_num_compare" value=""/>
                            <div class="Validform_checktip Validform_wrong"></div>
                            <div class="misplacement" id="cost_number_msg"></div>
                        </div>
                    </div>
                    <div class="information-list field-workorder-account_id" style="margin-top: 15px;margin-bottom: 15px;">
                        <label class="name-title-lable" for="workorder-account_id">
                            <i class="red-i"></i> 总价：
                        </label>
                        <span class="c_red"></span>
                        <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                            <span id="count_price" style="line-height: 34px"><?php echo isset($data['work_info']['cost_real_amount'])?number_format($data['work_info']['cost_real_amount'],2,'.',''):''?></span>元
                        </div>
                        <div class="Validform_checktip Validform_wrong"></div>
                    </div>
                    <?= $form->field($data['model'], 'voucher_img')->widget('manks\FileInput', [
                            'clientOptions'=>[
                                'server' => Url::to('../upload/upload?source=fws'),
                                'pick'=>[
                                    'multiple'=>true,
                                ],
                                'fileNumLimit'=>'5',
                                'duplicate'=>true,
                                'fileSingleSizeLimit'=>'2097152', //限制上传大小为2M
                            ],
                        ]
                    ); ?>
                    <div class="information-list field-workorder-account_id" style="margin-top: -50px;" id="workorderBox">
                        <div class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list" style="padding-bottom: 30px">
                            <div class="input-group multi-img-details" id="edatImg">
                            <?php if(isset($data['work_info']) && isset($data['work_info']['voucher_img'])&&($data['work_info']['voucher_img']) ) {?>
                                <?php foreach ($data['work_info']['voucher_img'] as $k=>$v) {?>
                                    <div class="multi-item">
                                        <img class="img-responsive img-thumbnail cus-img" src="<?php echo $v?>" alt="">
                                        <em class="close delMultiImage" title="删除这张图片">×</em>
                                    </div>
                                <?php }?>
                            <?php }?>
                            </div>
                        </div>
                    </div>
                    <div class="information-list field-workorder-account_id">
                        <label class="name-title-lable" for="workorder-account_id">
                            备注：
                        </label>
                        <div  class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                            <textarea class="ramark-box" id="comment" maxlength="200" placeholder="请填写备注" name="comment"><?php echo isset($data['work_info']['comment'])?$data['work_info']['comment']:''?></textarea>
                            <p class="xianzhi"><span  id='word'>0</span>/200</p>
                        </div>
                    </div>
                    <div class="information-list" style="padding-top: 30px">
                        <label class="name-title-lable"> </label>
                        <div  class="col-lg-6 col-md-10 col-sm-10 col-xs-10  addinput-list">
                            <div style="padding:0px;text-align: left;">
                                <a href="/order/view?order_no=<?php echo substr($data['work_no'],0,strlen($data['work_no'])-2);?>"><button type="button" style="border: 1px solid #cccccc;" class="btn bg-f7 mgr20">取消</button></a>
                                <input type="submit" id="btnSubmit" class="btn btn-orange  " style=" color:#FFF;" value="提交"  />
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- 时间插件/ -->
<?=Html::jsFile('@web/js/WdatePicker/WdatePicker.js')?>
<?=Html::jsFile('@web/js/WdatePicker/time.js')?>
<script type="text/javascript" src="/js/jquery-migrate-1.1.0.js"></script>
<script src="/js/jquery.autocomplete.js" type="text/javascript"></script>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>

<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
        $('#edatImg').find('.multi-item img').attr('src')==0?$('#workorderBox').hide:'';
        $('.delMultiImage').on('click',function() {
            console.log(11);
            console.log($('.multi-item').length);
           $('#edatImg').find('.multi-item img').attr('src')==0?$('#workorderBox').hide:'';
        })
    }
    //点击选择产品弹窗
    $(".addBtn").click(function(){
        edit_products();
    })
    function edit_products() {
        var storageSwitch    = $("#storageSwitch").val() ;  //是否开启仓储功能 1开启 0未开启
        //var technician_id = 242;   //技师id
        var technician_id = $("#technican").val();
        var edit_btn = '<a href="javascript:" class="btn" onclick="edit_products()" style="">修改</a>';
        if(storageSwitch == 1 && technician_id > 0){
            //获取当前技师的备件列表
            layer.open({
                type: 2,
                title: '选择产品',
                area: ['800px', '400px'],
                fixed: false, //不固定
                maxmin: false,
                content: '/tech-storage?technician_id='+technician_id+'&is_list=1',
                end:function(){
                    var str = $("#product_hide").html();
                    var data1 = str.split('+stock_num=');
                    stock_num = data1[1];
                    data = eval("("+data1[0]+")");
                    prod_id = data.data.id;
                    sale_price = data.data.sale_price;
                    prod_name = data.data.prod_name;
                    $("#cost_price").val(sale_price);
                    $("#storck_num_compare").val(stock_num);
                    $("#prod_id").val(prod_id);
                    $("#prod_name").html(prod_name+edit_btn);

                }
            });
        }else {
            //获取当前系统的产品列表
            layer.open({
                type: 2,
                title: '选择产品',
                area: ['800px', '400px'],
                fixed: false, //不固定
                maxmin: false,
                content: '/product/get-list',
                end:function(){
                    var str = $("#product_hide").html();
                    data = eval("("+str+")");
                    prod_id = data.data.id;
                    sale_price = data.data.sale_price;
                    prod_name = data.data.prod_name;
                    $("#cost_price").val(sale_price);
                    $("#prod_id").val(prod_id);
                    $("#prod_name").html(prod_name+edit_btn);

                    //获取价格数据进行计算
                    $(".prod_num").keyup(function(){
                        var prod_num = $(this).val();
                        if(isNaN(prod_num)){
                            alert('请输入数字');
                            return false;
                        }
                        var id = $(this).parents('tr').attr("id");
                        getTotal(id);
                    })

                    $(".purchase_price").keyup(function(){
                        var id = $(this).parents('tr').attr("id");
                        getTotal(id);
                    })
                    //计算总额
                    function getTotal(id){
                        var price      =  $("#"+id).find('.purchase_price').val();
                        var prod_num   =  $("#"+id).find('.prod_num').val();
                        var total = price*prod_num;
                        total          = new Number(total);
                        total          = total.toFixed(2);
                        $("#"+id).find('.amount').html(total);
                    }
                }
            });
        }
    }
    //选择收费项目后查出详情
    $("#cost_id").change(function(){
        var costId = $(this).val();
        var costPrice = $(this).find("option:selected").attr('data-price');
        var costUnit = $(this).find("option:selected").attr('data-unit');
        //alert(costPrice);return false;
        $("#guide_price").text(costPrice);
        $("#unit_set").text(costUnit);
        $("#hidden_guide_price").val(costPrice);
        if(costId==6){
            $("#beijian").css('display','block');
            $("#cost_type").val('2');
        }else{
            $("#beijian").css('display','none');
            $("#cost_type").val('1');
        }
        /*$.ajax({
            url:'/cost-level-item/cost-detail',
            type:'POST',
            data:{cost_item_id:costId},
            datatype:'json',
            success:function(data){
                //console.log(data.id);return false;
                var dataObj=eval("("+data+")");//转换为json对象
                //console.log(dataObj.id);return false;
                $("#guide_price").text(dataObj.price);
                $("#unit_set").text(dataObj.unit);
            }
        })*/
    });
   //单价和数量失去焦点计算总价
    $(".count").blur(function(){
        var cost_price  = $("#cost_price").val();  //单价
        var cost_number = $("#cost_number").val();  //数量
        var c_price  = '';
        if(cost_price !='' && cost_number!=''){
            cc_price = cost_price*cost_number;
            c_price = Math.floor(cc_price * 100) / 100
        }
        $("#count_price").text(c_price);
        //alert(c_price);
        //$("input").css("background-color","#D6D6FF");
    });
    /**********************   提交信息验证   ************************/
    $("#btnSubmit").click(function(e){
        var flag = checkAll();
        //询问框
        if(flag){
            confirm('确定添加吗?',function(){
                $('#form2').submit();
            },1);
        }
        return false;
    });
    function checkAll() {
        //验证
        var flag = true;
        var payer_id              = $("#payer_id").val();//付费方
        var cost_id               = $("#cost_id").val();//收费项目
        var cost_price            = $("#cost_price").val();  //单价
        var cost_number           = $("#cost_number").val();  //数量

        var storageSwitch         = $("#storageSwitch").val();//仓储开关，为1时才会有库存比对

        var stock_num             = $("#storck_num_compare").val();//库存数量比对

        var prod_id               = $("#prod_id").val();

        //编辑时候需要work_id
        var work_id              = $("#work_id").val();  //编辑当前收费项目id
        //alert(work_id);return false;
        //验证付费方
        if(payer_id == ''){
            flag = false;
            $("#payer_id_msg").text("请选择付费方");
            $("#payer_id_msg").show();
        }
        //验证收费项目
        if(cost_id == ''){
            flag = false;
            $("#cost_id_msg").text("请选择收费项目");
            $("#cost_id_msg").show();
        }
        //验证单价
        if(cost_price == ''){
            flag = false;
            $("#cost_price_msg").text("请输入单价");
            $("#cost_price_msg").show();
        }
        //验证价格
        var ret=/^(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/;
        if(cost_price != ''){
            if(!ret.test(cost_price)){
                flag = false;
                $("#cost_price_msg").text("请输入单价");
                $("#cost_price_msg").show();
            }
        }
        //验证数量
        if(cost_number == '' || isNaN(parseInt(cost_number))){
            flag = false;
            $("#cost_number_msg").text("请输入数量");
            $("#cost_number_msg").show();
        }


        if(storageSwitch==1  && parseInt(cost_number)>parseInt(stock_num)){
            flag = false;
            $("#cost_number_msg").text("库存不足");
            $("#cost_number_msg").show();
        }
        if(prod_id){
            //$("#cost_id").val(prod_id);
        }
        return flag;

    }

    //获取焦点提示框消失
    $("#payer_id").change(function() {
        $("#payer_id_msg").text("");
        $("#payer_id_msg").hide();
    })
    $("#cost_id").change(function() {
        $("#cost_id_msg").text("");
        $("#cost_id_msg").hide();
    })
    $("#cost_price").focus(function() {
        $("#cost_price_msg").text("");
        $("#cost_price_msg").hide();
    })
    $("#cost_number").focus(function() {
        $("#cost_number_msg").text("");
        $("#cost_number_msg").hide();
    })
    //提示
    // $(".tip").popover();
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message');?>');
    <?php endif;?>

</script>


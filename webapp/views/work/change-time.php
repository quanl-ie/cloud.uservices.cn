<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '修改服务时间';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jedate.css');?>?v=<?php echo time()?>">

<!DOCTYPE html>
<html>
<head>
</head>
<body>
<style type="text/css">
    .city3{width: 45%;}
    body{background:#fff;}
    .time-equable1{margin-top: 10px;position: relative;width: }
    .addinput-list{margin-right: 30px;}
</style>
<section id="main-content">
    <div style="height: 145px;">
        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
               服务日期：
            </label>
            <span class="c_red"></span>
            <div class="addinput-list">
                <input type="hidden" id="work_no" value="<?=$work_no ?>" >
                <input type="text"  class="form-control  planTime" id="selectDate" name="selectDate"  placeholder="请选择日期" readonly  onclick="selectDateDay ()">
                <i class="icon2-rili riqi1" id="rili"  onclick="selectDateDay ()"></i>
                <p class=" misplacement" id='plan_selectDate_msg'>错误提示</p>
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable" for="workorder-account_id">
               服务时间：
            </label>
            <span class="c_red"></span>
            <div class="addinput-list">
                 <select name="plan_time_type" id="plan_time_type" class="plan_time_type form-control">
                    <option value="0">请选择时间</option>
                    <?php foreach($setTimeType as $k=>$v) :?>
                        <option value="<?=$k?>"><?=$v?></option>
                    <?php endforeach;?>
                </select>
                <p class=" misplacement" id='plan_time_msg'>错误提示</p>
            </div>
        </div>
         <div class="information-list" id="show_setTime_box"  style="display: none">
            <label class="name-title-lable" for="workorder-account_id">
               具体时间：
            </label>
            <span class="c_red"></span>
            <div class="addinput-list">
                <div id="show_setTime"></div>
            </div>
        </div>
    </div>
    <hr>
    <div class="information-list" style="text-align: center;">
        <button class="btn bg-f7" id="closeIframe">取消</button>
        <a class="no-time-a btn btn-orange" onclick="saveTime()" href="javascript:">确定</a>
    </div>
  <!--   <p class="xian"></p>
    <div style="text-align: center;clear: both;">
        <button class="btn-a" id="closeIframe">取消</button>
        <button class="btn-a btn-orange" id="baocun">保存</button>
    </div> -->
</section>
<script src="/js/jquery-1.10.2.min.js"></script>
<?=Html::jsFile('@web/js/WdatePicker/WdatePicker.js')?>
<!-- <?=Html::jsFile('@web/js/WdatePicker/time.js')?> -->

<script src="/js/jedate.js"></script>

<script>
    //日历插件调用
    var myDate = new Date();
    var myMonth = myDate.getMonth()<10?"0"+(myDate.getMonth()+1):myDate.getMonth();
    var myDay = myDate.getDate()<9?'0'+myDate.getDate():myDate.getDate();
    var minDate = myDate.getFullYear()+'-'+myMonth+'-'+myDay;
    function selectDateDay (){
        jeDate('#selectDate', {
            trigger:false,
            isToday:false,
            format: 'YYYY-MM-DD',
            maxDate: '2099-06-16 23:59:59',
            donefun:function(){
                var d = new Date();
                var selectDate = new Date($('#selectDate').val());
                if(selectDate.getDate() == d.getDate())
                {
                    $('.plan_time_type').find('option').each(function(){
                        if(d.getHours() >=12 && $(this).val() == 2){
                            $(this).attr('disabled','disabled');
                            $(this).css('background','#f8f8f8');
                        }
                        if(d.getHours() >=18 && $(this).val() == 3){
                            $(this).attr('disabled','disabled');
                            $(this).css('background','#f8f8f8');
                        }
                     });
                }
                else 
                {
                    $('.plan_time_type').find('option').each(function(){
                        $(this).removeAttr('disabled');
                        $(this).removeAttr('style');
                    });
                }
            }
        })
        $(".plan_time_type").val(0);
        $(".plan_time_type").attr('selected');
        $("#show_setTime").html('');
        $("#plan_time_type").attr('disabled',false);
        $("#plan_time_msg").html('');
    }
    //服务时间类型为5时候，显示具体时间下拉框
    $("#plan_time_type").change(function(){
        var id = $(this).val();
        var selectDate = $("#selectDate").val();
        if(selectDate==''){
            $(this).val("");
            $(this).attr('disabled',true);
            $("#plan_selectDate_msg").html('请选择日期');
            $("#plan_selectDate_msg").css('display','block');
            
            return false;
        }else{
            $("#plan_selectDate_msg").css('display','none');
        }
         if(id!=0){
          $("#plan_time_msg").hide();
        }
        if(id==5){
            $.ajax({
                url:'/order/set-time',
                type:'POST',
                data:{time_type:id,date:selectDate},
                success:function (msg) {
                    $("#show_setTime").html(msg);

                }
            })
            $("#show_setTime_box").css('display','block');
        }else{
            $("#show_setTime_box").css('display','none');
        }
    })
    
    
    var index = parent.layer.getFrameIndex(window.name);

   //提交数据
   function saveTime () {
       var workNo   = $('#work_no').val();
       var select_date              = $("#selectDate").val();//选择日期
       var plan_time_type           = $(".plan_time_type").val();//服务时间类型
       var set_time                 = $("#setTime").val();  //选择具体时间

       //验证时间
       if(select_date==null || select_date==undefined || select_date==''){
           $("#plan_selectDate_msg").text("请选择日期");
           $("#plan_selectDate_msg").show();
           return false;
       }

       //验证时间
       if(plan_time_type==0 || plan_time_type==undefined || plan_time_type==''){
           $("#plan_time_msg").text("请选择服务时间类型");
           $("#plan_time_msg").show();
           return false;
       }

       //验证时间
       if(plan_time_type==5&&(set_time==null || set_time==undefined || set_time=='')){
           $("#setTime").text("请选择具体的服务时间");
           $("#plan_time_msg").show();
           return false;
       }
       
      $.ajax({
           url:'/work/change-time',
           data:{work_no:workNo,select_date:select_date,plan_time_type:plan_time_type,set_time:set_time},
           type:'POST',
           dataType:'json',
           success:function(data) {
               parent.layer.close(index);
               parent.layer.alert(data.message);
              // parent.reloadPage();
           }
       })
   }

    //关闭iframe
    $('#closeIframe').click(function(){
        parent.layer.close(index);
    });
   // 时间
   //处理所有时间
    var makeDate = new Date();
    var nowDate = new Date();
    var timeStr = '';
    //var dateStr = nowDate.getDay();
    var yearStr = nowDate.getFullYear();
    var monthsStr = nowDate.getMonth();
    var dayStr = nowDate.getDate();

    //小于10前置0
    function addPreZero(num) {
        if(num<10) {
            return '0'+num;
        }
        return num;
    }


    //填充日期
    $(function () {
       var planTime ='';//获取时间

        nowDate.setDate(nowDate.getDate());

        $('.warp-date ul li h3').each(function (i,ele) {

            var newDate = new Date();//new Date();
            newDate.setDate(nowDate.getDate()+i);

            var weekStr = ['周日','周一','周二','周三','周四','周五','周六'];
            var week = weekStr[newDate.getDay()];

            switch(i) {
                case 0:
                    week = '今天';
                    break;
                case 1:
                    week = '明天';
                    break;
                case 2:
                    week = '后天';
                    break;
            }

            var month = addPreZero(newDate.getMonth()+1);
            var day = addPreZero(newDate.getDate());

            var str = week+month+'/'+day;
            $(ele).text(str);
        });

        noSelectTime();

    });
    var isToday = true ;

    //过期时间显示为灰色
    function noSelectTime() {
        //@todo上线要22改回18
        if (nowDate.getHours()<6 || nowDate.getHours()>=18){

            $('.tdpr-wap').css("borderColor","#ccc");
            $('.tdpr-wap a').css("color","#ccc");
            $('.tdpr-wap').unbind('click');
        }

        $('.shijian-wap li').each(function (i,ele) {
            $(this).removeClass('acti');
            //设置非选中
            if(dayStr == nowDate.getDate() && parseInt($('a',ele).text().substr(0,2))<=nowDate.getHours()){
                if(parseInt($('a',ele).text().substr(0,2))==nowDate.getHours() && parseInt($('a',ele).text().substr(-2))>nowDate.getMinutes()){
                    $(ele).removeClass('no-sele');
                }else {
                    $(ele).addClass('no-sele');
                    $(ele).unbind('click');
                }

            }else {
                $(ele).removeClass('no-sele');
            }

            //设计激活
            if(makeDate.getDate() == dayStr && parseInt($('a',ele).text().substr(0,2)) == parseInt(timeStr.substr(0,2)) && parseInt($('a',ele).text().substr(-2)) == parseInt(timeStr.substr(-2))){
                if (tee && !$(this).hasClass('no-sele')){
                    $(this).addClass('acti');
                }
            }

            $(this).unbind('click');

        });
    }
    //点击确定，隐藏选项卡且将选择的时间填充
    $('.yes-time-a').click(function (eve) {
        eve.preventDefault();
        var weekStr = ['周日','周一','周二','周三','周四','周五','周六'];
        var week = weekStr[makeDate.getDay()];

        switch(makeDate.getDay()-nowDate.getDay()) {
            case 0:
                week = '今天';
                break;
            case 1:
                week = '明天';
                break;
            case 2:
                week = '后天';
                break;
        }

        //填写页面时间2017-09-29 19:17:00
        var showDate = ''+(makeDate.getFullYear())+'-'+(addPreZero(makeDate.getMonth()+1))+'-'+addPreZero(makeDate.getDate())+' '+addPreZero(makeDate.getHours())+':'+addPreZero(makeDate.getMinutes())+':00';
        planTime =showDate;
        saveTime ();

        //将时间写入li的data数据中
        $('.lwsrs span').data('time',parseInt(makeDate.getTime()/1000));
        timeSelect = true;
        location.hash = 'home';
        
    });

    var tee=true;
    //立即上门
    var dianji = 1;
    $(".tdpr-wap").click(function(){
        // console.log(isToday);
        if(!isToday){
            return ;
        }
        if(tee){
            $(this).addClass('rotate');
            $('.no-time-a').css("display",'none');
            $(".shijian-wap ul li").removeClass('acti');
            tee=false;
            //预约时间为当前时间
            makeDate = new Date();
        }else{
            tee=true;
            $(this).removeClass('rotate');
            $('.no-time-a').css("display",'block');
            //清空预约时间
            makeDate = new Date();
        }
        dayStr = makeDate.getDate();
        makeDate.setHours(makeDate.getHours()+2)
        noSelectTime()
         $('#order_make_time_type').val(1);
    });
    //点击时间，移除立即上门激活
    $(".shijian-wap ul").delegate('li:not(.no-sele)','click',function(event) {
        $('.no-time-a').css("display",'none');
        $(".tdpr-wap").removeClass('rotate');
        tee=true;
        setMakeTime($('a',this).text());
        // 。。。
        $('.shijian-wap ul li').find('img').hide();
        $(this).find('img').show();
        $('.shijian-wap ul li').removeClass('acti');
        $(this).addClass('acti');
        $('.shijian-wap ul .acti').find('img').show();
    });
    //点击 日期选项卡，移除立即上门激活
    $(".datexz-box .warp-date li").click(function(event) {
        setTimeout(function() {
           $('.shijian-wap ul li').find('img').hide();
           $('.shijian-wap ul .acti').find('img').show();
        },50)
        $(".datexz-box .warp-date li").find("h3").css('color','#444');
        $(this).find("h3").css('color',"#2693ff");
        if($(this).index() == 0){
            $(".tdpr-wap").removeClass('no-sele')
//   
            isToday = true ;
        }else {
            $(".tdpr-wap").addClass('no-sele');
            isToday = false ;
        }
        $(".tdpr-wap").removeClass('rotate');
        tee=true;
        setDate($('h3',this).text());
    });
    //点击选择时间，弹出时间选择选项卡
    $("#plan_time").on('click',function(event) {
        $(".datexz-wap").show();
    });
    $('#rili').on('click',function () {
        $(".datexz-wap").show();
    })
    // 点击关闭时间选择选项卡
    $(".rogate").click(function(event) {
        $(".datexz-wap").hide();
    });

    //设置时间
    function setMakeTime(time){

        timeStr = time;
        makeDate.setFullYear(yearStr);
        makeDate.setMonth(monthsStr);
        makeDate.setDate(dayStr);

        makeDate.setHours(parseInt(time.substr(0,2)));
        makeDate.setMinutes(parseInt(time.substr(3,2)));
        makeDate.setSeconds(0);
    }

    //设置日期
    function setDate(date) {
        var nowDateTime = new Date();
        //预约月份小于当前月，年份加1
        if (parseInt(date.substr(-5,2))-1 < nowDateTime.getMonth()) {
            yearStr = nowDateTime.getFullYear()+1;
        }
        //否者年份为当前年份
        else{
            yearStr = nowDateTime.getFullYear();
        }

        monthsStr = parseInt(date.substr(-5,2)-1);
        dayStr = parseInt(date.substr(-2));
        noSelectTime();
    }

    $("#note").keyup(function(){  
        if($("#note").val().length > 200){
            $("#note").val( $("#note").val().substring(0,200) );
        }
            $("#word").text( $("#note").val().length ) ;
    });
    // zi
    jQuery.fn.extend({
            autoHeight: function(){
                return this.each(function(){
                    var $this = jQuery(this);
                    if( !$this.attr('_initAdjustHeight') ){
                        $this.attr('_initAdjustHeight', $this.outerHeight());
                    }
                    _adjustH(this).on('input', function(){
                        _adjustH(this);
                    });
                });
                function _adjustH(elem){
                    var $obj = jQuery(elem);
                    return $obj.css({height: $obj.attr('_initAdjustHeight'), 'overflow-y': 'hidden'})
                            .height( elem.scrollHeight );
                }
            }
        });
        // 使用









</script>
</body>
</html>

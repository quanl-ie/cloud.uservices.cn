<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '待服务工单';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time()?>">
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">待服务工单
    <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="等待技师初次上门服务的工单"></span>
    </span>
</div> 
<section id="main-content">       
    <div class="panel-body">
        <form action="/work/wait-service" class="form-horizontal form-border" id="form">
            <div class="form-group jn-form-box">
                <!-- 搜索栏 -->
                <div class="col-sm-12 no-padding-left">
                    <div class="single-search">
                        <label class="search-box-lable">工单编号</label>
                        <div class="single-search-kuang1">
                            <input type="text" class="form-control" name="work_no" value="<?php echo isset($_GET['work_no'])?$_GET['work_no']:'';?>">
                        </div>
                    </div> 
                    <div class="single-search">
                        <label class="search-box-lable">客户姓名</label>
                        <div class="single-search-kuang1">
                            <input type="text" class="form-control" name="account_name" value="<?php echo isset($_GET['account_name'])?$_GET['account_name']:'';?>">
                        </div>
                    </div>
                    <div class="single-search">
                        <label class="search-box-lable">客户电话</label>
                        <div class="single-search-kuang1">
                            <input type="text" class="form-control" name="account_mobile" value="<?php echo isset($_GET['account_mobile'])?$_GET['account_mobile']:'';?>">
                        </div>
                    </div>
                    <div class="single-search">
                        <label class="search-box-lable">技师姓名</label>
                        <div class="single-search-kuang1">
                            <input type="text" class="form-control" name="tec_member" value="<?php echo isset($_GET['tec_member'])?$_GET['tec_member']:'';?>">
                        </div>
                    </div>
                    <div class="single-search">
                        <label class="search-box-lable">技师电话</label>
                        <div class="single-search-kuang1">
                            <input type="text" class="form-control" name="tec_mobile" value="<?php echo isset($_GET['tec_mobile'])?$_GET['tec_mobile']:'';?>">
                        </div>
                    </div>
                    <div class="single-search">
                        <label class="search-box-lable">服务类型</label>
                        <div class="single-search-kuang1">
                            <select class="form-control" name="work_type">
                                <option value="">请选择</option>
                                <?php foreach ($workTypeArr as $key=>$val):?>
                                    <option value="<?php echo $val['id']; ?>" <?php if(isset($_GET['work_type']) && $_GET['work_type']==$key):?>selected<?php endif; ?>><?php echo $val['title'];?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <!--<div class="single-search region-limit" style="overflow: visible;">
                        <label class=" search-box-lable">产品类目</label>
                        <div class="single-search-kuang1">
                            <input type="hidden" name="class_id" id="class_id" value="<?/*=isset($_GET['class_ids']) ? $_GET['class_ids'] : ''*/?>">
                            <input type="text" class="form-control" id="class_name" value="<?/*=isset($_GET['class_name']) ? $_GET['class_name'] : '' */?>" placeholder="请选择类目" readonly onclick="clickCategory()">
                            <div class="drop-dw-layerbox" style="display: none;">
                                <div class="ul-box-h180">
                                    <ul id="depart1" class="depart">
                                        <?php /*if (!empty($class)) : */?>
                                            <?php /*foreach ($class as $key => $val) : */?>
                                                <li>
                                                             <span>
                                                                <?php /*if (isset($val['exist']) && $val['exist'] == 1) : */?>
                                                                    <i class="icon2-shixinyou" onclick="getNextClass(this)"></i>
                                                                <?php /*endif;*/?>
                                                                 <input type="checkbox" id="a<?/*=$val['id'] */?>" value="<?/*=$val['id'] */?>" name="class_id[]" >
                                                                 <label for="a<?/*=$val['id'] */?>"><?/*=$val['class_name'] */?><i class="gou-i"></i></label>
                                                            </span>
                                                </li>
                                            <?php /*endforeach; */?>
                                        <?php /*endif; */?>
                                    </ul>
                                </div>
                                <button class="btn btn-orange" id="belongedBtn">确定</button>
                            </div>
                        </div>
                    </div>-->
                    <div class="tow-search" >
                        <label class="search-box-lable">服务时间</label>
                        <div class="single-search-kuang2">
                            <div class="half-search">
                                <input type="text" id="datetimepicker-pay-top" class="form-control" name="plan_start_time" value="<?php echo isset($_GET['plan_start_time'])?$_GET['plan_start_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})">
                                <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})"><i class="icon2-rili"></i></span>
                                <span class="zhi"> 至</span>
                            </div>
                            <div class="half-search">
                                <input type="text" id="datetimepicker-pay-end" class="form-control" name="plan_end_time" value="<?php echo isset($_GET['plan_end_time'])?$_GET['plan_end_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})" >
                                <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})"><i class="icon2-rili"></i></span>
                            </div>
                            <p class="city3_fix"></p>
                        </div>
                    </div> 
                   <div class="search-confirm">   
                         <button class="btn btn-success "><i class=" "></i> 搜索</button>
                     </div> 

                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead bgcolor="#2693ff">
                        <tr>
                            <th>工单编号</th>
                            <th>客户姓名</th>
                            <th>客户电话</th>
                            <th>技师姓名</th>
                            <th>技师电话</th>
                            <th>品牌</th>
                            <th>产品名称</th>
                            <th>服务流程</th>
                            <th>预约服务时间</th>
                            <th>服务地址</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if(isset($data['list']) && $data['list']):?>
    					<?php foreach ($data['list'] as $key=>$val):?>
                        <tr>
                            <td><a href="/work/view?work_no=<?php echo $val['work_no'];?>"><?php echo $val['work_no'];?></a></td>
                            <td><?php echo $val['account_name'];?></td>
                            <td><?php echo $val['account_mobile'];?></td>
                            <td><?php echo $val['technician_name'].$val['technician_title']; ?></td>
                            <td><?php echo $val['technician_mobile'];?></td>
                            <td><?php echo $val['brand_name']; ?></td>
                            <td><?php echo $val['prod_desc']; ?></td>
                            <td><?php echo $val['work_flow_desc'];?></td>
                            <td><?php echo $val['plan_time']; ?></td>
                            <td><a href="<?php echo $val['order_no'];?>" class="checkPosition"  ><?php echo $val['address'];?></a></td>
                            <td  class="operation">
                                <a href="/order/re-assign?order_no=<?=$val['order_no']?>&work_no=<?php echo $val['work_no']?>" >改派</a>
                                <a href="javascript:void(0);" class="changeTime" data-no="<?php echo $val['work_no'];?>">修改服务时间</a>
                                <a href="javascript:void(0);" data-href="<?php echo $val['work_no'];?>" class="startService">开始服务</a>
                                <?php if($val['is_first'] == 2): ?>
                                <a href="javascript:void(0);" data-href="<?php echo $val['work_no'];?>" class="cancelOrder">取消服务</a>
                                <?php endif;?>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        <?php else:?>
                            <tr>
                                <tr><td colspan="11" class="no-record">暂无待服务工单</td></tr>
                            </tr>
                        <?php endif;?>
                    </tbody>
                </table>
            </div>
        </form>
        <div class="col-xs-12 text-center pagination">
                <?php echo $pageHtml;?>
        </div>
    </div>  
</section>
<!-- 取消弹层 -->
<div id="cancelLayer" style="display: none">
    <div style="text-align: center;padding:20px 30px 10px">
        <p style="margin:0 0 20px 0">请选择关闭原因</p>
        <div class="select-fuji">
            <select  class="form-control cancelReason" style="width: 90%;margin-left: 5%">
                <option value="">请选择</option>
                <option value="下次服务不确定">下次服务不确定</option>
                <option value="客户不需要再次服务">客户不需要再次服务</option>
                <option value="其他原因">其他原因</option>
            </select>
        </div>
        <div style="margin-top:30px;">
            <button class="btn btn-success" id="layerSubmit"  style="padding: 4px 12px"><i class="fa fa-search"></i>确定</button>
            <button class="btn" id="layerCancel" style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>
<script src="<?php echo Url::to('/js/orderTree.js');?>"></script>
<script>
window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    <?php endif;?>

    //提示
    $(".tip").popover();
    
    //服务地址定位
    $(".checkPosition").click(function (){
        var orderNo = $(this).attr('href');
        layer.open({
          type: 2,
          title: '订单位置',
          scrollbar:false,
          area: ['893px', '600px'],
          anim: 2,
          content: ['/order/position?order_no='+orderNo, 'no'], //iframe的url，no代表不显示滚动条
        });
        return false;
    });

    //类目二级联动
    $('.first_class_id').change(function () {
        var pid = $(this).val();
        var html = '<option value="">全部二级类目</option>';
        if(pid!='' && pid > 0 ){
            $.getJSON('/common/ajax-get-class',{'pid':pid},function (json) {
                //console.log(json);
                if(json.success == true){
                    for(item in json.data.data){
                        html+='<option value="'+item+'">'+json.data.data[item]+'</option>';
                    }
                    $('.second_class_id').html(html);
                }
            })
        }
        else {
            $('.second_class_id').html(html);
        }
    });

    var layerIndex = null;
    var workNo = '';
    $(".cancelOrder").click(function () {
        layerIndex = layer.open({
            type: 1,
            title: '提示',
            area: ['300px', '220px'],
            fixed: true,
            maxmin: false,
            content: $('#cancelLayer').html()
        });
        workNo = $(this).attr('data-href');
        return false;
    });
    //修改服务时间
    $(".changeTime").click(function () {
        var workNO = $(this).attr('data-no');
        layer.open({
            type: 2,
            title: '请选择服务时间',
            area: ['400px', '300px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/work/change-time?work_no='+workNO, 'yes'],
            // end:function(){
            //     alert('修改成功');
            //     // location.reload();
            // }
        });
        return false;
    });


    //提交取消订单
    $("body").delegate("#layerSubmit","click",function(){
        var cancelReason = $(this).parents('div').find('.cancelReason').find('option:selected').val();
        if($.trim(workNo) == ''){
            alert('订单号不能为空');
            return false;
        }
        if($.trim(cancelReason) == ''){
            alert('请选择关闭原因');
            return false;
        }
        $.getJSON('/work/close',{'work_no':workNo,'cancel_reason':cancelReason},function (json) {
            if(json.success == 1){
                if(layerIndex){
                    layer.close(layerIndex);
                }
                alert('关闭成功');
                window.location.reload(true);
            }
            else {
                alert(json.message);
            }
        });
    });
    //关闭弹层
    $("body").delegate("#layerCancel","click",function(){
        if(layerIndex){
            layer.close(layerIndex);
        }
    });

    //查看实时位置
    $(".viewLocation").click(function(){
        var id = $(this).attr("id-data");
        layer.open({
            type: 2,
            title: ['技师位置', 'font-size:18px;'],
            area: ['800px', '500px'],
            fixed: false, //不固定
            maxmin: false,
            content: '/technician/view-location?id='+id,
        });
        return false;
    });

    ///开始服务
    $('.startService').click(function () {
         var orderNo = $(this).data('href');
         confirm("确定要将此订单开始服务吗？",function(obj){
             $.getJSON('/work/ajax-start-service',{work_no:orderNo},function (json) {
                 if(json.success == true){
                     alert('操作成功',function () {
                         document.location.reload(true);
                     });
                 }
                 else {
                     alert(json.message);
                 }
             });
         },this);
    });
}

function reloadPage(){
    setTimeout(function(){
        location.reload(true);
    },2000);
}
// 选择类目
// 选中内容
$('#belongedBtn').bind('click', function(event) {
    event.preventDefault();
    var id_array= [];
    var data_array=[];
    $('input[name="class_id[]"]:checked').each(function(){
        data_array.push($(this).next('label').text());
        id_array.push($(this).val());//向数组中添加元素
    });
    var idstr=id_array.join(',');//将数组元素连接起来以构建一个字符串
    var datatr = data_array.join(',');
    $('#class_name').val(datatr);
    $("#class_id").val(idstr);
    $('.drop-dw-layerbox').hide();
});
//获取下级分类
function getNextClass(el) {
    var id = $(el).next('input').val();
    var _this = $(el).parent('span');
    $.getJSON('/common/ajax-get-class',{'pid':id},function (json) {
        var ulData = '';
        if (_this.next('ul').size()==0) {
            ulData +="<ul>"
            var data = json.data;
            delete data.flag;

            for (item in data)
            {
                ulData +="<li><span>";
                data[item].exist==1?ulData+= '<i class="icon2-shixinyou"  onclick="getNextClass(this)"></i>':'';
                ulData+='<input type="checkbox" value="'+data[item].id+'" name="class_id[]" id="class'+data[item].id+'"><label for="class'+data[item].id+'">'+data[item].class_name+'<i class="gou-i"></i></label></span></li>';
            }

            ulData +="</ul>";

            _this.parent('li').append(ulData);
            $(el).removeClass('icon2-shixinyou');
            $(el).addClass('icon2-shixinxia');

        }else{
            if(
                _this.next('ul').is(':hidden')){
                $(el).removeClass('icon2-shixinyou');
                $(el).addClass('icon2-shixinxia');
                _this.next('ul').show();

            }else{
                $(el).addClass('icon2-shixinyou');
                $(el).removeClass('icon2-shixinxia');
                _this.next('ul').hide();
            }
        }
        return false;
    })
}
</script>

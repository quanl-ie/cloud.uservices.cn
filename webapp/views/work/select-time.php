<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '修改服务xcvvxv时间';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<!DOCTYPE html>
<html>
<head>
</head>
<body>
    <style type="text/css">
        a{text-decoration: none;}
    </style>
    <input type="text" name="plan_time" class="form-control  planTime" id="plan_time" value="<?php if(isset($data['plan_time'])) echo date("Y-m-d H:i:s",$data['plan_time'])?>"  placeholder="请选择服务时间" readonly style="width: 90%;margin-left: 5%;margin-top: 10px">
    <i class="icon2-rili riqi1" id="rili" style="right: 8%;top: 9px"></i>
    <!-- 时间插件 -->
    <div class="datexz-wap center fixed">
        <div class="datexz-box">
           
            <div class="warp-date">
                <ul>
                    <li class="today acti">
                        <h3>今天<br>07/10</h3>
                        <!--<p>可预约</p>-->
                    </li>
                    <li>
                        <h3>明天<br>07/11</h3>
                        <!--<p>可预约</p>-->
                    </li>
                    <li>
                        <h3>后天<br>07/12</h3>
                        <!--<p>可预约</p>-->
                    </li>
                    <li>
                        <h3>周四<br>07/13</h3>
                        <!--<p>可预约</p>-->
                    </li>
                    <li>
                        <h3>周五<br>07/14</h3>
                        <!--<p>可预约</p>-->
                    </li>
                    <li>
                        <h3>周六<br>07/15</h3>
                        <!--<p>可预约</p>-->
                    </li>
                    <li>
                        <h3>周日<br>07/16</h3>
                        <!--<p>可预约</p>-->
                    </li>
                </ul>
            </div>
            <!--<div class="tdpr-wap">
                <a href="javascript:void(0);">2小时立即上门</a>
                <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
            </div>-->
            <div class="shijian-wap">
                <ul>
                    <li class="no-sele">
                        <a href="javascript:void(0);">08:00</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li class="no-sele">
                        <a href="javascript:void(0);">08:30</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li class="no-sele">
                        <a href="javascript:void(0);">09:00</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li class="">
                        <a href="javascript:void(0);">09:30</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">10:00</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">10:30</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">11:00</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">11:30</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">12:00</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">13:00</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">13:30</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">14:00</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">14:30</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">15:00</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">15:30</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">16:00</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">16:30</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">17:00</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">17:30</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">18:00</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">18:30</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">19:00</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">19:30</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                    <li>
                        <a href="javascript:void(0);">20:00</a>
                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                    </li>
                </ul>
            </div>
           <!--  <div class="buton rediuset" style="position: relative;">
                <a class="yes-time-a" href="javascript:void(0);">确定</a>
                <a class="no-time-a" href="javascript:">确定</a>
            </div> -->
        </div>
    </div>
                    
<script src="/js/jquery-1.10.2.min.js"></script>
<?=Html::jsFile('@web/js/WdatePicker/WdatePicker.js')?>
 <!-- <?=Html::jsFile('@web/js/WdatePicker/time.js')?> -->
 <script type="text/javascript">
      // 时间
   //处理所有时间
    var makeDate = new Date();
    var nowDate = new Date();
    var timeStr = '';
    //var dateStr = nowDate.getDay();
    var yearStr = nowDate.getFullYear();
    var monthsStr = nowDate.getMonth();
    var dayStr = nowDate.getDate();

    //小于10前置0
    function addPreZero(num) {
        if(num<10) {
            return '0'+num;
        }
        return num;
    }


    //填充日期
    $(function () {
       var planTime ='';//获取时间

        nowDate.setDate(nowDate.getDate());

        $('.warp-date ul li h3').each(function (i,ele) {

            var newDate = new Date();//new Date();
            newDate.setDate(nowDate.getDate()+i);

            var weekStr = ['周日','周一','周二','周三','周四','周五','周六'];
            var week = weekStr[newDate.getDay()];

            switch(i) {
                case 0:
                    week = '今天';
                    break;
                case 1:
                    week = '明天';
                    break;
                case 2:
                    week = '后天';
                    break;
            }

            var month = addPreZero(newDate.getMonth()+1);
            var day = addPreZero(newDate.getDate());

            var str = week+month+'/'+day;
            $(ele).text(str);
        });

        noSelectTime();

    });
    var isToday = true ;

    //过期时间显示为灰色
    function noSelectTime() {
        //@todo上线要22改回18
        if (nowDate.getHours()<6 || nowDate.getHours()>=18){

            $('.tdpr-wap').css("borderColor","#ccc");
            $('.tdpr-wap a').css("color","#ccc");
            $('.tdpr-wap').unbind('click');
        }

        $('.shijian-wap li').each(function (i,ele) {
            $(this).removeClass('acti');
            //设置非选中
            if(dayStr == nowDate.getDate() && parseInt($('a',ele).text().substr(0,2))<=nowDate.getHours()+4){
                if(parseInt($('a',ele).text().substr(0,2))==nowDate.getHours()+4 && parseInt($('a',ele).text().substr(-2))>nowDate.getMinutes()){
                    $(ele).removeClass('no-sele');
                }else {
                    $(ele).addClass('no-sele');
                    $(ele).unbind('click');
                }

            }else {
                $(ele).removeClass('no-sele');
            }

            //设计激活
            if(makeDate.getDate() == dayStr && parseInt($('a',ele).text().substr(0,2)) == parseInt(timeStr.substr(0,2)) && parseInt($('a',ele).text().substr(-2)) == parseInt(timeStr.substr(-2))){
                if (tee && !$(this).hasClass('no-sele')){
                    $(this).addClass('acti');
                }
            }

            $(this).unbind('click');

        });
    }
    //点击确定，隐藏选项卡且将选择的时间填充
    // $('.yes-time-a').click(function (eve) {
    //     // liTime ();
    //     // planTime =showDate;
    //     // saveTime ();

    //     // //将时间写入li的data数据中
    //     // $('.lwsrs span').data('time',parseInt(makeDate.getTime()/1000));
    //     // timeSelect = true;
    //     // location.hash = 'home';
        
    // });

    var tee=true;
    //立即上门
    var dianji = 1;
    $(".tdpr-wap").click(function(){
        // console.log(isToday);
        if(!isToday){
            return ;
        }
        if(tee){
            $(this).addClass('rotate');
            $('.no-time-a').css("display",'none');
            $(".shijian-wap ul li").removeClass('acti');
            tee=false;
            //预约时间为当前时间
            makeDate = new Date();
        }else{
            tee=true;
            $(this).removeClass('rotate');
            $('.no-time-a').css("display",'block');
            //清空预约时间
            makeDate = new Date();
        }
        dayStr = makeDate.getDate();
        makeDate.setHours(makeDate.getHours()+2)
        noSelectTime()
         $('#order_make_time_type').val(1);
         $('#plan_time').val(333);
    });
    function liTime () {
        var weekStr = ['周日','周一','周二','周三','周四','周五','周六'];
        var week = weekStr[makeDate.getDay()];

        switch(makeDate.getDay()-nowDate.getDay()) {
            case 0:
                week = '今天';
                break;
            case 1:
                week = '明天';
                break;
            case 2:
                week = '后天';
                break;
        }

        //填写页面时间2017-09-29 19:17:00
        var showDate = ''+(makeDate.getFullYear())+'-'+(addPreZero(makeDate.getMonth()+1))+'-'+addPreZero(makeDate.getDate())+' '+addPreZero(makeDate.getHours())+':'+addPreZero(makeDate.getMinutes())+':00';
        $('#plan_time').val(showDate);
    }
    //点击时间，移除立即上门激活
    $(".shijian-wap ul").delegate('li:not(.no-sele)','click',function(event) {
        // liTime ();
        setTimeout(liTime,100);
        $('.no-time-a').css("display",'none');
        $(".tdpr-wap").removeClass('rotate');
        tee=true;
        setMakeTime($('a',this).text());
        // 。。。
        $('.shijian-wap ul li').find('img').hide();
        $(this).find('img').show();
        $('.shijian-wap ul li').removeClass('acti');
        $(this).addClass('acti');
        $('.shijian-wap ul .acti').find('img').show();
    });
    //点击 日期选项卡，移除立即上门激活
    $(".datexz-box .warp-date li").click(function(event) {
        setTimeout(function() {
           $('.shijian-wap ul li').find('img').hide();
           $('.shijian-wap ul .acti').find('img').show();
        },50)
        $(".datexz-box .warp-date li").find("h3").css('color','#444');
        $(this).find("h3").css('color',"#2693ff");
        if($(this).index() == 0){
            $(".tdpr-wap").removeClass('no-sele')
//   
            isToday = true ;
        }else {
            $(".tdpr-wap").addClass('no-sele');
            isToday = false ;
        }
        $(".tdpr-wap").removeClass('rotate');
        tee=true;
        setDate($('h3',this).text());
    });
    //点击选择时间，弹出时间选择选项卡
    $("#plan_time").on('click',function(event) {
        $(".datexz-wap").show();
    });
    $('#rili').on('click',function () {
        $(".datexz-wap").show();
    })
    // 点击关闭时间选择选项卡
    $(".rogate").click(function(event) {
        $(".datexz-wap").hide();
    });

    //设置时间
    function setMakeTime(time){

        timeStr = time;
        makeDate.setFullYear(yearStr);
        makeDate.setMonth(monthsStr);
        makeDate.setDate(dayStr);

        makeDate.setHours(parseInt(time.substr(0,2)));
        makeDate.setMinutes(parseInt(time.substr(3,2)));
        makeDate.setSeconds(0);
    }

    //设置日期
    function setDate(date) {
        var nowDateTime = new Date();
        //预约月份小于当前月，年份加1
        if (parseInt(date.substr(-5,2))-1 < nowDateTime.getMonth()) {
            yearStr = nowDateTime.getFullYear()+1;
        }
        //否者年份为当前年份
        else{
            yearStr = nowDateTime.getFullYear();
        }

        monthsStr = parseInt(date.substr(-5,2)-1);
        dayStr = parseInt(date.substr(-2));
        noSelectTime();
    }

    $("#note").keyup(function(){  
        if($("#note").val().length > 200){
            $("#note").val( $("#note").val().substring(0,200) );
        }
            $("#word").text( $("#note").val().length ) ;
    });
    // zi
    jQuery.fn.extend({
            autoHeight: function(){
                return this.each(function(){
                    var $this = jQuery(this);
                    if( !$this.attr('_initAdjustHeight') ){
                        $this.attr('_initAdjustHeight', $this.outerHeight());
                    }
                    _adjustH(this).on('input', function(){
                        _adjustH(this);
                    });
                });
                function _adjustH(elem){
                    var $obj = jQuery(elem);
                    return $obj.css({height: $obj.attr('_initAdjustHeight'), 'overflow-y': 'hidden'})
                            .height( elem.scrollHeight );
                }
            }
        });
        // 使用
 </script>
</body>
</html>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '工单回访';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time()?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time()?>">
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">工单回访
</div> 
<section id="main-content">
                 
    <div class="panel-body">
        <div style="width: 100%;" class="tab-group">
            <ul id="myTab" class="nav nav-tabs">
                <li><a href="/work/wait-visit">待回访</a></li>
                <li class="active">
                    <a href="#responseOvertime">
                        已回访
                    </a>
                </li>
            </ul>
        </div>

        <form action="/work/already-visit" class="form-horizontal form-border" id="form">
            <div class="form-group jn-form-box">
                <!-- 搜索栏 -->
                <div class="col-sm-12 no-padding-left">
                    <div class="single-search">
                        <label class="search-box-lable">工单编号</label>
                        <div class="single-search-kuang1">
                            <input type="text" class="form-control" name="work_no" value="<?php echo isset($_GET['work_no'])?$_GET['work_no']:'';?>">
                        </div>
                    </div> 
                    <div class="single-search">
                        <label class="search-box-lable">客户姓名</label>
                        <div class="single-search-kuang1">
                            <input type="text" class="form-control" name="account_name" value="<?php echo isset($_GET['account_name'])?$_GET['account_name']:'';?>">
                        </div>
                    </div>
                    <div class="single-search">
                        <label class="search-box-lable">客户电话</label>
                        <div class="single-search-kuang1">
                            <input type="text" class="form-control" name="account_mobile" value="<?php echo isset($_GET['account_mobile'])?$_GET['account_mobile']:'';?>">
                        </div>
                    </div>
                    <div class="single-search">
                        <label class="search-box-lable">技师姓名</label>
                        <div class="single-search-kuang1">
                            <input type="text" class="form-control" name="tec_member" value="<?php echo isset($_GET['tec_member'])?$_GET['tec_member']:'';?>">
                        </div>
                    </div>
                    <div class="single-search">
                        <label class="search-box-lable">技师电话</label>
                        <div class="single-search-kuang1">
                            <input type="text" class="form-control" name="tec_mobile" value="<?php echo isset($_GET['tec_mobile'])?$_GET['tec_mobile']:'';?>">
                        </div>
                    </div>
                    <div class="single-search">
                        <label class="search-box-lable">服务类型</label>
                        <div class="single-search-kuang1">
                            <select class="form-control" name="work_type">
                                <option value="">请选择</option>
                                <?php foreach ($workTypeArr as $key=>$val):?>
                                <option value="<?php echo $key; ?>" <?php if(isset($_GET['work_type']) && $_GET['work_type']==$key):?>selected<?php endif; ?>><?php echo $val;?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="tow-search">
                        <label class="search-box-lable">产品类目</label>
                        <div class="single-search-kuang2">
                            <div class="half-search">
                                <select class="form-control first_class_id" name="first_class_id">
                                    <option value="">全部一级类目</option>
                                    <?php foreach ($firstClass as $key=>$val):?>
                                        <option value="<?php echo $key; ?>" <?php if(isset($_GET['first_class_id']) && $_GET['first_class_id']==$key):?>selected<?php endif; ?>><?php echo $val;?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="half-search">
                                <select class="form-control second_class_id" name="second_class_id">
                                    <option value="">全部二级类目</option>
                                    <?php if($secondClass): ?>
                                        <?php foreach ($secondClass as $key=>$val):?>
                                            <option value="<?php echo $key; ?>" <?php if(isset($_GET['second_class_id']) && $_GET['second_class_id']==$key):?>selected<?php endif; ?>><?php echo $val;?></option>
                                        <?php endforeach;?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <p class="city3_fix"></p>
                        </div>
                    </div>
                    <div class="tow-search" >
                        <label class="search-box-lable">服务时间</label>
                        <div class="single-search-kuang2">
                            <div class="half-search">
                                <input type="text" id="datetimepicker-pay-top" class="form-control" name="plan_start_time" value="<?php echo isset($_GET['plan_start_time'])?$_GET['plan_start_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})">
                                <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-top',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datetimepicker-pay-end\')}'})"><i class="icon2-rili"></i></span>
                                <span class="zhi"> 至</span>
                            </div>
                            <div class="half-search">
                                <input type="text" id="datetimepicker-pay-end" class="form-control" name="plan_end_time" value="<?php echo isset($_GET['plan_end_time'])?$_GET['plan_end_time']:'';?>" readonly="readonly" style="cursor:text;" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})" >
                                <span class="calendar-icon" style="z-index:3;" onclick="WdatePicker({el:'datetimepicker-pay-end',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datetimepicker-pay-top\')}'})"><i class="icon2-rili"></i></span>
                            </div>
                            <p class="city3_fix"></p>
                        </div>
                    </div> 
                    <div class="search-confirm">     
                         <button class="btn btn-success "><i class=" "></i> 搜索</button>
                     </div> 

                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead bgcolor="#2693ff">
                        <tr>
                            <th>工单编号</th>
                            <th>客户姓名</th>
                            <th>客户电话</th>
                            <th>技师姓名</th>
                            <th>技师电话</th>
                            <th>服务内容</th>
                            <th>服务流程</th>
                            <th>预约服务时间</th>
                            <th>完成时间</th>
                            <th style="width: 100px">回访记录</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if(isset($data['list']) && $data['list']):?>
    					<?php foreach ($data['list'] as $key=>$val):?>
                        <tr>
                            <td><a href="/work/view?work_no=<?php echo $val['work_no'];?>"><?php echo $val['work_no'];?></a></td>
                            <td><?php echo $val['account_name'];?></td>
                            <td><?php echo $val['account_mobile'];?></td>
                            <td><?php echo $val['technician_name'].$val['technician_title']; ?></td>
                            <td><?php echo $val['technician_mobile'];?></td>
                            <td><?php echo $val['service_content'];?></td>
                            <td><?php echo $val['work_flow_desc'];?></td>
                            <td><?php echo $val['plan_time']; ?></td>
                            <td><?php echo $val['finsh_service_time']; ?></td>
                            <td><a href="<?php echo $val['work_no'];?>" class="viewVisit"  >点击查看</a></td>
                        </tr>
                        <?php endforeach;?>
                        <?php else:?>
                            <tr>
                                <tr><td colspan="10" class="no-record">暂无数据</td></tr>
                            </tr>
                        <?php endif;?>
                    </tbody>
                </table>
            </div>
        </form>
        <div class="col-xs-12 text-center  pagination">
                <?php echo $pageHtml;?>
        </div>
    </div> 
</section>
<!-- 取消弹层 -->
<div id="cancelLayer" style="display: none">
    <div style="text-align: center;padding:20px 30px 10px">
        <div id="showVisitContent" class="select-fuji">

        </div>
    </div>
</div>
<script>
window.onload = function(){
    <?php if(Yii::$app->session->hasFlash('message')):?>
    alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
    window.parent.document.location.reload();
    <?php endif;?>

    //提示
    $(".tip").popover();
    
    //服务地址定位
    $(".checkPosition").click(function (){
        var orderNo = $(this).attr('href');
        layer.open({
          type: 2,
          title: '订单位置',
          scrollbar:false,
          area: ['893px', '600px'],
          anim: 2,
          content: ['/order/position?order_no='+orderNo, 'no'], //iframe的url，no代表不显示滚动条
        });
        return false;
    });

    //类目二级联动
    $('.first_class_id').change(function () {
        var pid = $(this).val();
        var html = '<option value="">全部二级类目</option>';
        if(pid!='' && pid > 0 ){
            $.getJSON('/common/ajax-get-class',{'pid':pid},function (json) {
                //console.log(json);
                if(json.success == true){
                    for(item in json.data.data){
                        html+='<option value="'+item+'">'+json.data.data[item]+'</option>';
                    }
                    $('.second_class_id').html(html);
                }
            })
        }
        else {
            $('.second_class_id').html(html);
        }
    });

    var layerIndex = null;
    $(".viewVisit").click(function () {
        workNo = $(this).attr('href');
        $.getJSON('/work/view-visit',{'work_no':workNo},function (json) {
            if(json.success == 1){
                $('#showVisitContent').html(json.data.content);

                if(layerIndex){
                    layer.close(layerIndex);
                }
                layerIndex = layer.open({
                    type: 1,
                    title: '提示',
                    area: ['300px', '220px'],
                    fixed: true,
                    maxmin: false,
                    content: $('#cancelLayer').html()
                });
            }
            else {
                alert(json.message);
            }
        });
        return false;
    });
    //关闭弹层
    $("body").delegate("#layerCancel","click",function(){
        if(layerIndex){
            layer.close(layerIndex);
        }
    });
}
</script>
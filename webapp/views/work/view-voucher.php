<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '查看凭证';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<!DOCTYPE html>
<html>
<head>
</head>
<body>
<style type="text/css">
  body{background: #fff;}
	.dts-list>span{text-align: right;font-weight: 500;font-family: "微软雅黑"}
  .layui-layer-setwin .layui-layer-close2{background-position: 1px -40px;right: 0;top: 10px;font-family: "微软雅黑"}
</style>
<section id="main-content">
    <!-- <h5 style="text-align: center;padding: 10px;background: #f8f8f8;margin:0;font-size: 16px;">收费详情</h5> -->
    <div class="bothEnds" style="width: 550px;margin:30px auto">
        <div class="dts-list">
			<span>收款项目:</span>
			<div class="dts-list-xq">
				<?php echo isset($data['work_cost_name'])?$data['work_cost_name']:'';?>
			</div>
		</div>
		<div class="dts-list">
			<span>付费方:</span>
			<div class="dts-list-xq">
                <?php echo isset($data['cost_payer'])?$data['cost_payer']:'';?>
			</div>
		</div>
		<div class="dts-list">
			<span>实际收费:</span>
			<div class="dts-list-xq">
                <?php echo isset($data['cost_real_amount'])?number_format($data['cost_real_amount'],2,'.',''):'';?>元
			</div>
		</div>
		<div class="dts-list">
			<span>凭证:</span>
			<div class="dts-list-xq">
                <?php if($data['voucher_img']) foreach ($data['voucher_img'] as $key=>$val){?>
                    <div class="gz-img">
                        <img src="<?php echo $val;?>" alt="">
                    </div>
                <?php }?>
			</div>
		</div>
		<div class="dts-list">
			<span>备注:</span>
			<div class="dts-list-xq">
                <?php echo isset($data['comment'])?$data['comment']:'';?>
			</div>
		</div>
    </div>
<img id="tong" src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" alt="placeholder+image" style="display: none;width: 450px">
             
</section>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script type="text/javascript">
	$(function(){
        fontSize ();
    })
    $(window).resize(function (){
        fontSize ();
    });
    function fontSize () {
        var whdef = 100/1920;
         var wH = window.innerHeight;
         var wW = window.innerWidth;
         var rem = wW * whdef;
         $('html').css('font-size', rem + "px");
    }
    $('.gz-img').on('click',function(argument) {
    	var srcImg = $(this).find('img').attr('src');
      window.open(srcImg);
    // 	$('#tong').attr('src',srcImg)
  		// layer.open({
    //         type: 1,
    //         title: false,
    //         closeBtn: 0,

    //         skin: 'layui-layer-nobg', 
    //         shadeClose: true,
    //         content: $('#tong')
    //       });

    })
</script>
</body>
</html>
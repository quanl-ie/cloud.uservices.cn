<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = '订单详情';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>?v=<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>?v=<?php echo time();?>">
<style type="text/css">

th, td{border: 0px solid transparent}
tr{border-bottom: 1px solid #d5d5d5}
td>a:link,td>a:visited{color: #264be8}
.layui-layer-setwin .layui-layer-close2{background-position: 1px -40px;right: 0;top: -4px;width: 16px;height: 16px;}
.layui-layer-setwin .layui-layer-close2:hover{background-position: 1px -40px;right: 0;top: -4px;width: 16px;height: 16px;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">工单详情</span>
</div>
<section id="main-content">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body" style="font-size: 14px;">
					<div class="col-md-4 col-lg-3" style="border-right: 1px solid #cdcdcc">
				   	  <div>
				   	  		<div class="order-top">
				   	  			<p class="order-number"><i class="icon iconfont icon-dingdan"></i><?=isset($result['data']['work_no'])?$result['data']['work_no']:''?></p>
                                <span class="ordoer-state <?php if($result['data']['status'] == 4){echo "green";}elseif ($result['data']['status'] == 6){echo "gray";}else{echo "orange";}?>">

                                    <?php if($result['data']['cancel_status']!=2):?>
                                        <?php echo isset($result['data']['status_desc'])?$result['data']['status_desc']:''?>
                                    <?php else:?>
                                    已关闭
                                    <?php endif;?>

                                </span>
				   	  		</div>
				   	  		<div>
				   	  			<ul class="order-details">
				   	  				<li><h5 class="title-vh5">客户信息</h5></li>
				   	  				<li>客户姓名 ：<?=isset($result['data']['account_name'])?$result['data']['account_name']:''?></li>
				   	  				<li>客户电话 ：<?=isset($result['data']['account_mobile'])?$result['data']['account_mobile']:''?></li>
				   	  				<li>服务地址 ：<?php echo $result['data']['conact_name'].' '. $result['data']['conact_phone'];?> <?=isset($result['data']['address_desc'])?$result['data']['address_desc']:''?></li>
				   	  				<li><h5 class="title-vh5">服务信息</h5></li>
                                    <li>服务内容 ：<?=isset($result['data']['service_content'])?$result['data']['service_content']:'';?></li>
				   	  				<li>预约服务时间 ：<?=isset($result['data']['plan_time'])?$result['data']['plan_time']:''?></li>
				   	  				<!--<li>质保状态：<?php /*if($result['data']['is_scope'] == 1){echo '保内';}elseif ($result['data']['is_scope'] == 2){echo '保外';}else{echo '未知';};*/?>
                                        <?php /*if(!in_array($result['data']['status'],[4,6]) && $result['data']['cancel_status']!=2){*/?>
                                            <span><a href="javascript:" class="edit_scope" data-no="<?php /*echo $result['data']['order_no'];*/?>" data-scope="<?php /*echo $result['data']['is_scope'];*/?>" data-id="<?php /*echo $result['data']['order_info']['department_id'];*/?>" data-work-no="<?php /*echo $result['data']['work_no'];*/?>" data-technicianid="<?php /*echo isset($result['data']['technician_id'])?$result['data']['technician_id']:0;*/?>">修改</a></span>
                                        <?php /*}*/?>
                                    </li>-->
				   	  				<!--<li><span class="o-details-span">质保凭证：</span>
										<div class="o-details-img">
                                            <?php /*if($result['data']['scope_img']) foreach ($result['data']['scope_img'] as $key=>$val){*/?>
                                                <?php /*if($val):*/?>
                                                <div class="gz-img">
                                                    <img src="<?php /*echo $val.'?x-oss-process=image/resize,m_fixed,h_150,w_250';*/?>" alt="" class="enlarge-img">
                                                </div>
                                                <?php /*endif;*/?>
                                            <?php /*}*/?>
				   	  					</div>
				   	  				</li>-->
				   	  				<li>
				   	  					<span class="o-details-span">故障图片：</span>
				   	  					<div class="o-details-img">
                                            <?php if($result['data']['fault_img']) foreach ($result['data']['fault_img'] as $key=>$val){?>
                                                <?php if($val):?>
                                                <div class="gz-img">
                                                    <img src="<?php echo $val.'?x-oss-process=image/resize,m_fixed,h_150,w_250';?>" alt=""  class="enlarge-img">
                                                </div>
                                                <?php endif;?>
                                            <?php }?>
				   	  					</div>
				   	  				</li>
				   	  				<li>备注：<?php echo isset($result['data']['description'])?$result['data']['description']:'无'?></li>
				   	  				<!--<li><h5 class="title-vh5">服务商信息</h5></li>
				   	  				<li>服务商：</li>
				   	  				<li>服务商电话：</li>-->
				   	  				<li><h5 class="title-vh5">下单信息</h5></li>
				   	  				<li>下单人 ：<?php
                                        if($result['data']['order_info']['source'] == 4){
                                            echo '微信小程序';
                                        }
                                        else if(isset($result['data']['create_user'])){
                                            echo $result['data']['create_user'];
                                        }
                                        ?></li>
				   	  				<li>下单时间 ：<?=isset($result['data']['create_time'])?$result['data']['create_time']:''?></li>
				   	  			</ul>
				   	  		</div>
				   	  </div>
				   </div>
				   <div class="col-md-8  col-lg-9">

                            <?php if($result['data']['cancel_status']!=2): ?>
					   		<div class="dts-top">
					   			<label class="dts-title">上门安装</label>
					   			<div class="dts-btnBox" >
                                    <?php if($result['data']['status'] != 4){?>
                                        <a href="<?php echo $result['data']['work_no'];?>" class="cancelOrder">
                                            <button class="btn bg-f7">关闭工单</button>
                                        </a>
                                    <?php }?>
                                    <?php if($result['data']['status'] == 1){?>
                                        <a href="/order/assign?work_no=<?php echo $result['data']['work_no']?>" >
                                            <button class="btn bg-f7">指派技师</button>
                                        </a>
                                    <?php }?>
                                    <!-- 待服务显示修改服务时间 -->
                                    <?php if($result['data']['status'] == 2){?>
                                        <a href="/order/re-assign?work_no=<?php echo $result['data']['work_no']?>" >
                                            <button class="btn bg-f7">改派技师</button>
                                        </a>
                                        <button class="btn bg-f7 changeTime" data-no="<?php echo $result['data']['work_no'];?>">修改服务时间</button>
                                    <?php }?>
                                    <!-- 待再次服务显示修改服务时间 -->
                                    <?php if($result['data']['status'] == 1 && $result['data']['is_first'] == 2){?>
                                        <button class="btn bg-f7 changeTime" data-no="<?php echo $result['data']['work_no'];?>">修改服务时间</button>
                                    <?php }?>
                                    <?php if($result['data']['status'] == 3){?>
                                        <a href="/work/set-finish?work_no=<?php echo $result['data']['work_no'];?>"><button class="btn bg-f7">完成服务</button></a>
                                    <?php }?>
                                    <?php if($result['data']['status'] == 4 && $result['data']['visit_status'] != 1){?>
                                        <button class="btn bg-f7 confirmVisit" data-workno="<?php echo $result['data']['work_no'];?>" data-orderno="<?php echo $result['data']['order_no'];?>">
                                            确认回访
                                        </button>
                                    <?php }?>
                                    <?php if($result['data']['status'] == 4 && $result['data']['confirm_status'] == 1){?>
                                        <button class="btn bg-f7 confirmSettlement" data-workno="<?php echo $result['data']['work_no'];?>">
                                            确认结算
                                        </button>
                                    <?php }?>
					   				<button class="btn btn-orange" onclick="javascript:history.back(-1);">返回</button>
					   			</div>
					   		</div>
							<p class="xian"></p>
                            <?php endif;?>
                       <?php if($result['data']['status'] == 4){?>
							<!-- 已完成的工单详情 -->
							<div class="dts-ok">
								<div class="dts-list">
                                    <span>技师信息:</span>
                                    <?php if($result['data']['technicianArr']):?>
                                    <?php foreach ($result['data']['technicianArr'] as $jsVal) :?>
									<div class="dts-list-xq" style="width:150px;float:left;margin:0px;">
										<div class="touxiang">
                                            <img src="<?php echo $jsVal['avatar'];?>?x-oss-process=image/resize,m_fixed,h_150,w_250">
										</div>
										<div class="lianxiren" style="display: inherit;">
                                            <?php echo $jsVal['name'] . ($jsVal['title']==''?'':('('.$jsVal['title'].')')) ;?></br>
										</div>
									</div>
                                    <?php endforeach; else :?>
                                    暂无
                                    <?php endif;?>
								</div>
								<div class="dts-list" style="clear:both;">
									<span style="width: 91px">预约上门时间:</span>
									<div class="dts-list-xq">
                                        <?=isset($result['data']['work_plan_time'])?$result['data']['work_plan_time']:''?>
									</div>
								</div>
								<div class="dts-list">
									<span style="width: 98px">服务完成时间：</span>
									<div class="dts-list-xq">
										<?php  if($result['data']['finsh_service_time'] != ''){echo date('Y-m-d H:i',$result['data']['finsh_service_time']);}?>
									</div>
								</div>
								<div class="dts-list">
									<span>服务工单：</span>
									<div class="dts-list-xq">
                                        <?php if($result['data']['work_img']) foreach ($result['data']['work_img'] as $key=>$val){
                                            if($val){
                                            ?>
                                            <div class="gz-img">
                                                <img src="<?php echo $val.'?x-oss-process=image/resize,m_fixed,h_150,w_250';?>" alt=""  class="enlarge-img">
                                            </div>
                                        <?php
                                            }
                                            }?>
									</div>
								</div>
								<div class="dts-list">
									<span>现场拍照：</span>
									<div class="dts-list-xq">
                                        <?php if($result['data']['scene_img']) foreach ($result['data']['scene_img'] as $key=>$val){?>
                                            <div class="gz-img">
                                              	<img src="<?php echo $val.'?x-oss-process=image/resize,m_fixed,h_150,w_250';?>" alt=""   class="enlarge-img">
                                            </div>
                                        <?php }?>
									</div>
								</div>
								<div class="dts-list">
									<span>服务记录：</span>
									<div class="dts-list-xq">
                                        <?php if($result['data']['service_record'] != ''){echo $result['data']['service_record'];}else{echo '';}?>
									</div>
								</div>
								<div class="dts-list">
									<span>产品记录：</span>
									<div class="dts-list-xq">
                                        <?php if($result['data']['product_logs']) foreach ($result['data']['product_logs'] as $key=>$val){?>
                                            <p class="product-record-list">
                                                序列号：<?php echo isset($val['sn'])?$val['sn']:'';?>
                                                </br>
                                                设备名称：<?php echo isset($val['product_name'])?$val['product_name']:'';?>
                                            </p>
                                        <?php }?>
									</div>
								</div>
								<div class="dts-list">
									<span>收费项目：</span>
									<div class="dts-list-xq">
										<table>
											<thead>
												<tr style="background: #f8f8f8">
													<th>项目名称</th>
													<th>单价</th>
													<th>数量</th>
													<th>实际总价</th>
													<th>付费方</th>
													<th>凭证</th>

												</tr>
											</thead>
											<tbody>
                                            <?php if($result['data']['work_cost']) foreach ($result['data']['work_cost'] as $key=>$val){?>
                                                <tr>
                                                    <td><?php echo isset($val['work_cost_name'])?$val['work_cost_name']:'';?></td>
                                                    <td><?php echo isset($val['cost_price'])?$val['cost_price']:'';?>元</td>
                                                    <td><?php echo isset($val['cost_number'])?$val['cost_number']:'';?></td>
                                                    <td><?php echo isset($val['cost_real_amount'])?$val['cost_real_amount']:'';?>元</td>
                                                    <td><?php echo isset($val['cost_payer'])?$val['cost_payer']:'';?></td>
                                                    <td><a href="<?php echo $val['id'];?>" class="view_voucher" data-id="<?php echo $val['id'];?>">立即查看</a></td>
                                                </tr>
                                            <?php }?>
											</tbody>
										</table>
									</div>
								</div>
								<div class="dts-list">
									<span>技师分成：</span>
									<div class="dts-list-xq">
										<!--<form class="dts-list-xq">-->
											<table>
												<thead>
													<tr style="background: #f8f8f8">
														<th>项目名称</th>
														<th>结算规则</th>
														<th>预结算金额</th>
														<th style="width: 100px">结算金额</th>
														<th style="width: 150px;">备注</th>
													</tr>
												</thead>
												<tbody>
                                                <?php if($result['data']['cost_divide_arr']) foreach ($result['data']['cost_divide_arr'] as $key=>$val){?>
                                                    <tr class="divide-into-tr" data-id="<?php echo isset($val['id'])?$val['id']:'';?>">
                                                        <td><?php echo isset($val['cost_name'])?$val['cost_name']:'';?></td>
                                                        <td><?php echo isset($val['divide_rules'])?$val['divide_rules']:'';?></td>
                                                        <td><?php echo isset($val['expect_amount'])?$val['expect_amount']/10000:'';?>元</td>
                                                        <td>
                                                            <span class="post-editing"><?php echo isset($val['divide_amount'])?$val['divide_amount']/10000:'';?>元</span>
                                                            <input class="edit-ipt ipt_amount" type="" name="" style="width: 90%" value="<?php echo isset($val['divide_amount'])?$val['divide_amount']/10000:'';?>">
                                                        </td>
                                                        <td>
                                                            <span class="post-editing"><?php echo isset($val['comment'])?$val['comment']:'';?></span>
                                                            <input class="edit-ipt" type="" name="" style="width: 90%" value="<?php echo isset($val['comment'])?$val['comment']:'';?>" title="">
                                                        </td>
                                                    </tr>
                                                <?php }?>
												</tbody>
											</table>
                                            <?php if($result['data']['status'] == 4 && $result['data']['settlement_status'] == 0): ?>
											<div style="margin-top: 30px;text-align: right;">
												<a id="edit" href="javascript:" class="btn btn-orange" style="color: #fff">编辑</a>
												<div class="editors">
													<a href="javascript:" id="edit-cancel" class="btn bg-f7">取消</a>
													<button class="btn btn-orange save-divide" style="margin-left: 20px">保存</button>
												</div>
											</div>
                                            <?php endif; ?>
										<!--</form>-->
									</div>
								</div>
							</div>
                       <?php }else{?>
                           <!-- 未完成的详情 -->
                           <div class="dts-buttom" style="">
                               <p>预约上门时间：<?=isset($result['data']['work_plan_time'])?$result['data']['work_plan_time']:''?></p>
                               <p>技师信息：
                               <?php if($result['data']['technicianArr']):?>
                                <?php foreach ($result['data']['technicianArr'] as $key=>$val):?>
                                <?php echo $val['name'] .'('.$val['title'].')' . ($key!= count($result['data']['technicianArr'])-1 ?'，':'');?>
                                <?php endforeach;else:?>
                               暂无
                                <?php endif;?>
                               </p>
                               <div style="margin-top: 30px">
                                   <span style="float: left;">收费项目：</span>
                                   <div class="dts-buttom-content">
                                       <table>
                                           <thead>
                                           <tr style="background: #f8f8f8">
                                               <th>项目名称</th>
                                               <th>单价</th>
                                               <th>数量</th>
                                               <th>实际总价</th>
                                               <th>付费方</th>
                                               <th>凭证</th>
                                               <th>操作</th>
                                           </tr>
                                           </thead>
                                           <tbody>
                                           <?php if($result['data']['work_cost']) foreach ($result['data']['work_cost'] as $key=>$val){?>
                                               <tr>
                                                   <td><?php echo $val['work_cost_name'];?></td>
                                                   <td><?php echo number_format($val['cost_price'],2,'.','');?>元/<?php echo $val['unit'];?></td>
                                                   <td><?php echo $val['cost_number'];?></td>
                                                   <td><?php echo number_format($val['cost_real_amount'],2,'.','');?>元</td>
                                                   <td><?php echo $val['cost_payer'];?></td>
                                                   <td><a href="javascript:" class="view_voucher" data-id="<?php echo $val['id'];?>">立即查看</a></td>
                                                   <td><a href="javascript:" class="edit_work_cost" data-id="<?php echo $val['id'];?>" data-pay-id="<?php echo $val['payer_id'];?>">修改</a></td>
                                               </tr>
                                           <?php }?>
                                           </tbody>
                                       </table>
                                   </div>
                                   <div style="text-align: right;margin-top: 30px">
                                        <?php if($result['data']['cancel_status']!=2):?>
                                       <button class="btn btn-orange add_work_cost" data-no="">添加收费项目</button>
                                        <?php endif;?>

                                   </div>
                                   <div style="display: none">
                                    <span id="work_no_add" data-id="<?php echo isset($result['data']['work_no'])?$result['data']['work_no']:'';?>"></span>
                                    <span id="type_add" data-id="<?php echo isset($result['data']['order_standard']['type'])?$result['data']['order_standard']['type']:'';?>"></span>
                                    <span id="department_id" data-id="<?php echo isset($result['data']['order_info']['department_id'])?$result['data']['order_info']['department_id']:'';?>"></span>
                                    <span id="standard_id_add" data-id="<?php echo isset($result['data']['order_standard']['standard_id'])?$result['data']['order_standard']['standard_id']:'';?>"></span>
                                    <span id="work_cost_id" data-id="<?php echo isset($result['data']['work_no'])?$result['data']['work_no']:'';?>"></span>
                                    <span id="technician_id" data-id="<?php echo isset($result['data']['technician_id'])?$result['data']['technician_id']:'';?>"></span>
                                   </div>
                               </div>
                           </div>
                       <?php }?>
				   </div>
            </div>
       </div>
   </div>
   
 </div>
</section>
<div id="cancelLayer" style="display: none">
    <div style="text-align: center;padding:20px 30px 10px">
        <p style="margin:0 0 20px 0">请选择关闭原因</p>
        <div class="select-fuji">
            <select  class="form-control cancelReason" style="width: 90%;margin-left: 5%">
                <option value="">请选择</option>
                <option value="下次服务不确定">下次服务不确定</option>
                <option value="客户不需要再次服务">客户不需要再次服务</option>
                <option value="其它原因">其它原因</option>
            </select>
        </div>
        <div style="margin-top:30px;">
            <button class="btn btn-success" id="layerSubmit"  style="padding: 4px 12px"><i class="fa fa-search"></i>确定</button>
            <button class="btn" id="layerCancel" style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>
<div id="setVisit" style="display: none">
    <div style="text-align: center;padding:20px 20px 10px">
        <p style="margin:0 0 10px 0;text-align: left;">请确认技师已完成服务并填写回访记录</p>
        <div class="select-fuji">
            <textarea placeholder="回访记录" id="visitContent" style="width: 100%;height: 100px;"></textarea>
        </div>
        <div style="text-align: right;margin-top:30px;">
            <button class="btn btn-success" id="layerSubmit2"  style="padding: 4px 12px"><i class="fa fa-search"></i>确定</button>
            <button class="btn" id="layerCancel" style="margin-left: 20px;padding: 4px 12px"><i class="fa fa-search" ></i>取消</button>
        </div>
    </div>
</div>
<!-- 图片放大 -->
<div class="imgEnlarge-box">
    <div class="hezi">
        <div class="imgEnlarge-chlid">
             <img src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" alt="placeholder+image" id="enlargeImg">
        </div>
    </div>
</div>
<script type="text/javascript">
    var layerIndex = null;
	$(function(){
        fontSize ();
        })
        $(window).resize(function (){
            fontSize ();
        });
        function fontSize () {
            var whdef = 100/1920;
             var wH = window.innerHeight;
             var wW = window.innerWidth;
             var rem = wW * whdef;
             $('html').css('font-size', rem + "px");
        }
        // 点击编辑技师分成按钮
        $('#edit').on('click',function() {
        	$('.post-editing').hide();
        	$('.edit-ipt').show();
        	$('.editors').show();
        	$(this).hide();
        });
        $('#edit-cancel').on('click',function () {
        	$('.post-editing').show();
        	$('.edit-ipt').hide();
        	$('.editors').hide();
        	$('#edit').show();
        });
    //修改技师分成
    $(".save-divide").click(function () {
    	//获取修改内容
        var divide_into_array = [];
        var divide_into_tr = $('.divide-into-tr');
        for (var i = 0; i < divide_into_tr.length; i++) {
        	var save_divideID = divide_into_tr[i].getAttribute('data-id');
        	var save_divideAmount = divide_into_tr[i].children[3].children[1].value;
        	var save_divideContent = divide_into_tr[i].children[4].children[1].value;
        	var object = {
        		id:save_divideID,
        		divide_amount:save_divideAmount,
        		comment:save_divideContent
        	}
        	divide_into_array.push(object);
        };
    	var check_data = 0;
    	// 验证
    	$('.ipt_amount').each(function(i) {
    		if($(this).val()==''){
	    		alert('请正确填写内容');
	    	}else{
	    		check_data ++;
	    		if(check_data==$('.ipt_amount').length){
	    			$.ajax({
			            url:'/work/edit-divide',
                        data:{data_array:divide_into_array},
			            type:'POST',
			            dataType:'json',
			            success:function(data) {
			                if(data.success == 1){
			                    if(layerIndex){
			                        layer.close(layerIndex);
			                    }
			                    alert('修改成功');
                                $('.post-editing').show();
                                $('.edit-ipt').hide();
                                $('.editors').hide();
                                $('#edit').show();
                                setTimeout(function(){
			                        window.location.reload(true);
                                },2000);
			                }
			                else {
			                    alert(data.message);
			                }
			                /*parent.layer.close(index);
			                parent.layer.alert(data.message, function(index){
			                    parent.location.reload();
			                });*/
			            }
			        });
	    		}

	    	}
    	})
    });
    //查看凭证详情
    $(".view_voucher").click(function () {
        var voucher_id = $(this).attr('data-id');
        layer.open({
            type: 2,
            title:false,
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/work/view-voucher?id='+voucher_id, 'no'],
            /*end:function(){
                location.reload();
            }*/
        });
        return false;
    });
 
    // 图片放大
	$('.enlarge-img').on('click',function() {
	   var imgUrl = $(this).attr('src');
	   var bb = imgUrl.substring(0,imgUrl.indexOf('?'));
	    $('#enlargeImg').attr('src',bb);
	    $('.imgEnlarge-box').show();
	})
	$('.imgEnlarge-box').on('click',function() {
	     $('.imgEnlarge-box').hide();
	})
    //修改服务时间
    $(".changeTime").click(function () {
        var workNO = $(this).attr('data-no');
        layer.open({
            type: 2,
            title: '请选择服务时间',
            area: ['600px', '350px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/work/change-time?work_no='+workNO, 'no'],
            /*end:function(){
                location.reload();
            }*/
        });
        return false;
    });
    //添加收费项目
    $(".add_work_cost").click(function () {
        var workNO          = $('#work_no_add').attr('data-id');       //工单号码
        var type            = $('#type_add').attr('data-id');        //此工单收费类型，1商家 2服务商
        var type_id         = $('#type_id_add').attr('data-id');      //商家或服务商id
        var standard_id     = $('#standard_id_add').attr('data-id');      //收费标准id
        var departmentId    = $('#department_id').attr('data-id');     //订单来源机构id

        if (workNO === null || workNO === undefined || workNO == '') {
            alert('数据获取失败，请联系管理员');return false;
        }
        if (departmentId === null || departmentId === undefined || departmentId == '') {
            alert('数据获取失败，请联系管理员');return false;
        }
        if (type === null || type === undefined || type == '') {
            //alert('数据获取失败，请联系管理员');return false;
        }
        if (type_id === null || type_id === undefined || type_id == '') {
            //alert('数据获取失败，请联系管理员');return false;
        }
        if (standard_id === null || standard_id === undefined || standard_id == '') {
            //alert('数据获取失败，请联系管理员');return false;
        }
        window.location.href = '/work/add-work-cost?work_no='+workNO+'&department_id='+departmentId;
        //window.location.href = '/work/add-work-cost?work_no='+workNO+'&type='+type+'&type_id='+type_id+'&standard_id='+standard_id+'&technician_id='+technician_id;

        /*layer.open({
            type: 2,
            title: '添加收费项目',
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/work/add-work-cost?work_no='+workNO, 'no'],
            /!*end:function(){
                location.reload();
            }*!/
        });*/
        return false;
    });
    //编辑收费项目
    $(".edit_work_cost").click(function () {
        var workNO          = $('#work_no_add').attr('data-id');       //工单号码
        var type            = $('#type_add').attr('data-id');        //此工单收费类型，1商家 2服务商
        var type_id         = $('#type_id_add').attr('data-id');      //商家或服务商id
        var standard_id     = $('#standard_id_add').attr('data-id');      //收费标准id
        var work_cost_id    = $(this).attr('data-id');     //修改时候此条收费标准id
        var cost_id         = $(this).attr('data-id');     //修改时候此条收费标准id
        var tech_id         = $('#technician_id').attr('data-id');
        var departmentId    = $('#department_id').attr('data-id');     //订单来源机构id
        var payerId         = $(this).attr('data-pay-id');     //订单来源机构id
        //window.location.href = '/work/edit-work-cost?work_no='+workNO+'&type='+type+'&type_id='+type_id+'&standard_id='+standard_id+'&cost_id='+cost_id+'&technician_id='+tech_id+'&work_cost_id='+work_cost_id;
        window.location.href = '/work/edit-work-cost?work_no='+workNO+'&department_id='+departmentId+'&cost_id='+cost_id+'&technician_id='+tech_id+'&work_cost_id='+work_cost_id+'&payer_id='+payerId;
        /*layer.open({
            type: 2,
            title: '添加收费项目',
            area: ['700px', '450px'],
            fixed: false, //不固定
            maxmin: false,
            scrollbar: false,
            content: ['/work/add-work-cost?work_no='+workNO, 'no'],
            /!*end:function(){
                location.reload();
            }*!/
        });*/
        return false;
    });
    //编辑质保状态
    $(".edit_scope").click(function () {
        var orderNO         = $('.edit_scope').attr('data-no');       //订单号码
        var workNO          = $('.edit_scope').attr('data-work-no');  //工单号码
        var orderScope      = $('.edit_scope').attr('data-scope');    //订单质保状态
        var srcType         = $('.edit_scope').attr('data-type');    //订单来源类型
        var srcId           = $('.edit_scope').attr('data-id');    //订单来源id
        var technicianId    = $('.edit_scope').attr('data-technicianid');    //技师id
        //alert(orderScope);return false;
        window.location.href = '/order/edit-scope?order_no='+orderNO+'&order_scope='+orderScope+'&src_type='+srcType+'&src_id='+srcId+'&work_no='+workNO+'&technician_id='+technicianId;
        return false;
    });
    $(".cancelOrder").click(function () {
        layerIndex = layer.open({
            type: 1,
            title: '提示',
            area: ['300px', '220px'],
            fixed: true,
            maxmin: false,
            content: $('#cancelLayer').html()
        });
        workNo = $(this).attr('href');
        return false;
    });
    //确认回访
    var workNo = '';
    var orderNo = '';
    $(".confirmVisit").click(function () {
        layerIndex = layer.open({
            type: 1,
            title: '提示',
            area: ['400px', '300px'],
            fixed: true,
            maxmin: false,
            content: $('#setVisit').html()
        });
        workNo = $(this).attr('data-workno');
        orderNo = $(this).attr('data-orderno');
        //alert(orderNo);
        return false;
    });
    //提交取消订单
    $("body").delegate("#layerSubmit","click",function(){
        var cancelReason = $(this).parents('div').find('.cancelReason').find('option:selected').val();
        if($.trim(workNo) == ''){
            alert('订单号不能为空');
            return false;
        }
        if($.trim(cancelReason) == ''){
            alert('请选择关闭原因');
            return false;
        }
        $.getJSON('/work/close',{'work_no':workNo,'cancel_reason':cancelReason},function (json) {
            if(json.success == 1){
                if(layerIndex){
                    layer.close(layerIndex);
                }
                alert('关闭成功');
                window.location=document.referrer;
            }
            else {
                alert(json.message);
            }
        });
    });
    //提交确认回访
    $("body").delegate("#layerSubmit2","click",function(){
        var visitContent = $(this).parents('div').find('#visitContent').val();
        if($.trim(workNo) == ''){
            alert('工单号不能为空');
            return false;
        }
        if($.trim(orderNo) == ''){
            alert('订单号不能为空');
            return false;
        }
        if($.trim(visitContent) == ''){
            alert('请填写回访记录');
            return false;
        }
        $.getJSON('/work/visit',{'order_no':orderNo,'work_no':workNo,'visit_content':visitContent},function (json) {
            if(json.success == 1){
                if(layerIndex){
                    layer.close(layerIndex);
                }
                alert('操作成功');
                window.location.reload(true);
            }
            else {
                alert(json.message);
            }
        });
    });
    //关闭弹层
    $("body").delegate("#layerCancel","click",function(){
        if(layerIndex){
            layer.close(layerIndex);
        }
    });
    //确认结算
    var confirmSettlementFlag = true;
    $('.confirmSettlement').click(function () {
        var workNo = $(this).attr('data-workno');
        confirm('确定要提交结算吗？',function (obj) {
            if(confirmSettlementFlag == true){
                confirmSettlementFlag = false;
                $.getJSON('/work/confirm-settlement',{'work_no':workNo},function (json) {
                    if(json.success == 1){
                        if(layerIndex){
                            layer.close(layerIndex);
                        }
                        alert('操作成功');
                        window.location.reload(true);
                    }
                    else {
                        alert(json.message);
                    }
                });
            }
        },this)
    });
</script>

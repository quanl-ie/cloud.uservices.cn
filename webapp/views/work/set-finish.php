<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$this->title = '确认完成服务';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">确认完成服务</span>
</div>
<!--引入CSS-->
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/webuploader/webuploader.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/jquery.autocomplete.css');?>">
<!--引入JS-->
<?=Html::jsFile('@web/webuploader/webuploader.js')?>
<?=Html::jsFile('@web/js/bootstrap-select.js')?>
<?=Html::jsFile('@web/webuploader/init.js')?>
<style>
    .datexz-wap{top: 37px;}
    .form-control[disabled], .form-control[readonly]{background: #fff;border: 1px solid  #ccc;cursor:pointer;}
    .popover{max-width: 500px !important;}
    input[name=fault_img]{display:none !important;}
    .help-block,.input-group{margin-bottom: 0};
    .btn-default:hover{color: #fff;}ul{margin-bottom: 0}
    em{font-style: normal;}
    .layui-layer-dialog .layui-layer-content{text-align: center;}
    .layui-layer-btn{text-align: center;}
    .layui-layer-btn0{position: static;width: auto;}
    .layui-layer-btn a{height: auto;line-height: 2}
</style>
<section id="main-content">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default" >
                <div class="panel-body"><?php
                    $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'form-horizontal',
                            'id' => 'form2',
                            'name' => 'form1',
                            'enctype' => 'multipart/form-data',
                        ],
                        'fieldConfig' => [
                            'template' => "{label}<i class=\"red-i\">{hint}</i>\n<div class=\"col-lg-6 col-md-7 col-sm-7 col-xs-7 fuzzy-query\">{input}{error}</div>",
                            'labelOptions' => ['class' => 'name-title-lable'],
                        ]
                    ]);
                    ?>
                    <input type="hidden" name="type" value="<?php echo $type;?>">
                    <input type="hidden" name="work_no" value="<?php echo isset($work_no)?$work_no:'';?>" id="work_no">
                    <?= $form->field($model, 'work_img')->widget('manks\FileInput', [
                            'clientOptions'=>[
                                'server' => Url::to('../upload/upload?source=fws'),
                                'pick'=>[
                                    'multiple'=>true,
                                ],
                                'fileNumLimit'=>'5',
                                'duplicate'=>true,
                                'fileSingleSizeLimit'=>'2097152', //限制上传大小为2M
                            ],
                        ]
                    ); ?>
                    <div class="col-md-offset-2 col-md-12 misplacement" style="display:none;margin-top: -20px" id='work_img_msg'></div>
                    <?= $form->field($model, 'scene_img')->widget('manks\FileInput', [
                            'clientOptions'=>[
                                'server' => Url::to('../upload/upload?source=fws'),
                                'pick'=>[
                                    'multiple'=>true,
                                ],
                                'fileNumLimit'=>'5',
                                'duplicate'=>true,
                                'fileSingleSizeLimit'=>'2097152', //限制上传大小为2M
                            ],
                        ]
                    ); ?>
                    <div class="form-group field-workorder-account_id">
                        <label class="name-title-lable" for="workorder-account_id">
                            服务记录：
                        </label>
                        <div  class="col-lg-4 col-md-7 col-sm-7 col-xs-12 fuzzy-query">
                            <textarea class="ramark-box" id="remarks" maxlength="200" placeholder="请填写服务记录" name="service_record" id="service_record"></textarea>
                            <p class="xianzhi"><span  id='word'>0</span>/200</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div  class="col-lg-6 col-md-7 col-sm-7 col-xs-7 fuzzy-query">
                            <div style="padding:0px;text-align: left;">
                                <a href="javascript:history.back(-1)"><button type="button" style="border: 1px solid #cccccc;" class="btn bg-f7">取消</button></a>
                                <input type="submit" id="btnSubmit" class="btn btn-orange mgr20 " style=" color:#FFF;" value="提交"  />
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="/js/jquery-migrate-1.1.0.js"></script>
<script src="/js/jquery.autocomplete.js" type="text/javascript"></script>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/myfunc.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>

<script>
    window.onload = function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        alert('<?php echo Yii::$app->session->getFlash('message'); ?>');
        <?php endif;?>
    }
    //单项选择
    $('.radio-li').on('click',function() {
        $(this).find('span').css('border-color','#2693ff');
        $(this).siblings('li').find('span').css('border-color','#ccc');
        $(this).parent('ul').next('input').val($(this).find('span').attr('id'));
    })
    /**********************   提交信息验证   ************************/
    $("#btnSubmit").click(function(e){
        // var flag = checkAll();
        //询问框
        if(flag){
            confirm('确定完成服务吗?',function(){
                $('#form2').submit();
            },1);
        }
        return false;
    });
    function checkAll() {
        //验证
        var flag = true;
        var amount_type              = $("#amount_type").val();//状态
        var work_img                 = $("input[name='WorkForm[work_img][]']").val();
        //验证质保
        if(amount_type.length==0 || isNaN(parseInt(amount_type))){
            flag = false;
            $("#amount_msg").text("请选择质保状态");
            $("#amount_msg").show();
        }
        //验证服务工单
        if(work_img == '' || work_img == undefined){
            flag = false;
            $("#work_img_msg").text("请上传服务工单");
            $("#work_img_msg").show();
        }
        return flag;
    }
    //获取焦点提示框消失
    $(".amount_type").click(function() {
        $("#amount_msg").text("");
        $("#amount_msg").hide();
    })
    $(".input-group-btn").click(function() {
        $("#work_img_msg").text("");
        $("#work_img_msg").hide();
    })
    //提示
    // $(".tip").popover();

</script>


<?php
$this->title = '服务区域管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    .region-box-ul,.region-box-title{margin-bottom: 0;}
    .region-box-ul{height: 249px;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">服务区域管理
    <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="请在您的可服务范围进行选择，如配置有误，可能会影响到商家派单"></span>
    </span>
    <?php if (isset($flag) && $flag == 1 ) : ?>
    <div class="right-btn-box">
        <?php if(in_array('/area/add',$selfRoles)){?>
            <a  href="/area/add"><button class="btn btn-success">编辑</button></a>
        <?php };?>
    </div>
    
    <div class="bc-btn">
        <button class="btn btn-success" id="regionOk" >保存</button>
        <button class="btn btn-success" id="regionCancel" style="margin-left: 30px;">取消</button>
    </div>
    <?php endif; ?>
</div> 
<section id="main-content">
    <div class="panel-body">
            <!-- 展示状态 -->
            <div class="region-show  col-lg-12 col-md-12 col-sm-12 sol-xs-12 " style="display: block">
                <div class="col-lg-3 col-md-4 col-sm-6 sol-xs-12">
                    <div class="region-box">
                        <p class="region-box-title">选择服务省份</p>
                        <ul class="region-box-ul">
                            <?php if (!empty($province)) : ?>
                            <?php foreach ($province as $key => $val) : ?>
                            <li><?=$val['region_name'] ?></li>
                            <?php endforeach; ?>
                            <?php endif;?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 sol-xs-12">
                    <div class="region-box">
                        <p class="region-box-title">选择服务城市</p>
                        <ul class="region-box-ul">
                            <?php if (!empty($city)) : ?>
                            <?php foreach ($city as $key => $val) : ?>
                            <li><?=$val['region_name'] ?></li>
                            <?php endforeach; ?>
                            <?php endif;?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 sol-xs-12">
                    <div class="region-box">
                        <p class="region-box-title">选择服务区/县</p>
                        <ul class="region-box-ul">
                            <?php if (!empty($district)) : ?>
                            <?php foreach ($district as $key => $val) : ?>
                            <li><?=$val['region_name'] ?></li>
                            <?php endforeach; ?>
                            <?php endif;?>
                        </ul>
                    </div>
                </div>
            </div>
            
    </div>
</section>
<script>
$(".tip").popover();
</script>

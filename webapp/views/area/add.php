<?php
$this->title = '服务区域管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    .region-box-ul,.region-box-title{margin-bottom: 0;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">服务区域管理</span>
    <div  class="right-btn-box">
        <button class="btn btn-success" id="regionOk1" href="javascript:">保存</button>
        <a href="/area/index"><button class="btn btn-success" style="margin-left: 30px;">取消</button></a>
    </div>
</div> 
<section id="main-content">
    <div class="panel-body">
        <!-- 修改状态 -->
        <div class="region-revise col-lg-12 col-md-12 col-sm-12 sol-xs-12">
            <form>
                <div class="col-lg-3 col-md-4 col-sm-6 sol-xs-12">
                    <div class="region-box">
                        <p class="region-box-title">选择服务省份</p>
                        <ul class="region-box-ul" id="provinceUl" style="height: 249px">
                            <?php if (!empty($provinceList)) : ?>
                            <?php foreach ($provinceList as $key => $val): ?>
                            <?php if (in_array($val['region_id'], $province)) : ?>
                            <li>
                                <input type="checkbox" id="<?=$val['region_id'] ?>" checked="checked" name="">
                                <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                            </li>
                            <?php else : ?>
                            <li>
                                <input type="checkbox" id="<?=$val['region_id'] ?>" name="">
                                <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                            </li>
                            <?php endif; ?>
                            <?php endforeach ; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 sol-xs-12">
                    <div class="region-box">
                        <p class="region-box-title">选择服务城市</p>
                        <span class="whole-region-check">
                            <input type="checkbox" id="wholeCity" name="">
                            <label class="brand-label" for="wholeCity"><i class="gou-i"></i>全部</label>
                        </span> 
                        <ul class="region-box-ul" id="cityUl">
                            <?php if (!empty($city)) : ?>
                            <?php foreach ($city as $key => $val): ?>
                            <li>
                                <input type="checkbox" id="<?=$val['region_id'] ?>" checked="checked" name="">
                                <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                            </li>
                            <?php endforeach ; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 sol-xs-12">
                    <div class="region-box">
                        <p class="region-box-title">选择服务区/县</p>
                        <span class="whole-region-check">
                            <input type="checkbox" id="wholeArea" name="">
                            <label class="brand-label" for="wholeArea"><i class="gou-i"></i>全部</label>
                        </span> 
                        <ul class="region-box-ul" id="areaUl">
                            <?php if (!empty($city)) : ?>
                            <?php foreach ($district as $key => $val): ?>
                            <li>
                                <input type="checkbox" id="<?=$val['region_id'] ?>" checked="checked" name="">
                                <label class="brand-label" for="<?=$val['region_id'] ?>"><i class="gou-i"></i><?=$val['region_name'] ?></label>
                            </li>
                            <?php endforeach ; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function() {
        initializationCheckAll($('#cityUl'))
        initializationCheckAll($('#areaUl'))
    })
    function initializationCheckAll(ele) {
        var ckdLength1 = $(ele).find('input').length;
        var ckdLength2 = [];
        $(ele).find('input').each(function(i,el) {
            if($(el).is(':checked')){
                ckdLength2.push($('el').val());
            }
        })
        ckdLength2.length == ckdLength1 ? $(ele).prevAll('.whole-region-check').find('input').prop('checked',true): '';
    }
    var id1=[],id2=[],id3=[],emptyId2=[],emptyId3=[],districtIds =[];
    // 保存 取消
    $('#regionOk1').on('click',function() {

        function_name ();

    })
    $(function() {
        var checkboxInput =$("#areaUl li input[type=checkbox]:checked");
        for(var i=0;i<checkboxInput.length;i++){
                districtIds[i] =checkboxInput[i].getAttribute('id');
        }
    })
    $('#regionCancel').on('click',function() {
        $('.bj-btn button').show();
        $('.bc-btn').hide();
        $('.region-show').show();
        $('.region-revise').hide();
    })
    // 联动点击
    $('#provinceUl li input').on('change',function() {
        $('#wholeArea').prop("checked",false);
        $('#wholeCity').prop("checked",false);
        $('#areaUl').html('');
        var el = {
            Url:'/area/city',
            element:$('#provinceUl li input'),
            data:{province:[]},
            box:$('#cityUl'),
            judge:1

        }
        linkage (el);
        if ($("#provinceUl li input[type=checkbox]:checked").length ===  el.element.length ) {
            $('#wholeProvince').prop("checked",true);
        }else{
            $('#wholeProvince').prop("checked",false);
        }
    })
    $('#cityUl').on('change','li input',function() {
         var el = {
            Url:'/area/district',
            element:$('#cityUl li input'),
            data:{citys:[]},
            box:$('#areaUl'),
            judge:2
        }
        linkage (el);
        if ($("#cityUl li input[type=checkbox]:checked").length ===  el.element.length ) {
            $('#wholeCity').prop("checked",true);
        }else{
            $('#wholeCity').prop("checked",false);
        }
        $('#areaUl').prevAll('.whole-region-check').find('input').prop('checked',false);

    })
    $('#areaUl').on('change','li input',function() {
        areaUl ();
        var wholeCheck = $('#areaUl li input');
        if ($("#areaUl li input[type=checkbox]:checked").length ===  wholeCheck.length ) {
            $('#wholeArea').prop("checked",true);
        }else{
            $('#wholeArea').prop("checked",false);
        }

    })
    function linkage (el) {
        // elment:获取input的值,
        var provinceUlHtml='';
        var inputChecked = el.element;
        var ids      = [];
        if(inputChecked.is(':checked')){
            for(var i=0;i<inputChecked.length;i++){
                if(inputChecked[i].checked){
                    if (inputChecked[i].getAttribute('id') !== '') {
                        ids[i] = inputChecked[i].getAttribute('id');
                    }
                }
            }
            (el.judge==2)?el.data.citys=ids : el.data.province = ids;
        }
        var data1 = (el.judge==2)?el.data.citys.length:el.data.province.length;
        if(data1>0){
            $.ajax({
                url:el.Url,
                data:el.data,
                type:'POST',
                dataType:'json',
                success:function(data){
                    if(el.judge==1){
                        $.each(data.city,function (i,pro) {
                            emptyId2[i]=pro.region_id;
                            provinceUlHtml += '<li>\n' +
                                '                   <input type="checkbox" id="'+pro.region_id+'" name="">\n' +
                                '                       <label class="brand-label" for="'+pro.region_id+'"><i class="gou-i"></i>'+pro.region_name+'</label>\n' +
                                '              </li>';
                        });
                    }else{
                        $.each(data,function (i,pro) {
                             provinceUlHtml += '<li>\n' +
                                 '                  <input type="checkbox" id="'+pro.region_id+'" name="">\n' +
                                 '                      <label class="brand-label" for="'+pro.region_id+'"><i class="gou-i"></i>'+pro.region_name+'</label>\n' +
                                 '               </li>';
                              
                        })
                    }
                    el.box.html(provinceUlHtml);
                }
            });
        }else{
            el.box.html('');
        }
    }
    function areaUl () {
        var selected = $('#areaUl li input');
        var ids      = [];
        if(selected.is(':checked')){
            for(var i=0;i<selected.length;i++){
                if(selected[i].checked){
                    if (selected[i].getAttribute('id') !== '') {
                        ids[i] = selected[i].getAttribute('id');
                    }
                    
                }
            }
            blank (ids);
            districtIds=ids;
        }

    }
    // 去掉数组中的空符
    function blank (ids) {
        for (var i = 0; i < ids.length; i++) {
                if(ids[i] ==""||typeof(ids[i])=="undefined"){
                        ids.splice(i,1);
                        i = i-1;
                    }
            };
    }
    // 选择全部
    $('#wholeProvince').on('change',function() {

        if ($(this).prop('checked')==true) {
            $('#provinceUl').find('li input').prop("checked",true);
            provinceUl();
        }else{
            $('#provinceUl').find('li input').prop("checked",false);
            // $('#cityUl').find('li input').prop("checked",false);
            // $('#areaUl').find('li input').prop("checked",false);
            $('#cityUl').html('');
            $('#areaUl').html('');
        }
    })
    $('#wholeCity').on('change',function() {
        $('#wholeArea').prop("checked",false);
        if(this.checked){
            $('#cityUl').find('li input').prop("checked",true);
        }else{
            $('#cityUl').find('li input').prop("checked",false);
            $('#areaUl').html('');
        }
        var el = {
            Url:'/area/district',
            element:$('#cityUl li input'),
            data:{citys:[]},
            box:$('#areaUl'),
            judge:2
        }
        linkage (el);
    })
    $('#wholeArea').on('change',function() {
        districtIds = new  Array();
        if($(this).prop('checked') == true){
            $("#areaUl li input").prop('checked',true);
        }
        else {
            $("#areaUl li input").prop('checked',false);
        }

        $("#areaUl li input").each(function(){
           if($(this).prop('checked') == true){
                districtIds.push($(this).attr('id'));
           }
        });
    })

    
    // 提交
    function function_name ()
    {
        if($("#provinceUl li input[type=checkbox]:checked").length>0){
            provinceIds=id1;
        }
        else{
            districtIds='';
            alert('请选择服务区域');
            return false;
        }

        if($("#areaUl li input[type=checkbox]:checked").length == 0){
            alert('请选择服务区域');
            return false;
        }

        if(districtIds!=''){
            $.ajax({
                type: "POST",
                url:'/area/add',
                data:{districtIds:districtIds},
                dataType:'json',
                success: function (data) {
                    if (data.code == 200) {
                        alert(data.message, function(index){
                            location.href='/area/index';
                        });
                    }else{
                        alert(data.message);
                    }
                }
            })
        }else{
            alert('请选择服务区域2');
            return false;
        }
    }
</script>
<?php

use yii\helpers\Url;
use webapp\models\BrandQualification;

$this->title = '技师接单统计';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/us-icon.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/css/return-purchase/index.css'); ?>?v=<?php echo time(); ?>">
<style type="text/css">
    .us-table-body-empty {
        padding: 10px 0;
    }

    .us-table tbody, .us-table thead, .us-th {
        border: 1px solid rgb(234, 234, 234) !important;
    }

    .bottom-none {
        border-bottom: none !important;
    }

    .us-th-rigth {
        border-right: 1px solid rgb(234, 234, 234) !important;
    }

    .us-table th, .us-table td {
        text-align: center;
        white-space: nowrap;
    }

    .us-table thead th {
        height: 38px !important;
    }

    .us-table-wrap {
        width: auto !important;
    }
</style>
<div id="app" v-cloak>
    <?php if (!in_array('/reporting/technician-order-count', $selfRoles)): ?>
        <div style="text-align: center;font-size:20px; font-weight: bold;">无权限</div>
    <?php else: ?>
        <section id="main-content">
        <div class="panel-body">
            <us-adv-search title="技师接单统计" placeholder="请输入技师姓名/电话" @on-search="searchData" @on-input-search="inputData"
                           v-model="search.technicianInfo">
                <div style="float: right;" slot="adv-header">
                    <us-button type="primary" @click="exportBtn()">导出</us-button>
                </div>
                <us-form label-whl="70px" item-whl="260px">
                    <us-form-row>
                        <us-form-item label="日期：" whl="320px">
                            <us-ipt-datetime type="date" whl="200px"
                                             :format="['yyyy-MM']"
                                             v-model="search.date"></us-ipt-datetime>
                        </us-form-item>

                        <us-form-item label="机构：" whl="300px">
                            <us-ipt-select v-model="search.company" whl="200px">
                                <us-select-option value="0" label="请选择机构"></us-select-option>
                                <us-select-option v-for="item in companyList" :key="item.id" :label="item.name"
                                                  :value="item.id"></us-select-option>
                            </us-ipt-select>
                        </us-form-item>

                        <us-form-item label="服务类型：" whl="300px">
                            <us-ipt-select v-model="search.serviceType" whl="200px">
                                <us-select-option value="0" label="请选择服务类型"></us-select-option>
                                <us-select-option v-for="item in serviceTypeList" :key="item.id" :label="item.title"
                                                  :value="item.id"></us-select-option>
                            </us-ipt-select>
                        </us-form-item>
                    </us-form-row>
                </us-form>
            </us-adv-search>
            <div class="table-list-wrap">
                <div class="us-table-wrap" :style="[{borderBottom:haveDataVisible?'none':'1px solid #eee'}]">
                    <table class="us-table" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="us-th bottom-none"><span class="us-table-cell">技师</span></th>
                                <th class="us-th bottom-none"><span class="us-table-cell">所属机构</span></th>
                                <th class="us-th bottom-none"><span class="us-table-cell">接单数</span></th>
                                <th class="us-th bottom-none"><span class="us-table-cell">完成数</span></th>
                                <th class="us-th bottom-none"><span class="us-table-cell">好评率</span></th>
                                <th class="us-th bottom-none"><span class="us-table-cell">差评率</span></th>
                                <th class="us-th bottom-none"><span class="us-table-cell">按时上门率</span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item in dataList" class="noBorderBottom" v-if="haveDataVisible">
                                <td><span class="us-table-cell us-table-cell-ellipsis" :title="item.technician_name">{{item.technician_name}}</span></td>
                                <td><span class="us-table-cell us-table-cell-ellipsis" :title="item.department_name">{{item.department_name}}</span></td>
                                <td><span class="us-table-cell">{{item.work_num}}</span></td>
                                <td><span class="us-table-cell">{{item.finish_num}}</span></td>
                                <td><span class="us-table-cell">{{item.goods_appraise_num}}</span></td>
                                <td><span class="us-table-cell">{{item.bad_appraise_num}}</span></td>
                                <td><span class="us-table-cell">{{item.on_time}}</span></td>
                            </tr>
                        </tbody>
                    </table>
                    <div v-if="nodataVisible" class="us-table-body-empty text-center">无数据</div>
                </div>
                <div class="pagination-align" v-if="haveDataVisible">
                    <us-pagination
                            @change="changePage"
                            :page-no="pageParams.pageNo"
                            :total-page="pageParams.totalPage"
                            :total="pageParams.total"></us-pagination>
                </div>
            </div>
        </div>
    </section>
    <?php endif;?>
</div>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/reporting/technician-order-count.js?v=<?php echo $v; ?>"></script>
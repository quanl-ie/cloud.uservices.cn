<?php

use yii\helpers\Url;
use webapp\models\BrandQualification;

$this->title = '平台服务收费统计';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/us-icon.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/css/return-purchase/index.css'); ?>?v=<?php echo time(); ?>">
<style type="text/css">
    .us-table-body-empty {
        padding: 10px 0;
    }

    .us-table tbody, .us-table thead, .us-th {
        border: 1px solid rgb(234, 234, 234) !important;
    }

    .bottom-none {
        border-bottom: none !important;
    }

    .us-th-rigth {
        border-right: 1px solid rgb(234, 234, 234) !important;
    }

    .us-table th, .us-table td {
        text-align: center;
        white-space: nowrap;
    }

    .us-table thead th {
        height: 25px !important;
    }

    .us-table thead th .us-table-cell {
        height: 20px !important;
        min-height: auto;
        line-height: 20px;
    }

    .us-table thead tr:first-child th {
        padding-top: 10px !important;
        padding-bottom: 0 !important;
    }

    .us-table th .us-table-cell {
        height: auto;
        line-height: normal;
    }

    .us-table-wrap {
        width: auto !important;
    }
    .us-adv-search-fixed .us-ipt-wrap{
        display:none;
    }
    .us-adv-search .h3{
        padding-right: 0;
    }
</style>
<div id="app" v-cloak>
    <section id="main-content">
        <div class="panel-body">
            <us-adv-search title="平台服务收费统计" @on-search="searchData" @on-input-search="inputData"
                           v-model="search.name">
                <div style="float: right;" slot="adv-header">
                    <us-button type="primary" @click="exportBtn()">导出</us-button>
                </div>
                <us-form label-whl="70px" item-whl="260px">
                    <us-form-row>
                        <us-form-item label="日期：" whl="320px">
                            <us-ipt-datetime type="date" whl="200px"
                                             :format="['yyyy-MM']"
                                             v-model="search.date"></us-ipt-datetime>
                        </us-form-item>

                        <us-form-item label="服务类型：" whl="370px">
                            <us-ipt-select v-model="search.serviceType" rule="defined?请选择服务类型">
                                <us-select-option value="1" label="安装"></us-select-option>
                                <us-select-option value="2" label="维修"></us-select-option>
                            </us-ipt-select>
                        </us-form-item>

                        <us-form-item label="收费项目：" whl="370px">
                            <us-ipt-select v-model="search.feePro" rule="defined?请选择收费项目">
                                <us-select-option value="1" label="项目1"></us-select-option>
                                <us-select-option value="2" label="项目2"></us-select-option>
                            </us-ipt-select>
                        </us-form-item>
                    </us-form-row>
                </us-form>
            </us-adv-search>
            <div class="table-list-wrap">
                <div class="us-table-wrap" :style="[{borderBottom:dataList.length>0?'none':'1px solid #eee'}]">
                    <table class="us-table" cellspacing="0" v-if="dataList.length>0">
                        <thead>
                        <tr>
                            <th class="us-th bottom-none"><span class="us-table-cell">收费项目</span></th>
                            <th class="us-th bottom-none"><span class="us-table-cell">客户付费/元</span>
                            </th>
                            <th class="us-th bottom-none"><span class="us-table-cell">商家付费/元</span></th>
                            <th class="us-th bottom-none"><span class="us-table-cell">免费</span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="item in dataList">
                            <td><span class="us-table-cell">{{item.feePro}}</span></td>
                            <td><span class="us-table-cell">{{item.customerFee}}</span></td>
                            <td><span class="us-table-cell">{{item.storeFee}}</span></td>
                            <td><span class="us-table-cell">{{item.freeFee}}</span></td>
                        </tr>
                        <tr>
                            <td><span class="us-table-cell">合计</span></td>
                            <td><span class="us-table-cell">1</span></td>
                            <td><span class="us-table-cell">2</span></td>
                            <td><span class="us-table-cell">3</span></td>
                        </tr>
                        </tbody>
                    </table>
                    <div v-if="dataList.length == 0" class="us-table-body-empty text-center">无数据</div>
                </div>
                <div class="pagination-align">
                    <us-pagination
                        @change="changePage"
                        :page-no="pageParams.pageNo"
                        :total-page="pageParams.totalPage"
                        :total="pageParams.total"></us-pagination>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/reporting/fee-count.js?v=<?php echo $v; ?>"></script>
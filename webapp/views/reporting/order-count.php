<?php

use yii\helpers\Url;
use webapp\models\BrandQualification;

$this->title = '平台派单量统计';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/app.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/font-awesome.min.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/js/vue/static/css/us-icon.css'); ?>?v=<?php echo time(); ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo Url::to('/css/return-purchase/index.css'); ?>?v=<?php echo time(); ?>">
<style type="text/css">
    .us-table-body-empty {
        padding: 10px 0;
    }

    .us-table tbody, .us-table thead, .us-th {
        border: 1px solid rgb(234, 234, 234) !important;
    }

    .us-table th, .us-table td {
        text-align: center;
        white-space: nowrap;
    }

    .us-table thead th {
        height: 38px !important;
    }

    .us-table-wrap {
        width: auto !important;
    }

    .us-adv-search-fixed .us-ipt-wrap {
        display: none;
    }

    .us-adv-search .h3 {
        padding-right: 0;
    }

    .noBorderBottom td {
        border: none;
    }

    .us-table tr:last-child td {
        border-top: 1px solid #eee;
    }

    #container {
        background: #fff;
    }
</style>
<div id="app" v-cloak>
    <?php if (!in_array('/reporting/order-count', $selfRoles)): ?>
        <div style="text-align: center;font-size:20px; font-weight: bold;">无权限</div>
    <?php else: ?>
        <section id="main-content">
        <div class="panel-body">
            <us-adv-search title="平台派单量统计" @on-search="searchData">
                <div style="float: right;" slot="adv-header">
                    <us-button type="primary" @click="exportBtn()">导出</us-button>
                </div>
                <us-form label-whl="70px">
                    <us-form-row>

                        <us-form-item label="日期：" whl="320px">
                            <us-ipt-datetime type="date" whl="200px"
                                             :format="['yyyy-MM']"
                                             v-model="search.date"></us-ipt-datetime>
                        </us-form-item>

                        <us-form-item label="机构：" whl="300px">
                            <us-ipt-select v-model="search.company" whl="200px">
                                <us-select-option value="0" label="请选择机构"></us-select-option>
                                <us-select-option v-for="item in companyList" :key="item.id" :label="item.name"
                                                  :value="item.id"></us-select-option>
                            </us-ipt-select>
                        </us-form-item>

                        <us-form-item label="服务类型：" whl="300px">
                            <us-ipt-select v-model="search.serviceType" whl="200px">
                                <us-select-option value="0" label="请选择服务类型"></us-select-option>
                                <us-select-option v-for="item in serviceTypeList" :key="item.id" :label="item.title"
                                                  :value="item.id"></us-select-option>
                            </us-ipt-select>
                        </us-form-item>

                    </us-form-row>
                </us-form>
            </us-adv-search>
            <div class="table-list-wrap">
                <div class="us-table-wrap" :style="[{borderBottom:haveDataVisible?'none':'1px solid #eee'}]">
                    <table class="us-table" cellspacing="0">
                        <thead>
                        <tr>
                            <th>日期</th>
                            <th>新增单数/单</th>
                            <th>完成单数/单</th>
                            <th>取消单数/单</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="item in dataList" class="noBorderBottom" v-if="haveDataVisible">
                            <td><span class="us-table-cell">{{item.date}}</span></td>
                            <td><span class="us-table-cell">{{item.new_work_num}}</span></td>
                            <td><span class="us-table-cell">{{item.finish_work_num}}</span></td>
                            <td><span class="us-table-cell">{{item.cancel_work_num}}</span></td>
                        </tr>
                        <tr v-if="haveDataVisible">
                            <td><span class="us-table-cell">合    计</span></td>
                            <td><span class="us-table-cell">{{sumData.new_work_sum}}</span></td>
                            <td><span class="us-table-cell">{{sumData.finish_work_sum}}</span></td>
                            <td><span class="us-table-cell">{{sumData.cancel_work_sum}}</span></td>
                        </tr>
                        </tbody>
                    </table>
                    <div v-if="nodataVisible" class="us-table-body-empty text-center">无数据</div>
                </div>
                <div class="pagination-align" v-if="haveDataVisible">
                    <us-pagination @change="changePage"
                                   :page-no="pageParams.pageNo"
                                   :total-page="pageParams.totalPage"
                                   :total="pageParams.total">
                    </us-pagination>
                </div>
            </div>
        </div>
    </section>
    <?php endif;?>
</div>
<script type="text/javascript" src="/js/vue/lib/axios.min.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/axios.config.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/manifest.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/vendor.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/static/js/app.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/lib/vue.ajax.js?v=<?php echo $v; ?>"></script>
<script type="text/javascript" src="/js/vue/controller/reporting/order-count.js?v=<?php echo $v; ?>"></script>
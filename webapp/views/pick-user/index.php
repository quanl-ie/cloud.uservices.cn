<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = '提货人管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::cssFile('@web/webuploader/webuploader.css') ?>
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/public.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/iconfont.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo Url::to('/css/style.css'); ?>">
<!DOCTYPE html>
<html>
<head>
</head>
<style type="text/css">
    body {
        font-family: "微软雅黑", sans-serif;
        background: #fff;
    }

    .col-sm-4, .col-md-4, .col-lg-4 {
        padding-left: 0;
    }

    input[type=radio] {
        margin-top: 10px;
    }

    .linkage-justify-list {
        width: 30%;
    }

    .three-linkage {
        width: 100%;
    }

    .information-list {
        padding: 0 40px;
    }

    .name-title-lable {
        left: 40px;
    }

    .main-content-wrapper {
        height: auto;
        min-height: auto;
        background: #fff;
    }

    #container {
        background: #fff;
    }

    #app {
        padding: 20px;
    }

    .table-grid {
        border-collapse: collapse;
        max-width: 100%;
        background-color: transparent;
        width: 100%;
        table-layout: fixed;
    }

    .table-grid thead th, .table-grid tbody td {
        text-align: center;
    }

    .table-grid thead th {
        background: #f8f8f8;
        font-size: 12px;
        font-weight: bold;
        font-family: MicrosoftYaHei-Bold, sans-serif;
    }

    .table-grid thead th:first-child {
        width: 70px;
    }

    .table-grid tbody td {
        background: #fff;
        font-size: 12px;
        font-family: MicrosoftYaHei, sans-serif;
    }

    .table-grid tbody td:before {
        content: "";
        display: inline-block;
        height: 100%;
        vertical-align: middle;
    }

    .table-grid tbody td span {
        display: inline-block;
        vertical-align: middle;
        overflow: hidden;
        -ms-text-overflow: ellipsis;
        text-overflow: ellipsis;
        white-space: nowrap;
        width: 70%;
    }

    .table-grid tbody td input[type='text'] {
        display: inline-block;
        vertical-align: middle;
        margin: 0;
    }

    input[type="checkbox"] + label {
        position: relative;
        display: inline-block;
        width: 15px;
        height: 15px;
    }

    input[type="checkbox"]:checked + label::before {
        top: 10px !important;
    }

    input[type="checkbox"]:checked + label i {
        top: 6px;
    }

    .icon2-bainji-1 {
        display: inline-block;
        font-size: 20px;
        vertical-align: middle;
        color: #999999;
        cursor: pointer;
    }

    #app .buttons {
        padding-top: 10px;
    }

    .buttons button {
        margin-right: 10px;
    }

</style>
<body>
<div id="app">
    <form action="/pick-user/add" method="post" id="account-address-add">

        <table id="data-grid" cellspacing="0" class="table-grid">
            <thead>
            <tr>
                <th>
                    <input type="checkbox" id="check-all">
                    <label for="check-all">
                        <i class="gou-i"></i>
                    </label>
                </th>
                <th>提货人姓名</th>
                <th>提货人电话</th>
                <th>身份证号</th>
                <th>车牌号</th>
            </tr>
            </thead>
            <tbody id="table-body"></tbody>
        </table>
        <div class="buttons">
            <button type="button" id="add" class="btn bg-f7">添加</button>
            <button type="button" id="delete" class="btn bg-f7">删除</button>
        </div>
    </form>
</div>
</body>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/layer/layer.js"></script>
<script src="/js/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script>
    
    $( function () {
        
        var index = parent.layer.getFrameIndex( window.name );
        //关闭iframe
        $( '#closeIframe' ).click( function () {
            parent.layer.close( index );
        } );
        
        var mobileReg = /^(?:[1]\d{10})$/;
        // var cardReg = /^(?:\d{15}|\d{18}|(?:\d{17}[xX]))$/;
        var blankReg = /^\s*$/;
        var fields = ['pick_name', 'pick_mobile', 'pick_card', 'pick_license'];//字段属性
        var valid = {
            pick_name   : function ( v ) {
                return blankReg.test( v ) ? "提货人姓名不能为空" : true
            },
            pick_mobile : function ( v ) {
                return blankReg.test( v )
                    ? "提货人电话不能为空" : mobileReg.test( v )
                        ? true : "提货人电话格式不正确"
            },
            pick_card   : function ( v ) {
                return blankReg.test( v ) ? "提货人身份证号码不能为空" : true
            }
        };

        function noop() { return true}

        var dataGrid = (function ( fields, valid ) {

            // 通知事件名称
            var INPUT_HAS_CHECKED = "ipt.has",// 复选框是否选择
                INPUT_CHECK_ALL = "ipt.checkAll",// 全选或取消
                INPUT_CANCEL_CHECK = "ipt.cancelCheck", //取消选择
                OUT_SUBMIT = "out.submit",
                DATA_UPDATE = "data.update", // 更新dom上的数据
                DATA_COLLECT = "data.collect",// 搜集数据
                DATA_AFTER_SUBMIT = "data.afterSubmit",// 提交操作后
                CLICK_OUTSIDE = 'click.outside',
                DATA_EMPTY = "data.empty";// 空数据
            // 定义变量
            var tbody = $( "#table-body" ),
                checkAllJq = $( "#check-all" ),
                calls = $.Callbacks(),// 创建内部回调函数队列
                outCalls = $.Callbacks(),// 创建外部回调函数队列
                slice = [].slice,
                inArray = function ( arr, i ) {// 是否在数组中
                    return arr.indexOf( i ) > -1;
                },
                // 列表数据
                list = [],
                // 是否有验证出错的情况
                hasError = function ( obj ) {
                    for ( var key in obj ) if ( obj[key] === false ) return true;
                },
                // 订阅
                appendCall = function ( target, name, fn ) {
                    if ( typeof target === "string" ) {
                        fn = name;
                        name = target;
                        target = calls;
                    }
                    target.add( function ( event, args ) {
                        if ( event === name ) fn.apply( null, args );
                    } );
                },
                // 通知
                notifyCall = function () {
                    var args, target = arguments[0], name = arguments[1];
                    if ( typeof target === "string" ) {
                        name = target;
                        args = slice.call( arguments, 1 );
                        target = calls;
                    } else {
                        args = slice.call( arguments, 2 );
                    }
                    target.fire( name, args );
                },
                // 创建第一个td
                createFirstTd = function ( index ) {
                    var td = $( "<td></td>" ),
                        id = "ipt" + index,
                        input = $( "<input type=\"checkbox\" id=\"" + id + "\">" ),
                        inputEl = input[0],
                        label = $( "<label for=\"" + id + "\"><i class=\"gou-i\"></i></label>" );
                    // 是否选中
                    appendCall( INPUT_HAS_CHECKED, function ( arr ) {
                        if ( inputEl.checked && list[index] ) arr.push( index );
                    } );
                    // 全选/取消
                    appendCall( INPUT_CHECK_ALL, function ( flag ) {
                        inputEl.checked = flag;
                    } );
                    // 取消选择
                    appendCall( INPUT_CANCEL_CHECK, function ( i ) {
                        if ( i === index || (Array.isArray( i ) && inArray( i, index )) ) inputEl.checked = false;
                    } );
                    return td.append( input, label );
                },
                // 提交数据操作
                submitSource = function ( index ) {
                    var data = {};
                    notifyCall( DATA_COLLECT, index, data );
                    // 如果验证通过
                    return !hasError( data ) ? notifyCall( outCalls, OUT_SUBMIT, data, list[index],
                        function ( flag ) { notifyCall( DATA_AFTER_SUBMIT, index, flag ) } ) : false;
                },
                // 获取验证
                getValid = function ( field ) {
                    return (valid[field] || noop);
                },
                // 创建其他td
                createTd = function ( index, field ) {
                    var td = $( "<td></td>" ), closeable = true,
                        span = $( "<span></span>" ),
                        icon = $( "<i class=\"icon2-bainji-1\"></i>" ),
                        input = $( "<input type=\"text\"  class=\"ac_input\">" ).hide(),
                        div = $( "<div style='color:red'></div>" ).hide();
                    // 获取当前行的原始数据
                    var getItem = function () {
                            return list[index];
                        },
                        // 更新原始数据
                        updateItem = function () {
                            var item = getItem();
                            if ( item ) item[field] = input.val();
                        },
                        // 更新dom数据
                        update = function () {
                            var value, item = getItem() || {};
                            value = item[field] || "";
                            input.val( value );
                            span.text( value ).prop( "title", value );
                            if ( !item.id ) showIpt();
                        },
                        // 输入错误时
                        onError = function ( msg ) {
                            input.css( "border-color", "red" );
                            div.show().text( msg );
                        },
                        // 输入正确时
                        onDone = function () {
                            input.css( "border-color", "" );
                            div.hide();
                        },
                        // 隐藏输入框
                        hideIpt = function () {
                            input.hide();
                            span.show();
                            icon.show();
                            closeable = true;
                        },
                        // 显示输入框
                        showIpt = function () {
                            input.show();
                            span.hide();
                            icon.hide();
                        };
                    // 收集数据
                    appendCall( DATA_COLLECT, function ( i, data ) {
                        if ( index === i ) {
                            var val = input.val(), check = getValid( field ),
                                res = check( val );
                            if ( res === true ) {
                                data[field] = val;
                                onDone();
                            } else {
                                data[field] = false;
                                onError( res );
                            }
                        }
                    } );
                    // 提交后的操作
                    appendCall( DATA_AFTER_SUBMIT, function ( i, flag ) {
                        // 如果提交成功
                        if ( i === index && flag ) {
                            updateItem();
                            update();
                            hideIpt();
                        }
                    } );
                    // 没有这条数据时
                    appendCall( DATA_EMPTY, function ( i ) {
                        if ( i === index ) {
                            update();
                            notifyCall( INPUT_CANCEL_CHECK, index );
                        }
                    } );
                    // 更新dom数据
                    appendCall( DATA_UPDATE, function ( i ) { if ( i === index ) update(); } );
                    // 编辑按钮
                    icon.click( showIpt );
                    // 输入框失去焦点
                    input.blur( function () {
                        var checked = submitSource( index );
                        closeable = (checked !== false);
                    } );
                    appendCall( CLICK_OUTSIDE, function ( el ) {
                        if ( closeable && (el !== icon[0]) && el !== input[0] ) hideIpt();
                    } );
                    // 第一次创建时，直接更新数据
                    update();
                    //
                    return td.append( span, icon, input, div );
                },
                // 创建tr
                createTr = function ( index ) {
                    var tr = $( "<tr></tr>" ).append( createFirstTd( index ) );
                    for ( var field, i = 0, l = fields.length; i < l; i++ ) {
                        field = fields[i];
                        tr.append( createTd( index, field ) );
                    }
                    tbody.append( tr );
                },
                // 刷新列表
                refreshTable = function () {
                    var ll = list.length, trs = tbody.find( "tr" ),
                        tl = trs.length, jqTr, item,
                        index = 0, length = ll > tl ? ll : tl;
                    for ( ; index < length; index++ ) {
                        jqTr = trs.eq( index );
                        item = list[index];
                        // 如果没有当前列，但有数据
                        if ( !jqTr.length && item ) {
                            createTr( index );
                            // 如果有当前列，但没有数据
                        } else if ( jqTr.length && !item ) {
                            notifyCall( DATA_EMPTY, index );
                            jqTr.hide();
                        } else {
                            notifyCall( DATA_UPDATE, index );
                            jqTr.show();
                        }
                    }
                },
                self = {
                    // 加载数据
                    load       : function ( data ) {
                        refreshTable( list = data || [] );
                    },
                    // 移除焦点事件
                    onBlur     : function ( fn ) {
                        appendCall( outCalls, OUT_SUBMIT, fn );
                    },
                    // 删除数据
                    deleteItem : function ( fn ) {
                        var inds = [], ep = [], select = [],
                            del = function ( sor ) {
                                var other = list.filter( function ( item ) { return !inArray( sor, item )} );
                                self.load( other );
                            };
                        notifyCall( INPUT_HAS_CHECKED, inds );
                        for ( var g, ind, i = 0, l = inds.length; i < l; i++ ) {
                            ind = inds[i];
                            g = list[ind];
                            if ( g.id ) {
                                select.push( g );
                            } else {
                                ep.push( g );
                            }
                        }
                        del( ep );
                        if ( select.length ) fn( select, function ( flag ) {
                            // 删除数据成功！
                            if ( flag ) del( select );
                        } );

                    },
                    // 添加一条新数据
                    addItem    : function () {
                        list = list || [];
                        list.push( {} );
                        self.load( list );
                    }
                };
            fields = fields || [];
            valid = valid || {};
            // 全选
            checkAllJq.on( "click", function () {
                notifyCall( INPUT_CHECK_ALL, this.checked );
            } );
            // 点击其他地方
            $( document.body ).on( 'click', function ( e ) {
                notifyCall( CLICK_OUTSIDE, e.target );
            } );
            return self;
        })( fields, valid );
        
        dataGrid.load(<?php echo $data;?> );
        
        dataGrid.onBlur( function ( data, orig, check ) {
            data.id = orig.id;
            console.log( data );
            console.log( orig );
            var hasId = !!orig.id;
            $.post( "/pick-user/add", data, function ( res ) {
                if ( res.code == 200 ) {
                    if ( !hasId ) orig.id = res.data;
                    check( true );
                } else {
                    alert( "操作失败！" );
                }
            }, "json" )
        } );
        $( "#add" ).on( "click", function () {
            dataGrid.addItem();
        } );
        
        $( "#delete" ).on( "click", function () {
            dataGrid.deleteItem( function ( list, check ) {
                var ids = [], i = 0, l = list.length;
                for ( ; i < l; i++ ) ids.push( list[i].id );
                if ( !ids.length ) return alert( '请选择要删除的数据！' );
                $.post( "/pick-user/del", { ids : ids.join( ',' ) }, function ( data ) {
                    check( data.code == 200 );
                }, "json" )
            } );
        } );
        
    } );


</script>
</html>

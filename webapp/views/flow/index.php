<?php
$this->title = '服务流程管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .set_status, .layui-layer-btn0{width: auto;}
    .layui-layer-btn a{height: 34px;line-height: 34px;}
</style>
<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">服务流程管理
     <span class="tip icon2-bangzhu" title="说明" data-container="body" data-trigger="hover" data-toggle="popover" data-placement="auto" data-html="true" data-content="如所提供的服务内容需要分阶段上门的，您可在此添加相应步骤，并在预约下次上门时进行选择，例：壁挂炉安装分为设备安装及调试两步。"></span>
    </span>
</div>
<section id="main-content">
    <div class="panel-body">
        <?php if (!empty($serviceType)): ?>
        <div class="information-list">
            <label class="name-title-lable" >服务类型：</label>
            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list bothEnds">
                <?php foreach ($serviceType as $key => $val) : ?>
                <?php if ($val['status'] == 1) : ?>
                <div class="radioLable">
                    <input type="radio" class="checkTypeId" name="typeId" value="<?=$key ?>" <?php if(substr($key,strlen(00)) == $key) : ?>checked="checked"<?php endif; ?> id="<?=$key ?>">
                    <label  for="<?=$key ?>"><?=$val['title'] ?></label>
                </div>
                <?php else: ?>
                <div class="radioLable">
                    <input type="radio" class="checkTypeId" name="typeId" value="<?=$key ?>" <?php if(substr($key,strlen(00)) == $key) : ?>checked="checked"<?php endif; ?> id="<?=$key ?>">
                    <label style="color: grey" for="<?=$key ?>"><?=$val['title'] ?></label>
                </div>
                <?php endif; ?>
                <?php endforeach; ?>
                <div class="city3_fix"></div>
                <div class="city3_fix"></div>
                <div class="city3_fix"></div>
            </div>
        </div>
        <div class="information-list">
            <label class="name-title-lable" >服务流程：</label>
            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list bothEnds">
                 <table  >
                    <thead>
                        <tr>
                            <th style="width: 250px">服务流程名称</th>
                            <th style="width: 250px">状态</th>
                            <?php if (!empty($flag) && $flag== 1) : ?>
                            <th style="width: 150px">操作</th>
                            <?php endif; ?>
                        </tr>
                   </thead>
                   <tbody>
                       <?php if (!empty($serviceFlow)): ?>
                       <?php foreach ($serviceFlow as $k => $val): ?>
                        <tr>
                            <td><?=$val['title'] ?></td>
                            <td><?= $val['status'] == 1 ? '已启用' : '已禁用' ?></td>
                            <?php if (!empty($flag) && $flag== 1) : ?>
                            <td>
                                <?php if ($k != 0 && !empty($flag) && $flag== 1 && !empty($disbled) && $disbled != 1): ?>
                                <?php if ($val['status'] == 1) : ?>
                                    <?php if(in_array('/flow/update',$selfRoles)): ?>
                                      <a href="javascript:" style="color:#3333ff" onclick="revise('<?=$k?>','<?=$val['title']?>');">修改</a>
                                    <?php endif; ?>
                                    <?php if(in_array('/flow/disable',$selfRoles)): ?>
                                    <a href="javascript:" style="color:#3333ff" onclick="disBranf(<?=$k;?>,2)">禁用</a>
                                     <?php endif; ?>
                                <?php else: ?>
                                    <?php if(in_array('/flow/enable',$selfRoles)): ?>
                                     <a href="javascript:" style="color:#3333ff" onclick="enaBrand(<?=$k;?>,1)" >启用</a>
                                     <?php endif; ?>
                                <?php endif; ?>
                                <?php endif;?>
                            </td>
                            <?php endif; ?>
                        </tr>
                        <?php endforeach;?>
                        <?php endif; ?>
                   </tbody>
               </table>
            </div>
        </div>
        <?php if (!empty($flag) && $flag== 1) : ?>
        <div class="information-list">
            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10  addinput-list bothEnds" style="padding-top: 20px">
                <?php if(in_array('/flow/add',$selfRoles)): ?>
                <button onclick="revise();" class="btn btn-orange mgr20 revise">添加</button>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
        <?php else: ?>
            <div class="no-data-page">
                <div>
                    <div class="kbtp"></div>
                    <p class="no-data-zi">您还没有添加过服务类型，请先添加服务类型再设置</p>
                    <?php if (!empty($flag) && $flag== 1) : ?>
                    <span class="btn btn-success" onclick="addParentIframe ()">添加服务类型</span>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
<script type="text/javascript">
window.onload = function(){
    var  heightKs = ($(window).height())-265+'px';
    $('.no-data-page').height(heightKs);
};
$(window).resize(function(){
        var heightKs = ($(window).height())-265+'px';
        $('.no-data-page').height(heightKs);
    });

function addParentIframe (argument) {
    layer.open({
        type: 2,
        title: '请选择服务类类型',
        area: ['700px', '450px'],
        fixed: false, //不固定
        maxmin: false,
        scrollbar: false,
        content: ['/type/add', 'no'],
        end:function(){
            location.reload();
        }
    });
}
$(".tip").popover(); 
    $(".checkTypeId").click(function(){
        var tdContent = '';
        var typeId = $(":checked").val();
        $.ajax({
            url:'/flow/ajax-index',
            type:'GET',
            data:{typeId:typeId},
            dataType:'json',
            success:function(json){
                if(json.serviceFlow === undefined){
                     $('tbody').html('');
                }else{
                    var data = json.serviceFlow;
                    var flag = json.flag;
                    for (item in data)
                    {
                        tdContent += '<tr><td>'+data[item].title+'</td>';
                        
                        if (data[item].status ==1) {
                            tdContent += '<td>已启用</td>';
                        }else{
                            tdContent += '<td>已禁用</td>';
                        }

                        if (item != 0 && flag == 1 && json.disbled != 1)
                        {
                            if (data[item].status == 1)
                            {
                                var flowName = data[item].title;

                                tdContent += '<td>\n' ;
                                <?php if(in_array('/flow/update',$selfRoles)): ?>
                                tdContent +='<a href="javascript:" style="color:#3333ff" onclick="revise('+item+','+"'"+flowName+'\');" >修改</a>\n' ;
                                <?php endif; ?>
                                <?php if(in_array('/flow/disable',$selfRoles)): ?>
                                tdContent +='<a href="javascript:" style="color:#3333ff" onclick="disBranf('+item+',2);" >禁用</a>\n' ;
                                <?php endif; ?>
                                tdContent +='</td>';
                            }
                            else
                            {
                                <?php if(in_array('/flow/enable',$selfRoles)): ?>
                                    tdContent += '<td>\n' +
                                         '<a href="javascript:" style="color:#3333ff" onclick="enaBrand('+item+',1);" >启用</a>\n' +
                                    '</td>';
                                <?php endif;?>
                            }
                        }
                        else
                        {
                            flag==2?'':tdContent += '<td></td>';
                        }

                        tdContent += '</tr>';
                    }
                  
                    $('tbody').html(tdContent);
                }
            }
        });
    });
    
    //编辑和添加
   function revise(flowId,title) {


       //获取选中的服务类型
       var typeId = $(":checked").val();
       if (flowId == undefined) {
           flowId = 0;
       }
       //查询状态是否被禁用
       $.ajax({
           url:'/flow/get-status',
           type:'POST',
           data:{typeId:typeId},
           dataType:'json',
           success:function(json){
               if (json.code != 200) {
                   alert(json.message);
                   return false;
               }else{
                   //判断服务流程名称
                   if (title == undefined){
                       title = '';
                   }
                   //打开添加或编辑页面
                   layer.open({
                       title:false,
                       closeBtn: 0,
                       type: 1,
                       skin: 'yourclass', //加上边框
                       area: ['350px', '200px'], //宽高
                       content: '<h4 style="padding: 10px 20px;background: #F1F2F7;margin-top: 0;text-align:left;">请输入服务流程名称</h4><div style="width: 300px;margin:0 auto;text-align: center;;padding-top:20px">\n' +
                       '<input type="hidden" id="typeId" class="form-control" value="'+typeId+'">\n' +
                       '<input type="hidden" id="flowId" class="form-control" value="'+flowId+'">\n' +
                       '<input type="text" id="flowName" maxlength="10" placeholder="不超过10个字" class="form-control" value="'+title+'">\n' +
                       '<div style="margin-top:30px">\n' +
                       '<p>\n' +
                       '<button class="btn bg-f7 mgr20" id="cancel" onClick="cancel()">取消</button>\n' +
                       '<button class="btn btn-orange" id="preservation" onClick="preservation()">保存</button>\n' +
                       '</p>\n' +
                       '</div>\n' +
                       '</div>',

                   });
               }
           }
       });
       
    };
    //取消
    function cancel(){
        if(layer.index > 1) {
            layer.close(layer.index-1);
        }
        layer.close(layer.index);
    }
    //保存
    function  preservation() {
        var typeId = $('#typeId').val();
        var flowId = $("#flowId").val();
        var flowName = $('#flowName').val();
        
        if (flowName == '') {
            alert('请输入服务流程名称');
            return false;
        }
        
        $.ajax({
            url:'/flow/add',
            type:'POST',
            data:{typeId:typeId,flowId:flowId,flowName:flowName},
            dataType:'json',
            success:function(data){
                
                if (data.code == 200) {
                        alert(data.message, function(index){
                            location='/flow/index?typeId='+typeId;
                        });
                }else{
                    alert(data.message);
                }
            }
        });
    }

    /**
     * 禁用
     * @param id
     * @param status
     */
    function disBranf(id,status) {
        layer.confirm('您确定要禁用该服务流程吗？禁用后此服务类型下该流程将不能使用，但不影响未完成和已完成订单数据。', {
            area:['330px','215px'],
            btn: ['确定','取消'] //按钮
        }, function(){
            ajaxPost(id,status);
        }, function(){
            layer.close();
        });
    }
    
    /**
     *  启用
     * @param id
     * @param status
     */
    function enaBrand(id,status) {
        layer.confirm('您确定要启用该服务流程吗？启用后此服务类型下该流程将再次使用，但不影响未完成和已完成订单数据。', {
            area:['330px','215px'],
            btn: ['确定','取消'] //按钮
        }, function(){
            ajaxPost(id,status);
        }, function(){
            layer.close();
        });
    }
    
    function ajaxPost(id,status){
        var typeId = $(":checked").val();
        var url = '/flow/change-status';
        $.ajax({
            type: 'POST',
            url: url,
            data: {'id':id,'status':status},
            dataType: 'json',
            success: function(data){
                if (data.code == 200) {

                    parent.layer.alert(data.message, function(index){
                        parent.location='/flow/index?typeId='+typeId;
                    });
                }else{
                    alert(data.message);
                }
            }
        });
    }

</script>
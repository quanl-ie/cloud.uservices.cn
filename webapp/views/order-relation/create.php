<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use Webmozart\Assert\Assert;
//use kartik\datetime\DateTimePicker;
//use kartik\date\DatePicker;


$this->title = '创建新订单';
$this->params['breadcrumbs'][] = ['label' => '订单管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.redactor-editor{height:300px;}
div.required label:before {
    content: " *";
    color: red;
}
.date-pick{display:block};
</style>
<!-- 时间控件样式 -->
<style type="text/css">
	<style type="text/css">
	li{
		list-style-type: none;
	}
    /*预约日期的选择*/
    .datexz-wap li,.datexz-wap ul{list-style-type: none;padding: 0px;margin: 0px;}
    .datexz-wap a:hover,.datexz-wap a:link,.datexz-wap a:active,.datexz-wap a:visited{color: #000000}
    .datexz-wap{width: 95%;border:1px solid #ccc; 
        float: left;
        position:absolute;
    	display: none;
        top:45px;
        left:2.5%;
        z-index: 5;

    }
    .datexz-wap .datexz-box{position: relative;display: table;background: #fff;width: 100%;text-align: center;z-index:111;}
    .datexz-box .dt-title-wap{height: 34px;line-height: 34px;font-size: 16px;}
    .datexz-box .dt-title-wap img{width: 10px;height: 10px; position: absolute;right: 10px;top:10px;}

    .warp-date{overflow: hidden;overflow-x:scroll;width:100%;background: #f8f8f8;-webkit-overflow-scrolling : touch;}
    .warp-date::-webkit-scrollbar{height:0px;}
    .warp-date ul {display:flex;height: 100%;}

    .warp-date ul li.acti{color: #2693ff;}


    .datexz-box .warp-date li{width: 20%;flex-shrink:0;text-align: center;cursor: pointer;}
    .datexz-box .warp-date li h3{font-size: 12px;color: #111;height:36px;line-height: 36px;font-weight: normal;}
    .datexz-box .warp-date ul .acti h3{color: #2693ff;}

    .datexz-wap .tdpr-wap{width: 94%;height: 32px;line-height: 32px;
        border: 1px solid #999;border-radius: 2px;font-size: 16px;text-align: center;margin: 10px auto}
    .datexz-wap .tdpr-wap a{display: block;}
    .datexz-wap .tdpr-wap.rotate{border-color: #ffa91f;}
    .datexz-wap .tdpr-wap.rotate a{color: #ffa91f;}
    .tdpr-wap.no-sele{border-color: #ccc;}
    .tdpr-wap.no-sele a{color: #ccc;}


    .datexz-wap .shijian-wap{margin-top: 20px;width: 94%;display: table;margin: 0 auto}
    .datexz-wap .shijian-wap ul{width: 100%;display: table;}
    .shijian-wap ul li{float:left; width: 23%;height: 28px;border: 1px solid #999;margin-right: 7px;border-radius:3px;line-height: 28px;text-align: center;position: relative;margin-bottom: 5px;}
    .shijian-wap ul li:nth-child(4n+0){margin-right: 0;}
    .shijian-wap ul .no-sele a{color: #ccc;}
    .shijian-wap ul .no-sele{border-color:#ccc}
    .shijian-wap ul li img{width: 17px;height: 16px;position: absolute;right: 0;bottom: 0;
        display: none;}
    .shijian-wap ul li.acti{border-color: #2693ff;display: block;}
    .shijian-wap ul li.acti img{display: block;}

    .shijian-wap ul li.acti a{color: #2693ff;}

    .datexz-box .buton{width: 94%;height: 40px;background: #2693ff;border-radius: 3px;line-height: 40px;margin: 10px auto}
    .datexz-box .buton a{display: block;color: #fff;}


    .tdpr-wap {
        position: relative;
    }
    .tdpr-wap img {
        width: 17px;
        height: 16px;
        position: absolute;
        right: 0;
        bottom: 0;
        display: none;
    }
    .rotate img {
        display: block;
    }
    .types-list ul .remarks {
        height: auto;
        display: flex;
        min-height: .85rem;
    }
    .remarks p {
        flex-shrink: 0;
    }
    .remarks_text {
        margin: 0;
        width: 100%;
        display: flex ;
        align-items: center;
        padding-right: .2rem;
        padding-bottom: .5rem;
        position: relative;
        display: none;
    }
    .remarks_text textarea {
        margin-top: .24rem;
        width: 100%;
        line-height: 1.5;
        border: none;
        /*border: 1px blue solid;*/
    }

    /**/
    #note{
        padding-bottom: 20px;
         overflow-y: hidden;
        text-align: left;
        outline: none;

    }
    #order_make_time_type{
        display: none;
    }
    .multi-img-details .multi-item em{
        right: 8px;top: 12px;
    }
</style>



<!--引入CSS-->
<?=Html::cssFile('@web/css/bootstrapValidator.min.css')?>
<?=Html::jsFile('@web/js/bootstrapValidator.min.js')?>
<?=Html::jsFile('@web/js/bootstrapValidator.js')?>
<?=Html::cssFile('@web/webuploader/webuploader.css')?>
<!--引入JS-->
<?=Html::jsFile('@web/webuploader/webuploader.js')?>
<div class="jn-title-box">
    <span class="jn-title"><?= Html::encode($this->title) ?></span>
</div>


<section id="main-content">
    <div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                        'options' => [
                                'class' => 'form-horizontal',
                                'id' => 'form2',
                                'name' => 'form2',
                                'enctype' => 'multipart/form-data',
                                'enableClientValidation'=>true,
                        ],
                        'fieldConfig' => [
                                'template' => "{label}<span class=\"c_red\">{hint}</span>\n<div class=\"col-xs-6\">{input}{error}</div>",
                                'labelOptions' => ['class' => 'col-xs-2 control-label text-right'],
                        ],
                ]); ?>
                <input name="_csrf" type="hidden" id="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                <div class="form-group field-order-class_id required">
                    <label class="col-xs-2 control-label text-right" for="order-class_s_id">产品类型：</label>
                    <span class="c_red"></span>
                    <!-- <div class="col-xs-6"> -->
                        <div class="col-xs-2">
                            <select id="order-class_id" class="form-control" name="order_class_id" value="" aria-required="true">
                            <option value="">请选择</option>
                            <?php foreach($data['class'] as $key => $val){
                                    echo '<option value="'.$key.'"';echo '>'.$val.'</option>';
                            }?>
                            </select>
                            <p class="help-block help-block-error"></p>
                        </div>
                        <div id="class_s_id" style="display: none">
                            <label class="col-xs-1 control-label text-right" for="order-class_s_id"  style="width: 110px">产品分类：</label>
                            <span class="c_red"></span>
                            <div class="col-xs-2">
                            <select id="order-class_s_id" class="form-control" name="order_class_s_id" aria-required="true">
                            <option value="">请选择</option>
                            </select><p class="help-block help-block-error"></p></div>
                        </div>
                    <!-- </div> -->
                    
                </div>
                <div class="form-group field-order-type_id required">
                    <label class="col-xs-2 control-label text-right" for="order-type_id">服务类型：</label><span class="c_red"></span>
                    <div class="col-xs-6"><select id="order-type_id" class="form-control" name="order_type_id" aria-required="true">
                    <option value="">请选择</option>
                    </select><p class="help-block help-block-error"></p></div>
                </div>
                <div class="form-group field-order-brand_id required">
                    <label class="col-xs-2 control-label text-right" for="order-brand_id">品牌：</label><span class="c_red"></span>
                    <div class="col-xs-6"><select id="order-brand_id" class="form-control" name="order_brand_id" aria-required="true">
                    <option value="">请选择</option>
                    </select><p class="help-block help-block-error"></p></div>
                </div>
                
               <div class="form-group field-order-make_time" style="line-height:35px">
                    <!-- <label class="col-xs-2 control-label text-right" for="order-make_time"><span class="c_red">*</span>服务时间：</label>
                    <span class="c_red"></span>
                    <div class="col-xs-6 field-s_time" style="height: 51px;line-height:51px;">
                        <label><input class="s_radio" type="radio" name="order_make_time_type" value="1" >立即上门</label>
                        <label style="float:left">&nbsp;&nbsp;<input class="s_radio" type="radio" name="order_make_time_type" value="2" checked>预约时间</label>
                    </div>
                    <div class="col-xs-2">
                        <div id="order-make_time-datetime" class="input-group date date-pick">
                        
                        <div id="w0-datetime" class="input-group date">
                            <span class="input-group-addon" title="日期和时间">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                            <input type="text" id="w0" class="form-control" name="order_make_time" value="" readonly placeholder="" data-krajee-datetimepicker="">
                        </div>
                    </div>
                    <p class="help-block help-block-error"></p>
                    </div> -->
                    <!-- 时间插件开始 -->
                    <div class="formbox " style="position: relative" id="mtdiv">
                    <div class="col-xs-2 control-label text-right">
                        <label><span class="c_red">*</span>预约时间：</label>
                    </div>
                    <div class="col-xs-6 control-label text-right" style="position: relative;">
                        <input name="order_make_time_type" value="1" id="order_make_time_type">
                        <input name="make_time" id="make_time" class="Wdate bzinput form-control" type="text"  datatype="*" nullmsg="请选择服务时间" value="" onClick="" readonly="readonly">
                        <div class="datexz-wap center fixed">
                        <div class="datexz-box">
                            <div class="dt-title-wap">选择服务时间<img class="fr rogate" src="<?php echo Url::to('@web/js/WdatePicker/p27.png');?>" alt=""></div>
                            <div class="warp-date">
        
                                <ul>
                                    <li class="today acti">
                                        <h3>今天<br>07/10</h3>
                                        <!--<p>可预约</p>-->
                                    </li>
                                    <li>
                                        <h3>明天<br>07/11</h3>
                                        <!--<p>可预约</p>-->
                                    </li>
                                    <li>
                                        <h3>后天<br>07/12</h3>
                                        <!--<p>可预约</p>-->
                                    </li>
                                    <li>
                                        <h3>周四<br>07/13</h3>
                                        <!--<p>可预约</p>-->
                                    </li>
                                    <li>
                                        <h3>周五<br>07/14</h3>
                                        <!--<p>可预约</p>-->
                                    </li>
                                    <li>
                                        <h3>周六<br>07/15</h3>
                                        <!--<p>可预约</p>-->
                                    </li>
                                    <li>
                                        <h3>周日<br>07/16</h3>
                                        <!--<p>可预约</p>-->
                                    </li>
                                </ul>
                            </div>
                            <div class="tdpr-wap">
                                <a href="javascript:void(0);">2小时立即上门</a>
                                <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                            </div>
                            <div class="shijian-wap">
                                <ul>
                                    <li class="no-sele">
                                        <a href="javascript:void(0);">08:00</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li class="no-sele">
                                        <a href="javascript:void(0);">08:30</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li class="no-sele">
                                        <a href="javascript:void(0);">09:00</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li class="">
                                        <a href="javascript:void(0);">09:30</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">10:00</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">10:30</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">11:00</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">11:30</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">12:00</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">13:00</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">13:30</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">14:00</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">14:30</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">15:00</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">15:30</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">16:00</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">16:30</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">17:00</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">17:30</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">18:00</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">18:30</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">19:00</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">19:30</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">20:00</a>
                                        <img src="<?php echo Url::to('@web/js/WdatePicker/p28.png');?>" alt="">
                                    </li>
                                </ul>
                            </div>
                            <div class="buton rediuset" style="position: relative;">
                                <a class="yes-time-a" href="javascript:void(0);">确定</a>
                                <a class="no-time-a" href="javascript:">确定</a>
                            </div>
                        </div>
                    </div>
                    </div>
                    <!--WdatePicker({el:this,dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'%y-%M-%d',minTime:'09:00:00',maxTime:'18:00:00'})-->
                    <!--此处的日期时间插件设置minDate，实现只能选当天以后的日期；minTime为上班时间，maxTime为下班时间-->
                    <!--时间弹框-->
                    
                    <p class="help-block help-block-error"></p>
                </div>
               <!-- 时间控件结束 -->  
                    
                    
                    
                    
                    
                    
                </div> 
                <div class="form-group ">
                <label class="col-xs-2 control-label text-right" for="order-address">上门费：</label>
                <div class="col-xs-6">¥<span class="brand_price">0.00</span>
                    <input type="hidden" name="total_price"  id="total_price" >
                </div>
                </div>
                <div class="form-group ">
                <label class="col-xs-2 control-label text-right" for="order-address">支付方式：</label>
                <div class="col-xs-6">
                    <span>线下支付</span>
                    <input type="text" name="order_pay_type" value="3" hidden>
                </div>
                </div>
                <div class="form-group field-order-note">
                    <label class="col-xs-2 control-label text-right" for="order-note">故障描述：</label><span class="c_red"></span>
                    <div class="col-xs-6">
                    <input type="text" id="order-note" class="form-control" name="order_note" maxlength="255" placeholder ="请输入详细的故障信息，以便技师准备配件工具" style="height:100px">
                    <p class="help-block help-block-error"></p>
                    </div>
                </div>
                <?= $form->field($model, 'image')->widget('manks\FileInput', [
                    'clientOptions'=>[
                        'server' => Url::to('../upload/upload?source=manu_order'),
                        'pick'=>[
                            'multiple'=>true,
                        ],
                        'fileNumLimit'=>'5',
                        'duplicate'=>true,
                        'fileSingleSizeLimit'=>'2097152', //限制上传大小为2M
                    ],
                ]); ?>
                <div class="form-group field-order-name">
                    <label class="col-xs-2 control-label text-right" for="order-name"><span class="c_red">*</span>业主姓名：</label>
                    <span class="c_red"></span>
                    <div class="col-xs-6">
                        <input type="text" id="order-name" class="form-control" name="order_name" maxlength="255" placeholder ="不超过10个字">
                        <p class="help-block help-block-error"></p>
                    </div>
                </div>
                <div class="form-group field-order-tel">
                    <label class="col-xs-2 control-label text-right" for="order-tel"><span class="c_red">*</span>业主电话：</label>
                    <span class="c_red"></span>
                    <div class="col-xs-6">
                        <input type="text" id="order-tel" class="form-control" name="order_tel" maxlength="14" placeholder ="请输入11位数字的手机号">
                        <p class="help-block help-block-error"></p>
                    </div>
                </div> 
              
                <div class="form-group field-order-province required">
                    <label class="col-xs-2 control-label text-right" for="order-province">所在区域：</label>
                    <span class="c_red"></span>
                    <div class="col-xs-2">
                        <select id="order-province" class="form-control" name="order_province_id" value="" aria-required="true">
                        <option value="">请选择省</option>
                        <?php foreach($data['province'] as $val){
                                echo '<option value="'.$val['region_id'].'"';echo '>'.$val['region_name'].'</option>';
                        }?>
                        </select>
                    </div>
                    <div class="col-xs-2">
                        <select id="order-city" class="form-control" name="order_city" value="" aria-required="true">
                        <option value="">请选择市</option>
                        </select>
                    </div>
                    <div class="col-xs-2">
                        <select id="order-district" class="form-control" name="order_district" value="" aria-required="true">
                        <option value="">请选择区</option>
                        </select>
                    </div>
                </div>
                <div class="form-group field-order-address">
                    <label class="col-xs-2 control-label text-right" for="order-address"><span class="c_red">*</span>详细地址：</label>
                    <span class="c_red"></span>
                    <div class="col-xs-6">
                        <input type="text" id="order-address" class="form-control" name="order_address" maxlength="255" placeholder ="请输入业主的详细住址，街道、楼牌号等">
                        <p class="help-block help-block-error"></p>
                    </div>
                </div>
                <input type="hidden" id="lat" class="form-control" name="lat" >
                <input type="hidden" id="lon" class="form-control" name="lon" >
                <div class="form-group">
                        <label class="col-xs-2"> </label>
                        <div class="col-xs-6 text-right">
                            <input type="submit" class="btn btn-success mgr20 " style=" color:#FFF;background: #2693ff;border: 1px solid #2693ff" value="提交"/>
                            <a type="button" class="btn btn-green-line-min btnClose" href="/order-relation/index">取消</a>
                        </div>
                 </div>
                <?php $form = ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    </div>
</section>

<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=bbcdabda1427179d77bb95fac7b2b902&plugin=AMap.Geocoder"></script>
<script type="text/javascript">
    //下单预约时间
//     $("#w0-datetime").datetimepicker({
//     	language:'zh-CN',
//         format: 'yyyy-mm-dd HH:ii',
//         autoclose: true,
//        startDate: '<?php //echo $data['start_time']?>',
//        endDate :   '<?php //echo date('Y-m-d 20:00:00', strtotime('+7 days'))?>',
//         todayHighlight: true,
//         minuteStep: 30,    //设置时间间隔
//         startView : 2,     //月视图
//         minView : 0,       //提供的最精确的时间选择视图
//         startView : 1, 
        
//     });
    //根据产品类型父级id获取子级别分类数据
	$("#order-class_id").change(function() {
		var p_id = $("#order-class_id").val();
		if(p_id != ''){
			$.post("/order-relation/get-class", { "parentId": p_id }, function (data) {
				if(data.length == 0){
					$("#class_s_id").hide();
				}else {
					$("#class_s_id").show();
					$("#order-class_s_id").html(data);
				}
		        
		    })
		}
	});
	//根据产品类型父级id获取服务类型数据
	$("#order-class_id").change(function() {
		var p_id = $("#order-class_id").val();
		if(p_id != ''){
			$.post("/order-relation/get-class-type", { "class_id": p_id }, function (data) {
				$("#order-type_id").html(data);
		    })
		}
	});
	//根据产品类型父级id获取品牌数据
	$("#order-class_id").change(function() {
		var p_id = $("#order-class_id").val();
		if(p_id != ''){
			$.post("/order-relation/get-brand-class", { "class_id": p_id }, function (data) {
				$("#order-brand_id").html(data);
		    })
		}
	});
	//根据开通城市父级id获取市级数据
	$("#order-province").change(function() {
		var p_id = $("#order-province").val();
		if(p_id != ''){
			$.post("/order-relation/get-city", { "province": p_id }, function (data) {
		        $("#order-city").html(data);
		        $("#order-district").html("<option value=''>区</option>");
		    })
		}
		
	});
	//根据城市获取区县
	$("#order-city").change(function() {
		var p_id = $("#order-city").val();
		if(p_id != ''){
			$.post("/order-relation/get-district", { "city_id": p_id }, function (data) {
		        $("#order-district").html(data);
		    })
		}
	});
	//根据品牌id获取上门费用
	$("#order-brand_id").change(function() {
		var brand_id = $("#order-brand_id").val();
		if(brand_id != ''){
			$.post("/order-relation/get-brand-price", { "brand_id": brand_id }, function (data) {
		        $(".brand_price").text(data);
                $("#total_price").val(data);
		    })
		}
	});
	//获取选中的服务时间
	$(".s_radio").click(function() {
		var s_radio = $("input[name='order_make_time_type']:checked").val();
		if(s_radio == '1'){
			$(".date-pick").hide();
		}
		if(s_radio == '2'){
			$(".date-pick").show();
		}
	});
	
	
	//});
	//验证必填项
	$(function () {
        $('form').bootstrapValidator({
　　　　　　　　message: 'This value is not valid',
　　　　　　　　//excluded : [':disabled'],//[':disabled', ':hidden', ':not(:visible)'] //设置隐藏组件可验证
            fields: {
            	order_class_id: {
                    validators: {
                        notEmpty: {
                            message: '请选择产品类型'
                        }
                    }
                },
                order_class_s_id: {
                    validators: {
                        notEmpty: {
                            message: '请选择产品分类'
                        }
                    }
                },
                order_type_id: {
                    validators: {
                        notEmpty: {
                            message: '请选择服务类型'
                        }
                    }
                },
                order_brand_id: {
                    validators: {
                        notEmpty: {
                            message: '请选择品牌'
                        }
                    }
                },
                make_time: {
                	trigger:"change",
                	validators: {
                        notEmpty: {
                            message: '预约时间不能为空'
                        }
                    }
                },
            	order_name: {
                    message: '用户名验证失败',
                    validators: {
                        notEmpty: {
                            message: '业主姓名不能为空'
                        }
                    }
                },
                order_tel: {
                    validators: {
                        notEmpty: {
                            message: '业主电话不能为空'
                        },
                		numeric: {message: '请输入正确的电话号码'}
                    }
                },
                order_address: {
                    //message: '用户名验证失败',
                    validators: {
                        notEmpty: {
                            message: '详细地址不能为空'
                        }
                    }
                },
                order_province_id: {
                	validators: {
                        notEmpty: {
                            message: '请选择所在区域'
                        }
                    }
                },
                order_city: {
                	validators: {
                        notEmpty: {
                            message: '请选择所在城市'
                        }
                    }
                },
                order_district:{
                	validators: {
                        notEmpty: {
                            message: '请选择所在区县'
                        }
                    }
                  }
            }
        });
        //event.preventDefault()
    });
//创建高德
//var map = new AMap.Map("container", {
//    resizeEnable: true
//});
//保存地址
$('#order-address').blur(function(){
    addsave();
})
function addsave(){
    var province = $('#order-province').val();
    var city = $('#order-city').val();
    var district = $('#order-district').val();
    var address_new = $('#order-address').val();
   
    //地理编码,返回地理编码结果
    var province2 = $("#order-province").find('option:selected').html();
    var city2 = $("#order-city").find('option:selected').html();
    var district2 = $("#order-district").find('option:selected').html();
    var address_all = province2 + city2 + district2 + address_new;
    var geocoder = new AMap.Geocoder({
        city: "010", //城市，默认：“全国”
        radius: 1000 //范围，默认：500
    });
    geocoder.getLocation(address_all, function(status, result) {
        if (status === 'complete' && result.info === 'OK') {
            var arr = result.geocodes[0].location;
            $('#lat').val(arr.lat);
            $('#lon').val(arr.lng);
        }
    });
}
</script>
<script type="text/javascript">
$(document).on('click','.btn-s',function(){
	//var s = document.getElementsByClass("field-goodsmanagement-goods_pics");
	var images = $('.field-goodsmanagement-goods_pics img');
	if(images.length >= 5) {
		alert('超出数量限制');
	}
});
</script>
<!-- 时间插件/ -->
<?=Html::jsFile('@web/js/WdatePicker/WdatePicker.js')?>
<?=Html::jsFile('@web/js/WdatePicker/time.js')?>
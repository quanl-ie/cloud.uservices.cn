<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\Order;
?>

<div class="jn-title-box no-margin-bottom">
    <span class="jn-title">订单等待页</span>
</div>
<div class="brand-qualification-view">
    <section id="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                <?php if(!empty($data)){?>
                    <?php if($data['dif_time'] == 0){?>
                        <div class="panel-body" style="margin:0 auto;text-align:center">
                            <div class="">
                                <img id="img_status" alt="" src="<?php echo Url::to('@web/images/error.png');?>">
                            </div>
                            <p><span id="msg">订单异常111</span></p>
                        </div>
                    <?php }elseif($data['order_status'] == 0) {?>
                        <div class="panel-body" style="margin:0 auto;text-align:center">
                            <div class="">
                                <img id="img_status" alt="" src="<?php echo Url::to('@web/images/error.png');?>">
                            </div>
                            <p><span id="msg">订单已取消</span></p>
                        </div>
                    <?php }else {?>
                        <div class="panel-body" style="margin:0 auto;text-align:center">
                            <div class="">
                                <img id="img_status" alt="" src="<?php echo Url::to('@web/images/waiting.png');?>">
                            </div>
                            <p><div id="show_div"><span id="show_time"><?php echo isset($data['show_last_time'])?$data['show_last_time']:'10:00';?></span></div></p>
                            <p><span id="msg">等待技师接单中</span></p>
                            <p><span id="msg1">倒计时结束后，如没有技师接单，系统将自动为您取消订单</span>
                            <br><span id="link"><a id="a_link" href="/order-relation/remove?id=<?php echo $data['order_id']?>">取消订单</a></span>
                            <br><span id="show_div2">温馨提示：您可以离开此页面进行其他操作，如订单有变化，系统将会第一时间通知您</span></p>
                        </div>
                    <?php }?>
                <?php }else {?>
                    <div class="panel-body" style="margin:0 auto;text-align:center">
                        <div class="">
                            <img id="img_status" alt="" src="<?php echo Url::to('@web/images/error.png');?>">
                        </div>
                        <p><div><span>订单异常</span></div></p>
                    </div>
                <?php }?>
                </div>
            </div>
        </div>
    </section>
</div>
<script  type="text/javascript" >
    var timer;
    var i;
    //var tt = 30*1000;  //程序执行的超时时间
    var tt = <?php echo isset($data['dif_time'])?$data['dif_time']:'0'?>;   //程序执行的超时时间
    	tt = tt*1000;
    //var t = 30;        //页面显示的倒计时
    var t = <?php echo isset($data['dif_time'])?$data['dif_time']:'0'?>;    //页面显示的倒计时
    var set_time = 60*1000;      				   //设定的执行间隔时间，60秒
    window.onload = function(){
    	stop(t);
    	timer = window.setInterval("timelyFun()",set_time);  //每间隔多长时间执行一次 
    	//页面显示倒计时执行函数
    	var minute = 0; //分
    	var second = 0; //秒
    	var s_t = 0;
    	i = window.setInterval(function() {
    		stop(t);
    		t--;
    		//将传入的秒计算为 分：秒
        	if(t>60){
        		minute = parseInt(t/60);
        		second = parseInt(t%60);
            }
        	var result = ""+parseInt(second);
        	var s = result < 10 ? '0' + result: result;
        	if(minute > 0) {
        		result = "0"+parseInt(minute)+":"+ s;
        		document.getElementById("show_time").innerHTML = result;
        	}
        	if(t == 60){
        		result = "01:00";
        		document.getElementById("show_time").innerHTML = result;
            }
        	if(t < 60){
        		if(t<10){
        			t="0"+t;
        		}
        		result = "00:"+ t;
        		document.getElementById("show_time").innerHTML = result;
            } 
    		if(t<=0) {
    			window.clearInterval(i);
    		}
    		//document.getElementById("show_time").innerHTML = t;
    	},1000);
    	
    }
    //执行查询执行函数
    function timelyFun(){
    	tt-=set_time;
        var   	id   = <?php echo isset($data['order_id'])?$data['order_id']:'0'?>;
    	$.ajax({
            type:"POST",
            dataType:"json",
            url:"/order-relation/get-data",
            data:{order_id:id}, 
            success:function(data,textStatus){
                //从服务器得到数据，显示数据并继续查询
                if(data.success == "1"){
                	$("#img_status").attr('src', src="<?php echo Url::to('@web/images/ok.png');?>"); //显示图片
                    //$("#msg").append("<br>"+data.name);
                    $("#msg").text(data.name);
                    $("#msg1").text("技师姓名："+data.info.name+"技师电话："+data.info.tel);
                    $("#a_link").attr("href","/order-relation/view?id="+data.info.order_id); 
                    $("#a_link").text("点击查看订单详情");
                    document.getElementById("show_div").style.display="none";
                    document.getElementById("show_div2").style.display="none";
                    window.clearInterval(timer);
                    window.clearInterval(i);
                }
                //未从服务器得到数据，继续查询
                if(data.success == "0"){
                	//$("#msg").append("<br>[无数据]"+data.text+data.name);
                	//document.getElementById("show_div").style.display="none";
                }
                if(data.success == "0" && tt == 0){
                	$("#img_status").attr('src', src="<?php echo Url::to('@web/images/error.png');?>"); //显示图片
                    $("#msg").text("无技师接单");
                    $("#msg1").text("非常抱歉，您为"+data.info+"业主提交的订单无技师接单，系统已为您取消此订单。如有疑问请联系客服400-6377-637");
                    window.clearInterval(timer);
                    document.getElementById("show_div").style.display="none";
                    document.getElementById("show_div2").style.display="none";
                    document.getElementById("link").style.display="none";
                    
                }
                if(data == ''){
                	$("#img_status").attr('src', src="<?php echo Url::to('@web/images/error.png');?>"); //显示图片
                	$("#msg").text("订单异常");
                	window.clearInterval(timer);
                    window.clearInterval(i);
                    document.getElementById("msg1").style.display="none";
                    document.getElementById("show_div").style.display="none";
                    document.getElementById("show_div2").style.display="none";
                    document.getElementById("link").style.display="none";
               }
            }
        });    	
    };

    function stop(t) {
		if(t <= 0) {
			$("#img_status").attr('src', src="<?php echo Url::to('@web/images/error.png');?>"); //显示图片
        	$("#msg").text("订单异常");
        	window.clearInterval(timer);
            window.clearInterval(i);
            document.getElementById("msg1").style.display="none";
            document.getElementById("show_div").style.display="none";
            document.getElementById("show_div2").style.display="none";
            document.getElementById("link").style.display="none";
		}
    }
</script>